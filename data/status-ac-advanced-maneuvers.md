# État de la traduction (ac-advanced-maneuvers)

 * **libre**: 56


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0WtqP7Wei3AxHh5M.htm](ac-advanced-maneuvers/0WtqP7Wei3AxHh5M.htm)|Frightening Display|Posture effrayante|libre|
|[6LvtdtVVwtbzqUXG.htm](ac-advanced-maneuvers/6LvtdtVVwtbzqUXG.htm)|Screaming Skull|Crâne hurlant|libre|
|[7C9iFWBpEOJs4Uo2.htm](ac-advanced-maneuvers/7C9iFWBpEOJs4Uo2.htm)|Pterosaur Swoop|Piqué du ptérosaure|libre|
|[7H8wqKtsAaSgyLyp.htm](ac-advanced-maneuvers/7H8wqKtsAaSgyLyp.htm)|Grab and Sting|Saisir et piquer|libre|
|[8JdETVTpbZAgtBA3.htm](ac-advanced-maneuvers/8JdETVTpbZAgtBA3.htm)|Breath Weapon|Arme de souffle|libre|
|[9o7NwxGqohpa9M7r.htm](ac-advanced-maneuvers/9o7NwxGqohpa9M7r.htm)|Pick at the Meat|Piquer la viande|libre|
|[aDuPDWgutiDIoWjW.htm](ac-advanced-maneuvers/aDuPDWgutiDIoWjW.htm)|Gulp Blood|Gorgée de sang|libre|
|[AH3kNbdGLYaRjVAr.htm](ac-advanced-maneuvers/AH3kNbdGLYaRjVAr.htm)|Bony Constriction|Constriction osseuse|libre|
|[ClUiCoBn3lHBBK5c.htm](ac-advanced-maneuvers/ClUiCoBn3lHBBK5c.htm)|Darting Attack|Attaque foudroyante|libre|
|[cpMkN79PdNci3nGp.htm](ac-advanced-maneuvers/cpMkN79PdNci3nGp.htm)|Distracting Spray|Aspersion distrayante|libre|
|[cVrW2GGLVpydj8h5.htm](ac-advanced-maneuvers/cVrW2GGLVpydj8h5.htm)|Blood Feast|Festin sanglant|libre|
|[ePNzRvGvBqzIujJr.htm](ac-advanced-maneuvers/ePNzRvGvBqzIujJr.htm)|Circling Flyby|Vol circulaire|libre|
|[EvuMVR9ut9wIHOtq.htm](ac-advanced-maneuvers/EvuMVR9ut9wIHOtq.htm)|Bay|Plainte|libre|
|[EwCqdvU8WOw2SSxm.htm](ac-advanced-maneuvers/EwCqdvU8WOw2SSxm.htm)|Boar Charge|Charge du sanglier|libre|
|[F98ajoIakyOPEuwj.htm](ac-advanced-maneuvers/F98ajoIakyOPEuwj.htm)|Shred|Déchiqueter|libre|
|[FKh7ZMNyKxgnV7kI.htm](ac-advanced-maneuvers/FKh7ZMNyKxgnV7kI.htm)|Throw Rock|Lancé de rocher|libre|
|[g4JEtCnKXyY4LJpm.htm](ac-advanced-maneuvers/g4JEtCnKXyY4LJpm.htm)|Feast on the Fallen|Festin sur le déchu|libre|
|[gvOo0KSSHkqfW3j5.htm](ac-advanced-maneuvers/gvOo0KSSHkqfW3j5.htm)|Grabbing Branches|Branches accrocheuses|libre|
|[gzefJocgdWbXWWPi.htm](ac-advanced-maneuvers/gzefJocgdWbXWWPi.htm)|Extend Pseudopod|Étendre un pseudopode|libre|
|[H1ElYt6KovGYGzLD.htm](ac-advanced-maneuvers/H1ElYt6KovGYGzLD.htm)|Lumbering Knockdown|Renversement boutoir|libre|
|[h1Rldre8WVjUR5XO.htm](ac-advanced-maneuvers/h1Rldre8WVjUR5XO.htm)|Disgusting Gallop|Galop immonde|libre|
|[HLD4JW6SUn5Oy8C9.htm](ac-advanced-maneuvers/HLD4JW6SUn5Oy8C9.htm)|Badger Rage|Rage du blaireau|libre|
|[IJYikMs3TzYGpdHm.htm](ac-advanced-maneuvers/IJYikMs3TzYGpdHm.htm)|Weighted Kick|Ruade lestée|libre|
|[IoWvKP6WDijlkrin.htm](ac-advanced-maneuvers/IoWvKP6WDijlkrin.htm)|Knock Aside|Pousser de côté|libre|
|[JMHoCFb886K6dT1n.htm](ac-advanced-maneuvers/JMHoCFb886K6dT1n.htm)|Rhinoceros Charge|Charge du rhinocéros|libre|
|[kKW68gi1FNJlNSOp.htm](ac-advanced-maneuvers/kKW68gi1FNJlNSOp.htm)|Constrict|Constriction|libre|
|[LaBfTYUsvoI3nscv.htm](ac-advanced-maneuvers/LaBfTYUsvoI3nscv.htm)|Take a Taste|Goûte un morceau|libre|
|[LCilgiiIZp408R3k.htm](ac-advanced-maneuvers/LCilgiiIZp408R3k.htm)|Flamethrower|Lance-flammes|libre|
|[lrchn6ROZcuKCg3C.htm](ac-advanced-maneuvers/lrchn6ROZcuKCg3C.htm)|Careful Withdraw|Retrait précautionneux|libre|
|[MNiXOHI1Ezh0D24r.htm](ac-advanced-maneuvers/MNiXOHI1Ezh0D24r.htm)|Tail Swipe|Coup de queue|libre|
|[MQv67R7KBIrJkshN.htm](ac-advanced-maneuvers/MQv67R7KBIrJkshN.htm)|Grip Throat|Gorge serrée|libre|
|[NMaMTiDB40L5O41S.htm](ac-advanced-maneuvers/NMaMTiDB40L5O41S.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[oCgdnQP5bMDD02YC.htm](ac-advanced-maneuvers/oCgdnQP5bMDD02YC.htm)|Bear Hug|Étreinte de l'ours|libre|
|[ODGf6brAHHKrSAPo.htm](ac-advanced-maneuvers/ODGf6brAHHKrSAPo.htm)|Wing Thrash|Balayage d'aile|libre|
|[oisXYTTYE0TABboA.htm](ac-advanced-maneuvers/oisXYTTYE0TABboA.htm)|Gnaw|Ronger|libre|
|[ow3S5zhS51Nj8tsI.htm](ac-advanced-maneuvers/ow3S5zhS51Nj8tsI.htm)|Unnerving Screech|Cri strident|libre|
|[oYEXImMSzg0eDqzR.htm](ac-advanced-maneuvers/oYEXImMSzg0eDqzR.htm)|Grabbing Trunk|Trompe préhensile|libre|
|[oyNZpuQ6EU3prvo2.htm](ac-advanced-maneuvers/oyNZpuQ6EU3prvo2.htm)|Spiked Bunker|Bunker pointu|libre|
|[PAXJgysWqv3T0tTD.htm](ac-advanced-maneuvers/PAXJgysWqv3T0tTD.htm)|Sand Stride|Marche sur le sable|libre|
|[pMPtswKNokDNyYR2.htm](ac-advanced-maneuvers/pMPtswKNokDNyYR2.htm)|Defensive Curl|Posture défensive|libre|
|[q0Vh2V6KlzdMi1Pv.htm](ac-advanced-maneuvers/q0Vh2V6KlzdMi1Pv.htm)|Tongue Pull|Traction de la langue|libre|
|[TCEQtO5DawzVAIyT.htm](ac-advanced-maneuvers/TCEQtO5DawzVAIyT.htm)|Tearing Clutch|Serrage déchirant|libre|
|[tsOaXTRWQvsKTyaL.htm](ac-advanced-maneuvers/tsOaXTRWQvsKTyaL.htm)|Gallop|Galop|libre|
|[ttJ1CanrAuehoSV8.htm](ac-advanced-maneuvers/ttJ1CanrAuehoSV8.htm)|Death Roll|Roulade mortelle|libre|
|[U0J8J5ukmqnOskE8.htm](ac-advanced-maneuvers/U0J8J5ukmqnOskE8.htm)|Cat Pounce|Bond du félin|libre|
|[Upr7u0KhPAIMOqXv.htm](ac-advanced-maneuvers/Upr7u0KhPAIMOqXv.htm)|Snatch and Zap|Saisir et électrocuter|libre|
|[uR3j6TAJFeOVUHpY.htm](ac-advanced-maneuvers/uR3j6TAJFeOVUHpY.htm)|Lurching Rush|Embardée ruante|libre|
|[Uv49tQCwUUjaMCj5.htm](ac-advanced-maneuvers/Uv49tQCwUUjaMCj5.htm)|Bouncing Slam|Coup rebondissant|libre|
|[W6zkL0y4MqvlR0yW.htm](ac-advanced-maneuvers/W6zkL0y4MqvlR0yW.htm)|Rolling Knockdown|Roulade renversante|libre|
|[WdTzlCO2i4loeYkQ.htm](ac-advanced-maneuvers/WdTzlCO2i4loeYkQ.htm)|Ultrasonic Scream|Cri ultrasonique|libre|
|[x4clojDNIvYB5V7X.htm](ac-advanced-maneuvers/x4clojDNIvYB5V7X.htm)|Drench|Tremper|libre|
|[X4VjMfVVAjZjwcrT.htm](ac-advanced-maneuvers/X4VjMfVVAjZjwcrT.htm)|Flyby Attack|Attaque en vol|libre|
|[Xd4dnyHWexQRVyXn.htm](ac-advanced-maneuvers/Xd4dnyHWexQRVyXn.htm)|Seedpod Spring|Éclosion printanière|libre|
|[xssFjTXqGlsaddvl.htm](ac-advanced-maneuvers/xssFjTXqGlsaddvl.htm)|Knockdown|Renversement|libre|
|[yXw82Zd5nkcCUfRQ.htm](ac-advanced-maneuvers/yXw82Zd5nkcCUfRQ.htm)|Overwhelm|Prise de mâchoire|libre|
|[zSlsnbvsGKdCTzL1.htm](ac-advanced-maneuvers/zSlsnbvsGKdCTzL1.htm)|Hustle|S'empresser|libre|
