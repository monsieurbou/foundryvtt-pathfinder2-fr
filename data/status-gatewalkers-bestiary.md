# État de la traduction (gatewalkers-bestiary)

 * **aucune**: 81


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[16Ru7zfHAHD544xO.htm](gatewalkers-bestiary/16Ru7zfHAHD544xO.htm)|Glimmervine|
|[2i3x4XFKf8G2vN0N.htm](gatewalkers-bestiary/2i3x4XFKf8G2vN0N.htm)|Shroud of Silence|
|[2mq2bKfkW7gtxKla.htm](gatewalkers-bestiary/2mq2bKfkW7gtxKla.htm)|Blackfrost Zombie|
|[2MSC6ZWMXaKAt6OA.htm](gatewalkers-bestiary/2MSC6ZWMXaKAt6OA.htm)|Cordon Alarm|
|[2njOBjTJAhjda0gt.htm](gatewalkers-bestiary/2njOBjTJAhjda0gt.htm)|Shadowshift Field|
|[4LTBKID3ZcZgyOVw.htm](gatewalkers-bestiary/4LTBKID3ZcZgyOVw.htm)|Elder Thing Researcher|
|[4NaQRMtVlGv7ySFb.htm](gatewalkers-bestiary/4NaQRMtVlGv7ySFb.htm)|Mindhammer Mushrooms|
|[5kcL1xtNY0apAwhO.htm](gatewalkers-bestiary/5kcL1xtNY0apAwhO.htm)|Blackfrost Prophet|
|[6c4I4D3DgJmsSZEI.htm](gatewalkers-bestiary/6c4I4D3DgJmsSZEI.htm)|Haunted Aiudara|
|[6HB8S40uMTxM9prV.htm](gatewalkers-bestiary/6HB8S40uMTxM9prV.htm)|Crownbound Constellation|
|[78E4gYNIFP3jd8Bt.htm](gatewalkers-bestiary/78E4gYNIFP3jd8Bt.htm)|Protosoul|
|[7LdB1zPPNiWX4Ji5.htm](gatewalkers-bestiary/7LdB1zPPNiWX4Ji5.htm)|Kvernknurr|
|[7VoxPNp1BMlhGrss.htm](gatewalkers-bestiary/7VoxPNp1BMlhGrss.htm)|Amelekana|
|[8typuC7n5nE01Gzu.htm](gatewalkers-bestiary/8typuC7n5nE01Gzu.htm)|Dreamscraper|
|[9CAmwWwblxh0RWfj.htm](gatewalkers-bestiary/9CAmwWwblxh0RWfj.htm)|Blooming Jijioa|
|[9pu5FhW9oW1mZaaC.htm](gatewalkers-bestiary/9pu5FhW9oW1mZaaC.htm)|Capstan Swabbie|
|[A9gKXEQXwKb4U6wA.htm](gatewalkers-bestiary/A9gKXEQXwKb4U6wA.htm)|Myroga|
|[AeM32zT3kFOOAugS.htm](gatewalkers-bestiary/AeM32zT3kFOOAugS.htm)|Equendia|
|[AihjWbZiorAJ80gE.htm](gatewalkers-bestiary/AihjWbZiorAJ80gE.htm)|Cliffhunter Pteranodon|
|[BlF4FiqivSuigBxy.htm](gatewalkers-bestiary/BlF4FiqivSuigBxy.htm)|Skin Beetle|
|[bnKUnG9XzsKzIUxK.htm](gatewalkers-bestiary/bnKUnG9XzsKzIUxK.htm)|Ancient Tupilaq|
|[bz5OjD8rYMm1M4EC.htm](gatewalkers-bestiary/bz5OjD8rYMm1M4EC.htm)|Delphon|
|[CBCMrAi44SYguevM.htm](gatewalkers-bestiary/CBCMrAi44SYguevM.htm)|Desa-Desa|
|[cgCENLL4fwwGxvzo.htm](gatewalkers-bestiary/cgCENLL4fwwGxvzo.htm)|The Looksee Man|
|[COyGm2KW6YKFE67u.htm](gatewalkers-bestiary/COyGm2KW6YKFE67u.htm)|Skin Beetle Swarm|
|[DAfDg86y1mWHHQEy.htm](gatewalkers-bestiary/DAfDg86y1mWHHQEy.htm)|Temagyr|
|[DP1XF7ygEPlBOdVB.htm](gatewalkers-bestiary/DP1XF7ygEPlBOdVB.htm)|Snowy Owl|
|[DWT0VLHV9MCbbjbx.htm](gatewalkers-bestiary/DWT0VLHV9MCbbjbx.htm)|Stone-Breasted Owl|
|[EfoaqozIWUdm8unh.htm](gatewalkers-bestiary/EfoaqozIWUdm8unh.htm)|Captain Salah|
|[eR1tzQX7WrTsJMb3.htm](gatewalkers-bestiary/eR1tzQX7WrTsJMb3.htm)|Kitari Lambossa|
|[eRRWMgdYHwNIm6zp.htm](gatewalkers-bestiary/eRRWMgdYHwNIm6zp.htm)|Aspect of Ruun|
|[ESgndiwI8BzwhDLi.htm](gatewalkers-bestiary/ESgndiwI8BzwhDLi.htm)|Kithangian|
|[F0YvQwAEdIuZ7mQI.htm](gatewalkers-bestiary/F0YvQwAEdIuZ7mQI.htm)|Poisonous Atmosphere|
|[f1Wpj3sqUrfr4m6K.htm](gatewalkers-bestiary/f1Wpj3sqUrfr4m6K.htm)|Valmar|
|[FGOMsKi1Z0a7uJJy.htm](gatewalkers-bestiary/FGOMsKi1Z0a7uJJy.htm)|The Guest|
|[FgQwsnyJOOSdgUbg.htm](gatewalkers-bestiary/FgQwsnyJOOSdgUbg.htm)|Memory of Osoyo|
|[flnTtopxODLH45H8.htm](gatewalkers-bestiary/flnTtopxODLH45H8.htm)|Test of the God Caller|
|[FmbIqtUllB7Dj3Rw.htm](gatewalkers-bestiary/FmbIqtUllB7Dj3Rw.htm)|Corpse Disposal|
|[GJCVwAFKDG91TXok.htm](gatewalkers-bestiary/GJCVwAFKDG91TXok.htm)|Formian Sting Trench|
|[h5S6ayqQTa7r6p8w.htm](gatewalkers-bestiary/h5S6ayqQTa7r6p8w.htm)|Tree Fisher|
|[HypIeplCDn0Phqs4.htm](gatewalkers-bestiary/HypIeplCDn0Phqs4.htm)|Symbiotic Amoeba|
|[itAkkjCM3EUQ4heC.htm](gatewalkers-bestiary/itAkkjCM3EUQ4heC.htm)|Quarry Construct|
|[JrMv2lQ1hRTpMumk.htm](gatewalkers-bestiary/JrMv2lQ1hRTpMumk.htm)|Alkoasha|
|[jzU3LPOPpzd5RDTW.htm](gatewalkers-bestiary/jzU3LPOPpzd5RDTW.htm)|Dream-Poisoned Door|
|[KgazbWdxKgGNP6jF.htm](gatewalkers-bestiary/KgazbWdxKgGNP6jF.htm)|Sacred Geyser|
|[kgW2ycNTgQsMmkTC.htm](gatewalkers-bestiary/kgW2ycNTgQsMmkTC.htm)|Ainamuuren|
|[kjnBy5qAn5Zfxjuo.htm](gatewalkers-bestiary/kjnBy5qAn5Zfxjuo.htm)|Reservoir Trap|
|[kneoApQfhlRhhp1R.htm](gatewalkers-bestiary/kneoApQfhlRhhp1R.htm)|Oaksteward Enforcer|
|[KzQARBhXpizdWcoP.htm](gatewalkers-bestiary/KzQARBhXpizdWcoP.htm)|Scholar's Bane|
|[m1M7f4Rd5zuBRVDe.htm](gatewalkers-bestiary/m1M7f4Rd5zuBRVDe.htm)|Ogmunzorius|
|[mcNpKicW6L1E327u.htm](gatewalkers-bestiary/mcNpKicW6L1E327u.htm)|Pholebis|
|[MmyFD8SI0IvzCTuR.htm](gatewalkers-bestiary/MmyFD8SI0IvzCTuR.htm)|Oaksteward Enforcer (Gatehouse)|
|[MnOXwpGD3vJIFydk.htm](gatewalkers-bestiary/MnOXwpGD3vJIFydk.htm)|Etward Ritalson|
|[N659rGqG2uyI2gp5.htm](gatewalkers-bestiary/N659rGqG2uyI2gp5.htm)|Ilverani Sentry|
|[NeNIMJO0P1KA8KCM.htm](gatewalkers-bestiary/NeNIMJO0P1KA8KCM.htm)|Demontangle|
|[PAQHiQqCcfFo8koZ.htm](gatewalkers-bestiary/PAQHiQqCcfFo8koZ.htm)|Ilakni|
|[PGqYo173cQAYLHTz.htm](gatewalkers-bestiary/PGqYo173cQAYLHTz.htm)|Valmar's Pit Trap|
|[piLAtgB51zXEHmFt.htm](gatewalkers-bestiary/piLAtgB51zXEHmFt.htm)|Yaiafineti|
|[Q1HLeQDQdukZTYmu.htm](gatewalkers-bestiary/Q1HLeQDQdukZTYmu.htm)|Rushing Wind|
|[qbTNd1UeIxYEihNs.htm](gatewalkers-bestiary/qbTNd1UeIxYEihNs.htm)|Mindmoppet|
|[QqWfKGhNB6xJu54F.htm](gatewalkers-bestiary/QqWfKGhNB6xJu54F.htm)|Shadow Guards|
|[qsRrYCsKLiMwZ1I9.htm](gatewalkers-bestiary/qsRrYCsKLiMwZ1I9.htm)|Bolan Nogasso|
|[rrMtTABR1587fJGU.htm](gatewalkers-bestiary/rrMtTABR1587fJGU.htm)|Ghodrak the Quick|
|[RwdczmexMQkSckwL.htm](gatewalkers-bestiary/RwdczmexMQkSckwL.htm)|Ocluai|
|[SbJe7zmuevvBFhad.htm](gatewalkers-bestiary/SbJe7zmuevvBFhad.htm)|Abyssal Muckrager|
|[tjDGU0T5RYZG49EN.htm](gatewalkers-bestiary/tjDGU0T5RYZG49EN.htm)|Elder Thing|
|[tWT89vRMBNtU2i5y.htm](gatewalkers-bestiary/tWT89vRMBNtU2i5y.htm)|Freezing Floor|
|[TWu5SOTZzsv97Q1O.htm](gatewalkers-bestiary/TWu5SOTZzsv97Q1O.htm)|Falling Bridge|
|[UPmGB6VZqrF2Mo8B.htm](gatewalkers-bestiary/UPmGB6VZqrF2Mo8B.htm)|Toppling Shelves|
|[uTaGZT1vl7UG9tdf.htm](gatewalkers-bestiary/uTaGZT1vl7UG9tdf.htm)|Apothecary's Cabinet|
|[wdeje31VJ7Ivcdd6.htm](gatewalkers-bestiary/wdeje31VJ7Ivcdd6.htm)|Sakuachi|
|[wi3CaCYAyWcQy221.htm](gatewalkers-bestiary/wi3CaCYAyWcQy221.htm)|Violet Mister|
|[wt9kH6AwMRuSeGBV.htm](gatewalkers-bestiary/wt9kH6AwMRuSeGBV.htm)|Vhisa Resgrido|
|[X7ScCatwncmk5P9o.htm](gatewalkers-bestiary/X7ScCatwncmk5P9o.htm)|Immolis|
|[xSKsWdRWd69OA92K.htm](gatewalkers-bestiary/xSKsWdRWd69OA92K.htm)|Etward's Nightmare|
|[y9W4wnQ5mzs09Px8.htm](gatewalkers-bestiary/y9W4wnQ5mzs09Px8.htm)|Soporific Lecture|
|[yJSqKx1kBbadhQX6.htm](gatewalkers-bestiary/yJSqKx1kBbadhQX6.htm)|Kareq|
|[yTREQiKhDqp16UeO.htm](gatewalkers-bestiary/yTREQiKhDqp16UeO.htm)|Green Monkey|
|[Yy2DDehhj47BMWgP.htm](gatewalkers-bestiary/Yy2DDehhj47BMWgP.htm)|Blackfrost Guecubu|
|[z4QsiqC9EDD7UV1s.htm](gatewalkers-bestiary/z4QsiqC9EDD7UV1s.htm)|Exploding Stove|
|[ZzVs5T27LnLhRZ5u.htm](gatewalkers-bestiary/ZzVs5T27LnLhRZ5u.htm)|Kaneepo the Slim|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
