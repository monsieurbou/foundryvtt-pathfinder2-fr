# État de la traduction (campaign-effects)

 * **libre**: 24
 * **aucune**: 17
 * **changé**: 9


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[2NKpljDuFttd9054.htm](campaign-effects/2NKpljDuFttd9054.htm)|Grudge|
|[4ZGa4tzrh2vLNZR3.htm](campaign-effects/4ZGa4tzrh2vLNZR3.htm)|Sap Life|
|[54Q1eRCmuu6ueM4j.htm](campaign-effects/54Q1eRCmuu6ueM4j.htm)|Curiosity|
|[7vui0qQPmlM5SD99.htm](campaign-effects/7vui0qQPmlM5SD99.htm)|Passion|
|[8Nj0vGsCqiyb9WgY.htm](campaign-effects/8Nj0vGsCqiyb9WgY.htm)|Undying Flesh|
|[DvHX1hgZE6a6jhTm.htm](campaign-effects/DvHX1hgZE6a6jhTm.htm)|Delusion|
|[ECCyLvt5uRw8Qv8V.htm](campaign-effects/ECCyLvt5uRw8Qv8V.htm)|Effect: Spirit Power (Passion)|
|[gYzQuqQy3egp45kj.htm](campaign-effects/gYzQuqQy3egp45kj.htm)|Resist Death|
|[ir1MMSrUECPiRs07.htm](campaign-effects/ir1MMSrUECPiRs07.htm)|Flight|
|[M5vBlRWVnXoyRpqS.htm](campaign-effects/M5vBlRWVnXoyRpqS.htm)|Dematerialize|
|[N2EqUYDVS5I3rnKS.htm](campaign-effects/N2EqUYDVS5I3rnKS.htm)|Duty|
|[PEZoeYCF2R2nZG88.htm](campaign-effects/PEZoeYCF2R2nZG88.htm)|Obsession|
|[QXXgTRJyycu8FYyD.htm](campaign-effects/QXXgTRJyycu8FYyD.htm)|Flickering Figure|
|[R4oTPhsKSqCBd64F.htm](campaign-effects/R4oTPhsKSqCBd64F.htm)|River's Reflection|
|[rl2s1MXAXnGOQF9c.htm](campaign-effects/rl2s1MXAXnGOQF9c.htm)|Chilling Paralysis|
|[rSlgE1Xlt3RDwYr4.htm](campaign-effects/rSlgE1Xlt3RDwYr4.htm)|Spite|
|[Rzlo3kvFUavFQJ3S.htm](campaign-effects/Rzlo3kvFUavFQJ3S.htm)|Effect: Spirit Powers|
|[SidX2yZ5Z0ORfpdr.htm](campaign-effects/SidX2yZ5Z0ORfpdr.htm)|Love|
|[tWFcPYueuOdSgSgg.htm](campaign-effects/tWFcPYueuOdSgSgg.htm)|Effect: Forgive Foe|
|[UQmizjvdBfSVYoZe.htm](campaign-effects/UQmizjvdBfSVYoZe.htm)|Effect: Spirit Power (Flight)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[5DBYunGL2HV8aekO.htm](campaign-effects/5DBYunGL2HV8aekO.htm)|Resonant Reflection: Reflection of Stone|Reflet retentissant : Reflet de pierre|changé|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|Cocktail : Lumière|changé|
|[scgEAXAqHEr3Egeb.htm](campaign-effects/scgEAXAqHEr3Egeb.htm)|Resonant Reflection: Reflection of Water|Reflet retentissant : reflet d'eau|changé|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|Bénédiction de Geb|changé|
|[uDQw7YPMiKPXCbaV.htm](campaign-effects/uDQw7YPMiKPXCbaV.htm)|Resonant Reflection: Reflection of Light|Reflet retentissant : Reflet de lumière|changé|
|[UXBOvlJbnI76UoGp.htm](campaign-effects/UXBOvlJbnI76UoGp.htm)|Resonant Reflection: Reflection of Storm|Reflet retentissant : Reflet de tempête|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1iIObOrXUGA3RwQ9.htm](campaign-effects/1iIObOrXUGA3RwQ9.htm)|Effect: Bickering Family|Effet : Chamailleries familiales|libre|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|Effet : Réflexes améliorés|libre|
|[8eMGYNilWV3dFAUI.htm](campaign-effects/8eMGYNilWV3dFAUI.htm)|Resonant Reflection: Reflection of Life|Reflet retentissant : Reflet de vie|libre|
|[8KtjzHHR7n7ALvQc.htm](campaign-effects/8KtjzHHR7n7ALvQc.htm)|Effect: Seasonal Boon (Outskirt Dweller)|Effet : Récompense saisonnière (Habitant de la périphérie)|libre|
|[axtAzam8kiKbTD2c.htm](campaign-effects/axtAzam8kiKbTD2c.htm)|Malevolence|Malveillance|libre|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|Cocktail : Assistance surnaturelle|libre|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|Effet : Gorge et langue brulées (Chanter ou Crier)|libre|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|Effet : Reflet de vie (Guérison accélérée)|libre|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|Cocktail : Chance|libre|
|[GBaN8XZrzFp7C7fL.htm](campaign-effects/GBaN8XZrzFp7C7fL.htm)|Effect: Little Ripple's Blessing|Effet : Bénédiction de Petit Ripple|libre|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|Cocktail : Prestidigitation|libre|
|[LJOZk2O6KS3iulXy.htm](campaign-effects/LJOZk2O6KS3iulXy.htm)|Effect: Boar Form|Effet : Forme de sanglier|libre|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|Effet : crampes d'estomac intenses|libre|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|Effet : Langue brûlée (Linguistique)|libre|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|Effet : Espoir ou Désespoir (Succès critique)|libre|
|[NwF6m7SyaqwjdQxT.htm](campaign-effects/NwF6m7SyaqwjdQxT.htm)|Effect: Manipulate Luck - Drusilla (Good)|Effet : Manipulation de la chance - Drusilla (Bonne)|libre|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|Effet : Langue et gorge brûlées (linguistique)|libre|
|[PnAzso0F6XUvbYKb.htm](campaign-effects/PnAzso0F6XUvbYKb.htm)|Effect: Tiger Form|Effet : Forme de tigre|libre|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|Effet : Espoir ou désespoir (Échec ou Échec critique)|libre|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|Effet : Mal de tête immédiat et intense|libre|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened Awareness|Effet : Perception augmentée|libre|
|[W8MD8WCjVC4pLlqn.htm](campaign-effects/W8MD8WCjVC4pLlqn.htm)|Effect: Heart Break|Effet : Coeur brisé|libre|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|Effet : Langue brûlée (Pas de chant ou de cri)|libre|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|Effet : Perception affûtée|libre|
