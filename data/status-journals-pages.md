# État de la traduction (journals-pages)

 * **changé**: 235
 * **libre**: 183


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09VwtSsYldMkVzU1.htm](journals-pages/09VwtSsYldMkVzU1.htm)|Runelord|Seigneur des runes (Archétype de classe)|changé|
|[0e2o62hyCxbIkVoC.htm](journals-pages/0e2o62hyCxbIkVoC.htm)|Assassin|Assassin|changé|
|[0UrqPv7XLDDRwZ13.htm](journals-pages/0UrqPv7XLDDRwZ13.htm)|Pactbinder|Pactiseur|changé|
|[0ZXiLwvBG20MzkO6.htm](journals-pages/0ZXiLwvBG20MzkO6.htm)|Scrollmaster|Maître des parchemins|changé|
|[26YL1jOPQQS6k7A9.htm](journals-pages/26YL1jOPQQS6k7A9.htm)|Kitsune|Kitsune|changé|
|[3B1PqqSZYQj9YXac.htm](journals-pages/3B1PqqSZYQj9YXac.htm)|Hallowed Necromancer|Nécromant sacré|changé|
|[4gKrDFB1GlILn9la.htm](journals-pages/4gKrDFB1GlILn9la.htm)|Scroll Trickster|Usurpateur de parchemins|changé|
|[4VMzG36Pm1oivS6Y.htm](journals-pages/4VMzG36Pm1oivS6Y.htm)|Beastmaster|Maître des bêtes|changé|
|[4YtHynO8i1a33zo9.htm](journals-pages/4YtHynO8i1a33zo9.htm)|Flexible Spellcaster (Class Archetype)|Incantateur flexible (Archétype de classe)|changé|
|[57rElfZ9sWKFQ8Ep.htm](journals-pages/57rElfZ9sWKFQ8Ep.htm)|Knight Vigilant|Chevalier vigilant|changé|
|[5RH1CkZQHYpsOium.htm](journals-pages/5RH1CkZQHYpsOium.htm)|Edgewatch Detective|Officier de la garde du précipice|changé|
|[5S1Pq3GoEzLfBbH7.htm](journals-pages/5S1Pq3GoEzLfBbH7.htm)|Catfolk|Homme-félin ou félide|changé|
|[5v7k1XWQxaP0DoGX.htm](journals-pages/5v7k1XWQxaP0DoGX.htm)|Monk|Moine|changé|
|[6A9zBmTyt8cn6ioa.htm](journals-pages/6A9zBmTyt8cn6ioa.htm)|Eldritch Archer|Archer mystique|changé|
|[6BXJUVvwvzljaCFT.htm](journals-pages/6BXJUVvwvzljaCFT.htm)|Armor Traits|Traits d'armure|changé|
|[6DgCLFEGEHvqmy7Y.htm](journals-pages/6DgCLFEGEHvqmy7Y.htm)|Clockwork Reanimator|Réanimateur nécromécanicien|changé|
|[7uERHpfJplGtYf8w.htm](journals-pages/7uERHpfJplGtYf8w.htm)|Aura of Protection|Aura de protection|changé|
|[88K0xjShkhkckbhv.htm](journals-pages/88K0xjShkhkckbhv.htm)|Gunslinger|Franc-tireur|changé|
|[8qYJAx4O97sSxbUF.htm](journals-pages/8qYJAx4O97sSxbUF.htm)|Basic Actions|Actions basiques|changé|
|[9dpHTBpL3j8ZpqTS.htm](journals-pages/9dpHTBpL3j8ZpqTS.htm)|Anadi|Anadi|changé|
|[9g14DjBZNj27goTt.htm](journals-pages/9g14DjBZNj27goTt.htm)|Soul Warden|Gardien de l'âme|changé|
|[A5HwvubNii1d2UND.htm](journals-pages/A5HwvubNii1d2UND.htm)|Sniping Duo|Duo de tireur d'élite|changé|
|[a5XWVMjK5OsZciRi.htm](journals-pages/a5XWVMjK5OsZciRi.htm)|Equipment|Équipement|changé|
|[aAm6jY2k5qBuWETd.htm](journals-pages/aAm6jY2k5qBuWETd.htm)|Inventor|Inventeur|changé|
|[ackdHdIAmSb0AiVL.htm](journals-pages/ackdHdIAmSb0AiVL.htm)|Loremaster|Maître savant|changé|
|[aIpIUbupTjw2863C.htm](journals-pages/aIpIUbupTjw2863C.htm)|Ghost|Fantôme|changé|
|[b5VoSJNzoNuqbtvD.htm](journals-pages/b5VoSJNzoNuqbtvD.htm)|Summoner|Conjurateur|changé|
|[b7oePIMEMdFFS5TH.htm](journals-pages/b7oePIMEMdFFS5TH.htm)|Captivator|Enjôleur|changé|
|[bb4nv44qEdpt0pLq.htm](journals-pages/bb4nv44qEdpt0pLq.htm)|Stoke the Magical Flame|Attiser la flamme magique|changé|
|[BHn7nSm4BMPh9QoZ.htm](journals-pages/BHn7nSm4BMPh9QoZ.htm)|Using Deep Backgrounds|Utilisation des Historiques approfondis|changé|
|[bKoV037XO3qiakGw.htm](journals-pages/bKoV037XO3qiakGw.htm)|Ghoul|Goule|changé|
|[bL5cccuvnP5h7Bnn.htm](journals-pages/bL5cccuvnP5h7Bnn.htm)|Animal Trainer|Dompteur|changé|
|[bqaqOx3naiwTozBX.htm](journals-pages/bqaqOx3naiwTozBX.htm)|Oracle|Oracle|changé|
|[bQeLZt29NnHEn6fh.htm](journals-pages/bQeLZt29NnHEn6fh.htm)|Juggler|Jongleur|changé|
|[BRxlFHXu8tN14TDI.htm](journals-pages/BRxlFHXu8tN14TDI.htm)|Staff Acrobat|Funambule|changé|
|[C3P0TycdFCN02p9u.htm](journals-pages/C3P0TycdFCN02p9u.htm)|Bastion|Bastion|changé|
|[C4b2AvMpVpe5BcVv.htm](journals-pages/C4b2AvMpVpe5BcVv.htm)|Material Hardness, Hit Points and Broken Threshold|Solidité du matériau, Points de vie et Seuil de rupture|changé|
|[c8psqIuH4YFi6msK.htm](journals-pages/c8psqIuH4YFi6msK.htm)|Family Background|Historique familial|changé|
|[CB9YE4P7A2Wty1IX.htm](journals-pages/CB9YE4P7A2Wty1IX.htm)|Red Mantis Assassin|Assassin des mantes rouges|changé|
|[cbLJs37Nc8FveriK.htm](journals-pages/cbLJs37Nc8FveriK.htm)|Lizardfolk|Homme-lézard|changé|
|[cdnkS3A8OpOnRjKu.htm](journals-pages/cdnkS3A8OpOnRjKu.htm)|Bellflower Tiller|Laboureur de la Campanule|changé|
|[cjD6YVPQpmYiOPdC.htm](journals-pages/cjD6YVPQpmYiOPdC.htm)|Terrain in Encounters|Terrain dans les rencontres|changé|
|[Ck8L9livQph1GFal.htm](journals-pages/Ck8L9livQph1GFal.htm)|Poppet|Poupée|changé|
|[CkBvj5y1lAm1jnsc.htm](journals-pages/CkBvj5y1lAm1jnsc.htm)|Sun Domain|Domaine du Soleil|changé|
|[CMgYob7Cy4meoQKg.htm](journals-pages/CMgYob7Cy4meoQKg.htm)|Ranger|Rôdeur|changé|
|[CSVoyUvynmM5LzPW.htm](journals-pages/CSVoyUvynmM5LzPW.htm)|Gunslinger|Franc tireur|changé|
|[Czi3XXuNOSE7ISpd.htm](journals-pages/Czi3XXuNOSE7ISpd.htm)|Perfection Domain|Domaine de la Perfection|changé|
|[DAVBjDSysgXgtVQu.htm](journals-pages/DAVBjDSysgXgtVQu.htm)|Gray Gardener|Jardinier gris|changé|
|[DBxoE3TNDPtcKHxH.htm](journals-pages/DBxoE3TNDPtcKHxH.htm)|Animals & Barding|Animaux & bardes|changé|
|[DJiYP5tFBrBMD0We.htm](journals-pages/DJiYP5tFBrBMD0We.htm)|Sorcerer|Ensorceleur|changé|
|[dnbUao9yk8Bs99eT.htm](journals-pages/dnbUao9yk8Bs99eT.htm)|Weapon Traits|Traits d'arme|changé|
|[DOc3Pf8wmVxanTIv.htm](journals-pages/DOc3Pf8wmVxanTIv.htm)|Spellcasting Archetypes|Archétypes d'incantation|changé|
|[dRu66xNqJ9Ihe1or.htm](journals-pages/dRu66xNqJ9Ihe1or.htm)|Difficulty Classes|Degrés de difficulté|changé|
|[DUDsA3sIlOlibvIu.htm](journals-pages/DUDsA3sIlOlibvIu.htm)|High-Quality Items|Objets de haute qualité|changé|
|[duqPy1VMQYNJrw7q.htm](journals-pages/duqPy1VMQYNJrw7q.htm)|Conditions|États|changé|
|[DuWzeY4zugKFFthl.htm](journals-pages/DuWzeY4zugKFFthl.htm)|Hobgoblin|Hobgobelin|changé|
|[DXtjeVaWNB8zSjpA.htm](journals-pages/DXtjeVaWNB8zSjpA.htm)|Champion|Champion|changé|
|[dyusFHHyaQOaQhZ0.htm](journals-pages/dyusFHHyaQOaQhZ0.htm)|Android|Androïde|changé|
|[DztQF2FexWvnzcaE.htm](journals-pages/DztQF2FexWvnzcaE.htm)|Spell Trickster|Mystificateur de sort|changé|
|[EcNSrvwIj22Jxkjz.htm](journals-pages/EcNSrvwIj22Jxkjz.htm)|Skeleton|Squelette|changé|
|[Edn6qQNVcEVpkhPl.htm](journals-pages/Edn6qQNVcEVpkhPl.htm)|Detecting Creatures|Détecter des créatures|changé|
|[eLc6TSgWykjtSeuF.htm](journals-pages/eLc6TSgWykjtSeuF.htm)|Magaambyan Attendant|Gardien du Magaambya|changé|
|[eNTStXeuABNPSLjw.htm](journals-pages/eNTStXeuABNPSLjw.htm)|Witch|Sorcier|changé|
|[ePfibeHcnRcjO6lC.htm](journals-pages/ePfibeHcnRcjO6lC.htm)|Reanimator|Réanimateur|changé|
|[EWiUv3UiR3RHSGlA.htm](journals-pages/EWiUv3UiR3RHSGlA.htm)|Cathartic Mage|Mage cathartique|changé|
|[F5gBiVGbS8YSgPSI.htm](journals-pages/F5gBiVGbS8YSgPSI.htm)|Sixth Pillar|Sixième pilier|changé|
|[F7W15pGOeSeNJD0C.htm](journals-pages/F7W15pGOeSeNJD0C.htm)|Firebrand Braggart|Agitateur vantard|changé|
|[FhIJtKcINEe7fipX.htm](journals-pages/FhIJtKcINEe7fipX.htm)|Grippli|Grippli|changé|
|[FJGPBhYmD7xTFsrW.htm](journals-pages/FJGPBhYmD7xTFsrW.htm)|Spellshot (Class Archetype)|Sortiléro (Archétype de classe)|changé|
|[FOI43M8DJe2lkMwl.htm](journals-pages/FOI43M8DJe2lkMwl.htm)|Structures|Structures|changé|
|[fTxZjmNLckRwZ4Po.htm](journals-pages/fTxZjmNLckRwZ4Po.htm)|Pathfinder Agent|Agent des Éclaireurs|changé|
|[fYJruhQfzs4dj0mp.htm](journals-pages/fYJruhQfzs4dj0mp.htm)|Witch|Sorcier|changé|
|[FzzMt7ybWy03D9qZ.htm](journals-pages/FzzMt7ybWy03D9qZ.htm)|Basic Services|Services et consommables basiques|changé|
|[G3AtnAkIKNHrahmd.htm](journals-pages/G3AtnAkIKNHrahmd.htm)|Vampire|Vampire|changé|
|[gA4Ud8oSUGkkLwAi.htm](journals-pages/gA4Ud8oSUGkkLwAi.htm)|Summoner|Conjurateur|changé|
|[gbBf6x89m4SEFpsL.htm](journals-pages/gbBf6x89m4SEFpsL.htm)|Druid|Druide|changé|
|[gcRmL4Id1ggKDhvg.htm](journals-pages/gcRmL4Id1ggKDhvg.htm)|Inventor|Inventeur|changé|
|[Gn0XGCWDb0N4zLE0.htm](journals-pages/Gn0XGCWDb0N4zLE0.htm)|Goblin|Gobelin|changé|
|[GRjgmsZRxLYcK7Ko.htm](journals-pages/GRjgmsZRxLYcK7Ko.htm)|Class Features|Capacités de classe|changé|
|[Gtvdok4YnzNR2Xx4.htm](journals-pages/Gtvdok4YnzNR2Xx4.htm)|Spells|Sorts|changé|
|[hGKd722Kn4ZxIOJs.htm](journals-pages/hGKd722Kn4ZxIOJs.htm)|Monk|Moine|changé|
|[hOsFg2VpnOshaSOi.htm](journals-pages/hOsFg2VpnOshaSOi.htm)|Environmental Damage|Dégâts environnementaux|changé|
|[HZHzpATOyhTjUDrR.htm](journals-pages/HZHzpATOyhTjUDrR.htm)|Alter Ego|Imposteur|changé|
|[I6FbkKYncDuu7eWq.htm](journals-pages/I6FbkKYncDuu7eWq.htm)|Geomancer|Géomancien|changé|
|[iH3Vrt35DyUbhxZj.htm](journals-pages/iH3Vrt35DyUbhxZj.htm)|Mammoth Lord|Seigneur des mammouths|changé|
|[IliVJaEHaOklcg86.htm](journals-pages/IliVJaEHaOklcg86.htm)|Stamina|Endurance|changé|
|[inxtk4rYj2UZaytg.htm](journals-pages/inxtk4rYj2UZaytg.htm)|Fighter|Guerrier|changé|
|[INYPqM7rs9SNFRba.htm](journals-pages/INYPqM7rs9SNFRba.htm)|Downtime Events|Évènements d'intermèdes|changé|
|[iVY32tNvfS1tPYU2.htm](journals-pages/iVY32tNvfS1tPYU2.htm)|Formula Price|Prix des formules|changé|
|[ixBx2wZpnf4qUoEv.htm](journals-pages/ixBx2wZpnf4qUoEv.htm)|Sorcerer|Ensorceleur|changé|
|[J0AfK0jJgS5WxhCE.htm](journals-pages/J0AfK0jJgS5WxhCE.htm)|Scions of Domora|Scion de Domora|changé|
|[j2hOTtHcdu2SA8Ha.htm](journals-pages/j2hOTtHcdu2SA8Ha.htm)|Shoony|Shoony|changé|
|[j5uZbCwoHOOEO1bC.htm](journals-pages/j5uZbCwoHOOEO1bC.htm)|Runescarred|Scarifié de runes|changé|
|[j6hsF2TWFtGMyClW.htm](journals-pages/j6hsF2TWFtGMyClW.htm)|Shoot Through|Tir à travers|changé|
|[JBPXisA4IdXI9gVn.htm](journals-pages/JBPXisA4IdXI9gVn.htm)|Eldritch Researcher|Chercheur mystique|changé|
|[jEf21wTIEXzGtPU6.htm](journals-pages/jEf21wTIEXzGtPU6.htm)|Detecting with Other Senses|Détecter avec d'autres sens|changé|
|[jKglnHBFvzI80tIe.htm](journals-pages/jKglnHBFvzI80tIe.htm)|Goloma|Goloma|changé|
|[jTiXRtNNvMOnsg98.htm](journals-pages/jTiXRtNNvMOnsg98.htm)|Beast Gunner|Bestioléro|changé|
|[JYJd1xZwqUNRNsqG.htm](journals-pages/JYJd1xZwqUNRNsqG.htm)|Remaster Changes|Changements du remaster|changé|
|[JzUDumJ3Dlyame4z.htm](journals-pages/JzUDumJ3Dlyame4z.htm)|Drow Shootist|Tireur drow|changé|
|[k9Ebp52kt0ZLHtMl.htm](journals-pages/k9Ebp52kt0ZLHtMl.htm)|Swashbuckler|Bretteur|changé|
|[K9Krytj8OtUvQxoc.htm](journals-pages/K9Krytj8OtUvQxoc.htm)|Thaumaturge|Thaumaturge|changé|
|[kAMseeY0TewNmnLQ.htm](journals-pages/kAMseeY0TewNmnLQ.htm)|Deep Backgrounds|Historiques approfondis|changé|
|[kL8yx8vz9FMCIYC1.htm](journals-pages/kL8yx8vz9FMCIYC1.htm)|Treat Wounds|Soigner les blessures|changé|
|[KRUWPDzuwG0LC26d.htm](journals-pages/KRUWPDzuwG0LC26d.htm)|Curse Maelstrom|Maelström maudit|changé|
|[kVC4kgYKbhqPsaDt.htm](journals-pages/kVC4kgYKbhqPsaDt.htm)|Rogue|Roublard|changé|
|[KVoEUJBAbAFUJTqw.htm](journals-pages/KVoEUJBAbAFUJTqw.htm)|Psychic|Psychiste|changé|
|[KXAxZfdMpnHQZQoc.htm](journals-pages/KXAxZfdMpnHQZQoc.htm)|Golarion Calendar|Calendrier de Golarion|changé|
|[kxDloJH5ImKnIV9P.htm](journals-pages/kxDloJH5ImKnIV9P.htm)|Temperature Effects|Effets de la température|changé|
|[l0rlB52Pzy6Vt1Ic.htm](journals-pages/l0rlB52Pzy6Vt1Ic.htm)|Scroll Prices|Prix des parchemins|changé|
|[l76UxUsfrsPnP9U9.htm](journals-pages/l76UxUsfrsPnP9U9.htm)|Worm Caller|Invocateur de vers|changé|
|[lcCPztCqmXpwdmWx.htm](journals-pages/lcCPztCqmXpwdmWx.htm)|Elite/Weak Adjustments|Ajustements de créatures Élite/Faible|changé|
|[LgkfmZnFP3TWtkuy.htm](journals-pages/LgkfmZnFP3TWtkuy.htm)|Living Vessel|Réceptacle vivant|changé|
|[LL4YD16kPqcGibKG.htm](journals-pages/LL4YD16kPqcGibKG.htm)|Ghost Hunter|Chasseur de fantômes|changé|
|[lNWQPwgT4vhYCOT7.htm](journals-pages/lNWQPwgT4vhYCOT7.htm)|Kineticist|Kinétiste ou Cinétiste|changé|
|[lQ9MmX3zg4Bd9hjb.htm](journals-pages/lQ9MmX3zg4Bd9hjb.htm)|GM Screen|Écran de base MJ|changé|
|[lvuutBI9BSCU6cWe.htm](journals-pages/lvuutBI9BSCU6cWe.htm)|Barbarian|Barbare|changé|
|[LWgcWM8B85HHVCtZ.htm](journals-pages/LWgcWM8B85HHVCtZ.htm)|Soulforger|Mentallurgiste|changé|
|[Ly8l2GT6dbvuY3A2.htm](journals-pages/Ly8l2GT6dbvuY3A2.htm)|Treasure|Trésors (Niveau)|changé|
|[M5QOocGyP4zkMo9m.htm](journals-pages/M5QOocGyP4zkMo9m.htm)|Horizon Walker|Arpenteur d'horizon|changé|
|[MKU4d2hIpLuXGN2J.htm](journals-pages/MKU4d2hIpLuXGN2J.htm)|Shadowdancer|Danseur de l'ombre|changé|
|[mmB3EkkdCpLke7Lk.htm](journals-pages/mmB3EkkdCpLke7Lk.htm)|Investigator|Enquêteur|changé|
|[mmfWcV3Iyql5nzTo.htm](journals-pages/mmfWcV3Iyql5nzTo.htm)|Counteract|Contrer|changé|
|[mV0K9sxD3TWnZDcy.htm](journals-pages/mV0K9sxD3TWnZDcy.htm)|Undead Slayer|Tueur de mort-vivant|changé|
|[mXywYJJCM9IVItZz.htm](journals-pages/mXywYJJCM9IVItZz.htm)|Mind Smith|Façonneur spirituel|changé|
|[N01RLMQPaSeoHEuL.htm](journals-pages/N01RLMQPaSeoHEuL.htm)|Martial Artist|Artiste martial|changé|
|[N8aShsDuf3ur0vZ2.htm](journals-pages/N8aShsDuf3ur0vZ2.htm)|Strix|Strix|changé|
|[naQrTTE7CXu5lnNt.htm](journals-pages/naQrTTE7CXu5lnNt.htm)|Kobold|Kobold|changé|
|[ngVnNmi1Qke3lTy0.htm](journals-pages/ngVnNmi1Qke3lTy0.htm)|Oracle|Oracle|changé|
|[nmcEiM2gjqjGWp2c.htm](journals-pages/nmcEiM2gjqjGWp2c.htm)|Cavalier|Cavalier|changé|
|[o71hqcfzhCKXcSml.htm](journals-pages/o71hqcfzhCKXcSml.htm)|Archer|Archer|changé|
|[O79hOcsaQyj3aQC5.htm](journals-pages/O79hOcsaQyj3aQC5.htm)|Archaeologist|Archéologue|changé|
|[O7fKfFxCx3e1YasX.htm](journals-pages/O7fKfFxCx3e1YasX.htm)|Spellmaster|Maître des sorts|changé|
|[O9P1YgtiCgHlPNp5.htm](journals-pages/O9P1YgtiCgHlPNp5.htm)|Pirate|Pirate|changé|
|[oBlKgVYRup5ORqx1.htm](journals-pages/oBlKgVYRup5ORqx1.htm)|Bard|Barde|changé|
|[oBOw6E6V7ncrB1rN.htm](journals-pages/oBOw6E6V7ncrB1rN.htm)|Treasure Per Encounter|Trésor par niveau de rencontre|changé|
|[OBTmo8rD2x8kFzeH.htm](journals-pages/OBTmo8rD2x8kFzeH.htm)|Sleepwalker|Somnambule|changé|
|[OcioDiLTdgzvT8VX.htm](journals-pages/OcioDiLTdgzvT8VX.htm)|Knight Reclaimant|Chevalier reconquérant|changé|
|[oM0DobZe9uEkz9kb.htm](journals-pages/oM0DobZe9uEkz9kb.htm)|Force Open|Ouvrir de force|changé|
|[Om4e4tsgNWrComie.htm](journals-pages/Om4e4tsgNWrComie.htm)|Alchemist|Alchimiste|changé|
|[omDi7otL3UWLVgct.htm](journals-pages/omDi7otL3UWLVgct.htm)|Investigator|Enquêteur|changé|
|[oMIA3aFKvpuV9f2H.htm](journals-pages/oMIA3aFKvpuV9f2H.htm)|Elementalist (Class Archetype)|Élémentaliste (Archétype de classe)|changé|
|[OtBPlgtudM4PlTWD.htm](journals-pages/OtBPlgtudM4PlTWD.htm)|Harrower|Liseur de Tourment|changé|
|[pDmUITVao0FDMnVf.htm](journals-pages/pDmUITVao0FDMnVf.htm)|Halcyon Speaker|Orateur syncrétique|changé|
|[pEjd65isUfXKSB18.htm](journals-pages/pEjd65isUfXKSB18.htm)|Hero Points|Points d'héroïsme|changé|
|[PfoKLAsQeXHsDgzf.htm](journals-pages/PfoKLAsQeXHsDgzf.htm)|Lifestyle Expenses|Dépenses liées au mode de vie|changé|
|[pg9fG1ikcgJZRWlk.htm](journals-pages/pg9fG1ikcgJZRWlk.htm)|Lastwall Sentry|Sentinelle de Dernier-Rempart|changé|
|[PpoPRKuwanrnhd0Y.htm](journals-pages/PpoPRKuwanrnhd0Y.htm)|Living Monolith|Monolithe vivant|changé|
|[PVrMSF93KIejBTMm.htm](journals-pages/PVrMSF93KIejBTMm.htm)|Critical Specialization Effects|Effets critique spécialisé|changé|
|[PVwU1LMWcSgD7Q0m.htm](journals-pages/PVwU1LMWcSgD7Q0m.htm)|Oatia Skysage|Sagecéleste d'Oatie|changé|
|[Q72YDLPh0urLfWPm.htm](journals-pages/Q72YDLPh0urLfWPm.htm)|Vehicle Mechanic|Mécanicien de véhicule|changé|
|[qigU4oNH2KDVvhjX.htm](journals-pages/qigU4oNH2KDVvhjX.htm)|Thaumaturge|Thaumaturge|changé|
|[qIu10HByKeRaPPvD.htm](journals-pages/qIu10HByKeRaPPvD.htm)|Gnome|Gnome|changé|
|[qMNBD91gc5kIKPs2.htm](journals-pages/qMNBD91gc5kIKPs2.htm)|Spellcasting Services|Services d'incantation|changé|
|[qsbc1ZLCWzfl1sfZ.htm](journals-pages/qsbc1ZLCWzfl1sfZ.htm)|Shisk|Shisk|changé|
|[QSk78hQR3zskMlq2.htm](journals-pages/QSk78hQR3zskMlq2.htm)|Cities Domain|Domaine des Villes|changé|
|[quxPxuMub8k6abzN.htm](journals-pages/quxPxuMub8k6abzN.htm)|Ancestral Might|Puissance ancestrale|changé|
|[QWw0D2YPKdz6HJPu.htm](journals-pages/QWw0D2YPKdz6HJPu.htm)|Influential Associate|Associé influent|changé|
|[r43lPEEL7WHOZjHL.htm](journals-pages/r43lPEEL7WHOZjHL.htm)|Lich|Liche|changé|
|[r79jab1XgOdcgvKJ.htm](journals-pages/r79jab1XgOdcgvKJ.htm)|Magus|Magus|changé|
|[rebppVgBVi8J6TT2.htm](journals-pages/rebppVgBVi8J6TT2.htm)|Champion|Champion|changé|
|[rhDvoOHAhAlABiae.htm](journals-pages/rhDvoOHAhAlABiae.htm)|Acrobat|Acrobate|changé|
|[RlMgeku1FNJjF7YR.htm](journals-pages/RlMgeku1FNJjF7YR.htm)|Fetchling|Fetchelin|changé|
|[RoF5NOFBefXAPftS.htm](journals-pages/RoF5NOFBefXAPftS.htm)|Mauler|Cogneur|changé|
|[rsDTKu4HUuKft7fk.htm](journals-pages/rsDTKu4HUuKft7fk.htm)|Swashbuckler|Bretteur|changé|
|[rsx8cRI20oEDgfQh.htm](journals-pages/rsx8cRI20oEDgfQh.htm)|Automaton|Automate|changé|
|[rtAFOZOjWHC2zRNO.htm](journals-pages/rtAFOZOjWHC2zRNO.htm)|Vigilante|Justicier|changé|
|[rvbMGyBr6y5K7HDP.htm](journals-pages/rvbMGyBr6y5K7HDP.htm)|Sprite|Sprite|changé|
|[rxyJUbQosVfgRjLr.htm](journals-pages/rxyJUbQosVfgRjLr.htm)|Crystal Keeper|Gardien des cristaux|changé|
|[s9kLzXOCl2GNT6TY.htm](journals-pages/s9kLzXOCl2GNT6TY.htm)|Dandy|Dandy|changé|
|[sAFWD8D0RKH4m25n.htm](journals-pages/sAFWD8D0RKH4m25n.htm)|Dual-Weapon Warrior|Combattant à deux armes|changé|
|[SAnmegCTIqGW9S7S.htm](journals-pages/SAnmegCTIqGW9S7S.htm)|Family Domain|Domaine de la Famille|changé|
|[SiMXtLf4YQeigzIE.htm](journals-pages/SiMXtLf4YQeigzIE.htm)|Senses|Sens|changé|
|[SNhp04cirQnnBTbb.htm](journals-pages/SNhp04cirQnnBTbb.htm)|Azarketi|Azarketi|changé|
|[SnSGU7DPiogZ9JOr.htm](journals-pages/SnSGU7DPiogZ9JOr.htm)|Automatic Bonus Progression|Progression automatique des bonus|changé|
|[SO9d7RSf1zqmTlmW.htm](journals-pages/SO9d7RSf1zqmTlmW.htm)|Dragon Disciple|Disciple draconique|changé|
|[SpWLU1ixQRT6rkUP.htm](journals-pages/SpWLU1ixQRT6rkUP.htm)|Downtime Tasks|Tâches d'Intermède|changé|
|[SUDV5hFZ9WocxWqv.htm](journals-pages/SUDV5hFZ9WocxWqv.htm)|Blessed One|Élu divin|changé|
|[SWkwNivrfzWZqas9.htm](journals-pages/SWkwNivrfzWZqas9.htm)|Leshy|Léchi|changé|
|[sY25SoDaHBPIG5Jw.htm](journals-pages/sY25SoDaHBPIG5Jw.htm)|Nantambu Chime-Ringer|Sonneur de carillon de Nantambu|changé|
|[tJllU1E8g7W1QxYe.htm](journals-pages/tJllU1E8g7W1QxYe.htm)|Cover|Abri|changé|
|[ts7gtyAhDEI4V7Ve.htm](journals-pages/ts7gtyAhDEI4V7Ve.htm)|Character Wealth|Richesse du personnage|changé|
|[ttkG4geKXTao5pku.htm](journals-pages/ttkG4geKXTao5pku.htm)|Alkenstar Agent|Agent d'Alkenastre|changé|
|[TwSQ0VPXZL0mfPez.htm](journals-pages/TwSQ0VPXZL0mfPez.htm)|Bounded Spellcasting Archetypes|Archétypes d'incantation limitée|changé|
|[u6IGUd8Gtarj6h7K.htm](journals-pages/u6IGUd8Gtarj6h7K.htm)|Versatile Heritages|Héritages polyvalents|changé|
|[uA6XIArPfGSwBvZi.htm](journals-pages/uA6XIArPfGSwBvZi.htm)|Psychic Duelist|Duelliste psychique|changé|
|[ufWdokSN5W3Einus.htm](journals-pages/ufWdokSN5W3Einus.htm)|Conrasu|Conrasu|changé|
|[uGsGn5sgR7wn0TQD.htm](journals-pages/uGsGn5sgR7wn0TQD.htm)|Turpin Rowe Lumberjack|Bûcheron de Turpin Rowe|changé|
|[ukLpSqTkmRLvZj0z.htm](journals-pages/ukLpSqTkmRLvZj0z.htm)|Stalwart Defender|Défenseur fidèle|changé|
|[uWJfFysCcoU1o2Hs.htm](journals-pages/uWJfFysCcoU1o2Hs.htm)|Skill Actions|Actions de compétence|changé|
|[Ux0sa5SUBu616i5k.htm](journals-pages/Ux0sa5SUBu616i5k.htm)|Alchemist|Alchimiste|changé|
|[uxdPLsxY8fNrenDR.htm](journals-pages/uxdPLsxY8fNrenDR.htm)|Death and Dying|Mort et l'état Mourant|changé|
|[uYCTmos3yZtbD9qs.htm](journals-pages/uYCTmos3yZtbD9qs.htm)|Barbarian|Barbare|changé|
|[vKuJVojicBmNsL1C.htm](journals-pages/vKuJVojicBmNsL1C.htm)|Butterfly Blade|Lame papillon|changé|
|[VnR8gBBq1oCG7YQh.htm](journals-pages/VnR8gBBq1oCG7YQh.htm)|Reputation|Réputation|changé|
|[voI7uPS9vsG74JIn.htm](journals-pages/voI7uPS9vsG74JIn.htm)|Halfling|Halfelin|changé|
|[VR9UdfhZphZGZdsx.htm](journals-pages/VR9UdfhZphZGZdsx.htm)|Bulk Conversions for Different Sizes|Conversions d'encombrement pour des tailles différentes|changé|
|[vS9iMDapZN9uW33Q.htm](journals-pages/vS9iMDapZN9uW33Q.htm)|Bright Lion|Lion radieux|changé|
|[vTB1bTYVE4onyyy1.htm](journals-pages/vTB1bTYVE4onyyy1.htm)|Fleshwarp|Distordu|changé|
|[wHs6IGCMaXsgFNop.htm](journals-pages/wHs6IGCMaXsgFNop.htm)|Shadowcaster|Incantateur de l'ombre|changé|
|[WL8YX3zIl4AhIrN9.htm](journals-pages/WL8YX3zIl4AhIrN9.htm)|Changing Equipment|Changement d'équipement|changé|
|[wMUAm1PJ1HjJ8fFU.htm](journals-pages/wMUAm1PJ1HjJ8fFU.htm)|Pistol Phenom|Phénomène du pistolet|changé|
|[WvleaXPfigawBvtN.htm](journals-pages/WvleaXPfigawBvtN.htm)|Exorcist|Exorciste|changé|
|[xBV8oGvecw7abZxc.htm](journals-pages/xBV8oGvecw7abZxc.htm)|Gnoll|Gnoll|changé|
|[XCxK6OGFY0KvJPo3.htm](journals-pages/XCxK6OGFY0KvJPo3.htm)|Learn a Spell|Apprendre un sort|changé|
|[XH3xabCB01FiCRal.htm](journals-pages/XH3xabCB01FiCRal.htm)|Proficiency Without Level|Maîtrise sans niveau|changé|
|[XHSXMcRaMOsbjoO1.htm](journals-pages/XHSXMcRaMOsbjoO1.htm)|Rending Swipe|Coup éventreur|changé|
|[XLug8rxIe1KPX6Nf.htm](journals-pages/XLug8rxIe1KPX6Nf.htm)|Provocator|Provocator|changé|
|[xMdIENo4w9bEzZuK.htm](journals-pages/xMdIENo4w9bEzZuK.htm)|Ghost Eater|Mangeur de fantômes|changé|
|[XszmLWy174esRrlg.htm](journals-pages/XszmLWy174esRrlg.htm)|Wizard|Magicien|changé|
|[XzRv30zY3UXRbBho.htm](journals-pages/XzRv30zY3UXRbBho.htm)|Dwarf|Nain|changé|
|[Y3DFBCWiM9GBIlfl.htm](journals-pages/Y3DFBCWiM9GBIlfl.htm)|Moon Domain|Domaine de la Lune|changé|
|[Y4DiXz5I8krCh1fC.htm](journals-pages/Y4DiXz5I8krCh1fC.htm)|Swordmaster|Maître épéiste|changé|
|[yaMJsfYZmWJLqbFE.htm](journals-pages/yaMJsfYZmWJLqbFE.htm)|Ambition Domain|Domaine de l'Ambition|changé|
|[yaR8E2drX6pFUFCK.htm](journals-pages/yaR8E2drX6pFUFCK.htm)|Familiar Master|Maître familier|changé|
|[ydbCjJ9PPmRzZhDN.htm](journals-pages/ydbCjJ9PPmRzZhDN.htm)|Creation Domain|Domaine de la Création|changé|
|[ydELQFQq0lGLTvrA.htm](journals-pages/ydELQFQq0lGLTvrA.htm)|Time Mage|Mage du temps|changé|
|[Yepac621abWWw5qj.htm](journals-pages/Yepac621abWWw5qj.htm)|Monster Abilities|Capacités des monstres|changé|
|[ygboVjCAFRpcysUb.htm](journals-pages/ygboVjCAFRpcysUb.htm)|Player Screen|Écran du joueur|changé|
|[YGIwmuVOJI7uNhGM.htm](journals-pages/YGIwmuVOJI7uNhGM.htm)|Tengu|Tengu|changé|
|[ygzv72IJNmjh0SPB.htm](journals-pages/ygzv72IJNmjh0SPB.htm)|Psychic|Psychiste|changé|
|[yHqDPytVY9bxWo9I.htm](journals-pages/yHqDPytVY9bxWo9I.htm)|Item Quirks|Bizarreries des objets|changé|
|[YODf1eNWi9jnR93y.htm](journals-pages/YODf1eNWi9jnR93y.htm)|Pactbound Initiate|Initié lié par un Pacte|changé|
|[yOEiNjHLQ6XuOruq.htm](journals-pages/yOEiNjHLQ6XuOruq.htm)|Overwatch|Observateur|changé|
|[YQXGtBHVoiSYUjtl.htm](journals-pages/YQXGtBHVoiSYUjtl.htm)|Twilight Speaker|Orateur du crépuscule|changé|
|[YVLMgNxXTUQuDJgp.htm](journals-pages/YVLMgNxXTUQuDJgp.htm)|Travel Speed|Vitesse de voyage|changé|
|[YVoyzV9v4QyP2UIC.htm](journals-pages/YVoyzV9v4QyP2UIC.htm)|Magus|Magus|changé|
|[Z2xXPHkxGLFTXzdI.htm](journals-pages/Z2xXPHkxGLFTXzdI.htm)|Creature Identification|Identification d'une créature|changé|
|[zB1mqE9IeyfyQDnn.htm](journals-pages/zB1mqE9IeyfyQDnn.htm)|Exploration Activities|Activités d'exploration|changé|
|[ZewC2i5YdZPsWO8X.htm](journals-pages/ZewC2i5YdZPsWO8X.htm)|Mummy|Momie|changé|
|[ZJHhPFjLnizAaUM1.htm](journals-pages/ZJHhPFjLnizAaUM1.htm)|Cleric|Prêtre|changé|
|[zoSkNev57C8OEDYL.htm](journals-pages/zoSkNev57C8OEDYL.htm)|Ratfolk|Homme-rat|changé|
|[zVcfWP1ac0JhNEhY.htm](journals-pages/zVcfWP1ac0JhNEhY.htm)|Quick Adventure Groups|Groupes d'aventure rapide|changé|
|[zxY04QNfB90gjLoh.htm](journals-pages/zxY04QNfB90gjLoh.htm)|Magic Warrior|Guerrier magique|changé|
|[zymZw8KQe0nkRf5L.htm](journals-pages/zymZw8KQe0nkRf5L.htm)|Victory Points|Points de victoire|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0GwpYEjCHWyfQvgg.htm](journals-pages/0GwpYEjCHWyfQvgg.htm)|Knowledge Domain|Domaine du Savoir|libre|
|[0pPtVXH6Duitakbp.htm](journals-pages/0pPtVXH6Duitakbp.htm)|Folklorist|Folkloriste|libre|
|[0RnXjFday2Lp4ECG.htm](journals-pages/0RnXjFday2Lp4ECG.htm)|Sample Chases|Exemples d'obstacles|libre|
|[0wCEUwABKPdKPj8e.htm](journals-pages/0wCEUwABKPdKPj8e.htm)|Dreams Domain|Domaine des Rêves|libre|
|[1F84oh5hoG86mZIw.htm](journals-pages/1F84oh5hoG86mZIw.htm)|Marshal|Capitaine|libre|
|[1kRGIb5LPPKypQpN.htm](journals-pages/1kRGIb5LPPKypQpN.htm)|Kineticist|Kinétiste ou Cinétiste|libre|
|[1lCAkGgIYYkczZ28.htm](journals-pages/1lCAkGgIYYkczZ28.htm)|Vishkanya|Vishkanya|libre|
|[2CbiV0VtDKZfQNte.htm](journals-pages/2CbiV0VtDKZfQNte.htm)|Viking|Viking|libre|
|[3a9kFb9x9UXeLA1s.htm](journals-pages/3a9kFb9x9UXeLA1s.htm)|Quick Environmental Details|Détails environnementaux|libre|
|[3cfdiId1IoxLsF9V.htm](journals-pages/3cfdiId1IoxLsF9V.htm)|Elf|Elfe|libre|
|[3H168yyaP6ze75kp.htm](journals-pages/3H168yyaP6ze75kp.htm)|Inspiring Relationships|Relations inspirantes|libre|
|[3P0NWwP3s7bIiidH.htm](journals-pages/3P0NWwP3s7bIiidH.htm)|Time Domain|Domaine du Temps|libre|
|[40koksC576nYV2Tf.htm](journals-pages/40koksC576nYV2Tf.htm)|Vanara|Vanara|libre|
|[4ZPZqbe0Ai8ZIfcp.htm](journals-pages/4ZPZqbe0Ai8ZIfcp.htm)|Golden League Xun|Xun de la Ligue dorée|libre|
|[5MjSsuKOLBoiL8FB.htm](journals-pages/5MjSsuKOLBoiL8FB.htm)|Freedom Domain|Domaine de la Liberté|libre|
|[5nd8ON2kFGOsCdwQ.htm](journals-pages/5nd8ON2kFGOsCdwQ.htm)|Bullet Dancer|Danseur de balle|libre|
|[5TqEbLR9QT3gJGe3.htm](journals-pages/5TqEbLR9QT3gJGe3.htm)|Sorrow Domain|Domaine du Chagrin|libre|
|[6bDpXy7pQdGrd2og.htm](journals-pages/6bDpXy7pQdGrd2og.htm)|Star Domain|Domaine des Étoiles|libre|
|[6KciIDJV6ZdJcAVa.htm](journals-pages/6KciIDJV6ZdJcAVa.htm)|Sterling Dynamo|Dynamo sterling|libre|
|[6qTjtFWaBO5b60zJ.htm](journals-pages/6qTjtFWaBO5b60zJ.htm)|Dust Domain|Domaine de la Poussière|libre|
|[6xnJaS3qRZGdDHtr.htm](journals-pages/6xnJaS3qRZGdDHtr.htm)|Roll Back|Roulade arrière|libre|
|[798PFdS8FmefcOl0.htm](journals-pages/798PFdS8FmefcOl0.htm)|Death Domain|Domaine de la Mort|libre|
|[7FsXXkrOUXHtROnq.htm](journals-pages/7FsXXkrOUXHtROnq.htm)|Falling|Chuter|libre|
|[7jQ2x83KUFD8Wj6w.htm](journals-pages/7jQ2x83KUFD8Wj6w.htm)|Light Effects|Effets de lumière|libre|
|[7pU8yM7yPMw92SY3.htm](journals-pages/7pU8yM7yPMw92SY3.htm)|Human|Humain|libre|
|[7xrNAgAnBqBgE3yM.htm](journals-pages/7xrNAgAnBqBgE3yM.htm)|Change Domain|Domaine du Changement|libre|
|[8ElntNAGahQka70r.htm](journals-pages/8ElntNAGahQka70r.htm)|Druid|Druide|libre|
|[8fgkR236jbcX08qz.htm](journals-pages/8fgkR236jbcX08qz.htm)|Feats|Dons|libre|
|[8gcp880pEWZ9VPnF.htm](journals-pages/8gcp880pEWZ9VPnF.htm)|Summon Trait|Trait convocation|libre|
|[8NHTJ8kK9qCzOVxp.htm](journals-pages/8NHTJ8kK9qCzOVxp.htm)|Battle Cry|Cri de guerre|libre|
|[8RYKz1WDPMJBmMNt.htm](journals-pages/8RYKz1WDPMJBmMNt.htm)|Game Hunter|Chasseur de gros gibier|libre|
|[8w220Bvj6W2Ui8Ae.htm](journals-pages/8w220Bvj6W2Ui8Ae.htm)|Surge of Speed|Poussée de vitesse|libre|
|[9g1dNytABTpmmGkG.htm](journals-pages/9g1dNytABTpmmGkG.htm)|Glyph Domain|Domaine des Glyphes|libre|
|[9hfEG4kJmEbrjGS1.htm](journals-pages/9hfEG4kJmEbrjGS1.htm)|Encounter Building & XP Awards|Bâtir une rencontre & Récompenses en PX|libre|
|[a3gPiGqSiWCZ6kGl.htm](journals-pages/a3gPiGqSiWCZ6kGl.htm)|Worldbuilding|Construction d'univers|libre|
|[a68Phr1qsqVRPMVp.htm](journals-pages/a68Phr1qsqVRPMVp.htm)|Class Might|Puissance de classe|libre|
|[A7vErdGAweYsFcW8.htm](journals-pages/A7vErdGAweYsFcW8.htm)|Healing Domain|Domaine de la Guérison|libre|
|[aBALUjLyOqXKlhrP.htm](journals-pages/aBALUjLyOqXKlhrP.htm)|Earn Income|Gagner de l'argent|libre|
|[act12TfHpnKx6ZTu.htm](journals-pages/act12TfHpnKx6ZTu.htm)|Different Size Items|Objets de différente taille|libre|
|[AeGcoQNaZ1BUmWvu.htm](journals-pages/AeGcoQNaZ1BUmWvu.htm)|Last Second Sidestep|Pas de côté au dernier moment|libre|
|[ajCEExOaxuB4C1tY.htm](journals-pages/ajCEExOaxuB4C1tY.htm)|Passion Domain|Domaine de la Passion|libre|
|[AOQZjqgfafqqtHOB.htm](journals-pages/AOQZjqgfafqqtHOB.htm)|Destruction Domain|Domaine de la Destruction|libre|
|[ARYrFIOsT3JxpjDY.htm](journals-pages/ARYrFIOsT3JxpjDY.htm)|Hellknight Armiger|Écuyer des chevaliers infernaux|libre|
|[bAIAWQ7q3ycIAnoj.htm](journals-pages/bAIAWQ7q3ycIAnoj.htm)|Relationships|Relations|libre|
|[bApp2BZEMuYQCTDM.htm](journals-pages/bApp2BZEMuYQCTDM.htm)|Scout|Éclaireur|libre|
|[bkTCYlTFNifrM3sh.htm](journals-pages/bkTCYlTFNifrM3sh.htm)|Artillerist|Artilleur|libre|
|[BsaTOw6p74DW5d8t.htm](journals-pages/BsaTOw6p74DW5d8t.htm)|Wand Prices|Prix des baguettes|libre|
|[bTujFcUut9RX4GCy.htm](journals-pages/bTujFcUut9RX4GCy.htm)|Travel Domain|Domaine du Voyage|libre|
|[BwPUY2X7TSmlJj5c.htm](journals-pages/BwPUY2X7TSmlJj5c.htm)|Duelist|Duelliste|libre|
|[cAxBEZsej32riaY5.htm](journals-pages/cAxBEZsej32riaY5.htm)|Decay Domain|Domaine de la Décomposition|libre|
|[cBBwTnJrVmvaRMYq.htm](journals-pages/cBBwTnJrVmvaRMYq.htm)|Aldori Duelist|Duelliste Aldori|libre|
|[CbsAiY68e8n5vVVN.htm](journals-pages/CbsAiY68e8n5vVVN.htm)|Repose Domain|Domaine de la Quiétude|libre|
|[CM9ZqWwl7myKn2X1.htm](journals-pages/CM9ZqWwl7myKn2X1.htm)|Darkness Domain|Domaine des Ténèbres|libre|
|[COTDddn4psjIoWry.htm](journals-pages/COTDddn4psjIoWry.htm)|Lion Blade|Lame du lion|libre|
|[cQH9Syl1SQpbAi5A.htm](journals-pages/cQH9Syl1SQpbAi5A.htm)|Rage and Fury|rage et furie|libre|
|[d15Zjdqplmj7ZTKK.htm](journals-pages/d15Zjdqplmj7ZTKK.htm)|Desperate Swing|Balancement désespéré|libre|
|[D6hqfmZUksRvqNFR.htm](journals-pages/D6hqfmZUksRvqNFR.htm)|Fluid Motion|Mouvement fluide|libre|
|[dCd0YXLVx0DQwitm.htm](journals-pages/dCd0YXLVx0DQwitm.htm)|Tuck and Roll|Se plier et rouler|libre|
|[DI3MYGIK8iEycanU.htm](journals-pages/DI3MYGIK8iEycanU.htm)|Zeal Domain|Domaine du Zèle|libre|
|[dpL9RyApn4y7jJ3y.htm](journals-pages/dpL9RyApn4y7jJ3y.htm)|Homeland|Patrie|libre|
|[drgCQcXZbIJU0Zhw.htm](journals-pages/drgCQcXZbIJU0Zhw.htm)|Wrestler|Lutteur|libre|
|[DS95vr2zmTsjsMhU.htm](journals-pages/DS95vr2zmTsjsMhU.htm)|Magic Domain|Domaine de la Magie|libre|
|[Dx47K8wpx8KZUa9S.htm](journals-pages/Dx47K8wpx8KZUa9S.htm)|Protection Domain|Domaine de la Protection|libre|
|[E4aI0BTV9ATxfjGd.htm](journals-pages/E4aI0BTV9ATxfjGd.htm)|Reckless Charge|Charge téméraire|libre|
|[EC2eB0JglDG5j1gT.htm](journals-pages/EC2eB0JglDG5j1gT.htm)|Fate Domain|Domaine du Destin|libre|
|[EGa7oNFMSLkvkQjL.htm](journals-pages/EGa7oNFMSLkvkQjL.htm)|Flash of Insight|Éclair de lucidité|libre|
|[egSErNozlL3HRK1y.htm](journals-pages/egSErNozlL3HRK1y.htm)|Fire Domain|Domaine du Feu|libre|
|[EQfZepZX6rxxBRqG.htm](journals-pages/EQfZepZX6rxxBRqG.htm)|Toil Domain|Domaine du Labeur|libre|
|[EwlYs1OzaMj9BB5I.htm](journals-pages/EwlYs1OzaMj9BB5I.htm)|Student of Perfection|Étudiant de la perfection|libre|
|[eZUSi5q5NnEvCDEk.htm](journals-pages/eZUSi5q5NnEvCDEk.htm)|Cut Through the Fog|Traverser le brouillard|libre|
|[EZZxz9jeEB0N3FPZ.htm](journals-pages/EZZxz9jeEB0N3FPZ.htm)|Spark of Courage|Étincelle de courage|libre|
|[f2wr86966A0oUUA0.htm](journals-pages/f2wr86966A0oUUA0.htm)|Adjusting Treasure|Ajuster les trésors|libre|
|[f7tFLR7aFonXTLQa.htm](journals-pages/f7tFLR7aFonXTLQa.htm)|Undead Master|Maître des morts-vivants|libre|
|[Fea8ZereQhNolDoP.htm](journals-pages/Fea8ZereQhNolDoP.htm)|Bounty Hunter|Chasseur de primes|libre|
|[FgA086C40VR9mzNr.htm](journals-pages/FgA086C40VR9mzNr.htm)|Hold the Line|Tenez la ligne|libre|
|[flmxRzGxN2rRNyxZ.htm](journals-pages/flmxRzGxN2rRNyxZ.htm)|Confidence Domain|Domaine de la Confiance en soi|libre|
|[fPqSekmEm5byReOk.htm](journals-pages/fPqSekmEm5byReOk.htm)|Talisman Dabbler|Amateur de talismans|libre|
|[Fq1KEUyv0zBp5nMV.htm](journals-pages/Fq1KEUyv0zBp5nMV.htm)|Last Ounce of Strength|Dernière once de force|libre|
|[ftdrUGjW8A4TPkMa.htm](journals-pages/ftdrUGjW8A4TPkMa.htm)|Protect the Innocent|Protéger l'innocent|libre|
|[FtW1gtbHgO0KofPl.htm](journals-pages/FtW1gtbHgO0KofPl.htm)|Pain Domain|Domaine de la Souffrance|libre|
|[G1Rje9eYCFEByzjg.htm](journals-pages/G1Rje9eYCFEByzjg.htm)|Special Battles|Batailles spéciales (LdB)|libre|
|[G38usCLuTDmFYw7V.htm](journals-pages/G38usCLuTDmFYw7V.htm)|Zombie|Zombie|libre|
|[g5G4unm9tbEDu3pZ.htm](journals-pages/g5G4unm9tbEDu3pZ.htm)|Hasty Block|Blocage précipité|libre|
|[G9Fzy5ZK4KtAmcFb.htm](journals-pages/G9Fzy5ZK4KtAmcFb.htm)|Snarecrafter|Fabricant de pièges artisanaux|libre|
|[GCxhDyf1ZNlLiDKZ.htm](journals-pages/GCxhDyf1ZNlLiDKZ.htm)|Orc|Orc|libre|
|[gdEmPPRYhRai7I1N.htm](journals-pages/gdEmPPRYhRai7I1N.htm)|Ranger|Rôdeur|libre|
|[gE3x4USaU4VWdwKD.htm](journals-pages/gE3x4USaU4VWdwKD.htm)|Gamemastery|Maîtrise du jeu|libre|
|[gfhnRp2TEy9JCfHI.htm](journals-pages/gfhnRp2TEy9JCfHI.htm)|Gladiator|Gladiateur|libre|
|[GiuzDTtkQAgtGW6n.htm](journals-pages/GiuzDTtkQAgtGW6n.htm)|Indulgence Domain|Domaine des Petits plaisirs|libre|
|[gmwyfTlk0BGZLdCt.htm](journals-pages/gmwyfTlk0BGZLdCt.htm)|Magical Reverberation|Réverbération magique|libre|
|[GqZLaGQDNe41saZ7.htm](journals-pages/GqZLaGQDNe41saZ7.htm)|Misdirected Attack|Attaque mal dirigée|libre|
|[Gu6CHAPMigpV9awj.htm](journals-pages/Gu6CHAPMigpV9awj.htm)|Tumble Through|Déplacement acrobatique|libre|
|[h1zdUIWX1k5PKTnc.htm](journals-pages/h1zdUIWX1k5PKTnc.htm)|Celebrity|Célébrité|libre|
|[HDTW0vr8QL20M5ND.htm](journals-pages/HDTW0vr8QL20M5ND.htm)|Weapon Improviser|Improvisateur d'armes|libre|
|[hGoWOjdsUz16oJUm.htm](journals-pages/hGoWOjdsUz16oJUm.htm)|Plague Domain|Domaine du Fléau|libre|
|[hH8gPjc3GxFzgLHR.htm](journals-pages/hH8gPjc3GxFzgLHR.htm)|Variant Rules|Variantes de règles|libre|
|[HtFFpq0n6tjZqLOO.htm](journals-pages/HtFFpq0n6tjZqLOO.htm)|Rules and Languages|Règles et langues|libre|
|[HvbDEgCsLbzuMRiR.htm](journals-pages/HvbDEgCsLbzuMRiR.htm)|Poisoner|Empoisonneur|libre|
|[HZbrRRVzW7w17L2W.htm](journals-pages/HZbrRRVzW7w17L2W.htm)|Fighter|Guerrier|libre|
|[I6lHRH3mWjfvhJrR.htm](journals-pages/I6lHRH3mWjfvhJrR.htm)|Resting|Repos|libre|
|[IJOH7nk0w1IybAhp.htm](journals-pages/IJOH7nk0w1IybAhp.htm)|Kashrishi|Kashrishi|libre|
|[IQ56rAleMRc53Lcv.htm](journals-pages/IQ56rAleMRc53Lcv.htm)|Major Childhood Event|Évènement marquant votre enfance|libre|
|[jFhTp57zO3ej6HDt.htm](journals-pages/jFhTp57zO3ej6HDt.htm)|Corpse Tender|Éleveur de cadavres|libre|
|[jHn1m1r95YgMQSvM.htm](journals-pages/jHn1m1r95YgMQSvM.htm)|Daring Attempt|Tentative audacieuse|libre|
|[jIs5mtRaqG0aGQ8u.htm](journals-pages/jIs5mtRaqG0aGQ8u.htm)|Make Way!|Faites place !|libre|
|[jMhD1Z6aagYAYCnb.htm](journals-pages/jMhD1Z6aagYAYCnb.htm)|Last Stand|Dernier combat|libre|
|[jq9O1tl76g2AzLOh.htm](journals-pages/jq9O1tl76g2AzLOh.htm)|Cold Domain|Domaine du Froid|libre|
|[k0ue81dFKH4OKxed.htm](journals-pages/k0ue81dFKH4OKxed.htm)|Impossible Shot|Tir impossible|libre|
|[Kca7UPuMm44tOo9n.htm](journals-pages/Kca7UPuMm44tOo9n.htm)|Lightning Domain|Domaine de la Foudre|libre|
|[KORSADviZaSccs2W.htm](journals-pages/KORSADviZaSccs2W.htm)|Zephyr Guard|Garde zéphyr|libre|
|[kRjkrWAqQNXj3LLP.htm](journals-pages/kRjkrWAqQNXj3LLP.htm)|Ghoran|Ghoran|libre|
|[ksg8D5ssP6WGwGqi.htm](journals-pages/ksg8D5ssP6WGwGqi.htm)|Opportune Distraction|Distraction opportune|libre|
|[L11XsA5G89xVKlDw.htm](journals-pages/L11XsA5G89xVKlDw.htm)|Luck Domain|Domaine de la Chance|libre|
|[LafJ1PXfnR4Ogyzh.htm](journals-pages/LafJ1PXfnR4Ogyzh.htm)|Catch your Breath|reprends ton souffle|libre|
|[LaNM3BfoZUG2B39v.htm](journals-pages/LaNM3BfoZUG2B39v.htm)|Warding Sign|Signe de protection|libre|
|[lc3iBCamQ8jvr9dp.htm](journals-pages/lc3iBCamQ8jvr9dp.htm)|Trapsmith|Fabricant de pièges|libre|
|[lgsJz7mZ1OTe340e.htm](journals-pages/lgsJz7mZ1OTe340e.htm)|Truth Domain|Domaine de la Vérité|libre|
|[lJe2uRM1wNJw4Mgr.htm](journals-pages/lJe2uRM1wNJw4Mgr.htm)|Push Through the Pain|Surmonter la douleur|libre|
|[lLdAvaOo1rLLg5b4.htm](journals-pages/lLdAvaOo1rLLg5b4.htm)|Pierce Resistance|Perce résistance|libre|
|[mJBp4KIszuqrmnp5.htm](journals-pages/mJBp4KIszuqrmnp5.htm)|Wealth Domain|Domaine de la Richesse|libre|
|[Mk7ECwe1a971WyWl.htm](journals-pages/Mk7ECwe1a971WyWl.htm)|Demolitionist|Démolisseur|libre|
|[MOVMHZU1SfkhNN1K.htm](journals-pages/MOVMHZU1SfkhNN1K.htm)|Might Domain|Domaine de la Puissance|libre|
|[MSdKfjoNBj7PjEoo.htm](journals-pages/MSdKfjoNBj7PjEoo.htm)|Grazing Blow|Coup égratignant|libre|
|[mtc5frNxUHORGscz.htm](journals-pages/mtc5frNxUHORGscz.htm)|Linguist|Linguiste|libre|
|[N4ABcd6CcCbqmw3x.htm](journals-pages/N4ABcd6CcCbqmw3x.htm)|Cleric|Prêtre|libre|
|[NaG4fy33coUdSFtH.htm](journals-pages/NaG4fy33coUdSFtH.htm)|Jalmeri Heavenseeker|Chercheur de paradis du Jalmeray|libre|
|[nPcma8UOqWo7xw0P.htm](journals-pages/nPcma8UOqWo7xw0P.htm)|Trick Driver|Conducteur astucieux|libre|
|[nuywscaiVGXLQpZ1.htm](journals-pages/nuywscaiVGXLQpZ1.htm)|Wyrmkin Domain|Domaine de la Parenté dracosire|libre|
|[nVfhX1aisz6jY8qf.htm](journals-pages/nVfhX1aisz6jY8qf.htm)|Unexpected Sharpshooter|Tireur d'élite inattendu|libre|
|[O2hME2ilgN9BcEA4.htm](journals-pages/O2hME2ilgN9BcEA4.htm)|Dive Out of Danger|Se mettre hors de danger|libre|
|[oa0wVDfT3dbWwNDf.htm](journals-pages/oa0wVDfT3dbWwNDf.htm)|Surge of Magic|Afflux de magie|libre|
|[oAfVg9t7GGTW7R1H.htm](journals-pages/oAfVg9t7GGTW7R1H.htm)|Challenging Relationships|Relations compliquées|libre|
|[odrMigI38k7lgurE.htm](journals-pages/odrMigI38k7lgurE.htm)|Channel Life Force|Canalisation de force vitale|libre|
|[ok2IPQGIrxj8h1vs.htm](journals-pages/ok2IPQGIrxj8h1vs.htm)|Three-Dimensional Combat|Combat en trois dimensions|libre|
|[OnB94Y7wYE9j0ict.htm](journals-pages/OnB94Y7wYE9j0ict.htm)|I Hope This Works|J'espère que çà fonctionne|libre|
|[ox9mFGY50NEzRR12.htm](journals-pages/ox9mFGY50NEzRR12.htm)|Reverse Strike|Frappe à revers|libre|
|[P3mSUWRvCCwEouB0.htm](journals-pages/P3mSUWRvCCwEouB0.htm)|Critical Moment|Instant critique|libre|
|[PFBBmN58KtjXo79k.htm](journals-pages/PFBBmN58KtjXo79k.htm)|Basic Rules|Règles basiques|libre|
|[Ph7yWRR376aYh12T.htm](journals-pages/Ph7yWRR376aYh12T.htm)|Ritualist|Ritualiste|libre|
|[pS87Zh4QJnu0p8ES.htm](journals-pages/pS87Zh4QJnu0p8ES.htm)|Strike True|Frappe ultime|libre|
|[qjnUXickBOBDBu2N.htm](journals-pages/qjnUXickBOBDBu2N.htm)|Introspection Domain|Domaine de l'introspection|libre|
|[qlQSYjCwnoCzfto2.htm](journals-pages/qlQSYjCwnoCzfto2.htm)|Golem Grafter|Greffeur de golem|libre|
|[qMS6QepvY7UQQjcr.htm](journals-pages/qMS6QepvY7UQQjcr.htm)|Abomination Domain|Domaine de l'abomination|libre|
|[QUhXVsUDujUTYz7F.htm](journals-pages/QUhXVsUDujUTYz7F.htm)|Stay in the Fight|Continuer le combat|libre|
|[QzsUe3Rt3SifTQvb.htm](journals-pages/QzsUe3Rt3SifTQvb.htm)|Naga Domain|Domaine Naga|libre|
|[R20JXF43vU5RQyUj.htm](journals-pages/R20JXF43vU5RQyUj.htm)|Nightmares Domain|Domaine des Cauchemars|libre|
|[rd0jQwvTK4jpv95o.htm](journals-pages/rd0jQwvTK4jpv95o.htm)|Swarm Domain|Domaine des Nuées|libre|
|[RIlgBuWGfHC1rzYu.htm](journals-pages/RIlgBuWGfHC1rzYu.htm)|Undeath Domain|Domaine de la Non-mort|libre|
|[rlKxNuq1obXe3m5J.htm](journals-pages/rlKxNuq1obXe3m5J.htm)|Scrounger|Bricoleur|libre|
|[Rrjz5tMJtyVEQnh8.htm](journals-pages/Rrjz5tMJtyVEQnh8.htm)|Herbalist|Herboriste|libre|
|[rtobUemb6vF2Yu3Y.htm](journals-pages/rtobUemb6vF2Yu3Y.htm)|Soul Domain|Domaine de l'Âme|libre|
|[RxDsPgPCCxEdjcVQ.htm](journals-pages/RxDsPgPCCxEdjcVQ.htm)|Hellknight Signifer|Signifer|libre|
|[S1gyomjojgtCdxc3.htm](journals-pages/S1gyomjojgtCdxc3.htm)|Secrecy Domain|Domaine de la Confidentialité|libre|
|[S4BZW9c5n6CctDxl.htm](journals-pages/S4BZW9c5n6CctDxl.htm)|Firework Technician|Pyrotechnicien|libre|
|[spzRl5CS4vnvcrm5.htm](journals-pages/spzRl5CS4vnvcrm5.htm)|Called Foe|Ennemi désigné|libre|
|[StXN6IHR6evRaeXF.htm](journals-pages/StXN6IHR6evRaeXF.htm)|Vigil Domain|Domaine de la Veille|libre|
|[T0JHj79aGphlZ4Mt.htm](journals-pages/T0JHj79aGphlZ4Mt.htm)|Tyranny Domain|Domaine de la Tyrannie|libre|
|[T2y0vuYibZCL7CH0.htm](journals-pages/T2y0vuYibZCL7CH0.htm)|Air Domain|Domaine de l'Air|libre|
|[tJQ5f8C8m5gpZsF1.htm](journals-pages/tJQ5f8C8m5gpZsF1.htm)|Oozemorph|Vasemorphe|libre|
|[TlTIwW61zK0dk95v.htm](journals-pages/TlTIwW61zK0dk95v.htm)|Nagaji|Nagaji|libre|
|[TnD2hTWTyjGKlw9b.htm](journals-pages/TnD2hTWTyjGKlw9b.htm)|Sentinel|Sentinelle|libre|
|[Ttn2H0JHJVksChGi.htm](journals-pages/Ttn2H0JHJVksChGi.htm)|Healing Prayer|Prière de guérison|libre|
|[TU21DvdpQG7U2Q9Y.htm](journals-pages/TU21DvdpQG7U2Q9Y.htm)|Endure the Onslaught|Endurer l'assaut|libre|
|[tuThzOCvMLbRVba8.htm](journals-pages/tuThzOCvMLbRVba8.htm)|Delirium Domain|Domaine du Délirium|libre|
|[tUu3x2iHtOUqqWUc.htm](journals-pages/tUu3x2iHtOUqqWUc.htm)|Distract Foe|Distraire l'ennemi|libre|
|[Tz0NWVqhZyt8EyUV.htm](journals-pages/Tz0NWVqhZyt8EyUV.htm)|Bard|Barde|libre|
|[u4qPrDRBagxG9wj8.htm](journals-pages/u4qPrDRBagxG9wj8.htm)|Rogue|Roublard|libre|
|[U8WVR6EDfmUaMCbu.htm](journals-pages/U8WVR6EDfmUaMCbu.htm)|Water Domain|Domaine de l'Eau|libre|
|[uGQKjk2w4whzomky.htm](journals-pages/uGQKjk2w4whzomky.htm)|Duty Domain|Domaine du Devoir|libre|
|[VSZGUtcr5NGNPtlB.htm](journals-pages/VSZGUtcr5NGNPtlB.htm)|Rampage|Carnage|libre|
|[wBhgIgt47v9uspp3.htm](journals-pages/wBhgIgt47v9uspp3.htm)|Nature Domain|Domaine de la Nature|libre|
|[wDukeO3euLEGn6FA.htm](journals-pages/wDukeO3euLEGn6FA.htm)|Wizard|Magicien|libre|
|[xgk1Qc6nzGNJ7LSz.htm](journals-pages/xgk1Qc6nzGNJ7LSz.htm)|Chronoskimmer|Écumeur du temps|libre|
|[xJtbGqoz3BcCjUik.htm](journals-pages/xJtbGqoz3BcCjUik.htm)|Trickery Domain|Domaine de la Tromperie|libre|
|[xLxrtbsj4acqgsyC.htm](journals-pages/xLxrtbsj4acqgsyC.htm)|Void Domain|Domaine du Vide|libre|
|[xsdJIpd9NRLpRZT2.htm](journals-pages/xsdJIpd9NRLpRZT2.htm)|Drain Power|Drainer la puissance|libre|
|[XWkyCVISmVtJ0ZY3.htm](journals-pages/XWkyCVISmVtJ0ZY3.htm)|Medic|Médecin|libre|
|[Y20GMjqVbTvGZ87k.htm](journals-pages/Y20GMjqVbTvGZ87k.htm)|Shake it Off|Reprends-toi|libre|
|[yWtwNUkGyj79Q04W.htm](journals-pages/yWtwNUkGyj79Q04W.htm)|Hellknight|Chevalier infernal|libre|
|[zcBCxlvPCpmjt5IS.htm](journals-pages/zcBCxlvPCpmjt5IS.htm)|Press On|Poursuivre|libre|
|[zj9z2BwVX6TLHdx3.htm](journals-pages/zj9z2BwVX6TLHdx3.htm)|Wellspring Mage (Class Archetype)|Mage de la source (Archétype de classe)|libre|
|[zkiLWWYzzqoxmN2J.htm](journals-pages/zkiLWWYzzqoxmN2J.htm)|Earth Domain|Domaine de la Terre|libre|
|[zX28AGU1k5sO2Tpd.htm](journals-pages/zX28AGU1k5sO2Tpd.htm)|Run and Shoot|Courir et tirer|libre|
