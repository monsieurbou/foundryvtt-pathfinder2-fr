# État de la traduction (other-effects)

 * **libre**: 34


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0r8MznJe5UugViee.htm](other-effects/0r8MznJe5UugViee.htm)|Effect: -15-foot status penalty to your land Speed|Effet : Pénalité de -4,5 m à votre Vitesse au sol|libre|
|[5gX3PZztMecmWxX9.htm](other-effects/5gX3PZztMecmWxX9.htm)|Effect: -2 circumstance penalty to attack rolls|Effet : pénalité de circonstances de -2 aux jets d'attaque|libre|
|[9c93NfZpENofiGUp.htm](other-effects/9c93NfZpENofiGUp.htm)|Effect: Mounted|Effet : Monté|libre|
|[AHMUpMbaVkZ5A1KX.htm](other-effects/AHMUpMbaVkZ5A1KX.htm)|Effect: Aid|Effet : Aider|libre|
|[BjkhK1nFUw3vNKg2.htm](other-effects/BjkhK1nFUw3vNKg2.htm)|Effect: -2 circumstance penalty to spell attack rolls and spell DC's|Effet : pénalité de circonstances de - 2 aux jets d'attaques des sorts et aux DD des sorts|libre|
|[c6cwOc4Zri6QiqsR.htm](other-effects/c6cwOc4Zri6QiqsR.htm)|Effect: -2 circumstance penalty to checks and saving throws until healed|Effet : pénalité de circonstances de -2 aux tests et aux jets de sauvegarde jusqu'aux soins|libre|
|[Ck0MQP9og75AWix7.htm](other-effects/Ck0MQP9og75AWix7.htm)|Effect: -2 circumstance penalty to attack rolls until healed|Effet : pénalité de circonstances de -2 aux jets d'attaque jusqu'aux soins|libre|
|[EMqGwUi3VMhCjTlF.htm](other-effects/EMqGwUi3VMhCjTlF.htm)|Effect: Scouting|Effet : Éclaireur|libre|
|[fqk0ESqJACXqz21k.htm](other-effects/fqk0ESqJACXqz21k.htm)|Effect: -1 circumstance penalty to attack rolls until you score a critical hit|Effet : pénalité de circonstances de -1 aux jets d'attaque jusqu'à obtention d'un coup critique|libre|
|[HLBhINtyRcHk6d7h.htm](other-effects/HLBhINtyRcHk6d7h.htm)|Effect: +2 circumstance bonus to attack rolls|Effet : bonus de circonstances de +2 aux jets d'attaque|libre|
|[HTwG0F0LDu5ihBUm.htm](other-effects/HTwG0F0LDu5ihBUm.htm)|Effect: -5-foot status penalty to your land Speed|Effet : pénalité de statut de -1,5 m à votre Vitesse au sol|libre|
|[i5WccD9u62QPRZI8.htm](other-effects/i5WccD9u62QPRZI8.htm)|Effect: -2 circumstance penalty to attack rolls made with this attack until healed|Effet : pénalité de circonstances de -2 aux jets d'attaque avec cette attaque jusqu'aux soins|libre|
|[I9lfZUiCwMiGogVi.htm](other-effects/I9lfZUiCwMiGogVi.htm)|Effect: Cover|Effet : Abri|libre|
|[IUvO6CatanKAgmNr.htm](other-effects/IUvO6CatanKAgmNr.htm)|Effect: +1 circumstance bonus to attack rolls for 3 rounds|Effet : bonus de circonstances de +1 aux jets d'attaque pendant 3 rounds|libre|
|[la8rWwUtReElgTS6.htm](other-effects/la8rWwUtReElgTS6.htm)|Effect: Scouting (Incredible Scout)|Effet : Éclaireur incroyable|libre|
|[lTwF7aCtQ6napqYu.htm](other-effects/lTwF7aCtQ6napqYu.htm)|Effect: +2 status bonus to attack rolls|Effet : bonus de statut de +2 aux jets d'attaque|libre|
|[NU5wAbJLZO6T86eB.htm](other-effects/NU5wAbJLZO6T86eB.htm)|Effect: -10-foot circumstance penalty to your land Speed|Effet - pénalité de circonstances de -3m à votre Vitesse|libre|
|[PaS2kwv4vueI4PC7.htm](other-effects/PaS2kwv4vueI4PC7.htm)|Effect: +2 status bonus to saving throws against spells for 1 min|Effet : bonus de statut de +2 aux jets de sauvegarde contre les sorts|libre|
|[pbKvPM4cnmGTkDiZ.htm](other-effects/pbKvPM4cnmGTkDiZ.htm)|Effect: +4 circumstance bonus to AC against your ranged attacks|Effet : bonus de circonstances de +4 à la CA contre vos attaques à distance|libre|
|[QGTMOcJ1Dq5bk0GG.htm](other-effects/QGTMOcJ1Dq5bk0GG.htm)|Effect: +2 circumstance bonus to AC|Effet : bonus de circonstances de +2 à la CA|libre|
|[RcMZr0wSjrir1AnS.htm](other-effects/RcMZr0wSjrir1AnS.htm)|Effect: -5-foot circumstance penalty to your land Speed|Effet : pénalité de circonstances de -1,5 m à votre Vitesse|libre|
|[ScZGg91ri19H4IIc.htm](other-effects/ScZGg91ri19H4IIc.htm)|Effect: -10-foot circumstance penalty to all Speeds|Effet : pénalité de circonstances de -3 m à toutes les Vitesses|libre|
|[sq8lhVfn0pl5WJ4t.htm](other-effects/sq8lhVfn0pl5WJ4t.htm)|Effect: +2 status bonus to AC and all saving throws|Effet : bonus de statut de +2 à la CA et à tous les jets de sauvegarde|libre|
|[TPbr1kErAAJKBi3V.htm](other-effects/TPbr1kErAAJKBi3V.htm)|Effect: Aquatic Combat|Effet : Combat aquatique|libre|
|[uwYGEeqvt8pwjhXa.htm](other-effects/uwYGEeqvt8pwjhXa.htm)|Effect: Dazzled until end of your next turn|Effet : Ébloui jusqu'aà la fin de votre prochain tour|libre|
|[UZabiyynJWuUblMk.htm](other-effects/UZabiyynJWuUblMk.htm)|Effect: -2 circumstance penalty to attack rolls with this weapon|Effet : pénalité de circonstances de -2 aux jets d'attaque avec cette arme|libre|
|[VCSpuc3Tf3XWMkd3.htm](other-effects/VCSpuc3Tf3XWMkd3.htm)|Effect: Follow The Expert|Effet : Suivre l'expert|libre|
|[W2OF7VeLHqc7p3DO.htm](other-effects/W2OF7VeLHqc7p3DO.htm)|Effect: Deafened until end of your next turn|Effet : Sourd jusqu'à la fin de votre prochain tour|libre|
|[wHWWHkjDXmJl4Ia6.htm](other-effects/wHWWHkjDXmJl4Ia6.htm)|Effect: Adverse Subsist Situation|Effet : Situation de|libre|
|[WqgtMWgM9nUjwQiI.htm](other-effects/WqgtMWgM9nUjwQiI.htm)|Effect: Resistance 5 to all damage|Effet : Résistance 5 à tous les dégâts|libre|
|[wVGSlZEGxsg1s8AZ.htm](other-effects/wVGSlZEGxsg1s8AZ.htm)|Effect: -2 circumstance penalty to ranged attacks|Effet : pénalité de circonstances de -2 aux attaques à distance|libre|
|[y1GwyXv7iOf8DhBg.htm](other-effects/y1GwyXv7iOf8DhBg.htm)|Effect: Off-Guard until end of your next turn|Effet : Pris au dépourvu jusqu'à la fin de votre prochain tour|libre|
|[zDV1wo2ytNTbyTB0.htm](other-effects/zDV1wo2ytNTbyTB0.htm)|Effect: -10-foot status penalty to your land Speed|Effet : pénalité de statut de -3 m à votre Vitesse|libre|
|[ZXUOdZqUW22OX3ge.htm](other-effects/ZXUOdZqUW22OX3ge.htm)|Effect: Avert Gaze|Effet : Détourner le regard|libre|
