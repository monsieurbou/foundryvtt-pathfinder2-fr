# État de la traduction (journals)

 * **libre**: 7
 * **officielle**: 1


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[45SK8rdbbxvEHfMn.htm](journals/45SK8rdbbxvEHfMn.htm)|Ancestries|Ascendances|libre|
|[6L2eweJuM8W7OCf2.htm](journals/6L2eweJuM8W7OCf2.htm)|Remaster Changes|Changements du remaster|libre|
|[BSp4LUSaOmUyjBko.htm](journals/BSp4LUSaOmUyjBko.htm)|Hero Point Deck|Cartes de points d'héroïsme|libre|
|[EEZvDB1Z7ezwaxIr.htm](journals/EEZvDB1Z7ezwaxIr.htm)|Domains|Domaines|libre|
|[kzxu2dI7tFxv6Ix6.htm](journals/kzxu2dI7tFxv6Ix6.htm)|Classes|Classes|libre|
|[S55aqwWIzpQRFhcq.htm](journals/S55aqwWIzpQRFhcq.htm)|GM Screen|Écran du MJ|libre|
|[vx5FGEG34AxI2dow.htm](journals/vx5FGEG34AxI2dow.htm)|Archetypes|Archétypes|officielle|
|[xtrW5GEtPPuXR6k2.htm](journals/xtrW5GEtPPuXR6k2.htm)|Deep Backgrounds|Backgrounds/Historiques approfondis|libre|
