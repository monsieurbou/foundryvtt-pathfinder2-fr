# État de la traduction (highhelm-bestiary)

 * **changé**: 1
 * **libre**: 5


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[6X4zL4GSS9aUcP9f.htm](highhelm-bestiary/6X4zL4GSS9aUcP9f.htm)|Grand Defender|Grand défenseur|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[AbaAJaz8p6sSsaqy.htm](highhelm-bestiary/AbaAJaz8p6sSsaqy.htm)|Draft Lizard|Lézard de trait|libre|
|[cWHQmwUkLYBlFOGX.htm](highhelm-bestiary/cWHQmwUkLYBlFOGX.htm)|Augdunar|Augdunar|libre|
|[fQvTnIpC2Wq1U32z.htm](highhelm-bestiary/fQvTnIpC2Wq1U32z.htm)|Pagulin|Pagulin|libre|
|[IGoJDxjMiADXa1bm.htm](highhelm-bestiary/IGoJDxjMiADXa1bm.htm)|Graul|Graul|libre|
|[McZDmNR9sldfnsBw.htm](highhelm-bestiary/McZDmNR9sldfnsBw.htm)|The Bloodstorm|La tempête-sang|libre|
