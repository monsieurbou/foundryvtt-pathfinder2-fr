# État de la traduction (deities)

 * **libre**: 247
 * **changé**: 27
 * **officielle**: 1


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[14GLsVobnZTEmJrs.htm](deities/14GLsVobnZTEmJrs.htm)|Kelizandri|Kélizandre|changé|
|[22n3N47sqbqbDxbp.htm](deities/22n3N47sqbqbDxbp.htm)|Urgathoa|Urgathoa|changé|
|[88vRw2ZVPax4hhga.htm](deities/88vRw2ZVPax4hhga.htm)|Gorum|Gorum|changé|
|[9EKyWmjLUu42evxC.htm](deities/9EKyWmjLUu42evxC.htm)|Nethys|Néthys|changé|
|[aipkJQxP4GBsTaGq.htm](deities/aipkJQxP4GBsTaGq.htm)|Lamashtu|Lamashtu|changé|
|[AlL9GtlTldbqNoVm.htm](deities/AlL9GtlTldbqNoVm.htm)|Calistria|Calistria|changé|
|[Bhxji0kxIbMKqdkw.htm](deities/Bhxji0kxIbMKqdkw.htm)|Rovagug|Rovagug|changé|
|[BNycwu3I21dTh4D9.htm](deities/BNycwu3I21dTh4D9.htm)|Sarenrae|Sarenrae|changé|
|[d56paSkcwvll2OhR.htm](deities/d56paSkcwvll2OhR.htm)|Abadar|Abadar|changé|
|[eErEiVmBqltYXFJ6.htm](deities/eErEiVmBqltYXFJ6.htm)|Iomedae|Iomédae|changé|
|[EUOAV02rHz96jMPh.htm](deities/EUOAV02rHz96jMPh.htm)|Whispering Way|Voie du murmure|changé|
|[g1AVEGi8QibhS007.htm](deities/g1AVEGi8QibhS007.htm)|Walkena|Walkena|changé|
|[GX6PF9ifR47Xyjg9.htm](deities/GX6PF9ifR47Xyjg9.htm)|Norgorber|Norgorber|changé|
|[gxwmzxUWBOhdgFSN.htm](deities/gxwmzxUWBOhdgFSN.htm)|Shelyn|Shélyn|changé|
|[idAu7puhPp3kFTnl.htm](deities/idAu7puhPp3kFTnl.htm)|Lorthact's Cult|Culte de Lorthact|changé|
|[JDbrj8aKkLzdn8BU.htm](deities/JDbrj8aKkLzdn8BU.htm)|Atheism|Athéisme|changé|
|[JgqH3BhuEuA4Zyqs.htm](deities/JgqH3BhuEuA4Zyqs.htm)|Desna|Desna|changé|
|[KT7HKL7vbZEwbzX4.htm](deities/KT7HKL7vbZEwbzX4.htm)|Green Faith|La Verte religion|changé|
|[mm1j6UbHqFCI8fwB.htm](deities/mm1j6UbHqFCI8fwB.htm)|Erastil|Érastil|changé|
|[qqqrbq4uCrelQvgH.htm](deities/qqqrbq4uCrelQvgH.htm)|Irori|Irori|changé|
|[QRkcFciCOmFoxF1B.htm](deities/QRkcFciCOmFoxF1B.htm)|Asmodeus|Asmodeus|changé|
|[QZD0u1jxwz0kj8uI.htm](deities/QZD0u1jxwz0kj8uI.htm)|Pharasma|Pharasma|changé|
|[ra7mnjAZcEuJxI37.htm](deities/ra7mnjAZcEuJxI37.htm)|Shei|Sheï|changé|
|[swwwP7eVmlNuWTb7.htm](deities/swwwP7eVmlNuWTb7.htm)|Gozreh|Gozreh|changé|
|[sZrdzh0ANQnZGXJb.htm](deities/sZrdzh0ANQnZGXJb.htm)|Torag|Torag|changé|
|[v67fHklTZ6LoU54q.htm](deities/v67fHklTZ6LoU54q.htm)|Cayden Cailean|Cayden Cailéan|changé|
|[ZfN7jkK6boU1FuiS.htm](deities/ZfN7jkK6boU1FuiS.htm)|Zon-Kuthon|Zon-Kuthon|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0BH54ZVR8NcrGlMu.htm](deities/0BH54ZVR8NcrGlMu.htm)|Findeladlara|Findeladlara|libre|
|[0nQItOlce5ua6Eaf.htm](deities/0nQItOlce5ua6Eaf.htm)|Sekhmet|Sekhmet|libre|
|[0R2BjVb7d9tYCIbk.htm](deities/0R2BjVb7d9tYCIbk.htm)|Tlehar|Tléhar|libre|
|[0xCttKr5oqjirGCe.htm](deities/0xCttKr5oqjirGCe.htm)|Aakriti|Aakriti|libre|
|[1fmTXGRPa8fLXZKL.htm](deities/1fmTXGRPa8fLXZKL.htm)|Balumbdar|Balumbdar|libre|
|[1LiO5bDQIlJmXk35.htm](deities/1LiO5bDQIlJmXk35.htm)|Lady Razor|Dame Couperet (Déchu)|libre|
|[1nQVLOyWv5kC0bCU.htm](deities/1nQVLOyWv5kC0bCU.htm)|Ravithra|Ravithra|libre|
|[1NxkZf6ndijKeufX.htm](deities/1NxkZf6ndijKeufX.htm)|Sairazul|Saïrazul|libre|
|[1xG0PDxn0rhmJhMS.htm](deities/1xG0PDxn0rhmJhMS.htm)|Yelayne|Yélayne|libre|
|[1xHnoMlCyCV70TnT.htm](deities/1xHnoMlCyCV70TnT.htm)|Saloc|Saloc|libre|
|[30uY6NkhYmPI3Oii.htm](deities/30uY6NkhYmPI3Oii.htm)|Bergelmir|Bergelmir|libre|
|[39z55kowQFOUH2v0.htm](deities/39z55kowQFOUH2v0.htm)|Droskar|Droskar|libre|
|[3ffVxwgtvR1imHm8.htm](deities/3ffVxwgtvR1imHm8.htm)|Gendowyn|Gendowyn|libre|
|[3INt1X4ijBYnZYbU.htm](deities/3INt1X4ijBYnZYbU.htm)|Shyka|Shyka|libre|
|[3lgzcg84J0fsVqjh.htm](deities/3lgzcg84J0fsVqjh.htm)|Esoteric Order of the Palatine Eye|Ordre ésotérique de l'Oeil palatin|libre|
|[3nxIpMGeCvcBIVov.htm](deities/3nxIpMGeCvcBIVov.htm)|Nyarlathotep (The Crawling Chaos)|Nyarlathotep|libre|
|[44fcXgp3fnkoyb4R.htm](deities/44fcXgp3fnkoyb4R.htm)|Rowdrosh|Rowdrosh|libre|
|[4bEPoujxhYdUyUOg.htm](deities/4bEPoujxhYdUyUOg.htm)|Ydersius|Ydersius|libre|
|[4GO4iT585ynLJnc8.htm](deities/4GO4iT585ynLJnc8.htm)|Chaldira|Chaldira|libre|
|[4I1aVKED08cwcC4H.htm](deities/4I1aVKED08cwcC4H.htm)|Laws of Mortality|Lois de la mortalité|libre|
|[51kMMWaT6CqpGlGk.htm](deities/51kMMWaT6CqpGlGk.htm)|Achaekek|Achaékek|libre|
|[5YUNikBpJZ596MRD.htm](deities/5YUNikBpJZ596MRD.htm)|Anubis|Anubis|libre|
|[5ZIvg8CkqRMepVcX.htm](deities/5ZIvg8CkqRMepVcX.htm)|Korada|Korada|libre|
|[65o189p2eUPbqaDK.htm](deities/65o189p2eUPbqaDK.htm)|Narriseminek|Narriseminek|libre|
|[6mgCX7lvKtaEYoa9.htm](deities/6mgCX7lvKtaEYoa9.htm)|Dwarven Pantheon|Panthéon nain|libre|
|[6nq25AjImkT5GxDT.htm](deities/6nq25AjImkT5GxDT.htm)|The Prismatic Ray|Le rayon prismatique|libre|
|[6O4UGWcYERDEugzk.htm](deities/6O4UGWcYERDEugzk.htm)|Fumeiyoshi|Fumeiyoshi|libre|
|[7L9KIqixgseSCY3X.htm](deities/7L9KIqixgseSCY3X.htm)|Ymeri|Yméri|libre|
|[7zyxg8TlXCDzHGMf.htm](deities/7zyxg8TlXCDzHGMf.htm)|The Last Breath|Le Dernier Souffle|libre|
|[83jKKHQPoqVylmrF.htm](deities/83jKKHQPoqVylmrF.htm)|Yaezhing|Yaezhing|libre|
|[8DXel4AvpyUy6oJ0.htm](deities/8DXel4AvpyUy6oJ0.htm)|Eritrice|Éritrice|libre|
|[8Oy4qHylOOp79JxD.htm](deities/8Oy4qHylOOp79JxD.htm)|Nhimbaloth|Nhimbaloth|libre|
|[8roijlQ03l9rWFFe.htm](deities/8roijlQ03l9rWFFe.htm)|Zevgavizeb|Zevgavizeb|libre|
|[8wOAwLMgB1HFLo9n.htm](deities/8wOAwLMgB1HFLo9n.htm)|Winlas|Winlas|libre|
|[8ZSywyo8YQPduo92.htm](deities/8ZSywyo8YQPduo92.htm)|Zura|Zura|libre|
|[97Qe42wJN5EVUS14.htm](deities/97Qe42wJN5EVUS14.htm)|Sturovenen|Sturovenen|libre|
|[9eltBjLngkR9x1Xm.htm](deities/9eltBjLngkR9x1Xm.htm)|Trelmarixian|Trelmarixian|libre|
|[9FWBJcADLEeP8Krf.htm](deities/9FWBJcADLEeP8Krf.htm)|Valmallos|Valmallos|libre|
|[9LwncUwHbG1f6qIt.htm](deities/9LwncUwHbG1f6qIt.htm)|Marishi|Marishi|libre|
|[9n2CI0B3EA4y3BuU.htm](deities/9n2CI0B3EA4y3BuU.htm)|Alseta|Alséta|libre|
|[9vcKBv8h5vugb8lL.htm](deities/9vcKBv8h5vugb8lL.htm)|Fandarra|Fandarra|libre|
|[9WD0hbUT0IbnuTiM.htm](deities/9WD0hbUT0IbnuTiM.htm)|Baalzebul|Baalzébul|libre|
|[9Z1vOSaLsEiAs2yN.htm](deities/9Z1vOSaLsEiAs2yN.htm)|Thalaphyrr Martyr-Minder|Thalaphyrr Garde-Martyrs (Déchu)|libre|
|[A37XqON6QRPzQhp7.htm](deities/A37XqON6QRPzQhp7.htm)|Hshurha|Hshurha|libre|
|[aChBtIDfA59QQ7SN.htm](deities/aChBtIDfA59QQ7SN.htm)|Ayrzul|Ayrzul|libre|
|[Af4TSJjutVi9Vdwi.htm](deities/Af4TSJjutVi9Vdwi.htm)|Mahathallah|Mahathallah|libre|
|[AHX89nhuVhmg8k6w.htm](deities/AHX89nhuVhmg8k6w.htm)|Dagon|Dagon|libre|
|[AIcDyoff265z9285.htm](deities/AIcDyoff265z9285.htm)|Kofusachi|Kofusachi|libre|
|[aievwuZalZ2abs2q.htm](deities/aievwuZalZ2abs2q.htm)|Thoth|Thoth|libre|
|[aj4ChTlDcZCmrtJD.htm](deities/aj4ChTlDcZCmrtJD.htm)|Baphomet|Baphomet|libre|
|[AJ7Wgpj5Ext9wbtj.htm](deities/AJ7Wgpj5Ext9wbtj.htm)|Keepers of the Hearth|Gardiens du foyer|libre|
|[akcmQbvNWfhEfEhf.htm](deities/akcmQbvNWfhEfEhf.htm)|Arqueros|Arqueros|libre|
|[aLCfEgt48SRUHnMC.htm](deities/aLCfEgt48SRUHnMC.htm)|Jaidi|Jaïdi|libre|
|[AMFkYKUNQ3kCD0ob.htm](deities/AMFkYKUNQ3kCD0ob.htm)|Doloras|Doloras|libre|
|[ArmPZQrl0SFAKvrY.htm](deities/ArmPZQrl0SFAKvrY.htm)|Ma'at|Maât|libre|
|[aTYy1WHwAZiuei4h.htm](deities/aTYy1WHwAZiuei4h.htm)|Tsukiyo|Tsukiyo|libre|
|[aVyxfqY9GBjTqQul.htm](deities/aVyxfqY9GBjTqQul.htm)|Eiseth|Eiseth|libre|
|[b7H379HnCOv9UZtd.htm](deities/b7H379HnCOv9UZtd.htm)|Orcus|Orcus|libre|
|[bF77ydtptI54STl9.htm](deities/bF77ydtptI54STl9.htm)|Jaidz|Jaidz|libre|
|[bLI1JMpI2GXQT5AC.htm](deities/bLI1JMpI2GXQT5AC.htm)|Abraxas|Abraxas|libre|
|[BLNXYqvzmLgjMq2p.htm](deities/BLNXYqvzmLgjMq2p.htm)|Sun Wukong|Sun Wukong|libre|
|[BrbXGyzCeORNyJT5.htm](deities/BrbXGyzCeORNyJT5.htm)|Sifkesh|Sifkesh|libre|
|[Bzfc5mIUfMxOnQWu.htm](deities/Bzfc5mIUfMxOnQWu.htm)|Dahak|Dahak|libre|
|[c9NFjRR8gr6CWpjl.htm](deities/c9NFjRR8gr6CWpjl.htm)|Black Butterfly|Papillon noir|libre|
|[CDkr6eXR1lgu7g6A.htm](deities/CDkr6eXR1lgu7g6A.htm)|Andoletta|Andoletta|libre|
|[CeBP002LcMfqgIRl.htm](deities/CeBP002LcMfqgIRl.htm)|Picoperi|Picopéri|libre|
|[cHAbE86pto5bvIWo.htm](deities/cHAbE86pto5bvIWo.htm)|Angazhan|Angazhan|libre|
|[CnDEoo6WruQCXo1o.htm](deities/CnDEoo6WruQCXo1o.htm)|Lubaiko|Lubaiko|libre|
|[CzHjFLx4hpIEIoWt.htm](deities/CzHjFLx4hpIEIoWt.htm)|Dispater|Dispater|libre|
|[d47XTSODGAKANu3d.htm](deities/d47XTSODGAKANu3d.htm)|Vineshvakhi|Vineshvakhi|libre|
|[dcbf3pBSNa2q1Kkq.htm](deities/dcbf3pBSNa2q1Kkq.htm)|Dhalavei|Dhalavei|libre|
|[DEKKbg6riAdL2ARf.htm](deities/DEKKbg6riAdL2ARf.htm)|Hei Feng|Hei Feng|libre|
|[DFxbPmQLOqcmbPTz.htm](deities/DFxbPmQLOqcmbPTz.htm)|Tanagaar|Tanagaar|libre|
|[DJ7LDFTtns6YFodT.htm](deities/DJ7LDFTtns6YFodT.htm)|Ashava|Ashava|libre|
|[Dm8qlzpUj7g82QFv.htm](deities/Dm8qlzpUj7g82QFv.htm)|Erecura|Érécura|libre|
|[E1dUhoSEf2XfklU2.htm](deities/E1dUhoSEf2XfklU2.htm)|Mother Vulture|Mère Vautour|libre|
|[EE8sNM4JraOPN669.htm](deities/EE8sNM4JraOPN669.htm)|Cernunnos|Cernunnos|libre|
|[EfHlurXdqkdRXaqv.htm](deities/EfHlurXdqkdRXaqv.htm)|Set|Seth|libre|
|[EFyDjlH7XjGq7Erd.htm](deities/EFyDjlH7XjGq7Erd.htm)|Isis|Isis|libre|
|[ENzMYuzBZGIujSMx.htm](deities/ENzMYuzBZGIujSMx.htm)|Treerazer|Fléau des arbres|libre|
|[eyGEoggFlTEUQmvJ.htm](deities/eyGEoggFlTEUQmvJ.htm)|Bes|Bès|libre|
|[FeMWwhaVbaoqu3q6.htm](deities/FeMWwhaVbaoqu3q6.htm)|Reshmit of the Heavy Voice|Reshmit Voix forte (Déchu)|libre|
|[fTJLgTfEMZreJ19r.htm](deities/fTJLgTfEMZreJ19r.htm)|Milani|Milani|libre|
|[fTs56epghXwclPcX.htm](deities/fTs56epghXwclPcX.htm)|Stag Mother of the Forest of Stones|Mère Élaphe de la Forêt de pierres|libre|
|[fuPTR1laXO6EionS.htm](deities/fuPTR1laXO6EionS.htm)|Grandmother Spider|Grand-mère araignée|libre|
|[g1VxllMjs2YRIJAm.htm](deities/g1VxllMjs2YRIJAm.htm)|Moloch|Moloch|libre|
|[GBdxyS79vTozcv3P.htm](deities/GBdxyS79vTozcv3P.htm)|The Laborer's Bastion|Le Bastion du travailleur|libre|
|[GEBaoeiv5CB9h2Bn.htm](deities/GEBaoeiv5CB9h2Bn.htm)|Ragathiel|Ragathiel|libre|
|[giAsBYiL2omr0x2L.htm](deities/giAsBYiL2omr0x2L.htm)|Qi Zhong|Qi Zhong|libre|
|[gpS3hv7LP2IRwEHT.htm](deities/gpS3hv7LP2IRwEHT.htm)|Sorrow's Sword|Épée du chagrin|libre|
|[gsFaxWHbZmc1moY6.htm](deities/gsFaxWHbZmc1moY6.htm)|Nalinivati|Nalinivati|libre|
|[GUmYLD7PGmE2TOx6.htm](deities/GUmYLD7PGmE2TOx6.htm)|Wadjet|Wadjet|libre|
|[gutbzYTZvRTnq4on.htm](deities/gutbzYTZvRTnq4on.htm)|Adanye|Adanye|libre|
|[gXHoxuuiAD54LnWV.htm](deities/gXHoxuuiAD54LnWV.htm)|Cyth-V'sug|Cyth-V'sug|libre|
|[Gy78DCwfY4ltBGI6.htm](deities/Gy78DCwfY4ltBGI6.htm)|Husk|Dépouille (Déchu)|libre|
|[H1VrZ1QX0d2aPXT8.htm](deities/H1VrZ1QX0d2aPXT8.htm)|Dammerich|Dammerich|libre|
|[hAoVzkeFjTwh5Ykl.htm](deities/hAoVzkeFjTwh5Ykl.htm)|Shumunue|Shumunue|libre|
|[HbuynQRCjGKtbhcC.htm](deities/HbuynQRCjGKtbhcC.htm)|The Lost Prince|Le prince perdu|libre|
|[HD7g8T8ioy1EH3M8.htm](deities/HD7g8T8ioy1EH3M8.htm)|Bolka|Bolka|libre|
|[hhV2g6aU8jKLEca1.htm](deities/hhV2g6aU8jKLEca1.htm)|Narakaas|Narakaas|libre|
|[HtRxo4J3elFteIqu.htm](deities/HtRxo4J3elFteIqu.htm)|Arazni|Arazni|libre|
|[hWTTS0e7J9hJfsp3.htm](deities/hWTTS0e7J9hJfsp3.htm)|Matravash|Matravash|libre|
|[iEt6idGIEPXY2HzV.htm](deities/iEt6idGIEPXY2HzV.htm)|Ydajisk|Ydajisk|libre|
|[Ij1KRM8ufkEjGnR3.htm](deities/Ij1KRM8ufkEjGnR3.htm)|Ranginori|Ranginori|libre|
|[iJyhAFmHOLZBvWnJ.htm](deities/iJyhAFmHOLZBvWnJ.htm)|Sivanah|Sivanah|libre|
|[IK2AL3bL6htrrnnC.htm](deities/IK2AL3bL6htrrnnC.htm)|Skode|Skode|libre|
|[iKgERiIc8rt3fatf.htm](deities/iKgERiIc8rt3fatf.htm)|Nyarlathotep (Haunter in the Dark)|Nyarlathotep (Celui qui hante les ténèbres)|libre|
|[iMRXc519Uepf8yH3.htm](deities/iMRXc519Uepf8yH3.htm)|Magdh|Magdh|libre|
|[ItYptsfjV8XvfhOV.htm](deities/ItYptsfjV8XvfhOV.htm)|Lahkgya|Lahkgya|libre|
|[IXLzDSMGndFb5rGY.htm](deities/IXLzDSMGndFb5rGY.htm)|Falayna|Falayna|libre|
|[iXvyp9rGnqvFfy4q.htm](deities/iXvyp9rGnqvFfy4q.htm)|Kazutal|Kazutal|libre|
|[IZcdF1G8ZsnMotaM.htm](deities/IZcdF1G8ZsnMotaM.htm)|Pazuzu|Pazuzu|libre|
|[JcIf4uwKLxEWdplI.htm](deities/JcIf4uwKLxEWdplI.htm)|Kostchtchie|Kostchtchie|libre|
|[JgDqFUB1NtQkGmzp.htm](deities/JgDqFUB1NtQkGmzp.htm)|Otolmens|Otolmens|libre|
|[jL1qiVOZBKXETtVD.htm](deities/jL1qiVOZBKXETtVD.htm)|Nocticula|Nocticula|libre|
|[JndhOIHnQoMwnsw3.htm](deities/JndhOIHnQoMwnsw3.htm)|The Godclaw|Le dieu griffu|libre|
|[jrPmEfubRDzMKbxl.htm](deities/jrPmEfubRDzMKbxl.htm)|Arundhat|Arundhat|libre|
|[K1tzUClsozKcdpIe.htm](deities/K1tzUClsozKcdpIe.htm)|Halcamora|Halcamora|libre|
|[KB6O6dvwJj7wuwO7.htm](deities/KB6O6dvwJj7wuwO7.htm)|Urban Prosperity|Prospérité urbaine|libre|
|[KDFujU3Yug6AwCOz.htm](deities/KDFujU3Yug6AwCOz.htm)|Arshea|Arshéa|libre|
|[KIEgpcmHYtFu1dpg.htm](deities/KIEgpcmHYtFu1dpg.htm)|Imot|Imot|libre|
|[kiTcZxmKxGPCwDpg.htm](deities/kiTcZxmKxGPCwDpg.htm)|Kalekot|Kalekot|libre|
|[KPrtgoIcgiBSKzS3.htm](deities/KPrtgoIcgiBSKzS3.htm)|The Freeing Flame|La flamme libératrice|libre|
|[KWVdoAm3b01M8Lcp.htm](deities/KWVdoAm3b01M8Lcp.htm)|Mephistopheles|Méphistophélès|libre|
|[LGOkWwN6AZhLiZpl.htm](deities/LGOkWwN6AZhLiZpl.htm)|Touch of the Sun|Caresse du soleil|libre|
|[liXIbl7y8www2eAn.htm](deities/liXIbl7y8www2eAn.htm)|Findeladlara (The Guiding Hand)|Findeladlara|libre|
|[LLMTMgARSTlO7S9U.htm](deities/LLMTMgARSTlO7S9U.htm)|Zyphus|Zyphus|libre|
|[lSciGZGcbU9PJxQy.htm](deities/lSciGZGcbU9PJxQy.htm)|Irez|Irèz|libre|
|[lUFMwdv1UOyzFhIM.htm](deities/lUFMwdv1UOyzFhIM.htm)|Likha|Likha|libre|
|[lZ74P8kUA1ti808w.htm](deities/lZ74P8kUA1ti808w.htm)|Prophecies of Kalistrade|Prohéties de Kalistrade|libre|
|[M3eb94Nd6uXti2IK.htm](deities/M3eb94Nd6uXti2IK.htm)|Daikitsu|Daikitsu|libre|
|[m6UYxDnwGkOo8IY5.htm](deities/m6UYxDnwGkOo8IY5.htm)|Thisamet|Thisamet|libre|
|[MbJ93Nw39h2cBYi5.htm](deities/MbJ93Nw39h2cBYi5.htm)|Lao Shu Po|Lao Shu Po|libre|
|[McUqN13BGLTpYWCg.htm](deities/McUqN13BGLTpYWCg.htm)|Green Man|Homme vert|libre|
|[mebdNVKeKuTmDDHj.htm](deities/mebdNVKeKuTmDDHj.htm)|Xhamen-Dor|Xhamen-dor|libre|
|[Mv6uTJLh3VGEoGLH.htm](deities/Mv6uTJLh3VGEoGLH.htm)|Nivi Rhombodazzle|Nivi Rhomboéblouissante|libre|
|[N8a76dkGfUvxvSW0.htm](deities/N8a76dkGfUvxvSW0.htm)|Trudd|Trudd|libre|
|[nsiDGdgbAzkTQzOt.htm](deities/nsiDGdgbAzkTQzOt.htm)|Magrim|Magrim|libre|
|[nuqIimigyt986WLE.htm](deities/nuqIimigyt986WLE.htm)|The Lantern King|Le Roi Lanterne|libre|
|[nuvfUC4TcOvFbKxx.htm](deities/nuvfUC4TcOvFbKxx.htm)|Osiris|Osiris|libre|
|[o8ayCv3LAHWlu0vQ.htm](deities/o8ayCv3LAHWlu0vQ.htm)|Apsu|Apsu|libre|
|[o9tK1T28LNEVzpFV.htm](deities/o9tK1T28LNEVzpFV.htm)|Ragdya|Ragdya|libre|
|[Oci540vcXkJKR0gW.htm](deities/Oci540vcXkJKR0gW.htm)|The Enlightened Scholar's Path|La Voie de l'érudit éclairé|libre|
|[oduHa5yyTisFsIRD.htm](deities/oduHa5yyTisFsIRD.htm)|God Calling|Appelle dieu|libre|
|[oiEo6tDP3gICHtfZ.htm](deities/oiEo6tDP3gICHtfZ.htm)|Barbatos|Barbatos|libre|
|[oJPwMa9xG8oMnC14.htm](deities/oJPwMa9xG8oMnC14.htm)|Apollyon|Apollyon|libre|
|[OlVCyRgOaPJji4qt.htm](deities/OlVCyRgOaPJji4qt.htm)|Chohar|Chohar|libre|
|[Ons4JwstcXOlkaRF.htm](deities/Ons4JwstcXOlkaRF.htm)|The Perplexing Jest|La Plaisanterie Perplexe|libre|
|[oRvuLq3o2FxXizt9.htm](deities/oRvuLq3o2FxXizt9.htm)|Ghlaunder|Ghlaunder|libre|
|[oV5cnlPJQS8JfMIg.htm](deities/oV5cnlPJQS8JfMIg.htm)|Cosmic Caravan|Caravane cosmique|libre|
|[oyp2E685VsQFKMXi.htm](deities/oyp2E685VsQFKMXi.htm)|Dranngvit|Dranngvit|libre|
|[p4TbCVOwHB4ccmT4.htm](deities/p4TbCVOwHB4ccmT4.htm)|The Endless Road|La route sans fin|officielle|
|[pafYqAGj47TmGBib.htm](deities/pafYqAGj47TmGBib.htm)|Verilorn|Vérilorn|libre|
|[pCtJONJlhAugUXaG.htm](deities/pCtJONJlhAugUXaG.htm)|Ahriman|Ahriman|libre|
|[PFp4Sdp2TDDKD62l.htm](deities/PFp4Sdp2TDDKD62l.htm)|Thamir|Thamir|libre|
|[PIMd4yRGj8XQgGbW.htm](deities/PIMd4yRGj8XQgGbW.htm)|The Divine Dare|Le Défi divin|libre|
|[pm8H2DbtrHM01KNV.htm](deities/pm8H2DbtrHM01KNV.htm)|Naderi|Nadéri|libre|
|[pqrmviqboqjeS4z8.htm](deities/pqrmviqboqjeS4z8.htm)|Eyes That Watch|Yeux veilleurs (Déchu)|libre|
|[pVQzTL5elNTxvffu.htm](deities/pVQzTL5elNTxvffu.htm)|The Green Mother|La Mère verte|libre|
|[QArqJ387HeYgGZxG.htm](deities/QArqJ387HeYgGZxG.htm)|Gravelady's Guard|La garde de la Dame des tombes|libre|
|[QibpLccM5bLUMopj.htm](deities/QibpLccM5bLUMopj.htm)|Lymnieris|Lymniéris|libre|
|[QlDnoOwf0PppymTn.htm](deities/QlDnoOwf0PppymTn.htm)|Hanspur|Hanspur|libre|
|[R0DvoLO8WbYIYhb3.htm](deities/R0DvoLO8WbYIYhb3.htm)|Atreia|Atréia|libre|
|[r5vooG7NfvA9prJK.htm](deities/r5vooG7NfvA9prJK.htm)|Kurgess|Kurgess|libre|
|[r6SFB1PzKsagDXff.htm](deities/r6SFB1PzKsagDXff.htm)|Lady Nanbyo|Dame Nanbyo|libre|
|[RbslST0RZPXj6Eby.htm](deities/RbslST0RZPXj6Eby.htm)|Ashukharma|Ashukharma|libre|
|[rDTXcBJ8LyqA2TkL.htm](deities/rDTXcBJ8LyqA2TkL.htm)|Szuriel|Szuriel|libre|
|[remyY1BwhEVmBwya.htm](deities/remyY1BwhEVmBwya.htm)|Belial|Bélial|libre|
|[RlOuyadtreI7pht1.htm](deities/RlOuyadtreI7pht1.htm)|The Resplendent Court|La Cour Resplendissante|libre|
|[RnS5P7zX0HRPsDnt.htm](deities/RnS5P7zX0HRPsDnt.htm)|Ferrumnestra|Ferrumnestra|libre|
|[rRuxBgC58PxrLzRv.htm](deities/rRuxBgC58PxrLzRv.htm)|Horus|Horus|libre|
|[RUzMtIQal0drPwQY.htm](deities/RUzMtIQal0drPwQY.htm)|Gogunta|Gogunta|libre|
|[RzTd9PmgnfHNlFVf.htm](deities/RzTd9PmgnfHNlFVf.htm)|Ng|Ng|libre|
|[s1PgIQ8Oi35oS2Et.htm](deities/s1PgIQ8Oi35oS2Et.htm)|Shoanti Animism|Animisme shoanti|libre|
|[s81N9hxffmRuy3ik.htm](deities/s81N9hxffmRuy3ik.htm)|Kerkamoth|Kerkamoth|libre|
|[s9opv94lGDRKnw6W.htm](deities/s9opv94lGDRKnw6W.htm)|Shizuru|Shizuru|libre|
|[SAuFz101iP6VVwvw.htm](deities/SAuFz101iP6VVwvw.htm)|Nurgal|Nurgal|libre|
|[SCGdJmm7Wzle9jFP.htm](deities/SCGdJmm7Wzle9jFP.htm)|Zohls|Zohls|libre|
|[SCMlQDcoU1QcF3AS.htm](deities/SCMlQDcoU1QcF3AS.htm)|Lysianassa|Lysianassa|libre|
|[se2OQ8L4F4ku304o.htm](deities/se2OQ8L4F4ku304o.htm)|Arundhat (The Sacred Perfume)|Arundhat|libre|
|[sILda7I8Ak20Ioj9.htm](deities/sILda7I8Ak20Ioj9.htm)|Azathoth|Azathoth|libre|
|[smGfeOk8Jgcz0Qkv.htm](deities/smGfeOk8Jgcz0Qkv.htm)|Immonhiel|Immonhiel|libre|
|[sNjPI1iaS8Z43YJf.htm](deities/sNjPI1iaS8Z43YJf.htm)|Mammon|Mammon|libre|
|[soiVh53y9RAeSLEk.htm](deities/soiVh53y9RAeSLEk.htm)|Casandalee|Casandalee|libre|
|[SUaGdvQXxSAQAw03.htm](deities/SUaGdvQXxSAQAw03.htm)|Yog-Sothoth|Yog-Sothoth|libre|
|[tb0oADxOoMhyrYRA.htm](deities/tb0oADxOoMhyrYRA.htm)|Yamatsumi|Yamatsumi|libre|
|[tcvobm8O84qv0aO5.htm](deities/tcvobm8O84qv0aO5.htm)|Gruhastha|Gruhastha|libre|
|[tcXrtRYCOr2EmgGB.htm](deities/tcXrtRYCOr2EmgGB.htm)|Charon|Charon|libre|
|[tk7gRxXsqvBwQrUF.htm](deities/tk7gRxXsqvBwQrUF.htm)|Grundinnar|Grundinnar|libre|
|[TpAMy8tb5LrRgvqN.htm](deities/TpAMy8tb5LrRgvqN.htm)|Hathor|Hathor|libre|
|[TvxsVRztDlGcWpcI.htm](deities/TvxsVRztDlGcWpcI.htm)|Sobek|Sobek|libre|
|[TW7Q6O928YWNME9r.htm](deities/TW7Q6O928YWNME9r.htm)|Kitumu|Kitumu|libre|
|[u2tnG7fKkZ2KHMcT.htm](deities/u2tnG7fKkZ2KHMcT.htm)|Suyuddha|Suyuddha|libre|
|[u3YfcJh1C4w6n75j.htm](deities/u3YfcJh1C4w6n75j.htm)|Raumya|Raumya|libre|
|[ub0PJ5QkrdoGzzb2.htm](deities/ub0PJ5QkrdoGzzb2.htm)|Luhar|Luhar|libre|
|[upRQcwjjaksc1g7h.htm](deities/upRQcwjjaksc1g7h.htm)|Enkaar, the Malformed Prisoner|Enkaar, le Prisonnier malformé (Déchu)|libre|
|[UUcAqU3KeJSMNxGF.htm](deities/UUcAqU3KeJSMNxGF.htm)|Barzahk|Barzahk|libre|
|[uXkMGx0OD896ZQlT.htm](deities/uXkMGx0OD896ZQlT.htm)|Alglenweis|Alglenweis|libre|
|[uydTiyN5AzhBnQY8.htm](deities/uydTiyN5AzhBnQY8.htm)|Ardad Lili|Ardad Lili|libre|
|[uYpxTi4byHc5w78R.htm](deities/uYpxTi4byHc5w78R.htm)|Yuelral|Yuelral|libre|
|[v1fsLP6nTkiy5ghB.htm](deities/v1fsLP6nTkiy5ghB.htm)|Mazludeh|Mazludeh|libre|
|[V5PbbkZUP9N7kl6h.htm](deities/V5PbbkZUP9N7kl6h.htm)|Kabriri|Kabriri|libre|
|[vEMCpm7iidycRT5D.htm](deities/vEMCpm7iidycRT5D.htm)|Brigh|Brigh|libre|
|[VEyowuhC4FsBnG8c.htm](deities/VEyowuhC4FsBnG8c.htm)|Folgrit|Folgrit|libre|
|[VG51y4FbTLdeGHZB.htm](deities/VG51y4FbTLdeGHZB.htm)|The Path of the Heavens|Le chemin des cieux|libre|
|[Vk1FM54rJx1RHtUA.htm](deities/Vk1FM54rJx1RHtUA.htm)|Seafarers' Hope|L'espoir des marins|libre|
|[vnmoAGURp8oWCY6R.htm](deities/vnmoAGURp8oWCY6R.htm)|Soralyon|Soralyon|libre|
|[VRPVppdQ4W0oXeB1.htm](deities/VRPVppdQ4W0oXeB1.htm)|Alocer|Alocer|libre|
|[vSUz97Y29l1O50bC.htm](deities/vSUz97Y29l1O50bC.htm)|Count Ranalc|Comte Ranalc|libre|
|[VvxEvEKhCGRwrMJp.htm](deities/VvxEvEKhCGRwrMJp.htm)|Imbrex|Imbrex|libre|
|[Vzxm5FyIA40b2OSP.htm](deities/Vzxm5FyIA40b2OSP.htm)|Pulura|Pulura|libre|
|[w4fDrwL8VIqJDPfR.htm](deities/w4fDrwL8VIqJDPfR.htm)|Uvuko|Uvuko|libre|
|[W4QGkdn9V5ACRpkh.htm](deities/W4QGkdn9V5ACRpkh.htm)|Monad|La Monade|libre|
|[w66gCNiZ7dslqG8K.htm](deities/w66gCNiZ7dslqG8K.htm)|Lissala|Lissala|libre|
|[w7B4HD5XOFaLb3ZG.htm](deities/w7B4HD5XOFaLb3ZG.htm)|Hearth and Harvest|Foyer et Récolte|libre|
|[w9vtltygWNG4eZDR.htm](deities/w9vtltygWNG4eZDR.htm)|Jin Li|Jin Li|libre|
|[WfqLY2Fm1LGCcG5X.htm](deities/WfqLY2Fm1LGCcG5X.htm)|Sangpotshi|Sangpotshi|libre|
|[wjXNXFWBLbJaeZQR.htm](deities/wjXNXFWBLbJaeZQR.htm)|Ketephys|Kétéphys|libre|
|[wkDOeK7ENz1ra8IC.htm](deities/wkDOeK7ENz1ra8IC.htm)|Besmara|Besmara|libre|
|[WloVxo6HXJdS4YTt.htm](deities/WloVxo6HXJdS4YTt.htm)|Groetus|Groétus|libre|
|[WO9UsCY41oTtC2G0.htm](deities/WO9UsCY41oTtC2G0.htm)|Pillars of Knowledge|Piliers de la connaissance|libre|
|[woDEgBs1MznhzpJ4.htm](deities/woDEgBs1MznhzpJ4.htm)|Ra|Râ|libre|
|[WOiSJ4IWjaRsSgRO.htm](deities/WOiSJ4IWjaRsSgRO.htm)|Geryon|Géryon|libre|
|[WvoMr86bSSYxXKtI.htm](deities/WvoMr86bSSYxXKtI.htm)|Hastur|Hastur|libre|
|[XaaDDVS4sEJd4m99.htm](deities/XaaDDVS4sEJd4m99.htm)|Dajermube|Dajermube|libre|
|[Xb1N5YfxboJH26Jh.htm](deities/Xb1N5YfxboJH26Jh.htm)|Angradd|Angradd|libre|
|[XHDlP7o1jNz9851v.htm](deities/XHDlP7o1jNz9851v.htm)|Diomazul|Diomazul|libre|
|[xLbSMedLDFyb8sFw.htm](deities/xLbSMedLDFyb8sFw.htm)|Laudinmio|Laudimio|libre|
|[xTD0XFaguBX6QQ2L.htm](deities/xTD0XFaguBX6QQ2L.htm)|Selket|Selket|libre|
|[xu05XGotyT5st56H.htm](deities/xu05XGotyT5st56H.htm)|Lady Jingxi|Dame Jingxi|libre|
|[XxIxEaW8NG952Bc0.htm](deities/XxIxEaW8NG952Bc0.htm)|Grasping Iovett|Iovett la Prise (Déchu)|libre|
|[xxUGdZYUj5IW7E0L.htm](deities/xxUGdZYUj5IW7E0L.htm)|Followers of Fate|Fidèles du destin|libre|
|[XYJupIPx0GW4s7eu.htm](deities/XYJupIPx0GW4s7eu.htm)|Ragadahn|Ragadahn|libre|
|[y3RlZ3FnbZN8wv8j.htm](deities/y3RlZ3FnbZN8wv8j.htm)|Demon Bringers|Porteurs de démons|libre|
|[y93whGnGZjFyw5A0.htm](deities/y93whGnGZjFyw5A0.htm)|Vildeis|Vildéïs|libre|
|[YeUhlOt3c98ocwjZ.htm](deities/YeUhlOt3c98ocwjZ.htm)|The Offering Plate|Plateau d'Offrandes|libre|
|[yIfdsL1788boOOYk.htm](deities/yIfdsL1788boOOYk.htm)|Sky Keepers|Gardiens du ciel|libre|
|[yiKle2URZ43KYVKQ.htm](deities/yiKle2URZ43KYVKQ.htm)|Chamidu|Chamidu|libre|
|[yX5U1Uvdug9i08nU.htm](deities/yX5U1Uvdug9i08nU.htm)|Conqueror Worm|Ver conquérant|libre|
|[YXmeBdOZfWgUljW1.htm](deities/YXmeBdOZfWgUljW1.htm)|Ylimancha|Ylimancha|libre|
|[zgM2jsxw52HjALV0.htm](deities/zgM2jsxw52HjALV0.htm)|The Deliberate Journey|Le Voyage délibéré|libre|
|[zhPhYge7VJbIlJvA.htm](deities/zhPhYge7VJbIlJvA.htm)|Kols|Kols|libre|
|[Zi1UARhode3FSFLr.htm](deities/Zi1UARhode3FSFLr.htm)|General Susumu|Général Susumu|libre|
|[zlJGKpeLBwajP91o.htm](deities/zlJGKpeLBwajP91o.htm)|Gyronna|Gyronna|libre|
|[zRmAXj7FSlrMfDeA.htm](deities/zRmAXj7FSlrMfDeA.htm)|Bastet|Bastet|libre|
|[Zw8nhcpRxinoYRSa.htm](deities/Zw8nhcpRxinoYRSa.htm)|Elven Pantheon|Panthéon elfique|libre|
|[ZynnoQJFz12FD0Xn.htm](deities/ZynnoQJFz12FD0Xn.htm)|Wards of the Pharaoh|Protection du pharaon|libre|
|[zzhYFcShT9JoE2Mp.htm](deities/zzhYFcShT9JoE2Mp.htm)|Shax|Shax|libre|
