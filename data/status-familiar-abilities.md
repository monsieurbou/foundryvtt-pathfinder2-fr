# État de la traduction (familiar-abilities)

 * **officielle**: 11
 * **libre**: 74
 * **changé**: 2


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[onye3PN84lmca11D.htm](familiar-abilities/onye3PN84lmca11D.htm)|Familiar of Keen Senses|Familier des sens acérés|changé|
|[ZCze1FY3Rmnvd6Dx.htm](familiar-abilities/ZCze1FY3Rmnvd6Dx.htm)|Familiar of Restored Spirit|Familier d'esprit récupéré|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Xrkk46IM43iI1Fv.htm](familiar-abilities/0Xrkk46IM43iI1Fv.htm)|Darkvision|Vision dans le noir|officielle|
|[2fiQzoEKu6YUnrU9.htm](familiar-abilities/2fiQzoEKu6YUnrU9.htm)|Independent|Indépendant|libre|
|[3y6GGXQgyJ4Hq4Yt.htm](familiar-abilities/3y6GGXQgyJ4Hq4Yt.htm)|Radiant|Radieux|libre|
|[43xB5UnexISlfRa5.htm](familiar-abilities/43xB5UnexISlfRa5.htm)|Purify Air|Purificateur d'air|libre|
|[5019hvUZcNe9dLEO.htm](familiar-abilities/5019hvUZcNe9dLEO.htm)|Elemental Familiar (Fire)|Familier élémentaire (feu)|libre|
|[57b8u8s3fV0UCrgJ.htm](familiar-abilities/57b8u8s3fV0UCrgJ.htm)|Plant Form|Forme de plante|libre|
|[5eqcXtPsYunphMOZ.htm](familiar-abilities/5eqcXtPsYunphMOZ.htm)|Construct|Créature artificielle|libre|
|[5gwqSpkRqWzrbDDU.htm](familiar-abilities/5gwqSpkRqWzrbDDU.htm)|Damage Avoidance: Will|Évitement des dégâts : Volonté|libre|
|[6ZmgkMHUGGb1mDhr.htm](familiar-abilities/6ZmgkMHUGGb1mDhr.htm)|Familiar of Balanced Luck|Familier de la chance équilibrée|libre|
|[7QosmRHlyLLhU1hX.htm](familiar-abilities/7QosmRHlyLLhU1hX.htm)|Lab Assistant|Assistant de laboratoire|officielle|
|[7ZxPS0UU7pf7wjp0.htm](familiar-abilities/7ZxPS0UU7pf7wjp0.htm)|Ambassador|Ambassadeur|libre|
|[8Z1UkLEWkFWIjOF8.htm](familiar-abilities/8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|Réservoir de poison|libre|
|[92lgSEPFIDLvKOCF.htm](familiar-abilities/92lgSEPFIDLvKOCF.htm)|Accompanist|Accompagnateur|libre|
|[9PsptrEoCC4QdM23.htm](familiar-abilities/9PsptrEoCC4QdM23.htm)|Valet|Valet|libre|
|[9xTjRt8LkZPB42Vi.htm](familiar-abilities/9xTjRt8LkZPB42Vi.htm)|Familiar of Freezing Rime|Familier de glace verglacée|libre|
|[9ypX5ZMor6RMiFKv.htm](familiar-abilities/9ypX5ZMor6RMiFKv.htm)|Familiar of Flowing Script|Familier d'inscriptions fluides|libre|
|[A0C86V3MUECpX5jd.htm](familiar-abilities/A0C86V3MUECpX5jd.htm)|Amphibious|Amphibie|libre|
|[aEKA3YWekLhEhuV8.htm](familiar-abilities/aEKA3YWekLhEhuV8.htm)|Threat Display|Démonstration menaçante|libre|
|[Amzezp93MZckBYRZ.htm](familiar-abilities/Amzezp93MZckBYRZ.htm)|Wavesense|Perception des ondes|libre|
|[asOhEdyF8CWFbR96.htm](familiar-abilities/asOhEdyF8CWFbR96.htm)|Spellcasting|Incantateur|libre|
|[BXssJhTJjKrfojwG.htm](familiar-abilities/BXssJhTJjKrfojwG.htm)|Fast Movement: Land|Déplacement rapide : Au sol|libre|
|[C16JgmeJJG249WXz.htm](familiar-abilities/C16JgmeJJG249WXz.htm)|Mask Freeze|Masque figé|officielle|
|[ch6nFcRJO1fm8B4D.htm](familiar-abilities/ch6nFcRJO1fm8B4D.htm)|Elemental|Élémentaire|libre|
|[cT5octWchU4gjrhP.htm](familiar-abilities/cT5octWchU4gjrhP.htm)|Manual Dexterity|Dextérité manuelle|libre|
|[cy7bdQqVANipyljS.htm](familiar-abilities/cy7bdQqVANipyljS.htm)|Erudite|Érudit|libre|
|[D0ltNUJnN7UjJpA1.htm](familiar-abilities/D0ltNUJnN7UjJpA1.htm)|Innate Surge|Déferlante innée|officielle|
|[deC1yIM2S5szGdzT.htm](familiar-abilities/deC1yIM2S5szGdzT.htm)|Lifelink|Lien vital|libre|
|[dpjf1CyMEILpJWyp.htm](familiar-abilities/dpjf1CyMEILpJWyp.htm)|Darkeater|Mangeur noir|libre|
|[dWTfO5WbLkD5mw2H.htm](familiar-abilities/dWTfO5WbLkD5mw2H.htm)|Climber|Grimpeur|officielle|
|[embGS7NjgydDw8KO.htm](familiar-abilities/embGS7NjgydDw8KO.htm)|Dragon|Dragon|libre|
|[FcQQLMAJMgOLjnSv.htm](familiar-abilities/FcQQLMAJMgOLjnSv.htm)|Resistance|Résistance|libre|
|[FeEts6s2ABVEi0H6.htm](familiar-abilities/FeEts6s2ABVEi0H6.htm)|Familiar of Stalking Night|Familier de la nuit de la traque|libre|
|[fhyQ6uTSvfErTzyE.htm](familiar-abilities/fhyQ6uTSvfErTzyE.htm)|Jet|Propulseur|libre|
|[FlRUb8U13Crj3NaA.htm](familiar-abilities/FlRUb8U13Crj3NaA.htm)|Scent|Odorat|libre|
|[fmsVItn94FeY5Q3X.htm](familiar-abilities/fmsVItn94FeY5Q3X.htm)|Snoop|Fouineur|libre|
|[gPceRQqO847lvSnb.htm](familiar-abilities/gPceRQqO847lvSnb.htm)|Share Senses|Partage des sens|officielle|
|[hMrxiUPHXKpKu1Ha.htm](familiar-abilities/hMrxiUPHXKpKu1Ha.htm)|Major Resistance|Résistance majeure|libre|
|[iBZzOaRNodGtfMVJ.htm](familiar-abilities/iBZzOaRNodGtfMVJ.htm)|Absorb Familiar|Familier absorbé|libre|
|[j1qZiH50Bl2SJ8vT.htm](familiar-abilities/j1qZiH50Bl2SJ8vT.htm)|Shadow Step|Pas d'ombre|libre|
|[j9vOSbF9kLibhSIf.htm](familiar-abilities/j9vOSbF9kLibhSIf.htm)|Second Opinion|Seconde opinion|libre|
|[jdlefpPcSCIe27vO.htm](familiar-abilities/jdlefpPcSCIe27vO.htm)|Familiar Focus|Focalisation du familier|officielle|
|[jevzf9JbJJibpqaI.htm](familiar-abilities/jevzf9JbJJibpqaI.htm)|Skilled|Compétent|libre|
|[jew52aXGdNyR4N5B.htm](familiar-abilities/jew52aXGdNyR4N5B.htm)|Fungus|Champignon|libre|
|[jfCmKk6pXcINVROk.htm](familiar-abilities/jfCmKk6pXcINVROk.htm)|Echolocation|Écholocalisation|libre|
|[JRP2bdkdCdj2JDrq.htm](familiar-abilities/JRP2bdkdCdj2JDrq.htm)|Master's Form|Forme du maître|libre|
|[K5OLRDsGCfPZ6mO6.htm](familiar-abilities/K5OLRDsGCfPZ6mO6.htm)|Damage Avoidance: Reflex|Évitement des dégâts : Réflexes|libre|
|[Le8UWr5BU8rV3iBf.htm](familiar-abilities/Le8UWr5BU8rV3iBf.htm)|Tough|Robuste|libre|
|[lLWkoGiNLNIEm5Sp.htm](familiar-abilities/lLWkoGiNLNIEm5Sp.htm)|Plant|Plante|libre|
|[lpyJAl5B4j2DC7mc.htm](familiar-abilities/lpyJAl5B4j2DC7mc.htm)|Gills|Branchies|libre|
|[LrDnat1DsGJoAiKv.htm](familiar-abilities/LrDnat1DsGJoAiKv.htm)|Tremorsense|Perception des vibrations|libre|
|[LUBS9csNNgRTui4p.htm](familiar-abilities/LUBS9csNNgRTui4p.htm)|Grasping Tendrils|Vrilles agrippantes|libre|
|[mIl4YjbssQ5AERWS.htm](familiar-abilities/mIl4YjbssQ5AERWS.htm)|Elemental Familiar (Water)|Familier élémentaire (eau)|libre|
|[mK3mAUWiRLZZYNdz.htm](familiar-abilities/mK3mAUWiRLZZYNdz.htm)|Damage Avoidance: Fortitude|Évitement des dégâts : Vigueur|libre|
|[mKmBgQmPmiLOlEvw.htm](familiar-abilities/mKmBgQmPmiLOlEvw.htm)|Augury|Augure|libre|
|[mu9OL8v4rmr9311e.htm](familiar-abilities/mu9OL8v4rmr9311e.htm)|Familiar of Ongoing Misery|Familier de misère persistante|libre|
|[nrPl3Dz7fbnmas7T.htm](familiar-abilities/nrPl3Dz7fbnmas7T.htm)|Spirit Touch|Contact spirituel|libre|
|[o0fxDDUu2ZSWYDTr.htm](familiar-abilities/o0fxDDUu2ZSWYDTr.htm)|Soul Sight|Vision de l'âme|libre|
|[O5TIjqXAuta8iVSz.htm](familiar-abilities/O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|Récupération concentrée|libre|
|[ou91pzf9TlOnIjYn.htm](familiar-abilities/ou91pzf9TlOnIjYn.htm)|Luminous|Lumineux|libre|
|[Q42nrq9LwDdbeZM5.htm](familiar-abilities/Q42nrq9LwDdbeZM5.htm)|Restorative Familiar|Familier curatif|libre|
|[q9IXWngAOCpsXxsO.htm](familiar-abilities/q9IXWngAOCpsXxsO.htm)|Levitator|Léviteur|libre|
|[qTxH8mSOvc4PMzrP.htm](familiar-abilities/qTxH8mSOvc4PMzrP.htm)|Kinspeech|Parole des semblables|officielle|
|[ReIgBsaM95BTvHpN.htm](familiar-abilities/ReIgBsaM95BTvHpN.htm)|Medic|Médecin|libre|
|[REJfFezELjc5Gzsy.htm](familiar-abilities/REJfFezELjc5Gzsy.htm)|Recall Familiar|Rappel de familier|libre|
|[RIx6b1GvTEBh4ExW.htm](familiar-abilities/RIx6b1GvTEBh4ExW.htm)|Elemental Familiar (Air)|Familier élémentaire (air)|libre|
|[rs4Awf4k1e0Mj797.htm](familiar-abilities/rs4Awf4k1e0Mj797.htm)|Cantrip Connection|Lien avec les tours de magie|libre|
|[Rv1KTRioaZOWyQI6.htm](familiar-abilities/Rv1KTRioaZOWyQI6.htm)|Alchemical Gut|Entrailles alchimiques|libre|
|[SKIS1xexaOvrecdV.htm](familiar-abilities/SKIS1xexaOvrecdV.htm)|Verdant Burst|Explosion verdoyante|libre|
|[SxWYVgqNMsq0OijU.htm](familiar-abilities/SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|Déplacement rapide : Escalade|libre|
|[tEWAHPPZULvPgHnT.htm](familiar-abilities/tEWAHPPZULvPgHnT.htm)|Tattoo Transformation|Transformation en tatouage|libre|
|[uUrsZ4WvhjKjFjnt.htm](familiar-abilities/uUrsZ4WvhjKjFjnt.htm)|Toolbearer|Porte outils|libre|
|[uy15sDBuYNK48N3v.htm](familiar-abilities/uy15sDBuYNK48N3v.htm)|Burrower|Fouisseur|officielle|
|[v7zE3tKQb9G6PaYU.htm](familiar-abilities/v7zE3tKQb9G6PaYU.htm)|Partner in Crime|Associés dans le crime|libre|
|[VHQUZcjUxfC3GcJ9.htm](familiar-abilities/VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|Déplacement rapide : Vol|libre|
|[vpw2ReYdcyQBpdqn.htm](familiar-abilities/vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|Déplacement rapide : Nage|libre|
|[wOgvBymJOVQDSm1Q.htm](familiar-abilities/wOgvBymJOVQDSm1Q.htm)|Spell Delivery|Transfert de sort|libre|
|[Xanjwv4YU0CBnsMw.htm](familiar-abilities/Xanjwv4YU0CBnsMw.htm)|Spell Battery|Accumulateur de sort|libre|
|[XCqYnlVbLGqEGPeX.htm](familiar-abilities/XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|Télépathie de contact|libre|
|[XYb2udoNGaE0M11Y.htm](familiar-abilities/XYb2udoNGaE0M11Y.htm)|Elemental Familiar (Wood)|Familier élémentaire (bois)|libre|
|[ZHSzNt3NxkXbj1mI.htm](familiar-abilities/ZHSzNt3NxkXbj1mI.htm)|Flier|Volant|libre|
|[ZJdMyCjRvUTtg90u.htm](familiar-abilities/ZJdMyCjRvUTtg90u.htm)|Elemental Familiar (Metal)|Familier élémentaire (métal)|libre|
|[zKTL9y9et0oTHEYS.htm](familiar-abilities/zKTL9y9et0oTHEYS.htm)|Greater Resistance|Résistance supérieure|libre|
|[ZtKb89o1PPhwJ3Lx.htm](familiar-abilities/ZtKb89o1PPhwJ3Lx.htm)|Extra Reagents|Réactifs supplémentaires|officielle|
|[ZV4J5YTZGrqh6cvY.htm](familiar-abilities/ZV4J5YTZGrqh6cvY.htm)|Elemental Familiar (Earth)|Familier élémentaire (terre)|libre|
|[zyMRLQnFCQVpltiR.htm](familiar-abilities/zyMRLQnFCQVpltiR.htm)|Speech|Parole|officielle|
