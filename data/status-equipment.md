# État de la traduction (equipment)

 * **libre**: 3585
 * **officielle**: 389
 * **changé**: 667


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[armor-13-4XYnZij3BqhFuayG.htm](equipment/armor-13-4XYnZij3BqhFuayG.htm)|Tales in Timber (Greater)|Contes de bois supérieurs|changé|
|[armor-13-n4kPBFzVh5LcJAMA.htm](equipment/armor-13-n4kPBFzVh5LcJAMA.htm)|Harmonic Hauberk|Haubert harmonique|changé|
|[armor-13-OlGFuOcqy0bSdtTl.htm](equipment/armor-13-OlGFuOcqy0bSdtTl.htm)|Suit of Armoire|Panoplie d'armoire|changé|
|[armor-13-peAvz7u35GEfTXxp.htm](equipment/armor-13-peAvz7u35GEfTXxp.htm)|Elven Chain (Standard-Grade)|Maille elfique de qualité standard|changé|
|[armor-13-WgxgGLp3q9Et6BwC.htm](equipment/armor-13-WgxgGLp3q9Et6BwC.htm)|Faerie Queen's Bower|Charmille de la Reine fée|changé|
|[armor-14-bu4IrYBeE68KeEOW.htm](equipment/armor-14-bu4IrYBeE68KeEOW.htm)|Shared-Pain Sankeit|Sankeit de douleur partagée|changé|
|[armor-14-h9sZfo6UmHIOU5wb.htm](equipment/armor-14-h9sZfo6UmHIOU5wb.htm)|Life-Saver Mail|Clibanion salvateur|changé|
|[armor-14-Iic9QpNemaumfkHc.htm](equipment/armor-14-Iic9QpNemaumfkHc.htm)|Medusa Armor|Armure de la méduse|changé|
|[armor-15-ed2F0L3R6UNSFUmS.htm](equipment/armor-15-ed2F0L3R6UNSFUmS.htm)|Victory Plate (Greater)|Harnois de la victoire supérieur|changé|
|[armor-15-TJsmB2aHk9Yogqup.htm](equipment/armor-15-TJsmB2aHk9Yogqup.htm)|Fungal Armor|Armure fongique|changé|
|[armor-16-nLhb0w0DbtUC6weH.htm](equipment/armor-16-nLhb0w0DbtUC6weH.htm)|Armor of the Holy Warrior|Armure du guerrier saint|changé|
|[armor-17-630xJ5zceXFXiTi2.htm](equipment/armor-17-630xJ5zceXFXiTi2.htm)|Prismatic Plate|Cuirasse prismatique|changé|
|[armor-17-aLA7pikfeNIAAGLw.htm](equipment/armor-17-aLA7pikfeNIAAGLw.htm)|Impenetrable Scale|Armure impénétrable|changé|
|[armor-18-o9y5aRGsbH5x77Mn.htm](equipment/armor-18-o9y5aRGsbH5x77Mn.htm)|Tales in Timber (Major)|Contes de bois majeurs|changé|
|[armor-18-XKON66YXYLXlGPPg.htm](equipment/armor-18-XKON66YXYLXlGPPg.htm)|Breastplate of Command (Greater)|Cuirasse de commandement supérieur|changé|
|[armor-19-TBmDARyj67joVe4m.htm](equipment/armor-19-TBmDARyj67joVe4m.htm)|Library Robes (True)|Robe de bibliothécaire ultime|changé|
|[armor-20-JU6gJSmibl5Q3k6F.htm](equipment/armor-20-JU6gJSmibl5Q3k6F.htm)|Breastplate of the Mountain|Cuirasse de la montagne|changé|
|[armor-20-oRBcuFdQDRCn1pwo.htm](equipment/armor-20-oRBcuFdQDRCn1pwo.htm)|Life-Saver Mail (Greater)|Clibanion salvateur supérieur|changé|
|[armor-20-PuqdH7DmNsN79HyE.htm](equipment/armor-20-PuqdH7DmNsN79HyE.htm)|Elven Chain (High-Grade)|Maille elfique de haute qualité|changé|
|[armor-26-yNteshMHb0xIXFlC.htm](equipment/armor-26-yNteshMHb0xIXFlC.htm)|Ancestral Embrace|Étreinte ancestrale|changé|
|[backpack-12-iAfqKpHyJ6beLGjB.htm](equipment/backpack-12-iAfqKpHyJ6beLGjB.htm)|Lucky Draw Bandolier (Greater)|Bandoulière du veinard supérieur|changé|
|[backpack-13-9cK0QVSjNd8tQtiQ.htm](equipment/backpack-13-9cK0QVSjNd8tQtiQ.htm)|Knapsack of Halflingkind (Greater)|Besace Halfeline supérieure|changé|
|[backpack-15-ulgsKBzOXJbjqnmW.htm](equipment/backpack-15-ulgsKBzOXJbjqnmW.htm)|Portable Hole|Trou portable|changé|
|[backpack-17-LRSIRUERqBAJ1HGT.htm](equipment/backpack-17-LRSIRUERqBAJ1HGT.htm)|Voyager's Pack|Sac à dos du voyageur|changé|
|[consumable-02-1TWHN8RbimPVXM0U.htm](equipment/consumable-02-1TWHN8RbimPVXM0U.htm)|Sinew-Shock Serum (Lesser)|Sérum de choc-vigueur inférieur|changé|
|[consumable-02-66EBP5Ii9QMlZdDx.htm](equipment/consumable-02-66EBP5Ii9QMlZdDx.htm)|Static Snare|Piège artisanal statique|changé|
|[consumable-02-8KbayiwrUJtvif0a.htm](equipment/consumable-02-8KbayiwrUJtvif0a.htm)|Focus Cathartic (Lesser)|Élixir de concentration cathartique inférieur|changé|
|[consumable-02-ENAlE5JV00GFkGjb.htm](equipment/consumable-02-ENAlE5JV00GFkGjb.htm)|Quickmelt Slick (Lesser)|Pommade de fonte rapide inférieure|changé|
|[consumable-02-xky9NMHpP3yqzYOj.htm](equipment/consumable-02-xky9NMHpP3yqzYOj.htm)|Dragon's Blood Pudding|Poudding sang de dragon|changé|
|[consumable-03-89uak2oyHnEuIn0b.htm](equipment/consumable-03-89uak2oyHnEuIn0b.htm)|Fulu of Fire Suppression|Fulu d'extinction des incendies|changé|
|[consumable-03-SRMgleqCK2tsZjvS.htm](equipment/consumable-03-SRMgleqCK2tsZjvS.htm)|Etheric Essence Disruptor (Lesser)|Perturbateur d'essence éthérique inférieur|changé|
|[consumable-03-wu9cqBx65txvAODd.htm](equipment/consumable-03-wu9cqBx65txvAODd.htm)|Material Essence Disruptor (Lesser)|Perturbateur d'essence matérielle inférieur|changé|
|[consumable-03-ZvOaZLwTT4kDbrF7.htm](equipment/consumable-03-ZvOaZLwTT4kDbrF7.htm)|Fulu of Flood Suppression|Fulu de suppression d'inondation|changé|
|[consumable-04-8D71bv2HsX29ZtwC.htm](equipment/consumable-04-8D71bv2HsX29ZtwC.htm)|Quickmelt Slick (Moderate)|Pommade de fonte rapide modérée|changé|
|[consumable-04-tslVf3qtQE7V1YvG.htm](equipment/consumable-04-tslVf3qtQE7V1YvG.htm)|Sinew-Shock Serum (Moderate)|Sérum de choc-vigueur modéré|changé|
|[consumable-04-XfcSVHwMA60JxUXJ.htm](equipment/consumable-04-XfcSVHwMA60JxUXJ.htm)|Focus Cathartic (Moderate)|Élixir de concentration cathartique modéré|changé|
|[consumable-05-EKhROfCklhZ7Je7h.htm](equipment/consumable-05-EKhROfCklhZ7Je7h.htm)|Contagion Metabolizer (Lesser)|Métaboliseur de contagion inférieur|changé|
|[consumable-05-fzAkwYDi8sXWfCNu.htm](equipment/consumable-05-fzAkwYDi8sXWfCNu.htm)|Dragon's Blood Pudding (Moderate)|Poudding sang de dragon modéré|changé|
|[consumable-05-xDWUGwLpKLm8WdQP.htm](equipment/consumable-05-xDWUGwLpKLm8WdQP.htm)|Fu Water|Eau fu|changé|
|[consumable-05-YnkfinMN0l06iR1t.htm](equipment/consumable-05-YnkfinMN0l06iR1t.htm)|Wine of the Blood|Vin de sang|changé|
|[consumable-07-d49GcmPZ2xWxU8Zj.htm](equipment/consumable-07-d49GcmPZ2xWxU8Zj.htm)|Etheric Essence Disruptor (Moderate)|Perturbateur d'essence éthérique modéré|changé|
|[consumable-07-rfU6tyzEadCXHSSN.htm](equipment/consumable-07-rfU6tyzEadCXHSSN.htm)|Material Essence Disruptor (Moderate)|Perturbateur d'essence matériel modéré|changé|
|[consumable-09-kv0Hg5RPROT4xqmh.htm](equipment/consumable-09-kv0Hg5RPROT4xqmh.htm)|Silencing Ammunition|Munition silencieuse|changé|
|[consumable-09-q07Ej07uIsKx5zab.htm](equipment/consumable-09-q07Ej07uIsKx5zab.htm)|Mana-Rattler Liniment|Liniment de crotale de la Désolation|changé|
|[consumable-10-BYl1jXHOpDJVYsvY.htm](equipment/consumable-10-BYl1jXHOpDJVYsvY.htm)|Sairazul Blue|Bleu sairazul|changé|
|[consumable-10-cEfqMV1lFaT7s0QC.htm](equipment/consumable-10-cEfqMV1lFaT7s0QC.htm)|Potion of Stable Form|Potion de forme stable|changé|
|[consumable-10-IzniYBi8QYxHnffr.htm](equipment/consumable-10-IzniYBi8QYxHnffr.htm)|Vat-Grown Brain|Cerveau cultivé en cuve|changé|
|[consumable-11-1DHjXJEdJ7GlGSzg.htm](equipment/consumable-11-1DHjXJEdJ7GlGSzg.htm)|Oil of Keen Edges|Huile d'affûtage|changé|
|[consumable-11-4DfaAHIxnWPMoWb5.htm](equipment/consumable-11-4DfaAHIxnWPMoWb5.htm)|Ferrofluid Urchin (Moderate)|Oursin ferrofluide modéré|changé|
|[consumable-11-4gAndhLFJ4Is3NJ2.htm](equipment/consumable-11-4gAndhLFJ4Is3NJ2.htm)|Drought Powder|Poudre asséchante|changé|
|[consumable-11-byFW6sILekJaPERu.htm](equipment/consumable-11-byFW6sILekJaPERu.htm)|Material Essence Disruptor (Greater)|Perturbateur d'essence matériel supérieur|changé|
|[consumable-11-Cc4SokzVBoBCkHId.htm](equipment/consumable-11-Cc4SokzVBoBCkHId.htm)|Blightburn Resin|Résine de champibrûle|changé|
|[consumable-11-EmuRXJpGelePyaO5.htm](equipment/consumable-11-EmuRXJpGelePyaO5.htm)|Enigma-Sight Potion|Potion de vision-énigmatique|changé|
|[consumable-11-GWMrc8hZINhmDwvt.htm](equipment/consumable-11-GWMrc8hZINhmDwvt.htm)|Contagion Metabolizer (Moderate)|Métaboliseur de contagion modéré|changé|
|[consumable-11-IZRfgOYlZ3HRBkYX.htm](equipment/consumable-11-IZRfgOYlZ3HRBkYX.htm)|Oil of Repulsion|Huile de répulsion|changé|
|[consumable-11-LcNUAglIYiTxwmNo.htm](equipment/consumable-11-LcNUAglIYiTxwmNo.htm)|Etheric Essence Disruptor (Greater)|Perturbateur d'essence éthérique supérieur|changé|
|[consumable-11-PkksgUAYXJeewn7T.htm](equipment/consumable-11-PkksgUAYXJeewn7T.htm)|Silencing Shot|Tir de silence|changé|
|[consumable-11-TMtQnY6yvIRCpK9v.htm](equipment/consumable-11-TMtQnY6yvIRCpK9v.htm)|Juggernaut Mutagen (Greater)|Mutagène de juggernaut supérieur|changé|
|[consumable-11-uf05VGoYSaeKtUG1.htm](equipment/consumable-11-uf05VGoYSaeKtUG1.htm)|Elder Seed|Graine ancienne|changé|
|[consumable-12-0ySQli0JRrkZySOB.htm](equipment/consumable-12-0ySQli0JRrkZySOB.htm)|Snarling Badger (Moderate)|Blaireau grognant modéré|changé|
|[consumable-12-4RKfLoqVluZGWzLc.htm](equipment/consumable-12-4RKfLoqVluZGWzLc.htm)|Incense of Distilled Death|Encens de mort distillée|changé|
|[consumable-12-62HxCEDwhlZaeR0Q.htm](equipment/consumable-12-62HxCEDwhlZaeR0Q.htm)|Sinew-Shock Serum (Greater)|Sérum de choc-vigueur supérieur|changé|
|[consumable-12-cxOf1V1kN9tnj1g9.htm](equipment/consumable-12-cxOf1V1kN9tnj1g9.htm)|Eye of Apprehension|Oeil d'appréhension|changé|
|[consumable-12-drxEWZl8mqMOZ23E.htm](equipment/consumable-12-drxEWZl8mqMOZ23E.htm)|Potion of True Speech|Potion de Langage universel|changé|
|[consumable-12-FrbvejTNfS5fep9f.htm](equipment/consumable-12-FrbvejTNfS5fep9f.htm)|Fade Band|Fil de disparition|changé|
|[consumable-12-JmxKmYQPQmUlauO4.htm](equipment/consumable-12-JmxKmYQPQmUlauO4.htm)|Fury Cocktail (Moderate)|Cocktail de la fureur modéré|changé|
|[consumable-12-jUiva1MoZDtbi6xI.htm](equipment/consumable-12-jUiva1MoZDtbi6xI.htm)|Quickmelt Slick (Greater)|Pommade de fonte rapide supérieure|changé|
|[consumable-12-KMcYZKGlsjKMjSNi.htm](equipment/consumable-12-KMcYZKGlsjKMjSNi.htm)|Dimension Shot|Tir dimension|changé|
|[consumable-12-KVrmsNbro6mJ9wuT.htm](equipment/consumable-12-KVrmsNbro6mJ9wuT.htm)|Oil of Animation|Huile d'animation|changé|
|[consumable-12-tOg1nt08dBTZbkwR.htm](equipment/consumable-12-tOg1nt08dBTZbkwR.htm)|Penetrating Ammunition|Munition pénétrante|changé|
|[consumable-12-vMs9n8oXlZttcJkX.htm](equipment/consumable-12-vMs9n8oXlZttcJkX.htm)|Focus Cathartic (Greater)|Élixir de concentration cathartique supérieur|changé|
|[consumable-12-WvDbdESFJY6Be2u1.htm](equipment/consumable-12-WvDbdESFJY6Be2u1.htm)|Blackfinger Blight|Fléau du doigt noir|changé|
|[consumable-12-YScPBPwB4t9sydp0.htm](equipment/consumable-12-YScPBPwB4t9sydp0.htm)|Spell-Eating Pitch|Résine mange-sorts|changé|
|[consumable-12-z7eOUqVwyht6tj11.htm](equipment/consumable-12-z7eOUqVwyht6tj11.htm)|Slumber Wine|Vin soporifique|changé|
|[consumable-12-zAlDcTlvCd3MxXmI.htm](equipment/consumable-12-zAlDcTlvCd3MxXmI.htm)|Shadow Ash|Cendre d'ombre|changé|
|[consumable-13-1YROvQsCdq8WFWRj.htm](equipment/consumable-13-1YROvQsCdq8WFWRj.htm)|Time Shield Potion|Potion de bouclier temporel|changé|
|[consumable-13-87AzvRja9uQOLJCC.htm](equipment/consumable-13-87AzvRja9uQOLJCC.htm)|Deathcap Powder|Poudre d'amanite phalloïde|changé|
|[consumable-13-9nrB5W2evrc2U3op.htm](equipment/consumable-13-9nrB5W2evrc2U3op.htm)|Bower Fruit|Fruit de la charmille|changé|
|[consumable-13-cRtDr8WhXhxkGrmx.htm](equipment/consumable-13-cRtDr8WhXhxkGrmx.htm)|Reflecting Shard|Écharde réfléchissante|changé|
|[consumable-13-JSmdTDUEGwHVbV7g.htm](equipment/consumable-13-JSmdTDUEGwHVbV7g.htm)|Mending Lattice|Maillage réparateur|changé|
|[consumable-13-LRSiYsgEz3e0PEwX.htm](equipment/consumable-13-LRSiYsgEz3e0PEwX.htm)|Panacea|Panacée|changé|
|[consumable-13-QrjeT3xBssyjvEHQ.htm](equipment/consumable-13-QrjeT3xBssyjvEHQ.htm)|Dragon's Blood Pudding (Greater)|Poudding sang de dragon supérieur|changé|
|[consumable-13-vjwdfXDWSe7L3zwl.htm](equipment/consumable-13-vjwdfXDWSe7L3zwl.htm)|Reaper's Spellgun|Lancesort de la faucheuse|changé|
|[consumable-13-Vm0xyg3xE4iMq3V3.htm](equipment/consumable-13-Vm0xyg3xE4iMq3V3.htm)|Roaring Potion (Moderate)|Potion de rugissement modérée|changé|
|[consumable-14-Bgslo3kDz8bTiYie.htm](equipment/consumable-14-Bgslo3kDz8bTiYie.htm)|Defiled Costa|Côté souillée|changé|
|[consumable-14-ClKb4YQn8TfPIclE.htm](equipment/consumable-14-ClKb4YQn8TfPIclE.htm)|Dazing Coil|Anneaux d'étourdissement|changé|
|[consumable-14-j77uu6eFlsYoZApx.htm](equipment/consumable-14-j77uu6eFlsYoZApx.htm)|Ruby String|Fil de ruby|changé|
|[consumable-14-mWOxG8asXbjCRVG9.htm](equipment/consumable-14-mWOxG8asXbjCRVG9.htm)|Reflected Moonlight Fulu|Fulu du rayon lunaire réfléchi|changé|
|[consumable-14-sjxhPujyv6rJJRWO.htm](equipment/consumable-14-sjxhPujyv6rJJRWO.htm)|Viper's Fang|Croc de vipère|changé|
|[consumable-14-smPuKlJLRyoYaDpR.htm](equipment/consumable-14-smPuKlJLRyoYaDpR.htm)|Nevercold (Compressed)|Jamais froid compressé|changé|
|[consumable-14-vAEQsYQbOYmqfIc0.htm](equipment/consumable-14-vAEQsYQbOYmqfIc0.htm)|Liar's Demise|Chute du menteur|changé|
|[consumable-14-Zjevw0WzUs1IIUAL.htm](equipment/consumable-14-Zjevw0WzUs1IIUAL.htm)|Ghost Ammunition|Munition fantôme|changé|
|[consumable-15-cSrCTi2zE5OU5ylH.htm](equipment/consumable-15-cSrCTi2zE5OU5ylH.htm)|Lifeblight Residue|Résidu de mort affligée|changé|
|[consumable-15-dhn6dx5bvZBgZGHs.htm](equipment/consumable-15-dhn6dx5bvZBgZGHs.htm)|Ferrofluid Urchin (Greater)|Oursin ferrofluide supérieur|changé|
|[consumable-15-GMi5tw0cbMx3ZQPg.htm](equipment/consumable-15-GMi5tw0cbMx3ZQPg.htm)|Mindfog Mist|Brumentale|changé|
|[consumable-15-KO7ZftYQrcE5GFkJ.htm](equipment/consumable-15-KO7ZftYQrcE5GFkJ.htm)|Etheric Essence Disruptor (Major)|Perturbateur d'essence éthérique majeur|changé|
|[consumable-15-omWrYy6RY9rxeF50.htm](equipment/consumable-15-omWrYy6RY9rxeF50.htm)|Material Essence Disruptor (Major)|Perturbateur d'essence matériel majeur|changé|
|[consumable-15-QD5rDuk0OHadPGuh.htm](equipment/consumable-15-QD5rDuk0OHadPGuh.htm)|Obfuscation Oil|Huile d'obscurcissement|changé|
|[consumable-15-wTVBD8XHPG810rlH.htm](equipment/consumable-15-wTVBD8XHPG810rlH.htm)|Stone Bullet|Bille de pierre|changé|
|[consumable-15-ZJE92c4sE6cu8l8l.htm](equipment/consumable-15-ZJE92c4sE6cu8l8l.htm)|Heartblood Ring|Anneau Coeur de sang|changé|
|[consumable-16-3joQBdbHQo66FT6S.htm](equipment/consumable-16-3joQBdbHQo66FT6S.htm)|Dragonclaw Scutcheon|Écusson griffe de dragon|changé|
|[consumable-16-9nhvZ7VnDQHuyBdf.htm](equipment/consumable-16-9nhvZ7VnDQHuyBdf.htm)|Brimstone Fumes|Fumées sulfureuses|changé|
|[consumable-16-CoMwPsQ8mPj5Evti.htm](equipment/consumable-16-CoMwPsQ8mPj5Evti.htm)|Truesight Potion|Potion de vision véritable|changé|
|[consumable-16-CXCWF6jQQHKHFhpR.htm](equipment/consumable-16-CXCWF6jQQHKHFhpR.htm)|Gift of the Poisoned Heart|Don du coeur empoisonné|changé|
|[consumable-16-dmQAN56aMro0gecx.htm](equipment/consumable-16-dmQAN56aMro0gecx.htm)|Ghost Dust|Poussière fantomatique|changé|
|[consumable-16-KOWn72lufGfszQXr.htm](equipment/consumable-16-KOWn72lufGfszQXr.htm)|Potion of Stable Form (Greater)|Potion de forme stable supérieure|changé|
|[consumable-16-po3Lhiccc0RQYikr.htm](equipment/consumable-16-po3Lhiccc0RQYikr.htm)|Phoenix Cinder|Cendres de Phénix|changé|
|[consumable-16-WAUq4TB4Q9FDxWFz.htm](equipment/consumable-16-WAUq4TB4Q9FDxWFz.htm)|Candle of Invocation|Bougie d'invocation|changé|
|[consumable-17-alUdoHA3sbaSJL9f.htm](equipment/consumable-17-alUdoHA3sbaSJL9f.htm)|Hungering Maw|Gueule affamée|changé|
|[consumable-17-asuxBXmrZErr9tep.htm](equipment/consumable-17-asuxBXmrZErr9tep.htm)|Awakened Silver Shot|Tir d'argent éveillé|changé|
|[consumable-17-KsoofB7FCppaBTdK.htm](equipment/consumable-17-KsoofB7FCppaBTdK.htm)|Celestial Peach (Rejuvenation)|Pêche céleste de rajeunissement|changé|
|[consumable-17-kwp0N4MdOgRpdKi6.htm](equipment/consumable-17-kwp0N4MdOgRpdKi6.htm)|Awakened Cold Iron Shot|Tir de fer froid éveillé|changé|
|[consumable-17-mGIJCGFkBQLkzhTg.htm](equipment/consumable-17-mGIJCGFkBQLkzhTg.htm)|Dispelling Sliver|Éclat de dissipation|changé|
|[consumable-17-MP7updiiSct04vno.htm](equipment/consumable-17-MP7updiiSct04vno.htm)|Hemlock|Cigüe|changé|
|[consumable-17-MpyuCFBQwbOQhhm0.htm](equipment/consumable-17-MpyuCFBQwbOQhhm0.htm)|Reflecting Shard (Greater)|Écharde réfléchissante supérieure|changé|
|[consumable-17-oO0DqxI7EGkTYJzv.htm](equipment/consumable-17-oO0DqxI7EGkTYJzv.htm)|Defiled Costa (Greater)|Côte souillée supérieure|changé|
|[consumable-17-sK2zEyhxxP6UnhQk.htm](equipment/consumable-17-sK2zEyhxxP6UnhQk.htm)|Blast Boots (Major)|Bottes d'explosion majeures|changé|
|[consumable-18-aMXCMLI3spk0d7tq.htm](equipment/consumable-18-aMXCMLI3spk0d7tq.htm)|Nevercold (Refined)|Jamais froid raffiné|changé|
|[consumable-18-Ksz7kd6P9uKR8uLo.htm](equipment/consumable-18-Ksz7kd6P9uKR8uLo.htm)|Clockwork Rejuvenator|Régénérateur mécanique|changé|
|[consumable-18-m6oNZUSSWYMnk7ii.htm](equipment/consumable-18-m6oNZUSSWYMnk7ii.htm)|Roaring Potion (Greater)|Potion de rugissement supérieure|changé|
|[consumable-18-MUL0ABe8h7Ugc1Jt.htm](equipment/consumable-18-MUL0ABe8h7Ugc1Jt.htm)|Fury Cocktail (Greater)|Cocktail de la fureur supérieur|changé|
|[consumable-18-Pnw4A7fhUhTS85Te.htm](equipment/consumable-18-Pnw4A7fhUhTS85Te.htm)|Focus Cathartic (Major)|Élixir de concentration cathartique majeur|changé|
|[consumable-18-po9gLNA6GjystED5.htm](equipment/consumable-18-po9gLNA6GjystED5.htm)|Snarling Badger (Greater)|Blaireau grognant supérieur|changé|
|[consumable-18-UblsH5cGyUdXypek.htm](equipment/consumable-18-UblsH5cGyUdXypek.htm)|Potion of Undetectability|Potion de détection impossible|changé|
|[consumable-18-YIgIF1845n5UBFnE.htm](equipment/consumable-18-YIgIF1845n5UBFnE.htm)|Sinew-Shock Serum (Major)|Sérum de choc-vigueur majeur|changé|
|[consumable-19-9WenEIQrBV8POQU1.htm](equipment/consumable-19-9WenEIQrBV8POQU1.htm)|Panacea Fruit|Fruit panacée|changé|
|[consumable-19-kUvjWSGWpwBvTVax.htm](equipment/consumable-19-kUvjWSGWpwBvTVax.htm)|Dragon's Blood Pudding (Major)|Poudding de sang de dragon majeur|changé|
|[consumable-19-ommn7wIgLyWHGL11.htm](equipment/consumable-19-ommn7wIgLyWHGL11.htm)|Contagion Metabolizer (Greater)|Métaboliseur de contagion supérieur|changé|
|[consumable-19-xQS1MSqGQz44FWUh.htm](equipment/consumable-19-xQS1MSqGQz44FWUh.htm)|Black Lotus Extract|Extrait de lotus noir|changé|
|[consumable-20-737LQfIaLXBPEzc3.htm](equipment/consumable-20-737LQfIaLXBPEzc3.htm)|Antimagic Oil|Huile antimagie|changé|
|[consumable-20-gDGPvobJV1QGYBPy.htm](equipment/consumable-20-gDGPvobJV1QGYBPy.htm)|Tears of Death|Larmes de la mort|changé|
|[consumable-20-Im9LfiRTfAJxPvzS.htm](equipment/consumable-20-Im9LfiRTfAJxPvzS.htm)|Thousand-Pains Fulu (Void)|Fulu des mille douleurs (Vide)|changé|
|[consumable-20-ktAjhlI9NxpFJMdp.htm](equipment/consumable-20-ktAjhlI9NxpFJMdp.htm)|Celestial Peach (Life)|Pêche céleste de vie|changé|
|[equipment-03-1tqyghMGI6PEYFGK.htm](equipment/equipment-03-1tqyghMGI6PEYFGK.htm)|Dawnlight|Lumière de l'aube|changé|
|[equipment-03-rtfpj9WJHMnMtfuG.htm](equipment/equipment-03-rtfpj9WJHMnMtfuG.htm)|Polished Demon Horn|Corne de démon polie|changé|
|[equipment-05-KnI89eneLdJeURob.htm](equipment/equipment-05-KnI89eneLdJeURob.htm)|True Name Amulet (Lesser)|Amulette du nom véritable inférieure|changé|
|[equipment-06-qMmg6e6GlMAM0F39.htm](equipment/equipment-06-qMmg6e6GlMAM0F39.htm)|Society Portrait|Portrait de la société|changé|
|[equipment-07-babiGiHQwY6Bg4XP.htm](equipment/equipment-07-babiGiHQwY6Bg4XP.htm)|Dawnlight (Greater)|Lumière de l'aube supérieure|changé|
|[equipment-08-62oxvKUCReo7Iah7.htm](equipment/equipment-08-62oxvKUCReo7Iah7.htm)|Polished Demon Horn (Greater)|Corne de démon polie supérieure|changé|
|[equipment-08-8MclH3pggVkHGYnJ.htm](equipment/equipment-08-8MclH3pggVkHGYnJ.htm)|Carrion Cask|Tonneau à charogne|changé|
|[equipment-08-mb59fHB9IPExjqam.htm](equipment/equipment-08-mb59fHB9IPExjqam.htm)|Chair of Inventions|Fauteuil des inventions|changé|
|[equipment-08-UgfTCOJjjMsrqynr.htm](equipment/equipment-08-UgfTCOJjjMsrqynr.htm)|Clarity Goggles (Lesser)|Lunettes de clarté inférieures|changé|
|[equipment-09-Hd1AfC08ytBg67Ey.htm](equipment/equipment-09-Hd1AfC08ytBg67Ey.htm)|Mage's Hat (Greater)|Couvre-chef de thaumaturge supérieur|changé|
|[equipment-09-nx5Y4GybdZqf9rIf.htm](equipment/equipment-09-nx5Y4GybdZqf9rIf.htm)|Saurian Spike|Pointe saurienne|changé|
|[equipment-09-P5nasaE0JgvkZyZp.htm](equipment/equipment-09-P5nasaE0JgvkZyZp.htm)|Bracers of Missile Deflection (Greater)|Protège-bras de déviation de projectiles supérieur|changé|
|[equipment-09-TQsV76vrzj6WCqem.htm](equipment/equipment-09-TQsV76vrzj6WCqem.htm)|Mindsponge|Éponge mentale|changé|
|[equipment-09-yhw1mr4oDhzMQc3D.htm](equipment/equipment-09-yhw1mr4oDhzMQc3D.htm)|Heartstone|Cardioline|changé|
|[equipment-10-3b0wleCdaPw3c9UX.htm](equipment/equipment-10-3b0wleCdaPw3c9UX.htm)|Sun Sight|Vision du soleil|changé|
|[equipment-10-t28PWYfoGoj1Sq3e.htm](equipment/equipment-10-t28PWYfoGoj1Sq3e.htm)|True Name Amulet (Moderate)|Amulette du nom véritable modérée|changé|
|[equipment-11-0fHBB9Xb4m3oAoko.htm](equipment/equipment-11-0fHBB9Xb4m3oAoko.htm)|Hexing Jar|Jarre à maléfices|changé|
|[equipment-11-2Xpjq6eYMqvBL6eg.htm](equipment/equipment-11-2Xpjq6eYMqvBL6eg.htm)|Bewitching Bloom (Purple Iris)|Fleur envoûtante (Iris pourpre)|changé|
|[equipment-11-4Hb8tvvWFtykbZhG.htm](equipment/equipment-11-4Hb8tvvWFtykbZhG.htm)|Armor Potency (+2)|Puissance d'armure +2 (rune)|changé|
|[equipment-11-72gfEUjIcdKVJZg0.htm](equipment/equipment-11-72gfEUjIcdKVJZg0.htm)|Wardrobe Stone (Moderate)|Pierre de garde-robe modérée|changé|
|[equipment-11-8nz3fSOOkNmNduMr.htm](equipment/equipment-11-8nz3fSOOkNmNduMr.htm)|Master Magus Ring|Anneau du maître magus|changé|
|[equipment-11-AFE073UYI0mkWuUs.htm](equipment/equipment-11-AFE073UYI0mkWuUs.htm)|Skeleton Key (Greater)|Passe-partout supérieur|changé|
|[equipment-11-BagxmaKWmG5SYdFV.htm](equipment/equipment-11-BagxmaKWmG5SYdFV.htm)|Dragon Rune Bracelet|Bracelet runique de dragon|changé|
|[equipment-11-boQHr2Z0W6TQCJo3.htm](equipment/equipment-11-boQHr2Z0W6TQCJo3.htm)|Oracular Crown|Couronne oraculaire|changé|
|[equipment-11-byIihT9MMpTFvyYT.htm](equipment/equipment-11-byIihT9MMpTFvyYT.htm)|Bravery Baldric (Restoration, Greater)|Baudrier de bravoure (Restauration supérieure)|changé|
|[equipment-11-c0tITjJAX5nc2xXx.htm](equipment/equipment-11-c0tITjJAX5nc2xXx.htm)|Thoughtwhip Claw|Griffe fouettepensée|changé|
|[equipment-11-cMErkgi3GjwT0bTz.htm](equipment/equipment-11-cMErkgi3GjwT0bTz.htm)|Wand of Refracting Rays (4th-level)|Baguette des rayons réfractés (rang 4)|changé|
|[equipment-11-D2CGb29g3ZUNZNWk.htm](equipment/equipment-11-D2CGb29g3ZUNZNWk.htm)|Mask of Uncanny Breath|Masque de souffle étrange|changé|
|[equipment-11-DH0kB9Wbr5pDeunX.htm](equipment/equipment-11-DH0kB9Wbr5pDeunX.htm)|Holy|Sainte (rune)|changé|
|[equipment-11-EKYjF0GuuBkmMD1J.htm](equipment/equipment-11-EKYjF0GuuBkmMD1J.htm)|Accompaniment Cloak|Cape d'accompagnement|changé|
|[equipment-11-fumxKes1z2hLN2U7.htm](equipment/equipment-11-fumxKes1z2hLN2U7.htm)|Ready (Greater)|Paré (rune, supérieur)|changé|
|[equipment-11-FW4vzcHuehTXnCLX.htm](equipment/equipment-11-FW4vzcHuehTXnCLX.htm)|Mind's Light Circlet|Circlet de lumière de l'esprit|changé|
|[equipment-11-fZLZGO0YLY8xYPh5.htm](equipment/equipment-11-fZLZGO0YLY8xYPh5.htm)|Bracers of Devotion|Brassards de dévotion|changé|
|[equipment-11-FzwSBVaSTt1s4A66.htm](equipment/equipment-11-FzwSBVaSTt1s4A66.htm)|Amazing Pop-up Book|Livre éphémère extraordinaire|changé|
|[equipment-11-gmMrJREf4JSHd2dZ.htm](equipment/equipment-11-gmMrJREf4JSHd2dZ.htm)|Unholy|Impie (rune)|changé|
|[equipment-11-I2ul8Bzcth7sleow.htm](equipment/equipment-11-I2ul8Bzcth7sleow.htm)|Ochre Fulcrum Lens|Lentille à pivot ocre|changé|
|[equipment-11-idAyVZxTkPSNLbnt.htm](equipment/equipment-11-idAyVZxTkPSNLbnt.htm)|Quill of Passage|Plume de passage|changé|
|[equipment-11-JSYtBZ7XblyAEFoV.htm](equipment/equipment-11-JSYtBZ7XblyAEFoV.htm)|Crafter's Eyepiece (Greater)|Oculaire d'artisan supérieur|changé|
|[equipment-11-KFWi1lxXJi46VDmz.htm](equipment/equipment-11-KFWi1lxXJi46VDmz.htm)|Mirror Goggles (Moderate)|Lunettes miroir modérées|changé|
|[equipment-11-nLvP7U50hLqz26Uy.htm](equipment/equipment-11-nLvP7U50hLqz26Uy.htm)|Ring of Maniacal Devices|Anneau d'objets déments|changé|
|[equipment-11-nxAMYJrG96yn9iBq.htm](equipment/equipment-11-nxAMYJrG96yn9iBq.htm)|Eidolon Cape|Cape eidolon|changé|
|[equipment-11-OHs7L8CJMdp2UV0P.htm](equipment/equipment-11-OHs7L8CJMdp2UV0P.htm)|Holy Prayer Beads (Greater)|Chapelet sacré supérieur|changé|
|[equipment-11-OI20gS0SRKVAORqm.htm](equipment/equipment-11-OI20gS0SRKVAORqm.htm)|Fiendish Teleportation|Téléportation fiélonne|changé|
|[equipment-11-oxL8uExmIomunm5g.htm](equipment/equipment-11-oxL8uExmIomunm5g.htm)|Boots of Dancing|Bottes de danse effrénée|changé|
|[equipment-11-pW3pdFtKZLSlsqSm.htm](equipment/equipment-11-pW3pdFtKZLSlsqSm.htm)|Kindled Tome|Tome de l'embrasement|changé|
|[equipment-11-rnhXFlynrb79chQ5.htm](equipment/equipment-11-rnhXFlynrb79chQ5.htm)|Dawnflower Beads|Chapelet de la Fleur de l'aube|changé|
|[equipment-11-u7NB1e1rUqYoQ0nz.htm](equipment/equipment-11-u7NB1e1rUqYoQ0nz.htm)|Saurian Spike (Greater)|Pointe saurienne supérieure|changé|
|[equipment-11-WYg2dZ8UK1N3rpgs.htm](equipment/equipment-11-WYg2dZ8UK1N3rpgs.htm)|Gorget of the Primal Roar|Gorgerin de rugissement primal|changé|
|[equipment-11-xS7hRZp6jI8jIga4.htm](equipment/equipment-11-xS7hRZp6jI8jIga4.htm)|Ki Channeling Beads|Chapelet de canalisation du ki|changé|
|[equipment-11-Y23lqGpVYNacdUUl.htm](equipment/equipment-11-Y23lqGpVYNacdUUl.htm)|Corruption Cassock|Soutane de corruption|changé|
|[equipment-11-ZKFmyG9fIoQWIrql.htm](equipment/equipment-11-ZKFmyG9fIoQWIrql.htm)|Spectacles of Understanding (Greater)|Lunettes de compréhension supérieure|changé|
|[equipment-11-zSnvJub3XMlFes1G.htm](equipment/equipment-11-zSnvJub3XMlFes1G.htm)|Warden's Signet|Chavalière de gardien|changé|
|[equipment-12-07Nu4AC0B5CI3zpc.htm](equipment/equipment-12-07Nu4AC0B5CI3zpc.htm)|Bloodline Robe|Robe de lignage|changé|
|[equipment-12-18ztTUiUZNjuc7K1.htm](equipment/equipment-12-18ztTUiUZNjuc7K1.htm)|Trickster's Mandolin (Major)|Mandoline du mystificateur majeure|changé|
|[equipment-12-2sr23ZSpplB1oBHd.htm](equipment/equipment-12-2sr23ZSpplB1oBHd.htm)|Trinity Geode (Major)|Géode de la trinité majeure|changé|
|[equipment-12-6nJZGgIJIY3x0b1Q.htm](equipment/equipment-12-6nJZGgIJIY3x0b1Q.htm)|Spider Chair|Fauteuil araignée|changé|
|[equipment-12-8buhFcGwuaJRrp0y.htm](equipment/equipment-12-8buhFcGwuaJRrp0y.htm)|Fortification|Fortification (rune)|changé|
|[equipment-12-9CAWAKkZE7dr4FlJ.htm](equipment/equipment-12-9CAWAKkZE7dr4FlJ.htm)|Energy-Resistant (Greater)|Résistance à l'énergie supérieure (rune)|changé|
|[equipment-12-bbEoBR1OIqmOIX00.htm](equipment/equipment-12-bbEoBR1OIqmOIX00.htm)|Sandalwood Fan|Éventail en bois de santal|changé|
|[equipment-12-CTiLugp5ptSItEqN.htm](equipment/equipment-12-CTiLugp5ptSItEqN.htm)|Ebon Fulcrum Lens|Lentille à pivot ébène|changé|
|[equipment-12-cvO0dLw2B1Zno8rv.htm](equipment/equipment-12-cvO0dLw2B1Zno8rv.htm)|Conch of Otherworldly Seas|Conque des mers d'outremonde|changé|
|[equipment-12-eRlOMigJjx5jAEKw.htm](equipment/equipment-12-eRlOMigJjx5jAEKw.htm)|Polished Demon Horn (Major)|Corne de démon polie majeure|changé|
|[equipment-12-KwqN8qNqp3AFwFbn.htm](equipment/equipment-12-KwqN8qNqp3AFwFbn.htm)|Telekinetic Converters|Convertisseurs télékinésiques|changé|
|[equipment-12-L4pVWrrQDRVpdAlo.htm](equipment/equipment-12-L4pVWrrQDRVpdAlo.htm)|Everburning Coal|Charbon ardent perpétuel|changé|
|[equipment-12-LbdnZFlyLFdAE617.htm](equipment/equipment-12-LbdnZFlyLFdAE617.htm)|Brilliant|Brillante (rune)|changé|
|[equipment-12-nhyXfERJ0SNFmcb0.htm](equipment/equipment-12-nhyXfERJ0SNFmcb0.htm)|Bagpipes of Turmoil (Major)|Cornemuse du tumulte majeure|changé|
|[equipment-12-o5jn0wvPaSh65p3J.htm](equipment/equipment-12-o5jn0wvPaSh65p3J.htm)|Hummingbird Wayfinder|Guide du colibri|changé|
|[equipment-12-PGBAg9ywfdlJCoCl.htm](equipment/equipment-12-PGBAg9ywfdlJCoCl.htm)|Resonant Guitar|Guitare résonante|changé|
|[equipment-12-pXOPMJC9NAuwXz3w.htm](equipment/equipment-12-pXOPMJC9NAuwXz3w.htm)|Violet Ray|Rayon violet|changé|
|[equipment-12-pZhqdggB36MpDUwh.htm](equipment/equipment-12-pZhqdggB36MpDUwh.htm)|Ghost Lantern|Lanterne fantôme|changé|
|[equipment-12-QqN3JHXXNveMnjnd.htm](equipment/equipment-12-QqN3JHXXNveMnjnd.htm)|Seer's Flute (Major)|Flûte du devin majeure|changé|
|[equipment-12-rUxQi4pUbMeHrnSX.htm](equipment/equipment-12-rUxQi4pUbMeHrnSX.htm)|Cordelia's Greater Construct Key|Clé d'animation de Cordélia supérieure|changé|
|[equipment-12-TucjFO5NQHvUUc7a.htm](equipment/equipment-12-TucjFO5NQHvUUc7a.htm)|Jolt Coil (Major)|Bobine d'électrochoc majeure|changé|
|[equipment-12-uz3JCjRvkE44jxMd.htm](equipment/equipment-12-uz3JCjRvkE44jxMd.htm)|Fearsome (Greater)|Effrayante supérieure (rune)|changé|
|[equipment-12-v7YHA9UhDgsvQ47Q.htm](equipment/equipment-12-v7YHA9UhDgsvQ47Q.htm)|Ghostcaller's Planchette|Planchette de l'invocateur de fantôme|changé|
|[equipment-12-V8cYi1Gawe71d0rE.htm](equipment/equipment-12-V8cYi1Gawe71d0rE.htm)|Curtain Call Cloak|Cape de la claque en rideau|changé|
|[equipment-12-VmmlkU5qJHcTunXV.htm](equipment/equipment-12-VmmlkU5qJHcTunXV.htm)|Five-Feather Wreath (Major)|Couronne à cinq plumes majeur|changé|
|[equipment-12-XJZTTkI3ERnu6iFf.htm](equipment/equipment-12-XJZTTkI3ERnu6iFf.htm)|Flaming Star (Major)|Étoile enflammée majeure|changé|
|[equipment-12-YG6HRwfMVK3jdslZ.htm](equipment/equipment-12-YG6HRwfMVK3jdslZ.htm)|Perfect Droplet (Major)|Goutte parfaite majeure|changé|
|[equipment-12-Yguqipt5N29Bkz0d.htm](equipment/equipment-12-Yguqipt5N29Bkz0d.htm)|Bullhook (Greater)|Crochet de taureau supérieur|changé|
|[equipment-12-yI6L5ZHRxWpZmOP0.htm](equipment/equipment-12-yI6L5ZHRxWpZmOP0.htm)|Cloak of the False Foe|Cape du faux ennemi|changé|
|[equipment-12-ytGUz1nsPSsyuEsJ.htm](equipment/equipment-12-ytGUz1nsPSsyuEsJ.htm)|Entertainer's Lute (Major)|Luth du saltimbanque majeur|changé|
|[equipment-12-ZCWhnOmij4AFaytj.htm](equipment/equipment-12-ZCWhnOmij4AFaytj.htm)|Pallette of Masterstrokes|Palette de maître peintre|changé|
|[equipment-12-ZelCRLDI6M5IfjAI.htm](equipment/equipment-12-ZelCRLDI6M5IfjAI.htm)|Cloak of Elvenkind (Greater)|Cape elfique supérieure|changé|
|[equipment-12-zFdo0aBUw6cHEucT.htm](equipment/equipment-12-zFdo0aBUw6cHEucT.htm)|Spell Duelist's Siphon|Siphon du sort de duelliste|changé|
|[equipment-12-ZTlTXWcCV4FGgyOa.htm](equipment/equipment-12-ZTlTXWcCV4FGgyOa.htm)|Phantom Piano|Piano fantôme|changé|
|[equipment-13-1IeT2jIpBxxUo1uD.htm](equipment/equipment-13-1IeT2jIpBxxUo1uD.htm)|Slates of Distant Letters|Ardoises des lettres distantes|changé|
|[equipment-13-ap9Bn8MLUJa8AtGj.htm](equipment/equipment-13-ap9Bn8MLUJa8AtGj.htm)|Warding Statuette (Greater)|Statuette de protection supérieure|changé|
|[equipment-13-Ck5k13uTNqibLFJk.htm](equipment/equipment-13-Ck5k13uTNqibLFJk.htm)|Propulsive Boots|Bottes de rapidité|changé|
|[equipment-13-DCPsilr8wbPXxTUv.htm](equipment/equipment-13-DCPsilr8wbPXxTUv.htm)|Animated|Dansante (rune)|changé|
|[equipment-13-ds7j3D8IIyxWd2XI.htm](equipment/equipment-13-ds7j3D8IIyxWd2XI.htm)|Winged|Ailée (rune)|changé|
|[equipment-13-hg3IogR8ue2IWwgS.htm](equipment/equipment-13-hg3IogR8ue2IWwgS.htm)|Keen|Acérée (rune)|changé|
|[equipment-13-Jp9K5Q9t9ZiDGSaI.htm](equipment/equipment-13-Jp9K5Q9t9ZiDGSaI.htm)|Skarja's Heartstone|Cardioline de Skarja|changé|
|[equipment-13-lKMSAlp0ZoFUK4FV.htm](equipment/equipment-13-lKMSAlp0ZoFUK4FV.htm)|Eye of Fortune|Oeil de chance|changé|
|[equipment-13-mDIxIVMhbZ7voLhU.htm](equipment/equipment-13-mDIxIVMhbZ7voLhU.htm)|Clarity Goggles (Moderate)|Lunettes de clarté modérées|changé|
|[equipment-13-MM44urhBdG0x98bc.htm](equipment/equipment-13-MM44urhBdG0x98bc.htm)|Singing Stone|Pierre chantante|changé|
|[equipment-13-ONzSIJgJV27SHIMH.htm](equipment/equipment-13-ONzSIJgJV27SHIMH.htm)|Vigilant Eye (Major)|Oeil vigilant majeur|changé|
|[equipment-13-payq4TwkN2BRF6fs.htm](equipment/equipment-13-payq4TwkN2BRF6fs.htm)|Spell Reservoir|Stockage de sort (rune)|changé|
|[equipment-13-RH4e2ceNN7Jy5ioq.htm](equipment/equipment-13-RH4e2ceNN7Jy5ioq.htm)|Damaj's Gloves|Gants de Damaj|changé|
|[equipment-13-TAHcYiBzvg8F1AwY.htm](equipment/equipment-13-TAHcYiBzvg8F1AwY.htm)|Cube of Force|Cube de force|changé|
|[equipment-13-UlTQEI52DA1Z3dTc.htm](equipment/equipment-13-UlTQEI52DA1Z3dTc.htm)|Spiral Chimes|Carillon spirale|changé|
|[equipment-13-vBgElXswf6xBx4k9.htm](equipment/equipment-13-vBgElXswf6xBx4k9.htm)|Hellfire Boots|Bottes feu-d'enfer|changé|
|[equipment-13-WC0MDw9oLnAtLmh5.htm](equipment/equipment-13-WC0MDw9oLnAtLmh5.htm)|Desolation Locket (Greater)|Médaillon de la désolation supérieur|changé|
|[equipment-13-YoPUrPBjITTTgMKS.htm](equipment/equipment-13-YoPUrPBjITTTgMKS.htm)|Scope of Truth|Lunette de visée de vérité|changé|
|[equipment-14-0wcZzk6ZgvxRBsBx.htm](equipment/equipment-14-0wcZzk6ZgvxRBsBx.htm)|Communion Mat|Tapis de communion|changé|
|[equipment-14-31knfVD7lEd8BPrQ.htm](equipment/equipment-14-31knfVD7lEd8BPrQ.htm)|Boots of Bounding (Greater)|Bottes de saut supérieures|changé|
|[equipment-14-7CCGLWYzsRuodCMM.htm](equipment/equipment-14-7CCGLWYzsRuodCMM.htm)|Eye of the Unseen (Greater)|Oeil de l'invisible supérieur|changé|
|[equipment-14-8anSflDImoYUv2UK.htm](equipment/equipment-14-8anSflDImoYUv2UK.htm)|Illuminated Folio|Folio enluminé|changé|
|[equipment-14-CJtn848AL7Q0Lxf2.htm](equipment/equipment-14-CJtn848AL7Q0Lxf2.htm)|Soaring|Volante (rune)|changé|
|[equipment-14-gEenEoxLjL5z28rG.htm](equipment/equipment-14-gEenEoxLjL5z28rG.htm)|Rod of Negation|Sceptre de négation|changé|
|[equipment-14-ggAuloLn6sZpEgT9.htm](equipment/equipment-14-ggAuloLn6sZpEgT9.htm)|Veiled Figurehead (Greater)|Figure de proue voilée supérieure|changé|
|[equipment-14-GtSPUCDpIMUEq9B0.htm](equipment/equipment-14-GtSPUCDpIMUEq9B0.htm)|Jyoti's Feather (Greater)|Plume de jyoti supérieure|changé|
|[equipment-14-HZDkF6MlNQ6yYobD.htm](equipment/equipment-14-HZDkF6MlNQ6yYobD.htm)|Primeval Mistletoe (Greater)|Gui primitif supérieur|changé|
|[equipment-14-oCwTZg3JuwLSA7Yj.htm](equipment/equipment-14-oCwTZg3JuwLSA7Yj.htm)|Soaring Wings (Greater)|Ailes volantes supérieures|changé|
|[equipment-14-oVrVzML63VFvVfKk.htm](equipment/equipment-14-oVrVzML63VFvVfKk.htm)|Vitalizing (Greater)|Vitalisante supérieure (rune)|changé|
|[equipment-14-RM3SR1omNZfppZGl.htm](equipment/equipment-14-RM3SR1omNZfppZGl.htm)|Crystal Ball (Clear Quartz)|Boule de cristal en quartz translucide|changé|
|[equipment-14-X0pjLMXjW8RGr8pg.htm](equipment/equipment-14-X0pjLMXjW8RGr8pg.htm)|Fortune's Coin (Platinum)|Pièce porte-bonheur en platine|changé|
|[equipment-15-0Tu55QMhTXAijFvl.htm](equipment/equipment-15-0Tu55QMhTXAijFvl.htm)|Saurian Spike (Major)|Pointe saurienne majeure|changé|
|[equipment-15-3pKIEBtZBVmSmVPl.htm](equipment/equipment-15-3pKIEBtZBVmSmVPl.htm)|Earthglide Cloak|Robe de glisse-terre|changé|
|[equipment-15-4LnRc8lZjprPPQ0z.htm](equipment/equipment-15-4LnRc8lZjprPPQ0z.htm)|Oculus of Abaddon|Oculus d'Abaddon|changé|
|[equipment-15-7AdpzgqC9wCVyeoe.htm](equipment/equipment-15-7AdpzgqC9wCVyeoe.htm)|Wand of Toxic Blades (6th-level)|Baguettes des lames toxiques (rang 6)|changé|
|[equipment-15-9BXiGLvOFxDBmQrQ.htm](equipment/equipment-15-9BXiGLvOFxDBmQrQ.htm)|Phantasmal Doorknob (Major)|Poignée de porte imaginaire majeure|changé|
|[equipment-15-cIYjYb9e5ieQmHo7.htm](equipment/equipment-15-cIYjYb9e5ieQmHo7.htm)|Wand of Refracting Rays (7th-level)|Baguette des rayons réfractés (rang 7)|changé|
|[equipment-15-dABsOA9Qiy0nkGgV.htm](equipment/equipment-15-dABsOA9Qiy0nkGgV.htm)|True Name Amulet (Greater)|Amulette du nom véritable supérieur|changé|
|[equipment-15-EcdwnJMuYRqQ9uh7.htm](equipment/equipment-15-EcdwnJMuYRqQ9uh7.htm)|Bewitching Bloom (Amaranth)|Fleur envoûtante (Amarante)|changé|
|[equipment-15-hEnITJ2bJVPrqfGG.htm](equipment/equipment-15-hEnITJ2bJVPrqfGG.htm)|Lightweave Scarf (Greater)|Écharpe en tissu léger supérieure|changé|
|[equipment-15-lSpoAPC5VeGcSoDG.htm](equipment/equipment-15-lSpoAPC5VeGcSoDG.htm)|Wyrm Claw (Greater)|Griffe de guerre supérieure|changé|
|[equipment-15-o02lg3k1RBoFXVFV.htm](equipment/equipment-15-o02lg3k1RBoFXVFV.htm)|Antimagic|Antimagie (rune)|changé|
|[equipment-15-Q4o8pEdxSY0bnJQZ.htm](equipment/equipment-15-Q4o8pEdxSY0bnJQZ.htm)|Demilich Eye Gem|Oeil de Demi-liche|changé|
|[equipment-15-QbyF8jJ93SnN6NVL.htm](equipment/equipment-15-QbyF8jJ93SnN6NVL.htm)|Steamflight Pack|Module vol à vapeur|changé|
|[equipment-15-RnpzfRWsOW6zmAgN.htm](equipment/equipment-15-RnpzfRWsOW6zmAgN.htm)|Wondrous Figurine (Obsidian Steed)|Figurine merveilleuse (Destrier d'obsidienne)|changé|
|[equipment-15-RSZwUlCzUX7Nb4UA.htm](equipment/equipment-15-RSZwUlCzUX7Nb4UA.htm)|Flaming (Greater)|Enflammée supérieure (rune)|changé|
|[equipment-15-SemkRkQPVBljg9Id.htm](equipment/equipment-15-SemkRkQPVBljg9Id.htm)|Infernal Health|Santé d'enfer|changé|
|[equipment-15-Sexud7FdxIrg50vU.htm](equipment/equipment-15-Sexud7FdxIrg50vU.htm)|Frost (Greater)|Froid supérieur (rune)|changé|
|[equipment-15-TEa1oKZbwsOvC6TZ.htm](equipment/equipment-15-TEa1oKZbwsOvC6TZ.htm)|Shock (Greater)|Foudre supérieure (rune)|changé|
|[equipment-15-tzoN71erFxQ2HVLl.htm](equipment/equipment-15-tzoN71erFxQ2HVLl.htm)|Toshigami Blossom|Fleur de Toshigami|changé|
|[equipment-15-VDMYyVQUWJAjVyru.htm](equipment/equipment-15-VDMYyVQUWJAjVyru.htm)|Crystal Ball (Selenite)|Boule de cristal en sélénite|changé|
|[equipment-15-vQUIUAFOTOWj3ohh.htm](equipment/equipment-15-vQUIUAFOTOWj3ohh.htm)|Corrosive (Greater)|Corrosive supérieure (rune)|changé|
|[equipment-15-z5u7z3zBHZcCxfyX.htm](equipment/equipment-15-z5u7z3zBHZcCxfyX.htm)|Busine of Divine Reinforcements|Busine de renforts divins|changé|
|[equipment-16-2smAtaNAi2fIVTLF.htm](equipment/equipment-16-2smAtaNAi2fIVTLF.htm)|Fiddle of the Maestro|Violon du maestro|changé|
|[equipment-16-6epYSje4gOgYPvK0.htm](equipment/equipment-16-6epYSje4gOgYPvK0.htm)|Dinosaur Boots (Greater)|Bottes de dinosaure supérieur|changé|
|[equipment-16-CjfBdn0fOIarWzBc.htm](equipment/equipment-16-CjfBdn0fOIarWzBc.htm)|Wand of Slaying (7th-Level Spell)|Baguette tueuse (rang 7)|changé|
|[equipment-16-KnZL0xPWDzQx9vWQ.htm](equipment/equipment-16-KnZL0xPWDzQx9vWQ.htm)|Quickstrike|Rapide (rune)|changé|
|[equipment-16-OpEFPyX1jwuV7ljp.htm](equipment/equipment-16-OpEFPyX1jwuV7ljp.htm)|Wand of Clinging Rime (7th-level)|Baguette de glace collante (rang 7)|changé|
|[equipment-16-oqZszBqqanONR0ng.htm](equipment/equipment-16-oqZszBqqanONR0ng.htm)|Grimoire of Unknown Necessities|Grimoire des nécessités inconnues|changé|
|[equipment-16-S0IshWO7Vx29PKaq.htm](equipment/equipment-16-S0IshWO7Vx29PKaq.htm)|Crystal Ball (Moonstone)|Boule de cristal en pierre de lune|changé|
|[equipment-16-y2nb4D6iQOoc5LEX.htm](equipment/equipment-16-y2nb4D6iQOoc5LEX.htm)|Miogimo's Mask|Masque de Miogimo|changé|
|[equipment-16-y4dGIbK5wGUJTufR.htm](equipment/equipment-16-y4dGIbK5wGUJTufR.htm)|Jyoti's Feather (Major)|Plume de jyoti majeure|changé|
|[equipment-16-YSXwrbnxc8OX0jBm.htm](equipment/equipment-16-YSXwrbnxc8OX0jBm.htm)|Cipher of Elemental Planes|Décodeur des Plans élémentaires|changé|
|[equipment-17-0yeM77XLNrB0a0LF.htm](equipment/equipment-17-0yeM77XLNrB0a0LF.htm)|Circlet of Persuasion|Serre-tête de persuasion|changé|
|[equipment-17-2FjdEflsVldnuebM.htm](equipment/equipment-17-2FjdEflsVldnuebM.htm)|Shadow (Major)|Ombre majeure (rune)|changé|
|[equipment-17-6xaxxKfvXED6LfIY.htm](equipment/equipment-17-6xaxxKfvXED6LfIY.htm)|Vorpal|Vorpale (rune)|changé|
|[equipment-17-AynKTlER5ZbfF62f.htm](equipment/equipment-17-AynKTlER5ZbfF62f.htm)|Humbug Pocket (Greater)|Poche de falsification supérieure|changé|
|[equipment-17-b9YHyjOL4sg7tjI4.htm](equipment/equipment-17-b9YHyjOL4sg7tjI4.htm)|Messenger's Ring (Greater)|Anneau du messager supérieur|changé|
|[equipment-17-ByRyBRjNBcK5rwQ9.htm](equipment/equipment-17-ByRyBRjNBcK5rwQ9.htm)|Crystal Ball (Peridot)|Boule de cristal en péridot|changé|
|[equipment-17-C3SwBSW8XzzBPDVT.htm](equipment/equipment-17-C3SwBSW8XzzBPDVT.htm)|Cloak of Swiftness|Cape de rapidité|changé|
|[equipment-17-dxRKFkFeCDQLLEyT.htm](equipment/equipment-17-dxRKFkFeCDQLLEyT.htm)|Armbands of the Gorgon|Brassards de la gorgone|changé|
|[equipment-17-h7OCAvvnUnCIM9Aj.htm](equipment/equipment-17-h7OCAvvnUnCIM9Aj.htm)|Troubadour's Cap|Casquette de troubadour|changé|
|[equipment-17-hjfoSyfsGSTLpPMr.htm](equipment/equipment-17-hjfoSyfsGSTLpPMr.htm)|Headwrap of Wisdom|Bandeau d'inspiration|changé|
|[equipment-17-JnNZjAk32269e8VO.htm](equipment/equipment-17-JnNZjAk32269e8VO.htm)|Pilferer's Gloves|Gants du chapardeur|changé|
|[equipment-17-K2rMmiBlzcysNuj6.htm](equipment/equipment-17-K2rMmiBlzcysNuj6.htm)|Belt of Long Life|Ceinture de régénération|changé|
|[equipment-17-KFfM3Y8SbhdxpbQI.htm](equipment/equipment-17-KFfM3Y8SbhdxpbQI.htm)|Robe of Eyes|Robe de vision totale|changé|
|[equipment-17-m7oTstzUfkFfaaLc.htm](equipment/equipment-17-m7oTstzUfkFfaaLc.htm)|Wand of Toxic Blades (7th-level)|Baguettes des lames toxiques (rang 7)|changé|
|[equipment-17-mNgScZVAq037JuNb.htm](equipment/equipment-17-mNgScZVAq037JuNb.htm)|Amulet of the Third Eye|Amulette du troisième oeil|changé|
|[equipment-17-n6ZmkKhxd0iuS1RG.htm](equipment/equipment-17-n6ZmkKhxd0iuS1RG.htm)|Bangles of Crowns|Bracelets des couronnes|changé|
|[equipment-17-pDw2wi0znVb8Dysg.htm](equipment/equipment-17-pDw2wi0znVb8Dysg.htm)|Dread Blindfold|Bandeau d'effroi|changé|
|[equipment-17-q70WXJO1rswduHuT.htm](equipment/equipment-17-q70WXJO1rswduHuT.htm)|Ethereal|Éthérée (rune)|changé|
|[equipment-17-tajXzwAgjUBZLD6b.htm](equipment/equipment-17-tajXzwAgjUBZLD6b.htm)|Judgment Thurible (Greater)|Encensoir du jugement supérieur|changé|
|[equipment-17-UvABdSnNZ9gr2IkF.htm](equipment/equipment-17-UvABdSnNZ9gr2IkF.htm)|Cauldron of Nightmares|Chaudron des cauchemars|changé|
|[equipment-17-VVymhIF6UBThdHP9.htm](equipment/equipment-17-VVymhIF6UBThdHP9.htm)|Artificer Spectacles|Lunettes d'artificier|changé|
|[equipment-17-WOiCJSS2MicKCMVs.htm](equipment/equipment-17-WOiCJSS2MicKCMVs.htm)|Bracers of Strength|Bracelets de Force|changé|
|[equipment-17-WUICjaC9LYnLRIAE.htm](equipment/equipment-17-WUICjaC9LYnLRIAE.htm)|Phantom Shroud|Linceul du fantôme|changé|
|[equipment-17-zJKEDSxn2lhPamKS.htm](equipment/equipment-17-zJKEDSxn2lhPamKS.htm)|Clockwork Helm|Heaume mécanique|changé|
|[equipment-17-ZxwnnmMw9z1KG98W.htm](equipment/equipment-17-ZxwnnmMw9z1KG98W.htm)|Witchwyrd Beacon|Balise de sorcewyrd|changé|
|[equipment-18-6ELjUFY0sEJ7nZlZ.htm](equipment/equipment-18-6ELjUFY0sEJ7nZlZ.htm)|Armor Potency (+3)|Puissance d'armure +3 (rune)|changé|
|[equipment-18-aQEQkwuAabZG8wY1.htm](equipment/equipment-18-aQEQkwuAabZG8wY1.htm)|Sage's Lash|Ceinture du sage|changé|
|[equipment-18-at6rKBw85IkdbmOO.htm](equipment/equipment-18-at6rKBw85IkdbmOO.htm)|Sandals of the Stag|Sandales du cerf|changé|
|[equipment-18-cVxHaDAouFhm0bXP.htm](equipment/equipment-18-cVxHaDAouFhm0bXP.htm)|Emberheart|Coeur de braise|changé|
|[equipment-18-d8w28UjKHALM2GNu.htm](equipment/equipment-18-d8w28UjKHALM2GNu.htm)|Taljjae's Mask (The Hermit)|Masque de Taljjae (l'ermite)|changé|
|[equipment-18-FffMnXbwNHczb6bp.htm](equipment/equipment-18-FffMnXbwNHczb6bp.htm)|Maw of Hungry Shadows|Gueule des ombres affamées|changé|
|[equipment-18-fw1lODFkJJk3umz0.htm](equipment/equipment-18-fw1lODFkJJk3umz0.htm)|Cape of the Open Sky|Cape du ciel ouvert|changé|
|[equipment-18-kY41VIXUSEJYEznp.htm](equipment/equipment-18-kY41VIXUSEJYEznp.htm)|Anchoring (Greater)|Ancrage supérieur (rune)|changé|
|[equipment-18-LnXYSaLxWMyAT3Ef.htm](equipment/equipment-18-LnXYSaLxWMyAT3Ef.htm)|Wand of Slaying (8th-Level Spell)|Baguette tueuse (rang 8)|changé|
|[equipment-18-ma4BlTSjaMHrQmoz.htm](equipment/equipment-18-ma4BlTSjaMHrQmoz.htm)|Taljjae's Mask (The Grandmother)|Masque de Taljjae (la grand-mère)|changé|
|[equipment-18-n8MonEa4ZBdvEovc.htm](equipment/equipment-18-n8MonEa4ZBdvEovc.htm)|Brilliant (Greater)|Brillante supérieure (rune)|changé|
|[equipment-18-nbBtszla32wRC1bp.htm](equipment/equipment-18-nbBtszla32wRC1bp.htm)|Taljjae's Mask (The Wanderer)|Masque de Taljjae (le vagabond)|changé|
|[equipment-18-q11aZe8Gc09QkP0y.htm](equipment/equipment-18-q11aZe8Gc09QkP0y.htm)|Purloining Cloak|Cape du détrousseur|changé|
|[equipment-18-QZaOQ8luuxWXpFqJ.htm](equipment/equipment-18-QZaOQ8luuxWXpFqJ.htm)|Genius Diadem|Diadème du génie|changé|
|[equipment-18-uHUQnRGSKy655bTt.htm](equipment/equipment-18-uHUQnRGSKy655bTt.htm)|Spectacles of Piercing Sight|Lunettes de vision perçante|changé|
|[equipment-18-ujWnpVMkbTljMGN9.htm](equipment/equipment-18-ujWnpVMkbTljMGN9.htm)|Fortification (Greater)|Fortification supérieure (rune)|changé|
|[equipment-18-Uk342C6qpSkdjHBb.htm](equipment/equipment-18-Uk342C6qpSkdjHBb.htm)|Lightweave Scarf (Major)|Écharpe en tissu léger majeure|changé|
|[equipment-18-uT8HoQ3BKI5La8Fu.htm](equipment/equipment-18-uT8HoQ3BKI5La8Fu.htm)|Mirror Goggles (Greater)|Lunettes miroir supérieures|changé|
|[equipment-18-utRZnCD2XI9Q1dWE.htm](equipment/equipment-18-utRZnCD2XI9Q1dWE.htm)|Ghostcaller's Planchette (Greater)|Planchette de l'invocateur de fantôme supérieure|changé|
|[equipment-18-V9iVPhIk980GT6A2.htm](equipment/equipment-18-V9iVPhIk980GT6A2.htm)|Inexplicable Apparatus|Appareil inexplicable|changé|
|[equipment-18-v9tI40skGswMQTVN.htm](equipment/equipment-18-v9tI40skGswMQTVN.htm)|Clockwork Cloak|Cape mécanique|changé|
|[equipment-18-vZaXDOrp5Faxw5fS.htm](equipment/equipment-18-vZaXDOrp5Faxw5fS.htm)|Possibility Tome|Livre des possibilités|changé|
|[equipment-18-Wkm2VYPsfGjWBtQe.htm](equipment/equipment-18-Wkm2VYPsfGjWBtQe.htm)|Ring of Maniacal Devices (Greater)|Anneau d'objets déments supérieur|changé|
|[equipment-18-XORmdC62imVhz1BD.htm](equipment/equipment-18-XORmdC62imVhz1BD.htm)|Archivist's Gaze|Regard d'archiviste|changé|
|[equipment-18-Z8o1V947lwkhPhzI.htm](equipment/equipment-18-Z8o1V947lwkhPhzI.htm)|Wand of Clinging Rime (8th-level)|Baguette de glace collante (rang 8)|changé|
|[equipment-19-0NZuvpCVr21WcQtH.htm](equipment/equipment-19-0NZuvpCVr21WcQtH.htm)|Locket of Sealed Nightmares|Médaillon des cauchemars enfermés|changé|
|[equipment-19-3HR6rJT9NrIRUwe2.htm](equipment/equipment-19-3HR6rJT9NrIRUwe2.htm)|Shadowmist Cape|Cape brumombre|changé|
|[equipment-19-3p4fk9pbwDeue1NG.htm](equipment/equipment-19-3p4fk9pbwDeue1NG.htm)|Desolation Locket (Major)|Médaillon de la désolation majeur|changé|
|[equipment-19-b2shhButUcNimET9.htm](equipment/equipment-19-b2shhButUcNimET9.htm)|Mask of the Banshee (Greater)|Masque de la banshie supérieure|changé|
|[equipment-19-fbhRZskdWPrh4ML8.htm](equipment/equipment-19-fbhRZskdWPrh4ML8.htm)|Clarity Goggles (Greater)|Lunettes de clarté supérieures|changé|
|[equipment-19-ISsagQkLvsgpoLxP.htm](equipment/equipment-19-ISsagQkLvsgpoLxP.htm)|Ivory Baton (Greater)|Bâton d'ivoire supérieur|changé|
|[equipment-19-pIy2L56i70OUd2HZ.htm](equipment/equipment-19-pIy2L56i70OUd2HZ.htm)|Soaring Wings (Major)|Ailes volantes majeures|changé|
|[equipment-19-s9ciKYFPOepnHpxG.htm](equipment/equipment-19-s9ciKYFPOepnHpxG.htm)|Wand of Toxic Blades (8th-level)|Baguettes des lames toxiques (rang 8)|changé|
|[equipment-19-WKdI4LbwgcNHhMdp.htm](equipment/equipment-19-WKdI4LbwgcNHhMdp.htm)|Crystal Ball (Obsidian)|Boule de cristal en obsidienne|changé|
|[equipment-19-XazXltJ0tsYy1xXQ.htm](equipment/equipment-19-XazXltJ0tsYy1xXQ.htm)|Brightbloom Posy (Major)|Bouquet de fleurs lumineuses majeur|changé|
|[equipment-19-Z7xfDpdf1qwkJR94.htm](equipment/equipment-19-Z7xfDpdf1qwkJR94.htm)|Wand of Thundering Echoes (8th-Level Spell)|Baguette des échos tonitruants (rang 8)|changé|
|[equipment-19-Ztb4xv4UGZbF32TE.htm](equipment/equipment-19-Ztb4xv4UGZbF32TE.htm)|Winged (Greater)|Ailée supérieure (rune)|changé|
|[equipment-20-04GSFcLNlw1w2lAY.htm](equipment/equipment-20-04GSFcLNlw1w2lAY.htm)|The Cyclone|Le Cyclone|changé|
|[equipment-20-1I90sDJk6SFokiyl.htm](equipment/equipment-20-1I90sDJk6SFokiyl.htm)|The Beating|La raclée|changé|
|[equipment-20-1JHPSOUD7Ij75OAf.htm](equipment/equipment-20-1JHPSOUD7Ij75OAf.htm)|Vial of the Immortal Wellspring|Fiole de la Source d'immortalité|changé|
|[equipment-20-1qlJqqsVhocRLtS5.htm](equipment/equipment-20-1qlJqqsVhocRLtS5.htm)|The Peacock|Le Paon|changé|
|[equipment-20-2nKh3kUMuq8I9rnm.htm](equipment/equipment-20-2nKh3kUMuq8I9rnm.htm)|The Queen Mother|La Reine mère|changé|
|[equipment-20-4PtxcrCPnBhhYmg4.htm](equipment/equipment-20-4PtxcrCPnBhhYmg4.htm)|Judgment Thurible (Major)|Encensoir du jugement majeur|changé|
|[equipment-20-85hapkvhXc6Oa8XC.htm](equipment/equipment-20-85hapkvhXc6Oa8XC.htm)|The Tangled Briar|Le Roncier|changé|
|[equipment-20-cCnpBUjaGSNCT3JK.htm](equipment/equipment-20-cCnpBUjaGSNCT3JK.htm)|The Silent Hag|La Guenaude silencieuse|changé|
|[equipment-20-cFJUxi5kCcBV6lz1.htm](equipment/equipment-20-cFJUxi5kCcBV6lz1.htm)|Dragon's Breath (9th Level Spell)|Souffle de dragon de sort de rang 9 (rune)|changé|
|[equipment-20-ckJkldBWN8n1Hq2a.htm](equipment/equipment-20-ckJkldBWN8n1Hq2a.htm)|Lesser Cube of Nex|Cube de Nex inférieur|changé|
|[equipment-20-D9OGy69XaLBwjdd1.htm](equipment/equipment-20-D9OGy69XaLBwjdd1.htm)|True Name Amulet (Major)|Amulette du nom véritable majeur|changé|
|[equipment-20-dAPTUxg2NZBwzQ9T.htm](equipment/equipment-20-dAPTUxg2NZBwzQ9T.htm)|Laurel of the Empath|Lauriers de l'empathe|changé|
|[equipment-20-dB5oALF9UzMdE0MD.htm](equipment/equipment-20-dB5oALF9UzMdE0MD.htm)|The Mountain Man|L'Homme montagne|changé|
|[equipment-20-DKQrQkx13z6yiUNp.htm](equipment/equipment-20-DKQrQkx13z6yiUNp.htm)|The Liar|Le Menteur|changé|
|[equipment-20-e1w2q9r309N5gZ7E.htm](equipment/equipment-20-e1w2q9r309N5gZ7E.htm)|The Sickness|La Maladie|changé|
|[equipment-20-eTerYHAbXk9qwpgU.htm](equipment/equipment-20-eTerYHAbXk9qwpgU.htm)|The Owl|Le Hibou|changé|
|[equipment-20-eYhUB8qyBKsmJDDf.htm](equipment/equipment-20-eYhUB8qyBKsmJDDf.htm)|The Joke|La Blague|changé|
|[equipment-20-F2uej7timkyYGg83.htm](equipment/equipment-20-F2uej7timkyYGg83.htm)|The Lens of the Outreaching Eye|La loupe de l'Oeil hors de portée|changé|
|[equipment-20-gbDVj8XRLN9OVVjL.htm](equipment/equipment-20-gbDVj8XRLN9OVVjL.htm)|Wardrobe Stone (Greater)|Pierre de garde-robe supérieur|changé|
|[equipment-20-gswNvLPc2D0n0AOB.htm](equipment/equipment-20-gswNvLPc2D0n0AOB.htm)|Phoenix Necklace|Collier du phénix|changé|
|[equipment-20-gufc2pmaYn9jzFVL.htm](equipment/equipment-20-gufc2pmaYn9jzFVL.htm)|Unending Youth|Jeunesse éternelle|changé|
|[equipment-20-IfOwz6UoG7pnfbur.htm](equipment/equipment-20-IfOwz6UoG7pnfbur.htm)|The Fiend|Le Fiélon|changé|
|[equipment-20-IVGLVPxVorpTM8GL.htm](equipment/equipment-20-IVGLVPxVorpTM8GL.htm)|The Tyrant|Le Tyran|changé|
|[equipment-20-Ivqd84dkWA8DW8YJ.htm](equipment/equipment-20-Ivqd84dkWA8DW8YJ.htm)|Ring of Spell Turning|Anneau de renvoi de sorts|changé|
|[equipment-20-J0xCt39KNqUunhEr.htm](equipment/equipment-20-J0xCt39KNqUunhEr.htm)|The Bear|L'Ours|changé|
|[equipment-20-kLmUKkch8meabELi.htm](equipment/equipment-20-kLmUKkch8meabELi.htm)|The Inquisitor|L'Inquisiteur|changé|
|[equipment-20-mMZWdoHvP2yYJzrR.htm](equipment/equipment-20-mMZWdoHvP2yYJzrR.htm)|Wand of Slaying (9th-Level Spell)|Baguette tueuse (rang 9)|changé|
|[equipment-20-MXDA5rDemvT1O9vY.htm](equipment/equipment-20-MXDA5rDemvT1O9vY.htm)|The Betrayal|La Trahison|changé|
|[equipment-20-nJSlIxnU1s9H83NK.htm](equipment/equipment-20-nJSlIxnU1s9H83NK.htm)|The Keep|Le Fort|changé|
|[equipment-20-oelHQXS1g7Xk7xKo.htm](equipment/equipment-20-oelHQXS1g7Xk7xKo.htm)|The Hidden Truth|La Vérité cachée|changé|
|[equipment-20-ogE04B74e90cyCUz.htm](equipment/equipment-20-ogE04B74e90cyCUz.htm)|The Forge|La Forge|changé|
|[equipment-20-P112tFaNhgJrBhmu.htm](equipment/equipment-20-P112tFaNhgJrBhmu.htm)|Wand of Clinging Rime (9th-level)|Baguette de glace collante (rang 9)|changé|
|[equipment-20-Qh65GD5oXOhO5y3k.htm](equipment/equipment-20-Qh65GD5oXOhO5y3k.htm)|The Theater|Le Théatre|changé|
|[equipment-20-QWiXrqhSCkvdHbsi.htm](equipment/equipment-20-QWiXrqhSCkvdHbsi.htm)|Monkey's Paw|Main du singe|changé|
|[equipment-20-sKyJDfHdKacfbNOG.htm](equipment/equipment-20-sKyJDfHdKacfbNOG.htm)|Whisper of the First Lie|Murmure du mensonge originel|changé|
|[equipment-20-v8Dx5ABBpPFafIgy.htm](equipment/equipment-20-v8Dx5ABBpPFafIgy.htm)|Wand of Hybrid Form (9th-level)|Baguette de forme hybride (rang 9)|changé|
|[equipment-20-vhhjN70J1j3RfECJ.htm](equipment/equipment-20-vhhjN70J1j3RfECJ.htm)|Silhouette Cloak|Cape silhouette|changé|
|[equipment-20-W2yfqo9Oy8zyoOrA.htm](equipment/equipment-20-W2yfqo9Oy8zyoOrA.htm)|The Rakshasa|Le Rakshasa|changé|
|[equipment-20-X28YoZkN6UJYYYYC.htm](equipment/equipment-20-X28YoZkN6UJYYYYC.htm)|The Courtesan|Le Courtisan|changé|
|[equipment-20-yHaZJDX7xe8gPdL6.htm](equipment/equipment-20-yHaZJDX7xe8gPdL6.htm)|The Locksmith|Le Serrurier|changé|
|[equipment-20-YiitcoNxawdhTO1e.htm](equipment/equipment-20-YiitcoNxawdhTO1e.htm)|Void Mirror|Miroir du néant|changé|
|[equipment-20-z5EYyT8CMZkcj9qW.htm](equipment/equipment-20-z5EYyT8CMZkcj9qW.htm)|The Cricket|Le Criquet|changé|
|[equipment-21-9SmQvhTWvk7kuvJl.htm](equipment/equipment-21-9SmQvhTWvk7kuvJl.htm)|Forgotten Signet|Chevalière oubliée|changé|
|[equipment-21-CGcNXv33k83Gr5lf.htm](equipment/equipment-21-CGcNXv33k83Gr5lf.htm)|Halcyon Heart|Coeur syncrétique|changé|
|[equipment-22-BSIdE57Zuvv5iX93.htm](equipment/equipment-22-BSIdE57Zuvv5iX93.htm)|Perfected Robes|Robes de perfectionnement|changé|
|[equipment-23-6o6zvitwlNHNaNJM.htm](equipment/equipment-23-6o6zvitwlNHNaNJM.htm)|Shot of the First Vault|Projectile du premier coffre|changé|
|[equipment-23-8q9ylEH525g6XqFC.htm](equipment/equipment-23-8q9ylEH525g6XqFC.htm)|Primordial Flame|Flamme primordiale|changé|
|[equipment-23-Ub1GoxOzuTR1l1r4.htm](equipment/equipment-23-Ub1GoxOzuTR1l1r4.htm)|Redsand Hourglass|Sablier de sable rouge|changé|
|[equipment-24-jpQcKMmP1I5674P7.htm](equipment/equipment-24-jpQcKMmP1I5674P7.htm)|Forgefather's Seal|Sceau du père des forges|changé|
|[equipment-24-oPZ2s4KmwaNXiUcc.htm](equipment/equipment-24-oPZ2s4KmwaNXiUcc.htm)|Starfaring Cloak|Cape du voyage stellaire|changé|
|[equipment-25-1kLz3leyfccLcAMK.htm](equipment/equipment-25-1kLz3leyfccLcAMK.htm)|Cube of Nex|Cube de Nex|changé|
|[equipment-25-2VpuNz8EyXpvUf7P.htm](equipment/equipment-25-2VpuNz8EyXpvUf7P.htm)|Tears of the Last Azlanti|Larmes du dernier Azlant|changé|
|[equipment-25-D836jcTlNR4PHgXS.htm](equipment/equipment-25-D836jcTlNR4PHgXS.htm)|Radiant Spark|Étincelle radieuse|changé|
|[equipment-25-KEICt6Tusa3JdTE8.htm](equipment/equipment-25-KEICt6Tusa3JdTE8.htm)|Elder Sign|Sceau des anciens|changé|
|[equipment-25-L8OButuVM3PFxgrZ.htm](equipment/equipment-25-L8OButuVM3PFxgrZ.htm)|Orb of Dragonkind|Orbe des dragons|changé|
|[equipment-25-QPWWO89YR0uZ881r.htm](equipment/equipment-25-QPWWO89YR0uZ881r.htm)|The Deck of Destiny|Le Jeu du Destin|changé|
|[equipment-25-Ywt7p5Fyx18lK8km.htm](equipment/equipment-25-Ywt7p5Fyx18lK8km.htm)|Mirror of Sorshen|Miroir de Sorshen|changé|
|[equipment-26-SHzZSENlkz3Qy46j.htm](equipment/equipment-26-SHzZSENlkz3Qy46j.htm)|Horns of Naraga|Cornes de Naraga|changé|
|[equipment-28-mH3LImCgJAkfKAA3.htm](equipment/equipment-28-mH3LImCgJAkfKAA3.htm)|Philosopher's Extractor|Distillateur philosophal|changé|
|[kit-7E3sWotW56biNttA.htm](equipment/kit-7E3sWotW56biNttA.htm)|Class Kit (Ranger)|Panoplie de classe (Rôdeur)|changé|
|[shield-00-1k3AsSW7lpU0kEpY.htm](equipment/shield-00-1k3AsSW7lpU0kEpY.htm)|Buckler|Targe|changé|
|[shield-00-ezVp13Uw8cWW08Da.htm](equipment/shield-00-ezVp13Uw8cWW08Da.htm)|Wooden Shield|Bouclier en bois|changé|
|[shield-00-ltundBNFAnP7bgPr.htm](equipment/shield-00-ltundBNFAnP7bgPr.htm)|Tower Shield|Pavois|changé|
|[shield-00-Yr9yCuJiAlFh3QEB.htm](equipment/shield-00-Yr9yCuJiAlFh3QEB.htm)|Steel Shield|Bouclier en acier|changé|
|[shield-02-IxuDS3POB6EH8TVN.htm](equipment/shield-02-IxuDS3POB6EH8TVN.htm)|Glamorous Buckler|Targe étincelante|changé|
|[shield-04-ScU1DrBKn0ONHnq4.htm](equipment/shield-04-ScU1DrBKn0ONHnq4.htm)|Pillow Shield|Bouclier oreiller|changé|
|[shield-05-MK3Fq0BV2uTspvXn.htm](equipment/shield-05-MK3Fq0BV2uTspvXn.htm)|Burr Shield|Bouclier de ronces|changé|
|[shield-05-slQRh4FVDzP8h1wj.htm](equipment/shield-05-slQRh4FVDzP8h1wj.htm)|Exploding Shield|Bouclier explosif|changé|
|[shield-05-WDmuP3H7jNqobraf.htm](equipment/shield-05-WDmuP3H7jNqobraf.htm)|Helmsman's Recourse|Recours du timonier|changé|
|[shield-07-19T3MHwhB6Wk4AjV.htm](equipment/shield-07-19T3MHwhB6Wk4AjV.htm)|Helmsman's Recourse (Greater)|Recours du timonier supérieur|changé|
|[shield-07-EBcZ7ftWs8lVW7Fj.htm](equipment/shield-07-EBcZ7ftWs8lVW7Fj.htm)|Staff-Storing Shield|Bouclier de stockage de bâton|changé|
|[shield-08-rh4T08EsddrwfHcs.htm](equipment/shield-08-rh4T08EsddrwfHcs.htm)|Duelist's Beacon|Phare du duelliste|changé|
|[shield-09-Q8BscHUFiM1a86PO.htm](equipment/shield-09-Q8BscHUFiM1a86PO.htm)|Dragonslayer's Shield|Bouclier du pourfendeur de dragons|changé|
|[shield-10-M8QYa10fHOyViC5V.htm](equipment/shield-10-M8QYa10fHOyViC5V.htm)|Shining Shield|Bouclier étincelant|changé|
|[shield-10-NoTUhKkiTY5BQU5T.htm](equipment/shield-10-NoTUhKkiTY5BQU5T.htm)|Forge Warden|Gardien de la forge|changé|
|[shield-10-TPiF3YBADh10JDPV.htm](equipment/shield-10-TPiF3YBADh10JDPV.htm)|Amaranthine Pavise|Pavois d'amarante|changé|
|[shield-11-aBAU0sTkFLuTxO3X.htm](equipment/shield-11-aBAU0sTkFLuTxO3X.htm)|Staff-Storing Shield (Greater)|Bouclier de stockage de bâton supérieur|changé|
|[shield-11-hY6Qn7HwKxYaGk2S.htm](equipment/shield-11-hY6Qn7HwKxYaGk2S.htm)|Guardian Shield|Bouclier gardien|changé|
|[shield-11-kSjfrC31qFi2qovr.htm](equipment/shield-11-kSjfrC31qFi2qovr.htm)|Helmsman's Recourse (Major)|Recours du timonier majeur|changé|
|[shield-11-ydNocKN6afzIyVh6.htm](equipment/shield-11-ydNocKN6afzIyVh6.htm)|Floating Shield|Bouclier flottant|changé|
|[shield-12-63k6WWZstoY33LsH.htm](equipment/shield-12-63k6WWZstoY33LsH.htm)|Rampart Shield|Bouclier rempart|changé|
|[shield-13-nzQKwdUxzmmwWtzT.htm](equipment/shield-13-nzQKwdUxzmmwWtzT.htm)|Medusa's Scream|Bouclier du cri de la méduse|changé|
|[shield-15-nhCHLI5SnJXF3fQE.htm](equipment/shield-15-nhCHLI5SnJXF3fQE.htm)|Staff-Storing Shield (Major)|Bouclier de stockage de bâton majeur|changé|
|[shield-15-SJaFNXiHGILhwEMk.htm](equipment/shield-15-SJaFNXiHGILhwEMk.htm)|Silkspinner's Shield|Bouclier de la fileuse de soie|changé|
|[shield-16-TyW1XOWYMM2xHGaI.htm](equipment/shield-16-TyW1XOWYMM2xHGaI.htm)|Medusa's Scream (Greater)|Bouclier du cri de la méduse supérieur|changé|
|[shield-16-uAs0hUUgROrndceD.htm](equipment/shield-16-uAs0hUUgROrndceD.htm)|Floating Shield (Greater)|Bouclier flottant supérieur|changé|
|[shield-17-VioxW97SUADl3zTt.htm](equipment/shield-17-VioxW97SUADl3zTt.htm)|Cursebreak Bulwark|Rempart brise-malédiction|changé|
|[shield-18-La9qYc5NHsg423Jb.htm](equipment/shield-18-La9qYc5NHsg423Jb.htm)|Reflecting Shield|Targe réfléchissante|changé|
|[shield-18-SUbYk6B1iPoGyyjh.htm](equipment/shield-18-SUbYk6B1iPoGyyjh.htm)|Indestructible Shield|Bouclier indestructible|changé|
|[shield-19-f6antZJS94JyijFl.htm](equipment/shield-19-f6antZJS94JyijFl.htm)|Kraken's Guard|Gardien kraken|changé|
|[shield-20-xAOVPVH6cX3iVFlk.htm](equipment/shield-20-xAOVPVH6cX3iVFlk.htm)|Staff-Storing Shield (True)|Bouclier de stockage de bâton ultime|changé|
|[shield-21-XkIfwSTy0PQL3RPJ.htm](equipment/shield-21-XkIfwSTy0PQL3RPJ.htm)|Scale of Igroon|Écaille d'Igroon|changé|
|[treasure-00-BlqDXtRZcevCYRNz.htm](equipment/treasure-00-BlqDXtRZcevCYRNz.htm)|Jeweled dawnsilver crown|Couronne d'aubargent avec des joyaux|changé|
|[treasure-00-PTkTPYtxrPwgCzeq.htm](equipment/treasure-00-PTkTPYtxrPwgCzeq.htm)|Duskwood violin by a legend|Violon en bois crépusculaire d'un facteur légendaire|changé|
|[weapon-00-qRqGIxcadjjxcij3.htm](equipment/weapon-00-qRqGIxcadjjxcij3.htm)|Mithral Tree|Arbre d'aubargent|changé|
|[weapon-01-8JnOYyTdatqRnAV4.htm](equipment/weapon-01-8JnOYyTdatqRnAV4.htm)|Necrotic Bomb (Lesser)|Bombe nécrotique inférieure|changé|
|[weapon-01-ds0OdA989ZZw9km1.htm](equipment/weapon-01-ds0OdA989ZZw9km1.htm)|Dread Ampoule (Lesser)|Ampoule d'effroi inférieure|changé|
|[weapon-01-EGhUorZhB7nV73Ev.htm](equipment/weapon-01-EGhUorZhB7nV73Ev.htm)|Unholy Water|Eau impie|changé|
|[weapon-01-HtcjYkGS8vuC0e7T.htm](equipment/weapon-01-HtcjYkGS8vuC0e7T.htm)|Redpitch Bomb (Lesser)|Bombe poix-rouge inférieure|changé|
|[weapon-01-l3IOo6AiQj7pxPhB.htm](equipment/weapon-01-l3IOo6AiQj7pxPhB.htm)|Dragon's Crest|Cimier de dragon|changé|
|[weapon-01-LNd5u19GqC51ngby.htm](equipment/weapon-01-LNd5u19GqC51ngby.htm)|Ghost Charge (Lesser)|Charge fantôme inférieure|changé|
|[weapon-01-M1k5QQc1qQLxzyCK.htm](equipment/weapon-01-M1k5QQc1qQLxzyCK.htm)|Acid Flask (Lesser)|Fiole d'acide inférieure|changé|
|[weapon-01-WNWnpAsmFoFUlQLK.htm](equipment/weapon-01-WNWnpAsmFoFUlQLK.htm)|Alignment Ampoule (Lesser)|Ampoule d'alignement inférieure|changé|
|[weapon-01-z9T4c1hXwOotsMCp.htm](equipment/weapon-01-z9T4c1hXwOotsMCp.htm)|Holy Water|Eau bénite|changé|
|[weapon-02-iGT4UeQq3YFcNpEb.htm](equipment/weapon-02-iGT4UeQq3YFcNpEb.htm)|Bottled Sunlight (Lesser)|Soleil en bouteille inférieur|changé|
|[weapon-03-EuTZxxwdVeN6Xg3A.htm](equipment/weapon-03-EuTZxxwdVeN6Xg3A.htm)|Storm Hammer|Marteau des tempêtes|changé|
|[weapon-03-IvFEJqp2MUew65nQ.htm](equipment/weapon-03-IvFEJqp2MUew65nQ.htm)|Dread Ampoule (Moderate)|Ampoule d'effroi modérée|changé|
|[weapon-03-K7vNtVcHUKB9wks1.htm](equipment/weapon-03-K7vNtVcHUKB9wks1.htm)|Alignment Ampoule (Moderate)|Ampoule d'alignement modérée|changé|
|[weapon-03-moBSgma1rtJqh7Rp.htm](equipment/weapon-03-moBSgma1rtJqh7Rp.htm)|Necrotic Bomb (Moderate)|Bombe nécrotique modérée|changé|
|[weapon-03-opfpl1JmKgrfds9P.htm](equipment/weapon-03-opfpl1JmKgrfds9P.htm)|Staff of Fire|Bâton de feu|changé|
|[weapon-03-q6ZvspNDkzJSP6dg.htm](equipment/weapon-03-q6ZvspNDkzJSP6dg.htm)|Retribution Axe|Hache de représailles|changé|
|[weapon-03-qWRigVOjyLvSW6UA.htm](equipment/weapon-03-qWRigVOjyLvSW6UA.htm)|Redpitch Bomb (Moderate)|Bombe poix-rouge modéré|changé|
|[weapon-03-RapaDb10oJZOjujo.htm](equipment/weapon-03-RapaDb10oJZOjujo.htm)|Hunter's Bow|Arc du chasseur|changé|
|[weapon-03-RgVOC3rpptrwENbu.htm](equipment/weapon-03-RgVOC3rpptrwENbu.htm)|Little Love|Petit amour|changé|
|[weapon-03-Wf94e8YNhZdIvWc9.htm](equipment/weapon-03-Wf94e8YNhZdIvWc9.htm)|Ghost Charge (Moderate)|Charge fantôme modérée|changé|
|[weapon-04-5TOdQaW1XEjDhTP0.htm](equipment/weapon-04-5TOdQaW1XEjDhTP0.htm)|Drake Rifle (Cold)|Fusil de drake du froid|changé|
|[weapon-04-7XEVXfgeabmO7fMd.htm](equipment/weapon-04-7XEVXfgeabmO7fMd.htm)|Spore Shephard's Staff (Major)|Bâton du pâtre des spores majeur|changé|
|[weapon-04-FU9tZ3neEPM8szay.htm](equipment/weapon-04-FU9tZ3neEPM8szay.htm)|Drake Rifle (Acid)|Fusil de drake d'acide|changé|
|[weapon-04-Gfu13B6b9eDdHCtV.htm](equipment/weapon-04-Gfu13B6b9eDdHCtV.htm)|Drake Rifle (Fire)|Fusil de drake de feu|changé|
|[weapon-04-h1G2xCiMrHd7uFko.htm](equipment/weapon-04-h1G2xCiMrHd7uFko.htm)|Drake Rifle (Electricity)|Fusil de drake d'électricité|changé|
|[weapon-04-qLhb5ZvIDOmbwvc9.htm](equipment/weapon-04-qLhb5ZvIDOmbwvc9.htm)|Blast Lance|Lance d'arçon à explosion|changé|
|[weapon-04-WcuknnE3xYfSdbhm.htm](equipment/weapon-04-WcuknnE3xYfSdbhm.htm)|Animal Staff|Bâton animal|changé|
|[weapon-04-wIQjJGLZOKCY1es9.htm](equipment/weapon-04-wIQjJGLZOKCY1es9.htm)|Bottled Sunlight (Moderate)|Soleil en bouteille modéré|changé|
|[weapon-04-xwiZBOjispKVZzGA.htm](equipment/weapon-04-xwiZBOjispKVZzGA.htm)|Mentalist's Staff|Bâton du mentaliste|changé|
|[weapon-04-ywUMtG1viqbnDFYT.htm](equipment/weapon-04-ywUMtG1viqbnDFYT.htm)|Drake Rifle (Poison)|Fusil de drake de poison|changé|
|[weapon-05-2KNAip9W6IoBrfIU.htm](equipment/weapon-05-2KNAip9W6IoBrfIU.htm)|Caterwaul Sling|Fronde qui feule|changé|
|[weapon-05-AYdpUABAZeZnSA7s.htm](equipment/weapon-05-AYdpUABAZeZnSA7s.htm)|Infiltrator's Accessory|Accessoire d'Infiltrateur|changé|
|[weapon-05-bcZB7xmlQy8UWqe1.htm](equipment/weapon-05-bcZB7xmlQy8UWqe1.htm)|Heartripper Blade|Lame arrachecoeur|changé|
|[weapon-05-FSIDKg5gYiXlr39j.htm](equipment/weapon-05-FSIDKg5gYiXlr39j.htm)|Alicorn Lance|Lance d'arçon alicorne|changé|
|[weapon-05-OKAR7GIyJac8dmsi.htm](equipment/weapon-05-OKAR7GIyJac8dmsi.htm)|Singing Sword|Épée chantante|changé|
|[weapon-05-wWXnr3xw5TzDL1M7.htm](equipment/weapon-05-wWXnr3xw5TzDL1M7.htm)|Grasp of Droskar|Poigne de Droskar|changé|
|[weapon-05-YPo7jLMxob6E2p8b.htm](equipment/weapon-05-YPo7jLMxob6E2p8b.htm)|Auspicious Scepter|Sceptre de bonne augure|changé|
|[weapon-06-0Vw44XmzVS4knsa8.htm](equipment/weapon-06-0Vw44XmzVS4knsa8.htm)|Staff of Elemental Power|Bâton de pouvoir élémentaire|changé|
|[weapon-06-0yp4hAgJKt9Al2lz.htm](equipment/weapon-06-0yp4hAgJKt9Al2lz.htm)|Staff of the Desert Winds|Bâton des vents du désert|changé|
|[weapon-06-1uWytZ76MwVQYIO9.htm](equipment/weapon-06-1uWytZ76MwVQYIO9.htm)|Staff of the Dead|Bâton de la mort|changé|
|[weapon-06-214J8KhXn2H19DeC.htm](equipment/weapon-06-214J8KhXn2H19DeC.htm)|Staff of the Dreamlands|Bâton de la Dimension des rêves|changé|
|[weapon-06-2z4GSaKaVX22kzPU.htm](equipment/weapon-06-2z4GSaKaVX22kzPU.htm)|Gluttonous Spear|Lance gloutonne|changé|
|[weapon-06-4kyf3ScoJ4TvhPG3.htm](equipment/weapon-06-4kyf3ScoJ4TvhPG3.htm)|Arboreal's Revenge|Vengeance de l'arboréen|changé|
|[weapon-06-6IJZamE4JkERQumf.htm](equipment/weapon-06-6IJZamE4JkERQumf.htm)|Staff of Conjuration|Bâton d'invocation|changé|
|[weapon-06-70H54rIRjO9fJkdt.htm](equipment/weapon-06-70H54rIRjO9fJkdt.htm)|Spider Gun|Fusil araignée|changé|
|[weapon-06-96VBd7CV8NQyv3lP.htm](equipment/weapon-06-96VBd7CV8NQyv3lP.htm)|Staff of Evocation|Bâton d'évocation|changé|
|[weapon-06-bh1Jr4YqfXQe7r9I.htm](equipment/weapon-06-bh1Jr4YqfXQe7r9I.htm)|Librarian Staff|Bâton du bibliothécaire|changé|
|[weapon-06-dYydONniFSGwAdL1.htm](equipment/weapon-06-dYydONniFSGwAdL1.htm)|Iris of the Sky|Iris du ciel|changé|
|[weapon-06-eaYNVLnTX9VejnaA.htm](equipment/weapon-06-eaYNVLnTX9VejnaA.htm)|Staff of Enchantment|Bâton d'enchantement|changé|
|[weapon-06-EYRrABqjUYPrhrZr.htm](equipment/weapon-06-EYRrABqjUYPrhrZr.htm)|Staff of Abjuration|Bâton d'abjuration|changé|
|[weapon-06-fcRTJBvy1RqXr3ow.htm](equipment/weapon-06-fcRTJBvy1RqXr3ow.htm)|Lyrakien Staff|Bâton du lyrakien|changé|
|[weapon-06-HMjn8Ln1Wp1Ld2S3.htm](equipment/weapon-06-HMjn8Ln1Wp1Ld2S3.htm)|Staff of Final Rest|Bâton du repos final|changé|
|[weapon-06-ObmYN6I64Pjj7yEA.htm](equipment/weapon-06-ObmYN6I64Pjj7yEA.htm)|Staff of Divination|Bâton de divination|changé|
|[weapon-06-PFmBfQ4xENK3jX4M.htm](equipment/weapon-06-PFmBfQ4xENK3jX4M.htm)|Conflagration Club|Massue de conflagration|changé|
|[weapon-06-pjUACbrAXNIy9O7S.htm](equipment/weapon-06-pjUACbrAXNIy9O7S.htm)|Twining Staff|Bâton enroulé|changé|
|[weapon-06-sQFQlglhORQsxBKS.htm](equipment/weapon-06-sQFQlglhORQsxBKS.htm)|Verdant Staff|Bâton verdoyant|changé|
|[weapon-06-tNue4PqJe85ZEE5v.htm](equipment/weapon-06-tNue4PqJe85ZEE5v.htm)|Bloodletting Kukri|Kukri de la saignée|changé|
|[weapon-06-tpKD2TZEzAToow1O.htm](equipment/weapon-06-tpKD2TZEzAToow1O.htm)|Ringmaster's Staff|Bâton du maître de cérémonie|changé|
|[weapon-06-tRmoj42jniQOdjYS.htm](equipment/weapon-06-tRmoj42jniQOdjYS.htm)|Accursed Staff|Bâton de malédiction|changé|
|[weapon-06-UQ0h9IVtHla8gWT7.htm](equipment/weapon-06-UQ0h9IVtHla8gWT7.htm)|Guardian Staff|Bâton du gardien|changé|
|[weapon-06-v0IrX97Pf96jPvxP.htm](equipment/weapon-06-v0IrX97Pf96jPvxP.htm)|Staff of Summoning|Bâton de convocation|changé|
|[weapon-06-XeTmuhuNnhGf7c4t.htm](equipment/weapon-06-XeTmuhuNnhGf7c4t.htm)|Staff of Providence|Bâton de la providence|changé|
|[weapon-07-174fUKNwSgbKwroU.htm](equipment/weapon-07-174fUKNwSgbKwroU.htm)|Lady's Spiral|Spirale de la Dame|changé|
|[weapon-07-4ADeEQGQJR0ju2Nd.htm](equipment/weapon-07-4ADeEQGQJR0ju2Nd.htm)|Man-Feller|Tombeur d'humain|changé|
|[weapon-07-6aubQ21gRc5EMnbT.htm](equipment/weapon-07-6aubQ21gRc5EMnbT.htm)|Undead Scourge|Fléau des morts-vivants|changé|
|[weapon-07-Cb0h1Y2OSrbYXJQ2.htm](equipment/weapon-07-Cb0h1Y2OSrbYXJQ2.htm)|Spellstriker Staff|Bâton de Frappesort|changé|
|[weapon-07-eoSl6Rbop1EX5DwP.htm](equipment/weapon-07-eoSl6Rbop1EX5DwP.htm)|Spy Staff|Bâton de l'espion|changé|
|[weapon-07-JtCO2nielwAg1x9m.htm](equipment/weapon-07-JtCO2nielwAg1x9m.htm)|Zombie Staff|Bâton du zombie|changé|
|[weapon-08-0RCC0fOg1Lp7f79I.htm](equipment/weapon-08-0RCC0fOg1Lp7f79I.htm)|Mentalist's Staff (Greater)|Bâton du mentaliste supérieur|changé|
|[weapon-08-8No84rsBOCVCkXJK.htm](equipment/weapon-08-8No84rsBOCVCkXJK.htm)|Staff of Illumination|Bâton d'illumination|changé|
|[weapon-08-bT9azgpc96DNbitA.htm](equipment/weapon-08-bT9azgpc96DNbitA.htm)|Animal Staff (Greater)|Bâton animal supérieur|changé|
|[weapon-08-IePXTa0jDjDqXxYl.htm](equipment/weapon-08-IePXTa0jDjDqXxYl.htm)|Gray Prince|Prince gris|changé|
|[weapon-08-KcjaeMgrsBGgwUWL.htm](equipment/weapon-08-KcjaeMgrsBGgwUWL.htm)|Staff of Fire (Greater)|Bâton de feu supérieur|changé|
|[weapon-08-RijrZ8lnaYrGTeMV.htm](equipment/weapon-08-RijrZ8lnaYrGTeMV.htm)|Blast Lance (Greater)|Lance à explosion supérieure|changé|
|[weapon-08-RZkttcpkv4qLs1Lk.htm](equipment/weapon-08-RZkttcpkv4qLs1Lk.htm)|Hundred-Moth Caress|Caresse de mille papillons de nuit|changé|
|[weapon-08-sfKzk4bzplyT4zax.htm](equipment/weapon-08-sfKzk4bzplyT4zax.htm)|Sunken Pistol|Pistolet noyé|changé|
|[weapon-08-tU4PKaoo1XuPkKg1.htm](equipment/weapon-08-tU4PKaoo1XuPkKg1.htm)|Boreal Staff|Bâton boréal|changé|
|[weapon-08-xr99pbmKLO2eDMk5.htm](equipment/weapon-08-xr99pbmKLO2eDMk5.htm)|Poisoner's Staff (Greater)|Bâton de l'empoisonneur supérieur|changé|
|[weapon-09-B0R1MtLnQvBwnOne.htm](equipment/weapon-09-B0R1MtLnQvBwnOne.htm)|Sonic Tuning Mace|Masse mélodique|changé|
|[weapon-09-CtUPO3qFZ2Y6lozq.htm](equipment/weapon-09-CtUPO3qFZ2Y6lozq.htm)|Skeletal Claw|Griffe squelletique|changé|
|[weapon-09-DXidUrzzkwWIMNMD.htm](equipment/weapon-09-DXidUrzzkwWIMNMD.htm)|Whip of Compliance|Fouet d'obéissance|changé|
|[weapon-09-ET88qMrIBXBCWI5J.htm](equipment/weapon-09-ET88qMrIBXBCWI5J.htm)|Spider Gun (Greater)|Fusil araignée supérieur|changé|
|[weapon-09-f9SdiktChZHHBj8k.htm](equipment/weapon-09-f9SdiktChZHHBj8k.htm)|Bloodknuckles|Coups-de-poing sanglants|changé|
|[weapon-09-k5P9YZO4ARlE4By3.htm](equipment/weapon-09-k5P9YZO4ARlE4By3.htm)|Crimson Brand|Sceau du Serment écarlate|changé|
|[weapon-09-TO8UvbBy1NfVbrdB.htm](equipment/weapon-09-TO8UvbBy1NfVbrdB.htm)|Devil's Trident|Trident du diable|changé|
|[weapon-09-Us5dVXQHJzNjY0X8.htm](equipment/weapon-09-Us5dVXQHJzNjY0X8.htm)|Grisly Scythe|Faux macabre|changé|
|[weapon-09-w5ZX1R3dPvuLcuRx.htm](equipment/weapon-09-w5ZX1R3dPvuLcuRx.htm)|Gloom Blade|Lame obscure|changé|
|[weapon-09-X95j0ZdqjXJkGmfZ.htm](equipment/weapon-09-X95j0ZdqjXJkGmfZ.htm)|Spellender|Fin de sort|changé|
|[weapon-09-XOXUFjBGO6azymyb.htm](equipment/weapon-09-XOXUFjBGO6azymyb.htm)|Erraticannon|Couleuvrine erratique|changé|
|[weapon-09-ZiaAFSG9zQY0CXdL.htm](equipment/weapon-09-ZiaAFSG9zQY0CXdL.htm)|Vine of Roses|Rosier grimpant|changé|
|[weapon-10-0nVHDnehORCQlqP5.htm](equipment/weapon-10-0nVHDnehORCQlqP5.htm)|Drazmorg's Staff of All-Sight|Bâton de toutes les visions de Drazmorg|changé|
|[weapon-10-6u14xOkKhnB9lVy0.htm](equipment/weapon-10-6u14xOkKhnB9lVy0.htm)|Staff of Illusion (Greater)|Bâton d'illusion supérieur|changé|
|[weapon-10-aPbDAb6cJKPvCHk2.htm](equipment/weapon-10-aPbDAb6cJKPvCHk2.htm)|Staff of Nature's Vengeance|Bâton de vengeance de la nature|changé|
|[weapon-10-E63MaprijBuUK9Om.htm](equipment/weapon-10-E63MaprijBuUK9Om.htm)|Staff of Divination (Greater)|Bâton de divination supérieur|changé|
|[weapon-10-eIGcGgQDBihvJsEo.htm](equipment/weapon-10-eIGcGgQDBihvJsEo.htm)|Accursed Staff (Greater)|Bâton de malédiction supérieur|changé|
|[weapon-10-ExExv8dJVcCj4rL0.htm](equipment/weapon-10-ExExv8dJVcCj4rL0.htm)|Staff of Enchantment (Greater)|Bâton d'enchantement supérieur|changé|
|[weapon-10-GeAz02cQMU4X9nYp.htm](equipment/weapon-10-GeAz02cQMU4X9nYp.htm)|Staff of Summoning (Greater)|Bâton de convocation supérieur|changé|
|[weapon-10-HRNQ1j2OM54Qakhp.htm](equipment/weapon-10-HRNQ1j2OM54Qakhp.htm)|Staff of Abjuration (Greater)|Bâton d'abjuration supérieur|changé|
|[weapon-10-jIX2T21YvEsRaaqp.htm](equipment/weapon-10-jIX2T21YvEsRaaqp.htm)|Guardian Staff (Greater)|Bâton du gardien supérieur|changé|
|[weapon-10-nvSZioRW4J1sMtO7.htm](equipment/weapon-10-nvSZioRW4J1sMtO7.htm)|Staff of Elemental Power (Greater)|Bâton de pouvoir élémentaire supérieur|changé|
|[weapon-10-RCQ4DzDh34ZBiTxI.htm](equipment/weapon-10-RCQ4DzDh34ZBiTxI.htm)|Staff of Evocation (Greater)|Bâton d'évocation supérieur|changé|
|[weapon-10-T1L6XbgMqJLDv2Pi.htm](equipment/weapon-10-T1L6XbgMqJLDv2Pi.htm)|Staff of Providence (Greater)|Bâton de la providence supérieur|changé|
|[weapon-10-uepeQsiOEIqTyNsx.htm](equipment/weapon-10-uepeQsiOEIqTyNsx.htm)|Lyrakien Staff (Greater)|Bâton du lyrakien supérieur|changé|
|[weapon-10-xec2kwqFjxHjFwLa.htm](equipment/weapon-10-xec2kwqFjxHjFwLa.htm)|Staff of Final Rest (Greater)|Bâton du repos final supérieur|changé|
|[weapon-10-xQf98bdGgE7UiBe4.htm](equipment/weapon-10-xQf98bdGgE7UiBe4.htm)|Staff of Conjuration (Greater)|Bâton d'invocation supérieur|changé|
|[weapon-10-zB0Q86QD0A10CeFH.htm](equipment/weapon-10-zB0Q86QD0A10CeFH.htm)|Batsbreath Cane|Canne souffle de chauve-souris|changé|
|[weapon-11-28osNZINXLWzqzUL.htm](equipment/weapon-11-28osNZINXLWzqzUL.htm)|Ghost Charge (Greater)|Charge fantôme supérieure|changé|
|[weapon-11-4nEdlX9GOlXGyfpl.htm](equipment/weapon-11-4nEdlX9GOlXGyfpl.htm)|Spy Staff (Greater)|Bâton de l'espion supérieur|changé|
|[weapon-11-5B8sBxQ7IeKVI6TO.htm](equipment/weapon-11-5B8sBxQ7IeKVI6TO.htm)|Zombie Staff (Greater)|Bâton zombie supérieur|changé|
|[weapon-11-bheWnrBqMphBRUmn.htm](equipment/weapon-11-bheWnrBqMphBRUmn.htm)|Oathbow|Arc du serment|changé|
|[weapon-11-CS3mKu9koUfRiHJo.htm](equipment/weapon-11-CS3mKu9koUfRiHJo.htm)|Redpitch Bomb (Greater)|Bombe poix-rouge supérieure|changé|
|[weapon-11-DePdZBHfTorg74Ah.htm](equipment/weapon-11-DePdZBHfTorg74Ah.htm)|Reaper's Grasp|Poigne de la faucheuse|changé|
|[weapon-11-EtIXWXnfYdV5AGzy.htm](equipment/weapon-11-EtIXWXnfYdV5AGzy.htm)|Tiger's Claw|Griffe du tigre|changé|
|[weapon-11-GKBCShgNVVhDiNCb.htm](equipment/weapon-11-GKBCShgNVVhDiNCb.htm)|Redeemer's Pistol|Pistolet du rédempteur|changé|
|[weapon-11-jC8GmH0Un6vDxdMj.htm](equipment/weapon-11-jC8GmH0Un6vDxdMj.htm)|Acid Flask (Greater)|Fiole d'acide supérieure|changé|
|[weapon-11-kpJmjm843vaMZjIu.htm](equipment/weapon-11-kpJmjm843vaMZjIu.htm)|Dread Ampoule (Greater)|Ampoule d'effroi supérieur|changé|
|[weapon-11-py3PPoO2zMUjOzOI.htm](equipment/weapon-11-py3PPoO2zMUjOzOI.htm)|Rime Foil|Fleuret de givre|changé|
|[weapon-11-rvxphQJrycwqejMW.htm](equipment/weapon-11-rvxphQJrycwqejMW.htm)|Rat-Catcher Trident|Trident raticide|changé|
|[weapon-11-s3wY38FriyervrtC.htm](equipment/weapon-11-s3wY38FriyervrtC.htm)|Alignment Ampoule (Greater)|Ampoule d'alignement supérieur|changé|
|[weapon-11-u41wQz3bgu6J1XKo.htm](equipment/weapon-11-u41wQz3bgu6J1XKo.htm)|Necrotic Bomb (Greater)|Bombe nécrotique supérieure|changé|
|[weapon-11-Y2L8NM50BvRfUaat.htm](equipment/weapon-11-Y2L8NM50BvRfUaat.htm)|Spellstriker Staff (Greater)|Bâton de frappesort supérieur|changé|
|[weapon-11-yMbMbPW2WfjOIrmJ.htm](equipment/weapon-11-yMbMbPW2WfjOIrmJ.htm)|Nightmare Cudgel|Gourdin cauchemar|changé|
|[weapon-12-6qqsyhVHPo8LQH8Z.htm](equipment/weapon-12-6qqsyhVHPo8LQH8Z.htm)|Arboreal Staff|Bâton arboréen|changé|
|[weapon-12-AH0u9DDvs4napvdh.htm](equipment/weapon-12-AH0u9DDvs4napvdh.htm)|Bottled Sunlight (Greater)|Soleil en bouteille supérieur|changé|
|[weapon-12-DTIBj6Yhy73G5P6j.htm](equipment/weapon-12-DTIBj6Yhy73G5P6j.htm)|Mentalist's Staff (Major)|Bâton du mentaliste majeur|changé|
|[weapon-12-ED3By5mICaJeiQYo.htm](equipment/weapon-12-ED3By5mICaJeiQYo.htm)|Staff of the Black Desert|Bâton du désert noir|changé|
|[weapon-12-eHs1vJMXGLIPues8.htm](equipment/weapon-12-eHs1vJMXGLIPues8.htm)|Staff of the Dreamlands (Greater)|Bâton de la Dimension des rêves supérieur|changé|
|[weapon-12-FTVap8IjoKgCexH7.htm](equipment/weapon-12-FTVap8IjoKgCexH7.htm)|Staff of Healing (Major)|Bâton de guérison majeur|changé|
|[weapon-12-KxSyomQx7rpwqemP.htm](equipment/weapon-12-KxSyomQx7rpwqemP.htm)|Staff of Impossible Visions (Greater)|Bâton de visions impossibles supérieur|changé|
|[weapon-12-Oo7IpJQDSTmzUyzG.htm](equipment/weapon-12-Oo7IpJQDSTmzUyzG.htm)|Animal Staff (Major)|Bâton animal majeur|changé|
|[weapon-12-PAZQrWMLfYTpDoal.htm](equipment/weapon-12-PAZQrWMLfYTpDoal.htm)|Boreal Staff (Greater)|Bâton boréal supérieur|changé|
|[weapon-12-QFvB2qgpWdeEa4fT.htm](equipment/weapon-12-QFvB2qgpWdeEa4fT.htm)|Librarian Staff (Greater)|Bâton du bibliothécaire supérieur|changé|
|[weapon-12-qx4Cq99vng6GhzEh.htm](equipment/weapon-12-qx4Cq99vng6GhzEh.htm)|Staff of Fire (Major)|Bâton de feu majeur|changé|
|[weapon-12-uqbvXACABUJG0ifJ.htm](equipment/weapon-12-uqbvXACABUJG0ifJ.htm)|Staff of the Desert Winds (Greater)|Bâton des vents du désert supérieur|changé|
|[weapon-12-VOYQpn4yISYo5Ttq.htm](equipment/weapon-12-VOYQpn4yISYo5Ttq.htm)|Scarlet Queen|Reine écarlate|changé|
|[weapon-12-wWBi79jkzYGWD6uC.htm](equipment/weapon-12-wWBi79jkzYGWD6uC.htm)|Ringmaster's Staff (Greater)|Bâton de maître de cérémonie supérieur|changé|
|[weapon-12-ylRk8NvpK2kA8bjw.htm](equipment/weapon-12-ylRk8NvpK2kA8bjw.htm)|Verdant Staff (Greater)|Bâton verdoyant supérieur|changé|
|[weapon-13-3SjF539wVL6NK9qq.htm](equipment/weapon-13-3SjF539wVL6NK9qq.htm)|Pistol of Wonder|Pistolet merveilleux|changé|
|[weapon-13-ak3DfubhIWT3UiCf.htm](equipment/weapon-13-ak3DfubhIWT3UiCf.htm)|Sonic Tuning Mace (Greater)|Masse mélodique supérieure|changé|
|[weapon-13-AUJfvrouapAeC4Mg.htm](equipment/weapon-13-AUJfvrouapAeC4Mg.htm)|Blade of Four Energies|Lame des quatre énergies|changé|
|[weapon-13-f1UCVpGYLHa089SE.htm](equipment/weapon-13-f1UCVpGYLHa089SE.htm)|Spark Dancer|Danseur étincelles|changé|
|[weapon-13-K5PVPhK5EVM2Fxgg.htm](equipment/weapon-13-K5PVPhK5EVM2Fxgg.htm)|Duchy Defender|Défenseur du Duché|changé|
|[weapon-14-027hHaJojFz1qJBG.htm](equipment/weapon-14-027hHaJojFz1qJBG.htm)|Staff of Summoning (Major)|Bâton de convocation majeur|changé|
|[weapon-14-7aLpTxRVmoNQTqvJ.htm](equipment/weapon-14-7aLpTxRVmoNQTqvJ.htm)|Staff of Conjuration (Major)|Bâton d'invocation majeur|changé|
|[weapon-14-9UfuJHNdyBBNfCsO.htm](equipment/weapon-14-9UfuJHNdyBBNfCsO.htm)|Staff of Elemental Power (Major)|Bâton de pouvoir élémentaire majeur|changé|
|[weapon-14-E30OrRp0StKLQwbQ.htm](equipment/weapon-14-E30OrRp0StKLQwbQ.htm)|Staff of the Dead (Major)|Bâton de la mort majeur|changé|
|[weapon-14-gca5Pgt9Pg8G23VA.htm](equipment/weapon-14-gca5Pgt9Pg8G23VA.htm)|Staff of Nature's Vengeance (Greater)|Bâton de vengeance de la nature supérieur|changé|
|[weapon-14-gG4IUEzqD253s7nx.htm](equipment/weapon-14-gG4IUEzqD253s7nx.htm)|Staff of Illusion (Major)|Bâton d'illusion majeur|changé|
|[weapon-14-GlyiVrIokFpFFRu2.htm](equipment/weapon-14-GlyiVrIokFpFFRu2.htm)|Staff of Transmutation (Major)|Bâton de transmutation majeur|changé|
|[weapon-14-jE5H6jmDJEzxGvSP.htm](equipment/weapon-14-jE5H6jmDJEzxGvSP.htm)|Deepdread Claw|Griffe de la terreur profonde|changé|
|[weapon-14-jHCPejseO01ki3lO.htm](equipment/weapon-14-jHCPejseO01ki3lO.htm)|Accursed Staff (Major)|Bâton de malédiction majeur|changé|
|[weapon-14-kCN0QxUbJrvidysF.htm](equipment/weapon-14-kCN0QxUbJrvidysF.htm)|Brilliant Rapier|Rapière brillante|changé|
|[weapon-14-kNvo4FNaiQRnZfO7.htm](equipment/weapon-14-kNvo4FNaiQRnZfO7.htm)|Staff of Evocation (Major)|Bâton d'évocation majeur|changé|
|[weapon-14-lOeShVkqV98KlvTI.htm](equipment/weapon-14-lOeShVkqV98KlvTI.htm)|Staff of Divination (Major)|Bâton de divination majeur|changé|
|[weapon-14-MeUgw1hsSEcWP97G.htm](equipment/weapon-14-MeUgw1hsSEcWP97G.htm)|Staff of Enchantment (Major)|Bâton d'enchantement majeur|changé|
|[weapon-14-REvrniIEqBykkWyI.htm](equipment/weapon-14-REvrniIEqBykkWyI.htm)|Guardian Staff (Major)|Bâton du gardien majeur|changé|
|[weapon-14-smrNvKVL976JNEab.htm](equipment/weapon-14-smrNvKVL976JNEab.htm)|Fluid Form Staff (Major)|Bâton de forme fluide majeur|changé|
|[weapon-14-U8vVCE2ePjyca666.htm](equipment/weapon-14-U8vVCE2ePjyca666.htm)|Staff of Providence (Major)|Bâton de la providence majeur|changé|
|[weapon-14-UFAIquTmQcgqSl7s.htm](equipment/weapon-14-UFAIquTmQcgqSl7s.htm)|Blessed Reformer|Réformateur béni|changé|
|[weapon-14-YMwlSFUoIEPIyctl.htm](equipment/weapon-14-YMwlSFUoIEPIyctl.htm)|Poisoner's Staff (Major)|Bâton de l'empoisonneur majeur|changé|
|[weapon-14-Za5Mnae009cDCwuN.htm](equipment/weapon-14-Za5Mnae009cDCwuN.htm)|Storm Flash|Eclair d'orage|changé|
|[weapon-14-ZSt2PxZVik7UHGG3.htm](equipment/weapon-14-ZSt2PxZVik7UHGG3.htm)|Staff of Abjuration (Major)|Bâton d'abjuration majeur|changé|
|[weapon-14-zXUmfJEBQ7Pfovve.htm](equipment/weapon-14-zXUmfJEBQ7Pfovve.htm)|Deflecting Branch|Branche de déviation|changé|
|[weapon-15-dIUmjoLjlcrKgXbH.htm](equipment/weapon-15-dIUmjoLjlcrKgXbH.htm)|Golden Blade of Mzali|Lame dorée de Mzali|changé|
|[weapon-15-fLqRH3XpvDZEMxOO.htm](equipment/weapon-15-fLqRH3XpvDZEMxOO.htm)|Radiant Lance|Lance radieuse|changé|
|[weapon-15-kJzp8I0rAmcucHuw.htm](equipment/weapon-15-kJzp8I0rAmcucHuw.htm)|Zombie Staff (Major)|Bâton zombie majeur|changé|
|[weapon-15-LGbyZ4z6SLRC1cuB.htm](equipment/weapon-15-LGbyZ4z6SLRC1cuB.htm)|Spider Gun (Major)|Fusil araignée majeur|changé|
|[weapon-15-mKEhLq7Ha4lvupMP.htm](equipment/weapon-15-mKEhLq7Ha4lvupMP.htm)|Animate Dreamer|Rêveur animé|changé|
|[weapon-15-NS2j5bYEoHeESlyN.htm](equipment/weapon-15-NS2j5bYEoHeESlyN.htm)|Black King|Roi noir|changé|
|[weapon-15-O7pgJB6M5UqUZ9Qq.htm](equipment/weapon-15-O7pgJB6M5UqUZ9Qq.htm)|Hyldarf's Fang|Croc de Hyldarf|changé|
|[weapon-15-PeRLqNTbzaiITNuO.htm](equipment/weapon-15-PeRLqNTbzaiITNuO.htm)|Beast Staff (Major)|Bâton de la bête majeure|changé|
|[weapon-15-tD4ZNDutHoCGIFQn.htm](equipment/weapon-15-tD4ZNDutHoCGIFQn.htm)|Blade of the Black Sovereign|Lame du Souverain Noir|changé|
|[weapon-15-tzzvevwmwwKQaHK0.htm](equipment/weapon-15-tzzvevwmwwKQaHK0.htm)|Spy Staff (Major)|Bâton de l'espion majeur|changé|
|[weapon-15-U266pQUnWwKYImhD.htm](equipment/weapon-15-U266pQUnWwKYImhD.htm)|Rod of Razors|Sceptre de rasoirs|changé|
|[weapon-16-6m9niDjhA6tBfp5x.htm](equipment/weapon-16-6m9niDjhA6tBfp5x.htm)|Staff of Power|Bâton de surpuissance|changé|
|[weapon-16-AFkybwwQzAArjrF4.htm](equipment/weapon-16-AFkybwwQzAArjrF4.htm)|Spellstriker Staff (Major)|Bâton de frappesort majeur|changé|
|[weapon-16-FkT0UkXdNEmKRhRx.htm](equipment/weapon-16-FkT0UkXdNEmKRhRx.htm)|Staff of the Desert Winds (Major)|Bâton des vents du désert majeur|changé|
|[weapon-16-oT4pyqLKpJVXDb46.htm](equipment/weapon-16-oT4pyqLKpJVXDb46.htm)|Staff of Impossible Visions (Major)|Bâton de visions impossibles majeur|changé|
|[weapon-16-Ri09KBg9DG0aapLw.htm](equipment/weapon-16-Ri09KBg9DG0aapLw.htm)|Staff of the Black Desert (Greater)|Bâton du Désert noir supérieur|changé|
|[weapon-16-SSZojuAMC9Npvfm3.htm](equipment/weapon-16-SSZojuAMC9Npvfm3.htm)|Staff of Final Rest (Major)|Bâton du repos final majeur|changé|
|[weapon-16-uX8urrzOQM5wdoTD.htm](equipment/weapon-16-uX8urrzOQM5wdoTD.htm)|Lyrakien Staff (Major)|Bâton du lyrakien majeur|changé|
|[weapon-16-WbcQqXrUytXkCMK3.htm](equipment/weapon-16-WbcQqXrUytXkCMK3.htm)|Staff of Healing (True)|Bâton de guérison ultime|changé|
|[weapon-17-5MuAq1wFhy5NE0U0.htm](equipment/weapon-17-5MuAq1wFhy5NE0U0.htm)|Zealot Staff|Bâton du zélote|changé|
|[weapon-17-Aulo25tnSdOCOmrI.htm](equipment/weapon-17-Aulo25tnSdOCOmrI.htm)|Alignment Ampoule (Major)|Ampoule d'alignement majeure|changé|
|[weapon-17-GttLYMT2welSpwCd.htm](equipment/weapon-17-GttLYMT2welSpwCd.htm)|Acid Flask (Major)|Fiole d'acide majeure|changé|
|[weapon-17-gWSmUEKln103LVvB.htm](equipment/weapon-17-gWSmUEKln103LVvB.htm)|Wyrm Drinker|Buveur de Ver|changé|
|[weapon-17-i7bJcib5TUJKOd4Z.htm](equipment/weapon-17-i7bJcib5TUJKOd4Z.htm)|Ghost Charge (Major)|Charge fantôme majeure|changé|
|[weapon-17-LKIs3LJqrKNP2yrx.htm](equipment/weapon-17-LKIs3LJqrKNP2yrx.htm)|Spellcutter|Coupe-sort|changé|
|[weapon-17-MCHYtxP8E7njLC3s.htm](equipment/weapon-17-MCHYtxP8E7njLC3s.htm)|Dread Ampoule (Major)|Ampoule d'effroi majeure|changé|
|[weapon-17-QbEkoFL1EYNGsOom.htm](equipment/weapon-17-QbEkoFL1EYNGsOom.htm)|Hell Staff|Bâton infernal|changé|
|[weapon-17-RC8EmoFiMITFUopr.htm](equipment/weapon-17-RC8EmoFiMITFUopr.htm)|Necrotic Bomb (Major)|Bombe nécrotique majeure|changé|
|[weapon-17-riHdB9PBAXZUMsWC.htm](equipment/weapon-17-riHdB9PBAXZUMsWC.htm)|Redpitch Bomb (Major)|Bombe poix-rouge majeure|changé|
|[weapon-17-rYBX6v9JqpxAiQvn.htm](equipment/weapon-17-rYBX6v9JqpxAiQvn.htm)|Boreal Staff (Major)|Bâton boréal majeur|changé|
|[weapon-17-S1ALL8osqV1pOtf3.htm](equipment/weapon-17-S1ALL8osqV1pOtf3.htm)|Celestial Staff|Bâton céleste|changé|
|[weapon-17-TKvAPv3jgv4l143r.htm](equipment/weapon-17-TKvAPv3jgv4l143r.htm)|Celestial Peachwood Sword|Épée en pêcher céleste|changé|
|[weapon-18-1gi4mdZYNgPrzWHc.htm](equipment/weapon-18-1gi4mdZYNgPrzWHc.htm)|Life's Last Breath|Dernier souffle de vie|changé|
|[weapon-18-AWUzApS5PPwt3TCI.htm](equipment/weapon-18-AWUzApS5PPwt3TCI.htm)|Blade of Four Energies (Greater)|Lame des quatre énergies supérieure|changé|
|[weapon-18-hnx3dOQrYLBtsu3V.htm](equipment/weapon-18-hnx3dOQrYLBtsu3V.htm)|Staff of Nature's Vengeance (Major)|Bâton de Vengeance de la nature majeur|changé|
|[weapon-18-i9mxfSIBTTOwsSlx.htm](equipment/weapon-18-i9mxfSIBTTOwsSlx.htm)|Storm Flash (Greater)|Éclair d'Orage supérieur|changé|
|[weapon-18-Kmrkm1sHKkGXiJlr.htm](equipment/weapon-18-Kmrkm1sHKkGXiJlr.htm)|Torag's Silver Anvil|Enclume d'argent de Torag|changé|
|[weapon-18-NWMHPsVBIaNZ5X2W.htm](equipment/weapon-18-NWMHPsVBIaNZ5X2W.htm)|Staff of the Dreamlands (Major)|Bâton de la Dimension des rêves majeur|changé|
|[weapon-18-nz1v5aetUZVLBlvg.htm](equipment/weapon-18-nz1v5aetUZVLBlvg.htm)|Final Rest|Repos final|changé|
|[weapon-18-xoucExiZTvCbjWMB.htm](equipment/weapon-18-xoucExiZTvCbjWMB.htm)|Bottled Sunlight (Major)|Soleil en bouteille majeur|changé|
|[weapon-20-1glQQ63AeQOfQNvc.htm](equipment/weapon-20-1glQQ63AeQOfQNvc.htm)|Staff of Impossible Visions (True)|Bâton de visions impossibles ultime|changé|
|[weapon-20-ANvbi1zKF1So8bON.htm](equipment/weapon-20-ANvbi1zKF1So8bON.htm)|Sky Hammer|Marteau céleste|changé|
|[weapon-20-BzVwQ0Go1GFp9R0U.htm](equipment/weapon-20-BzVwQ0Go1GFp9R0U.htm)|Dragon Handwraps|Bandelettes draconiques|changé|
|[weapon-20-dh4FEXlA0FxTfnpY.htm](equipment/weapon-20-dh4FEXlA0FxTfnpY.htm)|Staff of Sieges|Bâton de sièges|changé|
|[weapon-20-FXY5YezrQ13dubnd.htm](equipment/weapon-20-FXY5YezrQ13dubnd.htm)|Briar|Roncier|changé|
|[weapon-20-IEmYcavVfSy58aYx.htm](equipment/weapon-20-IEmYcavVfSy58aYx.htm)|Staff of the Desert Winds (True)|Bâton des vents du désert ultime|changé|
|[weapon-20-kg6tEjEBFGeilmLO.htm](equipment/weapon-20-kg6tEjEBFGeilmLO.htm)|Spear of the Destroyer's Flame|Lance de la flamme du destructeur|changé|
|[weapon-20-lSwITRpBSdodhc1c.htm](equipment/weapon-20-lSwITRpBSdodhc1c.htm)|Phoenix Fighting Fan|Éventail de combat du phénix|changé|
|[weapon-20-nI9shR1EG3P09I8r.htm](equipment/weapon-20-nI9shR1EG3P09I8r.htm)|Staff of the Magi|Bâton du mage|changé|
|[weapon-20-nSAA77N0cnSkljmE.htm](equipment/weapon-20-nSAA77N0cnSkljmE.htm)|Viper Rapier|Rapière vipère|changé|
|[weapon-20-O6he0J7l1uQgsama.htm](equipment/weapon-20-O6he0J7l1uQgsama.htm)|Piereta|Piereta|changé|
|[weapon-20-pIagOwW8EFBaKW3k.htm](equipment/weapon-20-pIagOwW8EFBaKW3k.htm)|Staff of Providence (True)|Bâton de la providence ultime|changé|
|[weapon-20-tbc3dqTyibuGoCA6.htm](equipment/weapon-20-tbc3dqTyibuGoCA6.htm)|Ovinrbaane|Ovinrbaane|changé|
|[weapon-20-WrNHmsU8ZaBY3V9S.htm](equipment/weapon-20-WrNHmsU8ZaBY3V9S.htm)|Whispering Staff|Bâton de chuchotement|changé|
|[weapon-22-2WOpgJyaFE2gNW7H.htm](equipment/weapon-22-2WOpgJyaFE2gNW7H.htm)|Spiral Athame|Athamé spiralé|changé|
|[weapon-22-h2UebueRKfVLwKOa.htm](equipment/weapon-22-h2UebueRKfVLwKOa.htm)|Cane of the Maelstrom|Canne du Maelström|changé|
|[weapon-22-Vz7nITDWjrXmyoJn.htm](equipment/weapon-22-Vz7nITDWjrXmyoJn.htm)|Kortos Diamond|Diamant de Kortos|changé|
|[weapon-23-2jtZPnHF1M8vKWry.htm](equipment/weapon-23-2jtZPnHF1M8vKWry.htm)|Hunter's Dawn|Aube du chasseur|changé|
|[weapon-23-emGyagWNmjvtjiGK.htm](equipment/weapon-23-emGyagWNmjvtjiGK.htm)|Serithtial|Sérithtial|changé|
|[weapon-23-JYtqvOyF11GUsQTB.htm](equipment/weapon-23-JYtqvOyF11GUsQTB.htm)|Coldstar Pistols|Pistolets étoile froide|changé|
|[weapon-23-xxSpHT2vnCFxTr0q.htm](equipment/weapon-23-xxSpHT2vnCFxTr0q.htm)|Ghosthand's Comet|Comète main fantôme|changé|
|[weapon-25-LMKw9PCVGeZy7knY.htm](equipment/weapon-25-LMKw9PCVGeZy7knY.htm)|Cayden's Tankard|Chope de Cayden|changé|
|[weapon-25-o9IErbpmItz9NZT3.htm](equipment/weapon-25-o9IErbpmItz9NZT3.htm)|Blackaxe|Hache-noire|changé|
|[weapon-26-tPwYYwxNqdGQp2nW.htm](equipment/weapon-26-tPwYYwxNqdGQp2nW.htm)|Axe of the Dwarven Lords|Hache des seigneurs nains|changé|
|[weapon-28-rft8qacTooqfsUIo.htm](equipment/weapon-28-rft8qacTooqfsUIo.htm)|Whisperer of Souls|Chuchoteur des âmes|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[armor-00-3O1Iv7ztIPKhKWwe.htm](equipment/armor-00-3O1Iv7ztIPKhKWwe.htm)|Sankeit|Sankeit|libre|
|[armor-00-3ZfGYXKfBjHOZO7d.htm](equipment/armor-00-3ZfGYXKfBjHOZO7d.htm)|Lamellar Breastplate|Cuirasse lamellaire|libre|
|[armor-00-4tIVTg9wj56RrveA.htm](equipment/armor-00-4tIVTg9wj56RrveA.htm)|Leather Armor|Armure en cuir|officielle|
|[armor-00-56CTZheeNhNPpLo1.htm](equipment/armor-00-56CTZheeNhNPpLo1.htm)|Subterfuge Suit|Costume de subterfuge|libre|
|[armor-00-5yL2D3GPAggadZQN.htm](equipment/armor-00-5yL2D3GPAggadZQN.htm)|Light Barding|Bardes légères|libre|
|[armor-00-7xcLPvLDqOcnj0qs.htm](equipment/armor-00-7xcLPvLDqOcnj0qs.htm)|Quilted Armor|Armure molletonnée|libre|
|[armor-00-AnwzlOs0njF9Jqnr.htm](equipment/armor-00-AnwzlOs0njF9Jqnr.htm)|Hide Armor|Armure de peau|officielle|
|[armor-00-bZHqPBdQjPS02Rd4.htm](equipment/armor-00-bZHqPBdQjPS02Rd4.htm)|Coral Armor|Armure de corail|libre|
|[armor-00-dDIPA1WE9ESF67EB.htm](equipment/armor-00-dDIPA1WE9ESF67EB.htm)|Explorer's Clothing|Vêtements d'explorateur|libre|
|[armor-00-emh9kvqSJtxTX8He.htm](equipment/armor-00-emh9kvqSJtxTX8He.htm)|Buckle Armor|Armure de boucles|libre|
|[armor-00-ewQZ0VeL38v3qFnN.htm](equipment/armor-00-ewQZ0VeL38v3qFnN.htm)|Studded Leather Armor|Armure en cuir clouté|officielle|
|[armor-00-GvAGrcl14Ywcgm2I.htm](equipment/armor-00-GvAGrcl14Ywcgm2I.htm)|Armored Cloak|Cape renforcée|libre|
|[armor-00-Hg7btiITMu6Zf2EU.htm](equipment/armor-00-Hg7btiITMu6Zf2EU.htm)|Ceramic Plate|Harnois en céramique|libre|
|[armor-00-I8b3oNLl5MIMrG4r.htm](equipment/armor-00-I8b3oNLl5MIMrG4r.htm)|Niyaháat|Niyaháat|libre|
|[armor-00-IyBL38UtL4MDN6b5.htm](equipment/armor-00-IyBL38UtL4MDN6b5.htm)|Lattice Armor|Armure de treillis|libre|
|[armor-00-Kf4eJEXnFPuAsseP.htm](equipment/armor-00-Kf4eJEXnFPuAsseP.htm)|Chain Mail|Cotte de mailles|officielle|
|[armor-00-mkMWda6ivlhnXq4d.htm](equipment/armor-00-mkMWda6ivlhnXq4d.htm)|Armored Coat|Manteau renforcé|libre|
|[armor-00-MPcM4Wt6KmWE2kGL.htm](equipment/armor-00-MPcM4Wt6KmWE2kGL.htm)|Chain Shirt|Chemise de mailles|officielle|
|[armor-00-N42lmp3Ft6EsSvzg.htm](equipment/armor-00-N42lmp3Ft6EsSvzg.htm)|Power Suit|Costume de puissance|libre|
|[armor-00-QPgkjNm2uIV5tsRd.htm](equipment/armor-00-QPgkjNm2uIV5tsRd.htm)|Wooden Breastplate|Cuirasse en bois|libre|
|[armor-00-r0ifJfoz8aqf0mwk.htm](equipment/armor-00-r0ifJfoz8aqf0mwk.htm)|Breastplate|Cuirasse|officielle|
|[armor-00-U5IGgD7Z225OPnhK.htm](equipment/armor-00-U5IGgD7Z225OPnhK.htm)|Reinforced Chassis|Châssis renforcé|libre|
|[armor-00-VJ4s6ABlkimSyjjZ.htm](equipment/armor-00-VJ4s6ABlkimSyjjZ.htm)|Scroll Robes|Toges parchemin|libre|
|[armor-00-wx7WY6YGvgMphDL1.htm](equipment/armor-00-wx7WY6YGvgMphDL1.htm)|Leather Lamellar|Cuir lamelaire|libre|
|[armor-00-yiU4osCo6ql28r6M.htm](equipment/armor-00-yiU4osCo6ql28r6M.htm)|Gi|Gi|libre|
|[armor-00-YMQr577asquZIP65.htm](equipment/armor-00-YMQr577asquZIP65.htm)|Scale Mail|Armure d'écailles|officielle|
|[armor-00-zBYEU9E7034ENCmh.htm](equipment/armor-00-zBYEU9E7034ENCmh.htm)|Padded Armor|Armure matelassée|libre|
|[armor-00-zVSBjb6o5RudKA9c.htm](equipment/armor-00-zVSBjb6o5RudKA9c.htm)|Leaf Weave|Tissu de feuilles|libre|
|[armor-01-0KHgAaDi3tmu32Hq.htm](equipment/armor-01-0KHgAaDi3tmu32Hq.htm)|Rite of Reinforcement Exoskeleton|Exosquelette du rite de renforcement|libre|
|[armor-01-6AhDKX1dwRwFpQsU.htm](equipment/armor-01-6AhDKX1dwRwFpQsU.htm)|Splint Mail|Clibanion|officielle|
|[armor-01-7cy9gLlKX2vNja0a.htm](equipment/armor-01-7cy9gLlKX2vNja0a.htm)|Tough Skin|Peau robuste|libre|
|[armor-01-8WVYOughRWLb7kGF.htm](equipment/armor-01-8WVYOughRWLb7kGF.htm)|Mantis Shell|Carapace de mante|libre|
|[armor-01-KA0Ku5qOQfXqw3BK.htm](equipment/armor-01-KA0Ku5qOQfXqw3BK.htm)|Titan Nagaji Scales|Écailles de nagaji titan|libre|
|[armor-01-LF7HSNnlGxLJyDZS.htm](equipment/armor-01-LF7HSNnlGxLJyDZS.htm)|Hellknight Breastplate|Cuirasse du chevalier infernal|libre|
|[armor-01-O6cnU1Uiss81C4bG.htm](equipment/armor-01-O6cnU1Uiss81C4bG.htm)|Swarmsuit|Combinaison anti-nuée|libre|
|[armor-01-pRoikbRo5HFW6YUB.htm](equipment/armor-01-pRoikbRo5HFW6YUB.htm)|Half Plate|Armure de plates|officielle|
|[armor-01-U6XqhSFUUowHnnkk.htm](equipment/armor-01-U6XqhSFUUowHnnkk.htm)|Hellknight Half Plate|Plates de Chevalier infernal|libre|
|[armor-02-FSDEuHDr1I4JVIJg.htm](equipment/armor-02-FSDEuHDr1I4JVIJg.htm)|Fortress Plate|Harnois forteresse|libre|
|[armor-02-Gq1cZWSKOtJhKd2p.htm](equipment/armor-02-Gq1cZWSKOtJhKd2p.htm)|Full Plate|Harnois|officielle|
|[armor-02-juQGJI9deskAVfm7.htm](equipment/armor-02-juQGJI9deskAVfm7.htm)|Heavy Barding (Small or Medium)|Bardes lourdes (Petit ou Moyen)|libre|
|[armor-02-lCgXeV52eYYl5cUX.htm](equipment/armor-02-lCgXeV52eYYl5cUX.htm)|Hellknight Plate|Harnois de Chevalier infernal|libre|
|[armor-02-M6b20zZTGNWrx5oM.htm](equipment/armor-02-M6b20zZTGNWrx5oM.htm)|Gray Maiden Plate|Harnois de vierge grise|libre|
|[armor-02-oGVtymwknrJS8gWW.htm](equipment/armor-02-oGVtymwknrJS8gWW.htm)|Bastion Plate|Harnois bastion|libre|
|[armor-02-U2liOd8kJasy0Ghd.htm](equipment/armor-02-U2liOd8kJasy0Ghd.htm)|O-Yoroi|O-Yoroi|libre|
|[armor-03-MUtAHJXf22SiVBeH.htm](equipment/armor-03-MUtAHJXf22SiVBeH.htm)|Heavy Barding (Large)|Bardes lourdes (Grande)|libre|
|[armor-05-3ZBFkXQC7jgZ8kx9.htm](equipment/armor-05-3ZBFkXQC7jgZ8kx9.htm)|Ooze Skin|Peau de vase|libre|
|[armor-05-HsfoHX1B6ocflitz.htm](equipment/armor-05-HsfoHX1B6ocflitz.htm)|Crushing Coils|Anneaux constricteurs|libre|
|[armor-05-uPy4iagJMWRi1vNd.htm](equipment/armor-05-uPy4iagJMWRi1vNd.htm)|Living Leaf Weave|Armure de feuilles vivantes|libre|
|[armor-06-EHMRryMB3s6XdkRz.htm](equipment/armor-06-EHMRryMB3s6XdkRz.htm)|Clockwork Diving Suit|Scaphandre mécanisé|libre|
|[armor-06-hRwnNTMj7wa8S4Ji.htm](equipment/armor-06-hRwnNTMj7wa8S4Ji.htm)|Ghoul Hide|Peau de goule|libre|
|[armor-06-jau3C5ao3vm2gt8c.htm](equipment/armor-06-jau3C5ao3vm2gt8c.htm)|Clockwork Disguise|Déguisement mécanisé|libre|
|[armor-06-tzFMetc4VK6goDm2.htm](equipment/armor-06-tzFMetc4VK6goDm2.htm)|Trollhound Vest|Veste en limier de troll|libre|
|[armor-06-VyQZVv31q3CjCAQz.htm](equipment/armor-06-VyQZVv31q3CjCAQz.htm)|Sarkorian God-Caller Garb|Tenue d'invocateur divin Sarkorien|libre|
|[armor-06-WOwVBsiwLQxnIJRV.htm](equipment/armor-06-WOwVBsiwLQxnIJRV.htm)|Devil's Bargain|Marché avec le diable|libre|
|[armor-07-3mmS3Y291h6Ckgin.htm](equipment/armor-07-3mmS3Y291h6Ckgin.htm)|Powered Full Plate|Harnois alimenté|libre|
|[armor-07-JDgFHv334yCrBY38.htm](equipment/armor-07-JDgFHv334yCrBY38.htm)|Blade Byrnie|Lame Byrnie|libre|
|[armor-07-JG7p7yI12cAPuJ8X.htm](equipment/armor-07-JG7p7yI12cAPuJ8X.htm)|Arachnid Harness|Harnais arachnide|libre|
|[armor-07-tBGhH7AHyvJQZC5d.htm](equipment/armor-07-tBGhH7AHyvJQZC5d.htm)|Wolfjaw Armor|Armure Gueuledeloup|libre|
|[armor-07-vHZSx5f093Wrivzn.htm](equipment/armor-07-vHZSx5f093Wrivzn.htm)|Moonlit Chain|Maille lunaire|libre|
|[armor-08-3ytZSLqXQdNCuV4l.htm](equipment/armor-08-3ytZSLqXQdNCuV4l.htm)|Swarmsuit (Impenetrable)|Combinaison anti-nuées impénétrable|libre|
|[armor-08-mHHvzxHtlLbkbStG.htm](equipment/armor-08-mHHvzxHtlLbkbStG.htm)|Containment Contraption|Appareil de confinement|officielle|
|[armor-08-uV8fEVUuSyMsh1xF.htm](equipment/armor-08-uV8fEVUuSyMsh1xF.htm)|Wasp Guard|Garde-guêpe|libre|
|[armor-08-Wfu6R7y4w9iTZcDs.htm](equipment/armor-08-Wfu6R7y4w9iTZcDs.htm)|Rusting Carapace|Carapace oxydante|libre|
|[armor-09-Blp9Ha1YeIJOeFls.htm](equipment/armor-09-Blp9Ha1YeIJOeFls.htm)|Spangled Rider's Suit|Costume de cavalier pailleté|libre|
|[armor-09-bosOCvsQb8tG8fuD.htm](equipment/armor-09-bosOCvsQb8tG8fuD.htm)|Wisp Chain|Chaine bourrasque|libre|
|[armor-09-C4XKMcHZoGzrAZBl.htm](equipment/armor-09-C4XKMcHZoGzrAZBl.htm)|Rhino Hide|Peau de rhinocéros|libre|
|[armor-09-gawhJLVsVsUFAPd6.htm](equipment/armor-09-gawhJLVsVsUFAPd6.htm)|Cloister Robe (Lesser)|Robe de cloître inférieure|libre|
|[armor-09-GPrjYsQiC3jKdQmO.htm](equipment/armor-09-GPrjYsQiC3jKdQmO.htm)|Blast Suit|Armure anti-explosion|libre|
|[armor-09-jAKYIqG2BNO7yffJ.htm](equipment/armor-09-jAKYIqG2BNO7yffJ.htm)|Leopard's Armor|Armure du léopard|libre|
|[armor-09-NIyJqiW1Wci6gP9m.htm](equipment/armor-09-NIyJqiW1Wci6gP9m.htm)|Arachnid Harness (Greater)|Harnais arachnide supérieur|libre|
|[armor-09-nVlKU2et4EeReayr.htm](equipment/armor-09-nVlKU2et4EeReayr.htm)|Victory Plate|Harnois de la victoire|libre|
|[armor-09-SS1lwhzx0hne1Ljh.htm](equipment/armor-09-SS1lwhzx0hne1Ljh.htm)|Library Robes|Robe de bibliothécaire inférieure|libre|
|[armor-09-u9BdrpYhJDUPQFCF.htm](equipment/armor-09-u9BdrpYhJDUPQFCF.htm)|Dragon Turtle Plate|Armure de plates de tortue dragon|libre|
|[armor-10-7jGnVnqFt2gFtU1g.htm](equipment/armor-10-7jGnVnqFt2gFtU1g.htm)|Tideplate|Harnois des marées|libre|
|[armor-10-7qBpWdPZ0X8HoH9X.htm](equipment/armor-10-7qBpWdPZ0X8HoH9X.htm)|Bone Dreadnought Plate|Harnois d'os cuirassé|libre|
|[armor-10-8HxM35X8DDt2gw9d.htm](equipment/armor-10-8HxM35X8DDt2gw9d.htm)|Electric Eelskin|Peau d'anguille électrique|libre|
|[armor-10-b8bVEp4qOorBDsAF.htm](equipment/armor-10-b8bVEp4qOorBDsAF.htm)|Scarab Cuirass|Cuirasse de scarabée|libre|
|[armor-10-GDSclGF1yk0sw86V.htm](equipment/armor-10-GDSclGF1yk0sw86V.htm)|Numerian Steel Breastplate|Cuirasse en acier numérien|libre|
|[armor-10-gg6XF2dWafE0pcHu.htm](equipment/armor-10-gg6XF2dWafE0pcHu.htm)|Tales in Timber|Contes de bois|libre|
|[armor-10-oE6tZBBbP59DOWFM.htm](equipment/armor-10-oE6tZBBbP59DOWFM.htm)|Remorhaz Armor|Armure rémorharz|libre|
|[armor-10-qZUo5SmGxp5BlqFN.htm](equipment/armor-10-qZUo5SmGxp5BlqFN.htm)|Barbed Vest|Gilet barbelé|libre|
|[armor-10-RUEAV5LMUGFHcXcW.htm](equipment/armor-10-RUEAV5LMUGFHcXcW.htm)|Breastplate of Command|Cuirasse de commandement|libre|
|[armor-12-EBuQjgVZaQpjQUjc.htm](equipment/armor-12-EBuQjgVZaQpjQUjc.htm)|Lion's Armor|Armure du lion|libre|
|[armor-12-HoL83ca2TqEnnErs.htm](equipment/armor-12-HoL83ca2TqEnnErs.htm)|Library Robes (Greater)|Robe de bibliothécaire supérieure|libre|
|[armor-12-IAPztxiK2T5mC2Y5.htm](equipment/armor-12-IAPztxiK2T5mC2Y5.htm)|Bastion of the Inheritor|Bastion de l'héritière|libre|
|[armor-12-l3BVdNdtv3486bOk.htm](equipment/armor-12-l3BVdNdtv3486bOk.htm)|Autumn's Embrace|Étreinte d'automne|libre|
|[armor-12-M7H8LNnmUA17oqsW.htm](equipment/armor-12-M7H8LNnmUA17oqsW.htm)|Wisp Chain (Greater)|Chaine bourrasque supérieure|libre|
|[armor-12-uuBhzN6xrplrwKyI.htm](equipment/armor-12-uuBhzN6xrplrwKyI.htm)|Reef Heart|Coeur de récif inférieur|libre|
|[armor-12-VMksM7bLAZW9LxH4.htm](equipment/armor-12-VMksM7bLAZW9LxH4.htm)|Cloister Robe (Moderate)|Robe de cloître modérée|libre|
|[armor-13-2uHcTZ40oZ62R9gy.htm](equipment/armor-13-2uHcTZ40oZ62R9gy.htm)|Celestial Armor|Armure céleste|officielle|
|[armor-13-AdJgj7vVvNGvPIEj.htm](equipment/armor-13-AdJgj7vVvNGvPIEj.htm)|Mail of Luck|Maille chanceuse|officielle|
|[armor-13-AqVJOBjXbeao5An7.htm](equipment/armor-13-AqVJOBjXbeao5An7.htm)|Energizing Lattice|Maille énergisante|libre|
|[armor-13-BK1AKKi1gDMn5uBs.htm](equipment/armor-13-BK1AKKi1gDMn5uBs.htm)|Invisible Chain Shirt|Chemise de maille invisible|libre|
|[armor-13-GAu6v14pCSgLJh2D.htm](equipment/armor-13-GAu6v14pCSgLJh2D.htm)|Noxious Jerkin|Pourpoint nauséabond|libre|
|[armor-13-hYBZK1kaGPeR85CH.htm](equipment/armor-13-hYBZK1kaGPeR85CH.htm)|Demon Armor|Armure démoniaque|officielle|
|[armor-13-iCJrRoIsjdf665yE.htm](equipment/armor-13-iCJrRoIsjdf665yE.htm)|Psychic Brigandine|Clibanion psychique|libre|
|[armor-13-qdUlmO7XfBWQU3p5.htm](equipment/armor-13-qdUlmO7XfBWQU3p5.htm)|Blade Byrnie (Greater)|Lame Byrnie supérieure|libre|
|[armor-13-zPSImy0ENQjnRJNs.htm](equipment/armor-13-zPSImy0ENQjnRJNs.htm)|Forgotten Shell|Coquille oubliée|libre|
|[armor-14-SMruq5revpHcKDdt.htm](equipment/armor-14-SMruq5revpHcKDdt.htm)|Highhelm Stronghold Plate|Harnois forteresse de Heaume|libre|
|[armor-14-tLNsmY90aN668PMG.htm](equipment/armor-14-tLNsmY90aN668PMG.htm)|Glorious Plate|Harnois glorieux|libre|
|[armor-15-1X0E707FWf9jJgPK.htm](equipment/armor-15-1X0E707FWf9jJgPK.htm)|Troll Hide|Peau de troll|libre|
|[armor-15-BTHxd53T6xttQ0d9.htm](equipment/armor-15-BTHxd53T6xttQ0d9.htm)|Wisp Chain (Major)|Chaine bourrasque majeure|libre|
|[armor-15-gp5kgCySEOuntQPF.htm](equipment/armor-15-gp5kgCySEOuntQPF.htm)|Plate Armor of the Deep|Harnois des profondeurs|officielle|
|[armor-15-RODC2TYO4cUg6fXu.htm](equipment/armor-15-RODC2TYO4cUg6fXu.htm)|Reef Heart (Greater)|Coeur de récif supérieur|libre|
|[armor-15-RtJvTDFNPhJSPmWP.htm](equipment/armor-15-RtJvTDFNPhJSPmWP.htm)|Library Robes (Major)|Robe de bibliothécaire majeure|libre|
|[armor-15-Wkh5rQx3IqInEKdT.htm](equipment/armor-15-Wkh5rQx3IqInEKdT.htm)|Robe of the Archmagi|Robe de l'archimage|officielle|
|[armor-15-XPIDpF7dP6VyGKOM.htm](equipment/armor-15-XPIDpF7dP6VyGKOM.htm)|Cloister Robe (Greater)|Robe de cloître supérieure|libre|
|[armor-16-3xsQ1AA4dyMHLxpw.htm](equipment/armor-16-3xsQ1AA4dyMHLxpw.htm)|Dragonplate|Harnois du dragon|officielle|
|[armor-16-DUTKrSu5iJMktfZI.htm](equipment/armor-16-DUTKrSu5iJMktfZI.htm)|Black Hole Armor|Armure du Trou noir|libre|
|[armor-17-COsToVFlA36UmkB7.htm](equipment/armor-17-COsToVFlA36UmkB7.htm)|Jerkin of Liberation|Pourpoint de la libération|libre|
|[armor-18-XKsPGGbPKLkrqHwM.htm](equipment/armor-18-XKsPGGbPKLkrqHwM.htm)|Linnorm's Sankeit|Sankeit du linnorm|libre|
|[armor-19-4Zxm46hCidf2GmvV.htm](equipment/armor-19-4Zxm46hCidf2GmvV.htm)|Wisp Chain (True)|Chaine bourrasque ultime|libre|
|[armor-19-7ynjS9llFg7tPMoj.htm](equipment/armor-19-7ynjS9llFg7tPMoj.htm)|Robe of the Archmagi (Greater)|Robe de l'archimage supérieure|officielle|
|[armor-19-dmEmxoEVzOeEkscr.htm](equipment/armor-19-dmEmxoEVzOeEkscr.htm)|Lion's Armor (Greater)|Armure du lion supérieure|libre|
|[armor-19-fHkVbTtjhITsE2NJ.htm](equipment/armor-19-fHkVbTtjhITsE2NJ.htm)|Blade Byrnie (Major)|Lame Byrnie majeure|libre|
|[armor-19-mhCLoihjfhoqpiVp.htm](equipment/armor-19-mhCLoihjfhoqpiVp.htm)|Cloister Robe (Major)|Robe de cloître majeure|libre|
|[armor-19-o4U9Nt7ac7AlolNQ.htm](equipment/armor-19-o4U9Nt7ac7AlolNQ.htm)|Ouroboros Buckles|Boucles d'ouroboros|libre|
|[armor-20-FKNgrFDIRBumr6P5.htm](equipment/armor-20-FKNgrFDIRBumr6P5.htm)|Rebounding Breastplate|Cuirasse rebondissante|libre|
|[armor-20-qSqx72639EJTzBam.htm](equipment/armor-20-qSqx72639EJTzBam.htm)|Immortal Bastion|Bastion immortel|libre|
|[backpack-00-3lgwjrFEsQVKzhh7.htm](equipment/backpack-00-3lgwjrFEsQVKzhh7.htm)|Backpack|Sac à dos|officielle|
|[backpack-00-DujblC14ytJEZMaz.htm](equipment/backpack-00-DujblC14ytJEZMaz.htm)|Sack|Sac|officielle|
|[backpack-00-HgS4FYX0itxQvwub.htm](equipment/backpack-00-HgS4FYX0itxQvwub.htm)|Chair Storage|Stockage de fauteuil roulant|libre|
|[backpack-00-mBziC8v2eOtTs0f5.htm](equipment/backpack-00-mBziC8v2eOtTs0f5.htm)|Saddlebags|Fontes|officielle|
|[backpack-00-OdVDPN9vIpu3Zud3.htm](equipment/backpack-00-OdVDPN9vIpu3Zud3.htm)|Chest|Coffre|officielle|
|[backpack-02-KxUJu1IHY9FxxAGV.htm](equipment/backpack-02-KxUJu1IHY9FxxAGV.htm)|Sturdy Satchel|Sacoche robuste|libre|
|[backpack-03-6uHwCTK8dX1reraz.htm](equipment/backpack-03-6uHwCTK8dX1reraz.htm)|Immaculate Holsters|Étuis d'armes à feu immaculés|libre|
|[backpack-03-vLsoZKRzYQV6FQve.htm](equipment/backpack-03-vLsoZKRzYQV6FQve.htm)|Gunner's Bandolier|Bandoulière du tireur|libre|
|[backpack-04-1pglC9PQx6yOgcKL.htm](equipment/backpack-04-1pglC9PQx6yOgcKL.htm)|Sleeves of Storage|Poches de rangement|libre|
|[backpack-04-6z7VUJanx7nPwhk6.htm](equipment/backpack-04-6z7VUJanx7nPwhk6.htm)|Smuggler's Sack (Type I)|Sac du contrebandier (Type I)|libre|
|[backpack-04-jaEEvuQ32GjAa8jy.htm](equipment/backpack-04-jaEEvuQ32GjAa8jy.htm)|Spacious Pouch (Type I)|Sac spacieux (25 Enc)|libre|
|[backpack-04-W5znRDeklmWEGzFY.htm](equipment/backpack-04-W5znRDeklmWEGzFY.htm)|Bag of Weasels|Sac de belettes|officielle|
|[backpack-06-0c7zLY9c88K2n0GC.htm](equipment/backpack-06-0c7zLY9c88K2n0GC.htm)|Pathfinder's Pouch|Bourse d'Éclaireur|libre|
|[backpack-06-NBmOTmE2GAEI766h.htm](equipment/backpack-06-NBmOTmE2GAEI766h.htm)|Misdirecting Haversack|Sacoche trompeuse|libre|
|[backpack-07-eZ2kfpjboRrFWK9R.htm](equipment/backpack-07-eZ2kfpjboRrFWK9R.htm)|Smuggler's Sack (Type II)|Sac du contrebandier (Type II)|libre|
|[backpack-07-JBMBaN9dZLytfFLQ.htm](equipment/backpack-07-JBMBaN9dZLytfFLQ.htm)|Spacious Pouch (Type II)|Sac spacieux (50 Enc)|libre|
|[backpack-07-MN8SF2sArQhJg6QG.htm](equipment/backpack-07-MN8SF2sArQhJg6QG.htm)|Bag of Devouring Type I|Sac dévoreur (Type 1)|officielle|
|[backpack-09-3hv6NVC2rVu4QCNt.htm](equipment/backpack-09-3hv6NVC2rVu4QCNt.htm)|Sleeves of Storage (Greater)|Poches de rangement supérieures|libre|
|[backpack-09-G4luLo1IEtDben39.htm](equipment/backpack-09-G4luLo1IEtDben39.htm)|Thousand-Blade Thesis|Thèse des mille-lames|libre|
|[backpack-09-rdF2RKgFK0vOlaeV.htm](equipment/backpack-09-rdF2RKgFK0vOlaeV.htm)|Knapsack of Halflingkind|Besace halfeline|libre|
|[backpack-09-yUPUxRqyYdZaPhkF.htm](equipment/backpack-09-yUPUxRqyYdZaPhkF.htm)|Lucky Draw Bandolier|Bandoulière du veinard|libre|
|[backpack-11-EIBmADRICTyYzxik.htm](equipment/backpack-11-EIBmADRICTyYzxik.htm)|Bag of Devouring Type II|Sac dévoreur (Type 2)|officielle|
|[backpack-11-fPOidyARnS0McvHx.htm](equipment/backpack-11-fPOidyARnS0McvHx.htm)|Smuggler's Sack (Type III)|Sac du contrebandier (Type III)|libre|
|[backpack-11-jkb2WNby4mjcYqq9.htm](equipment/backpack-11-jkb2WNby4mjcYqq9.htm)|Spacious Pouch (Type III)|Sac spacieux (100 Enc)|libre|
|[backpack-13-34DA4rFy7bduRAld.htm](equipment/backpack-13-34DA4rFy7bduRAld.htm)|Spacious Pouch (Type IV)|Sac spacieux (150 Enc)|libre|
|[backpack-13-jEnDLdcuOgXDf0I6.htm](equipment/backpack-13-jEnDLdcuOgXDf0I6.htm)|Smuggler's Sack (Type IV)|Sac du contrebandier (Type IV)|libre|
|[backpack-13-k5y89wYu3NX1AhVI.htm](equipment/backpack-13-k5y89wYu3NX1AhVI.htm)|Bomber's Saddle|Selle de bombardier|libre|
|[backpack-13-n3kJYoTrzXYwlYaV.htm](equipment/backpack-13-n3kJYoTrzXYwlYaV.htm)|Bag of Devouring Type III|Sac dévoreur (Type 3)|officielle|
|[backpack-15-DtEhaF5fZcqgcQ3H.htm](equipment/backpack-15-DtEhaF5fZcqgcQ3H.htm)|Mother Maw|Gueule-mère|libre|
|[backpack-17-JL0j0AtJZYEaKeOa.htm](equipment/backpack-17-JL0j0AtJZYEaKeOa.htm)|Smuggler's Sack (Type V)|Sac du contrebandier (Type V)|libre|
|[backpack-20-HEvuhktgeFXdlDKm.htm](equipment/backpack-20-HEvuhktgeFXdlDKm.htm)|Extradimensional Stash|Cachette extradimensionnelle|libre|
|[consumable-00-11b6MN3UQ8XKqAer.htm](equipment/consumable-00-11b6MN3UQ8XKqAer.htm)|Rounds (Three Peaked Tree)|Munition (Arbre à trois piques)|libre|
|[consumable-00-154C222Y7M96nvrL.htm](equipment/consumable-00-154C222Y7M96nvrL.htm)|Rounds (Dragon Mouth Pistol)|Munition (Pistolet Gueule de dragon)|libre|
|[consumable-00-1UWYVwWVnp8Vng5t.htm](equipment/consumable-00-1UWYVwWVnp8Vng5t.htm)|Backpack Catapult Stone|Pierre de catapulte de sac à dos|libre|
|[consumable-00-20h1dSg7IVJ5NxNa.htm](equipment/consumable-00-20h1dSg7IVJ5NxNa.htm)|Rounds (Pepperbox)|Munition (Poivrière)|libre|
|[consumable-00-2ayp6yyQDr3iliBp.htm](equipment/consumable-00-2ayp6yyQDr3iliBp.htm)|Rounds (Fire Lance)|Munition (Lance de feu)|libre|
|[consumable-00-2DoUHoueAyyP4lMN.htm](equipment/consumable-00-2DoUHoueAyyP4lMN.htm)|Bloodeye Coffee|Café oeil-sanglant|officielle|
|[consumable-00-2VS3S2E3dbqvGifH.htm](equipment/consumable-00-2VS3S2E3dbqvGifH.htm)|Fake Blood Pack|Poche de faux sang|libre|
|[consumable-00-3nDKWIydNEfhJ8XJ.htm](equipment/consumable-00-3nDKWIydNEfhJ8XJ.htm)|Treat (Unique)|Friandise (unique)|libre|
|[consumable-00-3QGZS6l7NrX4auyR.htm](equipment/consumable-00-3QGZS6l7NrX4auyR.htm)|Rounds (Flintlock Pistol)|Munition (Pistolet à silex)|libre|
|[consumable-00-58fs0VsG71A9gyEk.htm](equipment/consumable-00-58fs0VsG71A9gyEk.htm)|Rounds (Clan Pistol)|Munition (Pistolet de clan)|libre|
|[consumable-00-9w8MrBhFtvM7RrYN.htm](equipment/consumable-00-9w8MrBhFtvM7RrYN.htm)|Magazine with 5 Bolts|Magasin de 5 carreaux|libre|
|[consumable-00-AITVZmakiu3RgfKo.htm](equipment/consumable-00-AITVZmakiu3RgfKo.htm)|Bolts|Carreaux|officielle|
|[consumable-00-AOOQJfSkTPiSzfB9.htm](equipment/consumable-00-AOOQJfSkTPiSzfB9.htm)|Black Powder (Dose or Round)|Poudre noire (Dose ou munition)|libre|
|[consumable-00-AqslI71DReZOzgAW.htm](equipment/consumable-00-AqslI71DReZOzgAW.htm)|Formulated Sunlight|Concoction de soleil|libre|
|[consumable-00-BZCzkDWQP3TU1Bbp.htm](equipment/consumable-00-BZCzkDWQP3TU1Bbp.htm)|Rounds (Harmona Gun)|Munition (Fusil harmona)|libre|
|[consumable-00-ckxYpfX3jTcZZOAz.htm](equipment/consumable-00-ckxYpfX3jTcZZOAz.htm)|Rounds (Gun Sword)|Munition (Épée-pistolet)|libre|
|[consumable-00-CmWkSgFyb6HB9KVK.htm](equipment/consumable-00-CmWkSgFyb6HB9KVK.htm)|Rounds (Hand Cannon)|Munition (Couleuvrine)|libre|
|[consumable-00-DKsfWwWZ23PHPIa1.htm](equipment/consumable-00-DKsfWwWZ23PHPIa1.htm)|Sun Shot|Balle soleil|libre|
|[consumable-00-Dlsv1n0Bk1zVJaJM.htm](equipment/consumable-00-Dlsv1n0Bk1zVJaJM.htm)|Repeating Crossbow Magazine|Magasin d'arbalète à répétition|libre|
|[consumable-00-dSZz8B96D20IWKZ5.htm](equipment/consumable-00-dSZz8B96D20IWKZ5.htm)|Rounds (Blunderbuss)|Munitions (Tromblon)|libre|
|[consumable-00-Du05UxCyCM0ZWH8z.htm](equipment/consumable-00-Du05UxCyCM0ZWH8z.htm)|Oil (1 pint)|Huile (1/2 l)|officielle|
|[consumable-00-eCRZcG7ZxFRP8bpx.htm](equipment/consumable-00-eCRZcG7ZxFRP8bpx.htm)|Rounds (Jezail)|Munition (Fusil Jezaïl)|libre|
|[consumable-00-F5JDZSMYYKYfyeEE.htm](equipment/consumable-00-F5JDZSMYYKYfyeEE.htm)|Repeating Heavy Crossbow Magazine|Magasin d'arbalète lourde à répétition|libre|
|[consumable-00-FCoziiYnBoRJFVaM.htm](equipment/consumable-00-FCoziiYnBoRJFVaM.htm)|Rounds (Dagger Pistol)|Munitions (Pistolet dague)|libre|
|[consumable-00-FKPuzveKRNJhA1mi.htm](equipment/consumable-00-FKPuzveKRNJhA1mi.htm)|Rounds (Dueling Pistol)|Munition (Pistolet de duel)|libre|
|[consumable-00-Fy63rwYVBq0hXh0T.htm](equipment/consumable-00-Fy63rwYVBq0hXh0T.htm)|Rounds (Flintlock Musket)|Munition (Mousquet à silex)|libre|
|[consumable-00-g190hQLpwU0jrSNa.htm](equipment/consumable-00-g190hQLpwU0jrSNa.htm)|Feed (Standard)|Alimentation (ordinaire)|libre|
|[consumable-00-GWTSMcXocmUsNLmN.htm](equipment/consumable-00-GWTSMcXocmUsNLmN.htm)|Poison Sedum|Poison de sedum|libre|
|[consumable-00-GyzinRH2ZSHANAFc.htm](equipment/consumable-00-GyzinRH2ZSHANAFc.htm)|Rounds (Hammer Gun)|Munition (Fusil marteau)|libre|
|[consumable-00-h7jTMfYGQau1PBBz.htm](equipment/consumable-00-h7jTMfYGQau1PBBz.htm)|Rounds (Double-Barreled Pistol)|Munition (Pistolet à double barillet)|libre|
|[consumable-00-HUgVrYvdy9SkqRZ9.htm](equipment/consumable-00-HUgVrYvdy9SkqRZ9.htm)|Rounds (Dwarven Scattergun)|Munition (Éparpilleur nain)|libre|
|[consumable-00-hXZrMJPlw1UvVzjC.htm](equipment/consumable-00-hXZrMJPlw1UvVzjC.htm)|Magazine with 8 Pellets|Magasin de 8 plombs|libre|
|[consumable-00-IAeGh5qvu7ob4yad.htm](equipment/consumable-00-IAeGh5qvu7ob4yad.htm)|Grappling Arrow|Flèche grappin|libre|
|[consumable-00-JPGOrHeYxjdnUogT.htm](equipment/consumable-00-JPGOrHeYxjdnUogT.htm)|Qat|Qat|libre|
|[consumable-00-kKnMlymiqZLVEAtI.htm](equipment/consumable-00-kKnMlymiqZLVEAtI.htm)|Basic Ingredient|Ingrédient basique|libre|
|[consumable-00-KODdKPhwSQ9t4LKx.htm](equipment/consumable-00-KODdKPhwSQ9t4LKx.htm)|Rounds (Black Powder Knuckle Dusters)|Munition (coup-de-poing à poudre noire)|libre|
|[consumable-00-L2df9pyWLFNgjfNh.htm](equipment/consumable-00-L2df9pyWLFNgjfNh.htm)|Rounds (Slide Pistol)|Munition (Pistolet semi-automatique)|libre|
|[consumable-00-L9ZV076913otGtiB.htm](equipment/consumable-00-L9ZV076913otGtiB.htm)|Rations|Rations|officielle|
|[consumable-00-lHyguikeoZlFlYdx.htm](equipment/consumable-00-lHyguikeoZlFlYdx.htm)|Rounds (Arquebus)|Munition (Arquebuse)|libre|
|[consumable-00-MKSeXwUm56c15MZa.htm](equipment/consumable-00-MKSeXwUm56c15MZa.htm)|Sling Bullets|Billes de fronde|officielle|
|[consumable-00-nb83vPkRwm47cu3z.htm](equipment/consumable-00-nb83vPkRwm47cu3z.htm)|Feed (Unique)|Alimentation (unique)|libre|
|[consumable-00-NELpewmAIxQXaMtv.htm](equipment/consumable-00-NELpewmAIxQXaMtv.htm)|Backpack Ballista Bolt|Carreau de baliste de sac à dos|libre|
|[consumable-00-NF8dWgLTxj8QRrmg.htm](equipment/consumable-00-NF8dWgLTxj8QRrmg.htm)|Treat (Standard)|Friandise (ordinaire)|libre|
|[consumable-00-NiioDSvyhhY4IWVH.htm](equipment/consumable-00-NiioDSvyhhY4IWVH.htm)|Magazine with 6 Pellets|Magasin de 6 plombs|libre|
|[consumable-00-oGKa9TWCrFC2LkBU.htm](equipment/consumable-00-oGKa9TWCrFC2LkBU.htm)|Rounds (Cane Pistol)|Munition (Canne-pistolet)|libre|
|[consumable-00-oQr0x30JoLrWxQdA.htm](equipment/consumable-00-oQr0x30JoLrWxQdA.htm)|Rounds (Rapier Pistol)|Munition (Pistolet rapière)|libre|
|[consumable-00-oX2Ra68qKcixAxbm.htm](equipment/consumable-00-oX2Ra68qKcixAxbm.htm)|Rounds (Coat Pistol)|Munition (Pistolet de poche)|libre|
|[consumable-00-P5RvQfgQNCgxpdPU.htm](equipment/consumable-00-P5RvQfgQNCgxpdPU.htm)|Rounds (Double-Barreled Musket)|Munition (Mousquet à double barillet)|libre|
|[consumable-00-pErNGR7u7SJDq1o0.htm](equipment/consumable-00-pErNGR7u7SJDq1o0.htm)|Practice Target|Cible d'entrainement|libre|
|[consumable-00-PTwm2NAcReBw2iUY.htm](equipment/consumable-00-PTwm2NAcReBw2iUY.htm)|Flayleaf|Éreintante|officielle|
|[consumable-00-qaAQnuLVia6vS1LU.htm](equipment/consumable-00-qaAQnuLVia6vS1LU.htm)|Spray Pellets|Granulés de pulvérisation|libre|
|[consumable-00-RQlZFVXv4LOxGyyn.htm](equipment/consumable-00-RQlZFVXv4LOxGyyn.htm)|Rounds (Flingflenser)|Munition (Dépeceur)|libre|
|[consumable-00-sAI6yzB11LlxisD7.htm](equipment/consumable-00-sAI6yzB11LlxisD7.htm)|Grappling Bolt|Carreau grappin|libre|
|[consumable-00-sfMHQsOv3W8jFFop.htm](equipment/consumable-00-sfMHQsOv3W8jFFop.htm)|Rounds (Piercing Wind)|Munition (Vent perçant)|libre|
|[consumable-00-sqhr1crb184s3Vnd.htm](equipment/consumable-00-sqhr1crb184s3Vnd.htm)|Blowgun Darts|Fléchettes de sarbacane|officielle|
|[consumable-00-Ti4gWILk69LPxKuU.htm](equipment/consumable-00-Ti4gWILk69LPxKuU.htm)|Candle|Bougie|officielle|
|[consumable-00-UMAXLDpI6YLSfYX1.htm](equipment/consumable-00-UMAXLDpI6YLSfYX1.htm)|Alcohol|Alcool|libre|
|[consumable-00-useyu9PIrr7QXUUh.htm](equipment/consumable-00-useyu9PIrr7QXUUh.htm)|Spikes|Piquants|libre|
|[consumable-00-UZGfofcq560tw43N.htm](equipment/consumable-00-UZGfofcq560tw43N.htm)|Powder|Poudre|libre|
|[consumable-00-V2CfXehNhCcxApbg.htm](equipment/consumable-00-V2CfXehNhCcxApbg.htm)|Rounds (Mace Multipistol)|Munition (Masse multi-canons)|libre|
|[consumable-00-V9QG93vXuh8d4sJQ.htm](equipment/consumable-00-V9QG93vXuh8d4sJQ.htm)|Rounds (Gnome Amalgam Musket)|Munitions (Mousquet amalgame gnome)|libre|
|[consumable-00-VctYvRnaKXjKlqQ7.htm](equipment/consumable-00-VctYvRnaKXjKlqQ7.htm)|Cutlery|Coutellerie|libre|
|[consumable-00-VeeH30MvHyTX4QAT.htm](equipment/consumable-00-VeeH30MvHyTX4QAT.htm)|Rounds (Axe Musket)|Munition (Mousquet-hache)|libre|
|[consumable-00-w2ENw2VMPcsbif8g.htm](equipment/consumable-00-w2ENw2VMPcsbif8g.htm)|Arrows|Flèches|officielle|
|[consumable-00-xk9bw9ccupAIx7xT.htm](equipment/consumable-00-xk9bw9ccupAIx7xT.htm)|Repeating Hand Crossbow Magazine|Magasin d'arbalète de poing à répétition|libre|
|[consumable-00-XnU7QmGlvBHzxkiJ.htm](equipment/consumable-00-XnU7QmGlvBHzxkiJ.htm)|Rounds (Mithral Tree)|Munition (Arbre d'aubargent)|libre|
|[consumable-00-xShIDyydOMkGvGNb.htm](equipment/consumable-00-xShIDyydOMkGvGNb.htm)|Chalk|Craie|officielle|
|[consumable-00-xwHTZGzD8HQrVSal.htm](equipment/consumable-00-xwHTZGzD8HQrVSal.htm)|Shield Pistol Rounds|Balles de pistolet bouclier|libre|
|[consumable-00-YrNmw0Rn3MqT0yKP.htm](equipment/consumable-00-YrNmw0Rn3MqT0yKP.htm)|Wooden Taws|Calots de bois|libre|
|[consumable-00-zxnBe0wgTAEXXzUU.htm](equipment/consumable-00-zxnBe0wgTAEXXzUU.htm)|Rounds (Explosive Dogslicer)|Munition (Tranchechien explosif)|libre|
|[consumable-01-24hmJWXhSmfVebEE.htm](equipment/consumable-01-24hmJWXhSmfVebEE.htm)|Marvelous Miniature (Campfire)|Figurine merveilleuse Feu de camp|libre|
|[consumable-01-2kFaQDsMZmPIEOct.htm](equipment/consumable-01-2kFaQDsMZmPIEOct.htm)|Impact Foam Chassis (Lesser)|Armature de mousse inférieure|libre|
|[consumable-01-2RuepCemJhrpKKao.htm](equipment/consumable-01-2RuepCemJhrpKKao.htm)|Healing Potion (Minor)|Potion de guérison mineure|libre|
|[consumable-01-3cWko20JPnjeoofV.htm](equipment/consumable-01-3cWko20JPnjeoofV.htm)|Marvelous Miniature (Ladder)|Figurine merveilleuse échelle|libre|
|[consumable-01-3V2U720YhW2nyGVx.htm](equipment/consumable-01-3V2U720YhW2nyGVx.htm)|Spike Snare|Piège artisanal à pointes|officielle|
|[consumable-01-3Wb0N7iqmTn6e2Xc.htm](equipment/consumable-01-3Wb0N7iqmTn6e2Xc.htm)|Ration Tonic|Ration énergisante|libre|
|[consumable-01-5MKBwpE401uz4kNN.htm](equipment/consumable-01-5MKBwpE401uz4kNN.htm)|Quicksilver Mutagen (Lesser)|Mutagène de vif-argent inférieur|officielle|
|[consumable-01-60DuFyGQoZhPj4To.htm](equipment/consumable-01-60DuFyGQoZhPj4To.htm)|Prey Mutagen (Lesser)|Mutagène proie inférieur|libre|
|[consumable-01-60RuBISKBTuYquBe.htm](equipment/consumable-01-60RuBISKBTuYquBe.htm)|Appetizing Flavor Snare|Piège artisanal alléchant|libre|
|[consumable-01-7e57yUVtRgYLOFwT.htm](equipment/consumable-01-7e57yUVtRgYLOFwT.htm)|Explosive Mine (Lesser)|Mine explosive inférieure|libre|
|[consumable-01-7tweUS8kQ6N45fEu.htm](equipment/consumable-01-7tweUS8kQ6N45fEu.htm)|Popdust|Poussière explosive|libre|
|[consumable-01-7V5fKnnvW7DUgbAd.htm](equipment/consumable-01-7V5fKnnvW7DUgbAd.htm)|Animal Repellent (Minor)|Répulsif animal mineur|libre|
|[consumable-01-7Y2yOr4ltpP2tyuL.htm](equipment/consumable-01-7Y2yOr4ltpP2tyuL.htm)|Eagle Eye Elixir (Lesser)|Élixir d'oeil du faucon inférieur|libre|
|[consumable-01-81XVuTsF1zD6EXmN.htm](equipment/consumable-01-81XVuTsF1zD6EXmN.htm)|Blindpepper Tube|Tube de poivre aveuglant|libre|
|[consumable-01-8eTGOQ9P69405jIO.htm](equipment/consumable-01-8eTGOQ9P69405jIO.htm)|Forensic Dye|Teinture forensique|libre|
|[consumable-01-9EVzmy4dY9LpUgyC.htm](equipment/consumable-01-9EVzmy4dY9LpUgyC.htm)|Silver Tripod|Trépied en argent|libre|
|[consumable-01-9F3d43xMDCJNIkDo.htm](equipment/consumable-01-9F3d43xMDCJNIkDo.htm)|Choker-Arm Mutagen (Lesser)|Mutagène d'assouplissement inférieur|libre|
|[consumable-01-AFX1V0Go9DqPWBlN.htm](equipment/consumable-01-AFX1V0Go9DqPWBlN.htm)|Alarm Snare|Piège artisanal d'alarme|officielle|
|[consumable-01-aP5wvs60j6VaKvWL.htm](equipment/consumable-01-aP5wvs60j6VaKvWL.htm)|Flare Beacon (Lesser)|Balise éclairante inférieure|libre|
|[consumable-01-apGQcVMSRprFEeKt.htm](equipment/consumable-01-apGQcVMSRprFEeKt.htm)|Potion of Acid Retaliation (Minor)|Potion de riposte acide mineure|libre|
|[consumable-01-arNOPU7h8nNbnZ1N.htm](equipment/consumable-01-arNOPU7h8nNbnZ1N.htm)|Bane Ammunition (Lesser)|Munition de proie favorite inférieure|libre|
|[consumable-01-azFpL1NLBJB7xuli.htm](equipment/consumable-01-azFpL1NLBJB7xuli.htm)|Dust Pods|Sachet de poussière|libre|
|[consumable-01-BjCGt377LpCwbtyS.htm](equipment/consumable-01-BjCGt377LpCwbtyS.htm)|Emergency Disguise|Déguisement d'urgence|libre|
|[consumable-01-BJtpbzjSza4wUlQX.htm](equipment/consumable-01-BJtpbzjSza4wUlQX.htm)|Smoke Screen Snare (Lesser)|Piège artisanal d'écran de fumée inférieur|libre|
|[consumable-01-bOPQDM54W8ZDoULY.htm](equipment/consumable-01-bOPQDM54W8ZDoULY.htm)|Serene Mutagen (Lesser)|Mutagène de sérénité inférieur|officielle|
|[consumable-01-CbZ72wdo1jiWLXeY.htm](equipment/consumable-01-CbZ72wdo1jiWLXeY.htm)|Armor Polishing Kit|Kit polisseur d'armure|libre|
|[consumable-01-CeKPffF0FaEyhLMp.htm](equipment/consumable-01-CeKPffF0FaEyhLMp.htm)|Spear Frog Poison|Poison de grenouille-lance|officielle|
|[consumable-01-clyXfh0aVXgij2Hb.htm](equipment/consumable-01-clyXfh0aVXgij2Hb.htm)|Wolf Fang|Croc de loup|libre|
|[consumable-01-dymL1IbxU5zR9fqo.htm](equipment/consumable-01-dymL1IbxU5zR9fqo.htm)|Searing Suture (Lesser)|Suture douloureuse inférieure|libre|
|[consumable-01-eHVZBmtzEcnsHSk8.htm](equipment/consumable-01-eHVZBmtzEcnsHSk8.htm)|Skeptic's Elixir (Lesser)|Élixir de l'incrédule inférieur|libre|
|[consumable-01-ev3F9qlMNlNdCOAI.htm](equipment/consumable-01-ev3F9qlMNlNdCOAI.htm)|Runestone|Pierre runique|libre|
|[consumable-01-EWujUs3YmlBu2jhm.htm](equipment/consumable-01-EWujUs3YmlBu2jhm.htm)|Shielding Salve|Onguent de bouclier|libre|
|[consumable-01-fRVCVfoV8KzmZsKs.htm](equipment/consumable-01-fRVCVfoV8KzmZsKs.htm)|Rust Scrub|Gommage anti-rouille|libre|
|[consumable-01-fTQ5e4utVfgtXV1e.htm](equipment/consumable-01-fTQ5e4utVfgtXV1e.htm)|Oil of Unlife (Minor)|Huile de non-vie mineure|libre|
|[consumable-01-G3EIZLApRwt3hq13.htm](equipment/consumable-01-G3EIZLApRwt3hq13.htm)|Firecracker Fulu|Fulu pétards|libre|
|[consumable-01-g91dodJKrkQ2gCA6.htm](equipment/consumable-01-g91dodJKrkQ2gCA6.htm)|Self-Immolating Note|Note autodestructrice|libre|
|[consumable-01-GS4YvQieBS11JNYR.htm](equipment/consumable-01-GS4YvQieBS11JNYR.htm)|Drakeheart Mutagen (Lesser)|Mutagène de coeur de drake inférieur|libre|
|[consumable-01-GZo6icE8v07xQKKc.htm](equipment/consumable-01-GZo6icE8v07xQKKc.htm)|Animal-Turning Fulu|Fulu repousse animal|libre|
|[consumable-01-H2BqcfujumZVWF69.htm](equipment/consumable-01-H2BqcfujumZVWF69.htm)|Slippery Ribbon|Ruban glissant|libre|
|[consumable-01-H3KAt7cHJ6ZLTJQE.htm](equipment/consumable-01-H3KAt7cHJ6ZLTJQE.htm)|War Blood Mutagen (Lesser)|Mutagène de sang guerrier inférieur|libre|
|[consumable-01-hd8D1Dm6aVhMYpEL.htm](equipment/consumable-01-hd8D1Dm6aVhMYpEL.htm)|Sparkler|Bougie magique|libre|
|[consumable-01-hDLbR56Id2OtU318.htm](equipment/consumable-01-hDLbR56Id2OtU318.htm)|Elixir of Life (Minor)|Élixir de vie mineur|libre|
|[consumable-01-hGw7Q1y2IEXRGAfv.htm](equipment/consumable-01-hGw7Q1y2IEXRGAfv.htm)|Gecko Potion|Potion de gecko|libre|
|[consumable-01-I2ZbXsfm0c9Eep4I.htm](equipment/consumable-01-I2ZbXsfm0c9Eep4I.htm)|Grolna|Grolna|libre|
|[consumable-01-INyiWvehFuo8nGaa.htm](equipment/consumable-01-INyiWvehFuo8nGaa.htm)|Glowing Lantern Fruit|Fruit lanterne brillant|libre|
|[consumable-01-IQK9N2mEOyAj3iWU.htm](equipment/consumable-01-IQK9N2mEOyAj3iWU.htm)|Bestial Mutagen (Lesser)|Mutagène bestial inférieur|officielle|
|[consumable-01-JbkAk2j7h31izKyR.htm](equipment/consumable-01-JbkAk2j7h31izKyR.htm)|Feyfoul (Lesser)|Cafouillefée inférieur|libre|
|[consumable-01-jlFx4JIBKJuaINpv.htm](equipment/consumable-01-jlFx4JIBKJuaINpv.htm)|Potion of Shared Memories|Potion des souvenirs partagés|libre|
|[consumable-01-JPALWW3w4z8STAYV.htm](equipment/consumable-01-JPALWW3w4z8STAYV.htm)|Deadweight Mutagen (Lesser)|Mutagène de poids-mort inférieur|libre|
|[consumable-01-jTfacZ4SRuQd7Avh.htm](equipment/consumable-01-jTfacZ4SRuQd7Avh.htm)|Potion of Emergency Escape|Potion de retraite accélérée|libre|
|[consumable-01-jVJwCB5ztkXHD5ML.htm](equipment/consumable-01-jVJwCB5ztkXHD5ML.htm)|Clockwork Goggles|Lunettes mécaniques|libre|
|[consumable-01-kF761P3ibBIFmLm9.htm](equipment/consumable-01-kF761P3ibBIFmLm9.htm)|Caltrop Snare|Piège artisanal de chausse-trappes|officielle|
|[consumable-01-Km4lSOsyrip5q6iD.htm](equipment/consumable-01-Km4lSOsyrip5q6iD.htm)|Hampering Snare|Piège artisanal ralentisseur|officielle|
|[consumable-01-kScHu3XS3XPvN9Db.htm](equipment/consumable-01-kScHu3XS3XPvN9Db.htm)|Theatrical Mutagen (Lesser)|Mutagène théatral inférieur|libre|
|[consumable-01-ktjFOp3U0wQD9t0Z.htm](equipment/consumable-01-ktjFOp3U0wQD9t0Z.htm)|Antidote (Lesser)|Antidote inférieur|libre|
|[consumable-01-lAmwzpbGaARQ2eP4.htm](equipment/consumable-01-lAmwzpbGaARQ2eP4.htm)|Shrine Inarizushi|Autel d'Inarizuhi|libre|
|[consumable-01-Led9ciHzpF8uPN6Y.htm](equipment/consumable-01-Led9ciHzpF8uPN6Y.htm)|Thunderbird Tuft (Lesser)|Touffe d'oiseau-tonnerre mineure|libre|
|[consumable-01-M0DMGUN84FQHapsF.htm](equipment/consumable-01-M0DMGUN84FQHapsF.htm)|Alchemical Fuse|Mèche alchimique|officielle|
|[consumable-01-mbrwudO35tItsldq.htm](equipment/consumable-01-mbrwudO35tItsldq.htm)|Energy Mutagen (Lesser)|Mutagène d'énergie inférieur|libre|
|[consumable-01-MoBlVd36uD9xVvZC.htm](equipment/consumable-01-MoBlVd36uD9xVvZC.htm)|Smoke Ball (Lesser)|Boule de fumée inférieure|libre|
|[consumable-01-mYfK7M3BhB57LmAl.htm](equipment/consumable-01-mYfK7M3BhB57LmAl.htm)|Missive Mint|Menthe missive|libre|
|[consumable-01-NI7twU2G6UCDmvCO.htm](equipment/consumable-01-NI7twU2G6UCDmvCO.htm)|Shining Ammunition|Munition luisante|officielle|
|[consumable-01-nrAaCGSppPhPxOj3.htm](equipment/consumable-01-nrAaCGSppPhPxOj3.htm)|Sack of Rotten Fruit|Sac de fruits pourris|libre|
|[consumable-01-nybZVDfQWMhwvL2C.htm](equipment/consumable-01-nybZVDfQWMhwvL2C.htm)|Blast Boots (Lesser)|Bottes d'explosion inférieures|libre|
|[consumable-01-nz4riYwJiko98Mva.htm](equipment/consumable-01-nz4riYwJiko98Mva.htm)|Battering Snare|Piège artisanal à impact|libre|
|[consumable-01-OIirLySQDLZgT15S.htm](equipment/consumable-01-OIirLySQDLZgT15S.htm)|Arsenic|Arsenic|libre|
|[consumable-01-oplQpQSTyTvHDDtq.htm](equipment/consumable-01-oplQpQSTyTvHDDtq.htm)|Marking Snare|Piège artisanal marqueur|officielle|
|[consumable-01-pBq1cHXnic8dGxx8.htm](equipment/consumable-01-pBq1cHXnic8dGxx8.htm)|Journeybread|Pain de voyage|libre|
|[consumable-01-PLun5Enmp8ZbjogV.htm](equipment/consumable-01-PLun5Enmp8ZbjogV.htm)|Bookthief Brew|Infusion du voleur de livres|libre|
|[consumable-01-PNwgGnN081l0cETR.htm](equipment/consumable-01-PNwgGnN081l0cETR.htm)|Blood Sap|Sève sanglante|officielle|
|[consumable-01-qF2GDYQCpSZ12Nk7.htm](equipment/consumable-01-qF2GDYQCpSZ12Nk7.htm)|Ablative Shield Plating (Lesser)|Blindage ablatif de bouclier inférieur|libre|
|[consumable-01-qkPlUzyNLmLKVtBL.htm](equipment/consumable-01-qkPlUzyNLmLKVtBL.htm)|Lady's Blessing Oil|Huile de bénédiction de la Dame|libre|
|[consumable-01-qnvq3PSTiejQTSi9.htm](equipment/consumable-01-qnvq3PSTiejQTSi9.htm)|Ghost Ink|Encre sympathique|libre|
|[consumable-01-qoM7Va5GqcLLBzgu.htm](equipment/consumable-01-qoM7Va5GqcLLBzgu.htm)|Predator's Claw|Griffe de prédateur|libre|
|[consumable-01-RjuupS9xyXDLgyIr.htm](equipment/consumable-01-RjuupS9xyXDLgyIr.htm)|Scroll of 1st-rank Spell|Parchemin de sort de rang 1|libre|
|[consumable-01-SNMggNsvgPoAYjpU.htm](equipment/consumable-01-SNMggNsvgPoAYjpU.htm)|Addiction Suppressant (Lesser)|Suppresseur de dépendance inférieur|libre|
|[consumable-01-SrjykpB3nEAQmgj5.htm](equipment/consumable-01-SrjykpB3nEAQmgj5.htm)|Elsie's Excellent Bottled Vim|Excellent Vim en bouteille d'Elsie|libre|
|[consumable-01-tLa4bewBhyqzi6Ow.htm](equipment/consumable-01-tLa4bewBhyqzi6Ow.htm)|Cantrip Deck (5-pack)|Paquet de tours de magie de 5 cartes|libre|
|[consumable-01-tspcGx4OrZEv2gQX.htm](equipment/consumable-01-tspcGx4OrZEv2gQX.htm)|Merciful Balm|Baume miséricordieux|libre|
|[consumable-01-txmX5ghhPS72GKXy.htm](equipment/consumable-01-txmX5ghhPS72GKXy.htm)|Giant Centipede Venom|Venin de mille-pattes géant|libre|
|[consumable-01-tyt6rFtv32MZ4DT9.htm](equipment/consumable-01-tyt6rFtv32MZ4DT9.htm)|Cheetah's Elixir (Lesser)|Élixir du guépard inférieur|officielle|
|[consumable-01-U9XNSFQzCOo6KMD1.htm](equipment/consumable-01-U9XNSFQzCOo6KMD1.htm)|Ablative Armor Plating (Lesser)|Blindage ablatif d'armure inférieur|libre|
|[consumable-01-UaSVmCl0JQ76kiYY.htm](equipment/consumable-01-UaSVmCl0JQ76kiYY.htm)|Privacy Ward Fulu (Hallway)|Fulu de protection de la confidentialité (Couloir)|libre|
|[consumable-01-uG3xtNrs26scOVgW.htm](equipment/consumable-01-uG3xtNrs26scOVgW.htm)|Leaper's Elixir (Lesser)|Élixir du sauteur inférieur|officielle|
|[consumable-01-ugIxgGIkJh2aXE2N.htm](equipment/consumable-01-ugIxgGIkJh2aXE2N.htm)|Potion of Fire Retaliation (Minor)|Potion de riposte enflammée mineure|libre|
|[consumable-01-ukTlC4G83aVQEg7u.htm](equipment/consumable-01-ukTlC4G83aVQEg7u.htm)|Nectar of Purification|Nectar de purification|libre|
|[consumable-01-UqinuuCWePTYGhVO.htm](equipment/consumable-01-UqinuuCWePTYGhVO.htm)|Antiplague (Lesser)|Antimaladie inférieur|libre|
|[consumable-01-UYj4AxqiwtlTW8Kl.htm](equipment/consumable-01-UYj4AxqiwtlTW8Kl.htm)|Cryomister (Lesser)|Cryobrumisateur inférieur|libre|
|[consumable-01-V35j3JEJMUbuJZNX.htm](equipment/consumable-01-V35j3JEJMUbuJZNX.htm)|Numbing Tonic (Minor)|Tonique anesthésiant mineur|libre|
|[consumable-01-VeCNWhvEr82ZNoSV.htm](equipment/consumable-01-VeCNWhvEr82ZNoSV.htm)|Lastwall Soup|Soupe de Dernier-Rempart|libre|
|[consumable-01-VIHsu1q3078gdQut.htm](equipment/consumable-01-VIHsu1q3078gdQut.htm)|Sanguine Mutagen (Lesser)|Mutagène sanguin inférieur|libre|
|[consumable-01-vl5dww56cbXo9QnP.htm](equipment/consumable-01-vl5dww56cbXo9QnP.htm)|Grit|Grit|libre|
|[consumable-01-Vy3z2cIQ8uJRUMYw.htm](equipment/consumable-01-Vy3z2cIQ8uJRUMYw.htm)|Refined Pesh|Pesh raffiné|officielle|
|[consumable-01-w0S1SIRsHgGdRh1d.htm](equipment/consumable-01-w0S1SIRsHgGdRh1d.htm)|Potion of Electricity Retaliation (Minor)|Potion de riposte électrique mineure|libre|
|[consumable-01-wbr6rkyaVYnDhdgV.htm](equipment/consumable-01-wbr6rkyaVYnDhdgV.htm)|Cognitive Mutagen (Lesser)|Mutagène cognitif inférieur|officielle|
|[consumable-01-xBZCVHAa1SnR8Xul.htm](equipment/consumable-01-xBZCVHAa1SnR8Xul.htm)|Snake Oil|Huile de serpent|libre|
|[consumable-01-XcD8p1o71tPohZWT.htm](equipment/consumable-01-XcD8p1o71tPohZWT.htm)|Signaling Snare|Piège artisanal indicateur|officielle|
|[consumable-01-xE0EdDrf734l2fQH.htm](equipment/consumable-01-xE0EdDrf734l2fQH.htm)|Juggernaut Mutagen (Lesser)|Mutagène de juggernaut inférieur|officielle|
|[consumable-01-xTdrhiLqFYUllrpK.htm](equipment/consumable-01-xTdrhiLqFYUllrpK.htm)|Cantrip Deck (Full Pack)|Paquet de tours de magie complet|libre|
|[consumable-01-y4WJY8rCbY6d1MET.htm](equipment/consumable-01-y4WJY8rCbY6d1MET.htm)|Glow Rod|Tige éclairante|libre|
|[consumable-01-YeF05oItxHNdOi6z.htm](equipment/consumable-01-YeF05oItxHNdOi6z.htm)|Poison Barbs Snare|Pièges artisanal à piquants empoisonnés|libre|
|[consumable-01-yi1iL9dbLDSr4NZd.htm](equipment/consumable-01-yi1iL9dbLDSr4NZd.htm)|Vaccine (Minor)|Vaccin mineur|libre|
|[consumable-01-ylUdMTsfOQGJ3MN3.htm](equipment/consumable-01-ylUdMTsfOQGJ3MN3.htm)|Matchstick|Allumette|libre|
|[consumable-01-YwRAHWW8yUI07sy9.htm](equipment/consumable-01-YwRAHWW8yUI07sy9.htm)|Silvertongue Mutagen (Lesser)|Mutagène de langue dorée inférieur|officielle|
|[consumable-01-ZCsksGf6NPUKz2Uw.htm](equipment/consumable-01-ZCsksGf6NPUKz2Uw.htm)|Potency Crystal|Cristal de puissance|libre|
|[consumable-01-zExWH2EsY9STTORq.htm](equipment/consumable-01-zExWH2EsY9STTORq.htm)|Potion of Cold Retaliation (Minor)|Potion de riposte glaciale mineure|libre|
|[consumable-01-zHzAZRDVtl2NFqyh.htm](equipment/consumable-01-zHzAZRDVtl2NFqyh.htm)|Elemental Ammunition (Lesser)|Munition élémentaire inférieure|libre|
|[consumable-02-0aSdDSjJ5sMzBz1U.htm](equipment/consumable-02-0aSdDSjJ5sMzBz1U.htm)|Onyx Panther|Panthère d'onyx|libre|
|[consumable-02-0QiHkpL9mFwNU0Ts.htm](equipment/consumable-02-0QiHkpL9mFwNU0Ts.htm)|Spring-Loaded Net Launcher|Lance-filet à ressorts|libre|
|[consumable-02-1ZcywTbxv9UH13T3.htm](equipment/consumable-02-1ZcywTbxv9UH13T3.htm)|Wasul Reed Mask|Masque de roseau wasul|libre|
|[consumable-02-2MgFoNXTccL8Own9.htm](equipment/consumable-02-2MgFoNXTccL8Own9.htm)|Jade Cat|Chat de jade|libre|
|[consumable-02-3Klm2gPmzOw6ntVb.htm](equipment/consumable-02-3Klm2gPmzOw6ntVb.htm)|Comprehension Elixir (Lesser)|Élixir de compréhension inférieur|officielle|
|[consumable-02-5DiekAhNEjr6Vwi3.htm](equipment/consumable-02-5DiekAhNEjr6Vwi3.htm)|Traveler's Fulu|Fulu du voyageur|libre|
|[consumable-02-6DmHDtIsGzH1s5JO.htm](equipment/consumable-02-6DmHDtIsGzH1s5JO.htm)|Oil of Weightlessness|Huile d'apesanteur|libre|
|[consumable-02-88pGCHV0uKMskTVO.htm](equipment/consumable-02-88pGCHV0uKMskTVO.htm)|Darkvision Elixir (Lesser)|Élixir de vision dans le noir inférieur|libre|
|[consumable-02-8fbsKEEbZ0CfBMIr.htm](equipment/consumable-02-8fbsKEEbZ0CfBMIr.htm)|Silver Salve|Apprêt d'argent|libre|
|[consumable-02-9fKNH0ktIYs4f7Dl.htm](equipment/consumable-02-9fKNH0ktIYs4f7Dl.htm)|Clockwork Monkey|Singe mécanique|libre|
|[consumable-02-a05LWc1gKq0NwRfs.htm](equipment/consumable-02-a05LWc1gKq0NwRfs.htm)|Spun Cloud (White)|Nuage tourbillonnant blanc|libre|
|[consumable-02-AALcDOpwxCBN50lA.htm](equipment/consumable-02-AALcDOpwxCBN50lA.htm)|Lover's Ink|L'encre des amants|libre|
|[consumable-02-aBGKmzkHsaNH07Di.htm](equipment/consumable-02-aBGKmzkHsaNH07Di.htm)|Instant Spy|Espion instantané|libre|
|[consumable-02-aKbrBW1SnFDxya5J.htm](equipment/consumable-02-aKbrBW1SnFDxya5J.htm)|Mesmerizing Opal|Opale hypnotisante|libre|
|[consumable-02-bJVk50mhaODgsOUe.htm](equipment/consumable-02-bJVk50mhaODgsOUe.htm)|Yellow Musk Poison|Poison de Moisissure Jaune|libre|
|[consumable-02-bqbCL8ZTeAa9EfcR.htm](equipment/consumable-02-bqbCL8ZTeAa9EfcR.htm)|Noisemaker Snare|Piège artisanal bruyant|libre|
|[consumable-02-bQPRKEpnLakJBAAh.htm](equipment/consumable-02-bQPRKEpnLakJBAAh.htm)|Cat's Eye Elixir|Élixir d'oeil de chat|libre|
|[consumable-02-cHKqK8g9dS3GJ58F.htm](equipment/consumable-02-cHKqK8g9dS3GJ58F.htm)|Emetic Paste (Lesser)|Pâte vomitive inférieure|libre|
|[consumable-02-craWRj7jI2mLs1Ok.htm](equipment/consumable-02-craWRj7jI2mLs1Ok.htm)|Deadweight Snare|Piège artisanal du poids-mort|libre|
|[consumable-02-cTBmQgWUyf50x3dY.htm](equipment/consumable-02-cTBmQgWUyf50x3dY.htm)|Black Smear Poison|Poison d'enduit noir|libre|
|[consumable-02-cuomhpenkqGM5lLG.htm](equipment/consumable-02-cuomhpenkqGM5lLG.htm)|Infiltrator's Elixir|Élixir d'infiltration|officielle|
|[consumable-02-cwurQLvQaqjK70UI.htm](equipment/consumable-02-cwurQLvQaqjK70UI.htm)|Feather Token (Holly Bush)|Figurine merveilleuse buisson de houx|officielle|
|[consumable-02-e20qv4NhYsoTlXtw.htm](equipment/consumable-02-e20qv4NhYsoTlXtw.htm)|Cauterizing Torch|Torche de cautérisation|libre|
|[consumable-02-eVI81XML1c9jqhIx.htm](equipment/consumable-02-eVI81XML1c9jqhIx.htm)|Holy Steam Ball Refill|Recharge pour une bille de vapeur sacrée|libre|
|[consumable-02-FL8QU8TcNauBMMhD.htm](equipment/consumable-02-FL8QU8TcNauBMMhD.htm)|Belladonna|Belladone|libre|
|[consumable-02-fSO6mFltCGuSapsi.htm](equipment/consumable-02-fSO6mFltCGuSapsi.htm)|Ooze Ammunition (Lesser)|Munition de vase inférieure|libre|
|[consumable-02-gB9Qp85rpuNtPVGx.htm](equipment/consumable-02-gB9Qp85rpuNtPVGx.htm)|Sneezing Powder|Poudre à éternuer|libre|
|[consumable-02-gi1zuwWrwcW7OKlK.htm](equipment/consumable-02-gi1zuwWrwcW7OKlK.htm)|Sanitizing Pin|Broche d'assainissement|libre|
|[consumable-02-gYYlqHTW1lgwd6ri.htm](equipment/consumable-02-gYYlqHTW1lgwd6ri.htm)|Expulsion Snare|Piège artisanal d'expulsion|libre|
|[consumable-02-H3nAm1FFvuG7j4TN.htm](equipment/consumable-02-H3nAm1FFvuG7j4TN.htm)|Adaptive Cogwheel|Roue dentée adaptable|libre|
|[consumable-02-HHELOoN5GVonUiIa.htm](equipment/consumable-02-HHELOoN5GVonUiIa.htm)|Crying Angel Pendant|Pendentif de l'ange en pleurs|libre|
|[consumable-02-I0eUrgVGl8a2ZjFc.htm](equipment/consumable-02-I0eUrgVGl8a2ZjFc.htm)|Boulderhead Bock|Bock têtederoc|libre|
|[consumable-02-ICDQcwf2U7bSoieB.htm](equipment/consumable-02-ICDQcwf2U7bSoieB.htm)|Anticorrosion Oil|Huile anti-corrosion|libre|
|[consumable-02-iYsDDvFduPhtaIOr.htm](equipment/consumable-02-iYsDDvFduPhtaIOr.htm)|Aromatic Ammunition|Munition aromatique|libre|
|[consumable-02-j2CHumvbjmlLQX2i.htm](equipment/consumable-02-j2CHumvbjmlLQX2i.htm)|Oil of Potency|Huile de puissance|libre|
|[consumable-02-jdDDqv9LbEYX2wAE.htm](equipment/consumable-02-jdDDqv9LbEYX2wAE.htm)|Monkey Pin|Broche de singe|libre|
|[consumable-02-l8wky2NQduHYsY9B.htm](equipment/consumable-02-l8wky2NQduHYsY9B.htm)|Lover's Knot|Noeud d'amoureux|libre|
|[consumable-02-LniGjFmZyw90Rdcj.htm](equipment/consumable-02-LniGjFmZyw90Rdcj.htm)|Dark Pepper Powder|Poudre de poivre noir|libre|
|[consumable-02-mDJSaarQsIMX7Opi.htm](equipment/consumable-02-mDJSaarQsIMX7Opi.htm)|Bloodhound Mask (Lesser)|Masque du limier inférieur|libre|
|[consumable-02-mDNDs5P8ZAJXYbDL.htm](equipment/consumable-02-mDNDs5P8ZAJXYbDL.htm)|Toad Tears|Poison larmes de crapaud|libre|
|[consumable-02-mvrpdVyFJc5eOoE9.htm](equipment/consumable-02-mvrpdVyFJc5eOoE9.htm)|Bubbling Scale|Écaille de bulles|libre|
|[consumable-02-N3jcmW5XzEJZQVtJ.htm](equipment/consumable-02-N3jcmW5XzEJZQVtJ.htm)|Antivenom Potion|Potion anti-venin|libre|
|[consumable-02-npSWkkSsQfiKixPO.htm](equipment/consumable-02-npSWkkSsQfiKixPO.htm)|Origin Unguent|Onguent de détermination d'origine|libre|
|[consumable-02-NSQOijKqomyotXkj.htm](equipment/consumable-02-NSQOijKqomyotXkj.htm)|Antler Arrow|Flèche bois-de-cerf|libre|
|[consumable-02-nXStoLxPrrP2b6WB.htm](equipment/consumable-02-nXStoLxPrrP2b6WB.htm)|Bronze Bull Pendant|Pendentif de taureau de bronze|libre|
|[consumable-02-Oo0KZTmFsu9iCAR9.htm](equipment/consumable-02-Oo0KZTmFsu9iCAR9.htm)|Life Shot (Minor)|Tir de vie mineur|libre|
|[consumable-02-oZBafUU1tk4tgwye.htm](equipment/consumable-02-oZBafUU1tk4tgwye.htm)|Mender's Soup|Soupe du réparateur|libre|
|[consumable-02-p3RduziOxQxtREg9.htm](equipment/consumable-02-p3RduziOxQxtREg9.htm)|Looter's Lethargy|Torpeur du pilleur|libre|
|[consumable-02-PPd89jN2a4rFoVHm.htm](equipment/consumable-02-PPd89jN2a4rFoVHm.htm)|Moon Radish Soup|Soupe de radis lunaire|libre|
|[consumable-02-q5Hr2nZr8RQ9DSZM.htm](equipment/consumable-02-q5Hr2nZr8RQ9DSZM.htm)|Blaze|Brûlure|libre|
|[consumable-02-qeTWg0TWw9CwMKCO.htm](equipment/consumable-02-qeTWg0TWw9CwMKCO.htm)|Savior Spike|Pointe salvatrice|libre|
|[consumable-02-QyEp8sN9mn36K5El.htm](equipment/consumable-02-QyEp8sN9mn36K5El.htm)|Moonlit Ink|Encre clair de lune|libre|
|[consumable-02-R3AnwcaHru4PFHJ4.htm](equipment/consumable-02-R3AnwcaHru4PFHJ4.htm)|Winterstep Elixir (Minor)|Élixir de démarche hivernale mineure|libre|
|[consumable-02-riLNCaVS9zGvt4Nn.htm](equipment/consumable-02-riLNCaVS9zGvt4Nn.htm)|Flare Snare|Piège artisanal de fusée éclairante|libre|
|[consumable-02-ScclzFrjyB0YJlrb.htm](equipment/consumable-02-ScclzFrjyB0YJlrb.htm)|Black Adder Venom|Venin de vipère noire|libre|
|[consumable-02-sHBk2pY0I6dCW2Mw.htm](equipment/consumable-02-sHBk2pY0I6dCW2Mw.htm)|Black Powder (Horn)|Poudre noire (corne)|libre|
|[consumable-02-T28lI7becoK1ZGqr.htm](equipment/consumable-02-T28lI7becoK1ZGqr.htm)|Cooperative Waffles|Gaufres coopératives|libre|
|[consumable-02-T6jPmGyInYATYOzr.htm](equipment/consumable-02-T6jPmGyInYATYOzr.htm)|Smoke Fan|Souffleur de fumée|libre|
|[consumable-02-TUya8pXhEa70ahAi.htm](equipment/consumable-02-TUya8pXhEa70ahAi.htm)|Wind-Up Cart|Chariot à manivelle|libre|
|[consumable-02-vAMgDuD3BphJL8EJ.htm](equipment/consumable-02-vAMgDuD3BphJL8EJ.htm)|Ember Dust|Poussière de braise|libre|
|[consumable-02-VPvyyQXjn2HBjnTS.htm](equipment/consumable-02-VPvyyQXjn2HBjnTS.htm)|Effervescent Ampoule|Ampoule effervescente|libre|
|[consumable-02-wU8HH03cNxe6Pkfi.htm](equipment/consumable-02-wU8HH03cNxe6Pkfi.htm)|Moonlit Spellgun (Minor)|Lancesort clair de lune mineur|libre|
|[consumable-02-x4QLLKEShYqUjCCn.htm](equipment/consumable-02-x4QLLKEShYqUjCCn.htm)|Red-Rib Gill Mask (Lesser)|Masque branchies de côte-rouge inférieur|libre|
|[consumable-02-x6V1tlDDBTVj5XnW.htm](equipment/consumable-02-x6V1tlDDBTVj5XnW.htm)|Spirit Trap|Piège spirituel|libre|
|[consumable-02-Yew9oddFsH0KeDLh.htm](equipment/consumable-02-Yew9oddFsH0KeDLh.htm)|Hunter's Bane|Fléau du chasseur|officielle|
|[consumable-02-yUPRtHCpAPx3EBUq.htm](equipment/consumable-02-yUPRtHCpAPx3EBUq.htm)|Oil of Skating|Huile de patinage|libre|
|[consumable-02-yUYr8j65fC7EN0NY.htm](equipment/consumable-02-yUYr8j65fC7EN0NY.htm)|Thunder Snare|Piège artisanal tonitruant|libre|
|[consumable-02-zA2UPGE9RYwlLPZJ.htm](equipment/consumable-02-zA2UPGE9RYwlLPZJ.htm)|Soothing Tonic (Lesser)|Tonique apaisant inférieur|libre|
|[consumable-02-zle8PCLlI8jIkgiY.htm](equipment/consumable-02-zle8PCLlI8jIkgiY.htm)|Tripline Arrow|Flèche de trébuchement|libre|
|[consumable-02-zM9VX3QwM81DzDUA.htm](equipment/consumable-02-zM9VX3QwM81DzDUA.htm)|Bravo's Brew (Lesser)|Breuvage de bravoure inférieur|libre|
|[consumable-02-zo0ophqfKunJFxZN.htm](equipment/consumable-02-zo0ophqfKunJFxZN.htm)|Lethargy Poison|Poison de léthargie|libre|
|[consumable-03-03Jfj5QE7twaY5nW.htm](equipment/consumable-03-03Jfj5QE7twaY5nW.htm)|Clockwork Chirper|Gazouilleur mécanique|libre|
|[consumable-03-097NJos0T8o7AMLi.htm](equipment/consumable-03-097NJos0T8o7AMLi.htm)|Venomous Cure Fulu|Fulu curatif d'empoisonnement|libre|
|[consumable-03-0CNSvLpeSM4aIfPJ.htm](equipment/consumable-03-0CNSvLpeSM4aIfPJ.htm)|Feather Step Stone|Pierre de pas de plume|libre|
|[consumable-03-0rxTsnAGYWYaQaMl.htm](equipment/consumable-03-0rxTsnAGYWYaQaMl.htm)|Torrent Spellgun (Lesser)|Lancesort torrent inférieur|libre|
|[consumable-03-1AFWnVcoYvLcKtL9.htm](equipment/consumable-03-1AFWnVcoYvLcKtL9.htm)|Colorful Coating (Red)|Revêtement coloré (rouge)|libre|
|[consumable-03-1Nez8K5C4fwgFrTz.htm](equipment/consumable-03-1Nez8K5C4fwgFrTz.htm)|Recording Rod (Basic)|Tige d'enregistrement basique|libre|
|[consumable-03-20fPmTHmGRyYXIgm.htm](equipment/consumable-03-20fPmTHmGRyYXIgm.htm)|Setup Snare|Piège artisanal préparatoire|libre|
|[consumable-03-3GUiG3raeyIwW4av.htm](equipment/consumable-03-3GUiG3raeyIwW4av.htm)|Thumper Snare|Pière artisanal martelant|libre|
|[consumable-03-3tCtnomxBkdyG43X.htm](equipment/consumable-03-3tCtnomxBkdyG43X.htm)|Diplomat's Charcuterie|Charcuterie du diplomate|libre|
|[consumable-03-5iNNOlIqvN1zb8v7.htm](equipment/consumable-03-5iNNOlIqvN1zb8v7.htm)|Clinging Bubbles (Lesser)|Bulles accrochées inférieures|libre|
|[consumable-03-5KYn9J1Hj4IG3Z0X.htm](equipment/consumable-03-5KYn9J1Hj4IG3Z0X.htm)|Snapleaf|Bouton de feuille|libre|
|[consumable-03-5lhQ79p0Z3j1VsY3.htm](equipment/consumable-03-5lhQ79p0Z3j1VsY3.htm)|Matsuki's Medicinal Wine|Vin médicinal de Matsuki|libre|
|[consumable-03-67KiGZtoTMbl7nv2.htm](equipment/consumable-03-67KiGZtoTMbl7nv2.htm)|Potion of Fire Retaliation (Lesser)|Potion de riposte enflammée inférieure|libre|
|[consumable-03-6P6lDnIxK7plyCEI.htm](equipment/consumable-03-6P6lDnIxK7plyCEI.htm)|Swamp Lily Quilt|Couette de Lys|libre|
|[consumable-03-6VCaHBbUBk4eBlPC.htm](equipment/consumable-03-6VCaHBbUBk4eBlPC.htm)|Choker-Arm Mutagen (Moderate)|Mutagène d'assouplissement modéré|libre|
|[consumable-03-6XMuLT5QKkoznKz9.htm](equipment/consumable-03-6XMuLT5QKkoznKz9.htm)|Oil of Ownership (Lesser)|Huile de propriété inférieure|libre|
|[consumable-03-7WuJbX5c3SCB04Uw.htm](equipment/consumable-03-7WuJbX5c3SCB04Uw.htm)|Demolition Fulu (Lesser)|Fulu de démolition inférieur|libre|
|[consumable-03-8g2U2PMC0lkPZEvq.htm](equipment/consumable-03-8g2U2PMC0lkPZEvq.htm)|Blood Booster (Lesser)|Activateur sanguin inférieur|libre|
|[consumable-03-8ijfXMMKWeUp6yoO.htm](equipment/consumable-03-8ijfXMMKWeUp6yoO.htm)|Putrid Sack of Rotting Fruit|Sac putride de fruit pourri|libre|
|[consumable-03-8rQ7HkUtSa8so0Et.htm](equipment/consumable-03-8rQ7HkUtSa8so0Et.htm)|Theatrical Mutagen (Moderate)|Mutagène théâtral modéré|libre|
|[consumable-03-9FiSGqe8s8531Tyh.htm](equipment/consumable-03-9FiSGqe8s8531Tyh.htm)|Spirit-Sealing Fulu|Fulu scelle-esprit|libre|
|[consumable-03-A4mt1d9CK9lpLiqa.htm](equipment/consumable-03-A4mt1d9CK9lpLiqa.htm)|Colorful Coating (Orange)|Revêtement coloré (orange)|libre|
|[consumable-03-AFFaFaAW3IbbYqkL.htm](equipment/consumable-03-AFFaFaAW3IbbYqkL.htm)|Life-Boosting Oil (Lesser)|Huile stimulatrice de vie inférieure|libre|
|[consumable-03-aJrMyopbSzvXn5Zl.htm](equipment/consumable-03-aJrMyopbSzvXn5Zl.htm)|Colorful Coating (Blue)|Revêtement coloré (bleu)|libre|
|[consumable-03-ANQz5rzp5kKe7ul6.htm](equipment/consumable-03-ANQz5rzp5kKe7ul6.htm)|Toadskin Salve|Baume Peau de crapaud|libre|
|[consumable-03-AOAXZzH4AOsQqYYv.htm](equipment/consumable-03-AOAXZzH4AOsQqYYv.htm)|War Blood Mutagen (Moderate)|Mutagène de sang guerrier modéré|libre|
|[consumable-03-aUzX848Ygqicy2qs.htm](equipment/consumable-03-aUzX848Ygqicy2qs.htm)|Drowsy Sun Eye Drops|Collyre du soleil somnolent|libre|
|[consumable-03-aY6Ukb7ZsMDelhmH.htm](equipment/consumable-03-aY6Ukb7ZsMDelhmH.htm)|Ablative Shield Plating (Moderate)|Blindage ablatif de bouclier modéré|libre|
|[consumable-03-aZm1x9tpvBAT8YCd.htm](equipment/consumable-03-aZm1x9tpvBAT8YCd.htm)|Cytillesh Oil|Huile de cytillesh|officielle|
|[consumable-03-aZSuB6RTb14XE2N3.htm](equipment/consumable-03-aZSuB6RTb14XE2N3.htm)|Candle of Inflamed Passions|Chandelle des passions enflammées|libre|
|[consumable-03-bMbWbKEbKSmVgoGT.htm](equipment/consumable-03-bMbWbKEbKSmVgoGT.htm)|Breech Ejectors|Éjecteurs de culasse|libre|
|[consumable-03-BSInwFNVBVkfFK0B.htm](equipment/consumable-03-BSInwFNVBVkfFK0B.htm)|Oil of Unlife (Lesser)|Huile de non-vie inférieure|libre|
|[consumable-03-bvT6uMBD8MALvZAZ.htm](equipment/consumable-03-bvT6uMBD8MALvZAZ.htm)|Tailor's Boll|Balle du tailleur|libre|
|[consumable-03-ByaYlK0QmnqEbCE2.htm](equipment/consumable-03-ByaYlK0QmnqEbCE2.htm)|Covenant Tea|Thé d'engagement|libre|
|[consumable-03-C1mX0irc7zvLqDEs.htm](equipment/consumable-03-C1mX0irc7zvLqDEs.htm)|Witch's Finger|Doigt de sorcière|libre|
|[consumable-03-C8kQuf7gqpjEDmib.htm](equipment/consumable-03-C8kQuf7gqpjEDmib.htm)|Merciful Charm|Charme miséricordieux|libre|
|[consumable-03-cImNgwDCqwa6Dil1.htm](equipment/consumable-03-cImNgwDCqwa6Dil1.htm)|Cold Iron Blanch (Lesser)|Blanchis de fer froid inférieur|libre|
|[consumable-03-cVntDkkZP9mcuFHU.htm](equipment/consumable-03-cVntDkkZP9mcuFHU.htm)|Trustworthy Round|Munition fiable|libre|
|[consumable-03-CwFqyOn3h5aFdF8V.htm](equipment/consumable-03-CwFqyOn3h5aFdF8V.htm)|Seeking Bracelets|Bracelets de recherche|libre|
|[consumable-03-cYkmDxRFtwQRvwVh.htm](equipment/consumable-03-cYkmDxRFtwQRvwVh.htm)|Blindpepper Bolt|Carreau de poivre aveuglant|libre|
|[consumable-03-Dn2KQgIeWNijaUzL.htm](equipment/consumable-03-Dn2KQgIeWNijaUzL.htm)|Marvelous Miniature (Chest)|Figurine merveilleuse coffre|libre|
|[consumable-03-DnFmNx99xYsYxNfO.htm](equipment/consumable-03-DnFmNx99xYsYxNfO.htm)|Rock Ripper Snare|Piège artisanal de rochers éventreurs|libre|
|[consumable-03-e0vSAQfxhHauiAoD.htm](equipment/consumable-03-e0vSAQfxhHauiAoD.htm)|Healing Potion (Lesser)|Potion de guérison inférieure|libre|
|[consumable-03-EbqQbozqVGEVxEG9.htm](equipment/consumable-03-EbqQbozqVGEVxEG9.htm)|Colorful Coating (Green)|Revêtement coloré (vert)|libre|
|[consumable-03-eEnzHpPEbdGgRETM.htm](equipment/consumable-03-eEnzHpPEbdGgRETM.htm)|Vine Arrow|Flèche de lierre|libre|
|[consumable-03-F5LJiS0wcwSqRrQ9.htm](equipment/consumable-03-F5LJiS0wcwSqRrQ9.htm)|Revealing Mist (Lesser)|Brume révélatrice inférieure|libre|
|[consumable-03-F8jLnpBuvzM0nBp4.htm](equipment/consumable-03-F8jLnpBuvzM0nBp4.htm)|Bane Oil|Huile tueuse|libre|
|[consumable-03-fFq8nsGvSUgzVeND.htm](equipment/consumable-03-fFq8nsGvSUgzVeND.htm)|Feather Token (Bird)|Figurine merveilleuse oiseau|libre|
|[consumable-03-FVB9noeUXXyau029.htm](equipment/consumable-03-FVB9noeUXXyau029.htm)|Blast Boots (Moderate)|Bottes d'explosion modérées|libre|
|[consumable-03-GNM1LvqENq8WqT9C.htm](equipment/consumable-03-GNM1LvqENq8WqT9C.htm)|Tracker's Stew|Ragoût du pisteur|libre|
|[consumable-03-guUmnzyZVuzJO9oD.htm](equipment/consumable-03-guUmnzyZVuzJO9oD.htm)|Recovery Bladder|Vessie de récupération|libre|
|[consumable-03-gXqU9CInB05jdxh6.htm](equipment/consumable-03-gXqU9CInB05jdxh6.htm)|Fire Box|Boîte à feu|libre|
|[consumable-03-hOemslrdMyHCgSBk.htm](equipment/consumable-03-hOemslrdMyHCgSBk.htm)|Impossible Cake|Gâteau de l'impossible|libre|
|[consumable-03-hoX2sJiYtJrSp6BH.htm](equipment/consumable-03-hoX2sJiYtJrSp6BH.htm)|Psychic Warding Bracelet|Bracelet de protection psychique|libre|
|[consumable-03-HuyWiPnkDZoAVhW9.htm](equipment/consumable-03-HuyWiPnkDZoAVhW9.htm)|Spiderfoot Brew (Lesser)|Infusion de pattes d'araignée inférieure|libre|
|[consumable-03-HVPbv5n2e1vcT9Ug.htm](equipment/consumable-03-HVPbv5n2e1vcT9Ug.htm)|Retrieval Prism|Prisme d'extraction|libre|
|[consumable-03-hw81uONdpyUbdWUJ.htm](equipment/consumable-03-hw81uONdpyUbdWUJ.htm)|Dream Pollen Snare|Piège artisanal de pollen onirique|libre|
|[consumable-03-hxYxRz9nOECtLak5.htm](equipment/consumable-03-hxYxRz9nOECtLak5.htm)|Grasping Tree|Piège artisanal de l'arbre agrippant|libre|
|[consumable-03-ibxFkN7NGB1pybWx.htm](equipment/consumable-03-ibxFkN7NGB1pybWx.htm)|Indomitable Keepsake|Souvenir indomptable|libre|
|[consumable-03-If2zV787OCMXVTUS.htm](equipment/consumable-03-If2zV787OCMXVTUS.htm)|Captivating Rosebud|Bouton de rose captivant|libre|
|[consumable-03-iNR6t5dDiYGQYTeA.htm](equipment/consumable-03-iNR6t5dDiYGQYTeA.htm)|Quickpatch Glue|Colle rustine|libre|
|[consumable-03-jIlFuZaIgMPEzoyP.htm](equipment/consumable-03-jIlFuZaIgMPEzoyP.htm)|Matchmaker Fulu|Fulu entremetteur|libre|
|[consumable-03-JiMljGYKx60DHs8O.htm](equipment/consumable-03-JiMljGYKx60DHs8O.htm)|Prey Mutagen (Moderate)|Mutagène proie modéré|libre|
|[consumable-03-k5A8zFQuDtGqNnk5.htm](equipment/consumable-03-k5A8zFQuDtGqNnk5.htm)|Toothwort Extract|Extrait de cardamine|libre|
|[consumable-03-K6D2Ld8Md6PVsAaV.htm](equipment/consumable-03-K6D2Ld8Md6PVsAaV.htm)|Serpent Oil (Lesser)|Huile de serpent inférieure|libre|
|[consumable-03-kO31GgqGY37MDRff.htm](equipment/consumable-03-kO31GgqGY37MDRff.htm)|Periscopic Viewfinder|Viseur périscopique|libre|
|[consumable-03-KPKmM9qSdQ5X587J.htm](equipment/consumable-03-KPKmM9qSdQ5X587J.htm)|Colorful Coating (Yellow)|Revêtement coloré (jaune)|libre|
|[consumable-03-KuTwNrDpuYVAPu7c.htm](equipment/consumable-03-KuTwNrDpuYVAPu7c.htm)|Bralani Breath|Souffle bralani|libre|
|[consumable-03-Lbe9L9ZDBJgjDIhT.htm](equipment/consumable-03-Lbe9L9ZDBJgjDIhT.htm)|Sprite Apple (Golden)|Pomme sprite (dorée)|libre|
|[consumable-03-lgqyRmQ3GZbbxQsD.htm](equipment/consumable-03-lgqyRmQ3GZbbxQsD.htm)|Vermin Repellent Agent (Lesser)|Agent répulsif de vermine inférieur|libre|
|[consumable-03-lIExlUFBKvBue8hb.htm](equipment/consumable-03-lIExlUFBKvBue8hb.htm)|Silvertongue Mutagen (Moderate)|Mutagène de langue dorée modéré|officielle|
|[consumable-03-llqztgP14IJGSxU4.htm](equipment/consumable-03-llqztgP14IJGSxU4.htm)|Metalmist Sphere (Lesser)|Sphère de brume-métal inférieure|libre|
|[consumable-03-M1rX9EahXV9pa0Ui.htm](equipment/consumable-03-M1rX9EahXV9pa0Ui.htm)|Disrupting Oil|Huile Vitalisante|libre|
|[consumable-03-mj9i9GeQTADByNPZ.htm](equipment/consumable-03-mj9i9GeQTADByNPZ.htm)|Juggernaut Mutagen (Moderate)|Mutagène de juggernaut modéré|officielle|
|[consumable-03-MMloq9cUKNJYCDLW.htm](equipment/consumable-03-MMloq9cUKNJYCDLW.htm)|Colorful Coating (Indigo)|Revêtement coloré (indigo)|libre|
|[consumable-03-mYVUS8p9gA3mCLES.htm](equipment/consumable-03-mYVUS8p9gA3mCLES.htm)|Ectoplasmic Tracer|Traceur ectoplasmique|libre|
|[consumable-03-n52BSbZsnx4Vmt2p.htm](equipment/consumable-03-n52BSbZsnx4Vmt2p.htm)|Quicksilver Mutagen (Moderate)|Mutagène de vif-argent modéré|officielle|
|[consumable-03-n5xlR7ApXgfJRkbF.htm](equipment/consumable-03-n5xlR7ApXgfJRkbF.htm)|Longnight Tea|Thé de la longue nuit|libre|
|[consumable-03-nPWDuoe2PcgE0z2S.htm](equipment/consumable-03-nPWDuoe2PcgE0z2S.htm)|Torrent Snare|Piège artisanal torrentiel|libre|
|[consumable-03-NSo0bFX7DGGjqKKl.htm](equipment/consumable-03-NSo0bFX7DGGjqKKl.htm)|Spellstrike Ammunition (Type I)|Munition de frappe magique (Type I)|libre|
|[consumable-03-nzeTcOyQmZNrurVF.htm](equipment/consumable-03-nzeTcOyQmZNrurVF.htm)|Vaccine (Lesser)|Vaccin inférieur|libre|
|[consumable-03-OCTireuX60MaPcEi.htm](equipment/consumable-03-OCTireuX60MaPcEi.htm)|Special Ingredient|Ingrédient spécial|libre|
|[consumable-03-Oip645RjC5y57wFa.htm](equipment/consumable-03-Oip645RjC5y57wFa.htm)|Violet Venom|Venin violet|libre|
|[consumable-03-omRmCbkQoK5YmCGv.htm](equipment/consumable-03-omRmCbkQoK5YmCGv.htm)|Potion of Acid Retaliation (Lesser)|Potion de riposte acide inférieure|libre|
|[consumable-03-OzSfVy1SoKzCWG8q.htm](equipment/consumable-03-OzSfVy1SoKzCWG8q.htm)|Draft of Stellar Radiance|Ébauche d'éclat stellaire|libre|
|[consumable-03-P7Evi8Wv8zIwLYtU.htm](equipment/consumable-03-P7Evi8Wv8zIwLYtU.htm)|Ablative Armor Plating (Moderate)|Blindage ablatif d'armure modéré|libre|
|[consumable-03-pGnoCqFy0ESPbuGv.htm](equipment/consumable-03-pGnoCqFy0ESPbuGv.htm)|Aurifying Salts|Sels aurifères|libre|
|[consumable-03-pP7RqEkNYg5ayWqx.htm](equipment/consumable-03-pP7RqEkNYg5ayWqx.htm)|Sanguine Mutagen (Moderate)|Mutagène sanguin modéré|libre|
|[consumable-03-qhn0PSd5TPGpblbg.htm](equipment/consumable-03-qhn0PSd5TPGpblbg.htm)|Forgetful Drops|Gouttes oublieuses|libre|
|[consumable-03-qLwwxk1j04rgXSF9.htm](equipment/consumable-03-qLwwxk1j04rgXSF9.htm)|Deadweight Mutagen (Moderate)|Mutagène de poids-mort modéré|libre|
|[consumable-03-QN8UIz0nMcnLUWHu.htm](equipment/consumable-03-QN8UIz0nMcnLUWHu.htm)|Oil of Mending|Huile de réparation|libre|
|[consumable-03-qpzL9UnTi4cDhy6J.htm](equipment/consumable-03-qpzL9UnTi4cDhy6J.htm)|Cognitive Mutagen (Moderate)|Mutagène cognitif modéré|officielle|
|[consumable-03-qQ9Rros3GdPDRLBg.htm](equipment/consumable-03-qQ9Rros3GdPDRLBg.htm)|Auric Noodles|Nouilles auriques|libre|
|[consumable-03-qQqAY59NgGNoy2xr.htm](equipment/consumable-03-qQqAY59NgGNoy2xr.htm)|Graveroot|Racine des tombes|libre|
|[consumable-03-Qx6CAAbSQ0RpruJH.htm](equipment/consumable-03-Qx6CAAbSQ0RpruJH.htm)|Hoof Stakes Snare|Piège artisanal à épieux sabots|libre|
|[consumable-03-qzT0myaHAiSqnwWW.htm](equipment/consumable-03-qzT0myaHAiSqnwWW.htm)|Parchment of Secrets|Parchemin des secrets|libre|
|[consumable-03-R0gsYr0jD1giTzpx.htm](equipment/consumable-03-R0gsYr0jD1giTzpx.htm)|Olfactory Obfuscator (Lesser)|Embrouilleur olfactif inférieur|libre|
|[consumable-03-RBYn6syZKENT0UiZ.htm](equipment/consumable-03-RBYn6syZKENT0UiZ.htm)|Camouflage Dye (Lesser)|Teinture de camouflage inférieure|libre|
|[consumable-03-RcQ4ZIzRK2xLf4G5.htm](equipment/consumable-03-RcQ4ZIzRK2xLf4G5.htm)|Sleep Arrow|Flèche soporifique|officielle|
|[consumable-03-rfyWXgrVD2lm91CC.htm](equipment/consumable-03-rfyWXgrVD2lm91CC.htm)|Blue Dragonfly Poison|Poison de libellule bleue|libre|
|[consumable-03-RmVh6urcDFWrcLxq.htm](equipment/consumable-03-RmVh6urcDFWrcLxq.htm)|Feather Token (Ladder) (Ammunition)|Figurine merveilleuse échelle (Munition)|libre|
|[consumable-03-s0btfYXKuufJhbyx.htm](equipment/consumable-03-s0btfYXKuufJhbyx.htm)|Stumbling Fulu|Fulu trébuchant|libre|
|[consumable-03-SHQv4yntEoU4uYRJ.htm](equipment/consumable-03-SHQv4yntEoU4uYRJ.htm)|Conrasu Coin (Arbiter)|Pièce conrasu (Arbitre)|libre|
|[consumable-03-SLJTJwyIRjuWjfuK.htm](equipment/consumable-03-SLJTJwyIRjuWjfuK.htm)|Energy Mutagen (Moderate)|Mutagène d'énergie modéré|libre|
|[consumable-03-taAjenWKjBJpQyrE.htm](equipment/consumable-03-taAjenWKjBJpQyrE.htm)|Beacon Shot|Signal lumineux|libre|
|[consumable-03-TG4SnQCGOsgQfrE1.htm](equipment/consumable-03-TG4SnQCGOsgQfrE1.htm)|Potion of Electricity Retaliation (Lesser)|Potion de riposte électrique inférieure|libre|
|[consumable-03-uCt7Kc2MXzRMc0GD.htm](equipment/consumable-03-uCt7Kc2MXzRMc0GD.htm)|Soothing Scents|Senteurs apaisantes|libre|
|[consumable-03-UfoaNXFA7AWZiXxI.htm](equipment/consumable-03-UfoaNXFA7AWZiXxI.htm)|Shiver|Grelotte|officielle|
|[consumable-03-UJWiN0K3jqVjxvKk.htm](equipment/consumable-03-UJWiN0K3jqVjxvKk.htm)|Magic Wand (1st-Rank Spell)|Baguette magique (sort de rang 1)|libre|
|[consumable-03-Uq3rnsAHbLKyW49E.htm](equipment/consumable-03-Uq3rnsAHbLKyW49E.htm)|Potion of Cold Retaliation (Lesser)|Potion de riposte glaciale inférieure|libre|
|[consumable-03-V18pnbAay4iG7O6C.htm](equipment/consumable-03-V18pnbAay4iG7O6C.htm)|Detonating Gears Snare|Piège artisanal d'engrenages détonant|libre|
|[consumable-03-vbH19NIBChzdEn4R.htm](equipment/consumable-03-vbH19NIBChzdEn4R.htm)|Lodestone Pellet|Granulé aimanté|libre|
|[consumable-03-VISk5uLPVIvNWovB.htm](equipment/consumable-03-VISk5uLPVIvNWovB.htm)|Bestial Mutagen (Moderate)|Mutagène bestial modéré|officielle|
|[consumable-03-vQ2XU6y1MBVDtQgr.htm](equipment/consumable-03-vQ2XU6y1MBVDtQgr.htm)|Feather Token (Puddle)|Figurine merveilleuse flaque|libre|
|[consumable-03-WETEtFdpuXa1c7Zc.htm](equipment/consumable-03-WETEtFdpuXa1c7Zc.htm)|Neophyte's Fipple|Flûte à bec du néophyte|libre|
|[consumable-03-WKp751kO2ba4qPeN.htm](equipment/consumable-03-WKp751kO2ba4qPeN.htm)|Ranging Shot|Projectile de portée|libre|
|[consumable-03-WQhnfj1LbrEzvh8z.htm](equipment/consumable-03-WQhnfj1LbrEzvh8z.htm)|Potion of Water Breathing|Potion de respiration aquatique|libre|
|[consumable-03-X345VfEx9DZwO47G.htm](equipment/consumable-03-X345VfEx9DZwO47G.htm)|Alloy Orb (Low-Grade)|Orbe d'alliage de basse qualité|libre|
|[consumable-03-XEYveTvLH1lJ4jeI.htm](equipment/consumable-03-XEYveTvLH1lJ4jeI.htm)|Serene Mutagen (Moderate)|Mutagène de sérénité modéré|officielle|
|[consumable-03-xY2MogTwH9Fd8UPG.htm](equipment/consumable-03-xY2MogTwH9Fd8UPG.htm)|Drakeheart Mutagen (Moderate)|Mutagène de coeur de drake modéré|libre|
|[consumable-03-Y7UD64foDbDMV9sx.htm](equipment/consumable-03-Y7UD64foDbDMV9sx.htm)|Scroll of 2nd-rank Spell|Parchemin de sort de rang 2|libre|
|[consumable-03-Y7VAygKtMnE6HaXJ.htm](equipment/consumable-03-Y7VAygKtMnE6HaXJ.htm)|Artevil Suspension|Poudre émétique|libre|
|[consumable-03-zAfSy6fshSGUtYF6.htm](equipment/consumable-03-zAfSy6fshSGUtYF6.htm)|Seventh Prism (Triangular)|Septième prisme triangulaire|libre|
|[consumable-03-ZVaiCVMdkakXagIU.htm](equipment/consumable-03-ZVaiCVMdkakXagIU.htm)|Waterproofing Wax|Cire imperméable|libre|
|[consumable-04-0lhh2l4kh3QrwYH9.htm](equipment/consumable-04-0lhh2l4kh3QrwYH9.htm)|Hobbling Snare|Piège artisanal titubeur|libre|
|[consumable-04-0RTsyWutvO42O0zK.htm](equipment/consumable-04-0RTsyWutvO42O0zK.htm)|Applereed Mutagen (Lesser)|Mutagène pousse-roseau inférieur|libre|
|[consumable-04-1B0672tSm46YQXSh.htm](equipment/consumable-04-1B0672tSm46YQXSh.htm)|Rattling Bolt|Carreau cliquetant|libre|
|[consumable-04-1Cj3rG1ugNPcWaPA.htm](equipment/consumable-04-1Cj3rG1ugNPcWaPA.htm)|Acid Spitter|Cracheur d'acide|libre|
|[consumable-04-1oWATIKLaZBXzw9N.htm](equipment/consumable-04-1oWATIKLaZBXzw9N.htm)|Boom Snare|Piège artisanal détonnant|libre|
|[consumable-04-2EigJwHDmem3YggO.htm](equipment/consumable-04-2EigJwHDmem3YggO.htm)|Ice Slick Snare|Piège artisanal de glace glissante|libre|
|[consumable-04-2n8pySCWPa5s5LX9.htm](equipment/consumable-04-2n8pySCWPa5s5LX9.htm)|Lion Badge|Badge du lion|libre|
|[consumable-04-3AjyXlvLUx4c2RCi.htm](equipment/consumable-04-3AjyXlvLUx4c2RCi.htm)|Sprite Apple (Pink)|Pomme sprite (rose)|libre|
|[consumable-04-3Fmbw9wJkqZBV9De.htm](equipment/consumable-04-3Fmbw9wJkqZBV9De.htm)|Feather Token (Fan)|Figurine merveilleuse éventail|libre|
|[consumable-04-3v03D7qP6ewN03SN.htm](equipment/consumable-04-3v03D7qP6ewN03SN.htm)|Admonishing Band|Bande de semonce|libre|
|[consumable-04-553H2rzgaHeY1VB4.htm](equipment/consumable-04-553H2rzgaHeY1VB4.htm)|Ghost Ampoule|Ampoule fantôme|libre|
|[consumable-04-6Frd3SmMZZ49DnUZ.htm](equipment/consumable-04-6Frd3SmMZZ49DnUZ.htm)|Clockwork Goggles (Greater)|Lunettes mécaniques supérieures|libre|
|[consumable-04-6G088rA3TjiuQ5NA.htm](equipment/consumable-04-6G088rA3TjiuQ5NA.htm)|Healing Vapor (Lesser)|Vapeur de guérison inférieure|libre|
|[consumable-04-76vu5p2S0wN77fJw.htm](equipment/consumable-04-76vu5p2S0wN77fJw.htm)|Fire-Douse Snare|Piège artisanal extincteur|libre|
|[consumable-04-77D3QdU33DLcwNrJ.htm](equipment/consumable-04-77D3QdU33DLcwNrJ.htm)|Irritating Thorn Snare|Piège artisanal d'épines irritantes|libre|
|[consumable-04-7Sm8NhvQcwi84Nsh.htm](equipment/consumable-04-7Sm8NhvQcwi84Nsh.htm)|Journeybread (Power)|Pain de voyage puissant|libre|
|[consumable-04-89955RkZoEmVNo3V.htm](equipment/consumable-04-89955RkZoEmVNo3V.htm)|Anointing Oil|Huile de dernière onction|libre|
|[consumable-04-9EZb1hmSKOGrU4Cf.htm](equipment/consumable-04-9EZb1hmSKOGrU4Cf.htm)|Biting Snare|Piège artisanal à mâchoires|officielle|
|[consumable-04-ALfqpWLbLPSXxZVG.htm](equipment/consumable-04-ALfqpWLbLPSXxZVG.htm)|Grindlegrub Steak|Steak d'hachticot|libre|
|[consumable-04-ALS7pobHFmOnR4yX.htm](equipment/consumable-04-ALS7pobHFmOnR4yX.htm)|Fearflower Nectar|Nectar de fleur d'effroi|libre|
|[consumable-04-aQ4Q8Rq6ntWaHcIv.htm](equipment/consumable-04-aQ4Q8Rq6ntWaHcIv.htm)|Fortifying Pebble|Caillou de fortification|officielle|
|[consumable-04-awavOTGutIrLRQyq.htm](equipment/consumable-04-awavOTGutIrLRQyq.htm)|Phantom Roll|Rouleau fantôme|libre|
|[consumable-04-bc3orvzCjY28HeQi.htm](equipment/consumable-04-bc3orvzCjY28HeQi.htm)|Mortalis Coin|Pièce mortalis|libre|
|[consumable-04-BeX3ForBrKTQD4T5.htm](equipment/consumable-04-BeX3ForBrKTQD4T5.htm)|Fury Cocktail (Lesser)|Cocktail de la fureur inférieur|libre|
|[consumable-04-bikFUFRLwfdvX2x2.htm](equipment/consumable-04-bikFUFRLwfdvX2x2.htm)|Invisibility Potion|Potion d'invisibilité|libre|
|[consumable-04-CM0Hq7KKCwUv0BV6.htm](equipment/consumable-04-CM0Hq7KKCwUv0BV6.htm)|Impact Foam Chassis (Moderate)|Armature de mousse modérée|libre|
|[consumable-04-CrIolPhNv3W9lJns.htm](equipment/consumable-04-CrIolPhNv3W9lJns.htm)|Bloodhammer Reserve|Réserve Sangmartel|libre|
|[consumable-04-dwaF9zHPK9823COa.htm](equipment/consumable-04-dwaF9zHPK9823COa.htm)|Dreamtime Tea|Thé des songes|officielle|
|[consumable-04-Ekk7o1gPu8RotixD.htm](equipment/consumable-04-Ekk7o1gPu8RotixD.htm)|Salamander Elixir (Lesser)|Élixir de la salamandre inférieur|officielle|
|[consumable-04-eKpL2j1JA92wndO6.htm](equipment/consumable-04-eKpL2j1JA92wndO6.htm)|Zerk|Zerk|officielle|
|[consumable-04-eONI7XvfdGF34z74.htm](equipment/consumable-04-eONI7XvfdGF34z74.htm)|Feather Token (Holly Bush) (Ammunition)|Figurine merveilleuse buisson de houx (Munition)|libre|
|[consumable-04-eyWT7VKdba72hKMw.htm](equipment/consumable-04-eyWT7VKdba72hKMw.htm)|Colorful Coating (Violet)|Revêtement coloré violet|libre|
|[consumable-04-FqbDpztscJfM4XMe.htm](equipment/consumable-04-FqbDpztscJfM4XMe.htm)|Shrinking Potion|Potion de rétrécissement|libre|
|[consumable-04-GnXKCkgZQG0UmuHz.htm](equipment/consumable-04-GnXKCkgZQG0UmuHz.htm)|Dragon Turtle Scale|Écaille de tortue dragon|libre|
|[consumable-04-GyO89RBVjAKFxsFm.htm](equipment/consumable-04-GyO89RBVjAKFxsFm.htm)|Mistform Elixir (Lesser)|Élixir de brumeforme inférieur|libre|
|[consumable-04-gza0CcZEYhPKIYEk.htm](equipment/consumable-04-gza0CcZEYhPKIYEk.htm)|Spider Mold|Moisissure d'araignée|libre|
|[consumable-04-Ha6n30Tj3TNru9Dj.htm](equipment/consumable-04-Ha6n30Tj3TNru9Dj.htm)|Timeless Salts|Sels intemporels|libre|
|[consumable-04-hKed91BOXjyaNG3x.htm](equipment/consumable-04-hKed91BOXjyaNG3x.htm)|Oozepick|Crochetvase|libre|
|[consumable-04-Hm0dZYfXHMNcTCo3.htm](equipment/consumable-04-Hm0dZYfXHMNcTCo3.htm)|Cinnamon Seers|Voyants à la cannelle|libre|
|[consumable-04-hSJWv6aOwlJyKSLs.htm](equipment/consumable-04-hSJWv6aOwlJyKSLs.htm)|Irondust Stew|Ragoût poudrefer|libre|
|[consumable-04-I2FmQuFJiJYmsTUd.htm](equipment/consumable-04-I2FmQuFJiJYmsTUd.htm)|Magical Lock Fulu|Fulu de verrouillage|libre|
|[consumable-04-ilB279mxqXnlaSFj.htm](equipment/consumable-04-ilB279mxqXnlaSFj.htm)|Viper Arrow|Flèche vipère|officielle|
|[consumable-04-JkQsRx2vwFqLLJmm.htm](equipment/consumable-04-JkQsRx2vwFqLLJmm.htm)|Oxygen Ooze|Vase d'oxygène|libre|
|[consumable-04-k6D64EAjcKMf8NZB.htm](equipment/consumable-04-k6D64EAjcKMf8NZB.htm)|Bloodseeker Beak|Trompe de cherchesang|libre|
|[consumable-04-L1tqtTYU8kk7G7h6.htm](equipment/consumable-04-L1tqtTYU8kk7G7h6.htm)|Glue Bullet|Balle de colle|libre|
|[consumable-04-LlmRVR4qv6H2AGPT.htm](equipment/consumable-04-LlmRVR4qv6H2AGPT.htm)|Golden-Cased Bullet (Standard)|Étui de balle doré|libre|
|[consumable-04-MIP28AmODXgyF2D2.htm](equipment/consumable-04-MIP28AmODXgyF2D2.htm)|Ghost Oil|Huile fantôme|libre|
|[consumable-04-n5L7HE9H8jn8ftQy.htm](equipment/consumable-04-n5L7HE9H8jn8ftQy.htm)|Climbing Bolt|Carreau d'escalade|officielle|
|[consumable-04-n9VrmK9Us0viv20P.htm](equipment/consumable-04-n9VrmK9Us0viv20P.htm)|Winter Wolf Elixir (Lesser)|Élixir de loup arctique inférieur|officielle|
|[consumable-04-NAkbCs6NqqPWnp5H.htm](equipment/consumable-04-NAkbCs6NqqPWnp5H.htm)|Brightshade|Ombrevive|libre|
|[consumable-04-NK0wjH9m32sjaeQe.htm](equipment/consumable-04-NK0wjH9m32sjaeQe.htm)|Messenger Missive|Missive messager|libre|
|[consumable-04-NtUB9xqRZQ2duOUo.htm](equipment/consumable-04-NtUB9xqRZQ2duOUo.htm)|Glittering Snare|Piège artisanal scintillant|libre|
|[consumable-04-NtUFIAMm2raeq5lr.htm](equipment/consumable-04-NtUFIAMm2raeq5lr.htm)|Energized Cartridge|Cartouche énergisée|libre|
|[consumable-04-nUaNtynMzD1DpHkB.htm](equipment/consumable-04-nUaNtynMzD1DpHkB.htm)|Bottled Omen|Présage en bouteille|libre|
|[consumable-04-omZLCSIxiF9oXyH2.htm](equipment/consumable-04-omZLCSIxiF9oXyH2.htm)|Glimmering Missive|Missive scintillante|libre|
|[consumable-04-pbQXfUhBfGeg4Bc1.htm](equipment/consumable-04-pbQXfUhBfGeg4Bc1.htm)|Chameleon Suit|Tenue de caméléon|libre|
|[consumable-04-pSAnfP76xGS9aVec.htm](equipment/consumable-04-pSAnfP76xGS9aVec.htm)|Potion Patch (Lesser)|Bandage de potion inférieur|libre|
|[consumable-04-pufVrJqj63H7kvAZ.htm](equipment/consumable-04-pufVrJqj63H7kvAZ.htm)|Sure-Step Potion|Potion de marche sûre|libre|
|[consumable-04-pvsJXFfbwGWhoG5N.htm](equipment/consumable-04-pvsJXFfbwGWhoG5N.htm)|Taster's Folly|Folie du goûteur|libre|
|[consumable-04-QJ1fTrX42PoEWpK5.htm](equipment/consumable-04-QJ1fTrX42PoEWpK5.htm)|Leadenleg|Jambes de plomb|libre|
|[consumable-04-qJMcnWmzColtya0Z.htm](equipment/consumable-04-qJMcnWmzColtya0Z.htm)|Sentry Fulu|Fulu sentinelle|libre|
|[consumable-04-QMTW4MjXMIUGp41b.htm](equipment/consumable-04-QMTW4MjXMIUGp41b.htm)|Exsanguinating Ammunition|Munition exsangue|libre|
|[consumable-04-qsPCBxSDbXacCnMB.htm](equipment/consumable-04-qsPCBxSDbXacCnMB.htm)|Mindmurk Oil|Huile de brouillard mental|libre|
|[consumable-04-rBZSDgJeCknH7G2o.htm](equipment/consumable-04-rBZSDgJeCknH7G2o.htm)|Yarrow-Root Bandage|Bandage de racine d'achillée|libre|
|[consumable-04-RK03uy9os4H0dUfS.htm](equipment/consumable-04-RK03uy9os4H0dUfS.htm)|Energizing Tea|Thé énergisant|libre|
|[consumable-04-RtTzHaHMrXiml7ut.htm](equipment/consumable-04-RtTzHaHMrXiml7ut.htm)|Galvasphere|Galvasphère|libre|
|[consumable-04-s9dtRS2SRTqzGdOF.htm](equipment/consumable-04-s9dtRS2SRTqzGdOF.htm)|Stalker Bane Snare|Piège artisanal du fléau rabatteur|libre|
|[consumable-04-SGS7fA2hcElw1EaL.htm](equipment/consumable-04-SGS7fA2hcElw1EaL.htm)|Deteriorating Dust|Poudre détériorante|libre|
|[consumable-04-ShFbUrFrQg7Ung8D.htm](equipment/consumable-04-ShFbUrFrQg7Ung8D.htm)|Capsaicin Tonic|Tonique de capsaïcine|libre|
|[consumable-04-skMIwUIMjWzgkZ5B.htm](equipment/consumable-04-skMIwUIMjWzgkZ5B.htm)|Blessed Ampoule|Ampoule bénie|libre|
|[consumable-04-suTxnYr8arqC9pt7.htm](equipment/consumable-04-suTxnYr8arqC9pt7.htm)|Animal Nip (Lesser)|Herbe aux animaux inférieure|libre|
|[consumable-04-SWqzv0hYCIczICeR.htm](equipment/consumable-04-SWqzv0hYCIczICeR.htm)|Trip Snare|Piège artisanal trébucheur|libre|
|[consumable-04-T4ouD4mVFHA3EHs6.htm](equipment/consumable-04-T4ouD4mVFHA3EHs6.htm)|Bomber's Eye Elixir (Lesser)|Élixir d'oeil de mitrailleur inférieur|libre|
|[consumable-04-THfWJr1qr3jrncYZ.htm](equipment/consumable-04-THfWJr1qr3jrncYZ.htm)|Grave Token|Amulette de tombe|libre|
|[consumable-04-TknN7T2RDy9cUtKU.htm](equipment/consumable-04-TknN7T2RDy9cUtKU.htm)|Marvelous Miniature (Horse)|Figurine merveilleuse Cheval|libre|
|[consumable-04-V7UcnQ0cXh1QSwo6.htm](equipment/consumable-04-V7UcnQ0cXh1QSwo6.htm)|Saboteur's Friend|Ami du saboteur|libre|
|[consumable-04-VJs47kgqu1ziZBRU.htm](equipment/consumable-04-VJs47kgqu1ziZBRU.htm)|Undead Detection Dye|Teinture de détection de mort-vivant|libre|
|[consumable-04-WeiUTSRc17HLNehg.htm](equipment/consumable-04-WeiUTSRc17HLNehg.htm)|Sniper's Bead|Perle du tireur d'élite|libre|
|[consumable-04-xcH5IW7ahE3q8cPq.htm](equipment/consumable-04-xcH5IW7ahE3q8cPq.htm)|Dazzling Rosary|Chapelet éblouissant|libre|
|[consumable-04-XLJc4GDQmeAv1cmG.htm](equipment/consumable-04-XLJc4GDQmeAv1cmG.htm)|Magnetic Suit|Tenue magnétique|libre|
|[consumable-04-XU8u9F3uoesGDjgM.htm](equipment/consumable-04-XU8u9F3uoesGDjgM.htm)|Warning Snare|Piège artisanal avertisseur|officielle|
|[consumable-04-Y8115p3cmQJBqk5d.htm](equipment/consumable-04-Y8115p3cmQJBqk5d.htm)|Darkvision Elixir (Moderate)|Élixir de vision dans le noir modéré|libre|
|[consumable-04-y8x66ougD79IuhI3.htm](equipment/consumable-04-y8x66ougD79IuhI3.htm)|Dragonbone Arrowhead|Pointe de flèche en os de dragon (Talisman)|libre|
|[consumable-04-YcvSw7Zn3oyqlJaw.htm](equipment/consumable-04-YcvSw7Zn3oyqlJaw.htm)|Stone Fist Elixir|Élixir de poing de pierre|libre|
|[consumable-04-Yqz5M71vM1RcvlCx.htm](equipment/consumable-04-Yqz5M71vM1RcvlCx.htm)|Camp Shroud (Minor)|Linceul de camp mineur|libre|
|[consumable-04-YVLwV9IGzNqIzbmV.htm](equipment/consumable-04-YVLwV9IGzNqIzbmV.htm)|Animal Repellent (Lesser)|Répulsif animal inférieur|libre|
|[consumable-04-YYD82q2NfAbuDmgf.htm](equipment/consumable-04-YYD82q2NfAbuDmgf.htm)|Fang Snare|Piège artisanal à crochets venimeux|libre|
|[consumable-04-ZAtwiAPkk1zwCf82.htm](equipment/consumable-04-ZAtwiAPkk1zwCf82.htm)|Fear Gem|Gemme effrayante|libre|
|[consumable-04-zC7LipQPHRYw2RXx.htm](equipment/consumable-04-zC7LipQPHRYw2RXx.htm)|Oak Potion|Potion du chêne|officielle|
|[consumable-04-zJZ5DObRUdTk6HUL.htm](equipment/consumable-04-zJZ5DObRUdTk6HUL.htm)|Tar Rocket Snare|Piège artisanal fusée de goudron|libre|
|[consumable-04-zpaWTsviTO16biJi.htm](equipment/consumable-04-zpaWTsviTO16biJi.htm)|Clinging Ooze Snare|Piège artisanal de vase collante|libre|
|[consumable-04-ZtfkPBCvHTmHCTix.htm](equipment/consumable-04-ZtfkPBCvHTmHCTix.htm)|Explosive Missive|Missive explosive|libre|
|[consumable-04-ZZaEVS1Vw2a8cqWS.htm](equipment/consumable-04-ZZaEVS1Vw2a8cqWS.htm)|Harpoon Bolt|Carreau harpon|libre|
|[consumable-05-0G3OmRIlDaPZyurj.htm](equipment/consumable-05-0G3OmRIlDaPZyurj.htm)|Wet Shock Snare|Piège artisanal de choc humide|libre|
|[consumable-05-0XUENeLPEG73uiP7.htm](equipment/consumable-05-0XUENeLPEG73uiP7.htm)|Tin Cobra|Cobra en étain|libre|
|[consumable-05-0Y0fmvvKSgFvUtA2.htm](equipment/consumable-05-0Y0fmvvKSgFvUtA2.htm)|Crackling Bubble Gum (Lesser)|Chewing-gum crépitant inférieur|libre|
|[consumable-05-1CAPGpQczfq5exrs.htm](equipment/consumable-05-1CAPGpQczfq5exrs.htm)|Diluted Hype|Adrénaline diluée|libre|
|[consumable-05-1v1OK06JxdXn6MP4.htm](equipment/consumable-05-1v1OK06JxdXn6MP4.htm)|Sneaky Key|Clé sournoise|libre|
|[consumable-05-26bM70yn28MjDT9A.htm](equipment/consumable-05-26bM70yn28MjDT9A.htm)|Elven Absinthe|Absinthe elfique|officielle|
|[consumable-05-2EC73UbxVP9f0SR9.htm](equipment/consumable-05-2EC73UbxVP9f0SR9.htm)|Cryomister (Moderate)|Cryobrumisateur modéré|libre|
|[consumable-05-2pB7AsbE0c7HvoZZ.htm](equipment/consumable-05-2pB7AsbE0c7HvoZZ.htm)|Elemental Ammunition (Moderate)|Munition élémentaire modérée|libre|
|[consumable-05-39cQgs70GbP7KZdy.htm](equipment/consumable-05-39cQgs70GbP7KZdy.htm)|Dupe's Gold Nugget|Pépite d'or de Dupe|libre|
|[consumable-05-3uhaf2YL9hmix3pe.htm](equipment/consumable-05-3uhaf2YL9hmix3pe.htm)|Emerald Grasshopper|Sauterelle d'émeraude|libre|
|[consumable-05-5a6pudUj6WG8RITy.htm](equipment/consumable-05-5a6pudUj6WG8RITy.htm)|Egg Cream Fizz|Crème aux oeufs pétillante|libre|
|[consumable-05-5ftFTiKY0BMvhzL9.htm](equipment/consumable-05-5ftFTiKY0BMvhzL9.htm)|Stone Body Mutagen (Lesser)|Mutagène corps-de-pierre inférieur|libre|
|[consumable-05-5lpBiEqrxiyj48JB.htm](equipment/consumable-05-5lpBiEqrxiyj48JB.htm)|Frozen Lava|Lave gelée|libre|
|[consumable-05-5RGNjhDxZ0yMhTds.htm](equipment/consumable-05-5RGNjhDxZ0yMhTds.htm)|Spider Venom|Venin d'araignée chasseresse|libre|
|[consumable-05-605M9h4bMsNCQne6.htm](equipment/consumable-05-605M9h4bMsNCQne6.htm)|Unsullied Blood (Lesser)|Sang non souillé inférieur|libre|
|[consumable-05-7D6ENmL3mRfUOMwf.htm](equipment/consumable-05-7D6ENmL3mRfUOMwf.htm)|Numbing Tonic (Lesser)|Tonique anesthésiant inférieur|libre|
|[consumable-05-7fS4k3K3p6SFLxKe.htm](equipment/consumable-05-7fS4k3K3p6SFLxKe.htm)|Freeze Ammunition|Munition congelante|libre|
|[consumable-05-7VjM9vjBMxhukiTf.htm](equipment/consumable-05-7VjM9vjBMxhukiTf.htm)|Moonlit Spellgun (Lesser)|Lancesort clair de lune inférieur|libre|
|[consumable-05-8pQw1HUzJ4duZbio.htm](equipment/consumable-05-8pQw1HUzJ4duZbio.htm)|Gadget Skates|Patins gadget|libre|
|[consumable-05-8wlCeramJW5cLw6e.htm](equipment/consumable-05-8wlCeramJW5cLw6e.htm)|Golden Branding Iron|Fer à marquer doré|libre|
|[consumable-05-9MpwTXDxblLFgx2J.htm](equipment/consumable-05-9MpwTXDxblLFgx2J.htm)|Spiritual Warhorn (Lesser)|Cor de guerre spirituel inférieur|libre|
|[consumable-05-9TAYUFDOJa0kvk9m.htm](equipment/consumable-05-9TAYUFDOJa0kvk9m.htm)|Mad Mammoth's Juke|Dérobade du mammouth fou|libre|
|[consumable-05-aH5MLDdkZ7aJA7hP.htm](equipment/consumable-05-aH5MLDdkZ7aJA7hP.htm)|Thousand-Pains Fulu (Stone)|Fulu des mille douleurs (Pierre)|libre|
|[consumable-05-aKrslrjq22S9uRgF.htm](equipment/consumable-05-aKrslrjq22S9uRgF.htm)|Discord Fulu|Fulu de discorde|libre|
|[consumable-05-AqLxu3ir4UGzdOaz.htm](equipment/consumable-05-AqLxu3ir4UGzdOaz.htm)|Cheetah's Elixir (Moderate)|Élixir du guépard modéré|libre|
|[consumable-05-aVW5OAwSlz5rsjtB.htm](equipment/consumable-05-aVW5OAwSlz5rsjtB.htm)|Pummeling Snare|Piège artisanal percutant|libre|
|[consumable-05-awPkC6AWcHS3T4oz.htm](equipment/consumable-05-awPkC6AWcHS3T4oz.htm)|Cooperative Waffles (Greater)|Gaufres coopératives supérieures|libre|
|[consumable-05-BUZ8yiAXgXH8otmM.htm](equipment/consumable-05-BUZ8yiAXgXH8otmM.htm)|Feather Token (Chest) (Ammunition)|Figurine merveilleuse coffre (Munition)|libre|
|[consumable-05-Cs96jFDDcL9sX4Hv.htm](equipment/consumable-05-Cs96jFDDcL9sX4Hv.htm)|Sampling Ammunition|Munition d'échantillonnage|libre|
|[consumable-05-D2h1E7fXsQfyjFcv.htm](equipment/consumable-05-D2h1E7fXsQfyjFcv.htm)|Rhino Hide Brooch|Broche en peau de rhinocéros|libre|
|[consumable-05-DqgkcKJL6ASqwuio.htm](equipment/consumable-05-DqgkcKJL6ASqwuio.htm)|Euphoric Loop|Loupe euphorisante|libre|
|[consumable-05-E7BcwZy8nTpTLYf1.htm](equipment/consumable-05-E7BcwZy8nTpTLYf1.htm)|Blindpepper Bomb|Bombe de poivre aveuglant|libre|
|[consumable-05-fv10pyjxJtrxMaQ4.htm](equipment/consumable-05-fv10pyjxJtrxMaQ4.htm)|Goblin-Eye Orb|Orbe d'oeil-gobelin|libre|
|[consumable-05-fz2OlQ9IYIcVASiv.htm](equipment/consumable-05-fz2OlQ9IYIcVASiv.htm)|Alkenstar Ice Wine|Vin de glace d'Alkenastre|libre|
|[consumable-05-G8h3vA0bSewbdrDS.htm](equipment/consumable-05-G8h3vA0bSewbdrDS.htm)|Fulu of the Stoic Ox|Fulu du boeuf stoïque|libre|
|[consumable-05-gMjoALuwSUdTmqqZ.htm](equipment/consumable-05-gMjoALuwSUdTmqqZ.htm)|Malleable Clay|Argile malléable|libre|
|[consumable-05-GNy0YEusxjcGMCNn.htm](equipment/consumable-05-GNy0YEusxjcGMCNn.htm)|Green Wyrmling Breath Potion|Potion de souffle du dragonnet vert|libre|
|[consumable-05-GyqCW00omWWQ2C4e.htm](equipment/consumable-05-GyqCW00omWWQ2C4e.htm)|Sky Serpent Bolt|Carreau serpent céleste|libre|
|[consumable-05-hH63OWEVoMEr4ypr.htm](equipment/consumable-05-hH63OWEVoMEr4ypr.htm)|Nightpitch|Bitume nocturne|libre|
|[consumable-05-hHsU2sRW47Rge8fU.htm](equipment/consumable-05-hHsU2sRW47Rge8fU.htm)|Tteokguk of Time Advancement|Tteokguk du déroulement du temps|libre|
|[consumable-05-HU9eYAAfZMYnFMd9.htm](equipment/consumable-05-HU9eYAAfZMYnFMd9.htm)|Beckoning Cat Amulet|Amulette d'attirance du chat|libre|
|[consumable-05-hWXRen2x3kBxeYlh.htm](equipment/consumable-05-hWXRen2x3kBxeYlh.htm)|Statue Skin Salve|Pommade peau de statue|libre|
|[consumable-05-igqlI1SbRPrXMtLT.htm](equipment/consumable-05-igqlI1SbRPrXMtLT.htm)|Poracha Fulu|Fulu poracha|libre|
|[consumable-05-ik15k2r6znOdguXo.htm](equipment/consumable-05-ik15k2r6znOdguXo.htm)|Thunderbird Tuft (Moderate)|Touffe d'oiseau-tonnerre modérée|libre|
|[consumable-05-IOurUQgkm7oKnyrt.htm](equipment/consumable-05-IOurUQgkm7oKnyrt.htm)|Sprite Apple (Chartreuse)|Pomme sprite (chartreuse)|libre|
|[consumable-05-iUOcC9vFboSFI8fU.htm](equipment/consumable-05-iUOcC9vFboSFI8fU.htm)|False Death|Mort simulée|libre|
|[consumable-05-JAOWw2GhupaYoYg9.htm](equipment/consumable-05-JAOWw2GhupaYoYg9.htm)|Sixfingers Elixir (Lesser)|Élixir Six-doigts inférieur|libre|
|[consumable-05-Jc0OJe0fRDdHXXWu.htm](equipment/consumable-05-Jc0OJe0fRDdHXXWu.htm)|Sparking Spellgun (Lesser)|Lancesort étincelant inférieur|libre|
|[consumable-05-JgZIWU1KaVL2pnAr.htm](equipment/consumable-05-JgZIWU1KaVL2pnAr.htm)|Fungal Walk Musk|Onguent de Musc fongique|libre|
|[consumable-05-JnGeiRdprh1j0qnT.htm](equipment/consumable-05-JnGeiRdprh1j0qnT.htm)|Fearcracker|Pétard de terreur|libre|
|[consumable-05-jrLgEJxPvUKtSMMO.htm](equipment/consumable-05-jrLgEJxPvUKtSMMO.htm)|Recording Rod (Reusable)|Tige d'enregistrement (Réutilisable)|libre|
|[consumable-05-kaIKYTrvw4qa1khc.htm](equipment/consumable-05-kaIKYTrvw4qa1khc.htm)|Flame Drake Snare|Piège artisanal du drake de feu|libre|
|[consumable-05-kR53wN4yZ30PxysG.htm](equipment/consumable-05-kR53wN4yZ30PxysG.htm)|Weapon Shot (Lesser)|Tir d'arme inférieur|libre|
|[consumable-05-KVrCsckKUW68FWcV.htm](equipment/consumable-05-KVrCsckKUW68FWcV.htm)|Grease Snare|Piège artisanal graisseux|libre|
|[consumable-05-l2M9P6kI3z3xPBOa.htm](equipment/consumable-05-l2M9P6kI3z3xPBOa.htm)|Serpent Oil (Moderate)|Huile de serpent modérée|libre|
|[consumable-05-L5AaEekLZ7Xt80FZ.htm](equipment/consumable-05-L5AaEekLZ7Xt80FZ.htm)|Gecko Pads|Coussinets de gecko|libre|
|[consumable-05-lkbmxnhvWKhO0cFt.htm](equipment/consumable-05-lkbmxnhvWKhO0cFt.htm)|Familiar Morsel|Bouchée de familier|libre|
|[consumable-05-LLkl4mTbV9q4iDHJ.htm](equipment/consumable-05-LLkl4mTbV9q4iDHJ.htm)|Rhino Shot|Tir de rhino|libre|
|[consumable-05-lPcnDlBGz5QwCMYw.htm](equipment/consumable-05-lPcnDlBGz5QwCMYw.htm)|Eagle Eye Elixir (Moderate)|Élixir d'oeil de faucon modéré|libre|
|[consumable-05-lvGSfPbpAd9lHeX5.htm](equipment/consumable-05-lvGSfPbpAd9lHeX5.htm)|Wounding Oil|Huile sanglante|libre|
|[consumable-05-lzjx7KiuoaQuh025.htm](equipment/consumable-05-lzjx7KiuoaQuh025.htm)|Mustard Powder|Poudre moutardée|libre|
|[consumable-05-mmsuA7qPxFLLghtx.htm](equipment/consumable-05-mmsuA7qPxFLLghtx.htm)|Spellstrike Ammunition (Type II)|Munition de frappe magique (Type II)|libre|
|[consumable-05-MobYbxEL4KgxVi63.htm](equipment/consumable-05-MobYbxEL4KgxVi63.htm)|Salve of Slipperiness|Onguent d'insaisissabilité|officielle|
|[consumable-05-mxnlzHb86s56F5wc.htm](equipment/consumable-05-mxnlzHb86s56F5wc.htm)|Chimera Thread|Fil chimérique|libre|
|[consumable-05-mzk9ekUydT8zpy4A.htm](equipment/consumable-05-mzk9ekUydT8zpy4A.htm)|Bola Shot|Tir bolas|libre|
|[consumable-05-NJOOeJc7bpE7gkUn.htm](equipment/consumable-05-NJOOeJc7bpE7gkUn.htm)|Fulu of the Drunken Monkey|Fulu du singe ivre|libre|
|[consumable-05-NKOAAPYL5RxReuKi.htm](equipment/consumable-05-NKOAAPYL5RxReuKi.htm)|Alchemist's Damper|Amortisseur de l'alchimiste|libre|
|[consumable-05-ntrmM8yBysmeqt71.htm](equipment/consumable-05-ntrmM8yBysmeqt71.htm)|Electromuscular Stimulator|Stimulateur électromusculaire|libre|
|[consumable-05-o9k5L682AlZfhpRu.htm](equipment/consumable-05-o9k5L682AlZfhpRu.htm)|Oil of Revelation|Huile de Révélation|libre|
|[consumable-05-OymrUAv6RGFaI1nm.htm](equipment/consumable-05-OymrUAv6RGFaI1nm.htm)|Soothing Powder (Lesser)|Poudre apaisante inférieure|libre|
|[consumable-05-PfVTTuTsErku7vss.htm](equipment/consumable-05-PfVTTuTsErku7vss.htm)|Privacy Ward Fulu (Room)|Fulu de protection de la confidentialité (Salle)|libre|
|[consumable-05-PTEQ6NCrFkYQCURJ.htm](equipment/consumable-05-PTEQ6NCrFkYQCURJ.htm)|Life Salt|Sel de vie|libre|
|[consumable-05-q8aRbsKX93R5AXNb.htm](equipment/consumable-05-q8aRbsKX93R5AXNb.htm)|Healer's Gel (Lesser)|Gel du guérisseur inférieur|libre|
|[consumable-05-qFEoXkORtrHfoMwx.htm](equipment/consumable-05-qFEoXkORtrHfoMwx.htm)|Reducer Round|Balle réductrice|libre|
|[consumable-05-QNub2kTE7LpdMPII.htm](equipment/consumable-05-QNub2kTE7LpdMPII.htm)|Potion of Disguise (Lesser)|Potion de Déguisement inférieure|libre|
|[consumable-05-qrgUIr4PAvLvHn5C.htm](equipment/consumable-05-qrgUIr4PAvLvHn5C.htm)|Invigorating Soap|Savon revigorant|libre|
|[consumable-05-qSmhcV5NLzlBjfAz.htm](equipment/consumable-05-qSmhcV5NLzlBjfAz.htm)|Tracking Fulu|Fulu pisteur|libre|
|[consumable-05-QSVIv5obLhKkmy67.htm](equipment/consumable-05-QSVIv5obLhKkmy67.htm)|Universal Solvent (Moderate)|Solvant universel modéré|libre|
|[consumable-05-qyjaueXduvCUeDbp.htm](equipment/consumable-05-qyjaueXduvCUeDbp.htm)|Resurrection Tea|Thé de résurrection|libre|
|[consumable-05-rab0x5F9KpRxxlnH.htm](equipment/consumable-05-rab0x5F9KpRxxlnH.htm)|Eroding Bullet|Balle érodée|libre|
|[consumable-05-rjUJEY424jyG9dGn.htm](equipment/consumable-05-rjUJEY424jyG9dGn.htm)|Cytillesh|Cytillesh|officielle|
|[consumable-05-RL2VI9IGuOtXsEX6.htm](equipment/consumable-05-RL2VI9IGuOtXsEX6.htm)|Bane Ammunition (Moderate)|Munition de proie favorite modérée|libre|
|[consumable-05-RLkHxGBbVRAT6AOL.htm](equipment/consumable-05-RLkHxGBbVRAT6AOL.htm)|Stage Fright Missive|Missive de trac|libre|
|[consumable-05-RM7WoJYfd2edlNZ8.htm](equipment/consumable-05-RM7WoJYfd2edlNZ8.htm)|Soothing Tonic (Moderate)|Tonique apaisant modéré|libre|
|[consumable-05-rzEQvcWfhR3T4FNd.htm](equipment/consumable-05-rzEQvcWfhR3T4FNd.htm)|Potion of Leaping|Potion de saut|libre|
|[consumable-05-T1FiDItPqd2xkYpt.htm](equipment/consumable-05-T1FiDItPqd2xkYpt.htm)|Golden Chrysalis|Chysalide dorée|libre|
|[consumable-05-Tccy21bO1sDb6hQM.htm](equipment/consumable-05-Tccy21bO1sDb6hQM.htm)|Clown Monarch|Monarque clown|libre|
|[consumable-05-Tj4uaNw2lgevxGl7.htm](equipment/consumable-05-Tj4uaNw2lgevxGl7.htm)|Shark Tooth Charm|Charme en dent de requin|libre|
|[consumable-05-TZUskLT7yvP7N2co.htm](equipment/consumable-05-TZUskLT7yvP7N2co.htm)|Elixir of Life (Lesser)|Élixir de vie inférieur|libre|
|[consumable-05-U9LhV1IBLbRug7uz.htm](equipment/consumable-05-U9LhV1IBLbRug7uz.htm)|Heartening Missive (Butterfly)|Missive réconfortante (Papillon)|libre|
|[consumable-05-uoWWFCt1B0lKtjwZ.htm](equipment/consumable-05-uoWWFCt1B0lKtjwZ.htm)|Blister Ammunition (Lesser)|Munition à balle inférieure|libre|
|[consumable-05-uPP678KODkgDy8UO.htm](equipment/consumable-05-uPP678KODkgDy8UO.htm)|Ginger Chew|Gingembre à mâcher|libre|
|[consumable-05-UuNVe6SDNztreNEN.htm](equipment/consumable-05-UuNVe6SDNztreNEN.htm)|Apotropaic Fulu|Fulu révélateur d'alignement|libre|
|[consumable-05-UUriyvai2k6COWpS.htm](equipment/consumable-05-UUriyvai2k6COWpS.htm)|Necrobinding Serum|Sérum de nécronoué|libre|
|[consumable-05-uwVTuejjSLl82jiA.htm](equipment/consumable-05-uwVTuejjSLl82jiA.htm)|Ichthyosis Mutagen|Mutagène de peau squameuse|libre|
|[consumable-05-vJZ49cgi8szuQXAD.htm](equipment/consumable-05-vJZ49cgi8szuQXAD.htm)|Magic Wand (2nd-Rank Spell)|Baguette magique (sort de rang 2)|libre|
|[consumable-05-VmMSP1EbMVnj2pZ2.htm](equipment/consumable-05-VmMSP1EbMVnj2pZ2.htm)|Firestarter Pellets|Boulettes d'ignition|libre|
|[consumable-05-VnUXcWG4xWGcb7sc.htm](equipment/consumable-05-VnUXcWG4xWGcb7sc.htm)|Pucker Pickle|Cornichon pucker|libre|
|[consumable-05-vNxKM5J5TqrbJKYk.htm](equipment/consumable-05-vNxKM5J5TqrbJKYk.htm)|Eye of Enlightenment|Oeil d'éclaircissement|libre|
|[consumable-05-Wn0H58sAkUtIaPPd.htm](equipment/consumable-05-Wn0H58sAkUtIaPPd.htm)|Gravemist Taper|Cierge de brume sépulcrale|libre|
|[consumable-05-xAEbIQTNonVjCg38.htm](equipment/consumable-05-xAEbIQTNonVjCg38.htm)|Oily Button|Bouton huileux|libre|
|[consumable-05-XaHXxSpSCgLGYhbs.htm](equipment/consumable-05-XaHXxSpSCgLGYhbs.htm)|Imp Shot|Tir de diablotin|libre|
|[consumable-05-xlwcMtJTKpS6EkSw.htm](equipment/consumable-05-xlwcMtJTKpS6EkSw.htm)|Wand of Caustic Effluence|Baguette d'effluence caustique|libre|
|[consumable-05-Xnmx6SJ6OkJOfYxF.htm](equipment/consumable-05-Xnmx6SJ6OkJOfYxF.htm)|Freezing Ammunition|Munition givrante|libre|
|[consumable-05-YeQjs2DarMVIPVjq.htm](equipment/consumable-05-YeQjs2DarMVIPVjq.htm)|Hippogriff in a Jar|Hippogriffe en bocal|libre|
|[consumable-05-Yp1eRc7NmzSa5KYZ.htm](equipment/consumable-05-Yp1eRc7NmzSa5KYZ.htm)|Depth Charge I|Charge de profondeur I|libre|
|[consumable-05-yqbW1T3Eu9KHRs1U.htm](equipment/consumable-05-yqbW1T3Eu9KHRs1U.htm)|Copper Penny|Piécette de cuivre|libre|
|[consumable-05-zcJgT5RS8p2MEbOB.htm](equipment/consumable-05-zcJgT5RS8p2MEbOB.htm)|Sea Touch Elixir (Lesser)|Élixir de caresse marine inférieur|libre|
|[consumable-05-ZiD7kDxU4KEkO6XH.htm](equipment/consumable-05-ZiD7kDxU4KEkO6XH.htm)|Shimmering Dust|Poussière chatoyante|libre|
|[consumable-05-ZmefGBXGJF3CFDbn.htm](equipment/consumable-05-ZmefGBXGJF3CFDbn.htm)|Scroll of 3rd-rank Spell|Parchemin de sort de rang 3|libre|
|[consumable-05-zrZ1FaaqW6VIajj7.htm](equipment/consumable-05-zrZ1FaaqW6VIajj7.htm)|Tiger Menuki|Menuki du tigre|libre|
|[consumable-06-1bLvKfodvgrIG4hp.htm](equipment/consumable-06-1bLvKfodvgrIG4hp.htm)|Light Writer Plates|Plaques d'enregistreur de lumière|libre|
|[consumable-06-3bZcqRUWQcPe2y8M.htm](equipment/consumable-06-3bZcqRUWQcPe2y8M.htm)|Black Powder (Keg)|Poudre noire (Tonnelet)|libre|
|[consumable-06-4owFeOy4zxy8rv7W.htm](equipment/consumable-06-4owFeOy4zxy8rv7W.htm)|Feather Token (Tree)|Figurine merveilleuse arbre|libre|
|[consumable-06-6iqRv3TGKt6DyIBs.htm](equipment/consumable-06-6iqRv3TGKt6DyIBs.htm)|Heartening Missive (Bull)|Missive réconfortante (Taureau)|libre|
|[consumable-06-6nmOC0blUxtrlLaY.htm](equipment/consumable-06-6nmOC0blUxtrlLaY.htm)|Winterstep Elixir (Lesser)|Élixir de démarche hivernale inférieur|libre|
|[consumable-06-7b6jSbZ7Xu88wyi8.htm](equipment/consumable-06-7b6jSbZ7Xu88wyi8.htm)|Eidetic Potion|Potion éidétique|libre|
|[consumable-06-7iZCCqXDGiTv0ar3.htm](equipment/consumable-06-7iZCCqXDGiTv0ar3.htm)|Antipode Oil|Huile antipodique|libre|
|[consumable-06-8iGmSwTTUdj6gqN5.htm](equipment/consumable-06-8iGmSwTTUdj6gqN5.htm)|Dust of Appearance|Poudre d'apparition|officielle|
|[consumable-06-AgBIZtwciSsCZeNN.htm](equipment/consumable-06-AgBIZtwciSsCZeNN.htm)|Potion of Fire Retaliation (Moderate)|Potion de riposte enflammée modérée|libre|
|[consumable-06-aiL53pkt0wFrsAQb.htm](equipment/consumable-06-aiL53pkt0wFrsAQb.htm)|Explosive Mine (Moderate)|Mine explosive modérée|libre|
|[consumable-06-AJ1dC7EtTIfBey0M.htm](equipment/consumable-06-AJ1dC7EtTIfBey0M.htm)|Antidote (Moderate)|Antidote modéré|libre|
|[consumable-06-Ax8XcBd0rRd1Z7hN.htm](equipment/consumable-06-Ax8XcBd0rRd1Z7hN.htm)|Mistform Elixir (Moderate)|Élixir de brumeforme modéré|libre|
|[consumable-06-B3QOvE43Qn1H8t8n.htm](equipment/consumable-06-B3QOvE43Qn1H8t8n.htm)|Inventor's Fulu|Fulu de l'inventeur|libre|
|[consumable-06-bDnk4TSzvD5BQmE5.htm](equipment/consumable-06-bDnk4TSzvD5BQmE5.htm)|Potion of Fire Resistance (Lesser)|Potion de résistance au feu inférieure|libre|
|[consumable-06-biRbqKo2C97XLfQ0.htm](equipment/consumable-06-biRbqKo2C97XLfQ0.htm)|Antiplague (Moderate)|Antimaladie modéré|libre|
|[consumable-06-cAyxcRSSnfscGyMa.htm](equipment/consumable-06-cAyxcRSSnfscGyMa.htm)|Potion of Electricity Resistance (Lesser)|Potion de résistance à l'électricité inférieure|libre|
|[consumable-06-cgWT0BUa6b6V7zDg.htm](equipment/consumable-06-cgWT0BUa6b6V7zDg.htm)|Feather Token (Balloon)|Figurine merveilleuse dirigeable|libre|
|[consumable-06-cWYa0i1BqhbEruD6.htm](equipment/consumable-06-cWYa0i1BqhbEruD6.htm)|Addiction Suppressant (Moderate)|Suppresseur de dépendance modéré|libre|
|[consumable-06-Cz0XxRVMma7cllCd.htm](equipment/consumable-06-Cz0XxRVMma7cllCd.htm)|Peace in Dreams Tea|Paix dans le thé des rêves|libre|
|[consumable-06-Eb4dEuV22QVlMumS.htm](equipment/consumable-06-Eb4dEuV22QVlMumS.htm)|Potion of Swimming|Potion de nage|libre|
|[consumable-06-EeqvTTUpqHobTs6u.htm](equipment/consumable-06-EeqvTTUpqHobTs6u.htm)|Sprite Apple (Teal)|Pomme sprite (sarcelle)|libre|
|[consumable-06-eSHX07MqMT4SM1zY.htm](equipment/consumable-06-eSHX07MqMT4SM1zY.htm)|Potion of Sonic Resistance (Lesser)|Potion de résistance au son inférieure|libre|
|[consumable-06-G7haQ5gDt30ftJLC.htm](equipment/consumable-06-G7haQ5gDt30ftJLC.htm)|Healing Potion (Moderate)|Potion de guérison modérée|libre|
|[consumable-06-GiiBkaa5JaJ5msPS.htm](equipment/consumable-06-GiiBkaa5JaJ5msPS.htm)|Peachwood Talisman|Talisman en pêcher céleste|libre|
|[consumable-06-HAEz4sSa6OH6C7Cs.htm](equipment/consumable-06-HAEz4sSa6OH6C7Cs.htm)|Truth Potion|Potion de vérité|libre|
|[consumable-06-hguIfHmI3LjhvlWO.htm](equipment/consumable-06-hguIfHmI3LjhvlWO.htm)|Life Shot (Lesser)|Tir de vie inférieur|libre|
|[consumable-06-HilBL7oeSSXqDor7.htm](equipment/consumable-06-HilBL7oeSSXqDor7.htm)|Vaccine (Moderate)|Vaccin modéré|libre|
|[consumable-06-iCRoLguNunOjIM7L.htm](equipment/consumable-06-iCRoLguNunOjIM7L.htm)|Forgetful Ink|Encre d'amnésie|libre|
|[consumable-06-j3Pe1dyszbOT9c6Y.htm](equipment/consumable-06-j3Pe1dyszbOT9c6Y.htm)|Dispersing Bullet|Balle dispersante|libre|
|[consumable-06-Jgv2PAJic7LPoGQx.htm](equipment/consumable-06-Jgv2PAJic7LPoGQx.htm)|Soothing Toddy|Grog apaisant|libre|
|[consumable-06-LKTbRpaw8bH3WHv1.htm](equipment/consumable-06-LKTbRpaw8bH3WHv1.htm)|Implosion Dust (Lesser)|Poussière d'implosion inférieure|libre|
|[consumable-06-LPSSrlS1Op6l9Kn5.htm](equipment/consumable-06-LPSSrlS1Op6l9Kn5.htm)|Mirror-Ball Snare|Piège artisanal boule-miroir|libre|
|[consumable-06-M6pES1Nck1S6SWX9.htm](equipment/consumable-06-M6pES1Nck1S6SWX9.htm)|Potion of Cold Retaliation (Moderate)|Potion de riposte glaciale modérée|libre|
|[consumable-06-M7Ful1B1IWeibSm7.htm](equipment/consumable-06-M7Ful1B1IWeibSm7.htm)|Golden Silencer (Standard)|Silencieux doré|libre|
|[consumable-06-MbPboT76BBKVGepB.htm](equipment/consumable-06-MbPboT76BBKVGepB.htm)|Nauseating Snare|Piège artisanal nauséabond|libre|
|[consumable-06-mBwtPr9SGyuq0vPW.htm](equipment/consumable-06-mBwtPr9SGyuq0vPW.htm)|Binding Coil|Lien d'attache|libre|
|[consumable-06-MMInXBlOscwZVXsm.htm](equipment/consumable-06-MMInXBlOscwZVXsm.htm)|Heartening Missive (Rabbit)|Missive réconfortante (Lapin)|libre|
|[consumable-06-mRs24OwmGPjoIDvO.htm](equipment/consumable-06-mRs24OwmGPjoIDvO.htm)|Potion of Cold Resistance (Lesser)|Potion de résistance au froid inférieure|libre|
|[consumable-06-NhCFZ043brhFTsni.htm](equipment/consumable-06-NhCFZ043brhFTsni.htm)|Singing Muse|Muse chantante|libre|
|[consumable-06-oDfucsKeWpJmmWN1.htm](equipment/consumable-06-oDfucsKeWpJmmWN1.htm)|Potion of Electricity Retaliation (Moderate)|Potion de riposte électrique modérée|libre|
|[consumable-06-OWPOTwMsrYma9d0v.htm](equipment/consumable-06-OWPOTwMsrYma9d0v.htm)|Potion of Acid Retaliation (Moderate)|Potion de riposte acide modérée|libre|
|[consumable-06-PaHw4xIxBujlCaqN.htm](equipment/consumable-06-PaHw4xIxBujlCaqN.htm)|Wind Ocarina|Ocarina du vent|libre|
|[consumable-06-PEjGV24mOK5gPPPS.htm](equipment/consumable-06-PEjGV24mOK5gPPPS.htm)|Spun Cloud (Blue)|Nuage tourbillonnant bleu|libre|
|[consumable-06-Pjxotvrj7uXe92Qc.htm](equipment/consumable-06-Pjxotvrj7uXe92Qc.htm)|Silver Crescent (Lesser)|Croissant argenté inférieur|libre|
|[consumable-06-pzUHY3C7JfmUgjuO.htm](equipment/consumable-06-pzUHY3C7JfmUgjuO.htm)|Tentacle Potion (Lesser)|Potion tentaculaire inférieure|libre|
|[consumable-06-qasFfdiZKIczoZ9p.htm](equipment/consumable-06-qasFfdiZKIczoZ9p.htm)|Bloodhound Mask (Moderate)|Masque du limier modéré|libre|
|[consumable-06-QGXNqpP5KvSldoZz.htm](equipment/consumable-06-QGXNqpP5KvSldoZz.htm)|Giant Scorpion Venom|Venin de scorpion géant|libre|
|[consumable-06-qnR646Oph1q88RNo.htm](equipment/consumable-06-qnR646Oph1q88RNo.htm)|Snarling Badger (Lesser)|Blaireau grognant inférieur|libre|
|[consumable-06-QWKDGY62Buq9dryf.htm](equipment/consumable-06-QWKDGY62Buq9dryf.htm)|Octopus Potion (Lesser)|Potion pieuvre inférieure|libre|
|[consumable-06-RBjyD36IrrFOwFXR.htm](equipment/consumable-06-RBjyD36IrrFOwFXR.htm)|Barricade Stone (Sphere)|Pierre de barricade (Sphère)|libre|
|[consumable-06-ROdjFw7wby982qf5.htm](equipment/consumable-06-ROdjFw7wby982qf5.htm)|Terrifying Ammunition|Munition terrifiante|libre|
|[consumable-06-Ru4xaA4kjdZ4IFS5.htm](equipment/consumable-06-Ru4xaA4kjdZ4IFS5.htm)|Oil of Weightlessness (Greater)|Huile d'apesanteur supérieure|libre|
|[consumable-06-sAMzkU7kzUZfqTPV.htm](equipment/consumable-06-sAMzkU7kzUZfqTPV.htm)|Extended Deteriorating Dust|Poudre détériorante étendue|libre|
|[consumable-06-SfqfTak3o0cuSqhL.htm](equipment/consumable-06-SfqfTak3o0cuSqhL.htm)|Ooze Ammunition (Moderate)|Munition de vase modérée|libre|
|[consumable-06-SlQ6KsMUnmG6FhtW.htm](equipment/consumable-06-SlQ6KsMUnmG6FhtW.htm)|Scholar's Drop|Goutte de l'érudit|libre|
|[consumable-06-sM0Cbkn8cQN4pUsp.htm](equipment/consumable-06-sM0Cbkn8cQN4pUsp.htm)|Shortbread Spy|Sablé-espion|libre|
|[consumable-06-sPceDzKeOA8PpK2v.htm](equipment/consumable-06-sPceDzKeOA8PpK2v.htm)|Insight Coffee (Lesser)|Café de perspicacité inférieur|libre|
|[consumable-06-SRaVakPZeb81ZLfL.htm](equipment/consumable-06-SRaVakPZeb81ZLfL.htm)|Heartening Missive (Turtle)|Missive réconfortante (Tortue)|libre|
|[consumable-06-StsE5POM7OSE36Ia.htm](equipment/consumable-06-StsE5POM7OSE36Ia.htm)|Iron Cube|Cube de fer|libre|
|[consumable-06-tO6nucXnHAhCpD6n.htm](equipment/consumable-06-tO6nucXnHAhCpD6n.htm)|Conduit Shot (Lesser)|Tir conducteur inférieur|libre|
|[consumable-06-TZJKwkvJD4NGXCG8.htm](equipment/consumable-06-TZJKwkvJD4NGXCG8.htm)|Oil of Swiftness|Huile de rapidité|libre|
|[consumable-06-u1gZaIZHnEfZDc1o.htm](equipment/consumable-06-u1gZaIZHnEfZDc1o.htm)|Potion of Acid Resistance (Lesser)|Potion de résistance à l'acide inférieure|libre|
|[consumable-06-U6wnudbnHP2RMKpN.htm](equipment/consumable-06-U6wnudbnHP2RMKpN.htm)|Bloodhammer Reserve Select|Réserve sélectionnée Sangmartel|libre|
|[consumable-06-Upso2k53BUfmeays.htm](equipment/consumable-06-Upso2k53BUfmeays.htm)|Liquid Gold|Or liquide|libre|
|[consumable-06-UtT7TVT7M6uJTlJH.htm](equipment/consumable-06-UtT7TVT7M6uJTlJH.htm)|Restful Sleep Fulu|Fulu de sommeil paisible|libre|
|[consumable-06-Uzf9hqwec8fY2j4G.htm](equipment/consumable-06-Uzf9hqwec8fY2j4G.htm)|Ghostbane Fulu|Fulu chasse-spectre|libre|
|[consumable-06-V2TUEoiDwJ125qzN.htm](equipment/consumable-06-V2TUEoiDwJ125qzN.htm)|Potion of Resistance (Lesser)|Potion de résistance inférieure|libre|
|[consumable-06-WAQABCTFGuHvC0yR.htm](equipment/consumable-06-WAQABCTFGuHvC0yR.htm)|Flare Beacon (Moderate)|Balise éclairante modérée|libre|
|[consumable-06-wyD9xIa4TAxoHnuQ.htm](equipment/consumable-06-wyD9xIa4TAxoHnuQ.htm)|Gearbinder Oil (Lesser)|Huile grippe engrenages inférieure|libre|
|[consumable-06-xGmX6Vuuhivyal8v.htm](equipment/consumable-06-xGmX6Vuuhivyal8v.htm)|Skeptic's Elixir (Moderate)|Élixir de l'incrédule modéré|libre|
|[consumable-06-xX9mq2iACVOas26l.htm](equipment/consumable-06-xX9mq2iACVOas26l.htm)|Fate Shot|Tir du destin|libre|
|[consumable-06-YHev1WJ2tOiTBg9o.htm](equipment/consumable-06-YHev1WJ2tOiTBg9o.htm)|Oil of Unlife (Moderate)|Huile de non-vie modérée|libre|
|[consumable-06-ymSbtkmW9xg9kWyN.htm](equipment/consumable-06-ymSbtkmW9xg9kWyN.htm)|Piercing Whistle Snare|Piège artisanal du sifflement perçant|libre|
|[consumable-06-yO0CjTBpv5O9HhTJ.htm](equipment/consumable-06-yO0CjTBpv5O9HhTJ.htm)|Peacemaker|Pacificateur|libre|
|[consumable-06-YoYqtOXWo5reE2ql.htm](equipment/consumable-06-YoYqtOXWo5reE2ql.htm)|Sense-Dulling Hood (Lesser)|Capuche émousse-sens inférieure|libre|
|[consumable-06-yYkxdzogiH7FPWVP.htm](equipment/consumable-06-yYkxdzogiH7FPWVP.htm)|Fulu of Concealment|Fulu de dissimulation|libre|
|[consumable-06-ZEKmCg8K2hUHbmnT.htm](equipment/consumable-06-ZEKmCg8K2hUHbmnT.htm)|Salve of Antiparalysis|Baume antiparalysie|libre|
|[consumable-06-ZROIWl7TLxKbPDLr.htm](equipment/consumable-06-ZROIWl7TLxKbPDLr.htm)|Demon Dust|Poussière de démon|libre|
|[consumable-07-0Lz5mlbB6QTZIYLf.htm](equipment/consumable-07-0Lz5mlbB6QTZIYLf.htm)|Spun Cloud (Black)|Nuage tourbillonnant noir|libre|
|[consumable-07-0XSl2DU7JvKXOqTo.htm](equipment/consumable-07-0XSl2DU7JvKXOqTo.htm)|Knockout Dram|Goutte d'assommoir|libre|
|[consumable-07-1VZpMEBamUpWADNZ.htm](equipment/consumable-07-1VZpMEBamUpWADNZ.htm)|Depth Charge II|Charge de profondeur II|libre|
|[consumable-07-1XoBUEUearOzrwEs.htm](equipment/consumable-07-1XoBUEUearOzrwEs.htm)|Camp Shroud (Lesser)|Linceul de camp inférieur|libre|
|[consumable-07-2Rnw5hTlMd0YSozb.htm](equipment/consumable-07-2Rnw5hTlMd0YSozb.htm)|Meteor Shot|Balle météore|libre|
|[consumable-07-2UQANe2ca1c8MO6Z.htm](equipment/consumable-07-2UQANe2ca1c8MO6Z.htm)|Dragonfly Fulu|Fulu libellule|libre|
|[consumable-07-3cMFVAOHXHOHUvSW.htm](equipment/consumable-07-3cMFVAOHXHOHUvSW.htm)|Blooming Lotus Seed Pod|Cosse de lotus fleuri|libre|
|[consumable-07-3fSc2e5xRjJ7w4g3.htm](equipment/consumable-07-3fSc2e5xRjJ7w4g3.htm)|Energy Breath Potion (Fire, Lesser)|Potion de souffle d'énergie de feu inférieure|libre|
|[consumable-07-4tnPWyApPZP1P1yO.htm](equipment/consumable-07-4tnPWyApPZP1P1yO.htm)|Swift Block Cabochon|Cabochon de blocage rapide|libre|
|[consumable-07-4TzKhSM0gD9czMdC.htm](equipment/consumable-07-4TzKhSM0gD9czMdC.htm)|Camouflage Dye (Greater)|Teinture de camouflage supérieure|libre|
|[consumable-07-53mPIEbUw5RzQ6pc.htm](equipment/consumable-07-53mPIEbUw5RzQ6pc.htm)|Escape Fulu|Fulu d'évasion|libre|
|[consumable-07-5OiGlKiNFKlcP4k1.htm](equipment/consumable-07-5OiGlKiNFKlcP4k1.htm)|Revealing Mist (Greater)|Brume révélatrice supérieure|libre|
|[consumable-07-6eQvHNHf1IC2X5Rx.htm](equipment/consumable-07-6eQvHNHf1IC2X5Rx.htm)|Leaper's Elixir (Greater)|Élixir du sauteur supérieur|officielle|
|[consumable-07-76T49dJYfxIrPvQe.htm](equipment/consumable-07-76T49dJYfxIrPvQe.htm)|Malyass Root Paste|Pâte de racine de malyasse|officielle|
|[consumable-07-7IrQPyMm76nLVoXx.htm](equipment/consumable-07-7IrQPyMm76nLVoXx.htm)|Sovereign Glue|Colle souveraine|libre|
|[consumable-07-903CuhvVUhE1lmoB.htm](equipment/consumable-07-903CuhvVUhE1lmoB.htm)|Corrosive Ammunition|Munition corrosive|libre|
|[consumable-07-9ignmYCACjfzkxDQ.htm](equipment/consumable-07-9ignmYCACjfzkxDQ.htm)|Serum of Sex Shift|Sérum de changement de sexe|libre|
|[consumable-07-9zyrMZF76hMxwizY.htm](equipment/consumable-07-9zyrMZF76hMxwizY.htm)|Life-Boosting Oil (Moderate)|Huile stimulatrice de vie modérée|libre|
|[consumable-07-A8Rv4EWEiQEaNSEX.htm](equipment/consumable-07-A8Rv4EWEiQEaNSEX.htm)|Magnetic Shot (Lesser)|Tir magnétique inférieur|libre|
|[consumable-07-aBOPYlfHAcXUmhF7.htm](equipment/consumable-07-aBOPYlfHAcXUmhF7.htm)|Giant Wasp Venom|Venin de guêpe géante|officielle|
|[consumable-07-AUHgdsygq9gaOZh4.htm](equipment/consumable-07-AUHgdsygq9gaOZh4.htm)|Smoke Screen Snare (Greater)|Piège artisanal d'écran de fumée supérieur|libre|
|[consumable-07-B3zoAu9NpUEHGe22.htm](equipment/consumable-07-B3zoAu9NpUEHGe22.htm)|Seventh Prism (Pentagonal)|Septième prisme pentagonal|libre|
|[consumable-07-bh3HJkgWC05VSXgs.htm](equipment/consumable-07-bh3HJkgWC05VSXgs.htm)|Frozen Lava of Blackpeak|Lave gelée de Picnoir|libre|
|[consumable-07-BnYABzPAgdU89nLk.htm](equipment/consumable-07-BnYABzPAgdU89nLk.htm)|Serpent Oil (Greater)|Huile de serpent supérieure|libre|
|[consumable-07-BzGI4g44y6LiHEPp.htm](equipment/consumable-07-BzGI4g44y6LiHEPp.htm)|Weapon-Weird Oil|Huile d'arme bizarre|libre|
|[consumable-07-C35j9PXcyDdrTsat.htm](equipment/consumable-07-C35j9PXcyDdrTsat.htm)|Dragon Throat Scale|Écaille de gorge de dragon|libre|
|[consumable-07-C5RNvjvyq655pphC.htm](equipment/consumable-07-C5RNvjvyq655pphC.htm)|Empath's Cordial|Cordial de l'empathe|libre|
|[consumable-07-ChPOYRnhwpkEJExQ.htm](equipment/consumable-07-ChPOYRnhwpkEJExQ.htm)|Warding Punch|Punch de protection|libre|
|[consumable-07-d5aCuFS1dhKXhsZ0.htm](equipment/consumable-07-d5aCuFS1dhKXhsZ0.htm)|Addlebrain|Brouillecervelle|officielle|
|[consumable-07-DPNuM3699xULCbmF.htm](equipment/consumable-07-DPNuM3699xULCbmF.htm)|Curled Cure Gel|Gel curatif frisé|libre|
|[consumable-07-dUREATQUwEOGiV13.htm](equipment/consumable-07-dUREATQUwEOGiV13.htm)|Oil of Ownership (Moderate)|Huile de propriété modérée|libre|
|[consumable-07-eEIWjTvZyKsKhYaz.htm](equipment/consumable-07-eEIWjTvZyKsKhYaz.htm)|Grim Trophy|Sinistre trophée|libre|
|[consumable-07-ehss8yPTXxiUdVlJ.htm](equipment/consumable-07-ehss8yPTXxiUdVlJ.htm)|Smoke Ball (Greater)|Boule de fumée supérieure|libre|
|[consumable-07-EkC2W5A5fohoIKSd.htm](equipment/consumable-07-EkC2W5A5fohoIKSd.htm)|Ration Tonic (Greater)|Ration énergisante supérieure|libre|
|[consumable-07-EMviiUBFb5hB1X5Q.htm](equipment/consumable-07-EMviiUBFb5hB1X5Q.htm)|Spiritsight Tea|Thé de détection spectrale|libre|
|[consumable-07-EN6Kdc6q5aRTlPS5.htm](equipment/consumable-07-EN6Kdc6q5aRTlPS5.htm)|Envenomed Snare|Piège artisanal venimeux|libre|
|[consumable-07-FgAPV0iLE6R1QMJ5.htm](equipment/consumable-07-FgAPV0iLE6R1QMJ5.htm)|Skinstitch Salve|Pommade Sutures|libre|
|[consumable-07-Fgcc2OXLIKXWjbSp.htm](equipment/consumable-07-Fgcc2OXLIKXWjbSp.htm)|Swirling Sand|Sable tourbillonnant|libre|
|[consumable-07-fkGHYeGVK6O2VW1s.htm](equipment/consumable-07-fkGHYeGVK6O2VW1s.htm)|Red Dragon's Breath Potion (Young)|Potion de souffle de jeune dragon rouge|officielle|
|[consumable-07-fpqYMeX0GXvTSlVQ.htm](equipment/consumable-07-fpqYMeX0GXvTSlVQ.htm)|Red-Handed Missive|Missive main-rouge|libre|
|[consumable-07-fVKiVp6yG85ADrR6.htm](equipment/consumable-07-fVKiVp6yG85ADrR6.htm)|Draconic Toxin Bottle|Bouteille de toxine draconique|libre|
|[consumable-07-G1YeBTz26wnL2Meo.htm](equipment/consumable-07-G1YeBTz26wnL2Meo.htm)|Torrent Spellgun (Moderate)|Lancesort torrent modéré|libre|
|[consumable-07-HUPCdlFkJQC7Krim.htm](equipment/consumable-07-HUPCdlFkJQC7Krim.htm)|Starshot Arrow (Lesser)|Flèche étoile inférieure|libre|
|[consumable-07-j08PcMwLhz7lWY3M.htm](equipment/consumable-07-j08PcMwLhz7lWY3M.htm)|Persistent Lodestone|Magnétite persistante|libre|
|[consumable-07-J7ZoRlhqiP6988t2.htm](equipment/consumable-07-J7ZoRlhqiP6988t2.htm)|Red-Rib Gill Mask (Moderate)|Masque branchie de Côte-rouge modéré|libre|
|[consumable-07-JvpiniS6wTHsDoFX.htm](equipment/consumable-07-JvpiniS6wTHsDoFX.htm)|Sloughing Toxin|Toxine de mollesse|libre|
|[consumable-07-KadHRRccZ5rWRl7c.htm](equipment/consumable-07-KadHRRccZ5rWRl7c.htm)|Fire and Iceberg|Feu et iceberg|libre|
|[consumable-07-Kc4d504GnbS0NcoS.htm](equipment/consumable-07-Kc4d504GnbS0NcoS.htm)|Messenger Missive (Multiple)|Missive messager multiple|libre|
|[consumable-07-KlrP8xrqFcbDkGLF.htm](equipment/consumable-07-KlrP8xrqFcbDkGLF.htm)|Emergency Eye|Oeil d'urgence|libre|
|[consumable-07-ktRGlgegUBZcr0aJ.htm](equipment/consumable-07-ktRGlgegUBZcr0aJ.htm)|Saints' Balm|Baume du Saint|libre|
|[consumable-07-kzfRjSrq4JNZlc32.htm](equipment/consumable-07-kzfRjSrq4JNZlc32.htm)|Whisper Briolette|Briolette de Chuchotement|libre|
|[consumable-07-LBhzIWBH9TJ8A2K5.htm](equipment/consumable-07-LBhzIWBH9TJ8A2K5.htm)|Invisible Net|Filet invisible|libre|
|[consumable-07-LKwI47eeJC0Y4hGu.htm](equipment/consumable-07-LKwI47eeJC0Y4hGu.htm)|Silver Dragon's Breath Potion (Young)|Potion de souffle du jeune dragon d'argent|officielle|
|[consumable-07-mAgIN0A0ywnhtefw.htm](equipment/consumable-07-mAgIN0A0ywnhtefw.htm)|Owlbear Egg|Oeuf d'hibours|libre|
|[consumable-07-MdX3CWn1nJhOkqSr.htm](equipment/consumable-07-MdX3CWn1nJhOkqSr.htm)|Lightning Rod Shot|Tir paratonnerre|libre|
|[consumable-07-MLbE6FZGXKcoTB52.htm](equipment/consumable-07-MLbE6FZGXKcoTB52.htm)|Clinging Bubbles (Moderate)|Bulles accrochées modérées|libre|
|[consumable-07-mz0GbCF2WT1DSYac.htm](equipment/consumable-07-mz0GbCF2WT1DSYac.htm)|Spellstrike Ammunition (Type III)|Munition de frappe magique (Type III)|libre|
|[consumable-07-NKRuXILXLlDbxTs9.htm](equipment/consumable-07-NKRuXILXLlDbxTs9.htm)|Faerie Dragon Liqueur (Young)|Liqueur de jeune dragon féerique|libre|
|[consumable-07-nn2HAN1XKkKHfVnM.htm](equipment/consumable-07-nn2HAN1XKkKHfVnM.htm)|Stupor Poison|Poison de stupeur|libre|
|[consumable-07-O3xp1TzJgSTEeqz9.htm](equipment/consumable-07-O3xp1TzJgSTEeqz9.htm)|Energizing Treat|Bonbon énergisant|libre|
|[consumable-07-OcBPjVplvy2GbQ8P.htm](equipment/consumable-07-OcBPjVplvy2GbQ8P.htm)|Comprehension Elixir (Greater)|Élixir de compréhension supérieur|officielle|
|[consumable-07-PblDdXHm98XWs9ba.htm](equipment/consumable-07-PblDdXHm98XWs9ba.htm)|Topology Protoplasm|Morphologie protoplasmique|libre|
|[consumable-07-pF4ggWXiiNksDKsU.htm](equipment/consumable-07-pF4ggWXiiNksDKsU.htm)|Smother Shroud|Linceul d'étouffement|libre|
|[consumable-07-PhtAfMPSrppdM9wT.htm](equipment/consumable-07-PhtAfMPSrppdM9wT.htm)|Ensnaring Disk|Disque piégeur|libre|
|[consumable-07-PVpViB2Wg3f61dFa.htm](equipment/consumable-07-PVpViB2Wg3f61dFa.htm)|Frost Breath|Souffle frigorifiant|libre|
|[consumable-07-QfdcOOHve9eNpsB9.htm](equipment/consumable-07-QfdcOOHve9eNpsB9.htm)|Fairy Bullet|Balle féerique|libre|
|[consumable-07-QLmgHzDlWuwaylPm.htm](equipment/consumable-07-QLmgHzDlWuwaylPm.htm)|Hovering Potion|Potion de vol stationnaire|libre|
|[consumable-07-Qm0ZiyXail7Lv0dq.htm](equipment/consumable-07-Qm0ZiyXail7Lv0dq.htm)|Ablative Armor Plating (Greater)|Blindage ablatif d'armure supérieur|libre|
|[consumable-07-qNIv0owESzOzo1s0.htm](equipment/consumable-07-qNIv0owESzOzo1s0.htm)|Black Dragon's Breath Potion (Young)|Potion de souffle de jeune dragon noir|officielle|
|[consumable-07-QSQZJ5BC3DeHv153.htm](equipment/consumable-07-QSQZJ5BC3DeHv153.htm)|Scroll of 4th-rank Spell|Parchemin de sort de rang 4|libre|
|[consumable-07-qwCefJjr7AJSqYay.htm](equipment/consumable-07-qwCefJjr7AJSqYay.htm)|Succubus Kiss|Baiser de la succube|libre|
|[consumable-07-RcVJ30230zAeLILV.htm](equipment/consumable-07-RcVJ30230zAeLILV.htm)|Stalagmite Seed|Graine de stalagmite|libre|
|[consumable-07-RuGGAFaTHMCC6chN.htm](equipment/consumable-07-RuGGAFaTHMCC6chN.htm)|Toadskin Salve (Greater)|Baume Peau de crapaud supérieur|libre|
|[consumable-07-SH0WmAdjIaNN3OiN.htm](equipment/consumable-07-SH0WmAdjIaNN3OiN.htm)|Grudgestone|Pierre de la rancune|libre|
|[consumable-07-SUn5qFbHYTLobgTt.htm](equipment/consumable-07-SUn5qFbHYTLobgTt.htm)|Ablative Shield Plating (Greater)|Blindage ablatif de bouclier supérieur|libre|
|[consumable-07-t3NpyArNDTpY6qU8.htm](equipment/consumable-07-t3NpyArNDTpY6qU8.htm)|Cold Comfort (Lesser)|Confort fraicheur inférieur|libre|
|[consumable-07-T7o5nMvWS8YeJOst.htm](equipment/consumable-07-T7o5nMvWS8YeJOst.htm)|Black Tendril Shot (Lesser)|Tir de vrilles noires inférieur|libre|
|[consumable-07-TqX34x9CijryfrlM.htm](equipment/consumable-07-TqX34x9CijryfrlM.htm)|Brass Dragon's Breath Potion (Young)|Potion de souffle de jeune dragon d'airain|libre|
|[consumable-07-TSbuKm91qmqdqQW3.htm](equipment/consumable-07-TSbuKm91qmqdqQW3.htm)|Putrescent Glob|Goutte putride|libre|
|[consumable-07-TzXNMv7tnFy1FYgg.htm](equipment/consumable-07-TzXNMv7tnFy1FYgg.htm)|Bronze Dragon's Breath Potion (Young)|Potion de souffle du jeune dragon de bronze|officielle|
|[consumable-07-ud970e7uPVEqx1FO.htm](equipment/consumable-07-ud970e7uPVEqx1FO.htm)|Big Rock Bullet|Bille gros rocher|libre|
|[consumable-07-uNdRC0j1hY3F4pP1.htm](equipment/consumable-07-uNdRC0j1hY3F4pP1.htm)|Stepping Stone Shot|Tir pierre de gué|libre|
|[consumable-07-vPcDIowg3LAqZYAE.htm](equipment/consumable-07-vPcDIowg3LAqZYAE.htm)|Energy Breath Potion (Acid, Lesser)|Potion de souffle d'énergie corrosive inférieure|libre|
|[consumable-07-vqGAy24GXV3jyGoZ.htm](equipment/consumable-07-vqGAy24GXV3jyGoZ.htm)|Ferrofluid Urchin (Lesser)|Oursin ferrofluide inférieur|libre|
|[consumable-07-vvgCQzWkScCzte7K.htm](equipment/consumable-07-vvgCQzWkScCzte7K.htm)|Green Dragon's Breath Potion (Young)|Potion de souffle du jeune dragon vert|officielle|
|[consumable-07-VvljzRwthKMgqUR3.htm](equipment/consumable-07-VvljzRwthKMgqUR3.htm)|Feather Token (Anchor)|Figurine merveilleuse ancre|officielle|
|[consumable-07-VwzIiWAtbJCgGOEz.htm](equipment/consumable-07-VwzIiWAtbJCgGOEz.htm)|Isolation Draught|Breuvage d'isolement|libre|
|[consumable-07-wrDmWkGxmwzYtfiA.htm](equipment/consumable-07-wrDmWkGxmwzYtfiA.htm)|Magic Wand (3rd-Rank Spell)|Baguette magique (sort de rang 3)|libre|
|[consumable-07-XfQSkbjxVHF82ael.htm](equipment/consumable-07-XfQSkbjxVHF82ael.htm)|Condensed Mana|Mana condensée|libre|
|[consumable-07-XpmPX3ScEOBgAoKd.htm](equipment/consumable-07-XpmPX3ScEOBgAoKd.htm)|Candle of Revealing|Bougie de révélation|libre|
|[consumable-07-xpVHMxLcwgXzL39e.htm](equipment/consumable-07-xpVHMxLcwgXzL39e.htm)|Energy Breath Potion (Electricity, Lesser)|Potion de souffle d'énergie électrique inférieure|libre|
|[consumable-07-xRrMmFzpa0WU9uV4.htm](equipment/consumable-07-xRrMmFzpa0WU9uV4.htm)|Energy Breath Potion (Cold, Lesser)|Potion de souffle du d'énergie inférieure|libre|
|[consumable-07-ydsXPuDjG2hPspw6.htm](equipment/consumable-07-ydsXPuDjG2hPspw6.htm)|Dimensional Knot|Noeud dimensionnel|libre|
|[consumable-07-YGaO4HyH6jn3P731.htm](equipment/consumable-07-YGaO4HyH6jn3P731.htm)|Murderer's Knot|Noeud du meurtier|libre|
|[consumable-07-zAn1SNGYW93kmzdn.htm](equipment/consumable-07-zAn1SNGYW93kmzdn.htm)|Energy Breath Potion (Sonic, Lesser)|Potion de souffle d'énergie sonique inférieure|libre|
|[consumable-07-zFYyegCfUlyCJhgu.htm](equipment/consumable-07-zFYyegCfUlyCJhgu.htm)|Server's Stew|Ragoût du serveur|libre|
|[consumable-08-0Wxiz4hJPERJPDAF.htm](equipment/consumable-08-0Wxiz4hJPERJPDAF.htm)|Detect Anathema Fulu|Fulu de détection d'anathème|libre|
|[consumable-08-25Rr05SIfTj0GA31.htm](equipment/consumable-08-25Rr05SIfTj0GA31.htm)|Potion of Disguise (Moderate)|Potion de déguisement modéré|libre|
|[consumable-08-2koNKqbQV05myfuL.htm](equipment/consumable-08-2koNKqbQV05myfuL.htm)|Dust of Corpse Animation|Poudre d'animation de cadavre|libre|
|[consumable-08-3hhRWYl1O2FRfAbn.htm](equipment/consumable-08-3hhRWYl1O2FRfAbn.htm)|Ghost Delivery Fulu|Fulu de livraison fantôme|libre|
|[consumable-08-3viqxerOKUlf7mV0.htm](equipment/consumable-08-3viqxerOKUlf7mV0.htm)|Bloodhammer Reserve Black Label|Réserve Sangmartel étiquette noire|libre|
|[consumable-08-4MfrhRlz4VeYtYVn.htm](equipment/consumable-08-4MfrhRlz4VeYtYVn.htm)|Dragon Turtle Scale (Greater)|Écaille de tortue dragon supérieure|libre|
|[consumable-08-5Q6EFM1MSRX6WNTE.htm](equipment/consumable-08-5Q6EFM1MSRX6WNTE.htm)|Exsanguinating Ammunition (Greater)|Munition exsangue supérieure|libre|
|[consumable-08-5xsfj30uXMKINxnk.htm](equipment/consumable-08-5xsfj30uXMKINxnk.htm)|Shrinking Potion (Greater)|Potion de rétrécissement supérieur|libre|
|[consumable-08-61mFRFnaCLHDtvdv.htm](equipment/consumable-08-61mFRFnaCLHDtvdv.htm)|Candle of Truth|Cierge de vérité|libre|
|[consumable-08-6KdYdFovFBivwI8M.htm](equipment/consumable-08-6KdYdFovFBivwI8M.htm)|Striking Snare|Piège artisanal de frappe|libre|
|[consumable-08-8BDBsf55gP0UW07Y.htm](equipment/consumable-08-8BDBsf55gP0UW07Y.htm)|Scour|Érode|officielle|
|[consumable-08-9FwGY0X3v4ZksZH7.htm](equipment/consumable-08-9FwGY0X3v4ZksZH7.htm)|Skull Bomb|Bombe crânienne|libre|
|[consumable-08-axU0I9xIm4xm2VPH.htm](equipment/consumable-08-axU0I9xIm4xm2VPH.htm)|Bomb Snare|Piège artisanal de bombes|libre|
|[consumable-08-AYKjrJnMo6mWCt2e.htm](equipment/consumable-08-AYKjrJnMo6mWCt2e.htm)|Firefoot Popcorn|Popcorn piedenflammé|libre|
|[consumable-08-bz8Y8bVUe7QfBk9g.htm](equipment/consumable-08-bz8Y8bVUe7QfBk9g.htm)|Darkvision Elixir (Greater)|Élixir de vision dans le noir supérieur|libre|
|[consumable-08-C0kSU5KWMzLngvTa.htm](equipment/consumable-08-C0kSU5KWMzLngvTa.htm)|Oil of Object Animation|Huile d'animation d'objet|libre|
|[consumable-08-cKqZe5imzxqSnzwD.htm](equipment/consumable-08-cKqZe5imzxqSnzwD.htm)|Jade Bauble|Babiole de jade|libre|
|[consumable-08-cvkKe1FUVW9kOKcC.htm](equipment/consumable-08-cvkKe1FUVW9kOKcC.htm)|Seven-Color Raw Fish Salad|Salade de poisson cru aux sept couleurs|libre|
|[consumable-08-d6u3xqPPvx3xWaqF.htm](equipment/consumable-08-d6u3xqPPvx3xWaqF.htm)|Animal Nip (Moderate)|Herbe aux animaux modérée|libre|
|[consumable-08-D8aBxqJW61WFiQM2.htm](equipment/consumable-08-D8aBxqJW61WFiQM2.htm)|Liquid Gold (Greater)|Or liquide supérieur|libre|
|[consumable-08-etgPdPhPviKkaFO8.htm](equipment/consumable-08-etgPdPhPviKkaFO8.htm)|Octopus Bottle|Bouteille de la pieuvre|libre|
|[consumable-08-eVowRVEo42PBFvNK.htm](equipment/consumable-08-eVowRVEo42PBFvNK.htm)|Barricade Stone (Cube)|Pierre de barricade (cube)|libre|
|[consumable-08-ezSqiHhz2OYTzoF2.htm](equipment/consumable-08-ezSqiHhz2OYTzoF2.htm)|Potion of Shared Life|Potion de vie partagée|libre|
|[consumable-08-FClD2GoWpq3qdPQl.htm](equipment/consumable-08-FClD2GoWpq3qdPQl.htm)|Unsullied Blood (Moderate)|Sang non souillé modéré|libre|
|[consumable-08-gckU3BF9uQompDOy.htm](equipment/consumable-08-gckU3BF9uQompDOy.htm)|Chain of Stars|Chaînette d'étoiles|libre|
|[consumable-08-gM1ZYvJacCdZsWo0.htm](equipment/consumable-08-gM1ZYvJacCdZsWo0.htm)|Orchestral Brooch|Broche orchestrale|libre|
|[consumable-08-gQuQetoDwP3GLtQy.htm](equipment/consumable-08-gQuQetoDwP3GLtQy.htm)|Clockwork Spider Bomb|Araignée mécanique explosive|libre|
|[consumable-08-hdxm6sdXLzI7ozul.htm](equipment/consumable-08-hdxm6sdXLzI7ozul.htm)|Potion Patch (Moderate)|Bandage de potion modéré|libre|
|[consumable-08-k1S8AdYhi5JGRLh1.htm](equipment/consumable-08-k1S8AdYhi5JGRLh1.htm)|Smoke Fan (Greater)|Ventilateur de fumée supérieur|libre|
|[consumable-08-KdeeRCrtsDCJLfgc.htm](equipment/consumable-08-KdeeRCrtsDCJLfgc.htm)|Nettleweed Residue|Résidu de lierrortie|officielle|
|[consumable-08-KgF3nPT2tEskXqSS.htm](equipment/consumable-08-KgF3nPT2tEskXqSS.htm)|Metalmist Sphere (Moderate)|Sphère de brume-métal modérée|libre|
|[consumable-08-KlJSw919hpN6V9oK.htm](equipment/consumable-08-KlJSw919hpN6V9oK.htm)|Potion of Flying (Standard)|Potion de vol de base|libre|
|[consumable-08-kORpovlPYicysr2g.htm](equipment/consumable-08-kORpovlPYicysr2g.htm)|Potion of Quickness|Potion de rapidité|libre|
|[consumable-08-KsAygNRyR2FdwuOm.htm](equipment/consumable-08-KsAygNRyR2FdwuOm.htm)|Psychic Colors Elixir|Élixir des couleurs psychiques|libre|
|[consumable-08-LbRuAXvoACxPqHqn.htm](equipment/consumable-08-LbRuAXvoACxPqHqn.htm)|Feyfoul (Moderate)|Cafouillefée modéré|libre|
|[consumable-08-mIqL9F9e1F1FmVgF.htm](equipment/consumable-08-mIqL9F9e1F1FmVgF.htm)|Ghost Courier Fulu|Fulu livreur fantôme|libre|
|[consumable-08-mQSV1gq64kKeAfuN.htm](equipment/consumable-08-mQSV1gq64kKeAfuN.htm)|Scarlet Mist|Brume écarlate|libre|
|[consumable-08-Mtqc1UzOotdclIEb.htm](equipment/consumable-08-Mtqc1UzOotdclIEb.htm)|Incense Bundle of Annual Blessings|Lot d'encens de bénédiction annuelle|libre|
|[consumable-08-MywpJQc2lVjWvAGA.htm](equipment/consumable-08-MywpJQc2lVjWvAGA.htm)|Clockwork Goggles (Major)|Lunettes mécaniques majeures|libre|
|[consumable-08-NLUsrnvQsYK0CYp3.htm](equipment/consumable-08-NLUsrnvQsYK0CYp3.htm)|Nimbus Breath|Souffle nimbus|libre|
|[consumable-08-NXYEgMq4ERIzbJKx.htm](equipment/consumable-08-NXYEgMq4ERIzbJKx.htm)|Rusting Snare|Piège artisanal de rouille|libre|
|[consumable-08-o7kaj57buBADMLhV.htm](equipment/consumable-08-o7kaj57buBADMLhV.htm)|Chameleon Suit (Greater)|Tenue de caméléon supérieure|libre|
|[consumable-08-OCAwJ6CAqS0YkHCU.htm](equipment/consumable-08-OCAwJ6CAqS0YkHCU.htm)|Stormfeather|Plume tempête|libre|
|[consumable-08-OHxRdRv4ZLBGPQi6.htm](equipment/consumable-08-OHxRdRv4ZLBGPQi6.htm)|Animal Repellent (Moderate)|Répulsif animal modéré|libre|
|[consumable-08-OOli8iQmLZoMOoG0.htm](equipment/consumable-08-OOli8iQmLZoMOoG0.htm)|Alloy Orb (Standard-Grade)|Orbe d'alliage de qualité standard|libre|
|[consumable-08-oTRUHPASPD4USU4o.htm](equipment/consumable-08-oTRUHPASPD4USU4o.htm)|Bloodseeker Beak (Greater)|Trompe de cherchesang supérieure|libre|
|[consumable-08-oYjlcrarrZoA01D0.htm](equipment/consumable-08-oYjlcrarrZoA01D0.htm)|Roaring Potion (Lesser)|Potion de rugissement inférieure|libre|
|[consumable-08-p070A88bOm8gi0sS.htm](equipment/consumable-08-p070A88bOm8gi0sS.htm)|Runescribed Disk|Disque gravé de runes|libre|
|[consumable-08-p0TKEjVqYFZmfz6v.htm](equipment/consumable-08-p0TKEjVqYFZmfz6v.htm)|Malleable Mixture (Lesser)|Mixture de mollesse inférieure|libre|
|[consumable-08-qDvsHszooFbPAqlN.htm](equipment/consumable-08-qDvsHszooFbPAqlN.htm)|Spun Cloud (Red)|Nuage tourbillonnant rouge|libre|
|[consumable-08-s0iiOMicSe4EspMM.htm](equipment/consumable-08-s0iiOMicSe4EspMM.htm)|Therapeutic Snap Peas|Pois mange-tout thérapeutique|libre|
|[consumable-08-tB1NYErrB94dTFuh.htm](equipment/consumable-08-tB1NYErrB94dTFuh.htm)|Charm of the Ordinary|Charme de l'ordinaire|libre|
|[consumable-08-TFnr9Gq7VXPJu0GQ.htm](equipment/consumable-08-TFnr9Gq7VXPJu0GQ.htm)|Grasping Snare|Piège artisanal agrippant|libre|
|[consumable-08-TXkHGUqvqIko7W9m.htm](equipment/consumable-08-TXkHGUqvqIko7W9m.htm)|Thousand-Pains Fulu (Blade)|Fulu des mille douleurs (Lame)|libre|
|[consumable-08-U5G4FXAUvtxhYLYj.htm](equipment/consumable-08-U5G4FXAUvtxhYLYj.htm)|Thawing Candle|Bougie de dégel|libre|
|[consumable-08-u6g7AClRFEAj4lf4.htm](equipment/consumable-08-u6g7AClRFEAj4lf4.htm)|Gallows Tooth|Dent du gibet|libre|
|[consumable-08-uy2U0qftoOEOpygw.htm](equipment/consumable-08-uy2U0qftoOEOpygw.htm)|Spirit Snare|Piège artisanal à esprit|libre|
|[consumable-08-VvN9EjaBLwYg13z3.htm](equipment/consumable-08-VvN9EjaBLwYg13z3.htm)|Curare|Curare|libre|
|[consumable-08-WGbE7qAGw8O5yH07.htm](equipment/consumable-08-WGbE7qAGw8O5yH07.htm)|Vermin Repellent Agent (Moderate)|Agent répulsif de vermine modéré|libre|
|[consumable-08-WL4O32qFifxnMj0H.htm](equipment/consumable-08-WL4O32qFifxnMj0H.htm)|Wyvern Poison|Poison de vouivre|libre|
|[consumable-08-wRBz9mjTO9ZqXch6.htm](equipment/consumable-08-wRBz9mjTO9ZqXch6.htm)|Feather Token (Tree) (Ammunition)|Figurine merveilleuse arbre (Munition)|libre|
|[consumable-08-wtNZDBNjFsrrFjOR.htm](equipment/consumable-08-wtNZDBNjFsrrFjOR.htm)|Cayden's Brew|Breuvage de Cayden|libre|
|[consumable-08-WyOF9Zy2FHtZsCa7.htm](equipment/consumable-08-WyOF9Zy2FHtZsCa7.htm)|Quenching Potion|Potion purifiante|libre|
|[consumable-08-WzsKbMMewXbf1nws.htm](equipment/consumable-08-WzsKbMMewXbf1nws.htm)|Marvelous Miniature (Boat)|Figurine merveilleuse bateau|libre|
|[consumable-08-xcwPOFTAjQpmiFsN.htm](equipment/consumable-08-xcwPOFTAjQpmiFsN.htm)|Warpwobble Poison|Poison distordeur|libre|
|[consumable-08-XdHMroAomWalt7I9.htm](equipment/consumable-08-XdHMroAomWalt7I9.htm)|Poison Fizz (Lesser)|Poison pétillant inférieur|libre|
|[consumable-08-Xjjj7uHQzbOwENYV.htm](equipment/consumable-08-Xjjj7uHQzbOwENYV.htm)|Healing Vapor (Moderate)|Vapeur de guérison modérée|libre|
|[consumable-08-Y2dyidYfphGo7qvs.htm](equipment/consumable-08-Y2dyidYfphGo7qvs.htm)|Sighting Shot|Tir de visée|libre|
|[consumable-08-YnhcVcTbXrVfiH83.htm](equipment/consumable-08-YnhcVcTbXrVfiH83.htm)|Mnemonic Acid|Acide mnémonique|libre|
|[consumable-08-yVCZrZLrLcIsR8aH.htm](equipment/consumable-08-yVCZrZLrLcIsR8aH.htm)|Brine Dragon Scale|Éccaille de dragon de saumure|libre|
|[consumable-08-z3IG6t2o0zsfP2XQ.htm](equipment/consumable-08-z3IG6t2o0zsfP2XQ.htm)|Hype|Adrénaline|libre|
|[consumable-08-zg4aR2ndtTzO1osa.htm](equipment/consumable-08-zg4aR2ndtTzO1osa.htm)|Galvanic Chew|Gomme galvanique|libre|
|[consumable-09-0GY1BYECGqz6YJ88.htm](equipment/consumable-09-0GY1BYECGqz6YJ88.htm)|Noxious Incense|Encens nocif|libre|
|[consumable-09-0jhX5ibOIt77UmPP.htm](equipment/consumable-09-0jhX5ibOIt77UmPP.htm)|Octopus Potion (Moderate)|Potion pieuvre modérée|libre|
|[consumable-09-2pfQkBdEfA5Fxuqg.htm](equipment/consumable-09-2pfQkBdEfA5Fxuqg.htm)|Vital Earth|Terre vitale|libre|
|[consumable-09-4AqxogIf4G5qZsq3.htm](equipment/consumable-09-4AqxogIf4G5qZsq3.htm)|Terror Spores|Spores terrorisantes|libre|
|[consumable-09-5jUIOETCI3QG69cS.htm](equipment/consumable-09-5jUIOETCI3QG69cS.htm)|Amnemonic Charm|Charme amnésique|libre|
|[consumable-09-6UoPcaXO70MNyU6W.htm](equipment/consumable-09-6UoPcaXO70MNyU6W.htm)|Sparking Spellgun (Moderate)|Lancesort étincelant modéré|libre|
|[consumable-09-6xS59NsgGLTEUW4m.htm](equipment/consumable-09-6xS59NsgGLTEUW4m.htm)|Bewildering Spellgun|Lancesort déconcertant|libre|
|[consumable-09-9BNMhEZ2ztFtu2WU.htm](equipment/consumable-09-9BNMhEZ2ztFtu2WU.htm)|Spun Cloud (Green)|Nuage tourbillonnant vert|libre|
|[consumable-09-9XSOAzq3xp9g6qkF.htm](equipment/consumable-09-9XSOAzq3xp9g6qkF.htm)|Dust of Disappearance|Poudre de disparition|libre|
|[consumable-09-aSRUX4WGfalK4A5J.htm](equipment/consumable-09-aSRUX4WGfalK4A5J.htm)|Potency Crystal (Greater)|Cristal de puissance supérieure|libre|
|[consumable-09-aSvDtG5i5l7scgru.htm](equipment/consumable-09-aSvDtG5i5l7scgru.htm)|Assassin Vine Wine|Vin de liane meurtrière|libre|
|[consumable-09-Bajwrol5y2n2ZIVm.htm](equipment/consumable-09-Bajwrol5y2n2ZIVm.htm)|Crackling Bubble Gum (Moderate)|Chewing-gum crépitant modéré|libre|
|[consumable-09-BFre5eREvbqQdwPC.htm](equipment/consumable-09-BFre5eREvbqQdwPC.htm)|Chromatic Jellyfish Oil (Lesser)|Huile de méduse chromatique inférieure|libre|
|[consumable-09-bImVngTkDNsdWIjr.htm](equipment/consumable-09-bImVngTkDNsdWIjr.htm)|Searing Suture (Greater)|Suture douloureuse supérieure|libre|
|[consumable-09-bwqyL0H9Ssa7dedV.htm](equipment/consumable-09-bwqyL0H9Ssa7dedV.htm)|Blast Boots (Greater)|Bottes d'explosion supérieures|libre|
|[consumable-09-c21stU5rhN4F2fZl.htm](equipment/consumable-09-c21stU5rhN4F2fZl.htm)|Elixir of Life (Moderate)|Élixir de vie modéré|libre|
|[consumable-09-ca2lzxfJxvuLDrKu.htm](equipment/consumable-09-ca2lzxfJxvuLDrKu.htm)|Lich Dust|Poussière de liche|officielle|
|[consumable-09-CfM2q0UgtG6gkPku.htm](equipment/consumable-09-CfM2q0UgtG6gkPku.htm)|Horned Lion Amulet|Amulette du lion cornu|libre|
|[consumable-09-cjAyXfWmn0IpOsaY.htm](equipment/consumable-09-cjAyXfWmn0IpOsaY.htm)|Impact Foam Chassis (Greater)|Armature de mousse supérieure|libre|
|[consumable-09-Dc9BJcvL5zOoxAJd.htm](equipment/consumable-09-Dc9BJcvL5zOoxAJd.htm)|Ixamè's Eye|Oeil d'Ixamè|libre|
|[consumable-09-DZHV8R2908H2cVWM.htm](equipment/consumable-09-DZHV8R2908H2cVWM.htm)|Mistranslator's Draft|Brouillon rectulien de mauvaise traduction|libre|
|[consumable-09-F4WcgJ3NB1kiF7OL.htm](equipment/consumable-09-F4WcgJ3NB1kiF7OL.htm)|Ghostly Portal Paint|Peinture à portail spectral|libre|
|[consumable-09-g6NpdrVR56B2jI64.htm](equipment/consumable-09-g6NpdrVR56B2jI64.htm)|Mourner's Dawnlight Fulu|Fulu de l'aube du mourant|libre|
|[consumable-09-GXH1PSioEkxTDceK.htm](equipment/consumable-09-GXH1PSioEkxTDceK.htm)|Caustic Deteriorating Dust|Poudre détériorante caustique|libre|
|[consumable-09-HNacrCOSJKJ6zn5t.htm](equipment/consumable-09-HNacrCOSJKJ6zn5t.htm)|Emetic Paste (Moderate)|Pâte vomitive modérée|libre|
|[consumable-09-Ja0KlgXV8DsOEkif.htm](equipment/consumable-09-Ja0KlgXV8DsOEkif.htm)|Oil of Corpse Restoration|Huile de restauration de cadavre|libre|
|[consumable-09-jnCP2zYJamUeZsJC.htm](equipment/consumable-09-jnCP2zYJamUeZsJC.htm)|Cold Iron Blanch (Moderate)|Blanchis de fer froid modéré|libre|
|[consumable-09-kOHWKOo4DFS8wQbE.htm](equipment/consumable-09-kOHWKOo4DFS8wQbE.htm)|Bottled Roc|Roc en bouteille|libre|
|[consumable-09-L4Db3CncPcZFPzGN.htm](equipment/consumable-09-L4Db3CncPcZFPzGN.htm)|Spiderfoot Brew (Moderate)|Infusion de pattes d'araignée supérieure|libre|
|[consumable-09-LbgblC0CzbTWJTmC.htm](equipment/consumable-09-LbgblC0CzbTWJTmC.htm)|Moonlit Spellgun (Moderate)|Lancesort clair de lune modéré|libre|
|[consumable-09-mBvMWiFb6VB0P3Fk.htm](equipment/consumable-09-mBvMWiFb6VB0P3Fk.htm)|Feast of Hungry Ghosts|Festin des fantômes affamés|libre|
|[consumable-09-MlzPOG2v9bO4XoLg.htm](equipment/consumable-09-MlzPOG2v9bO4XoLg.htm)|Brewer's Regret|Regret du brasseur|libre|
|[consumable-09-Mqayp3cYaZItkPsj.htm](equipment/consumable-09-Mqayp3cYaZItkPsj.htm)|Frost Worm Snare|Piège artisanal de ver de givre|libre|
|[consumable-09-mtfdonWkYLj0BuW3.htm](equipment/consumable-09-mtfdonWkYLj0BuW3.htm)|Puff Dragon|Dragon bouffée|libre|
|[consumable-09-nrY1yxGbl5MFUyea.htm](equipment/consumable-09-nrY1yxGbl5MFUyea.htm)|Indomitable Keepsake (Greater)|Souvenir indomptable supérieur|libre|
|[consumable-09-nYFnlWTNS6s7EcyF.htm](equipment/consumable-09-nYFnlWTNS6s7EcyF.htm)|Rebirth Potion|Potion de renaissance|libre|
|[consumable-09-oOXvk18K4izaJzG7.htm](equipment/consumable-09-oOXvk18K4izaJzG7.htm)|Spider Root|Racine arachnide|libre|
|[consumable-09-pDbWjlhIoQpUIvLb.htm](equipment/consumable-09-pDbWjlhIoQpUIvLb.htm)|Privacy Ward Fulu (Chamber)|Fulu de protection de la confidentialité (Chambre)|libre|
|[consumable-09-PTSdJqjiybZDxi4r.htm](equipment/consumable-09-PTSdJqjiybZDxi4r.htm)|Potion of Minute Echoes|Potion des minuscules échos|libre|
|[consumable-09-QbuAXdWjwXlSk1pa.htm](equipment/consumable-09-QbuAXdWjwXlSk1pa.htm)|Careless Delight|Délice insouciant|libre|
|[consumable-09-QGgTedo2uMi6e9PR.htm](equipment/consumable-09-QGgTedo2uMi6e9PR.htm)|Frozen Lava of Pale Mountain|Lave gelée de la Montagne pâle|libre|
|[consumable-09-qqiD1FLntgShkCVY.htm](equipment/consumable-09-qqiD1FLntgShkCVY.htm)|Watchful Portrait|Portrait vigilant|libre|
|[consumable-09-rEx0S0p3cIzybhvV.htm](equipment/consumable-09-rEx0S0p3cIzybhvV.htm)|Gecko Pads (Greater)|Coussinet de gecko supérieur|libre|
|[consumable-09-rKknk8odXDBpON5l.htm](equipment/consumable-09-rKknk8odXDBpON5l.htm)|Explosive Ammunition|Munition explosive|libre|
|[consumable-09-RkZgEViFodxXsLu3.htm](equipment/consumable-09-RkZgEViFodxXsLu3.htm)|Impossible Cake (Greater)|Gâteau de l'impossible supérieur|libre|
|[consumable-09-RpvH9EDquO0jS3Jz.htm](equipment/consumable-09-RpvH9EDquO0jS3Jz.htm)|Storm Arrow|Flèche d'orage|officielle|
|[consumable-09-rzRI8taFlNY8lbkl.htm](equipment/consumable-09-rzRI8taFlNY8lbkl.htm)|Body Recovery Kit|Kit de préservation des corps|libre|
|[consumable-09-sav38slFFlUVu75Z.htm](equipment/consumable-09-sav38slFFlUVu75Z.htm)|Abysium Powder|Poudre d'abysium|libre|
|[consumable-09-Sn7v9SsbEDMUIwrO.htm](equipment/consumable-09-Sn7v9SsbEDMUIwrO.htm)|Magic Wand (4th-Rank Spell)|Baguette magique (sort de rang 4)|libre|
|[consumable-09-Sq4MIaXhb2zcCT6y.htm](equipment/consumable-09-Sq4MIaXhb2zcCT6y.htm)|Moon Blossom Tea|Thé de floraison lunaire|libre|
|[consumable-09-sYTvRLIlhfUd1Dm5.htm](equipment/consumable-09-sYTvRLIlhfUd1Dm5.htm)|Thunderbird Tuft (Greater)|Touffe d'oiseau-tonnerre supérieure|libre|
|[consumable-09-t4X6GDybqLmt7UkN.htm](equipment/consumable-09-t4X6GDybqLmt7UkN.htm)|Cheetah's Elixir (Greater)|Élixir du guépard supérieur|libre|
|[consumable-09-tEI0hGZyPWYwVhJU.htm](equipment/consumable-09-tEI0hGZyPWYwVhJU.htm)|Rebound Fulu|Fulu de rebondissement|libre|
|[consumable-09-tjLvRWklAylFhBHQ.htm](equipment/consumable-09-tjLvRWklAylFhBHQ.htm)|Scroll of 5th-rank Spell|Parchemin de sort de rang 5|libre|
|[consumable-09-tNs0FKSthYCM7ivx.htm](equipment/consumable-09-tNs0FKSthYCM7ivx.htm)|Golden Branding Iron (Greater)|Fer à marquer doré supérieur|libre|
|[consumable-09-TWSdWjp6dpodZkeV.htm](equipment/consumable-09-TWSdWjp6dpodZkeV.htm)|Spiritual Warhorn (Moderate)|Cor de guerre spirituel modéré|libre|
|[consumable-09-VeqgYabyGPd7kuSg.htm](equipment/consumable-09-VeqgYabyGPd7kuSg.htm)|Emerald Grasshopper (Greater)|Sauterelle d'émeraude supérieure|libre|
|[consumable-09-Vk5VzPB2fj3Ym1Ia.htm](equipment/consumable-09-Vk5VzPB2fj3Ym1Ia.htm)|Bralani Breath (Greater)|Souffle bralani supérieur|libre|
|[consumable-09-vL6AtFbcxbipGvtf.htm](equipment/consumable-09-vL6AtFbcxbipGvtf.htm)|Basilisk Eye|Oeil de Basilic|libre|
|[consumable-09-wDhqRxuXPQfyD0eX.htm](equipment/consumable-09-wDhqRxuXPQfyD0eX.htm)|Trident of Lightning|Trident de foudre|libre|
|[consumable-09-WnJwPbObxmTNvYJy.htm](equipment/consumable-09-WnJwPbObxmTNvYJy.htm)|Numbing Tonic (Moderate)|Tonique anesthésiant modéré|libre|
|[consumable-09-WSZl9S0Nui7pTMa8.htm](equipment/consumable-09-WSZl9S0Nui7pTMa8.htm)|Spellstrike Ammunition (Type IV)|Munition de frappe magique (Type IV)|libre|
|[consumable-09-XAuWVlZYGgCguzwz.htm](equipment/consumable-09-XAuWVlZYGgCguzwz.htm)|Feather Token (Whip)|Figurine merveilleuse fouet|libre|
|[consumable-09-XPIsejlpyvHvR0Ma.htm](equipment/consumable-09-XPIsejlpyvHvR0Ma.htm)|Storm Breath|Souffle tempête|libre|
|[consumable-09-xvJ2jzqipBNrwZ2w.htm](equipment/consumable-09-xvJ2jzqipBNrwZ2w.htm)|Sight-Theft Grit|Sable voleur de vision|libre|
|[consumable-09-Y0oi9tSJYgfEQIxt.htm](equipment/consumable-09-Y0oi9tSJYgfEQIxt.htm)|Enfilading Arrow|Flèche en enfilade|libre|
|[consumable-09-yaKQS5aG2i7IioyH.htm](equipment/consumable-09-yaKQS5aG2i7IioyH.htm)|Corpsecaller Round|Balle du faiseur de cadavre|libre|
|[consumable-09-YCFiwaS94TyfYtOe.htm](equipment/consumable-09-YCFiwaS94TyfYtOe.htm)|Transposition Ammunition|Munition de transposition|libre|
|[consumable-09-z8lwQLmqyhjcvibq.htm](equipment/consumable-09-z8lwQLmqyhjcvibq.htm)|Wooden Nickel|Nickel en bois|libre|
|[consumable-09-ZxkTbyq61JTQ7Zkt.htm](equipment/consumable-09-ZxkTbyq61JTQ7Zkt.htm)|Healer's Gel (Moderate)|Gel du guérisseur modéré|libre|
|[consumable-10-04giYigfDL5geu5f.htm](equipment/consumable-10-04giYigfDL5geu5f.htm)|Mistform Elixir (Greater)|Élixir de brumeforme supérieur|libre|
|[consumable-10-1PH7kb7ZVSrfQsgD.htm](equipment/consumable-10-1PH7kb7ZVSrfQsgD.htm)|Delve Scale|Écaille de plongée|libre|
|[consumable-10-27aqRraefLK7x7m3.htm](equipment/consumable-10-27aqRraefLK7x7m3.htm)|Potion of Grounding|Potion de mise à la terre|libre|
|[consumable-10-3IknuOpIV1OSNl98.htm](equipment/consumable-10-3IknuOpIV1OSNl98.htm)|Lucky Rabbit's Foot|Patte de lapin de Chance|libre|
|[consumable-10-3YnHsf98MAe3lcdH.htm](equipment/consumable-10-3YnHsf98MAe3lcdH.htm)|Unsullied Blood (Greater)|Sang non souillé supérieur|libre|
|[consumable-10-5ucWbzvEQhd2eSJk.htm](equipment/consumable-10-5ucWbzvEQhd2eSJk.htm)|Golden-Cased Bullet (Greater)|Étui de balle doré supérieur|libre|
|[consumable-10-66yY2SClvqj0lMFX.htm](equipment/consumable-10-66yY2SClvqj0lMFX.htm)|Conduit Shot (Moderate)|Tir conducteur modéré|libre|
|[consumable-10-7TT0XIb8ovLFOP2e.htm](equipment/consumable-10-7TT0XIb8ovLFOP2e.htm)|Honeyscent|Senteur de miel|libre|
|[consumable-10-98KlJPs0tPFq4qKv.htm](equipment/consumable-10-98KlJPs0tPFq4qKv.htm)|Overloaded Brain Grenade|Grenade de cerveau surchargé|libre|
|[consumable-10-9Brjb8WQMVCT0P2v.htm](equipment/consumable-10-9Brjb8WQMVCT0P2v.htm)|Weapon Shot (Moderate)|Tir d'arme modéré|libre|
|[consumable-10-9UGxCsNE778Y5rk0.htm](equipment/consumable-10-9UGxCsNE778Y5rk0.htm)|Flare Beacon (Greater)|Balise éclairante supérieure|libre|
|[consumable-10-a46f60lt7NEUvNEy.htm](equipment/consumable-10-a46f60lt7NEUvNEy.htm)|Potion of Annulment (Lesser)|Potion d'abrogation inférieure|libre|
|[consumable-10-AeL8qd4hqaNN5MxP.htm](equipment/consumable-10-AeL8qd4hqaNN5MxP.htm)|Spirit Bulb|Bulbe spirituel|libre|
|[consumable-10-Ap2Styg25sZMx3wn.htm](equipment/consumable-10-Ap2Styg25sZMx3wn.htm)|Mudrock Snare|Piège artisanal de boue solidifiée|libre|
|[consumable-10-BbhoGheRSwcfQ5z2.htm](equipment/consumable-10-BbhoGheRSwcfQ5z2.htm)|Feather Token (Swan Boat) (Ammunition)|Jeton figurine bâteau (Munition)|libre|
|[consumable-10-BiFzQ1MlxK7F7jkE.htm](equipment/consumable-10-BiFzQ1MlxK7F7jkE.htm)|Elemental Fragment|Fragment élémentaire|libre|
|[consumable-10-CnPEbCCfRE8MJmdz.htm](equipment/consumable-10-CnPEbCCfRE8MJmdz.htm)|Tyrant Ampoule|Ampoule de tyranno|libre|
|[consumable-10-D2iSFSgRq2jSov76.htm](equipment/consumable-10-D2iSFSgRq2jSov76.htm)|Rose of Loves Lost|Rose des amours perdus|libre|
|[consumable-10-dBevXop3G2P3PGjp.htm](equipment/consumable-10-dBevXop3G2P3PGjp.htm)|Vanishing Coin|Pièce d'invisibilité|libre|
|[consumable-10-DDsCScnV4QfWlfeF.htm](equipment/consumable-10-DDsCScnV4QfWlfeF.htm)|Vulture's Wing|Aile de vautour|libre|
|[consumable-10-dEU38O24zT6JCfEZ.htm](equipment/consumable-10-dEU38O24zT6JCfEZ.htm)|Nevercold|Jamais froid|libre|
|[consumable-10-DFxyS72Uh2vqT2MZ.htm](equipment/consumable-10-DFxyS72Uh2vqT2MZ.htm)|Tentacle Potion (Moderate)|Potion tentaculaire modérée|libre|
|[consumable-10-dzfmP3WsA15puenS.htm](equipment/consumable-10-dzfmP3WsA15puenS.htm)|Potion of Resistance (Moderate)|Potion de résistance modérée|libre|
|[consumable-10-EOk6kNsNJOhoSbRZ.htm](equipment/consumable-10-EOk6kNsNJOhoSbRZ.htm)|Effervescent Decoction|Décoction effervescente|libre|
|[consumable-10-evdDpgFpiFZt9UyA.htm](equipment/consumable-10-evdDpgFpiFZt9UyA.htm)|Potion of Cold Resistance (Moderate)|Potion de résistance au froid modérée|libre|
|[consumable-10-ewzJzyJ4Vo9lZKvp.htm](equipment/consumable-10-ewzJzyJ4Vo9lZKvp.htm)|Iron Medallion|Médaillon de fer|libre|
|[consumable-10-eZIOsvyFRkqU43f8.htm](equipment/consumable-10-eZIOsvyFRkqU43f8.htm)|Baleblood Draft|Gorgée de sang étrange|libre|
|[consumable-10-FgZCLBhOOhhXDdvI.htm](equipment/consumable-10-FgZCLBhOOhhXDdvI.htm)|Conrasu Coin (Bythos)|Pièce conrasu (Bythos)|libre|
|[consumable-10-GIsGVMzTw7JfWweP.htm](equipment/consumable-10-GIsGVMzTw7JfWweP.htm)|Bottled Screams|Cris en bouteille|libre|
|[consumable-10-H2Xcd1nzesEeY96V.htm](equipment/consumable-10-H2Xcd1nzesEeY96V.htm)|Stepping Stone Shot (Greater)|Tir pierre de gué supérieur|libre|
|[consumable-10-HIPkTfJLlLu4bWE3.htm](equipment/consumable-10-HIPkTfJLlLu4bWE3.htm)|Soothing Tonic (Greater)|Tonique apaisant supérieur|libre|
|[consumable-10-HTH445aey1x9RoeG.htm](equipment/consumable-10-HTH445aey1x9RoeG.htm)|Shockguard Coil|Bobine de choc électrique|libre|
|[consumable-10-HwWrR4CG3Fe7QZrO.htm](equipment/consumable-10-HwWrR4CG3Fe7QZrO.htm)|Oozepick (Greater)|Crochetvase supérieur|libre|
|[consumable-10-j9G3tnrKQ1N1dLzN.htm](equipment/consumable-10-j9G3tnrKQ1N1dLzN.htm)|Snagging Hook Snare|Piège artisanal de crochets métalliques|libre|
|[consumable-10-JPNKZPtsdNpoLsBT.htm](equipment/consumable-10-JPNKZPtsdNpoLsBT.htm)|Camp Shroud (Moderate)|Linceul de camp modéré|libre|
|[consumable-10-kbjspHV7kwKKULmd.htm](equipment/consumable-10-kbjspHV7kwKKULmd.htm)|Potion of Sonic Resistance (Moderate)|Potion de résistance au son modérée|libre|
|[consumable-10-kksRfBsWEPYt8jpN.htm](equipment/consumable-10-kksRfBsWEPYt8jpN.htm)|Ruby Capacitor|Condensateur de rubis|libre|
|[consumable-10-l3itUlHLuWP6qlA0.htm](equipment/consumable-10-l3itUlHLuWP6qlA0.htm)|Life Shot (Moderate)|Tir de vie modéré|libre|
|[consumable-10-LDiF0GRYAiW2mool.htm](equipment/consumable-10-LDiF0GRYAiW2mool.htm)|Star of Cynosure|Étoile de Cynosure|libre|
|[consumable-10-m99u0zXVbyPdK8Mf.htm](equipment/consumable-10-m99u0zXVbyPdK8Mf.htm)|Potion of Fire Resistance (Moderate)|Potion de résistance au feu modérée|libre|
|[consumable-10-MBL8rOfvokYmUvwc.htm](equipment/consumable-10-MBL8rOfvokYmUvwc.htm)|Olfactory Obfuscator (Greater)|Embrouilleur olfactif supérieur|libre|
|[consumable-10-MHoghQhxbvZRyyaB.htm](equipment/consumable-10-MHoghQhxbvZRyyaB.htm)|Potion of Electricity Resistance (Moderate)|Potion de résistance à l'électricité modérée|libre|
|[consumable-10-MvMa010Je10GN5dx.htm](equipment/consumable-10-MvMa010Je10GN5dx.htm)|Azure Lily Pollen|Pollen de lys azur|libre|
|[consumable-10-MZ5fWDqpKEMv2d1Q.htm](equipment/consumable-10-MZ5fWDqpKEMv2d1Q.htm)|Mutagenic Renovator|Rénovateur mutagénique|libre|
|[consumable-10-NQvLv4qcTfK21y3H.htm](equipment/consumable-10-NQvLv4qcTfK21y3H.htm)|Spirit-Sealing Fulu (Greater)|Fulu scelle-esprit supérieur|libre|
|[consumable-10-NRoA1HpA3ElPGBEQ.htm](equipment/consumable-10-NRoA1HpA3ElPGBEQ.htm)|Wolfsbane|Aconit|libre|
|[consumable-10-nvjEr2dlbtMV0q3d.htm](equipment/consumable-10-nvjEr2dlbtMV0q3d.htm)|Deathless Light|Lumière sans mort|libre|
|[consumable-10-NZ1z3ee75XfctFwM.htm](equipment/consumable-10-NZ1z3ee75XfctFwM.htm)|Winterstep Elixir (Moderate)|Élixir de démarche hivernale modérée|libre|
|[consumable-10-oCuwJ9IUDAuzsUwa.htm](equipment/consumable-10-oCuwJ9IUDAuzsUwa.htm)|Bravo's Brew (Moderate)|Breuvage de bravoure modéré|libre|
|[consumable-10-ORDaAzMZLrf6Hn8A.htm](equipment/consumable-10-ORDaAzMZLrf6Hn8A.htm)|Antiplague (Greater)|Antimaladie supérieur|libre|
|[consumable-10-PkrpCUXdV3I1boRQ.htm](equipment/consumable-10-PkrpCUXdV3I1boRQ.htm)|Golden Silencer (Greater)|Silencieux doré supérieur|libre|
|[consumable-10-R8FHP4u80ee7PgWa.htm](equipment/consumable-10-R8FHP4u80ee7PgWa.htm)|Magnetic Suit (Greater)|Tenue magnétique supérieure|libre|
|[consumable-10-R9EZOG8Zeeo2RnwO.htm](equipment/consumable-10-R9EZOG8Zeeo2RnwO.htm)|Sniper's Bead (Greater)|Perle du tireur d'élite supérieure|libre|
|[consumable-10-RH1fNsslZ1wdeqy2.htm](equipment/consumable-10-RH1fNsslZ1wdeqy2.htm)|Fearweed|Herbe de terreur|libre|
|[consumable-10-rOicLo9vsSWCUSLu.htm](equipment/consumable-10-rOicLo9vsSWCUSLu.htm)|Raining Knives Snare|Piège artisanal pluie de couteaux|libre|
|[consumable-10-Sg9KRxEaRBCRCUhp.htm](equipment/consumable-10-Sg9KRxEaRBCRCUhp.htm)|Depth Charge III|Charge de profondeur III|libre|
|[consumable-10-sQM4J9Ek5GThLsjf.htm](equipment/consumable-10-sQM4J9Ek5GThLsjf.htm)|Binding Snare|Piège artisanal liant|libre|
|[consumable-10-SsWxgxols1Q1qPVf.htm](equipment/consumable-10-SsWxgxols1Q1qPVf.htm)|Firestarter Pellets (Greater)|Boulettes d'ignition supérieures|libre|
|[consumable-10-TgZdamADHuaKyriW.htm](equipment/consumable-10-TgZdamADHuaKyriW.htm)|Elemental Gem|Gemme élémentaire|libre|
|[consumable-10-UlZ6s3Wox11ApisX.htm](equipment/consumable-10-UlZ6s3Wox11ApisX.htm)|Immovable Potion|Potion inamovible|libre|
|[consumable-10-UnDjPxFOs6bldlcM.htm](equipment/consumable-10-UnDjPxFOs6bldlcM.htm)|Mummified Bat|Chauve-souris mommifiée|libre|
|[consumable-10-uswFgspZL8QrHecL.htm](equipment/consumable-10-uswFgspZL8QrHecL.htm)|Barricade Stone (Cylinder)|Pierre de barricade (Cylindre)|libre|
|[consumable-10-UYCQoe8sKZgbKYHg.htm](equipment/consumable-10-UYCQoe8sKZgbKYHg.htm)|Follypops|Follypops|libre|
|[consumable-10-VcEd9Bh4q5vQfpLv.htm](equipment/consumable-10-VcEd9Bh4q5vQfpLv.htm)|Lastwall Soup (Improved)|Soupe de Dernier-Rempart améliorée|libre|
|[consumable-10-vHAetFCiVDxAPdPD.htm](equipment/consumable-10-vHAetFCiVDxAPdPD.htm)|Toxic Effluence|Effluve toxique|libre|
|[consumable-10-W9j0jc5Ekvweo7MY.htm](equipment/consumable-10-W9j0jc5Ekvweo7MY.htm)|Burning Badger Guts Snare|Piège artisanal intestin brûlant de blaireau|libre|
|[consumable-10-Wjkw0lEUOhypYvzo.htm](equipment/consumable-10-Wjkw0lEUOhypYvzo.htm)|Shadow Essence|Distillation d'ombre|libre|
|[consumable-10-xK3rWA6HtaJvKSuC.htm](equipment/consumable-10-xK3rWA6HtaJvKSuC.htm)|Retrieval Prism (Greater)|Prisme d'extraction supérieure|libre|
|[consumable-10-xWCdsGSwLLTeX6tQ.htm](equipment/consumable-10-xWCdsGSwLLTeX6tQ.htm)|Eagle Eye Elixir (Greater)|Élixir d'oeil de faucon supérieur|libre|
|[consumable-10-XWkeL34yJK6t5qUE.htm](equipment/consumable-10-XWkeL34yJK6t5qUE.htm)|Antidote (Greater)|Antidote supérieur|libre|
|[consumable-10-yG6Za8FaG3hpXUGh.htm](equipment/consumable-10-yG6Za8FaG3hpXUGh.htm)|Potion of Acid Resistance (Moderate)|Potion de résistance à l'acide modérée|libre|
|[consumable-10-znITT3WIS59TYtCu.htm](equipment/consumable-10-znITT3WIS59TYtCu.htm)|Anointing Oil (Greater)|Huile de dernière onction supérieure|libre|
|[consumable-10-ZOOrK4yEW6rFpPJ1.htm](equipment/consumable-10-ZOOrK4yEW6rFpPJ1.htm)|Addiction Suppressant (Greater)|Suppresseur de dépendance supérieur|libre|
|[consumable-11-2wV9SHAWbC4rUlcc.htm](equipment/consumable-11-2wV9SHAWbC4rUlcc.htm)|Silver Crescent (Moderate)|Croissant argenté modéré|libre|
|[consumable-11-2x8F5s3PyKzYsRrZ.htm](equipment/consumable-11-2x8F5s3PyKzYsRrZ.htm)|Bloodhound Mask (Greater)|Masque du limier supérieur|libre|
|[consumable-11-3kELQauVoYunyFcd.htm](equipment/consumable-11-3kELQauVoYunyFcd.htm)|Life-Boosting Oil (Greater)|Huile stimulatrice de vie supérieure|libre|
|[consumable-11-3TxupnJlz7qXfdIr.htm](equipment/consumable-11-3TxupnJlz7qXfdIr.htm)|Oil of Ownership (Greater)|Huile de propriété supérieure|libre|
|[consumable-11-4hJDqukTYPMZv2vg.htm](equipment/consumable-11-4hJDqukTYPMZv2vg.htm)|Soothing Powder (Greater)|Poudre apaisante supérieure|libre|
|[consumable-11-4sGIy77COooxhQuC.htm](equipment/consumable-11-4sGIy77COooxhQuC.htm)|Scroll of 6th-rank Spell|Parchemin de sort de rang 6|libre|
|[consumable-11-4upzgnkxdGn2Fy9c.htm](equipment/consumable-11-4upzgnkxdGn2Fy9c.htm)|Lion Claw|Griffe de lion|libre|
|[consumable-11-5BF7zMnrPYzyigCs.htm](equipment/consumable-11-5BF7zMnrPYzyigCs.htm)|Magic Wand (5th-Rank Spell)|Baguette magique (sort de rang 5)|libre|
|[consumable-11-5Lode4gkHo4QUZbQ.htm](equipment/consumable-11-5Lode4gkHo4QUZbQ.htm)|Resonating Ammunition (Bolt)|Munition assourdissante (carreau)|libre|
|[consumable-11-6CnuU7AhzzAMGzAM.htm](equipment/consumable-11-6CnuU7AhzzAMGzAM.htm)|Pyronite|Pyronite|libre|
|[consumable-11-7PhPuAFP4mQz9jDr.htm](equipment/consumable-11-7PhPuAFP4mQz9jDr.htm)|Thousand-Pains Fulu (Needle)|Fulu des mille douleurs (Aiguille)|libre|
|[consumable-11-8GEsRsQAdDljRT1s.htm](equipment/consumable-11-8GEsRsQAdDljRT1s.htm)|Hunger Oil|Huile affamée|libre|
|[consumable-11-9kSITyK5yPTgTaQO.htm](equipment/consumable-11-9kSITyK5yPTgTaQO.htm)|Juxtaposition Ammunition|Munition de juxtaposition|libre|
|[consumable-11-9X77WeRsr543QKfz.htm](equipment/consumable-11-9X77WeRsr543QKfz.htm)|Mindlock Shot|Tir bloquesprit|libre|
|[consumable-11-aQMmbIF0I8zMyzN4.htm](equipment/consumable-11-aQMmbIF0I8zMyzN4.htm)|Whirlwind Vial|Fiole tourbillon|libre|
|[consumable-11-ARY8jRkg8aUJ6Zdv.htm](equipment/consumable-11-ARY8jRkg8aUJ6Zdv.htm)|Bonmuan Swapping Stone (Lesser)|Pierre d'échange Bonmuan inférieure|libre|
|[consumable-11-BB9ErvPViT4TtiYp.htm](equipment/consumable-11-BB9ErvPViT4TtiYp.htm)|Theatrical Mutagen (Greater)|Mutagène théatral supérieur|libre|
|[consumable-11-bVsNvqhfvDKGbRhd.htm](equipment/consumable-11-bVsNvqhfvDKGbRhd.htm)|Fundamental Oil|Huile fondamentale|libre|
|[consumable-11-dlaDnZFsy4JCYmEO.htm](equipment/consumable-11-dlaDnZFsy4JCYmEO.htm)|Toadskin Salve (Major)|Baume Peau de crapaud majeur|libre|
|[consumable-11-eQDTsetadI8u8Kc0.htm](equipment/consumable-11-eQDTsetadI8u8Kc0.htm)|Quicksilver Mutagen (Greater)|Mutagène de vif-argent supérieur|officielle|
|[consumable-11-F0x5a1MI3k9Du9j3.htm](equipment/consumable-11-F0x5a1MI3k9Du9j3.htm)|Blisterwort|Moût vésiculaire|libre|
|[consumable-11-fDLABfn3nY1R670j.htm](equipment/consumable-11-fDLABfn3nY1R670j.htm)|Chromatic Jellyfish Oil (Moderate)|Huile de méduse chromatique modérée|libre|
|[consumable-11-FHsRKAdiLlxfSOqy.htm](equipment/consumable-11-FHsRKAdiLlxfSOqy.htm)|Big Rock Bullet (Greater)|Bille gros rocher supérieure|libre|
|[consumable-11-fTpJc3XoC2gzhuSN.htm](equipment/consumable-11-fTpJc3XoC2gzhuSN.htm)|Reverberating Stone|Pierre réverbérante|libre|
|[consumable-11-gDheph8YteBtnyKp.htm](equipment/consumable-11-gDheph8YteBtnyKp.htm)|Bestial Mutagen (Greater)|Mutagène bestial supérieur|officielle|
|[consumable-11-haDkOWAZClCQA6Yq.htm](equipment/consumable-11-haDkOWAZClCQA6Yq.htm)|Clinging Bubbles (Greater)|Bulles accrochées supérieures|libre|
|[consumable-11-idZvtwLg0E3AWnur.htm](equipment/consumable-11-idZvtwLg0E3AWnur.htm)|Captivating Score|Partition captivante|libre|
|[consumable-11-ieVRS2BjiWqauly6.htm](equipment/consumable-11-ieVRS2BjiWqauly6.htm)|Sixfingers Elixir (Moderate)|Élixir Six-doigts modéré|libre|
|[consumable-11-ioCGarugWKBHqE5L.htm](equipment/consumable-11-ioCGarugWKBHqE5L.htm)|Blending Brooch|Broche estompante|libre|
|[consumable-11-j7VThCkenoE4giZt.htm](equipment/consumable-11-j7VThCkenoE4giZt.htm)|Cryomister (Greater)|Cryobrumisateur supérieur|libre|
|[consumable-11-JMvp1ca4IhLy4cGj.htm](equipment/consumable-11-JMvp1ca4IhLy4cGj.htm)|Resonating Ammunition (Arrow)|Munition assourdissante (flèche)|libre|
|[consumable-11-knQ5MnAIvKdXSpcQ.htm](equipment/consumable-11-knQ5MnAIvKdXSpcQ.htm)|Talespinner's Lyre|Lyre du tisseur de contes|libre|
|[consumable-11-lbr6acClmnMmB7QZ.htm](equipment/consumable-11-lbr6acClmnMmB7QZ.htm)|Grim Trophy (Greater)|Sinistre trophée supérieur|libre|
|[consumable-11-LNzJ2WwTg58g7vam.htm](equipment/consumable-11-LNzJ2WwTg58g7vam.htm)|Blood Booster (Moderate)|Activateur sanguin modéré|libre|
|[consumable-11-ne00KCUcrmRT3PM9.htm](equipment/consumable-11-ne00KCUcrmRT3PM9.htm)|Anathema Fulu|Fulu anathème|libre|
|[consumable-11-oaGUNWmrqtKhy1HP.htm](equipment/consumable-11-oaGUNWmrqtKhy1HP.htm)|War Blood Mutagen (Greater)|Mutagène de sang guerrier supérieur|libre|
|[consumable-11-OMJgFmy3jut79Iaj.htm](equipment/consumable-11-OMJgFmy3jut79Iaj.htm)|Potion of Swimming (Greater)|Potion de nage supérieure|libre|
|[consumable-11-pAldqSrzPotjYQe6.htm](equipment/consumable-11-pAldqSrzPotjYQe6.htm)|Demolition Fulu (Moderate)|Fulu de démolition modéré|libre|
|[consumable-11-q2TpCwdlLGdcasKD.htm](equipment/consumable-11-q2TpCwdlLGdcasKD.htm)|Elemental Ammunition (Greater)|Munition élémentaire supérieure|libre|
|[consumable-11-qoyelABKtk9DRSEk.htm](equipment/consumable-11-qoyelABKtk9DRSEk.htm)|Prey Mutagen (Greater)|Mutagène proie supérieur|libre|
|[consumable-11-QSofPEx2sjztUNao.htm](equipment/consumable-11-QSofPEx2sjztUNao.htm)|Golden Spur|Éperon doré|libre|
|[consumable-11-qZvCxp4noVqYGkT0.htm](equipment/consumable-11-qZvCxp4noVqYGkT0.htm)|Bane Ammunition (Greater)|Munition de proie favorite supérieure|libre|
|[consumable-11-RsAoEbp28Za4hlLH.htm](equipment/consumable-11-RsAoEbp28Za4hlLH.htm)|Choker-Arm Mutagen (Greater)|Mutagène d'assouplissement supérieur|libre|
|[consumable-11-se2g3LNsmQSsRv9w.htm](equipment/consumable-11-se2g3LNsmQSsRv9w.htm)|Sanguine Mutagen (Greater)|Mutagène sanguin supérieur|libre|
|[consumable-11-SjbenbdzV4rgGxZP.htm](equipment/consumable-11-SjbenbdzV4rgGxZP.htm)|Skeptic's Elixir (Greater)|Élixir de l'incrédule supérieur|libre|
|[consumable-11-ThX0ntpTqonGqguT.htm](equipment/consumable-11-ThX0ntpTqonGqguT.htm)|Potion of Disguise (Greater)|Potion de déguisement supérieure|libre|
|[consumable-11-uggpIk6vguWFXVli.htm](equipment/consumable-11-uggpIk6vguWFXVli.htm)|Silvertongue Mutagen (Greater)|Mutagène de langue dorée supérieur|officielle|
|[consumable-11-UOt1kd4ccF9sX9bt.htm](equipment/consumable-11-UOt1kd4ccF9sX9bt.htm)|Disrupting Oil (Greater)|Huile vitalisante supérieure|libre|
|[consumable-11-VTuKWBEGuBS3HqF4.htm](equipment/consumable-11-VTuKWBEGuBS3HqF4.htm)|Serpent Oil (Major)|Huile de serpent majeure|libre|
|[consumable-11-WMdHOfjjBtJAEvqU.htm](equipment/consumable-11-WMdHOfjjBtJAEvqU.htm)|Dancing Lamentation|Complainte dansante|libre|
|[consumable-11-WuzLBK78DgIt8SsN.htm](equipment/consumable-11-WuzLBK78DgIt8SsN.htm)|Drakeheart Mutagen (Greater)|Mutagène de coeur de drake supérieur|libre|
|[consumable-11-X1KGaXLDUxNJ1XXB.htm](equipment/consumable-11-X1KGaXLDUxNJ1XXB.htm)|Frozen Lava of Mhar Massif|Lave gelée du Massif de Mhar|libre|
|[consumable-11-xanr7nay2UQeR4Ec.htm](equipment/consumable-11-xanr7nay2UQeR4Ec.htm)|Deadweight Mutagen (Greater)|Mutagène de poids-mort supérieur|libre|
|[consumable-11-XbyPBiZnoKDK9vbH.htm](equipment/consumable-11-XbyPBiZnoKDK9vbH.htm)|Ghostshot Wrapping|Bandellette de tir fantôme|libre|
|[consumable-11-xBZniAmkWcLfQ48d.htm](equipment/consumable-11-xBZniAmkWcLfQ48d.htm)|Torrent Spellgun (Greater)|Lancesort torrent supérieur|libre|
|[consumable-11-xKME7GEjssNkyTgt.htm](equipment/consumable-11-xKME7GEjssNkyTgt.htm)|Blister Ammunition (Moderate)|Munition à balles modérée|libre|
|[consumable-11-xVtyaEnYZ1aJT4ki.htm](equipment/consumable-11-xVtyaEnYZ1aJT4ki.htm)|Energy Mutagen (Greater)|Mutagène d'énergie supérieur|libre|
|[consumable-11-XyrFcBF5ygIFFGes.htm](equipment/consumable-11-XyrFcBF5ygIFFGes.htm)|Spellstrike Ammunition (Type V)|Munition de frappe magique (Type V)|libre|
|[consumable-11-Yej7lnnDYDZybGqo.htm](equipment/consumable-11-Yej7lnnDYDZybGqo.htm)|Serene Mutagen (Greater)|Mutagène de sérénité supérieur|officielle|
|[consumable-11-ZGojRKG1yYiVWemR.htm](equipment/consumable-11-ZGojRKG1yYiVWemR.htm)|Cognitive Mutagen (Greater)|Mutagène cognitif supérieur|officielle|
|[consumable-12-16INAWEN5mkuGTga.htm](equipment/consumable-12-16INAWEN5mkuGTga.htm)|Black Tendril Shot (Moderate)|Tir de vrilles noires modéré|libre|
|[consumable-12-1iIb8vL2WQHOqN46.htm](equipment/consumable-12-1iIb8vL2WQHOqN46.htm)|Elemental Fragment (Greater)|Fragment élémentaire supérieur|libre|
|[consumable-12-2hc1EEcb3pfr7Hac.htm](equipment/consumable-12-2hc1EEcb3pfr7Hac.htm)|Sea Touch Elixir (Moderate)|Élixir de caresse marine modéré|libre|
|[consumable-12-2prxM8Q0F4sdSwPx.htm](equipment/consumable-12-2prxM8Q0F4sdSwPx.htm)|Winter Wolf Elixir (Moderate)|Élixir du loup arctique modéré|officielle|
|[consumable-12-2VC7FLItVrFWNe4L.htm](equipment/consumable-12-2VC7FLItVrFWNe4L.htm)|Energy Breath Potion (Acid, Moderate)|Potion de souffle d'énergie corrosive modérée|libre|
|[consumable-12-4F87PHMIzLHOUABL.htm](equipment/consumable-12-4F87PHMIzLHOUABL.htm)|Stonethroat Ammunition|Munition coince-geule|libre|
|[consumable-12-4XpPgF5smMjCCHRF.htm](equipment/consumable-12-4XpPgF5smMjCCHRF.htm)|Blight Breath|Souffle fléau|libre|
|[consumable-12-5RJivM5SShjbqFf0.htm](equipment/consumable-12-5RJivM5SShjbqFf0.htm)|Magnetic Shot (Moderate)|Tir magnétique modéré|libre|
|[consumable-12-7tqSRAuOgqt3CFhM.htm](equipment/consumable-12-7tqSRAuOgqt3CFhM.htm)|Astringent Venom|Venin astringent|libre|
|[consumable-12-82f7CTK0HxvitMLP.htm](equipment/consumable-12-82f7CTK0HxvitMLP.htm)|Dragonfly Potion|Potion libellule|libre|
|[consumable-12-8e37h6HBr4ZyBSGt.htm](equipment/consumable-12-8e37h6HBr4ZyBSGt.htm)|Applereed Mutagen (Moderate)|Mutagène pousse-roseau modéré|libre|
|[consumable-12-8uyRlAkWdEyfOziq.htm](equipment/consumable-12-8uyRlAkWdEyfOziq.htm)|Mage Bane|Fléau du mage|libre|
|[consumable-12-8vN6vm2fKB4rE86U.htm](equipment/consumable-12-8vN6vm2fKB4rE86U.htm)|Elysian Dew|Rosée Élyséenne|libre|
|[consumable-12-8XFicWnOf9VwSklQ.htm](equipment/consumable-12-8XFicWnOf9VwSklQ.htm)|Animal Nip (Major)|Herbe aux animaux majeure|libre|
|[consumable-12-9Kk6P7idLGRhZJ2q.htm](equipment/consumable-12-9Kk6P7idLGRhZJ2q.htm)|Bleeding Spines Snare|Piège artisanal à pointes barbelées|officielle|
|[consumable-12-9mXjEGJaESesndWh.htm](equipment/consumable-12-9mXjEGJaESesndWh.htm)|Vaccine (Greater)|Vaccin supérieur|libre|
|[consumable-12-a2XaerM1KkPyLIPM.htm](equipment/consumable-12-a2XaerM1KkPyLIPM.htm)|Stunning Snare|Piège artisanal étourdissant|libre|
|[consumable-12-A3rdTH2lpqQMoX6N.htm](equipment/consumable-12-A3rdTH2lpqQMoX6N.htm)|Gearbinder Oil (Moderate)|Huile grippe engrenages modérée|libre|
|[consumable-12-Bn1o5foPq1kqZKf0.htm](equipment/consumable-12-Bn1o5foPq1kqZKf0.htm)|Red Dragon's Breath Potion (Adult)|Potion de souffle du dragon rouge adulte|officielle|
|[consumable-12-bRqczrlQDaeUvg6b.htm](equipment/consumable-12-bRqczrlQDaeUvg6b.htm)|Plasma Hype|Adrénaline plasmatique|libre|
|[consumable-12-c6HS4K7omXsmZiZT.htm](equipment/consumable-12-c6HS4K7omXsmZiZT.htm)|Exsanguinating Ammunition (Major)|Munition exsangue majeur|libre|
|[consumable-12-CkrsPua4g1IlcBO0.htm](equipment/consumable-12-CkrsPua4g1IlcBO0.htm)|Dragonscale Cameo|Camée en écaille de dragon|libre|
|[consumable-12-CReWZjAd3szYQ6Mx.htm](equipment/consumable-12-CReWZjAd3szYQ6Mx.htm)|Vermin Repellent Agent (Greater)|Agent répulsif de vermine supérieur|libre|
|[consumable-12-CtePdUJWKgjwpatZ.htm](equipment/consumable-12-CtePdUJWKgjwpatZ.htm)|Faerie Dragon Liqueur (Adult)|Liqueur de dragon féerique adulte|libre|
|[consumable-12-Do8vjuUBOslgPtyw.htm](equipment/consumable-12-Do8vjuUBOslgPtyw.htm)|Balisse Feather|Plume de balisse|libre|
|[consumable-12-e2Von0aDHB03Iz3f.htm](equipment/consumable-12-e2Von0aDHB03Iz3f.htm)|Explosive Mine (Greater)|Mine explosive supérieure|libre|
|[consumable-12-E8pKcXFDiCWz68Db.htm](equipment/consumable-12-E8pKcXFDiCWz68Db.htm)|Chameleon Suit (Major)|Tenue de caméléon majeure|libre|
|[consumable-12-EEJiqzU89Ofk7dr6.htm](equipment/consumable-12-EEJiqzU89Ofk7dr6.htm)|Salamander Elixir (Moderate)|Élixir de la salamandre modéré|officielle|
|[consumable-12-ENrhapQe2Gw8HdwR.htm](equipment/consumable-12-ENrhapQe2Gw8HdwR.htm)|Meteor Shot (Greater)|Balle météore supérieure|libre|
|[consumable-12-eV0owvv0lYQph0nn.htm](equipment/consumable-12-eV0owvv0lYQph0nn.htm)|Green Dragon's Breath Potion (Adult)|Potion de souffle du dragon vert adulte|officielle|
|[consumable-12-FmfgfAW5MZ1mTH4T.htm](equipment/consumable-12-FmfgfAW5MZ1mTH4T.htm)|Captivating Bauble|Babiole convaincante|libre|
|[consumable-12-FW1Ml15eeMItIxS2.htm](equipment/consumable-12-FW1Ml15eeMItIxS2.htm)|Marvelous Pigment|Pigments merveilleux|libre|
|[consumable-12-FZ6SMwiei6NnEp3e.htm](equipment/consumable-12-FZ6SMwiei6NnEp3e.htm)|Stone Body Mutagen (Moderate)|Mutagène corps-de-pierre modéré|libre|
|[consumable-12-GDbDpe7W7vOEwnEr.htm](equipment/consumable-12-GDbDpe7W7vOEwnEr.htm)|Poison Fizz (Moderate)|Poison pétillant modéré|libre|
|[consumable-12-hMUYvp0neF0LAFqc.htm](equipment/consumable-12-hMUYvp0neF0LAFqc.htm)|Potion of Fire Retaliation (Greater)|Potion de riposte enflammée supérieure|libre|
|[consumable-12-jiDzJKmdK2BAOh1x.htm](equipment/consumable-12-jiDzJKmdK2BAOh1x.htm)|Banquet of Hungry Ghosts|Banquet des fantômes affamés|libre|
|[consumable-12-jjfxEYViPr4FDeMH.htm](equipment/consumable-12-jjfxEYViPr4FDeMH.htm)|Energy Breath Potion (Sonic, Moderate)|Potion de souffle d'énergie sonique modérée|libre|
|[consumable-12-jNdAX4hBp0pzWalL.htm](equipment/consumable-12-jNdAX4hBp0pzWalL.htm)|Fire and Iceberg (Greater)|Feu et iceberg supérieur|libre|
|[consumable-12-jVmTtUeNxDZMP5dU.htm](equipment/consumable-12-jVmTtUeNxDZMP5dU.htm)|Potion of Acid Retaliation (Greater)|Potion de riposte acide supérieure|libre|
|[consumable-12-KQfx4Ae8c3iMlwXM.htm](equipment/consumable-12-KQfx4Ae8c3iMlwXM.htm)|Sense-Dulling Hood (Greater)|Capuche émousse-sens supérieure|libre|
|[consumable-12-KrtZmC3qfZIGXu76.htm](equipment/consumable-12-KrtZmC3qfZIGXu76.htm)|Iron Equalizer|Fer d'équilibrage|officielle|
|[consumable-12-LhlsZogLBPLx5Tcf.htm](equipment/consumable-12-LhlsZogLBPLx5Tcf.htm)|Shrieking Skull|Crâne hurlant|libre|
|[consumable-12-mR9RD9S08jIX1IPm.htm](equipment/consumable-12-mR9RD9S08jIX1IPm.htm)|Universal Solvent (Greater)|Solvant universel supérieur|libre|
|[consumable-12-mWXaROc9ytjdGVVP.htm](equipment/consumable-12-mWXaROc9ytjdGVVP.htm)|Scything Blade Snare|Piège artisanal à faux|libre|
|[consumable-12-NqNwLeqto2isVWNL.htm](equipment/consumable-12-NqNwLeqto2isVWNL.htm)|Energy Breath Potion (Fire, Moderate)|Potion de souffle d'énergie de feu modérée|libre|
|[consumable-12-nsNFSjNZNVGEGTK4.htm](equipment/consumable-12-nsNFSjNZNVGEGTK4.htm)|Depth Charge IV|Charge de profondeur IV|libre|
|[consumable-12-OjNaLZI13rWlyqmM.htm](equipment/consumable-12-OjNaLZI13rWlyqmM.htm)|Animal Repellent (Greater)|Répulsif animal supérieur|libre|
|[consumable-12-PLuJEzJWaWamW0Fc.htm](equipment/consumable-12-PLuJEzJWaWamW0Fc.htm)|Oil of Potency (Greater)|Huile de puissance supérieure|libre|
|[consumable-12-Pp4JyxzRSqpOiQyL.htm](equipment/consumable-12-Pp4JyxzRSqpOiQyL.htm)|Weapon Shot (Greater)|Tir d'arme supérieur|libre|
|[consumable-12-QsYfjRMBz48H6UAp.htm](equipment/consumable-12-QsYfjRMBz48H6UAp.htm)|Potion of Electricity Retaliation (Greater)|Potion de riposte électrique supérieure|libre|
|[consumable-12-RhOx5wenvlljzOku.htm](equipment/consumable-12-RhOx5wenvlljzOku.htm)|Oil of Unlife (Greater)|Huile de Non-vie supérieure|libre|
|[consumable-12-rOQlcAV4m8Zaebue.htm](equipment/consumable-12-rOQlcAV4m8Zaebue.htm)|Brass Dragon's Breath Potion (Adult)|Potion de souffle du dragon d'airain adulte|officielle|
|[consumable-12-SEToMRNTXAVVRwyd.htm](equipment/consumable-12-SEToMRNTXAVVRwyd.htm)|Ooze Ammunition (Greater)|Munition de vase supérieure|libre|
|[consumable-12-sMrOHuFHjtLDOzp4.htm](equipment/consumable-12-sMrOHuFHjtLDOzp4.htm)|Healing Vapor (Greater)|Vapeur de guérison supérieure|libre|
|[consumable-12-t9wFCiFkCRcSLtaG.htm](equipment/consumable-12-t9wFCiFkCRcSLtaG.htm)|Silver Dragon's Breath Potion (Adult)|Potion de souffle du dragon d'argent adulte|officielle|
|[consumable-12-TmXTijJIpOxLJLXb.htm](equipment/consumable-12-TmXTijJIpOxLJLXb.htm)|Thrice-Fried Mudwings|Ailes de boue frites|libre|
|[consumable-12-Tqtcy25OCiNlWqF6.htm](equipment/consumable-12-Tqtcy25OCiNlWqF6.htm)|Clubhead Poison|Poison têtemassue|libre|
|[consumable-12-TSrbwHW8zm0mhkwb.htm](equipment/consumable-12-TSrbwHW8zm0mhkwb.htm)|Healing Potion (Greater)|Potion de guérison supérieure|libre|
|[consumable-12-tTrJnXJYt15arH6G.htm](equipment/consumable-12-tTrJnXJYt15arH6G.htm)|Alloy Orb (Exquisite Standard-Grade)|Orbe d'alliage raffiné de qualité standard|libre|
|[consumable-12-tW2rmGSanKQlUkiU.htm](equipment/consumable-12-tW2rmGSanKQlUkiU.htm)|Potion of Cold Retaliation (Greater)|Potion de riposte glaciale supérieure|libre|
|[consumable-12-tZiCnERZB6ww4eBu.htm](equipment/consumable-12-tZiCnERZB6ww4eBu.htm)|Aged Assassin Vine Wine|Vin de liane meurtrière hors d'âge|libre|
|[consumable-12-ve73Sb8MwLY9GDBD.htm](equipment/consumable-12-ve73Sb8MwLY9GDBD.htm)|Implosion Dust (Moderate)|Poussière d'implosion modérée|libre|
|[consumable-12-vXLZfi5iCSKb96tW.htm](equipment/consumable-12-vXLZfi5iCSKb96tW.htm)|Bronze Dragon's Breath Potion (Adult)|Potion de souffle du dragon de bronze adulte|officielle|
|[consumable-12-vzP0pLxes9bn0zV8.htm](equipment/consumable-12-vzP0pLxes9bn0zV8.htm)|Insight Coffee (Moderate)|Café de perspicacité modéré|libre|
|[consumable-12-w492F18MtpqNu2X7.htm](equipment/consumable-12-w492F18MtpqNu2X7.htm)|Spirit Bulb (Greater)|Bulbe spirituel supérieur|libre|
|[consumable-12-WwWssHoEhyh7Yrrw.htm](equipment/consumable-12-WwWssHoEhyh7Yrrw.htm)|Energy Breath Potion (Cold, Moderate)|Potion de souffle d'énergie de froid modérée|libre|
|[consumable-12-x04huvOBBXmExTDl.htm](equipment/consumable-12-x04huvOBBXmExTDl.htm)|Black Dragon's Breath Potion (Adult)|Potion de souffle du dragon noir adulte|officielle|
|[consumable-12-xAoRfG7uxFFjOHoT.htm](equipment/consumable-12-xAoRfG7uxFFjOHoT.htm)|Phoenix Flask|Flasque de phénix|libre|
|[consumable-12-xnIEWJYmJh6j58Wd.htm](equipment/consumable-12-xnIEWJYmJh6j58Wd.htm)|Dazzling Rosary (Greater)|Chapelet éblouissant supérieur|libre|
|[consumable-12-ybu2BMu2oWj74wl8.htm](equipment/consumable-12-ybu2BMu2oWj74wl8.htm)|Salve of Antiparalysis (Greater)|Baume antiparalysie supérieur|libre|
|[consumable-12-zQnidYCmLCBPfdii.htm](equipment/consumable-12-zQnidYCmLCBPfdii.htm)|Energy Breath Potion (Electricity, Moderate)|Potion de souffle d'énergie électrique modérée|libre|
|[consumable-13-10ddNHYJJOvEhEFD.htm](equipment/consumable-13-10ddNHYJJOvEhEFD.htm)|Whelming Scrimshaw|Os de baleine plongeur|libre|
|[consumable-13-3M7iB9tfrsWTccZ0.htm](equipment/consumable-13-3M7iB9tfrsWTccZ0.htm)|Healer's Gel (Greater)|Gel du guérisseur supérieur|libre|
|[consumable-13-6vyr3drpizIrd5PS.htm](equipment/consumable-13-6vyr3drpizIrd5PS.htm)|Daylight Vapor|Vapeur de lumière du jour|libre|
|[consumable-13-8qT6Jxy97jfdXfGi.htm](equipment/consumable-13-8qT6Jxy97jfdXfGi.htm)|Rattling Bolt (Greater)|Carreau cliquetant supérieur|libre|
|[consumable-13-AGD0qLB8zKGinKwv.htm](equipment/consumable-13-AGD0qLB8zKGinKwv.htm)|Camp Shroud (Greater)|Linceul de camp supérieur|libre|
|[consumable-13-ah9jxSBerznPowW7.htm](equipment/consumable-13-ah9jxSBerznPowW7.htm)|Binding Coil (Greater)|Lien d'attache supérieur|libre|
|[consumable-13-AvbQWGCBZIOsK45X.htm](equipment/consumable-13-AvbQWGCBZIOsK45X.htm)|Sparking Spellgun (Greater)|Lancesort étincelant supérieur|libre|
|[consumable-13-bLmuVUkaMeyq5QlI.htm](equipment/consumable-13-bLmuVUkaMeyq5QlI.htm)|Djezet Dose|Dose de djezet|libre|
|[consumable-13-BxzwmwQ5O4fewa4w.htm](equipment/consumable-13-BxzwmwQ5O4fewa4w.htm)|Purple Worm Repellent|Répulsif de ver pourpre|libre|
|[consumable-13-C0Zhu7Vwy9Aipwoh.htm](equipment/consumable-13-C0Zhu7Vwy9Aipwoh.htm)|Spectral Nightshade|Belladonne spectrale|libre|
|[consumable-13-dwRqo9KQki2iCwHu.htm](equipment/consumable-13-dwRqo9KQki2iCwHu.htm)|Ablative Shield Plating (Major)|Blindage ablatif de bouclier majeur|libre|
|[consumable-13-dyvwp4EHgJDzVjJh.htm](equipment/consumable-13-dyvwp4EHgJDzVjJh.htm)|Metalmist Sphere (Greater)|Sphère de brume-métal supérieure|libre|
|[consumable-13-ex1kVj3SUbTd1smt.htm](equipment/consumable-13-ex1kVj3SUbTd1smt.htm)|Crackling Bubble Gum (Greater)|Chewing-gum crépitant supérieur|libre|
|[consumable-13-FETCOUQa6rYtlAHB.htm](equipment/consumable-13-FETCOUQa6rYtlAHB.htm)|Kaiju Fulu|Fulu kaiju|libre|
|[consumable-13-fomEZZ4MxVVK3uVu.htm](equipment/consumable-13-fomEZZ4MxVVK3uVu.htm)|Scroll of 7th-rank Spell|Parchemin de sort de rang 7|libre|
|[consumable-13-g1aarurTGfb7QDTO.htm](equipment/consumable-13-g1aarurTGfb7QDTO.htm)|Cayden's Brew (Double)|Breuvage de Cayden double|libre|
|[consumable-13-g27kK39Nzqphl1Tb.htm](equipment/consumable-13-g27kK39Nzqphl1Tb.htm)|Explosive Ammunition (Greater)|Munition explosive supérieure|libre|
|[consumable-13-Hh2qMYtJU4WTOUFC.htm](equipment/consumable-13-Hh2qMYtJU4WTOUFC.htm)|Moonlit Spellgun (Greater)|Lancesort clair de lune supérieur|libre|
|[consumable-13-igJxUzuMlYdrwSU2.htm](equipment/consumable-13-igJxUzuMlYdrwSU2.htm)|Octopus Potion (Greater)|Potion pieuvre supérieure|libre|
|[consumable-13-iRDCa4OVSTGUD3vi.htm](equipment/consumable-13-iRDCa4OVSTGUD3vi.htm)|Purple Worm Venom|Venin de ver pourpre|officielle|
|[consumable-13-kiXh4SUWKr166ZeM.htm](equipment/consumable-13-kiXh4SUWKr166ZeM.htm)|Magic Wand (6th-Rank Spell)|Baguette magique (sort de rang 6)|libre|
|[consumable-13-kVng8qcoHLBORFIc.htm](equipment/consumable-13-kVng8qcoHLBORFIc.htm)|Golden Branding Iron (Major)|Fer à marquer doré majeur|libre|
|[consumable-13-lQn0OkifOW0Grp9z.htm](equipment/consumable-13-lQn0OkifOW0Grp9z.htm)|Rusting Ammunition (Moderate)|Munition oxydante modérée|libre|
|[consumable-13-m1XubXwbBy3JzMkY.htm](equipment/consumable-13-m1XubXwbBy3JzMkY.htm)|Pummel-Growth Toxin|Toxine|libre|
|[consumable-13-mMP0ycsdW2nLlCeO.htm](equipment/consumable-13-mMP0ycsdW2nLlCeO.htm)|Spiritual Warhorn (Greater)|Cor de guerre spirituel supérieur|libre|
|[consumable-13-mYozcVg9fQY6zO7C.htm](equipment/consumable-13-mYozcVg9fQY6zO7C.htm)|Elixir of Life (Greater)|Élixir de vie supérieur|libre|
|[consumable-13-n4FF7K3SLb7q1v6v.htm](equipment/consumable-13-n4FF7K3SLb7q1v6v.htm)|Spellstrike Ammunition (Type VI)|Munition de frappe magique (Type VI)|libre|
|[consumable-13-Pde3rUX3rx7VSCHn.htm](equipment/consumable-13-Pde3rUX3rx7VSCHn.htm)|Singularity Ammunition|Munition de sigularité|libre|
|[consumable-13-pXV4ov0DJHzIHSEW.htm](equipment/consumable-13-pXV4ov0DJHzIHSEW.htm)|Rovagug's Mud|Boue de Rovagug|libre|
|[consumable-13-QImdOdyolrMWwuxD.htm](equipment/consumable-13-QImdOdyolrMWwuxD.htm)|Gorgon's Breath|Souffle de gorgone|libre|
|[consumable-13-RtU28UqJuqJQMJym.htm](equipment/consumable-13-RtU28UqJuqJQMJym.htm)|False Death Vial|Fiole de fausse mort|libre|
|[consumable-13-TBuLxrVXa4UGHzb9.htm](equipment/consumable-13-TBuLxrVXa4UGHzb9.htm)|Force Tiles|Tommette de force|libre|
|[consumable-13-u1ouo0mH0sS6QoGw.htm](equipment/consumable-13-u1ouo0mH0sS6QoGw.htm)|Void Fragment|Fragment de vide|libre|
|[consumable-13-VbVw8IDiYczzbIHt.htm](equipment/consumable-13-VbVw8IDiYczzbIHt.htm)|Potion Patch (Greater)|Bandage de potion supérieur|libre|
|[consumable-13-vRO9Orlts0jP7nCx.htm](equipment/consumable-13-vRO9Orlts0jP7nCx.htm)|Euphoric Loop (Greater)|Loupe euphorisante supérieure|libre|
|[consumable-13-wuVD3kolQaFAEeBK.htm](equipment/consumable-13-wuVD3kolQaFAEeBK.htm)|Numbing Tonic (Greater)|Tonique anesthésiant supérieur|libre|
|[consumable-13-xXvuQABxlRH2HXsO.htm](equipment/consumable-13-xXvuQABxlRH2HXsO.htm)|Ablative Armor Plating (Major)|Blindage ablatif d'armure majeur|libre|
|[consumable-13-xYJwgmn2Nmthztb7.htm](equipment/consumable-13-xYJwgmn2Nmthztb7.htm)|Thunderbird Tuft (Major)|Touffe d'oiseau-tonnerre majeure|libre|
|[consumable-13-Xzuya7EEF5FwVGFr.htm](equipment/consumable-13-Xzuya7EEF5FwVGFr.htm)|Worm Vial|Fiole du ver|libre|
|[consumable-13-YS7LJSvooG7mfwH1.htm](equipment/consumable-13-YS7LJSvooG7mfwH1.htm)|Frozen Lava of Droskar's Crag|Lave gelée de la Faille de Droskar|libre|
|[consumable-14-1OJxRnPs0viJVrZq.htm](equipment/consumable-14-1OJxRnPs0viJVrZq.htm)|Potion of Fire Resistance (Greater)|Potion de résistance au feu supérieure|libre|
|[consumable-14-2iWD3YHmsQ9uicIF.htm](equipment/consumable-14-2iWD3YHmsQ9uicIF.htm)|Elemental Fragment (Major)|Fragment élémentaire majeur|libre|
|[consumable-14-4dwBcqeuXePd89qW.htm](equipment/consumable-14-4dwBcqeuXePd89qW.htm)|Starshot Arrow (Greater)|Flèche étoile supérieure|libre|
|[consumable-14-5koINU0dy5nPL8Cg.htm](equipment/consumable-14-5koINU0dy5nPL8Cg.htm)|Rending Snare|Piège artisanal d'arrachement|libre|
|[consumable-14-62IVHZyHA6239PHe.htm](equipment/consumable-14-62IVHZyHA6239PHe.htm)|Unsullied Blood (Major)|Sang non souillé majeur|libre|
|[consumable-14-6Qq7F1wlH5cx189e.htm](equipment/consumable-14-6Qq7F1wlH5cx189e.htm)|Dreaming Round|Munition de rêves|libre|
|[consumable-14-8eKDnjLeP9KpZFD3.htm](equipment/consumable-14-8eKDnjLeP9KpZFD3.htm)|Winterstep Elixir (Greater)|Élixir de démarche hivernale supérieur|libre|
|[consumable-14-8qUja4YdvewN4es0.htm](equipment/consumable-14-8qUja4YdvewN4es0.htm)|Death Knell Powder|Poudre de mise à mort|libre|
|[consumable-14-95ki5dSg5Porv04G.htm](equipment/consumable-14-95ki5dSg5Porv04G.htm)|Depth Charge V|Charge de profondeur V|libre|
|[consumable-14-9wwCHxuCfBA9w6Ik.htm](equipment/consumable-14-9wwCHxuCfBA9w6Ik.htm)|Feyfoul (Greater)|Cafouillefée supérieur|libre|
|[consumable-14-aBN9WkTHUCtE259N.htm](equipment/consumable-14-aBN9WkTHUCtE259N.htm)|Conduit Shot (Greater)|Tir conducteur supérieur|libre|
|[consumable-14-ANzB9WzlGQXDn76G.htm](equipment/consumable-14-ANzB9WzlGQXDn76G.htm)|Antidote (Major)|Antidote majeur|libre|
|[consumable-14-cplkaokMiskvFgAe.htm](equipment/consumable-14-cplkaokMiskvFgAe.htm)|Exuviae Powder|Poudre d'exuviae|libre|
|[consumable-14-DIQg2Tml1wWjSC1q.htm](equipment/consumable-14-DIQg2Tml1wWjSC1q.htm)|Engulfing Snare|Piège artisanal engloutissant|libre|
|[consumable-14-EDjJy68ZFLu57ldN.htm](equipment/consumable-14-EDjJy68ZFLu57ldN.htm)|Noxious Incense (Greater)|Encens nocif supérieur|libre|
|[consumable-14-EmnU1xFDutJ70HaI.htm](equipment/consumable-14-EmnU1xFDutJ70HaI.htm)|Dragontooth Trophy|Trophée dent de dragon|libre|
|[consumable-14-esDSCEI1cf8Op93I.htm](equipment/consumable-14-esDSCEI1cf8Op93I.htm)|Addiction Suppressant (Major)|Suppresseur de dépendance majeur|libre|
|[consumable-14-F8vrGctUqYIEkKOz.htm](equipment/consumable-14-F8vrGctUqYIEkKOz.htm)|Potion of Electricity Resistance (Greater)|Potion de résistance à l'électricité supérieur|libre|
|[consumable-14-GcllrP56XWEsYQ4j.htm](equipment/consumable-14-GcllrP56XWEsYQ4j.htm)|Potion of Sonic Resistance (Greater)|Potion de résistance au son supérieure|libre|
|[consumable-14-HBlUlkSlIQ0NQA9N.htm](equipment/consumable-14-HBlUlkSlIQ0NQA9N.htm)|Vapor Sphere|Sphère de vapeur|libre|
|[consumable-14-ipaBbiROyAN4McWA.htm](equipment/consumable-14-ipaBbiROyAN4McWA.htm)|Tentacle Potion (Greater)|Potion tentaculaire supérieure|libre|
|[consumable-14-JukXiZWas5RJaE3M.htm](equipment/consumable-14-JukXiZWas5RJaE3M.htm)|Bonmuan Swapping Stone (Moderate)|Pierre d'échange Bonmuan modéré|libre|
|[consumable-14-kajpTXg3oHIJStQa.htm](equipment/consumable-14-kajpTXg3oHIJStQa.htm)|Antiplague (Major)|Antimaladie majeur|libre|
|[consumable-14-lasqzrPVEHI5MkQd.htm](equipment/consumable-14-lasqzrPVEHI5MkQd.htm)|Bomber's Eye Elixir (Greater)|Élixir d'oeil de mitrailleur supérieur|libre|
|[consumable-14-mJSCOIJllj2GtACC.htm](equipment/consumable-14-mJSCOIJllj2GtACC.htm)|Grinning Pugwampi|Pugwampi ricanant|libre|
|[consumable-14-MpuFL2midIke1Sbn.htm](equipment/consumable-14-MpuFL2midIke1Sbn.htm)|Life Shot (Greater)|Tir de vie supérieur|libre|
|[consumable-14-P3EL4ntkd5InC3YB.htm](equipment/consumable-14-P3EL4ntkd5InC3YB.htm)|Ruby Capacitor (Greater)|Condensateur de rubis supérieur|libre|
|[consumable-14-p7kWhbRrUCbMzd7A.htm](equipment/consumable-14-p7kWhbRrUCbMzd7A.htm)|Spirit Bulb (Major)|Bulbe spirituel majeur|libre|
|[consumable-14-Pb0WOuCR34FYGmuy.htm](equipment/consumable-14-Pb0WOuCR34FYGmuy.htm)|Thousand-Pains Fulu (Icicle)|Fulu des mille douleurs (Glaçon)|libre|
|[consumable-14-pV5M6IYoPDhTy6FA.htm](equipment/consumable-14-pV5M6IYoPDhTy6FA.htm)|Chopping Evisceration Snare|Piège artisanal d'éviscération hachante|libre|
|[consumable-14-px55EOmlMx8xtEgM.htm](equipment/consumable-14-px55EOmlMx8xtEgM.htm)|Unending Itch|Démangeaison interminable|libre|
|[consumable-14-Qcla3yTa1MzcpMjb.htm](equipment/consumable-14-Qcla3yTa1MzcpMjb.htm)|Emetic Paste (Greater)|Pâte vomitive supérieure|libre|
|[consumable-14-RN8TnAhWQ4tNKHgP.htm](equipment/consumable-14-RN8TnAhWQ4tNKHgP.htm)|Brewer's Regret (Greater)|Regret du brasseur supérieur|libre|
|[consumable-14-TKOe8epJTCef87Bj.htm](equipment/consumable-14-TKOe8epJTCef87Bj.htm)|Malleable Mixture (Greater)|Mixture de mollesse supérieure|libre|
|[consumable-14-ulGv1u9BAqRYYe4H.htm](equipment/consumable-14-ulGv1u9BAqRYYe4H.htm)|Shrapnel Snare|Piège artisanal de mitraille|libre|
|[consumable-14-vDgDrbSoGuiUIz2R.htm](equipment/consumable-14-vDgDrbSoGuiUIz2R.htm)|Potion of Acid Resistance (Greater)|Potion de résistance à l'acide supérieure|libre|
|[consumable-14-VZmUrseCZs9xMaxt.htm](equipment/consumable-14-VZmUrseCZs9xMaxt.htm)|Potion of Cold Resistance (Greater)|Potion de résistance au froid supérieure|libre|
|[consumable-14-Wyh13xRaOL57fve7.htm](equipment/consumable-14-Wyh13xRaOL57fve7.htm)|Red-Rib Gill Mask (Greater)|Masque branchie de Côte-rouge supérieur|libre|
|[consumable-14-XvmYNnQ4vg8GREH7.htm](equipment/consumable-14-XvmYNnQ4vg8GREH7.htm)|Iron Cudgel|Gourdin de fer|officielle|
|[consumable-14-YvmRcEky6fuIR0xp.htm](equipment/consumable-14-YvmRcEky6fuIR0xp.htm)|Impact Foam Chassis (Major)|Armature de mousse majeure|libre|
|[consumable-14-ywYt2SyZaV95KcZH.htm](equipment/consumable-14-ywYt2SyZaV95KcZH.htm)|Potion of Resistance (Greater)|Potion de résistance supérieure|libre|
|[consumable-15-1DmcPUZERWeKpiMM.htm](equipment/consumable-15-1DmcPUZERWeKpiMM.htm)|Serpent Oil (True)|Huile de serpent ultime|libre|
|[consumable-15-4B8jRW7jGybSlovV.htm](equipment/consumable-15-4B8jRW7jGybSlovV.htm)|Potion of Annulment (Moderate)|Potion d'abrogation modérée|libre|
|[consumable-15-6swrLimere2nZtz9.htm](equipment/consumable-15-6swrLimere2nZtz9.htm)|Dragon Bile|Bile de dragon|officielle|
|[consumable-15-9a7nJAErs4dfetFJ.htm](equipment/consumable-15-9a7nJAErs4dfetFJ.htm)|Potency Crystal (Major)|Cristal de puissance majeure|libre|
|[consumable-15-9CNxvAalHBPdSUFl.htm](equipment/consumable-15-9CNxvAalHBPdSUFl.htm)|Transposition Ammunition (Greater)|Munition de transposition supérieur|libre|
|[consumable-15-AmxSqEoFhRLMYd1W.htm](equipment/consumable-15-AmxSqEoFhRLMYd1W.htm)|Elixir of Life (Major)|Élixir de vie majeur|libre|
|[consumable-15-aOFly4vZu24x85b0.htm](equipment/consumable-15-aOFly4vZu24x85b0.htm)|Eldritch Flare|Flambée mystique|libre|
|[consumable-15-cqTCg6C0lvYUKQmx.htm](equipment/consumable-15-cqTCg6C0lvYUKQmx.htm)|Stone Body Mutagen (Greater)|Mutagène corps-de-pierre supérieur|libre|
|[consumable-15-D8i3OBVMkxmCCREV.htm](equipment/consumable-15-D8i3OBVMkxmCCREV.htm)|Poison Fizz (Greater)|Poison pétillant supérieur|libre|
|[consumable-15-E999hL7XlAlfZhjK.htm](equipment/consumable-15-E999hL7XlAlfZhjK.htm)|Potion of Flying (Greater)|Potion de vol supérieure|libre|
|[consumable-15-FJw54ZPLuJpChJbe.htm](equipment/consumable-15-FJw54ZPLuJpChJbe.htm)|Reverberating Stone (Greater)|Pierre réverbérante supérieure|libre|
|[consumable-15-Fz6jQ0M3v55R8CqA.htm](equipment/consumable-15-Fz6jQ0M3v55R8CqA.htm)|Big Rock Bullet (Major)|Bille gros rocher majeure|libre|
|[consumable-15-Gfew65lwkzZc3mUV.htm](equipment/consumable-15-Gfew65lwkzZc3mUV.htm)|Disintegration Bolt|Carreau de désintégration|officielle|
|[consumable-15-gJboDbIGscCvSlFc.htm](equipment/consumable-15-gJboDbIGscCvSlFc.htm)|Frozen Lava of Ka|Lave gelée de Ka|libre|
|[consumable-15-hnBcuuzzrBT0L0CL.htm](equipment/consumable-15-hnBcuuzzrBT0L0CL.htm)|Mukradi Jar|Jarre du mukradi|libre|
|[consumable-15-i4D4B7tpYv9vMvQY.htm](equipment/consumable-15-i4D4B7tpYv9vMvQY.htm)|Sea Touch Elixir (Greater)|Élixir de caresse marine supérieur|libre|
|[consumable-15-iPki3yuoucnj7bIt.htm](equipment/consumable-15-iPki3yuoucnj7bIt.htm)|Scroll of 8th-rank Spell|Parchemin de sort de rang 8|libre|
|[consumable-15-kFqPDnz0dYvK0mIG.htm](equipment/consumable-15-kFqPDnz0dYvK0mIG.htm)|Firestarter Pellets (Major)|Boulettes d'ignition majeures|libre|
|[consumable-15-nmXPj9zuMRQBNT60.htm](equipment/consumable-15-nmXPj9zuMRQBNT60.htm)|Magic Wand (7th-Rank Spell)|Baguette magique (sort de rang 7)|libre|
|[consumable-15-R4UPjbSDwDEmDJBA.htm](equipment/consumable-15-R4UPjbSDwDEmDJBA.htm)|Azure Worm Repellent|Répulsif de ver azur|libre|
|[consumable-15-R8ZmXOfbfrp54VVB.htm](equipment/consumable-15-R8ZmXOfbfrp54VVB.htm)|Garrote Bolt|Carreau étrangleur|libre|
|[consumable-15-rEev4xrQyMvjz7W7.htm](equipment/consumable-15-rEev4xrQyMvjz7W7.htm)|Grudgestone (Greater)|Pierre de la rancune supérieure|libre|
|[consumable-15-rpqgJkCepHecj1eA.htm](equipment/consumable-15-rpqgJkCepHecj1eA.htm)|Crackling Bubble Gum (Major)|Chewing-gum crépitant majeur|libre|
|[consumable-15-SBpUiOJiap2cZk2Z.htm](equipment/consumable-15-SBpUiOJiap2cZk2Z.htm)|Indomitable Keepsake (Major)|Souvenir indomptable majeur|libre|
|[consumable-15-sVTg7RMv8qFuQD3y.htm](equipment/consumable-15-sVTg7RMv8qFuQD3y.htm)|Cold Comfort (Greater)|Confort fraicheur supérieur|libre|
|[consumable-15-sWPkbSlWAEAStBqD.htm](equipment/consumable-15-sWPkbSlWAEAStBqD.htm)|Bravo's Brew (Greater)|Breuvage de bravoure supérieur|libre|
|[consumable-15-UOH0A4xNSMnpV6i4.htm](equipment/consumable-15-UOH0A4xNSMnpV6i4.htm)|Life-Boosting Oil (Major)|Huile stimulatrice de vie majeure|libre|
|[consumable-15-VFUOEkaCaSCJsu6U.htm](equipment/consumable-15-VFUOEkaCaSCJsu6U.htm)|Bargainer's Instrument|Instrument de marchandage|libre|
|[consumable-15-VqJuhN2Ba1DnrJNW.htm](equipment/consumable-15-VqJuhN2Ba1DnrJNW.htm)|Mind-Swap Potion|Potion Échange esprit|libre|
|[consumable-15-WearPqN56FQofpF6.htm](equipment/consumable-15-WearPqN56FQofpF6.htm)|Spellstrike Ammunition (Type VII)|Munition de frappe magique (Type VII)|libre|
|[consumable-15-XQjGhnBWOIh1uB2w.htm](equipment/consumable-15-XQjGhnBWOIh1uB2w.htm)|Garrote Shot|Tir garrot|libre|
|[consumable-15-zELyHWSfkD4yRUjZ.htm](equipment/consumable-15-zELyHWSfkD4yRUjZ.htm)|Torrent Spellgun (Major)|Lancesort torrent majeur|libre|
|[consumable-16-4Pmo9gc81JAOzdke.htm](equipment/consumable-16-4Pmo9gc81JAOzdke.htm)|Flame Navette|Navette flamboyante|officielle|
|[consumable-16-7p3C9xf3XgUbvKHL.htm](equipment/consumable-16-7p3C9xf3XgUbvKHL.htm)|Frenzy Oil|Huile de frénésie|libre|
|[consumable-16-9hdT05ywPVyh9vQX.htm](equipment/consumable-16-9hdT05ywPVyh9vQX.htm)|Cerulean Scourge|Fléau céruléen|libre|
|[consumable-16-aDn5blt2iiYJpzbe.htm](equipment/consumable-16-aDn5blt2iiYJpzbe.htm)|Winter Wolf Elixir (Greater)|Élixir de loup arctique supérieur|officielle|
|[consumable-16-bKc5f8dAOjv1kwQ2.htm](equipment/consumable-16-bKc5f8dAOjv1kwQ2.htm)|Golden-Cased Bullet (Major)|Étui de balle doré majeur|libre|
|[consumable-16-C6v5p0ZtIUroYlU2.htm](equipment/consumable-16-C6v5p0ZtIUroYlU2.htm)|Vermin Repellent Agent (Major)|Agent répulsif de vermine majeur|libre|
|[consumable-16-dIqIuqjPbIuh9TYn.htm](equipment/consumable-16-dIqIuqjPbIuh9TYn.htm)|Tusk and Fang Chain|Chaîne du croc et de la défense|libre|
|[consumable-16-e90XsQumDxHzqS7z.htm](equipment/consumable-16-e90XsQumDxHzqS7z.htm)|Sniper's Bead (Major)|Perle du tireur d'élite majeure|libre|
|[consumable-16-EAgWYY1C4mDRfwqo.htm](equipment/consumable-16-EAgWYY1C4mDRfwqo.htm)|Numbing Tonic (Major)|Tonique anesthésiant majeur|libre|
|[consumable-16-EP8f2NL28vPlmX7k.htm](equipment/consumable-16-EP8f2NL28vPlmX7k.htm)|Oil of Object Animation (Greater)|Huile d'animation d'objet supérieur|libre|
|[consumable-16-FZxrYMcH0uH617EQ.htm](equipment/consumable-16-FZxrYMcH0uH617EQ.htm)|False Death Vial (Greater)|Fiole de fausse mort supérieure|libre|
|[consumable-16-HgI87CKuhY8IbFWG.htm](equipment/consumable-16-HgI87CKuhY8IbFWG.htm)|Camp Shroud (Major)|Linceul de camp majeur|libre|
|[consumable-16-hlXXODDYeRzWuPCK.htm](equipment/consumable-16-hlXXODDYeRzWuPCK.htm)|Magnetic Suit (Major)|Tenue magnétique majeure|libre|
|[consumable-16-jOuY6D1XmtHeax9H.htm](equipment/consumable-16-jOuY6D1XmtHeax9H.htm)|Weapon Shot (Major)|Tir d'arme majeur|libre|
|[consumable-16-kicNrnZz1KjJYRVI.htm](equipment/consumable-16-kicNrnZz1KjJYRVI.htm)|Eagle Eye Elixir (Major)|Élixir d'oeil de faucon majeur|libre|
|[consumable-16-mamIdMfPguGM8QV7.htm](equipment/consumable-16-mamIdMfPguGM8QV7.htm)|Weeping Midnight|Larmes de minuit|libre|
|[consumable-16-NQuE19X1YMWjiWHl.htm](equipment/consumable-16-NQuE19X1YMWjiWHl.htm)|Stormbreaker Fulu|Brise-tempête fulu|libre|
|[consumable-16-O4SbscW0mhRRSbRK.htm](equipment/consumable-16-O4SbscW0mhRRSbRK.htm)|Apricot of Bestial Might|Abricot de puissance bestiale|libre|
|[consumable-16-Odl2SyKw8Zg6ckKb.htm](equipment/consumable-16-Odl2SyKw8Zg6ckKb.htm)|Hail of Arrows Snare|Piège artisanal de volée de flèches|libre|
|[consumable-16-pkb6lKSanfcxvLtQ.htm](equipment/consumable-16-pkb6lKSanfcxvLtQ.htm)|Depth Charge VI|Charge de profondeur VI|libre|
|[consumable-16-PxrYRcawI6a3lRcf.htm](equipment/consumable-16-PxrYRcawI6a3lRcf.htm)|Animal Repellent (Major)|Répulsif animal majeur|libre|
|[consumable-16-R6qfTzNv79GH1xHU.htm](equipment/consumable-16-R6qfTzNv79GH1xHU.htm)|Life Shot (Major)|Tir de vie majeur|libre|
|[consumable-16-s0DffbZbYlav3mu1.htm](equipment/consumable-16-s0DffbZbYlav3mu1.htm)|Alloy Orb (High-Grade)|Orbe d'alliage de qualité supérieure|libre|
|[consumable-16-tnCKwIsRsKj3FtG6.htm](equipment/consumable-16-tnCKwIsRsKj3FtG6.htm)|Omnidirectional Spear Snare|Piège artisanal de lances omnidirectionnelles|libre|
|[consumable-16-VJ9mXi70JprHHA0z.htm](equipment/consumable-16-VJ9mXi70JprHHA0z.htm)|Flare Beacon (Major)|Balise éclairante majeur|libre|
|[consumable-16-ws3OXRgAawwYIKK6.htm](equipment/consumable-16-ws3OXRgAawwYIKK6.htm)|Repulsion Resin|Résine répulsive|libre|
|[consumable-16-WXnGIl4d62detRlf.htm](equipment/consumable-16-WXnGIl4d62detRlf.htm)|Cold Iron Blanch (Greater)|Blanchis de fer froid supérieur|libre|
|[consumable-16-X6vNtRjyHIuN7vqj.htm](equipment/consumable-16-X6vNtRjyHIuN7vqj.htm)|Nightmare Vapor|Vapeur de cauchemar|officielle|
|[consumable-16-xm8FszDcmRrnlk09.htm](equipment/consumable-16-xm8FszDcmRrnlk09.htm)|Blister Ammunition (Greater)|Munition à balle supérieure|libre|
|[consumable-16-XMuLrJYL6fxv4YNA.htm](equipment/consumable-16-XMuLrJYL6fxv4YNA.htm)|Salamander Elixir (Greater)|Élixir de la salamandre supérieur|officielle|
|[consumable-16-XWO5pMQcYNybqWzf.htm](equipment/consumable-16-XWO5pMQcYNybqWzf.htm)|Silver Crescent (Greater)|Croissant argenté supérieur|libre|
|[consumable-16-yPFHTY1GH3rdWwds.htm](equipment/consumable-16-yPFHTY1GH3rdWwds.htm)|Dust of Corpse Animation (Greater)|Poussière d'animation de cadavre supérieur|libre|
|[consumable-17-1kNp6yOS0aZPBPzZ.htm](equipment/consumable-17-1kNp6yOS0aZPBPzZ.htm)|Juggernaut Mutagen (Major)|Mutagène de juggernaut majeur|officielle|
|[consumable-17-2JXUER4xY8s36HUv.htm](equipment/consumable-17-2JXUER4xY8s36HUv.htm)|Energy Breath Potion (Acid, Greater)|Potion de souffle d'énergie corrosive supérieure|libre|
|[consumable-17-34MbgwW1SGlowyk2.htm](equipment/consumable-17-34MbgwW1SGlowyk2.htm)|Energy Mutagen (Major)|Mutagène d'énergie majeur|libre|
|[consumable-17-4GXzTN6iSDGfYEAi.htm](equipment/consumable-17-4GXzTN6iSDGfYEAi.htm)|Quicksilver Mutagen (Major)|Mutagène de vif-argent majeur|officielle|
|[consumable-17-4vb8fplhdtLDBxxh.htm](equipment/consumable-17-4vb8fplhdtLDBxxh.htm)|Energy Breath Potion (Sonic, Greater)|Potion de souffle d'énergie sonique supérieure|libre|
|[consumable-17-6odVabL0WL2H89Ic.htm](equipment/consumable-17-6odVabL0WL2H89Ic.htm)|Sixfingers Elixir (Greater)|Élixir Six-doigts supérieur|libre|
|[consumable-17-9Fps57Xc1XxxWK0j.htm](equipment/consumable-17-9Fps57Xc1XxxWK0j.htm)|Fire and Iceberg (Major)|Feu et iceberg majeur|libre|
|[consumable-17-bASMkq9syBtpaXok.htm](equipment/consumable-17-bASMkq9syBtpaXok.htm)|False Hope|Fausse espérance|libre|
|[consumable-17-bvIVxDq1wh6IavHP.htm](equipment/consumable-17-bvIVxDq1wh6IavHP.htm)|Choker-Arm Mutagen (Major)|Mutagène d'assouplissement majeur|libre|
|[consumable-17-ccrdVliTNBh2mNZf.htm](equipment/consumable-17-ccrdVliTNBh2mNZf.htm)|Serene Mutagen (Major)|Mutagène de sérénité majeur|officielle|
|[consumable-17-cFHomF3tty8Wi1e5.htm](equipment/consumable-17-cFHomF3tty8Wi1e5.htm)|Scroll of 9th-rank Spell|Parchemin de sort de rang 9|libre|
|[consumable-17-d2sU6nOtBdIVJMVI.htm](equipment/consumable-17-d2sU6nOtBdIVJMVI.htm)|Red Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire rouge|officielle|
|[consumable-17-eoSvkOAkx4YGLQ2O.htm](equipment/consumable-17-eoSvkOAkx4YGLQ2O.htm)|Bonmuan Swapping Stone (Greater)|Pierre d'échange Bonmuan supérieure|libre|
|[consumable-17-ETLSXa4Po518z9Qx.htm](equipment/consumable-17-ETLSXa4Po518z9Qx.htm)|Spiderfoot Brew (Greater)|Infusion de pattes d'araignée majeure|libre|
|[consumable-17-fiBThkOOXEdcTSMf.htm](equipment/consumable-17-fiBThkOOXEdcTSMf.htm)|Prey Mutagen (Major)|Mutagène proie majeur|libre|
|[consumable-17-H20eUVst7rgwhUb6.htm](equipment/consumable-17-H20eUVst7rgwhUb6.htm)|Breathtaking Vapor|Vapeur époustouflante|libre|
|[consumable-17-IBJIBTkTilVV0BdC.htm](equipment/consumable-17-IBJIBTkTilVV0BdC.htm)|Impossible Cake (Major)|Gâteau de l'impossible majeur|libre|
|[consumable-17-idtIMGkFmIe958Et.htm](equipment/consumable-17-idtIMGkFmIe958Et.htm)|Brass Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire d'airain|officielle|
|[consumable-17-iYOBu0hMYzqFYvZN.htm](equipment/consumable-17-iYOBu0hMYzqFYvZN.htm)|Moonlit Spellgun (Major)|Lancesort clair de lune majeur|libre|
|[consumable-17-JqKNiEJxSZX1sD06.htm](equipment/consumable-17-JqKNiEJxSZX1sD06.htm)|Magnetic Shot (Greater)|Tir magnétique supérieur|libre|
|[consumable-17-KZbAqbt7qxJPPkii.htm](equipment/consumable-17-KZbAqbt7qxJPPkii.htm)|Energy Breath Potion (Fire, Greater)|Potion de souffle d'énergie de feu supérieure|libre|
|[consumable-17-lJTl2SuUaluMzPBl.htm](equipment/consumable-17-lJTl2SuUaluMzPBl.htm)|Bronze Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire de bronze|officielle|
|[consumable-17-LR7Ke2V8vHjQYBni.htm](equipment/consumable-17-LR7Ke2V8vHjQYBni.htm)|Black Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire noir|officielle|
|[consumable-17-ltILKAEnrZ8vKwY0.htm](equipment/consumable-17-ltILKAEnrZ8vKwY0.htm)|Energy Breath Potion (Electricity, Greater)|Potion de souffle d'énergie électrique supérieure|libre|
|[consumable-17-lvsQe0qLzhnWYqa0.htm](equipment/consumable-17-lvsQe0qLzhnWYqa0.htm)|Frozen Lava of Sakalayo|Lave gelée du Sakalayo|libre|
|[consumable-17-M4ZOHOlne43ArjOC.htm](equipment/consumable-17-M4ZOHOlne43ArjOC.htm)|Drakeheart Mutagen (Major)|Mutagène de coeur de drake majeur|libre|
|[consumable-17-mPta1oQq0IWp5uE3.htm](equipment/consumable-17-mPta1oQq0IWp5uE3.htm)|Meteor Shot (Major)|Balle météore majeure|libre|
|[consumable-17-NfMteX8WWC5mxc52.htm](equipment/consumable-17-NfMteX8WWC5mxc52.htm)|Spiritual Warhorn (Major)|Cor de guerre spirituel majeur|libre|
|[consumable-17-NOPrIz6UNxof1M5d.htm](equipment/consumable-17-NOPrIz6UNxof1M5d.htm)|Black Tendril Shot (Greater)|Tir de vrilles noires supérieur|libre|
|[consumable-17-nS75vsM3x5jxlUqn.htm](equipment/consumable-17-nS75vsM3x5jxlUqn.htm)|Bestial Mutagen (Major)|Mutagène bestial majeur|officielle|
|[consumable-17-OiQDcUMrlrYXDgEf.htm](equipment/consumable-17-OiQDcUMrlrYXDgEf.htm)|Energy Breath Potion (Cold, Greater)|Potion de souffle d'énergie supérieure|libre|
|[consumable-17-OJvCWeJPHEc7GpFG.htm](equipment/consumable-17-OJvCWeJPHEc7GpFG.htm)|Green Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire vert|officielle|
|[consumable-17-OkYPLMDxprdm24hH.htm](equipment/consumable-17-OkYPLMDxprdm24hH.htm)|Cryomister (Major)|Cryobrumisateur majeur|libre|
|[consumable-17-oqiC4Mc5qKNCl88e.htm](equipment/consumable-17-oqiC4Mc5qKNCl88e.htm)|Spellstrike Ammunition (Type VIII)|Munition de frappe magique (Type VIII)|libre|
|[consumable-17-PW3H8TnouiLaTC8G.htm](equipment/consumable-17-PW3H8TnouiLaTC8G.htm)|Silver Dragon's Breath Potion (Wyrm)|Potion de souffle de dracosire d'argent|officielle|
|[consumable-17-Qa4qjxk5aPzx7HTR.htm](equipment/consumable-17-Qa4qjxk5aPzx7HTR.htm)|War Blood Mutagen (Major)|Mutagène de sang guerrier majeur|officielle|
|[consumable-17-Qs8RgNH6thRPv2jt.htm](equipment/consumable-17-Qs8RgNH6thRPv2jt.htm)|Magic Wand (8th-Rank Spell)|Baguette magique (sort de rang 8)|libre|
|[consumable-17-qvAtAv211cVqktgU.htm](equipment/consumable-17-qvAtAv211cVqktgU.htm)|Greengut|Tord-boyaux vert|libre|
|[consumable-17-sd3H2Nir3qLFNVI3.htm](equipment/consumable-17-sd3H2Nir3qLFNVI3.htm)|Faerie Dragon Liqueur (Wyrm)|Liqueur de dragon féerique vénérable|libre|
|[consumable-17-uD5vRYVOXNJ53sEE.htm](equipment/consumable-17-uD5vRYVOXNJ53sEE.htm)|Silvertongue Mutagen (Major)|Mutagène de langue dorée majeur|officielle|
|[consumable-17-UeUPNFw600HEdDgF.htm](equipment/consumable-17-UeUPNFw600HEdDgF.htm)|Soothing Tonic (Major)|Tonique apaisant majeur|libre|
|[consumable-17-UOvDye8obnqjfMQG.htm](equipment/consumable-17-UOvDye8obnqjfMQG.htm)|Healing Vapor (Major)|Vapeur de guérison majeure|libre|
|[consumable-17-VBK9i74dry8yf8f0.htm](equipment/consumable-17-VBK9i74dry8yf8f0.htm)|Cognitive Mutagen (Major)|Mutagène cognitif majeur|officielle|
|[consumable-17-vmwrU3lz1HFfadAS.htm](equipment/consumable-17-vmwrU3lz1HFfadAS.htm)|Sanguine Mutagen (Major)|Mutagène sanguin majeur|libre|
|[consumable-17-vuJi1kahoCAHHTSX.htm](equipment/consumable-17-vuJi1kahoCAHHTSX.htm)|Thousand-Pains Fulu (Burl)|Fulu des milles douleurs (loupe)|libre|
|[consumable-17-VXxwxLwJiDODlUZq.htm](equipment/consumable-17-VXxwxLwJiDODlUZq.htm)|Demolition Fulu (Greater)|Fulu de démolition supérieur|libre|
|[consumable-17-Wv0ck7AMP0s0jIue.htm](equipment/consumable-17-Wv0ck7AMP0s0jIue.htm)|Theatrical Mutagen (Major)|Mutagène théatral majeur|libre|
|[consumable-17-Wx5WhSNOmeICgHSC.htm](equipment/consumable-17-Wx5WhSNOmeICgHSC.htm)|Deadweight Mutagen (Major)|Mutagène de poids-mort majeur|libre|
|[consumable-18-1LKJvK9ofzIIkQX3.htm](equipment/consumable-18-1LKJvK9ofzIIkQX3.htm)|Sun Orchid Poultice|Cataplasme d'orchidée solaire|libre|
|[consumable-18-3UXsXwUbvOdpPg6b.htm](equipment/consumable-18-3UXsXwUbvOdpPg6b.htm)|Blood Booster (Greater)|Activateur sanguin supérieur|libre|
|[consumable-18-6CzLtRHIBfhMpyv2.htm](equipment/consumable-18-6CzLtRHIBfhMpyv2.htm)|Cloning Potion|Potion de clonage|libre|
|[consumable-18-aMbJl8eTV3Ri5O6e.htm](equipment/consumable-18-aMbJl8eTV3Ri5O6e.htm)|Rovagug's Mud (Greater)|Boue de Rovagug supérieure|libre|
|[consumable-18-bCPAiqiWmH7pVxNE.htm](equipment/consumable-18-bCPAiqiWmH7pVxNE.htm)|Potion of Cold Retaliation (Major)|Potion de riposte glaciale majeure|libre|
|[consumable-18-bEaiID2KLQ8lTCai.htm](equipment/consumable-18-bEaiID2KLQ8lTCai.htm)|Potion of Electricity Retaliation (Major)|Potion de ripose électrique majeure|libre|
|[consumable-18-ECjche2BfpWHZ3vU.htm](equipment/consumable-18-ECjche2BfpWHZ3vU.htm)|Alloy Orb (Exquisite High-Grade)|Orbe d'alliage raffiné de haute qualité|libre|
|[consumable-18-ed8gAxli6Of58kAM.htm](equipment/consumable-18-ed8gAxli6Of58kAM.htm)|Lastwall Soup (Greater)|Soupe de Dernier-Rempart supérieur|libre|
|[consumable-18-fabw5fDuaTMUF0tb.htm](equipment/consumable-18-fabw5fDuaTMUF0tb.htm)|Potion of Acid Retaliation (Major)|Potion de riposte acide majeure|libre|
|[consumable-18-FBS2dhoOfSlcuPt6.htm](equipment/consumable-18-FBS2dhoOfSlcuPt6.htm)|Explosive Mine (Major)|Mine explosive majeure|libre|
|[consumable-18-J3gd5vhcxwFbpCXe.htm](equipment/consumable-18-J3gd5vhcxwFbpCXe.htm)|Awakened Adamantine Shot|Tir d'adamantium éveillé|libre|
|[consumable-18-jdkMRl7zxOVGUuGI.htm](equipment/consumable-18-jdkMRl7zxOVGUuGI.htm)|Universal Solvent (Major)|Solvant universel majeur|libre|
|[consumable-18-JZZmDY1IJmKcdR2D.htm](equipment/consumable-18-JZZmDY1IJmKcdR2D.htm)|Vaccine (Major)|Vaccin majeur|libre|
|[consumable-18-lu3kPHtKZTHPx4Fx.htm](equipment/consumable-18-lu3kPHtKZTHPx4Fx.htm)|Kraken Bottle|Bouteille de kraken|libre|
|[consumable-18-nctubaNAmzvXYDkc.htm](equipment/consumable-18-nctubaNAmzvXYDkc.htm)|Crimson Worm Repellent|Répulsif de ver écarlate|libre|
|[consumable-18-nkozt1tueA0Xz6VF.htm](equipment/consumable-18-nkozt1tueA0Xz6VF.htm)|Depth Charge VII|Charge de profondeur VII|libre|
|[consumable-18-nlm26zGDAHTISTh6.htm](equipment/consumable-18-nlm26zGDAHTISTh6.htm)|Avalanche of Stones Snare|Piège artisanal d'avalanche de rochers|libre|
|[consumable-18-obER4cKi8UbGhSg7.htm](equipment/consumable-18-obER4cKi8UbGhSg7.htm)|Oil of Unlife (Major)|Huile de non-vie majeure|libre|
|[consumable-18-OtFKHfUQnn97ILBC.htm](equipment/consumable-18-OtFKHfUQnn97ILBC.htm)|Gearbinder Oil (Greater)|Huile grippe engrenages supérieure|libre|
|[consumable-18-p3ppzFSsZXFRe3H8.htm](equipment/consumable-18-p3ppzFSsZXFRe3H8.htm)|Healing Potion (Major)|Potion de guérison majeure|libre|
|[consumable-18-PFr49j091jjb8363.htm](equipment/consumable-18-PFr49j091jjb8363.htm)|Ruby Capacitor (Major)|Condensateur de rubis majeur|libre|
|[consumable-18-qJaIfdtDrysjcy37.htm](equipment/consumable-18-qJaIfdtDrysjcy37.htm)|Ooze Ammunition (Major)|Munition de vase majeure|libre|
|[consumable-18-RkI3jKKrCozvEvfr.htm](equipment/consumable-18-RkI3jKKrCozvEvfr.htm)|King's Sleep|Sommeil du roi|officielle|
|[consumable-18-TalLb1YlvXzutuPc.htm](equipment/consumable-18-TalLb1YlvXzutuPc.htm)|Potion of Fire Retaliation (Major)|Potion de riposte enflammée majeure|libre|
|[consumable-18-TSMeh3NfrVEMulWR.htm](equipment/consumable-18-TSMeh3NfrVEMulWR.htm)|Choleric Contagion|Contagion colérique|libre|
|[consumable-18-UpUJ6NHBin7bubeX.htm](equipment/consumable-18-UpUJ6NHBin7bubeX.htm)|Cayden's Brew (Triple)|Breuvage de Cayden triple|libre|
|[consumable-18-vTEWlRBUD4VzVwxK.htm](equipment/consumable-18-vTEWlRBUD4VzVwxK.htm)|Applereed Mutagen (Greater)|Mutagène pousse-roseau supérieur|libre|
|[consumable-18-ye6XAJY21YQqRnKH.htm](equipment/consumable-18-ye6XAJY21YQqRnKH.htm)|Implosion Dust (Greater)|Poussière d'implosion supérieure|libre|
|[consumable-18-Z333awQuoTxGkbgG.htm](equipment/consumable-18-Z333awQuoTxGkbgG.htm)|Rusting Ammunition (Greater)|Munition oxydabte supérieure|libre|
|[consumable-19-5hGJ6CZi0A9OjMd4.htm](equipment/consumable-19-5hGJ6CZi0A9OjMd4.htm)|Oblivion Essence|Essence de l'oubli|libre|
|[consumable-19-7VDf5tQKh01XV6HQ.htm](equipment/consumable-19-7VDf5tQKh01XV6HQ.htm)|Numbing Tonic (True)|Tonique anesthésiant ultime|libre|
|[consumable-19-aAGjV3wkopQ44VX3.htm](equipment/consumable-19-aAGjV3wkopQ44VX3.htm)|Spellstrike Ammunition (Type IX)|Munition de frappe magique (Type IX)|libre|
|[consumable-19-B9NVsgy2jvd6sJID.htm](equipment/consumable-19-B9NVsgy2jvd6sJID.htm)|Oil of Potency (Major)|Huile de puissance majeure|libre|
|[consumable-19-CorMilTH3XKGW49D.htm](equipment/consumable-19-CorMilTH3XKGW49D.htm)|Insight Coffee (Greater)|Café de perspicacité supérieur|libre|
|[consumable-19-Fgv722039TVM5JTc.htm](equipment/consumable-19-Fgv722039TVM5JTc.htm)|Magic Wand (9th-Rank Spell)|Baguette magique (sort de rang 9)|libre|
|[consumable-19-gn5ITN5GILHKxGA4.htm](equipment/consumable-19-gn5ITN5GILHKxGA4.htm)|Ambrosia of Undying Hope|Ambroisie d'éternel espoir|libre|
|[consumable-19-HD0RIJcRIIKKAdkn.htm](equipment/consumable-19-HD0RIJcRIIKKAdkn.htm)|Ablative Shield Plating (True)|Blindage ablatif de bouclier ultime|libre|
|[consumable-19-m16rhrqO81674IxV.htm](equipment/consumable-19-m16rhrqO81674IxV.htm)|Pale Fade|Affadisseur|libre|
|[consumable-19-o1XIHJ4MJyroAHfF.htm](equipment/consumable-19-o1XIHJ4MJyroAHfF.htm)|Scroll of 10th-rank Spell|Parchemin de sort de rang 10|libre|
|[consumable-19-qK964kZz1ALcysOa.htm](equipment/consumable-19-qK964kZz1ALcysOa.htm)|Elixir of Life (True)|Élixir de vie ultime|libre|
|[consumable-19-RdsQliP7Gi1xe8xH.htm](equipment/consumable-19-RdsQliP7Gi1xe8xH.htm)|Ablative Armor Plating (True)|Blindage ablatif d'armure ultime|libre|
|[consumable-19-SeIweTBlvD1EipTQ.htm](equipment/consumable-19-SeIweTBlvD1EipTQ.htm)|Chromatic Jellyfish Oil (Greater)|Huile de méduse chromatique supérieure|libre|
|[consumable-19-SxXy4a8uHf88iCf9.htm](equipment/consumable-19-SxXy4a8uHf88iCf9.htm)|Spell Echo Shot|Tir écho de sort|libre|
|[consumable-19-ugxI9kH7osJ3J5qG.htm](equipment/consumable-19-ugxI9kH7osJ3J5qG.htm)|Frozen Lava of Barrowsiege|Lave gelée de Barrowsiege|libre|
|[consumable-20-4642hELMtWF9jywN.htm](equipment/consumable-20-4642hELMtWF9jywN.htm)|Golden Breath Fulu|Fulu du souffle doré|libre|
|[consumable-20-dRi775Uhzqgn7aBs.htm](equipment/consumable-20-dRi775Uhzqgn7aBs.htm)|Nightmare Salt|Sel de cauchemars|libre|
|[consumable-20-FB7qAHjiiHTGhRnn.htm](equipment/consumable-20-FB7qAHjiiHTGhRnn.htm)|Celestial Hair|Chevelure céleste|libre|
|[consumable-20-Fv97oB3iEIFAyzTu.htm](equipment/consumable-20-Fv97oB3iEIFAyzTu.htm)|Philosopher's Stone|Pierre philosophale|officielle|
|[consumable-20-J5LsHDGEv4CVN7vr.htm](equipment/consumable-20-J5LsHDGEv4CVN7vr.htm)|Potion of Annulment (Greater)|Potion d'abrogation supérieure|libre|
|[consumable-20-NUlz1XulrXqhpd2w.htm](equipment/consumable-20-NUlz1XulrXqhpd2w.htm)|Achaekek's Kiss|Baiser d'Achaékek|libre|
|[consumable-20-P9dhnMcr3lbHBn2E.htm](equipment/consumable-20-P9dhnMcr3lbHBn2E.htm)|Sun Orchid Elixir|Élixir d'orchidée solaire|officielle|
|[consumable-20-pbsrb7FMMHQBd5CO.htm](equipment/consumable-20-pbsrb7FMMHQBd5CO.htm)|Starsong Nectar|Nectar du chant des étoiles|libre|
|[consumable-20-qvfO0jFIvIM8hReG.htm](equipment/consumable-20-qvfO0jFIvIM8hReG.htm)|Flying Blade Wheel Snare|Piège artisanal de lames tournoyantes|libre|
|[consumable-20-UHrLqWCnFEUspSQj.htm](equipment/consumable-20-UHrLqWCnFEUspSQj.htm)|Elixir of Rejuvenation|Élixir de jouvence|officielle|
|[consumable-20-UzyjCLHxs3JQuFt5.htm](equipment/consumable-20-UzyjCLHxs3JQuFt5.htm)|Life Shot (True)|Tir de vie ultime|libre|
|[consumable-20-X3DCIU2fmj97pi1J.htm](equipment/consumable-20-X3DCIU2fmj97pi1J.htm)|Bonmuan Swapping Stone (Major)|Pierre d'échange Bonmuan majeur|libre|
|[consumable-20-xqCz2vStMNJujfpp.htm](equipment/consumable-20-xqCz2vStMNJujfpp.htm)|Instant Evisceration Snare|Piège artisanal d'éviscération instantanée|officielle|
|[consumable-20-zZZRK9AJ5VGIcbzU.htm](equipment/consumable-20-zZZRK9AJ5VGIcbzU.htm)|Death Coil|Bobine mortelle|libre|
|[equipment-00-02luiB9xtE1yaKzo.htm](equipment/equipment-00-02luiB9xtE1yaKzo.htm)|Splint|Attelles|libre|
|[equipment-00-0BEbpOMBo7ET8NsR.htm](equipment/equipment-00-0BEbpOMBo7ET8NsR.htm)|Silencer|Silencieux|libre|
|[equipment-00-1RiYM3lQDx30XEfB.htm](equipment/equipment-00-1RiYM3lQDx30XEfB.htm)|Firearm Cleaning Kit|Kit de nettoyage d'armes à feu|libre|
|[equipment-00-1YkOplOqn9OgGLyg.htm](equipment/equipment-00-1YkOplOqn9OgGLyg.htm)|Extendable Pincer|Pince extensible|libre|
|[equipment-00-2c5pF6q5XkjLn8MU.htm](equipment/equipment-00-2c5pF6q5XkjLn8MU.htm)|Mask (Ordinary)|Masque ordinaire|libre|
|[equipment-00-2fVNJ23gb3dixVu8.htm](equipment/equipment-00-2fVNJ23gb3dixVu8.htm)|Buoyancy Vest|Gilet de flottaison|libre|
|[equipment-00-2qJyAnnVIMFIdeTD.htm](equipment/equipment-00-2qJyAnnVIMFIdeTD.htm)|Puzzle Box (Simple) (Hollow)|Boite à énigme simple creuse|libre|
|[equipment-00-3MGQ8KxH7HNA8THZ.htm](equipment/equipment-00-3MGQ8KxH7HNA8THZ.htm)|Jellyfish Lamp|Méduse lumineuse|officielle|
|[equipment-00-3tOyV4VZEZhwnAMO.htm](equipment/equipment-00-3tOyV4VZEZhwnAMO.htm)|Fishing Tackle|Matériel de pêche|officielle|
|[equipment-00-3yLu12TCwN8utY0u.htm](equipment/equipment-00-3yLu12TCwN8utY0u.htm)|Writing Set (Extra Ink and Paper)|Encre et papier supplémentaires|officielle|
|[equipment-00-3ZHl40YXlI47uIrT.htm](equipment/equipment-00-3ZHl40YXlI47uIrT.htm)|Cane|Canne|libre|
|[equipment-00-44F1mfJei4GY8f2X.htm](equipment/equipment-00-44F1mfJei4GY8f2X.htm)|Crowbar|Pied-de-biche|libre|
|[equipment-00-4AnNkIA5Ux6ePy7r.htm](equipment/equipment-00-4AnNkIA5Ux6ePy7r.htm)|Orichalcum Ingot|Lingot d'orichalque|libre|
|[equipment-00-4ftXXUCBHcf4b0MH.htm](equipment/equipment-00-4ftXXUCBHcf4b0MH.htm)|Alchemist's Toolkit|Matériel d'alchimiste|libre|
|[equipment-00-4QufuSeCbjDD2hMa.htm](equipment/equipment-00-4QufuSeCbjDD2hMa.htm)|Comealong|Treuil manuel|libre|
|[equipment-00-5j5KyZsGOfbrJNUZ.htm](equipment/equipment-00-5j5KyZsGOfbrJNUZ.htm)|Ladder (10-foot)|Échelle (3 m)|officielle|
|[equipment-00-5NHciGwLcVJwEjQu.htm](equipment/equipment-00-5NHciGwLcVJwEjQu.htm)|Duskwood Branch|Branche en bois crépusculaire|libre|
|[equipment-00-6cyw0yK91cNsbvSK.htm](equipment/equipment-00-6cyw0yK91cNsbvSK.htm)|Mirror|Miroir|officielle|
|[equipment-00-6DCy7tEF1cxaIJMy.htm](equipment/equipment-00-6DCy7tEF1cxaIJMy.htm)|Grappling Hook|Grappin|libre|
|[equipment-00-7C5xxZw1VTiyUg37.htm](equipment/equipment-00-7C5xxZw1VTiyUg37.htm)|Grappling Gun|Pistolet à grappin|libre|
|[equipment-00-7fSnvJ2xoSfa6JXD.htm](equipment/equipment-00-7fSnvJ2xoSfa6JXD.htm)|Caltrops|Chausse-trappes|libre|
|[equipment-00-7HHKalX0yYJSHROS.htm](equipment/equipment-00-7HHKalX0yYJSHROS.htm)|Depth Gauge|Jauge de profondeur|libre|
|[equipment-00-7XJQmpYWRZRRutc4.htm](equipment/equipment-00-7XJQmpYWRZRRutc4.htm)|Peachwood Branch|Branche en pêcher céleste|libre|
|[equipment-00-81aHsD27HFGnq1Nt.htm](equipment/equipment-00-81aHsD27HFGnq1Nt.htm)|Soap|Savon|officielle|
|[equipment-00-85FCQF18RslpIhPs.htm](equipment/equipment-00-85FCQF18RslpIhPs.htm)|Toy Carriage|Chariot-jouet|libre|
|[equipment-00-8Jdw4yAzWYylGePS.htm](equipment/equipment-00-8Jdw4yAzWYylGePS.htm)|Torch|Torche|officielle|
|[equipment-00-8SkeqnhhYm4QGuJQ.htm](equipment/equipment-00-8SkeqnhhYm4QGuJQ.htm)|Basic Companion Chair|Fauteuil roulant du compagnon basique|libre|
|[equipment-00-8WH6ub3FVFYtcXCT.htm](equipment/equipment-00-8WH6ub3FVFYtcXCT.htm)|Adamantine Ingot|Lingot d'adamantium|libre|
|[equipment-00-937vrPcl9rpzdtJw.htm](equipment/equipment-00-937vrPcl9rpzdtJw.htm)|Clockwork Dial|Chronomètre|libre|
|[equipment-00-9UJbMaglf35GVzaZ.htm](equipment/equipment-00-9UJbMaglf35GVzaZ.htm)|Climbing Kit|Kit d'escalade|libre|
|[equipment-00-A1CDXAP4bNGgntE7.htm](equipment/equipment-00-A1CDXAP4bNGgntE7.htm)|Ball|Ballon|libre|
|[equipment-00-AiYlhL35OEtkqsHo.htm](equipment/equipment-00-AiYlhL35OEtkqsHo.htm)|Storage|Rangement|libre|
|[equipment-00-AnJiuDSgJ1uBYRaZ.htm](equipment/equipment-00-AnJiuDSgJ1uBYRaZ.htm)|Snowshoes|Raquettes de neige|libre|
|[equipment-00-aV7cXniPdT6tEI2l.htm](equipment/equipment-00-aV7cXniPdT6tEI2l.htm)|Waffle Iron|Gaufrier en fer|libre|
|[equipment-00-AxUJfrOmu7dY3XHP.htm](equipment/equipment-00-AxUJfrOmu7dY3XHP.htm)|Waterproof Firearm Carrying Case|Etui d'armes à feu imperméable|libre|
|[equipment-00-B0gbs7xKQc2J7AiV.htm](equipment/equipment-00-B0gbs7xKQc2J7AiV.htm)|Dawnsilver Chunk|Morceau d'aubargent|libre|
|[equipment-00-B3aJa0csNZOGXLXT.htm](equipment/equipment-00-B3aJa0csNZOGXLXT.htm)|Harrow Deck (Common)|Jeu de carte du Tourment ordinaire|libre|
|[equipment-00-b4hSaH0ITd5fbR08.htm](equipment/equipment-00-b4hSaH0ITd5fbR08.htm)|Peachwood Lumber|Planche en pêcher céleste|libre|
|[equipment-00-BbWTlbl43SmPNIdo.htm](equipment/equipment-00-BbWTlbl43SmPNIdo.htm)|Support|Bandage|libre|
|[equipment-00-bkbL0YitEh46Ne0f.htm](equipment/equipment-00-bkbL0YitEh46Ne0f.htm)|Armored Skirt|Jupe d'armure|libre|
|[equipment-00-Bu2xT8NfB6xaeTJa.htm](equipment/equipment-00-Bu2xT8NfB6xaeTJa.htm)|Tack|Sellerie|officielle|
|[equipment-00-C1j0Zs26TPVjplbs.htm](equipment/equipment-00-C1j0Zs26TPVjplbs.htm)|Harrow Deck (Simple)|Jeu de carte du Tourment simple|libre|
|[equipment-00-C9VvYnjbWertzgMc.htm](equipment/equipment-00-C9VvYnjbWertzgMc.htm)|Ka Stone|Pierre Ka|libre|
|[equipment-00-ccsgob2TZ7WqTrp7.htm](equipment/equipment-00-ccsgob2TZ7WqTrp7.htm)|Ten-Foot Pole|Perche de 3 mètres|officielle|
|[equipment-00-csfJtggwGCF28U2j.htm](equipment/equipment-00-csfJtggwGCF28U2j.htm)|Musical Instrument (Heavy)|Instrument de musique (Lourd)|officielle|
|[equipment-00-DA3HgyEBGEbtRNOo.htm](equipment/equipment-00-DA3HgyEBGEbtRNOo.htm)|Dueling Cape|Cape de duel|libre|
|[equipment-00-Da78zUcKodY81Sj4.htm](equipment/equipment-00-Da78zUcKodY81Sj4.htm)|Kite|Cerf-volant|libre|
|[equipment-00-DAqI0GwKuRwBagz5.htm](equipment/equipment-00-DAqI0GwKuRwBagz5.htm)|False Manacles|Fausses menottes|libre|
|[equipment-00-dIRZ0LL7G31fJNYz.htm](equipment/equipment-00-dIRZ0LL7G31fJNYz.htm)|Lantern (Hooded)|Lanterne sourde|officielle|
|[equipment-00-e0hrybnm5FBr28Su.htm](equipment/equipment-00-e0hrybnm5FBr28Su.htm)|Concealment Coin|Pièce de monnaie de dissimulation|libre|
|[equipment-00-EcLENuK2LXZdigTu.htm](equipment/equipment-00-EcLENuK2LXZdigTu.htm)|Cage|Cage|libre|
|[equipment-00-eQjibJl41aQpQ47d.htm](equipment/equipment-00-eQjibJl41aQpQ47d.htm)|Cold Iron Ingot|Lingot de fer froid|libre|
|[equipment-00-EsqFTJXcmzfngRgL.htm](equipment/equipment-00-EsqFTJXcmzfngRgL.htm)|Playing Cards|Jeu de Cartes|libre|
|[equipment-00-ETwv7GshGM9IXnqH.htm](equipment/equipment-00-ETwv7GshGM9IXnqH.htm)|Hollowed Hilt|Pommeau creux|libre|
|[equipment-00-f0lZf1oZYDEEeyYl.htm](equipment/equipment-00-f0lZf1oZYDEEeyYl.htm)|Sovereign Steel Chunk|Morceau d'acier souverain|libre|
|[equipment-00-fagzYdmfYyMQ6J77.htm](equipment/equipment-00-fagzYdmfYyMQ6J77.htm)|Bedroll|Sac de couchage|libre|
|[equipment-00-fnyjcHzySnUxxSeW.htm](equipment/equipment-00-fnyjcHzySnUxxSeW.htm)|Shield Augmentation|Amélioration de bouclier|libre|
|[equipment-00-FOWF5f0tCaApv9RE.htm](equipment/equipment-00-FOWF5f0tCaApv9RE.htm)|Spellbook (Blank)|Grimoire|officielle|
|[equipment-00-FphoSIj3t6iQj3ew.htm](equipment/equipment-00-FphoSIj3t6iQj3ew.htm)|Harness|Attache|libre|
|[equipment-00-fti8z0JYDxH63n0J.htm](equipment/equipment-00-fti8z0JYDxH63n0J.htm)|Ruler|Règle à mesurer|libre|
|[equipment-00-FWMGkUotwUJuht7i.htm](equipment/equipment-00-FWMGkUotwUJuht7i.htm)|Disguise Kit (Replacement Cosmetics)|Kit de déguisement (Stock de cosmétiques)|libre|
|[equipment-00-fyYnQf1NAx9fWFaS.htm](equipment/equipment-00-fyYnQf1NAx9fWFaS.htm)|Rope|Corde|officielle|
|[equipment-00-ge0PEbPmxFLhyy4X.htm](equipment/equipment-00-ge0PEbPmxFLhyy4X.htm)|Marbles|Billes de jeu|libre|
|[equipment-00-GpcZXdiJppItyWJg.htm](equipment/equipment-00-GpcZXdiJppItyWJg.htm)|Wheelbarrow|Brouette|libre|
|[equipment-00-GVdbmKFLi5PlMr5R.htm](equipment/equipment-00-GVdbmKFLi5PlMr5R.htm)|Hearing Aid|Appareil auditif basique|libre|
|[equipment-00-gwP3Uums2ApH6o9K.htm](equipment/equipment-00-gwP3Uums2ApH6o9K.htm)|Pilgrim's Token|Amulette du pélerin|libre|
|[equipment-00-h4fhKJmmx232FGd2.htm](equipment/equipment-00-h4fhKJmmx232FGd2.htm)|Crutch|Béquille basique|libre|
|[equipment-00-h7gsRKv6rORYgsv0.htm](equipment/equipment-00-h7gsRKv6rORYgsv0.htm)|Snare Kit|Nécessaire de fabrication de pièges artisanaux|libre|
|[equipment-00-h8XkyI4OUitdxjMF.htm](equipment/equipment-00-h8XkyI4OUitdxjMF.htm)|Leash|Laisse|libre|
|[equipment-00-HB7VTopmeRZQGc0z.htm](equipment/equipment-00-HB7VTopmeRZQGc0z.htm)|Reinforced Surcoat|Surcot renforcé|libre|
|[equipment-00-HEtJeLXMcMbgamxd.htm](equipment/equipment-00-HEtJeLXMcMbgamxd.htm)|Thresholds of Truth|Tome du Seuil de vérité|libre|
|[equipment-00-Hl2JP8PQKrKxAfAD.htm](equipment/equipment-00-Hl2JP8PQKrKxAfAD.htm)|Bandalore|Yoyo|libre|
|[equipment-00-HYnwTFuLsFSsUAMG.htm](equipment/equipment-00-HYnwTFuLsFSsUAMG.htm)|Splendid Skull Mask|Masque crâne splendide|libre|
|[equipment-00-IGgK7NAdfyAPm8V0.htm](equipment/equipment-00-IGgK7NAdfyAPm8V0.htm)|Basic Face Mask|Masque facial basique|libre|
|[equipment-00-ilbLQNy6TbBPW7sQ.htm](equipment/equipment-00-ilbLQNy6TbBPW7sQ.htm)|Clothing (Ordinary)|Vêtements ordinaires|officielle|
|[equipment-00-IM2pz0wnO4OEMr1f.htm](equipment/equipment-00-IM2pz0wnO4OEMr1f.htm)|Blocks|Blocs de bois|libre|
|[equipment-00-iYiA6CMR1N2E2Ny2.htm](equipment/equipment-00-iYiA6CMR1N2E2Ny2.htm)|Earplugs (PFS Guide)|Bouchons d'oreille (Guide PFS)|libre|
|[equipment-00-jerA2fFiK5wb32r5.htm](equipment/equipment-00-jerA2fFiK5wb32r5.htm)|Silver Ingot|Lingot en argent|libre|
|[equipment-00-jJZbdMHMdh8UL2j8.htm](equipment/equipment-00-jJZbdMHMdh8UL2j8.htm)|Clothing (Fine)|Vêtements beaux|officielle|
|[equipment-00-JlClrm1WfJ45oWcS.htm](equipment/equipment-00-JlClrm1WfJ45oWcS.htm)|Tent (Four-Person)|Tente (4 personnes)|officielle|
|[equipment-00-jLVS6JezrxJ8G94C.htm](equipment/equipment-00-jLVS6JezrxJ8G94C.htm)|Paint Set|Set de peinture|libre|
|[equipment-00-JU0fzXWJIdz9WOGF.htm](equipment/equipment-00-JU0fzXWJIdz9WOGF.htm)|Tank (Stationary)|Réservoir (fixe)|libre|
|[equipment-00-Jvp0x2Sc82WVpExT.htm](equipment/equipment-00-Jvp0x2Sc82WVpExT.htm)|Disguise Kit|Kit de déguisement|libre|
|[equipment-00-JxT2I1jUqJ59JM8e.htm](equipment/equipment-00-JxT2I1jUqJ59JM8e.htm)|Noqual Chunk|Morceau en noqual|libre|
|[equipment-00-k6xZkDUpF7E1Totd.htm](equipment/equipment-00-k6xZkDUpF7E1Totd.htm)|Alchemist's Lab|Laboratoire d'alchimie|officielle|
|[equipment-00-kDrlBnkDMVxPrahI.htm](equipment/equipment-00-kDrlBnkDMVxPrahI.htm)|Water Purifier|Purificateur d'eau|libre|
|[equipment-00-KGA495XLcsDVonEt.htm](equipment/equipment-00-KGA495XLcsDVonEt.htm)|Siccatite Chunk|Morceau de siccatite|libre|
|[equipment-00-KIpkxwuPWt0rOGT1.htm](equipment/equipment-00-KIpkxwuPWt0rOGT1.htm)|Swim Fins|Palmes de natation|libre|
|[equipment-00-ksNUlyRHFUSK9xmz.htm](equipment/equipment-00-ksNUlyRHFUSK9xmz.htm)|Earplugs|Bouchons d'oreille|libre|
|[equipment-00-kz2Trn8ETSlGv0wy.htm](equipment/equipment-00-kz2Trn8ETSlGv0wy.htm)|Pinwheel|Moulin à vent|libre|
|[equipment-00-Ld8duhvwTQ5lCQmF.htm](equipment/equipment-00-Ld8duhvwTQ5lCQmF.htm)|Silver Chunk|Morceau en argent|libre|
|[equipment-00-M91ye9B7IdxDIDsS.htm](equipment/equipment-00-M91ye9B7IdxDIDsS.htm)|Brass Ear|Oreille en laiton|libre|
|[equipment-00-ML0GR2cIEdMn2hoD.htm](equipment/equipment-00-ML0GR2cIEdMn2hoD.htm)|Hammer|Marteau|officielle|
|[equipment-00-MPv5Yx4w7scZGj2Y.htm](equipment/equipment-00-MPv5Yx4w7scZGj2Y.htm)|Musical Instrument (Handheld)|Instruments de musique (portatifs)|officielle|
|[equipment-00-nmSMa84RpsNv6pSt.htm](equipment/equipment-00-nmSMa84RpsNv6pSt.htm)|Djezet Mass|Morceau d'alliage djezet|libre|
|[equipment-00-o0ccn5HQAudGgNdU.htm](equipment/equipment-00-o0ccn5HQAudGgNdU.htm)|Merchant's Scale|Balance de marchand|officielle|
|[equipment-00-O28nMnWDcXpMtrMv.htm](equipment/equipment-00-O28nMnWDcXpMtrMv.htm)|Djezet Alloy Ingot|Lingot d'alliage djezet|libre|
|[equipment-00-o29fgVteF3yj8NKn.htm](equipment/equipment-00-o29fgVteF3yj8NKn.htm)|Parrying Scabbard|Fourreau de parade|libre|
|[equipment-00-O6XNICvMAolYdgFP.htm](equipment/equipment-00-O6XNICvMAolYdgFP.htm)|False-Bottomed Mug|Tasse à double fond|libre|
|[equipment-00-OAjI2eaXvHQRrERK.htm](equipment/equipment-00-OAjI2eaXvHQRrERK.htm)|Tear-Away Clothing|Vêtement déchirable|libre|
|[equipment-00-oDyRlkRos1abOGz0.htm](equipment/equipment-00-oDyRlkRos1abOGz0.htm)|Handling Gloves|Gants de manipulation|libre|
|[equipment-00-oJZe5rRitvioUgRh.htm](equipment/equipment-00-oJZe5rRitvioUgRh.htm)|Shootist Bandolier|Bandoulière du tireur|libre|
|[equipment-00-oLLpwiNApEEAFbXF.htm](equipment/equipment-00-oLLpwiNApEEAFbXF.htm)|Abysium Chunk|Morceau en abysium|libre|
|[equipment-00-OnyNpbR0dowRKg3I.htm](equipment/equipment-00-OnyNpbR0dowRKg3I.htm)|Mask (Fine)|Masque de qualité|libre|
|[equipment-00-OP1TkZ9ugn86W4Br.htm](equipment/equipment-00-OP1TkZ9ugn86W4Br.htm)|Harrow Deck (Fine)|Jeu de carte du Tourment de qualité|libre|
|[equipment-00-oX39xqMLVB8kNrY0.htm](equipment/equipment-00-oX39xqMLVB8kNrY0.htm)|Piton|Piton|officielle|
|[equipment-00-P4hCmpeuBochkxsJ.htm](equipment/equipment-00-P4hCmpeuBochkxsJ.htm)|Harrow Carrying Case|Malette de transport de jeu du Tourment|libre|
|[equipment-00-PBlxTrEyr3qXFBqq.htm](equipment/equipment-00-PBlxTrEyr3qXFBqq.htm)|Lock (Poor)|Serrure (médiocre)|officielle|
|[equipment-00-plplsXJsqrdqNQVI.htm](equipment/equipment-00-plplsXJsqrdqNQVI.htm)|Religious Symbol (Wooden)|Symbole religieux en bois|libre|
|[equipment-00-Q4KkKGGXq4bNGHh2.htm](equipment/equipment-00-Q4KkKGGXq4bNGHh2.htm)|Marked Playing Cards|Jeu de cartes marquées|libre|
|[equipment-00-QbOlqr4lSkeOEfty.htm](equipment/equipment-00-QbOlqr4lSkeOEfty.htm)|Primal Symbol|Symbole primordial|libre|
|[equipment-00-qCEOZ6109Yo34tRx.htm](equipment/equipment-00-qCEOZ6109Yo34tRx.htm)|Formula Book (Blank)|Recueil vierge|libre|
|[equipment-00-QFAHoE7FJ9TPCGWL.htm](equipment/equipment-00-QFAHoE7FJ9TPCGWL.htm)|Rubbing Set|Kit de moulage|libre|
|[equipment-00-QFzn6s5w9tzhcoM6.htm](equipment/equipment-00-QFzn6s5w9tzhcoM6.htm)|Corrective Lenses|Verre correcteur basique|libre|
|[equipment-00-QJb8S927Yj81EgHH.htm](equipment/equipment-00-QJb8S927Yj81EgHH.htm)|Writing Set|Nécessaire d'écriture|officielle|
|[equipment-00-qnpqMwAnRc55R7lv.htm](equipment/equipment-00-qnpqMwAnRc55R7lv.htm)|Noqual Ingot|Lingot en noqual|libre|
|[equipment-00-QpP7Mo7ad5lMOTpv.htm](equipment/equipment-00-QpP7Mo7ad5lMOTpv.htm)|Dawnsilver Ingot|Lingot d'aubargent|libre|
|[equipment-00-QrNvP9SgnK9DrerA.htm](equipment/equipment-00-QrNvP9SgnK9DrerA.htm)|Lantern (Bull's Eye)|Lanterne à capote|officielle|
|[equipment-00-r9lPNhrvG69Ae88d.htm](equipment/equipment-00-r9lPNhrvG69Ae88d.htm)|Duskwood Lumber|Planche de bois crépusculaire|libre|
|[equipment-00-rEz9WYhlx2Pm0gKk.htm](equipment/equipment-00-rEz9WYhlx2Pm0gKk.htm)|Cold Iron Chunk|Morceau de fer froid|libre|
|[equipment-00-rHugmTjO3kgyiTH0.htm](equipment/equipment-00-rHugmTjO3kgyiTH0.htm)|Air Bladder|Vessie d'air|libre|
|[equipment-00-RYi2tVST7PVN56mU.htm](equipment/equipment-00-RYi2tVST7PVN56mU.htm)|Wheelchair|Fauteuil roulant|libre|
|[equipment-00-S0hmnalGCudgXaLG.htm](equipment/equipment-00-S0hmnalGCudgXaLG.htm)|Tank (Traveling)|Réservoir (transportable)|libre|
|[equipment-00-s1vB3HdXjMigYAnY.htm](equipment/equipment-00-s1vB3HdXjMigYAnY.htm)|Healer's Toolkit|Matériel de guérisseur|libre|
|[equipment-00-SLQgdLk5K9JV4ACF.htm](equipment/equipment-00-SLQgdLk5K9JV4ACF.htm)|Clothing (Cold-Weather)|Vêtement d'hiver|libre|
|[equipment-00-Sw7MBLASN3xK4Y44.htm](equipment/equipment-00-Sw7MBLASN3xK4Y44.htm)|Thieves' Toolkit (Replacement Picks)|Crochets de remplacement|libre|
|[equipment-00-T8EopYZLT137CsdW.htm](equipment/equipment-00-T8EopYZLT137CsdW.htm)|Net|Filet|libre|
|[equipment-00-TqqWFIiJYA5tXlSE.htm](equipment/equipment-00-TqqWFIiJYA5tXlSE.htm)|Animal Bed|Lit pour animal|libre|
|[equipment-00-ty9LXqqYAnlsTh46.htm](equipment/equipment-00-ty9LXqqYAnlsTh46.htm)|Tripod|Trépied|libre|
|[equipment-00-UlIxxLm71UdRgCFE.htm](equipment/equipment-00-UlIxxLm71UdRgCFE.htm)|Flint and Steel|Silex et amorce|officielle|
|[equipment-00-upzjwQ96cZG0Xlmx.htm](equipment/equipment-00-upzjwQ96cZG0Xlmx.htm)|Religious Symbol (Silver)|Symbole religieux en argent|libre|
|[equipment-00-USHK6XQRwmq17xKh.htm](equipment/equipment-00-USHK6XQRwmq17xKh.htm)|Signal Whistle|Sifflet de signalisation|officielle|
|[equipment-00-UtRH6jt3iNFCgLLG.htm](equipment/equipment-00-UtRH6jt3iNFCgLLG.htm)|Spyglass|Longue-vue|officielle|
|[equipment-00-uWe8GXBjAdA0q4ad.htm](equipment/equipment-00-uWe8GXBjAdA0q4ad.htm)|Voicebox|Boîte vocale|libre|
|[equipment-00-V78itAtTHI4ugy0P.htm](equipment/equipment-00-V78itAtTHI4ugy0P.htm)|Chain (10 feet)|Chaîne (3 m)|libre|
|[equipment-00-VBaxMSVNY2n6kLzT.htm](equipment/equipment-00-VBaxMSVNY2n6kLzT.htm)|Prosthesis|Prothèse basique|libre|
|[equipment-00-VbjANEHtdxO8kV1n.htm](equipment/equipment-00-VbjANEHtdxO8kV1n.htm)|Abysium Ingot|Lingot d'abysium|libre|
|[equipment-00-VHxXMvBeBTq2FSdf.htm](equipment/equipment-00-VHxXMvBeBTq2FSdf.htm)|Material Component Pouch|Bourse de composants matériels|officielle|
|[equipment-00-vLGDUFrg4yGzpTQX.htm](equipment/equipment-00-vLGDUFrg4yGzpTQX.htm)|Repair Toolkit|Matériel de réparation|libre|
|[equipment-00-vM88qqJrUo3Yqth4.htm](equipment/equipment-00-vM88qqJrUo3Yqth4.htm)|Traitor's Ring|Anneau du traitre|libre|
|[equipment-00-VnPh324pKwd2ZB66.htm](equipment/equipment-00-VnPh324pKwd2ZB66.htm)|Waterskin|Outre|libre|
|[equipment-00-vQiH5HYTsSRIsdK5.htm](equipment/equipment-00-vQiH5HYTsSRIsdK5.htm)|Collar|Collier|libre|
|[equipment-00-w4Hd6nunVVqw3GWj.htm](equipment/equipment-00-w4Hd6nunVVqw3GWj.htm)|Basic Crafter's Book|Livre d'artisan basique|libre|
|[equipment-00-w5pzAwbKGLOOomrK.htm](equipment/equipment-00-w5pzAwbKGLOOomrK.htm)|Wax Key Blank|Boîte à moulage en cire|libre|
|[equipment-00-WAsQ6WuMYUB77Uh0.htm](equipment/equipment-00-WAsQ6WuMYUB77Uh0.htm)|Hourglass|Sablier|officielle|
|[equipment-00-Wc9og4mTKZDH3xmz.htm](equipment/equipment-00-Wc9og4mTKZDH3xmz.htm)|Tent (Pup)|Tente (petite)|libre|
|[equipment-00-wewWxypLaXcHjJ5P.htm](equipment/equipment-00-wewWxypLaXcHjJ5P.htm)|Glass Cutter|Coupe-verre|libre|
|[equipment-00-WnxXVCjZcMHy4eGO.htm](equipment/equipment-00-WnxXVCjZcMHy4eGO.htm)|Clay|Argile|libre|
|[equipment-00-wob6DB1FFdWs0mbp.htm](equipment/equipment-00-wob6DB1FFdWs0mbp.htm)|Compass|Boussole|officielle|
|[equipment-00-wrpI5z7iWB8XvflQ.htm](equipment/equipment-00-wrpI5z7iWB8XvflQ.htm)|Tool (Long)|Outil|officielle|
|[equipment-00-wRSS5vP8ltrThsoC.htm](equipment/equipment-00-wRSS5vP8ltrThsoC.htm)|Mug|Tasse|officielle|
|[equipment-00-WTVLL9xk0Tk87w93.htm](equipment/equipment-00-WTVLL9xk0Tk87w93.htm)|Fox Marble|Bille de renard|libre|
|[equipment-00-X5lWxe4ChgaFGYvh.htm](equipment/equipment-00-X5lWxe4ChgaFGYvh.htm)|Keep Stone Ingot|Lingot de pierre de garde|libre|
|[equipment-00-xE2CGurZp4eHc1Oc.htm](equipment/equipment-00-xE2CGurZp4eHc1Oc.htm)|Familiar Satchel|Sacoche de familier|libre|
|[equipment-00-XlmoKh60WMF9ou3J.htm](equipment/equipment-00-XlmoKh60WMF9ou3J.htm)|Signifier's Mask|Masque de signifer|libre|
|[equipment-00-XUEoUlpB8yGaXjLc.htm](equipment/equipment-00-XUEoUlpB8yGaXjLc.htm)|Membership Cords|Cordons d'association|libre|
|[equipment-00-xuTO0Lbu999HnBhK.htm](equipment/equipment-00-xuTO0Lbu999HnBhK.htm)|Sovereign Steel Ingot|Lingot d'acier souverain|libre|
|[equipment-00-XV2EHt1RWHqzPeUT.htm](equipment/equipment-00-XV2EHt1RWHqzPeUT.htm)|Shield Sconce|Applique de bouclier|libre|
|[equipment-00-XxlCMccCPhl2pZMT.htm](equipment/equipment-00-XxlCMccCPhl2pZMT.htm)|Plated Duster|Manteau renforcé|libre|
|[equipment-00-y34yjumCFakrbtdw.htm](equipment/equipment-00-y34yjumCFakrbtdw.htm)|Artisan's Toolkit|Matériel d'artisan|libre|
|[equipment-00-Y5wqlqkmWy38vgeK.htm](equipment/equipment-00-Y5wqlqkmWy38vgeK.htm)|Siccatite Ingot|Lingot de siccatite|libre|
|[equipment-00-y9Ln2q1EhD3Jdu9H.htm](equipment/equipment-00-y9Ln2q1EhD3Jdu9H.htm)|Inubrix Ingot|Lingot d'inubrix|libre|
|[equipment-00-Yf7maiRDmmHAyF82.htm](equipment/equipment-00-Yf7maiRDmmHAyF82.htm)|Cookware|Batterie de cuisine|officielle|
|[equipment-00-YOgUBjv2YHzPFKUb.htm](equipment/equipment-00-YOgUBjv2YHzPFKUb.htm)|Orichalcum Chunk|Morceau d'orichalque|libre|
|[equipment-00-yXipdeJUEGMPT4dM.htm](equipment/equipment-00-yXipdeJUEGMPT4dM.htm)|Knight's Standard|Étendard de chevalier|libre|
|[equipment-00-Z1blVMXUeojHd9lE.htm](equipment/equipment-00-Z1blVMXUeojHd9lE.htm)|Keep Stone Chunk|Morceau de pierre de garde|libre|
|[equipment-00-z1okOYtNVnpBNj9F.htm](equipment/equipment-00-z1okOYtNVnpBNj9F.htm)|Religious Text|Texte religieux|libre|
|[equipment-00-Z5xNCrAN3TgdFTOV.htm](equipment/equipment-00-Z5xNCrAN3TgdFTOV.htm)|Splendid Pyschopomp Mask|Masque de psychopompe splendide|libre|
|[equipment-00-zDzs24ZMlzQgmTUd.htm](equipment/equipment-00-zDzs24ZMlzQgmTUd.htm)|Folding Ladder|Échelle pliante|libre|
|[equipment-00-Ze64YjceQqnwRCsU.htm](equipment/equipment-00-Ze64YjceQqnwRCsU.htm)|Harrow Mat|Tapis de cartes du Tourment|libre|
|[equipment-00-ZEDDVQDtUZ2qOB5q.htm](equipment/equipment-00-ZEDDVQDtUZ2qOB5q.htm)|Orc Warmask|Masque de guerre orc|libre|
|[equipment-00-zKrGossz3t0mQrQq.htm](equipment/equipment-00-zKrGossz3t0mQrQq.htm)|Inubrix Chunk|Morceau d'inubrix|libre|
|[equipment-00-Zo5MZWVBKssVPEcv.htm](equipment/equipment-00-Zo5MZWVBKssVPEcv.htm)|Adamantine Chunk|Morceau d'adamantium|libre|
|[equipment-00-ZPFDfWPmWLPUJeW1.htm](equipment/equipment-00-ZPFDfWPmWLPUJeW1.htm)|Doll|Poupée jouet|libre|
|[equipment-00-ZPY2dOaMYciIJv1Q.htm](equipment/equipment-00-ZPY2dOaMYciIJv1Q.htm)|Manacles (Poor)|Menottes médiocres|officielle|
|[equipment-00-Zr4PwTEuBLE30kkn.htm](equipment/equipment-00-Zr4PwTEuBLE30kkn.htm)|Tool (Short)|Outil (court)|officielle|
|[equipment-00-ZRaZSTEOpTC1ObdE.htm](equipment/equipment-00-ZRaZSTEOpTC1ObdE.htm)|Puzzle Box (Simple)|Boite à énigmes simple|libre|
|[equipment-00-zvLyCVD8g2PdHJAc.htm](equipment/equipment-00-zvLyCVD8g2PdHJAc.htm)|Thieves' Toolkit|Matériel de voleur|libre|
|[equipment-00-ZWTK4Q2JlvJ5Bx6A.htm](equipment/equipment-00-ZWTK4Q2JlvJ5Bx6A.htm)|Basic Chair|Fauteuil roulant basique|libre|
|[equipment-01-00gDg8WcPv3TKC9N.htm](equipment/equipment-01-00gDg8WcPv3TKC9N.htm)|Olfactory Stimulators|Stimulateurs olfactifs|libre|
|[equipment-01-1m0wiNht4sd05oL6.htm](equipment/equipment-01-1m0wiNht4sd05oL6.htm)|Floating Tent (Pup)|Tente flottante petite|libre|
|[equipment-01-20LwBpDPRaLv0dGt.htm](equipment/equipment-01-20LwBpDPRaLv0dGt.htm)|Magical Prosthetic Eye|Prothèse oculaire magique|libre|
|[equipment-01-2mt15LcfMArk7JqT.htm](equipment/equipment-01-2mt15LcfMArk7JqT.htm)|Ganjay Book (1 bulk book)|Livre de Ganjay (1 encombrement)|libre|
|[equipment-01-4CuEQqahVT8TmNld.htm](equipment/equipment-01-4CuEQqahVT8TmNld.htm)|Net Launcher|Lanceur de filet|libre|
|[equipment-01-4mW1K9vZ0VIIEazS.htm](equipment/equipment-01-4mW1K9vZ0VIIEazS.htm)|Weapon Harness|Attache d'arme|libre|
|[equipment-01-4qhnEsaypUjLm7eP.htm](equipment/equipment-01-4qhnEsaypUjLm7eP.htm)|Burnished Plating|Blindage poli|libre|
|[equipment-01-4rJjASdTnQO2zkt2.htm](equipment/equipment-01-4rJjASdTnQO2zkt2.htm)|Impulse Control|Contrôle d'impulsion|libre|
|[equipment-01-5wanv9n8UnUQ852q.htm](equipment/equipment-01-5wanv9n8UnUQ852q.htm)|Portable Weapon Mount (Monopod)|Monture d'arme portable monopode|libre|
|[equipment-01-6lmTRwT1dNzgWcUd.htm](equipment/equipment-01-6lmTRwT1dNzgWcUd.htm)|Puzzle Box (Complex)|Boite à énigmes complexe|libre|
|[equipment-01-6NEOf1UznG4Po0Qi.htm](equipment/equipment-01-6NEOf1UznG4Po0Qi.htm)|Games (Loaded Dice)|Jeux (dés pipés)|libre|
|[equipment-01-8Rt9cf86G5Ve9Ent.htm](equipment/equipment-01-8Rt9cf86G5Ve9Ent.htm)|Magnetic Construction Set|Set de construction magnétique|libre|
|[equipment-01-8SZ9FRW4rBhjyW3j.htm](equipment/equipment-01-8SZ9FRW4rBhjyW3j.htm)|Mask (Plague)|Masque de peste|libre|
|[equipment-01-9wBwuCi6jo4sfaLb.htm](equipment/equipment-01-9wBwuCi6jo4sfaLb.htm)|Puzzle Box (Complex) (Hollow)|Boite à énigmes creuse complexe|libre|
|[equipment-01-ApMC9PSOnj7cPAGL.htm](equipment/equipment-01-ApMC9PSOnj7cPAGL.htm)|Bomb Launcher|Lanceur de bombe|libre|
|[equipment-01-ba7Yba3Qexj4DpnD.htm](equipment/equipment-01-ba7Yba3Qexj4DpnD.htm)|Magical Hearing Aid|Appareil auditif magique|libre|
|[equipment-01-bWcH5lQ3AuaQwZ08.htm](equipment/equipment-01-bWcH5lQ3AuaQwZ08.htm)|Mudlily|Lys de vase|libre|
|[equipment-01-C5vVNVAHO5Kfaveo.htm](equipment/equipment-01-C5vVNVAHO5Kfaveo.htm)|Traveling Companion's Chair|Fauteuil roulant du compagnon de voyage|libre|
|[equipment-01-ckGYDocGEaelHfXF.htm](equipment/equipment-01-ckGYDocGEaelHfXF.htm)|Manacles (Simple)|Menottes simples|officielle|
|[equipment-01-CVTbxCY85nLoHYuw.htm](equipment/equipment-01-CVTbxCY85nLoHYuw.htm)|Aeon Stone (Consumed)|Pierre d'éternité brûlée|libre|
|[equipment-01-dj6sdn6b8Rw1Taih.htm](equipment/equipment-01-dj6sdn6b8Rw1Taih.htm)|Mechanical Torch|Torche mécanique|libre|
|[equipment-01-FABVnBeYtGXshlwm.htm](equipment/equipment-01-FABVnBeYtGXshlwm.htm)|Ring of Discretion|Anneau de discrétion|libre|
|[equipment-01-fXD42N5cGUYCtvp2.htm](equipment/equipment-01-fXD42N5cGUYCtvp2.htm)|Cloak of Feline Rest|Cape du repos félin|libre|
|[equipment-01-FYv6k2zkd0MJ93la.htm](equipment/equipment-01-FYv6k2zkd0MJ93la.htm)|Eye Slash|Entailles à l'oeil|libre|
|[equipment-01-geAAUwfmOc5U0qOE.htm](equipment/equipment-01-geAAUwfmOc5U0qOE.htm)|Rhythm Bone|Os de rythme|libre|
|[equipment-01-gxfIiPebrvjXeALk.htm](equipment/equipment-01-gxfIiPebrvjXeALk.htm)|Mortal Chronicle|Chronique mortelle|libre|
|[equipment-01-H5N2CacR3pNtanKB.htm](equipment/equipment-01-H5N2CacR3pNtanKB.htm)|Doll (Surprise)|Poupée surprise|libre|
|[equipment-01-hD95fU3S507iTHcb.htm](equipment/equipment-01-hD95fU3S507iTHcb.htm)|Injection Reservoir|Réservoir d'injection|libre|
|[equipment-01-HuC6uonXt6YXePjb.htm](equipment/equipment-01-HuC6uonXt6YXePjb.htm)|Armor Latches|Loquets|libre|
|[equipment-01-i2JwyZJpVZhlOObL.htm](equipment/equipment-01-i2JwyZJpVZhlOObL.htm)|Waterproof Journal|Journal imperméable|libre|
|[equipment-01-i5SEEbSALTzH9rgg.htm](equipment/equipment-01-i5SEEbSALTzH9rgg.htm)|Psychopomp Mask|Masque de psychopompe|libre|
|[equipment-01-iKKZdkn8KQJSuRjY.htm](equipment/equipment-01-iKKZdkn8KQJSuRjY.htm)|Stalk Goggles|Lunettes à tiges|libre|
|[equipment-01-j42OKMXPsp6Mg7nY.htm](equipment/equipment-01-j42OKMXPsp6Mg7nY.htm)|Subtle Armor|Armure subtile|libre|
|[equipment-01-JFd6QBDtSNC43dy0.htm](equipment/equipment-01-JFd6QBDtSNC43dy0.htm)|Traveler's Chair|Fauteuil du voyageur|libre|
|[equipment-01-JZXCTwoIWoGKjMX6.htm](equipment/equipment-01-JZXCTwoIWoGKjMX6.htm)|Lock (Simple)|Serrure (simple)|officielle|
|[equipment-01-k7ExX8IPY7mSzYPF.htm](equipment/equipment-01-k7ExX8IPY7mSzYPF.htm)|Reading Ring|Anneau de lecture|libre|
|[equipment-01-khU38JBZBOAxWzhY.htm](equipment/equipment-01-khU38JBZBOAxWzhY.htm)|Replacement Filter (Level 1)|Filtre de remplacement (niveau 1)|libre|
|[equipment-01-KKhw57JFYEUO4TnK.htm](equipment/equipment-01-KKhw57JFYEUO4TnK.htm)|Timepiece (Desktop Clock)|Horlogerie (Horloge de bureau)|libre|
|[equipment-01-LiK84TSQJoe1e6D7.htm](equipment/equipment-01-LiK84TSQJoe1e6D7.htm)|Clockwork Megaphone|Mégaphone mécanique|libre|
|[equipment-01-m4U2P5A1Dk1C1wr1.htm](equipment/equipment-01-m4U2P5A1Dk1C1wr1.htm)|Ignitor|Allumeur|libre|
|[equipment-01-MHJLBBESka5n3Bsg.htm](equipment/equipment-01-MHJLBBESka5n3Bsg.htm)|Traveler's Cloak|Cape du Voyageur|libre|
|[equipment-01-MJ1wJbEXRw5KcvwN.htm](equipment/equipment-01-MJ1wJbEXRw5KcvwN.htm)|Clockwork Bookshelf|Étagère mécanique|libre|
|[equipment-01-MpgoMNvnvy3Ysskq.htm](equipment/equipment-01-MpgoMNvnvy3Ysskq.htm)|Navigator's Star|Étoile du navigateur|libre|
|[equipment-01-mRz8Jmk4Q06SsZpC.htm](equipment/equipment-01-mRz8Jmk4Q06SsZpC.htm)|Everlight Crystal|Cristal de lumière éternelle|libre|
|[equipment-01-n5Or4nmtKPoFr6Hm.htm](equipment/equipment-01-n5Or4nmtKPoFr6Hm.htm)|Blade Launcher|Lanceur de lame|libre|
|[equipment-01-nbRNjXYEx6T0G8AW.htm](equipment/equipment-01-nbRNjXYEx6T0G8AW.htm)|Purifying Spoon (Teaspoon)|Cuillère purificatrice (cuillère à thé)|libre|
|[equipment-01-NocVi59Vtbi3kvsJ.htm](equipment/equipment-01-NocVi59Vtbi3kvsJ.htm)|Deployable Cover|Abri déployable|libre|
|[equipment-01-PIR3RduvFcou4xz6.htm](equipment/equipment-01-PIR3RduvFcou4xz6.htm)|Glittering Scarab|Scarabée scintillant|libre|
|[equipment-01-Q2MhPIDRXgD0K6C4.htm](equipment/equipment-01-Q2MhPIDRXgD0K6C4.htm)|Memoir Map|Carte mémorielle|libre|
|[equipment-01-SBQRbWKjGNkNJeG5.htm](equipment/equipment-01-SBQRbWKjGNkNJeG5.htm)|Poison Ring|Anneau à poison|libre|
|[equipment-01-STF54Mk8rE7uSMbv.htm](equipment/equipment-01-STF54Mk8rE7uSMbv.htm)|Waffle Iron (Imprint)|Empreinte de gaufrier en fer|libre|
|[equipment-01-thWEk8pHq1hqyhMn.htm](equipment/equipment-01-thWEk8pHq1hqyhMn.htm)|Communication Bangle|Bracelet de communication|libre|
|[equipment-01-touk9g0Lc2REwowo.htm](equipment/equipment-01-touk9g0Lc2REwowo.htm)|Weapon Siphon|Siphon d'arme|libre|
|[equipment-01-tSAL09zFgPtlK4Dn.htm](equipment/equipment-01-tSAL09zFgPtlK4Dn.htm)|Eyecatcher|Tape-à-l'oeil|libre|
|[equipment-01-TyWs8hIOYxjcm037.htm](equipment/equipment-01-TyWs8hIOYxjcm037.htm)|Grappling Gun (Clockwork)|Pistolet à grappin mécanique|libre|
|[equipment-01-UwGcNJS5jjjUssPb.htm](equipment/equipment-01-UwGcNJS5jjjUssPb.htm)|Candlecap|Coiffe chandelle|libre|
|[equipment-01-Uz5K5773R3DB3b3o.htm](equipment/equipment-01-Uz5K5773R3DB3b3o.htm)|Mask (Rubber)|Masque en caoutchouc|libre|
|[equipment-01-VKHKaCMRF30GWvZ0.htm](equipment/equipment-01-VKHKaCMRF30GWvZ0.htm)|Twining Chains|Chaînes torsadées|libre|
|[equipment-01-vqHSVgQJktWTXiDK.htm](equipment/equipment-01-vqHSVgQJktWTXiDK.htm)|Versatile Tinderbox|Boite à amadou polyvalente|libre|
|[equipment-01-WKT5mW2EccS22989.htm](equipment/equipment-01-WKT5mW2EccS22989.htm)|Guide Harness|Harnais de guidage|libre|
|[equipment-01-wTYxAWrdsQ6SLVr9.htm](equipment/equipment-01-wTYxAWrdsQ6SLVr9.htm)|Genealogy Mask|Masque généalogique|libre|
|[equipment-01-XLuRWK7QluA2NUNn.htm](equipment/equipment-01-XLuRWK7QluA2NUNn.htm)|Sibling's Coin|Pièce de la fraternité|libre|
|[equipment-01-ydpA0pz0pPNqIUXU.htm](equipment/equipment-01-ydpA0pz0pPNqIUXU.htm)|Throwing Shield|Bouclier de jet|libre|
|[equipment-01-yvS4UatIZAGH4tng.htm](equipment/equipment-01-yvS4UatIZAGH4tng.htm)|Ganjay Book (Light bulk book)|Livre de Ganjay (encombrement léger)|libre|
|[equipment-01-yzc8Sll1YMWq8PpR.htm](equipment/equipment-01-yzc8Sll1YMWq8PpR.htm)|Ring of Sigils|Anneau de symboles|libre|
|[equipment-01-ZDoB4t7P2OBMRwvh.htm](equipment/equipment-01-ZDoB4t7P2OBMRwvh.htm)|Purifying Spoon (Tablespoon)|Cuillère purificatrice (cuillère)|libre|
|[equipment-01-ZIQAzOavTXJCcCMD.htm](equipment/equipment-01-ZIQAzOavTXJCcCMD.htm)|Predictable Silver Piece|Pièce d'argent prévisible|libre|
|[equipment-01-Zmz2M2u7mGOCbUf4.htm](equipment/equipment-01-Zmz2M2u7mGOCbUf4.htm)|Walking Cauldron|Chaudron baladeur|libre|
|[equipment-02-4kKFzTy2Rlwg2xJG.htm](equipment/equipment-02-4kKFzTy2Rlwg2xJG.htm)|Legerdemain Handkerchief|Mouchoir de passe-passe|libre|
|[equipment-02-599vQVSFAKMzvVEt.htm](equipment/equipment-02-599vQVSFAKMzvVEt.htm)|Skittering Mask|Masque fuyant|libre|
|[equipment-02-5TX0wX3VtMqriZSu.htm](equipment/equipment-02-5TX0wX3VtMqriZSu.htm)|Quick-Change Outfit|Tenue de changement rapide|libre|
|[equipment-02-85lwgBrZh0cB3PX4.htm](equipment/equipment-02-85lwgBrZh0cB3PX4.htm)|Day Goggles|Lunettes de jour|libre|
|[equipment-02-9ajoltiJ2T60MLAh.htm](equipment/equipment-02-9ajoltiJ2T60MLAh.htm)|Soulspeaker|Diseur d'âme|libre|
|[equipment-02-9dKzjpAQiE48AIWD.htm](equipment/equipment-02-9dKzjpAQiE48AIWD.htm)|Empathy Charm|Breloque d'empathie|libre|
|[equipment-02-9uhJ7OB65ZTdUc8b.htm](equipment/equipment-02-9uhJ7OB65ZTdUc8b.htm)|Necklace of Knives|Collier de couteaux|libre|
|[equipment-02-aGkmq9k4QIEpLFs7.htm](equipment/equipment-02-aGkmq9k4QIEpLFs7.htm)|Hat of Disagreeable Disguise|Foulard de déguisement désagréable|libre|
|[equipment-02-AslqTIjbP81m7bzk.htm](equipment/equipment-02-AslqTIjbP81m7bzk.htm)|Furnace of Endings (Lesser)|Fournaise des terminaisons inférieure|libre|
|[equipment-02-BANLXq8FhwqsDu0v.htm](equipment/equipment-02-BANLXq8FhwqsDu0v.htm)|Wondrous Figurine (Onyx Dog)|Figurine merveilleuse (Chien d'onyx)|officielle|
|[equipment-02-BJxnnY9ap2wMDnRN.htm](equipment/equipment-02-BJxnnY9ap2wMDnRN.htm)|Bottomless Stein|Chope sans fond|libre|
|[equipment-02-c3dFpmXsIxH2i8ug.htm](equipment/equipment-02-c3dFpmXsIxH2i8ug.htm)|Standard Book of Translation (Tien)|Livre de traduction standard (Tien)|libre|
|[equipment-02-CF2BYEQnAmSwmPPE.htm](equipment/equipment-02-CF2BYEQnAmSwmPPE.htm)|Bewitching Bloom (Lilac)|Fleur envoûtante (Lilas)|libre|
|[equipment-02-cw3onzGfq0Ori5Mn.htm](equipment/equipment-02-cw3onzGfq0Ori5Mn.htm)|Everair Mask (Lesser)|Masque à air continu inférieur|libre|
|[equipment-02-DKWuJb2rSgiotOG7.htm](equipment/equipment-02-DKWuJb2rSgiotOG7.htm)|Weapon Potency (+1)|Puissance d'arme +1 (rune)|officielle|
|[equipment-02-e011UgQLOJtdUqoW.htm](equipment/equipment-02-e011UgQLOJtdUqoW.htm)|Paper Shredder|Déchiqueteuse de papier|libre|
|[equipment-02-e6lhw1SPdq3koAqG.htm](equipment/equipment-02-e6lhw1SPdq3koAqG.htm)|Gelid Shard|Écharde glaciale|libre|
|[equipment-02-EuEH4f3oDUQ4YFZS.htm](equipment/equipment-02-EuEH4f3oDUQ4YFZS.htm)|Stone of Weight|Pierre de lest|officielle|
|[equipment-02-eYemXoYEObmaeOAE.htm](equipment/equipment-02-eYemXoYEObmaeOAE.htm)|Apparition Gloves|Gants d'apparition|libre|
|[equipment-02-fEc4v3EiddH2v0kd.htm](equipment/equipment-02-fEc4v3EiddH2v0kd.htm)|Wrist Grappler|Lance-grappin de poignet|libre|
|[equipment-02-fvpLYx1Lo42cdleQ.htm](equipment/equipment-02-fvpLYx1Lo42cdleQ.htm)|Masquerade Scarf|Foulard de mascarade|libre|
|[equipment-02-gbwr57aT9ou8yKWT.htm](equipment/equipment-02-gbwr57aT9ou8yKWT.htm)|Wayfinder|Guide|libre|
|[equipment-02-gdaLHiWRhB1l2Xr3.htm](equipment/equipment-02-gdaLHiWRhB1l2Xr3.htm)|Alchemist's Flamethrower|Lance-flammes d'alchimiste|libre|
|[equipment-02-GLr0U1yY4hSHBApa.htm](equipment/equipment-02-GLr0U1yY4hSHBApa.htm)|Goz Mask|Masque goz|libre|
|[equipment-02-hHOi8jV85CN8WRJT.htm](equipment/equipment-02-hHOi8jV85CN8WRJT.htm)|Parade Armor|Armure de parade|libre|
|[equipment-02-hyMbJb2MRF5beIaU.htm](equipment/equipment-02-hyMbJb2MRF5beIaU.htm)|Archaic Wayfinder|Guide archaïque|officielle|
|[equipment-02-iX25AmfeRbV3M1EF.htm](equipment/equipment-02-iX25AmfeRbV3M1EF.htm)|Steadyfoot Tassel|Pampille pied sûr|libre|
|[equipment-02-J3KhiuihoyDjshWr.htm](equipment/equipment-02-J3KhiuihoyDjshWr.htm)|Purifying Spoon (Ladle)|Cuillère purificatrice (louche)|libre|
|[equipment-02-mmQT6AZjXO1ty3dv.htm](equipment/equipment-02-mmQT6AZjXO1ty3dv.htm)|Stone of Encouragement|Pierre d'encouragement|libre|
|[equipment-02-n8nmqJdfHrDfuG1J.htm](equipment/equipment-02-n8nmqJdfHrDfuG1J.htm)|Standard Astrolabe|Astrolabe standard|libre|
|[equipment-02-ocqIWF5PeU9BxNlM.htm](equipment/equipment-02-ocqIWF5PeU9BxNlM.htm)|Experimental Clothing|Vêtements expérimentaux|libre|
|[equipment-02-OdBdmv0Y1P7wQWTo.htm](equipment/equipment-02-OdBdmv0Y1P7wQWTo.htm)|Holy Steam Ball|Bille de vapeur sacrée|libre|
|[equipment-02-ONwLm7P7ICUtVN9V.htm](equipment/equipment-02-ONwLm7P7ICUtVN9V.htm)|Pocket Watch|Montre de poche|libre|
|[equipment-02-ozOVlLALtDChFJwc.htm](equipment/equipment-02-ozOVlLALtDChFJwc.htm)|Deployable Cover (Ballistic Cover)|Abri déployable ballistique|libre|
|[equipment-02-pcGdJvwun0tjrUTz.htm](equipment/equipment-02-pcGdJvwun0tjrUTz.htm)|Fanged|Crocs (rune)|libre|
|[equipment-02-pHeqYb5gTSUSmW0j.htm](equipment/equipment-02-pHeqYb5gTSUSmW0j.htm)|Dweomerweave Robe|Robe de tissu enchanté|libre|
|[equipment-02-PSKFnWMq5daOJa1R.htm](equipment/equipment-02-PSKFnWMq5daOJa1R.htm)|Ancestral Geometry|Tatouage Géométrique ancestral|libre|
|[equipment-02-QgSPvZupjCsWhqAp.htm](equipment/equipment-02-QgSPvZupjCsWhqAp.htm)|Ursine Avenger Hood|Capuche du vengeur ursin|libre|
|[equipment-02-QOAjwtH9gycbDZCY.htm](equipment/equipment-02-QOAjwtH9gycbDZCY.htm)|Jack's Tattered Cape|Cape en loques de Jack|libre|
|[equipment-02-r1hgg2rweqGL1LBl.htm](equipment/equipment-02-r1hgg2rweqGL1LBl.htm)|Hand of the Mage|Main du mage|officielle|
|[equipment-02-SHhSOqzt89GRk9kL.htm](equipment/equipment-02-SHhSOqzt89GRk9kL.htm)|Toy Carriage (Windup)|Chariot-jouet à remontoir|libre|
|[equipment-02-sZxPKnLtspXPRDNb.htm](equipment/equipment-02-sZxPKnLtspXPRDNb.htm)|Pathfinder's Coin|Pièce de l'éclaireur|libre|
|[equipment-02-T99WI1hocCXE7RI0.htm](equipment/equipment-02-T99WI1hocCXE7RI0.htm)|Doll (Exquisite surprise)|Poupée jouet surprise exquise|libre|
|[equipment-02-teSZWABjhXM4EKXM.htm](equipment/equipment-02-teSZWABjhXM4EKXM.htm)|Quick Wig|Perruque rapide|libre|
|[equipment-02-tlPwOqfjJxQQsgoc.htm](equipment/equipment-02-tlPwOqfjJxQQsgoc.htm)|Frostwalker Pattern|Motif de marcheur du givre|libre|
|[equipment-02-TmQalYKNNRuEdoTh.htm](equipment/equipment-02-TmQalYKNNRuEdoTh.htm)|Brooch of Shielding|Broche de défense|officielle|
|[equipment-02-u4mB03FhvuJ7DcX8.htm](equipment/equipment-02-u4mB03FhvuJ7DcX8.htm)|St. Alkitarem's Eye|Oeil de Saint Alkitarem|libre|
|[equipment-02-VpoBYfVUEA8wtQAb.htm](equipment/equipment-02-VpoBYfVUEA8wtQAb.htm)|Flask of Fellowship|Gourde de fraternité|libre|
|[equipment-02-XQBkuELucpI0c7XW.htm](equipment/equipment-02-XQBkuELucpI0c7XW.htm)|Floating Tent (Four-Person)|Tente flottante pour 4 personnes|libre|
|[equipment-02-xRQpaV9fCM7ptmVB.htm](equipment/equipment-02-xRQpaV9fCM7ptmVB.htm)|Luckless Dice|Dés malchanceux|libre|
|[equipment-02-xyzmQa3nhU8HxfUL.htm](equipment/equipment-02-xyzmQa3nhU8HxfUL.htm)|Tent (Pavilion)|Tente pour 4 personnes|libre|
|[equipment-02-YmTGzsenhogSNDXK.htm](equipment/equipment-02-YmTGzsenhogSNDXK.htm)|Pyrite Rat|Rat en pyrite|libre|
|[equipment-02-YZxq8rGPiLPIKWIQ.htm](equipment/equipment-02-YZxq8rGPiLPIKWIQ.htm)|Writ of Authenticity|Acte d'authenticité|libre|
|[equipment-02-ZIs2lAIDasxshMCf.htm](equipment/equipment-02-ZIs2lAIDasxshMCf.htm)|Triangular Teeth|Dents triangulaires|libre|
|[equipment-02-zSILrZ6pYWbWUm2D.htm](equipment/equipment-02-zSILrZ6pYWbWUm2D.htm)|Gunner's Saddle|Selle du tireur|libre|
|[equipment-02-ZZnh7BkwApPyNyqc.htm](equipment/equipment-02-ZZnh7BkwApPyNyqc.htm)|Periscope|Périscope|libre|
|[equipment-03-0gxqSZhYdOeJhPrp.htm](equipment/equipment-03-0gxqSZhYdOeJhPrp.htm)|Jolt Coil|Bobine d'électrochoc|libre|
|[equipment-03-0QgniSjpzksm5riV.htm](equipment/equipment-03-0QgniSjpzksm5riV.htm)|Artisan's Toolkit (Sterling)|Matériel d'artisan de précision|libre|
|[equipment-03-0yLfPmzRtBd6Avac.htm](equipment/equipment-03-0yLfPmzRtBd6Avac.htm)|Corpse Compass|Boussole à cadavre|libre|
|[equipment-03-15nkBxrvIrbGQCdS.htm](equipment/equipment-03-15nkBxrvIrbGQCdS.htm)|Clockwork Box Packer|Emballeur mécanique|libre|
|[equipment-03-1M4SSDuTv2SwBltC.htm](equipment/equipment-03-1M4SSDuTv2SwBltC.htm)|Survey Map (Atlas)|Carte topographique (Atlas)|libre|
|[equipment-03-1r6StS0irdvi5JHY.htm](equipment/equipment-03-1r6StS0irdvi5JHY.htm)|Ventriloquist's Ring|Anneau de ventriloquie|libre|
|[equipment-03-1uwONfUHwHjRJp6o.htm](equipment/equipment-03-1uwONfUHwHjRJp6o.htm)|Puzzle Box (Challenging) (Hollow)|Boite à énigmes creuse exigeante|libre|
|[equipment-03-2EOeljZiUdNVf8s2.htm](equipment/equipment-03-2EOeljZiUdNVf8s2.htm)|Bullhook|Crochet de taureau|libre|
|[equipment-03-2KnSYPFGKz4MDeiW.htm](equipment/equipment-03-2KnSYPFGKz4MDeiW.htm)|Tanglefoot Extruder|Extrudeuse de bombe collante|libre|
|[equipment-03-2NKC67OBIukM4tC5.htm](equipment/equipment-03-2NKC67OBIukM4tC5.htm)|Wig of Holding|Perruque de contenance|libre|
|[equipment-03-2pzrpfYE9AHMAX9H.htm](equipment/equipment-03-2pzrpfYE9AHMAX9H.htm)|Hongrui's Gratitude|Gratitude de Hongrui|libre|
|[equipment-03-3fHdvyWq5A9jvT9W.htm](equipment/equipment-03-3fHdvyWq5A9jvT9W.htm)|Concealed Holster|Holster dissimulé|libre|
|[equipment-03-3ld14dsn2RLu9owg.htm](equipment/equipment-03-3ld14dsn2RLu9owg.htm)|Musical Instrument (Virtuoso handheld)|Instruments de musique (Virtuose portatif)|libre|
|[equipment-03-3R13WDcHOnc5pzfm.htm](equipment/equipment-03-3R13WDcHOnc5pzfm.htm)|Aerial Cloak|Cape aérienne|libre|
|[equipment-03-3vxoffA4slKHXtj2.htm](equipment/equipment-03-3vxoffA4slKHXtj2.htm)|Channel Protection Amulet|Amulette de protection canalisée|officielle|
|[equipment-03-4A8SFipG78SMWQEU.htm](equipment/equipment-03-4A8SFipG78SMWQEU.htm)|Aeon Stone (Pearly White Spindle)|Pierre d'éternité (fuseau blanc perle)|libre|
|[equipment-03-4kz3vhkKPUuXBpxk.htm](equipment/equipment-03-4kz3vhkKPUuXBpxk.htm)|Crowbar (Levered)|Pied-de-biche à levier|libre|
|[equipment-03-4OqBL6zYcGU7ZsYw.htm](equipment/equipment-03-4OqBL6zYcGU7ZsYw.htm)|Snagging|Accroche|libre|
|[equipment-03-5GbC7RTgyAeaOcAI.htm](equipment/equipment-03-5GbC7RTgyAeaOcAI.htm)|Magnifying Glass|Loupe|libre|
|[equipment-03-5KHp3903LnOcHp8k.htm](equipment/equipment-03-5KHp3903LnOcHp8k.htm)|Hoax-Hunter's Kit|Kit de chasseur de rumeur|libre|
|[equipment-03-5QKAoWrpSetjHVJs.htm](equipment/equipment-03-5QKAoWrpSetjHVJs.htm)|Underwater|Sous-marine (rune)|libre|
|[equipment-03-5rkGXSbaqBbY4MiR.htm](equipment/equipment-03-5rkGXSbaqBbY4MiR.htm)|Scholarly Journal Compendium|Recueil de journal d'érudit|libre|
|[equipment-03-6nrCxNQFycUVFOV2.htm](equipment/equipment-03-6nrCxNQFycUVFOV2.htm)|Thieves' Toolkit (Infiltrator)|Matériel de voleur d'infiltration|libre|
|[equipment-03-7vwcuBIe4BNS5uuE.htm](equipment/equipment-03-7vwcuBIe4BNS5uuE.htm)|Kin-Warding|Protection des proches|libre|
|[equipment-03-8kv4fmpE1geu4bFx.htm](equipment/equipment-03-8kv4fmpE1geu4bFx.htm)|Battle Medic's Baton|Bâton du médecin de guerre|libre|
|[equipment-03-9k7X59AB8yDlIJ4s.htm](equipment/equipment-03-9k7X59AB8yDlIJ4s.htm)|Wardrobe Stone (Lesser)|Pierre de garde-robe inférieur|libre|
|[equipment-03-ACa9QlFqdmW4s2Th.htm](equipment/equipment-03-ACa9QlFqdmW4s2Th.htm)|Hag Eye|Oeil de guenaude|libre|
|[equipment-03-aDsdYMPpVc8hOnM5.htm](equipment/equipment-03-aDsdYMPpVc8hOnM5.htm)|Repair Toolkit (Superb)|Matériel de réparation superbe|libre|
|[equipment-03-bAfyWCvgsYDyw3ff.htm](equipment/equipment-03-bAfyWCvgsYDyw3ff.htm)|Maestro's Instrument (Lesser)|Instrument du maestro inférieur|libre|
|[equipment-03-bbSc1VU1LiQqReKd.htm](equipment/equipment-03-bbSc1VU1LiQqReKd.htm)|Pathfinder Chronicle|Chronique de l'éclaireur|libre|
|[equipment-03-BhSQej6dIZh6wkWC.htm](equipment/equipment-03-BhSQej6dIZh6wkWC.htm)|Varisian Emblem (Vangloris)|Emblème varisien (Vangloris)|libre|
|[equipment-03-BKdzb8hu3kZtKH3Z.htm](equipment/equipment-03-BKdzb8hu3kZtKH3Z.htm)|Bracelet of Dashing|Bracelet de fougue|libre|
|[equipment-03-bw2RNhvtX1vvHi0y.htm](equipment/equipment-03-bw2RNhvtX1vvHi0y.htm)|Varisian Emblem (Ragario)|Emblème varisien (Ragario)|libre|
|[equipment-03-BznmPaRjI4Orb0IH.htm](equipment/equipment-03-BznmPaRjI4Orb0IH.htm)|Thieves' Toolkit (Infiltrator Picks)|Crochets de remplacement d'infiltration|libre|
|[equipment-03-c9GaJKCxxqXKZokZ.htm](equipment/equipment-03-c9GaJKCxxqXKZokZ.htm)|Broken Tusk Pendant|Pendentif de la défense brisée|libre|
|[equipment-03-CDuN9kzyxBZ4cShq.htm](equipment/equipment-03-CDuN9kzyxBZ4cShq.htm)|Golden Legion Epaulet|Épaulette de la Légion dorée|officielle|
|[equipment-03-dmsdzOxdykeWXUHr.htm](equipment/equipment-03-dmsdzOxdykeWXUHr.htm)|Rime Crystal|Cristal gelé|libre|
|[equipment-03-duiTS3Y9JJUsJByq.htm](equipment/equipment-03-duiTS3Y9JJUsJByq.htm)|Bewitching Bloom (Cherry Blossom)|Fleur envoûtante (Fleur de cerisier)|libre|
|[equipment-03-DwMXEqy7Ws8NYQQh.htm](equipment/equipment-03-DwMXEqy7Ws8NYQQh.htm)|Doubling Rings|Anneaux de doublement|officielle|
|[equipment-03-Epc1e1Q9M9bcwOR0.htm](equipment/equipment-03-Epc1e1Q9M9bcwOR0.htm)|Dancing Scarf|Écharpe dansante|libre|
|[equipment-03-eqnm5vYNmDl37V5S.htm](equipment/equipment-03-eqnm5vYNmDl37V5S.htm)|Soft-Landing|Atterrissage en douceur|libre|
|[equipment-03-eTkMiedmXpSsnaod.htm](equipment/equipment-03-eTkMiedmXpSsnaod.htm)|Smoke Veil|Voile de fumée|libre|
|[equipment-03-eurAnvH8bK0ZctOR.htm](equipment/equipment-03-eurAnvH8bK0ZctOR.htm)|Bracers of Missile Deflection|Protège-bras de déviation de projectiles|libre|
|[equipment-03-F4jOdM3qhYELTz1I.htm](equipment/equipment-03-F4jOdM3qhYELTz1I.htm)|Spider Lily Tattoo|Tatouage de lys arachnéen|libre|
|[equipment-03-f6xV2LRekhoYJVwD.htm](equipment/equipment-03-f6xV2LRekhoYJVwD.htm)|Familiar Tattoo|Tatouage de familier|libre|
|[equipment-03-FkII0xws6an5vFSS.htm](equipment/equipment-03-FkII0xws6an5vFSS.htm)|Flaming Star|Étoile enflammée|libre|
|[equipment-03-Fq61XZxfgsn4ZDXf.htm](equipment/equipment-03-Fq61XZxfgsn4ZDXf.htm)|Manacles of Persuasion|Menottes de persuasion|libre|
|[equipment-03-fr2K2wJnSLD0ERpo.htm](equipment/equipment-03-fr2K2wJnSLD0ERpo.htm)|Unbreakable Heart|Coeur incassable|libre|
|[equipment-03-G4cmHHhAChAlypFN.htm](equipment/equipment-03-G4cmHHhAChAlypFN.htm)|Rope of Climbing (Lesser)|Corde d'escalade inférieure|libre|
|[equipment-03-gDfBzU6umebo5RXP.htm](equipment/equipment-03-gDfBzU6umebo5RXP.htm)|Palm Crossbow|Arbalète de paume|libre|
|[equipment-03-GGsECHQJ3RrnWXhV.htm](equipment/equipment-03-GGsECHQJ3RrnWXhV.htm)|Sisterstone Chunk|Morceau de gémellithe|libre|
|[equipment-03-Gh1FVqiP7uaSwCYz.htm](equipment/equipment-03-Gh1FVqiP7uaSwCYz.htm)|Preserving|Conservatrice (rune)|libre|
|[equipment-03-gScuJzOR6B0D5sHV.htm](equipment/equipment-03-gScuJzOR6B0D5sHV.htm)|Secret-Keeper's Mask (Reaper of Reputation)|Masque des gardiens des secrets (Faucheur de réputation)|libre|
|[equipment-03-gSf5aYA9D8MSolc9.htm](equipment/equipment-03-gSf5aYA9D8MSolc9.htm)|Confabulator|Confabulateur|libre|
|[equipment-03-hywANJCzT7hMgWna.htm](equipment/equipment-03-hywANJCzT7hMgWna.htm)|Scholarly Journal|Journal d'érudit|libre|
|[equipment-03-ioiMUDqv85BI4shY.htm](equipment/equipment-03-ioiMUDqv85BI4shY.htm)|Gate Attenuator|Atténuateur de portail|libre|
|[equipment-03-IRqbsE8MgGLTfHLz.htm](equipment/equipment-03-IRqbsE8MgGLTfHLz.htm)|Disguise Kit (Elite Cosmetics)|Kit de déguisement d'élite (Stock de cosmétiques)|libre|
|[equipment-03-ItLZL9Bd6xwgfeB8.htm](equipment/equipment-03-ItLZL9Bd6xwgfeB8.htm)|Detective's Kit|Kit du détective|libre|
|[equipment-03-JhSCwXhC3tLhI347.htm](equipment/equipment-03-JhSCwXhC3tLhI347.htm)|Varisian Emblem (Carnasia)|Emblème varisien (Carnasia)|libre|
|[equipment-03-JY8X4RSfg6xIqAC9.htm](equipment/equipment-03-JY8X4RSfg6xIqAC9.htm)|Crushing|Écrasante (rune)|libre|
|[equipment-03-k0AC0UxLKO4r2rwQ.htm](equipment/equipment-03-k0AC0UxLKO4r2rwQ.htm)|Presentable|Présentable (rune)|libre|
|[equipment-03-KeespXczBEt1ci26.htm](equipment/equipment-03-KeespXczBEt1ci26.htm)|Lady's Chalice|Calice de la Dame|libre|
|[equipment-03-kRBYoE6j2QUXQJGS.htm](equipment/equipment-03-kRBYoE6j2QUXQJGS.htm)|Wrist Grappler (Clockwork)|Lance-grappin de poignet mécanique|libre|
|[equipment-03-KSkIKaKM3n75BpUL.htm](equipment/equipment-03-KSkIKaKM3n75BpUL.htm)|Gaffe Glasses|Lunettes du gaffeur|libre|
|[equipment-03-ksorBjuXO0Bvdmhl.htm](equipment/equipment-03-ksorBjuXO0Bvdmhl.htm)|Fishing Tackle (Professional)|Matériel de pêche (professionnel)|officielle|
|[equipment-03-lasRdOMO1w9IMdwx.htm](equipment/equipment-03-lasRdOMO1w9IMdwx.htm)|Unexceptional|Non exceptionnelle (rune)|libre|
|[equipment-03-laU7xnX42wXch2Dv.htm](equipment/equipment-03-laU7xnX42wXch2Dv.htm)|Concealed Sheath|Fourreau dissimulé|libre|
|[equipment-03-liGtU10aUlVeI6IC.htm](equipment/equipment-03-liGtU10aUlVeI6IC.htm)|Menacing|Menaçante (rune)|libre|
|[equipment-03-LKdgj4UVmOvUwkZu.htm](equipment/equipment-03-LKdgj4UVmOvUwkZu.htm)|Charlatan's Gloves|Gants du prestidigitateur|libre|
|[equipment-03-Ln0gcJPFHU9sQ7Jd.htm](equipment/equipment-03-Ln0gcJPFHU9sQ7Jd.htm)|Diving Suit|Scaphandre de plongée|libre|
|[equipment-03-lN3Fn9AuNcKbXucJ.htm](equipment/equipment-03-lN3Fn9AuNcKbXucJ.htm)|Trinity Geode|Géode de la trinité|libre|
|[equipment-03-lu42Up1309MqFya4.htm](equipment/equipment-03-lu42Up1309MqFya4.htm)|Pickled Demon Tongue|Langue de démon marinée|libre|
|[equipment-03-lWeADBqMFwFlVIuV.htm](equipment/equipment-03-lWeADBqMFwFlVIuV.htm)|Manacles (Average)|Menottes intermédiaires|officielle|
|[equipment-03-maHPPEwKK2NxbMoV.htm](equipment/equipment-03-maHPPEwKK2NxbMoV.htm)|Raven Band|Brassard du corbeau|libre|
|[equipment-03-mcKF8McrMlp01wUP.htm](equipment/equipment-03-mcKF8McrMlp01wUP.htm)|Wrenchgear|Clé à molette|libre|
|[equipment-03-MKupH1T018JubYJW.htm](equipment/equipment-03-MKupH1T018JubYJW.htm)|Persona Mask|Masque de représentation|libre|
|[equipment-03-mvMeloQxSiEGIlhL.htm](equipment/equipment-03-mvMeloQxSiEGIlhL.htm)|Coyote Cloak|Cape de coyote|officielle|
|[equipment-03-naxISKMwhLQ5F9yS.htm](equipment/equipment-03-naxISKMwhLQ5F9yS.htm)|Puzzle Box (Challenging)|Boite à énigmes exigeante|libre|
|[equipment-03-nI4JorlalCpzsJ07.htm](equipment/equipment-03-nI4JorlalCpzsJ07.htm)|Sisterstone Ingot|Lingot de gémellithe|libre|
|[equipment-03-njHnUB4bwW26slzD.htm](equipment/equipment-03-njHnUB4bwW26slzD.htm)|Electrocable|Électrocâble|libre|
|[equipment-03-NJRACLvg6ULRMtZB.htm](equipment/equipment-03-NJRACLvg6ULRMtZB.htm)|Varisian Emblem (Voratalo)|Emblème varisien (Voratalo)|libre|
|[equipment-03-O1W40RBvlZW069Mw.htm](equipment/equipment-03-O1W40RBvlZW069Mw.htm)|Mariner's Astrolabe|Astrolabe de marin|libre|
|[equipment-03-OjOpj9u1EdEUhKuX.htm](equipment/equipment-03-OjOpj9u1EdEUhKuX.htm)|Grim Sandglass|Sablier sinistre|libre|
|[equipment-03-OoDp1T4IUUsNnrV6.htm](equipment/equipment-03-OoDp1T4IUUsNnrV6.htm)|Perfect Droplet|Goutte parfaite|libre|
|[equipment-03-Ouou5j07pm2MysXI.htm](equipment/equipment-03-Ouou5j07pm2MysXI.htm)|Glasses of Sociability|Lunettes sociables|libre|
|[equipment-03-P2DjThbSOWwra49r.htm](equipment/equipment-03-P2DjThbSOWwra49r.htm)|Snare Kit (Specialist)|Nécessaire de fabrication de pièges artisanaux spécialisé|libre|
|[equipment-03-P8hf8WT6oRxYmfLB.htm](equipment/equipment-03-P8hf8WT6oRxYmfLB.htm)|Blazons of Shared Power|Blasons du pouvoir partagé|libre|
|[equipment-03-P8xfjaEsrcKxl7k0.htm](equipment/equipment-03-P8xfjaEsrcKxl7k0.htm)|Everyneed Pack|Sac de tous les besoins|libre|
|[equipment-03-PCQtpTKiGfSSRwNV.htm](equipment/equipment-03-PCQtpTKiGfSSRwNV.htm)|Vestige Lenses|Lentilles de vestiges|libre|
|[equipment-03-pkD8SLooxP55tibr.htm](equipment/equipment-03-pkD8SLooxP55tibr.htm)|Codebreaker's Parchment|Parchemin de codage|libre|
|[equipment-03-PlB4I1LFOpRYwvEg.htm](equipment/equipment-03-PlB4I1LFOpRYwvEg.htm)|Aeon Stone (Polished Pebble)|Pierre d'éternité (galet poli)|libre|
|[equipment-03-PO0n5F4j8Pa0LB0Q.htm](equipment/equipment-03-PO0n5F4j8Pa0LB0Q.htm)|Portable Altar|Autel portatif|libre|
|[equipment-03-q2TYVAMaK6UfenbV.htm](equipment/equipment-03-q2TYVAMaK6UfenbV.htm)|Lock (Average)|Serrure intermédiaire|officielle|
|[equipment-03-QHc7AnKoMpcqsI2d.htm](equipment/equipment-03-QHc7AnKoMpcqsI2d.htm)|Called|Appelé (rune)|libre|
|[equipment-03-QlFJyxBTYFSN2EA7.htm](equipment/equipment-03-QlFJyxBTYFSN2EA7.htm)|Fingerprint Kit|Kit d'empreintes|libre|
|[equipment-03-qlunQzfnzPQpMG6U.htm](equipment/equipment-03-qlunQzfnzPQpMG6U.htm)|Returning|Boomerang (rune)|officielle|
|[equipment-03-qNMOpzpeQz51vMhf.htm](equipment/equipment-03-qNMOpzpeQz51vMhf.htm)|Varisian Emblem (Idolis)|Emblème varisien (Idolis)|libre|
|[equipment-03-QSeDIyIr2A7PNy6u.htm](equipment/equipment-03-QSeDIyIr2A7PNy6u.htm)|Coin of Comfort|Pièce d'argent de confort|libre|
|[equipment-03-QuPnoGmlgYVNXeTz.htm](equipment/equipment-03-QuPnoGmlgYVNXeTz.htm)|Extendable Tail|Queue extensible|libre|
|[equipment-03-R09AZzvyNA3Jginm.htm](equipment/equipment-03-R09AZzvyNA3Jginm.htm)|Abadar's Flawless Scale|Balance sans défaut d'Abadar|libre|
|[equipment-03-RF12ziGVG9YKGaqU.htm](equipment/equipment-03-RF12ziGVG9YKGaqU.htm)|Secret-Keeper's Mask (Blackfingers)|Masque des gardiens des secrets (Doigts noirs)|libre|
|[equipment-03-RgNBGpBc9G2yw1C2.htm](equipment/equipment-03-RgNBGpBc9G2yw1C2.htm)|Crafter's Eyepiece|Oculaire d'artisan|libre|
|[equipment-03-Rup7OOQ8aQi3sdZr.htm](equipment/equipment-03-Rup7OOQ8aQi3sdZr.htm)|Magnet Coin|Pièce magnétique|libre|
|[equipment-03-RxBVbQEd4DArkpKl.htm](equipment/equipment-03-RxBVbQEd4DArkpKl.htm)|Cloak of Gnawing Leaves|Cape de feuilles rogneuses|libre|
|[equipment-03-rXZxM7SbqEnvXyal.htm](equipment/equipment-03-rXZxM7SbqEnvXyal.htm)|Clothing (High-Fashion Fine)|Vêtements de haute couture|libre|
|[equipment-03-SECMe8QKVl45qUie.htm](equipment/equipment-03-SECMe8QKVl45qUie.htm)|Parchment of Direct Message|Parchemin de message direct|libre|
|[equipment-03-sG5JfXmH9wqmqW3w.htm](equipment/equipment-03-sG5JfXmH9wqmqW3w.htm)|Enhanced Hearing Aids|Appareil auditif amélioré|libre|
|[equipment-03-sgH9U2fKqWCedKNO.htm](equipment/equipment-03-sgH9U2fKqWCedKNO.htm)|Skinsaw Mask|Masque de l'écorcheur|libre|
|[equipment-03-SGkOHFyBbzWdBk8D.htm](equipment/equipment-03-SGkOHFyBbzWdBk8D.htm)|Healer's Toolkit (Expanded)|Matériel de guérisseur étendu|libre|
|[equipment-03-spqcRLBsMOC9WTcd.htm](equipment/equipment-03-spqcRLBsMOC9WTcd.htm)|Keymaking Tools|Outils du serrurier|libre|
|[equipment-03-SswqJqeAWGtX3tTF.htm](equipment/equipment-03-SswqJqeAWGtX3tTF.htm)|Mage's Hat|Couvre-chef de thaumaturge|libre|
|[equipment-03-td9LuqG8jhQQloG7.htm](equipment/equipment-03-td9LuqG8jhQQloG7.htm)|Sluggish Bracelet|Bracelet léthargique|libre|
|[equipment-03-tEXUCp02ylyoJoyP.htm](equipment/equipment-03-tEXUCp02ylyoJoyP.htm)|Authorized|Autorisée (rune)|libre|
|[equipment-03-TKr8IL5F3cxsfHPH.htm](equipment/equipment-03-TKr8IL5F3cxsfHPH.htm)|Aeon Stone (Dusty Rose Prism)|Pierre d'éternité (prisme vieux rose)|libre|
|[equipment-03-tqgCxayEMsUxb1PV.htm](equipment/equipment-03-tqgCxayEMsUxb1PV.htm)|Enveloping Light|Lumière enveloppante|libre|
|[equipment-03-tUAfKD2iIrQxHYWp.htm](equipment/equipment-03-tUAfKD2iIrQxHYWp.htm)|Ring of Torag|Anneau de Torag|libre|
|[equipment-03-TWv2EeDJaCjtmGUN.htm](equipment/equipment-03-TWv2EeDJaCjtmGUN.htm)|Climbing Kit (Extreme)|Kit d'escalade (extrême)|libre|
|[equipment-03-u6BZFzBYUnCmfnRr.htm](equipment/equipment-03-u6BZFzBYUnCmfnRr.htm)|Armory Bracelet (Minor)|Bracelet arsenal mineur|libre|
|[equipment-03-U7UVGM93EaNc6lzY.htm](equipment/equipment-03-U7UVGM93EaNc6lzY.htm)|Stalk Goggles (Greater)|Lunettes à tiges supérieures|libre|
|[equipment-03-Uc88bGfysDAeaAD8.htm](equipment/equipment-03-Uc88bGfysDAeaAD8.htm)|Backfire Mantle|Manteau de contre-feu|libre|
|[equipment-03-UhcRWtnjU2WLSClx.htm](equipment/equipment-03-UhcRWtnjU2WLSClx.htm)|Survey Map|Carte topographique|libre|
|[equipment-03-uhka9LHEP3wDKytG.htm](equipment/equipment-03-uhka9LHEP3wDKytG.htm)|Alchemist's Lab (Expanded)|Laboratoire d'alchimie complet|libre|
|[equipment-03-uTELMvkoRORmr5pm.htm](equipment/equipment-03-uTELMvkoRORmr5pm.htm)|Magnifying Scope|Lunette de visée grossissante|libre|
|[equipment-03-uU8bqDR4oWITWZNX.htm](equipment/equipment-03-uU8bqDR4oWITWZNX.htm)|Varisian Emblem (Avidais)|Emblème varisien (Avidais)|libre|
|[equipment-03-UXsHBUyx4b0dIPDf.htm](equipment/equipment-03-UXsHBUyx4b0dIPDf.htm)|Camouflage Suit|Tenue de camouflage|libre|
|[equipment-03-V5oTXRa6Jk8KXyXl.htm](equipment/equipment-03-V5oTXRa6Jk8KXyXl.htm)|Anylength Rope (Lesser)|Corde toute longueur inférieure|libre|
|[equipment-03-vDQSR3fgfYqLp0aB.htm](equipment/equipment-03-vDQSR3fgfYqLp0aB.htm)|Wind-up Wings (Flutterback)|Ailes à manivelle dos-flottant|libre|
|[equipment-03-vGQ7jbZvYpsEO28r.htm](equipment/equipment-03-vGQ7jbZvYpsEO28r.htm)|Scroll Belt|Ceinture parchemin|libre|
|[equipment-03-VLtEtfIggBKXuy7S.htm](equipment/equipment-03-VLtEtfIggBKXuy7S.htm)|Rappelling Kit|Kit de descente en rappel|libre|
|[equipment-03-Vr2h3E86Vaf5DJpZ.htm](equipment/equipment-03-Vr2h3E86Vaf5DJpZ.htm)|Portable Ram|Bélier portable|libre|
|[equipment-03-VUxPhU966hQaIq2C.htm](equipment/equipment-03-VUxPhU966hQaIq2C.htm)|Thrower's Bandolier|Bandoulière du lanceur|libre|
|[equipment-03-wBeljBAAFsvgkRMy.htm](equipment/equipment-03-wBeljBAAFsvgkRMy.htm)|Handcuffs (Average)|Menottes à cliquet intermédiaires|libre|
|[equipment-03-WgXPnTogb3MCueKP.htm](equipment/equipment-03-WgXPnTogb3MCueKP.htm)|Heckling Tools|Outils chahuteurs|libre|
|[equipment-03-WNlkRCPzc8YGQOAp.htm](equipment/equipment-03-WNlkRCPzc8YGQOAp.htm)|Ring of Observation (Lesser)|Anneau d'observation inférieur|libre|
|[equipment-03-xaWuuQoBJiMLzggR.htm](equipment/equipment-03-xaWuuQoBJiMLzggR.htm)|Thurible of Revelation (Lesser)|Encensoir de révélation inférieur|libre|
|[equipment-03-XFUsIDb6RjVFz9Ce.htm](equipment/equipment-03-XFUsIDb6RjVFz9Ce.htm)|One Hundred Victories|Cent victoires|libre|
|[equipment-03-XLGJ1bLhMtP1jc2w.htm](equipment/equipment-03-XLGJ1bLhMtP1jc2w.htm)|Fashionable Wayfinder|Guide d'élégance|libre|
|[equipment-03-XndR8hsJyCsVfZOi.htm](equipment/equipment-03-XndR8hsJyCsVfZOi.htm)|Smoked Goggles|Lunettes fumées|libre|
|[equipment-03-xVhd8NF9KQ6VWfMu.htm](equipment/equipment-03-xVhd8NF9KQ6VWfMu.htm)|Compass (Lensatic)|Boussole à lentilles|libre|
|[equipment-03-y0cRr28w57VapTcn.htm](equipment/equipment-03-y0cRr28w57VapTcn.htm)|Devil's Luck|Chance du Diable (Pacte infernal)|libre|
|[equipment-03-YhkomhtKBK3i9C7Q.htm](equipment/equipment-03-YhkomhtKBK3i9C7Q.htm)|Disguise Kit (Elite)|Kit de déguisement d'élite|libre|
|[equipment-03-YVtXlsEWb3NIkDyy.htm](equipment/equipment-03-YVtXlsEWb3NIkDyy.htm)|Shining Symbol|Symbole lumineux|libre|
|[equipment-03-yw1kPxsdCoDUzOaE.htm](equipment/equipment-03-yw1kPxsdCoDUzOaE.htm)|Musical Instrument (Virtuoso heavy)|Instrument de musique (Virtuose lourd)|libre|
|[equipment-03-Z5eDsW2Lka2cLTVR.htm](equipment/equipment-03-Z5eDsW2Lka2cLTVR.htm)|Mirror Robe|Robe miroir|libre|
|[equipment-03-ZEqAx8jEc6zhX3V1.htm](equipment/equipment-03-ZEqAx8jEc6zhX3V1.htm)|Tracker's Goggles|Lunettes de pisteur|officielle|
|[equipment-03-ZIt62AQb997FatRw.htm](equipment/equipment-03-ZIt62AQb997FatRw.htm)|Knight's Tabard|Tabard des chevaliers|libre|
|[equipment-03-zPhqmCvWyHO8i9ws.htm](equipment/equipment-03-zPhqmCvWyHO8i9ws.htm)|Pendant of the Occult|Pendentif des sciences occultes|officielle|
|[equipment-03-zTR7xbJGFm3oppFA.htm](equipment/equipment-03-zTR7xbJGFm3oppFA.htm)|Varisian Emblem (Avaria)|Emblème varisien (Avaria)|libre|
|[equipment-03-zVXDnx8i0KLf6fkF.htm](equipment/equipment-03-zVXDnx8i0KLf6fkF.htm)|Timepiece (Grand Clock)|Horlogerie (Grande horloge)|libre|
|[equipment-04-2gYZiUw9yjtb0yJY.htm](equipment/equipment-04-2gYZiUw9yjtb0yJY.htm)|Demon Mask|Masque du démon|libre|
|[equipment-04-41MNmuj0PbHnhz0M.htm](equipment/equipment-04-41MNmuj0PbHnhz0M.htm)|Rhythm Bone (Greater)|Os de rythme supérieur|libre|
|[equipment-04-4ObrlZ5GhCPA2E2s.htm](equipment/equipment-04-4ObrlZ5GhCPA2E2s.htm)|Hunter's Brooch|Broche du chasseur|libre|
|[equipment-04-5nqxkc2OlZAPpU4t.htm](equipment/equipment-04-5nqxkc2OlZAPpU4t.htm)|Words of Wisdom (Lesser)|Paroles de sagesse inférieures|libre|
|[equipment-04-5p4ORuCLOKePxUUR.htm](equipment/equipment-04-5p4ORuCLOKePxUUR.htm)|Five-Feather Wreath|Couronne à cinq plumes|libre|
|[equipment-04-5tyQQUdb2GtspEvy.htm](equipment/equipment-04-5tyQQUdb2GtspEvy.htm)|Draxie's Recipe Book|Livre de recettes de Draxie|libre|
|[equipment-04-7JVgLiNTAs4clEW8.htm](equipment/equipment-04-7JVgLiNTAs4clEW8.htm)|Alchemist Goggles|Lunettes d'alchimiste|officielle|
|[equipment-04-7tiAVxbZW8aGmKUO.htm](equipment/equipment-04-7tiAVxbZW8aGmKUO.htm)|Gyroscopic Stabilizer|Stabilisateur gyroscopique|libre|
|[equipment-04-7ZfWiHqDyb6NllN1.htm](equipment/equipment-04-7ZfWiHqDyb6NllN1.htm)|Deck of Mischief|Jeu de cartes espiègle|libre|
|[equipment-04-8qR43lT8rpmlkoKs.htm](equipment/equipment-04-8qR43lT8rpmlkoKs.htm)|Oracular Hag Eye|Oeil de guenaude oraculaire|libre|
|[equipment-04-9MT4uBht72VerwfR.htm](equipment/equipment-04-9MT4uBht72VerwfR.htm)|Extraction Cauldron|Chaudron d'extraction|libre|
|[equipment-04-9zdm3EyEQXgMox8b.htm](equipment/equipment-04-9zdm3EyEQXgMox8b.htm)|Divine Scroll Case of Simplicity|Étui à parchemin de simplicité divine|libre|
|[equipment-04-AhvjU4QbinWPM9t3.htm](equipment/equipment-04-AhvjU4QbinWPM9t3.htm)|Belt of Good Health|Ceinture de bonne santé|libre|
|[equipment-04-AL46eDfKdAnXKQPV.htm](equipment/equipment-04-AL46eDfKdAnXKQPV.htm)|Wand Cane|Canne à baguette|libre|
|[equipment-04-AYg155apTZtebV4Z.htm](equipment/equipment-04-AYg155apTZtebV4Z.htm)|Spiny Lodestone|Pierre aimantée épineuse|libre|
|[equipment-04-bCwSeD0Ub3f4BfjO.htm](equipment/equipment-04-bCwSeD0Ub3f4BfjO.htm)|Dragon's Breath (1st Level Spell)|Souffle de dragon de sort de rang 1 (rune)|libre|
|[equipment-04-BHjJpNILf85M2LJE.htm](equipment/equipment-04-BHjJpNILf85M2LJE.htm)|Symbol of Conflict|Symbole du conflit|libre|
|[equipment-04-BZImrMVyqeZ0RfF8.htm](equipment/equipment-04-BZImrMVyqeZ0RfF8.htm)|Bewitching Bloom (Red Rose)|Fleur envoûtante (Rose rouge)|libre|
|[equipment-04-cKYWot4ifTOa6ans.htm](equipment/equipment-04-cKYWot4ifTOa6ans.htm)|Alchemical Chart (Lesser)|Table alchimique inférieure|libre|
|[equipment-04-cyw2OgL4XJ9HOu0b.htm](equipment/equipment-04-cyw2OgL4XJ9HOu0b.htm)|Wand of Reaching (1st-level)|Baguette d'atteinte (rang 1)|libre|
|[equipment-04-d0ponRhw0JhSM4iH.htm](equipment/equipment-04-d0ponRhw0JhSM4iH.htm)|Aeon Stone (Clear Quartz Octagon)|Pierre d'éternité (octogone de quartz translucide)|libre|
|[equipment-04-Dntj7zwUTmj4fboD.htm](equipment/equipment-04-Dntj7zwUTmj4fboD.htm)|Shapespeak Mask|Masque de parole métamorphe|libre|
|[equipment-04-dsxATkJ2RwvYF3vm.htm](equipment/equipment-04-dsxATkJ2RwvYF3vm.htm)|Drums of War|Tambour de guerre|libre|
|[equipment-04-DxCuJKynlnMQZHgp.htm](equipment/equipment-04-DxCuJKynlnMQZHgp.htm)|Striking|Frappe (rune)|officielle|
|[equipment-04-EGYcFO9eYfajGKEf.htm](equipment/equipment-04-EGYcFO9eYfajGKEf.htm)|Occult Scroll Case of Simplicity|Étui à parchemin de simplicité occulte|libre|
|[equipment-04-Ejmv9IHGp9Ad9dgu.htm](equipment/equipment-04-Ejmv9IHGp9Ad9dgu.htm)|Thieves' Tools (Concealable)|Matériel de voleur dissimulable|libre|
|[equipment-04-eLsmwHVW5qiwcd7c.htm](equipment/equipment-04-eLsmwHVW5qiwcd7c.htm)|Seer's Flute|Flûte du devin|libre|
|[equipment-04-f9vNVQ2v45mDI8Xr.htm](equipment/equipment-04-f9vNVQ2v45mDI8Xr.htm)|Aeon Stone (Azure Briolette)|Pierre d'éternité (briolette azur)|libre|
|[equipment-04-g2oaOGSpttfH1q6W.htm](equipment/equipment-04-g2oaOGSpttfH1q6W.htm)|Lifting Belt|Ceinture de levée|libre|
|[equipment-04-g9pOZsJykSihdyrL.htm](equipment/equipment-04-g9pOZsJykSihdyrL.htm)|Sealing Chest (Lesser)|Coffre scellé inférieur|libre|
|[equipment-04-GMcU4kX1ldrSQMh6.htm](equipment/equipment-04-GMcU4kX1ldrSQMh6.htm)|Vengeful Arm|Bras vengeur|libre|
|[equipment-04-GuVgH50cUeytM68r.htm](equipment/equipment-04-GuVgH50cUeytM68r.htm)|Stargazer's Spyglass|Lorgnette de l'astronome|libre|
|[equipment-04-gvtFrrvCSdsCOlOj.htm](equipment/equipment-04-gvtFrrvCSdsCOlOj.htm)|Air Cartridge Firing System|Système de cartouche à air comprimé|libre|
|[equipment-04-hnbbqvzYDyhDiJnf.htm](equipment/equipment-04-hnbbqvzYDyhDiJnf.htm)|Bane|Fléau (rune)|libre|
|[equipment-04-IAJcB9cGEKl6HyGE.htm](equipment/equipment-04-IAJcB9cGEKl6HyGE.htm)|Eye Slash (Greater)|Entailles à l'oeil supérieures|libre|
|[equipment-04-j1To2GxVU46H1uOG.htm](equipment/equipment-04-j1To2GxVU46H1uOG.htm)|Thorn Triad|Triade d'épines|libre|
|[equipment-04-JCGsj7iY88o9uTP5.htm](equipment/equipment-04-JCGsj7iY88o9uTP5.htm)|Hunter's Arrowhead|Pointe de flèche du chasseur|libre|
|[equipment-04-JlI0oubjxL9WOt4p.htm](equipment/equipment-04-JlI0oubjxL9WOt4p.htm)|Wand of the Pampered Pet|Baguette du familier gâté|libre|
|[equipment-04-jOulX8kWlCF5Dveg.htm](equipment/equipment-04-jOulX8kWlCF5Dveg.htm)|Jug of Fond Remembrance|Cruche du bon souvenir|libre|
|[equipment-04-Jpgl3HlEd8u20fUY.htm](equipment/equipment-04-Jpgl3HlEd8u20fUY.htm)|Entertainer's Lute|Luth du saltimbanque|libre|
|[equipment-04-JQdwHECogcTzdd8R.htm](equipment/equipment-04-JQdwHECogcTzdd8R.htm)|Ghost Touch|Spectrale (rune)|libre|
|[equipment-04-kBCdBFb9IWlwy81B.htm](equipment/equipment-04-kBCdBFb9IWlwy81B.htm)|Wand of Pernicious Poison (Spider Sting)|Baguette de poison pernicieux (Morsure d'araignée)|libre|
|[equipment-04-kILYXCczAc6ZfArJ.htm](equipment/equipment-04-kILYXCczAc6ZfArJ.htm)|Trickster's Mandolin|Mandoline du mystificateur|libre|
|[equipment-04-l5aps7Qfc0lB0LYX.htm](equipment/equipment-04-l5aps7Qfc0lB0LYX.htm)|Noppera-Bo Hood|Cagoule Noppera-bo|libre|
|[equipment-04-LhpRuLfxuR3t819V.htm](equipment/equipment-04-LhpRuLfxuR3t819V.htm)|Bewitching Bloom (White Poppy)|Fleur envoûtante (Coqueliquot blanc)|libre|
|[equipment-04-LmIpYZ1lS2UDGXvU.htm](equipment/equipment-04-LmIpYZ1lS2UDGXvU.htm)|Mortar of Hidden Meaning|Mortier du sens caché|libre|
|[equipment-04-LSbv4rdvKfm4bPrR.htm](equipment/equipment-04-LSbv4rdvKfm4bPrR.htm)|Dread Helm|Heaume d'effroi|libre|
|[equipment-04-mqOjTuUHv4cRdt2N.htm](equipment/equipment-04-mqOjTuUHv4cRdt2N.htm)|Primal Scroll Case of Simplicity|Étui à parchemin de simplicité primordiale|libre|
|[equipment-04-nNinADW4vVC0roP2.htm](equipment/equipment-04-nNinADW4vVC0roP2.htm)|Pickpocket's Tailoring|Doublure du pickpocket|libre|
|[equipment-04-NUEwFlFb7RJnLD4w.htm](equipment/equipment-04-NUEwFlFb7RJnLD4w.htm)|Mask of Mercy|Masque de pitié|libre|
|[equipment-04-O0iTcOhlsEPGsxtD.htm](equipment/equipment-04-O0iTcOhlsEPGsxtD.htm)|Highhelm Drill Mark I|Foreuse de Heaume - Mark I|libre|
|[equipment-04-o1zKhvYUUc1hE2AE.htm](equipment/equipment-04-o1zKhvYUUc1hE2AE.htm)|Healer's Gloves|Gants du guérisseur|libre|
|[equipment-04-oC4ZMEdBJ3ia4ALm.htm](equipment/equipment-04-oC4ZMEdBJ3ia4ALm.htm)|Cloak of Repute|Cape de renom|libre|
|[equipment-04-oIMtXP8y7oxvrBVP.htm](equipment/equipment-04-oIMtXP8y7oxvrBVP.htm)|Hosteling Statuette|Statuette hôte|libre|
|[equipment-04-oJLQaiCSmFJx0bxe.htm](equipment/equipment-04-oJLQaiCSmFJx0bxe.htm)|Inquisitive Quill|Plume curieuse|libre|
|[equipment-04-PhjutBgR1egCuHvY.htm](equipment/equipment-04-PhjutBgR1egCuHvY.htm)|Wayfinder of Rescue|Guide de secours|libre|
|[equipment-04-PttRbxopuD0EpDTI.htm](equipment/equipment-04-PttRbxopuD0EpDTI.htm)|Frightful Hag Eye|Oeil de guenaude effrayant|libre|
|[equipment-04-qDiVe9Ob04gRXLFa.htm](equipment/equipment-04-qDiVe9Ob04gRXLFa.htm)|Warcaller's Chime of Destruction|Carillon de destruction du belliqueux|libre|
|[equipment-04-qdXwiDaPdsFtlXGH.htm](equipment/equipment-04-qdXwiDaPdsFtlXGH.htm)|Reading Glyphs|Glyphes de lecture|libre|
|[equipment-04-QRzpVYuATBjyBojJ.htm](equipment/equipment-04-QRzpVYuATBjyBojJ.htm)|Wand of Shrouded Step|Baguette du Pas voilé|libre|
|[equipment-04-r28DjJEjF6jvCcfb.htm](equipment/equipment-04-r28DjJEjF6jvCcfb.htm)|Merciful|Miséricordieuse (rune)|libre|
|[equipment-04-rT93xVoebhCm9uVA.htm](equipment/equipment-04-rT93xVoebhCm9uVA.htm)|Faith Tattoo|Tatouage du croyant|libre|
|[equipment-04-rV7MTDCseZmEZKDw.htm](equipment/equipment-04-rV7MTDCseZmEZKDw.htm)|Spyglass (Fine)|Longue-vue (belle)|libre|
|[equipment-04-teMhnTeAyWnvbo2C.htm](equipment/equipment-04-teMhnTeAyWnvbo2C.htm)|Tremorsensors|Senseurs de vibrations|libre|
|[equipment-04-TmH7coBHz9pjoDvP.htm](equipment/equipment-04-TmH7coBHz9pjoDvP.htm)|Lawbringer's Lasso|Lasso du porteur de loi|libre|
|[equipment-04-tnPteWjbm97bdTj5.htm](equipment/equipment-04-tnPteWjbm97bdTj5.htm)|Talisman Cord (Lesser)|Cordon de talisman inférieur|libre|
|[equipment-04-TtDWzugeKjtlWfZq.htm](equipment/equipment-04-TtDWzugeKjtlWfZq.htm)|Memory Guitar|Guitare mémorielle|libre|
|[equipment-04-TTvfgU2G5n3tpTby.htm](equipment/equipment-04-TTvfgU2G5n3tpTby.htm)|Pipes of Compulsion|Tuyaux de servitude|libre|
|[equipment-04-TxMFNfkoccirCitP.htm](equipment/equipment-04-TxMFNfkoccirCitP.htm)|Thieves' Tools (Concealable Picks)|Crochets de voleur de remplacement dissimulables|libre|
|[equipment-04-UNOHV2dnRphyNJFw.htm](equipment/equipment-04-UNOHV2dnRphyNJFw.htm)|Rhinoceros Mask|Masque de rhinocéros|libre|
|[equipment-04-uOaFBqnCYWCgyCl6.htm](equipment/equipment-04-uOaFBqnCYWCgyCl6.htm)|Wildwood Ink|Encre du bois sauvage|libre|
|[equipment-04-vSiePW2xIEOmXB0H.htm](equipment/equipment-04-vSiePW2xIEOmXB0H.htm)|Wand of Mercy (1st-level)|Baguette de miséricorde (rang 1)|libre|
|[equipment-04-vxBcsWCjWd6DZ0Jz.htm](equipment/equipment-04-vxBcsWCjWd6DZ0Jz.htm)|Arcane Scroll Case of Simplicity|Étui à parchemin de simplicité arcanique|libre|
|[equipment-04-vXMIKXn3RHh3UET8.htm](equipment/equipment-04-vXMIKXn3RHh3UET8.htm)|Cape of Justice|Cape de justice|libre|
|[equipment-04-WAYGyAaH7rtpX1y4.htm](equipment/equipment-04-WAYGyAaH7rtpX1y4.htm)|Wand of Crushing Leaps|Baguette des bonds écrasants|libre|
|[equipment-04-WKdmAhoji9Y9RC7D.htm](equipment/equipment-04-WKdmAhoji9Y9RC7D.htm)|Marvelous Calliope|Calliope merveilleux|libre|
|[equipment-04-wRZGJohTF0FFJBrq.htm](equipment/equipment-04-wRZGJohTF0FFJBrq.htm)|Diver's Gloves (Lesser)|Gants du plongeur inférieurs|libre|
|[equipment-04-x9SNVpAAnXKJeoqp.htm](equipment/equipment-04-x9SNVpAAnXKJeoqp.htm)|Reinforcing Rune (Minor)|Renforcement mineur (rune)|libre|
|[equipment-04-xTcQPSZlZ231gPWk.htm](equipment/equipment-04-xTcQPSZlZ231gPWk.htm)|Wand of Mental Purification (1st-level)|Baguette de purification mentale (rang 1)|libre|
|[equipment-04-XtYQFNEDlEVOr831.htm](equipment/equipment-04-XtYQFNEDlEVOr831.htm)|Sextant of the Night|Sextant de la nuit|libre|
|[equipment-04-y0ngvQ7ArcUuoEHT.htm](equipment/equipment-04-y0ngvQ7ArcUuoEHT.htm)|Blessed Tattoo|Tatouage de bénédiction|officielle|
|[equipment-04-y8nPmHXIdt4KhMQU.htm](equipment/equipment-04-y8nPmHXIdt4KhMQU.htm)|Bagpipes of Turmoil|Cornemuse du tumulte|libre|
|[equipment-04-z2QXO8vl0VsXaI1E.htm](equipment/equipment-04-z2QXO8vl0VsXaI1E.htm)|Wand of Legerdemain (1st-level)|Baguette de prestidigitation (rang 1)|libre|
|[equipment-04-z6eQdPTgSxTLR1Qr.htm](equipment/equipment-04-z6eQdPTgSxTLR1Qr.htm)|Shining Wayfinder|Guide étincelant|libre|
|[equipment-04-Zw3BKaJYxxxzNZ0f.htm](equipment/equipment-04-Zw3BKaJYxxxzNZ0f.htm)|Wand of Widening (1st-Rank Spell)|Baguette d'élargissement (sort de rang 1)|libre|
|[equipment-05-0LfgYMONUws8lzOv.htm](equipment/equipment-05-0LfgYMONUws8lzOv.htm)|Tlil Mask|Masque tlil|libre|
|[equipment-05-0T7JuFupBTfDBiHv.htm](equipment/equipment-05-0T7JuFupBTfDBiHv.htm)|Boots of Free Running (Lesser)|Bottes de course libre inférieures|libre|
|[equipment-05-0ykBs92jTd4dmNqb.htm](equipment/equipment-05-0ykBs92jTd4dmNqb.htm)|Brazier of Harmony|Braséro d'harmonie|libre|
|[equipment-05-1Md7gEeGcqs7n4iU.htm](equipment/equipment-05-1Md7gEeGcqs7n4iU.htm)|Shade Hat|Chapeau d'ombre|libre|
|[equipment-05-2EzUZq9pEVP4oQ0k.htm](equipment/equipment-05-2EzUZq9pEVP4oQ0k.htm)|Guiding Cajon Drum|Tambour cajun de guidage|libre|
|[equipment-05-3RPYWoUv1VKGjJ7i.htm](equipment/equipment-05-3RPYWoUv1VKGjJ7i.htm)|Ring of Minor Arcana|Anneau d'arcane mineures|libre|
|[equipment-05-3zgX1Uvwv3R262IV.htm](equipment/equipment-05-3zgX1Uvwv3R262IV.htm)|Uniter of Clans|Unificateur des clans|libre|
|[equipment-05-47FmnpSOE96SJ8H4.htm](equipment/equipment-05-47FmnpSOE96SJ8H4.htm)|Aeon Stone (Agate Ellipsoid)|Pierre d'éternité (agate ellipsoïde)|libre|
|[equipment-05-4eC5wMw1vcDDgC4x.htm](equipment/equipment-05-4eC5wMw1vcDDgC4x.htm)|Horn of Rust|Corne de rouille|libre|
|[equipment-05-5AHJQn2QrvdsTJsX.htm](equipment/equipment-05-5AHJQn2QrvdsTJsX.htm)|Warcaller's Chime of Resistance|Carillon de résistance du belliqueux|libre|
|[equipment-05-6cV9Kpwc7aiuhqbH.htm](equipment/equipment-05-6cV9Kpwc7aiuhqbH.htm)|Necklace of Fireballs I|Collier à boules de feu I|officielle|
|[equipment-05-6rhind8MDhtJHlwq.htm](equipment/equipment-05-6rhind8MDhtJHlwq.htm)|Wondrous Figurine (Candy Constrictor)|Figurine merveilleuse (Constricteur bonbon)|libre|
|[equipment-05-73lxT8RrbKtoHvty.htm](equipment/equipment-05-73lxT8RrbKtoHvty.htm)|Chaos Collar|Collier du chaos|libre|
|[equipment-05-7F6WGmvZ0r3PloXC.htm](equipment/equipment-05-7F6WGmvZ0r3PloXC.htm)|Furnace of Endings|Fournaise des terminaisons|libre|
|[equipment-05-7Z6cpy4rXCOaYJJH.htm](equipment/equipment-05-7Z6cpy4rXCOaYJJH.htm)|Nightbreeze Machine|Machine brise nocturne|libre|
|[equipment-05-8iet9wptkaoJzOmc.htm](equipment/equipment-05-8iet9wptkaoJzOmc.htm)|Magnifying Glass of Elucidation|Loupe d'élucidation|libre|
|[equipment-05-8WtXK4cyYcls72Yf.htm](equipment/equipment-05-8WtXK4cyYcls72Yf.htm)|Spyglass Eye|Oeil de lorgnette|libre|
|[equipment-05-8YnlhfZSQ89FjDZ2.htm](equipment/equipment-05-8YnlhfZSQ89FjDZ2.htm)|Corpseward Pendant|Pendentif protège morts-vivants|libre|
|[equipment-05-98JIeYGEuE6pPV05.htm](equipment/equipment-05-98JIeYGEuE6pPV05.htm)|Floorbell|Alarme de plancher|libre|
|[equipment-05-9XrsaIc80eXhKaoM.htm](equipment/equipment-05-9XrsaIc80eXhKaoM.htm)|Rope of Climbing (Moderate)|Corde d'escalade modérée|libre|
|[equipment-05-a60NH7OztaEaGlU8.htm](equipment/equipment-05-a60NH7OztaEaGlU8.htm)|Wand of Continuation (1st-Rank Spell)|Baguette de prolongation (sort de rang 1)|libre|
|[equipment-05-AHkmoVr1NSFytJwp.htm](equipment/equipment-05-AHkmoVr1NSFytJwp.htm)|Merchant's Guile|Ruse du marchand|libre|
|[equipment-05-aJEXZJYjzoMHc5Pm.htm](equipment/equipment-05-aJEXZJYjzoMHc5Pm.htm)|Eternal Eruption|Éternelle éruption|libre|
|[equipment-05-amJW9azNUSJLmWGP.htm](equipment/equipment-05-amJW9azNUSJLmWGP.htm)|Arboreal Boots|Bottes arboréennes|libre|
|[equipment-05-aneZJVb3sbpG2fN7.htm](equipment/equipment-05-aneZJVb3sbpG2fN7.htm)|Snowshoes of the Long Trek|Raquettes de neige de randonnée|libre|
|[equipment-05-aUohYRQ8lHzgblxi.htm](equipment/equipment-05-aUohYRQ8lHzgblxi.htm)|Toolkit of Bronze Whispers|Trousse des Murmures de bronze|libre|
|[equipment-05-B4wxZ7mvBDJPWPvZ.htm](equipment/equipment-05-B4wxZ7mvBDJPWPvZ.htm)|Bomb Coagulant Alembic|Alambic coagulateur de bombe|libre|
|[equipment-05-bhpYiOmBysl8ZEYn.htm](equipment/equipment-05-bhpYiOmBysl8ZEYn.htm)|Stalwart's Ring|Anneau de résolution|libre|
|[equipment-05-BMhTM2TfK06pEZ3Q.htm](equipment/equipment-05-BMhTM2TfK06pEZ3Q.htm)|Adamantine Echo|Écho d'adamantium|libre|
|[equipment-05-BNelZMBHKlPAWl9Z.htm](equipment/equipment-05-BNelZMBHKlPAWl9Z.htm)|Pocket Stage|Scène de poche|libre|
|[equipment-05-c0gjWASSeNIRNmEw.htm](equipment/equipment-05-c0gjWASSeNIRNmEw.htm)|Warrior's Training Ring|L'anneau d'entrainement du guerrier|libre|
|[equipment-05-ClfUJ4AtaxglVGvs.htm](equipment/equipment-05-ClfUJ4AtaxglVGvs.htm)|Fleshgem (Combat)|Gemme de chair de combat|libre|
|[equipment-05-CtAg4DpSssKBIw8R.htm](equipment/equipment-05-CtAg4DpSssKBIw8R.htm)|Replacement Filter (Level 5)|Filtre de remplacement (niveau 5)|libre|
|[equipment-05-Dc2DRXybi5dCxmqP.htm](equipment/equipment-05-Dc2DRXybi5dCxmqP.htm)|Knight's Maintenance Kit|Kit de maintenance du chevalier|libre|
|[equipment-05-DmzkzKB4EBleK0DA.htm](equipment/equipment-05-DmzkzKB4EBleK0DA.htm)|Armory Bracelet (Lesser)|Bracelet arsenal inférieur|libre|
|[equipment-05-e2hDvrdz0t0hzE5y.htm](equipment/equipment-05-e2hDvrdz0t0hzE5y.htm)|Heated Cloak|Cape chauffante|libre|
|[equipment-05-eDO54Z8RdjL2ymgW.htm](equipment/equipment-05-eDO54Z8RdjL2ymgW.htm)|Avernal Cape|Cape avernale|libre|
|[equipment-05-Ee9RgoMY3XczPidF.htm](equipment/equipment-05-Ee9RgoMY3XczPidF.htm)|Liar's Board|Tableau du menteur|libre|
|[equipment-05-eTOHAULu4kgqR1S5.htm](equipment/equipment-05-eTOHAULu4kgqR1S5.htm)|Instructions for Lasting Agony|Instructions pour une lente agonie|libre|
|[equipment-05-Ez0byAlwJwzRXof5.htm](equipment/equipment-05-Ez0byAlwJwzRXof5.htm)|Paired|Appariée (rune)|libre|
|[equipment-05-fprUZviW8khm2BLo.htm](equipment/equipment-05-fprUZviW8khm2BLo.htm)|Skeleton Key|Passe-partout|libre|
|[equipment-05-gjjF0sQMpXf3CrTF.htm](equipment/equipment-05-gjjF0sQMpXf3CrTF.htm)|Advanced Book of Translation (Tien)|Le livre de traduction avancé (Tien)|libre|
|[equipment-05-GkyZl0UeUAS5YaHA.htm](equipment/equipment-05-GkyZl0UeUAS5YaHA.htm)|Aether Appendage|Appendice éthéré|libre|
|[equipment-05-GVgA0w4CUP4lanPl.htm](equipment/equipment-05-GVgA0w4CUP4lanPl.htm)|Thunder Helm|Heaume du tonnerre|libre|
|[equipment-05-HM0uhpM9t97MAABn.htm](equipment/equipment-05-HM0uhpM9t97MAABn.htm)|Cape of Illumination (Lesser)|Cape d'illumination inférieure|libre|
|[equipment-05-HSAj9FBx2yAyjfzf.htm](equipment/equipment-05-HSAj9FBx2yAyjfzf.htm)|Spurned Lute|Luth renversé|libre|
|[equipment-05-iFkgpzf67iapKfyD.htm](equipment/equipment-05-iFkgpzf67iapKfyD.htm)|Silent Heart|Coeur silencieux|libre|
|[equipment-05-iS7hAQMAaThHYE8g.htm](equipment/equipment-05-iS7hAQMAaThHYE8g.htm)|Bort's Blessing|Bénédiction de Bort|libre|
|[equipment-05-iTxqImupNnm8gvoe.htm](equipment/equipment-05-iTxqImupNnm8gvoe.htm)|Raiment|Habillement (rune)|libre|
|[equipment-05-IVaQrUnLCzTZIfP6.htm](equipment/equipment-05-IVaQrUnLCzTZIfP6.htm)|Spring Heel|Talon à ressort|libre|
|[equipment-05-Izs8R6o4mpFvz3Wf.htm](equipment/equipment-05-Izs8R6o4mpFvz3Wf.htm)|Desiccating Scepter|Sceptre desséchant|libre|
|[equipment-05-JJZgRx6naNJmDa81.htm](equipment/equipment-05-JJZgRx6naNJmDa81.htm)|Diplomat's Badge|Insigne du diplomate|libre|
|[equipment-05-JWFR4O5V06UUvx6W.htm](equipment/equipment-05-JWFR4O5V06UUvx6W.htm)|Beastmaster's Sigil|Symbole du maître des bêtes|libre|
|[equipment-05-JWUTLYX1zaBntC1L.htm](equipment/equipment-05-JWUTLYX1zaBntC1L.htm)|Ring of the Tiger|Anneau du tigre|libre|
|[equipment-05-K7VHhUamFz3kTnm5.htm](equipment/equipment-05-K7VHhUamFz3kTnm5.htm)|Obsidian Goggles|Lunettes d'obsidienne|libre|
|[equipment-05-KekfZ6eRzoZVRemw.htm](equipment/equipment-05-KekfZ6eRzoZVRemw.htm)|Tengu Feather Fan|Éventail de plume tengu|libre|
|[equipment-05-kEy7Uc1VisizGgtf.htm](equipment/equipment-05-kEy7Uc1VisizGgtf.htm)|Shadow|Ombre (rune)|libre|
|[equipment-05-KPWN5tGGkvZR7K3K.htm](equipment/equipment-05-KPWN5tGGkvZR7K3K.htm)|Wand of Shardstorm (1st-Rank Spell)|Baguette de tempêtes d'éclats (rang 1)|libre|
|[equipment-05-KqzTdCwrfoqfSR5b.htm](equipment/equipment-05-KqzTdCwrfoqfSR5b.htm)|Tome of Restorative Cleansing (Lesser)|Tome du nettoyage restaurateur inférieur|libre|
|[equipment-05-krKFJmE139HBqa9K.htm](equipment/equipment-05-krKFJmE139HBqa9K.htm)|Darkvision Scope|Lunette de visée dans le noir|libre|
|[equipment-05-KrmSuQIyu6OEi5ew.htm](equipment/equipment-05-KrmSuQIyu6OEi5ew.htm)|Holy Prayer Beads|Chapelet sacré|libre|
|[equipment-05-l4FxEma0PrvUPeMe.htm](equipment/equipment-05-l4FxEma0PrvUPeMe.htm)|Anylength Rope (Moderate)|Corde toute longueur modérée|libre|
|[equipment-05-Lj6diNjoD5ilz7jd.htm](equipment/equipment-05-Lj6diNjoD5ilz7jd.htm)|Boozy Bottle|Bouteille alcoolisée|libre|
|[equipment-05-LwQb7ryTC8FlOXgX.htm](equipment/equipment-05-LwQb7ryTC8FlOXgX.htm)|Vitalizing|Vitalisante (rune)|libre|
|[equipment-05-LYT2X05grQJxx5qe.htm](equipment/equipment-05-LYT2X05grQJxx5qe.htm)|Shell of Easy Breathing|Coquillage de respiration facilitée|libre|
|[equipment-05-mdNNIF01ybgs1m3s.htm](equipment/equipment-05-mdNNIF01ybgs1m3s.htm)|War Saddle|Selle de guerre|libre|
|[equipment-05-me7GsgRKnqA7vaB5.htm](equipment/equipment-05-me7GsgRKnqA7vaB5.htm)|Waverider Barding|Barde de Monte-vague|libre|
|[equipment-05-N9bhBhJ6JawzVitu.htm](equipment/equipment-05-N9bhBhJ6JawzVitu.htm)|Large Bore Modifications|Modifications de gros calibre|libre|
|[equipment-05-NJtIwMIzjdRqupAM.htm](equipment/equipment-05-NJtIwMIzjdRqupAM.htm)|Stanching|Étanchement (rune)|libre|
|[equipment-05-NZJkdq4AezyeUvGi.htm](equipment/equipment-05-NZJkdq4AezyeUvGi.htm)|Astrolabe of the Falling Stars|Astrolabe des étoiles filantes|libre|
|[equipment-05-OClYfRHzoynib6wX.htm](equipment/equipment-05-OClYfRHzoynib6wX.htm)|Earthbinding|Tellurique (rune)|libre|
|[equipment-05-OdpmSFarov2qUoyN.htm](equipment/equipment-05-OdpmSFarov2qUoyN.htm)|Sparkwarden|Gardétincelant|libre|
|[equipment-05-OFo1349Pems2mboB.htm](equipment/equipment-05-OFo1349Pems2mboB.htm)|Quick Runner's Shirt|Chemise du coureur rapide|libre|
|[equipment-05-oOtrCBGLqNLdiOuF.htm](equipment/equipment-05-oOtrCBGLqNLdiOuF.htm)|Wand of Contagious Frailty|Baguette de fragilité contagieuse|libre|
|[equipment-05-OX71iN2SXMWMeI5R.htm](equipment/equipment-05-OX71iN2SXMWMeI5R.htm)|Secret-Keeper's Mask (Gray Master)|Masque des gardiens des secrets (Maître gris)|libre|
|[equipment-05-P1hk0BO89ihEAw8g.htm](equipment/equipment-05-P1hk0BO89ihEAw8g.htm)|Bloodhound Olfactory Stimulators|Stimulateurs olfactifs de la meute sanglante|libre|
|[equipment-05-P6JCnxSAzGcUl8D4.htm](equipment/equipment-05-P6JCnxSAzGcUl8D4.htm)|Kalmaug's Journal|Journal de Kalmaug|libre|
|[equipment-05-P6v2AtJw7AUwaDzf.htm](equipment/equipment-05-P6v2AtJw7AUwaDzf.htm)|Fearsome|Effrayante (rune)|libre|
|[equipment-05-pASszbK2LjpNUJY1.htm](equipment/equipment-05-pASszbK2LjpNUJY1.htm)|Conundrum Spectacles|Lunettes d'énigmes|libre|
|[equipment-05-pD9H7z7NB3o5dvPN.htm](equipment/equipment-05-pD9H7z7NB3o5dvPN.htm)|Anglerfish Lantern|Lanterne baudroie|libre|
|[equipment-05-PQ74afm5YWessacn.htm](equipment/equipment-05-PQ74afm5YWessacn.htm)|Bravery Baldric (Fleet)|Baudrier de bravoure (Pas rapide)|libre|
|[equipment-05-q6Wdgi5fE1zovsYh.htm](equipment/equipment-05-q6Wdgi5fE1zovsYh.htm)|Tactician's Helm|Heaume du tacticien|libre|
|[equipment-05-QiYXHgbAv29OuaWS.htm](equipment/equipment-05-QiYXHgbAv29OuaWS.htm)|Collar of the Shifting Spider|Collier de l'araignée changeante|libre|
|[equipment-05-QOEvD3xh2qY7v0kh.htm](equipment/equipment-05-QOEvD3xh2qY7v0kh.htm)|Wayfinderfinder|Cherche guide d'éclaireur|libre|
|[equipment-05-QOUjYRxXHvwMkGAw.htm](equipment/equipment-05-QOUjYRxXHvwMkGAw.htm)|Folding Drums|Tambour dépliant|libre|
|[equipment-05-qwHO8kRNKPqCM80x.htm](equipment/equipment-05-qwHO8kRNKPqCM80x.htm)|Spirit-Singer|Esprit-chanteur|libre|
|[equipment-05-R8I13CDRzvpVXOVe.htm](equipment/equipment-05-R8I13CDRzvpVXOVe.htm)|Pacifying|Pacificatrice (rune)|libre|
|[equipment-05-rcJwqJ88ubcRDFJH.htm](equipment/equipment-05-rcJwqJ88ubcRDFJH.htm)|Shrieking Key|Clé hurlante|libre|
|[equipment-05-RKPAHGFV3mq3a3dt.htm](equipment/equipment-05-RKPAHGFV3mq3a3dt.htm)|Portable Weapon Mount (Shielded Tripod)|Trépied portable blindé|libre|
|[equipment-05-Rm2cojERpLEWB9B3.htm](equipment/equipment-05-Rm2cojERpLEWB9B3.htm)|Assisting|D'assistance (rune)|libre|
|[equipment-05-Ro3g2JpJXrKXVyEr.htm](equipment/equipment-05-Ro3g2JpJXrKXVyEr.htm)|Armor Potency (+1)|Puissance d'armure +1 (rune)|libre|
|[equipment-05-S9eytXwDMdwdSh2z.htm](equipment/equipment-05-S9eytXwDMdwdSh2z.htm)|Hooked|à crochet (rune)|libre|
|[equipment-05-SCkQb4QdFWkATiby.htm](equipment/equipment-05-SCkQb4QdFWkATiby.htm)|Vanishing Wayfinder|Guide Invisible|libre|
|[equipment-05-sO3b9FRLFY43YQUc.htm](equipment/equipment-05-sO3b9FRLFY43YQUc.htm)|Hongbao of Many Things|Hongbao des merveilles|libre|
|[equipment-05-SUnKoeqNBsGvUAGe.htm](equipment/equipment-05-SUnKoeqNBsGvUAGe.htm)|Metuak's Pendant|Pendentif de Metuak|libre|
|[equipment-05-T00Xa9aDwHxd60Zh.htm](equipment/equipment-05-T00Xa9aDwHxd60Zh.htm)|Boots of Elvenkind|Bottes elfiques|officielle|
|[equipment-05-T4gTHDKJ0HI10p3y.htm](equipment/equipment-05-T4gTHDKJ0HI10p3y.htm)|Cunning|Astucieuse (rune)|libre|
|[equipment-05-TVpEf9gMFUuwfGoU.htm](equipment/equipment-05-TVpEf9gMFUuwfGoU.htm)|Resonating Fork|Diapason résonant|libre|
|[equipment-05-uQOaRpfkUFVYD0Gx.htm](equipment/equipment-05-uQOaRpfkUFVYD0Gx.htm)|Slick|Glissante (rune)|officielle|
|[equipment-05-V3FxGX0Yy8c7ucjH.htm](equipment/equipment-05-V3FxGX0Yy8c7ucjH.htm)|Pipe of Dancing Smoke|Pipe de fumée dansante|libre|
|[equipment-05-v935rvhKtJm7PSNF.htm](equipment/equipment-05-v935rvhKtJm7PSNF.htm)|Wondrous Figurine, Stuffed Fox|Figurine merveilleuse (Renard en peluche)|libre|
|[equipment-05-vCf4otZtC4RHtXdI.htm](equipment/equipment-05-vCf4otZtC4RHtXdI.htm)|Poison Concentrator (Lesser)|Concentrateur de poison inférieur|libre|
|[equipment-05-vx5bG5YNeC78u49n.htm](equipment/equipment-05-vx5bG5YNeC78u49n.htm)|Warding Tattoo|Tatouage de protection|libre|
|[equipment-05-w0iN9Q6UpqNp2xQs.htm](equipment/equipment-05-w0iN9Q6UpqNp2xQs.htm)|Homeward Swallow|Hirondelle de retour|libre|
|[equipment-05-wJOW9YfEp5RDYHc1.htm](equipment/equipment-05-wJOW9YfEp5RDYHc1.htm)|Pact of Blood-Taking|Pacte de profusion de sang (pacte infernal)|libre|
|[equipment-05-WW8uv3Nhg56UTZ35.htm](equipment/equipment-05-WW8uv3Nhg56UTZ35.htm)|Sun Wheel|Roue Solaire|libre|
|[equipment-05-x4l3JjePzROLDSK6.htm](equipment/equipment-05-x4l3JjePzROLDSK6.htm)|Spectacles of Understanding|Lunettes de compréhension|libre|
|[equipment-05-xdpP0y4EfRf0ALl8.htm](equipment/equipment-05-xdpP0y4EfRf0ALl8.htm)|Grim Ring|Anneau sinistre|libre|
|[equipment-05-Xv6NlFohsFtIHp6K.htm](equipment/equipment-05-Xv6NlFohsFtIHp6K.htm)|Ring of the Weary Traveler|Anneau du voyageur fatigué|libre|
|[equipment-05-xwX93cp1GiTwgtBj.htm](equipment/equipment-05-xwX93cp1GiTwgtBj.htm)|Aeon Stone (Preserving)|Pierre d'éternité préservatrice|libre|
|[equipment-05-y8M3fBQEtddg9Lbj.htm](equipment/equipment-05-y8M3fBQEtddg9Lbj.htm)|Remote Trigger|Déclencheur à distance|libre|
|[equipment-05-YafmTpwzMsKiAg5b.htm](equipment/equipment-05-YafmTpwzMsKiAg5b.htm)|Mirror Goggles (Lesser)|Lunettes miroir inférieures|libre|
|[equipment-05-yAG9XjecF5xpOEvg.htm](equipment/equipment-05-yAG9XjecF5xpOEvg.htm)|Corrosive Engravings|Gravures corrosives|libre|
|[equipment-05-zVY6VVKrrf3K5TSC.htm](equipment/equipment-05-zVY6VVKrrf3K5TSC.htm)|Wind At Your Back|Vent dans le dos|libre|
|[equipment-06-0SSu1fch6ah0oTti.htm](equipment/equipment-06-0SSu1fch6ah0oTti.htm)|Horned Hand Rests|Repose mains à cornes|libre|
|[equipment-06-0zTY4ogkffTxmJUj.htm](equipment/equipment-06-0zTY4ogkffTxmJUj.htm)|Deck of illusions|Jeu de carte des illusions|libre|
|[equipment-06-1ii8hvKjkf71wYMx.htm](equipment/equipment-06-1ii8hvKjkf71wYMx.htm)|Storyteller's Opus|Opus du bonimenteur|libre|
|[equipment-06-2ovu1AioLLff9p8w.htm](equipment/equipment-06-2ovu1AioLLff9p8w.htm)|Hauling|Traction (rune)|libre|
|[equipment-06-3jd8wUCHH5WhCTEJ.htm](equipment/equipment-06-3jd8wUCHH5WhCTEJ.htm)|Crown of the Companion|Couronne du compagnon|libre|
|[equipment-06-3sGpEBXsZwjGnoES.htm](equipment/equipment-06-3sGpEBXsZwjGnoES.htm)|Chime of Opening|Carillon d'ouverture|libre|
|[equipment-06-4dSzBQPb68ik7ul8.htm](equipment/equipment-06-4dSzBQPb68ik7ul8.htm)|Majordomo Torc|Torque de majordome|libre|
|[equipment-06-65ZhBT2S8bCeEIgz.htm](equipment/equipment-06-65ZhBT2S8bCeEIgz.htm)|Poisonous Cloak Type I|Cape empoisonnée (Type I)|officielle|
|[equipment-06-6irTMXWYnysLljb6.htm](equipment/equipment-06-6irTMXWYnysLljb6.htm)|Wand of Mercy (2nd-level)|Baguette de miséricorde (rang 2)|libre|
|[equipment-06-8y3c9suf90ww60gU.htm](equipment/equipment-06-8y3c9suf90ww60gU.htm)|Shifter Prosthesis|Bras prothétique|libre|
|[equipment-06-b0kltGQGBUBHN8Ap.htm](equipment/equipment-06-b0kltGQGBUBHN8Ap.htm)|Druid's Crown|Couronne du druide|libre|
|[equipment-06-BKjwg0TEGioiYpz1.htm](equipment/equipment-06-BKjwg0TEGioiYpz1.htm)|Swallow-Spike|Pique avalée (rune)|libre|
|[equipment-06-BYr1DRYmiRsyr4po.htm](equipment/equipment-06-BYr1DRYmiRsyr4po.htm)|Aeon Stone (Sprouting)|Pierre d'éternité d'éclosion|libre|
|[equipment-06-cICCgx4NJKSREeZD.htm](equipment/equipment-06-cICCgx4NJKSREeZD.htm)|Spare Wax Cylinder|Cylindre de cire de rechange|libre|
|[equipment-06-CKmIgVzlcN7g1YT4.htm](equipment/equipment-06-CKmIgVzlcN7g1YT4.htm)|Wand of Hawthorn (2nd-level)|Baguette d'aubépine (rang 2)|libre|
|[equipment-06-Cu9r7tKPL5lYbFSR.htm](equipment/equipment-06-Cu9r7tKPL5lYbFSR.htm)|Silver Snake Cane|Canne serpent en argent|libre|
|[equipment-06-DIBJnuEG85FYwqnM.htm](equipment/equipment-06-DIBJnuEG85FYwqnM.htm)|Everair Mask (Moderate)|Masque à air continu modéré|libre|
|[equipment-06-Duj5vC7F5588ugS6.htm](equipment/equipment-06-Duj5vC7F5588ugS6.htm)|Cryolite Eye|Oeil de cryolite|libre|
|[equipment-06-EkZVXMdtqTTgahiJ.htm](equipment/equipment-06-EkZVXMdtqTTgahiJ.htm)|Charm of Resistance|Charme de résistance aux énergies|libre|
|[equipment-06-esk1K85W9uqxijnb.htm](equipment/equipment-06-esk1K85W9uqxijnb.htm)|Lucky Kitchen Witch|Sorcier de cuisine chanceux|libre|
|[equipment-06-fsqAB2lS9N8MRaO5.htm](equipment/equipment-06-fsqAB2lS9N8MRaO5.htm)|Midday Lantern (Lesser)|Lanterne de midi inférieure|libre|
|[equipment-06-FZ9ELiYCNBlyAz6X.htm](equipment/equipment-06-FZ9ELiYCNBlyAz6X.htm)|Dragon's Breath (2nd Level Spell)|Souffle de dragon de sort de rang 2 (rune)|libre|
|[equipment-06-gSSibF07emWGpGKw.htm](equipment/equipment-06-gSSibF07emWGpGKw.htm)|Dread (Lesser)|Effroi inférieur (rune)|libre|
|[equipment-06-hAPCk1A2a4kJDzhV.htm](equipment/equipment-06-hAPCk1A2a4kJDzhV.htm)|Spiritsight Ring|Anneau de vision spirituelle|libre|
|[equipment-06-hcGvN03ieNWlSQYa.htm](equipment/equipment-06-hcGvN03ieNWlSQYa.htm)|Cloud Pouch|Pochette de nuage|libre|
|[equipment-06-hmmDa6LCS22dZT7P.htm](equipment/equipment-06-hmmDa6LCS22dZT7P.htm)|Clandestine Cloak|Cape de clandestinité|libre|
|[equipment-06-I95zlGUDCply1Ydm.htm](equipment/equipment-06-I95zlGUDCply1Ydm.htm)|Insistent Door Knocker|Heurtoir insistant|libre|
|[equipment-06-i9hF185TRK0cH8B4.htm](equipment/equipment-06-i9hF185TRK0cH8B4.htm)|Demolishing|Démolisseuse (rune)|libre|
|[equipment-06-ifsBwfIhd6UcoUCI.htm](equipment/equipment-06-ifsBwfIhd6UcoUCI.htm)|Bellflower Toolbelt|Ceinture à outils du Campanule|libre|
|[equipment-06-IISieImYvSQ8AqmC.htm](equipment/equipment-06-IISieImYvSQ8AqmC.htm)|Phantasmal Doorknob|Poignée de porte imaginaire|libre|
|[equipment-06-J5MqY1P3JWrezcQX.htm](equipment/equipment-06-J5MqY1P3JWrezcQX.htm)|Primeval Mistletoe|Gui primitif|libre|
|[equipment-06-JN2wkB9JG4nklVKP.htm](equipment/equipment-06-JN2wkB9JG4nklVKP.htm)|Wand of Noisome Acid (2nd-Level Spell)|Baguette de pestilence acide (rang 2)|libre|
|[equipment-06-k8YWyDliEVj4iA0p.htm](equipment/equipment-06-k8YWyDliEVj4iA0p.htm)|Stag's Helm|Heaume du cerf|libre|
|[equipment-06-KaMBXc0Yqn2rAUec.htm](equipment/equipment-06-KaMBXc0Yqn2rAUec.htm)|Gingerbread House|Maison en pain d'épice|libre|
|[equipment-06-KlkPJgiCiHpuyNpv.htm](equipment/equipment-06-KlkPJgiCiHpuyNpv.htm)|Chronicler Wayfinder|Guide chroniqueur|libre|
|[equipment-06-Kx1Se9u0LU1ZS5R1.htm](equipment/equipment-06-Kx1Se9u0LU1ZS5R1.htm)|Architect's Pattern Book|Livre des croquis de l'architecte|libre|
|[equipment-06-l96Y0J3ZgB3dkpuF.htm](equipment/equipment-06-l96Y0J3ZgB3dkpuF.htm)|Lambent Perfume|Parfum vacillant|libre|
|[equipment-06-LBoSycoKAXrp76zA.htm](equipment/equipment-06-LBoSycoKAXrp76zA.htm)|Courtier's Pillow Book|Confessions intimes du courtisan|libre|
|[equipment-06-Ld6K4snDzKbNXy2g.htm](equipment/equipment-06-Ld6K4snDzKbNXy2g.htm)|Ring of Sigils (Greater)|Anneau de symboles supérieur|libre|
|[equipment-06-LDtIZ822rqBD88U2.htm](equipment/equipment-06-LDtIZ822rqBD88U2.htm)|Bewitching Bloom (Magnolia)|Fleur envoûtante (Magnolia)|libre|
|[equipment-06-LDVqBvMNNcRBucW1.htm](equipment/equipment-06-LDVqBvMNNcRBucW1.htm)|Bi-Resonant Wayfinder|Guide à double résonance|libre|
|[equipment-06-LM5zag6Ogv1fF5zz.htm](equipment/equipment-06-LM5zag6Ogv1fF5zz.htm)|Tooth and Claw Tattoo|Tatouage dent et griffe|libre|
|[equipment-06-LmveO9fSuEnkMKAH.htm](equipment/equipment-06-LmveO9fSuEnkMKAH.htm)|Vaultbreaker's Harness|Harnais du briseur de coffre|libre|
|[equipment-06-MhbeSE3RyBPyFQpq.htm](equipment/equipment-06-MhbeSE3RyBPyFQpq.htm)|Falconsight Eye|Oeil de vision du faucon|libre|
|[equipment-06-mKlUg7SWC5LcOqaj.htm](equipment/equipment-06-mKlUg7SWC5LcOqaj.htm)|Aim-Aiding|Aide à la visée (rune)|libre|
|[equipment-06-Mnj6K8aC7itqSIdg.htm](equipment/equipment-06-Mnj6K8aC7itqSIdg.htm)|Private Workshop|Atelier privé|libre|
|[equipment-06-NLC3c1mudubNNekY.htm](equipment/equipment-06-NLC3c1mudubNNekY.htm)|Clockwork Recorder|Enregistreur mécanique|libre|
|[equipment-06-nNtakXnSrcXWndBV.htm](equipment/equipment-06-nNtakXnSrcXWndBV.htm)|Traveler's Any-Tool|Outil multifonction de baroudeur|libre|
|[equipment-06-o7Tr4KbPcmd3zzd1.htm](equipment/equipment-06-o7Tr4KbPcmd3zzd1.htm)|Herd Mask|Masque du troupeau|libre|
|[equipment-06-OCJ2dnG9z5xGEh1q.htm](equipment/equipment-06-OCJ2dnG9z5xGEh1q.htm)|Sentinel Horn|Corne de sentinelle|libre|
|[equipment-06-odSvUKzphdwvDqgE.htm](equipment/equipment-06-odSvUKzphdwvDqgE.htm)|Aeon Stone (Gold Nodule)|Pierre d'éternité (nodule doré)|officielle|
|[equipment-06-P3zAUF2yTOeS1GSH.htm](equipment/equipment-06-P3zAUF2yTOeS1GSH.htm)|Light Writer|Enregistreur de lumière|libre|
|[equipment-06-pDaFDSY1keNfbdTl.htm](equipment/equipment-06-pDaFDSY1keNfbdTl.htm)|Grub Gloves (Lesser)|Gants à larve inférieurs|libre|
|[equipment-06-POETsrh8lz9VnBEG.htm](equipment/equipment-06-POETsrh8lz9VnBEG.htm)|Spirit-Singer (Handheld)|Esprit-chanteur portable|libre|
|[equipment-06-pRjJ2aBDpCH1NKRz.htm](equipment/equipment-06-pRjJ2aBDpCH1NKRz.htm)|Brain Cylinder|Cylindre cérébral|libre|
|[equipment-06-q3Eyp5ncO6lzvGnI.htm](equipment/equipment-06-q3Eyp5ncO6lzvGnI.htm)|Secret-Keeper's Mask (Father Skinsaw)|Masque des gardiens des secrets (Père écorcheur)|libre|
|[equipment-06-Q5YwZv3PbCHkEImh.htm](equipment/equipment-06-Q5YwZv3PbCHkEImh.htm)|Twilight Lantern (Lesser)|Lanterne crépusculaire inférieure|libre|
|[equipment-06-qmWlvoIlJRJ6pAeG.htm](equipment/equipment-06-qmWlvoIlJRJ6pAeG.htm)|Wand of Widening (2nd-Rank Spell)|Baguette d'élargissement (sort de rang 2)|libre|
|[equipment-06-QNPwzwKervKpk6YO.htm](equipment/equipment-06-QNPwzwKervKpk6YO.htm)|Ready|Parée (rune)|libre|
|[equipment-06-qO1swPrA14AnuyBD.htm](equipment/equipment-06-qO1swPrA14AnuyBD.htm)|Codex of Unimpeded Sight|Codex de vision sans entraves|libre|
|[equipment-06-QPz923dZeG1TajQE.htm](equipment/equipment-06-QPz923dZeG1TajQE.htm)|Trackless|Sans trace (rune)|libre|
|[equipment-06-Qqh586pudsEqITUk.htm](equipment/equipment-06-Qqh586pudsEqITUk.htm)|Energizing|Énergisante (rune)|libre|
|[equipment-06-Rhr6rkOwjFwCJV0T.htm](equipment/equipment-06-Rhr6rkOwjFwCJV0T.htm)|Wand of Choking Mist (Obscuring Mist)|Baguette de brume étouffante (Brume)|libre|
|[equipment-06-rmbvBjcDMDAZLJ7v.htm](equipment/equipment-06-rmbvBjcDMDAZLJ7v.htm)|Wand of Reaching (2nd-level)|Baguette d'atteinte (rang 2)|libre|
|[equipment-06-roeYtwlIe65BPMJ1.htm](equipment/equipment-06-roeYtwlIe65BPMJ1.htm)|Shifting|Changeante (rune)|libre|
|[equipment-06-RoySt3EKFgcicm7D.htm](equipment/equipment-06-RoySt3EKFgcicm7D.htm)|Wand of Shattering Images|Baguettes d'images fracassantes|libre|
|[equipment-06-s97FDCHi2UcfzKGn.htm](equipment/equipment-06-s97FDCHi2UcfzKGn.htm)|Ring of the Ram|Anneau du bélier|officielle|
|[equipment-06-slkvVyRBrUJyZQj8.htm](equipment/equipment-06-slkvVyRBrUJyZQj8.htm)|Retaliation (Lesser)|Représailles inférieures (rune)|libre|
|[equipment-06-TacKaUs8cIddqiCU.htm](equipment/equipment-06-TacKaUs8cIddqiCU.htm)|Choker of Elocution|Collier d'élocution|officielle|
|[equipment-06-tNoQ2moQzt0BaHm4.htm](equipment/equipment-06-tNoQ2moQzt0BaHm4.htm)|Pathfinder's Mentor|Mentor de l'éclaireur|libre|
|[equipment-06-toest6lPgkXHI3cy.htm](equipment/equipment-06-toest6lPgkXHI3cy.htm)|Sure-Step Crampons|Crampons de démarche assurée|libre|
|[equipment-06-tpkkAtlMIOL8TnW6.htm](equipment/equipment-06-tpkkAtlMIOL8TnW6.htm)|Quenching|Extinction (rune)|libre|
|[equipment-06-U0R8AHRhK0Wu9r2i.htm](equipment/equipment-06-U0R8AHRhK0Wu9r2i.htm)|Blast Foot|Pied explosif|libre|
|[equipment-06-U4FSOEH2Z6NDpN39.htm](equipment/equipment-06-U4FSOEH2Z6NDpN39.htm)|Wand of Rolling Flames (2nd-level)|Baguette des flammes ronflantes (rang 2)|libre|
|[equipment-06-UkEDBhYxHWcTaMTG.htm](equipment/equipment-06-UkEDBhYxHWcTaMTG.htm)|Warding Tattoo (Wave)|Tatouage de protection (Vagues)|libre|
|[equipment-06-uZvIIfZ04Q0S8UBE.htm](equipment/equipment-06-uZvIIfZ04Q0S8UBE.htm)|Festrem Mortu|Festrem mortu|libre|
|[equipment-06-V5kv7e1r2qV2k3lf.htm](equipment/equipment-06-V5kv7e1r2qV2k3lf.htm)|Aeon Stone (Western Star)|Pierre d'éternité (étoile de l'ouest)|libre|
|[equipment-06-vT2OibHsrq7M6MMO.htm](equipment/equipment-06-vT2OibHsrq7M6MMO.htm)|Bestiary of Metamorphosis|Bestiaire de métamorphose|libre|
|[equipment-06-WQ3yrHtv2fN5UO57.htm](equipment/equipment-06-WQ3yrHtv2fN5UO57.htm)|Lantern of Empty Light|Lanterne de lumière vide|libre|
|[equipment-06-XFJD6eOX6H9sLcbC.htm](equipment/equipment-06-XFJD6eOX6H9sLcbC.htm)|Warding Tattoo (Trail)|Tatouage de protection (Sentier)|libre|
|[equipment-06-xH51QHrc9073UzH7.htm](equipment/equipment-06-xH51QHrc9073UzH7.htm)|Goz Mask (Greater)|Masque goz supérieur|libre|
|[equipment-06-XrM3Cza8vM6Jzv98.htm](equipment/equipment-06-XrM3Cza8vM6Jzv98.htm)|Endless Grimoire|Grimoire sans fin|libre|
|[equipment-06-YBKhjWqFuvgkArba.htm](equipment/equipment-06-YBKhjWqFuvgkArba.htm)|Cassisian Helmet|Casque cassissien|libre|
|[equipment-06-yK5maWEcZ0pNnFwq.htm](equipment/equipment-06-yK5maWEcZ0pNnFwq.htm)|Undertaker's Manifest|Manifeste du croque-mort|libre|
|[equipment-06-YoRL8PkjYInpmBWl.htm](equipment/equipment-06-YoRL8PkjYInpmBWl.htm)|Stony Hag Eye|Oeil de guenaude lapidaire|libre|
|[equipment-06-Yq3l3eB70BsbQFj6.htm](equipment/equipment-06-Yq3l3eB70BsbQFj6.htm)|Wand of Mental Purification (2nd-level)|Baguette de purification mentale (rang 2)|libre|
|[equipment-06-yWlQQOs0A3ApEc1J.htm](equipment/equipment-06-yWlQQOs0A3ApEc1J.htm)|Wand of Teeming Ghosts (2nd-Level Spell)|Baguette d'esprits grouillants (rang 2)|libre|
|[equipment-06-zaJ4HSNa6kMozYvM.htm](equipment/equipment-06-zaJ4HSNa6kMozYvM.htm)|Wand of Legerdemain (2nd-level)|Baguette de prestidigitation (rang 2)|libre|
|[equipment-06-ZJ3ahspZOXL4CK4J.htm](equipment/equipment-06-ZJ3ahspZOXL4CK4J.htm)|Wand of Hopeless Night (2nd-Level Spell)|Baguette de noir désespoir (rang 2)|libre|
|[equipment-06-zNtC1DsiAUYeHfDN.htm](equipment/equipment-06-zNtC1DsiAUYeHfDN.htm)|Wand of Hybrid Form (2nd-level)|Baguette de forme hybride (rang 2)|libre|
|[equipment-06-ZW8W30XqgFf1KB3I.htm](equipment/equipment-06-ZW8W30XqgFf1KB3I.htm)|Warcaller's Chime of Blasting|Carillon explosif du belliqueux|libre|
|[equipment-07-0HvjITehMdaFYUkb.htm](equipment/equipment-07-0HvjITehMdaFYUkb.htm)|Cloak of Illusions|Cape des illusions|libre|
|[equipment-07-14rbefsoClgClRQ8.htm](equipment/equipment-07-14rbefsoClgClRQ8.htm)|Ring of Sustenance|Anneau de subsistance|libre|
|[equipment-07-31tSOy1iHCjNNd4N.htm](equipment/equipment-07-31tSOy1iHCjNNd4N.htm)|Everyneed Pack (Greater)|Sac de tous les besoins supérieur|libre|
|[equipment-07-3cV1kzaP1ofw3xtU.htm](equipment/equipment-07-3cV1kzaP1ofw3xtU.htm)|Fearless Sash|Écharpe téméraire|libre|
|[equipment-07-3obTVPkQ5mGwXJat.htm](equipment/equipment-07-3obTVPkQ5mGwXJat.htm)|Aeon Stone (Smoothing)|Pierre d'éternité apaisante|libre|
|[equipment-07-3TQ9oSETsOHcHRBF.htm](equipment/equipment-07-3TQ9oSETsOHcHRBF.htm)|Bravery Baldric (Restoration)|Baudrier de bravoure (Restauration)|libre|
|[equipment-07-4DXupoMmwenFn4Kc.htm](equipment/equipment-07-4DXupoMmwenFn4Kc.htm)|Deathdrinking|Buveuse de mort (rune)|libre|
|[equipment-07-4J87czEOWwbGXE3r.htm](equipment/equipment-07-4J87czEOWwbGXE3r.htm)|Reflexive Tattoo|Tatouage réfléchi|libre|
|[equipment-07-5rpNqGkPSMkXKv0B.htm](equipment/equipment-07-5rpNqGkPSMkXKv0B.htm)|Hallajin Key|Clé de Hallajin|libre|
|[equipment-07-5V9bgqgQY1CHLd40.htm](equipment/equipment-07-5V9bgqgQY1CHLd40.htm)|Wand of Continuation (2nd-Rank Spell)|Baguette de prolongation (sort de rang 2)|libre|
|[equipment-07-6XX3tYxkyQXCMAbd.htm](equipment/equipment-07-6XX3tYxkyQXCMAbd.htm)|Called (Lastwall)|Appelé (Dernier-Rempart)|libre|
|[equipment-07-7Qc3KezVroqC7Jve.htm](equipment/equipment-07-7Qc3KezVroqC7Jve.htm)|Jar of Shifting Sands|Jarre de sable changeant|libre|
|[equipment-07-7TQw7V1zZKl0a0Xz.htm](equipment/equipment-07-7TQw7V1zZKl0a0Xz.htm)|Bottled Air|Air en bouteille|libre|
|[equipment-07-A4BhFOb4iQtJYYKq.htm](equipment/equipment-07-A4BhFOb4iQtJYYKq.htm)|Dragon's Eye Charm|Charme d'oeil de dragon|libre|
|[equipment-07-aBVrNIPoPGOYxm80.htm](equipment/equipment-07-aBVrNIPoPGOYxm80.htm)|Decanter of Endless Water|Carafe intarissable|officielle|
|[equipment-07-azZNewz2sEWwZwtZ.htm](equipment/equipment-07-azZNewz2sEWwZwtZ.htm)|Resolute Mind Wrap|Turban de l'esprit résolu|libre|
|[equipment-07-bfJI3xeIWaqcYRlU.htm](equipment/equipment-07-bfJI3xeIWaqcYRlU.htm)|Winder's Ring|Anneau du remontoir|libre|
|[equipment-07-BOjMAfhfDYc3MTLQ.htm](equipment/equipment-07-BOjMAfhfDYc3MTLQ.htm)|Collar of the Eternal Bond|Collier du lien éternel|libre|
|[equipment-07-cHCaDiKel0qAIQmC.htm](equipment/equipment-07-cHCaDiKel0qAIQmC.htm)|Conducting|Conductrice (rune)|libre|
|[equipment-07-dChkPrLJfZDPBvWR.htm](equipment/equipment-07-dChkPrLJfZDPBvWR.htm)|Venomed Tongue|Langue venimeuse|libre|
|[equipment-07-ecqz1iUGtyQEkZwy.htm](equipment/equipment-07-ecqz1iUGtyQEkZwy.htm)|Boots of Bounding|Bottes de saut|officielle|
|[equipment-07-EHMGDwY0oSZAHz8Y.htm](equipment/equipment-07-EHMGDwY0oSZAHz8Y.htm)|Hairpin of Blooming Flowers|Épingle à cheveux de fleurs épanouies|libre|
|[equipment-07-ErqxXatIYbv2WcsE.htm](equipment/equipment-07-ErqxXatIYbv2WcsE.htm)|Bewitching Bloom (Bellflower)|Fleur envoûtante (Campanule)|libre|
|[equipment-07-EVq8BRD0HO63S1GF.htm](equipment/equipment-07-EVq8BRD0HO63S1GF.htm)|Anylength Rope (Greater)|Corde toute longueur supérieure|libre|
|[equipment-07-faKyy6ETkDgrUnvf.htm](equipment/equipment-07-faKyy6ETkDgrUnvf.htm)|Ring of Wizardry (Type I)|Anneau des arcanes (Type I)|libre|
|[equipment-07-fo6Yhq5mbQXsnZs0.htm](equipment/equipment-07-fo6Yhq5mbQXsnZs0.htm)|Wounding|Sanglante (rune)|libre|
|[equipment-07-ftSaD4c5io4pP4OB.htm](equipment/equipment-07-ftSaD4c5io4pP4OB.htm)|Gloves of Carelessness|Ceinture de négligence|libre|
|[equipment-07-FUzPnIXLHZenTtYo.htm](equipment/equipment-07-FUzPnIXLHZenTtYo.htm)|Crown of Insight|Couronne de perspicacité|libre|
|[equipment-07-fVOObByW6XHADQ2J.htm](equipment/equipment-07-fVOObByW6XHADQ2J.htm)|Clawed Bracers|Bracelets griffus|libre|
|[equipment-07-fWgH0JxNCOpI7SVr.htm](equipment/equipment-07-fWgH0JxNCOpI7SVr.htm)|Beastmaster's Sigil (Greater)|Symbole du maître des bêtes supérieur|libre|
|[equipment-07-GAffuPaxuhSku90B.htm](equipment/equipment-07-GAffuPaxuhSku90B.htm)|Ring of Sneering Charity|Anneau de la charité narquoise|libre|
|[equipment-07-geXmZMgU7kVbLHIj.htm](equipment/equipment-07-geXmZMgU7kVbLHIj.htm)|Ring of Observation (Moderate)|Anneau d'observation modéré|libre|
|[equipment-07-gfBAxuitJje6NL7G.htm](equipment/equipment-07-gfBAxuitJje6NL7G.htm)|Rope of Climbing (Greater)|Corde d'escalade supérieur|libre|
|[equipment-07-GNX0BNOoCSOYPedi.htm](equipment/equipment-07-GNX0BNOoCSOYPedi.htm)|Flurrying|Diluvienne (rune)|libre|
|[equipment-07-GUyavibqPPxwFxyR.htm](equipment/equipment-07-GUyavibqPPxwFxyR.htm)|Swarmform Collar|Collier Forme-nuée|libre|
|[equipment-07-GW45HOqel23w465z.htm](equipment/equipment-07-GW45HOqel23w465z.htm)|Admirer's Bouquet|Bouquet de l'admirateur|libre|
|[equipment-07-Gzz2F90inajTPvcX.htm](equipment/equipment-07-Gzz2F90inajTPvcX.htm)|Mirror of Sleeping Vigil|Miroir de la sentinelle assoupie|libre|
|[equipment-07-hBGFCZbI9nAjSdfE.htm](equipment/equipment-07-hBGFCZbI9nAjSdfE.htm)|Drover's Band|Protège-bracelet du berger|libre|
|[equipment-07-Hqu2CmkLIE5Ov41i.htm](equipment/equipment-07-Hqu2CmkLIE5Ov41i.htm)|Wand of Spiritual Warfare (2nd-Level Spell)|Baguette de guerre spirituelle (rang 2)|libre|
|[equipment-07-Jn0PNvEONsnhYFO9.htm](equipment/equipment-07-Jn0PNvEONsnhYFO9.htm)|Veiled Figurehead|Figure de proue voilée|libre|
|[equipment-07-JrvrGairz2itBqhj.htm](equipment/equipment-07-JrvrGairz2itBqhj.htm)|Red Thread Knot|Ficelle de nœud rouge|libre|
|[equipment-07-Jx7cxxTqOENzlGaj.htm](equipment/equipment-07-Jx7cxxTqOENzlGaj.htm)|Enigma Mirror|Miroir énigmatique|libre|
|[equipment-07-K0uJJSso0WGpFzYe.htm](equipment/equipment-07-K0uJJSso0WGpFzYe.htm)|Swiftmount Saddle|Selle de monte rapide|libre|
|[equipment-07-KAOlwH6kBCCvEYbW.htm](equipment/equipment-07-KAOlwH6kBCCvEYbW.htm)|Verdant Branch|Branche verdoyante|libre|
|[equipment-07-kOEZCUTCPCqCFoJf.htm](equipment/equipment-07-kOEZCUTCPCqCFoJf.htm)|Deathless|Impérissable (rune)|libre|
|[equipment-07-kQiO32jZmuxAiqGV.htm](equipment/equipment-07-kQiO32jZmuxAiqGV.htm)|Root Boots|Bottes racine|libre|
|[equipment-07-KR35T5By2iaLKEwH.htm](equipment/equipment-07-KR35T5By2iaLKEwH.htm)|Hat of Disagreeable Disguise (Greater)|Foulard de déguisement désagréable supérieur|libre|
|[equipment-07-kSaUlWgYMywIRV3C.htm](equipment/equipment-07-kSaUlWgYMywIRV3C.htm)|Aeon Stone (Delaying)|Pierre d'éternité retardatrice|libre|
|[equipment-07-ktHdg7fqKtl5hXKJ.htm](equipment/equipment-07-ktHdg7fqKtl5hXKJ.htm)|Fossil Fragment (Deinonychus Claw)|Fragment de fossile (Griffe de Déinonychus)|libre|
|[equipment-07-LH9nHvta0q39JD86.htm](equipment/equipment-07-LH9nHvta0q39JD86.htm)|Wind-Catcher|Attrape-vent (rune)|libre|
|[equipment-07-lxIwa8sbAsfyREYE.htm](equipment/equipment-07-lxIwa8sbAsfyREYE.htm)|Ash Gown|Toge cendreuse|libre|
|[equipment-07-M2CPAgSAoEL4oawq.htm](equipment/equipment-07-M2CPAgSAoEL4oawq.htm)|Aeon Stone (Nourishing)|Pierre d'éternité nourricière|libre|
|[equipment-07-M450qjybosAkGcdN.htm](equipment/equipment-07-M450qjybosAkGcdN.htm)|Hand-Hewed Face|Visage sculpté à la main|libre|
|[equipment-07-MNBnZn0b80Q7yHJM.htm](equipment/equipment-07-MNBnZn0b80Q7yHJM.htm)|Cloak of Elvenkind|Cape elfique|officielle|
|[equipment-07-ngz7dYysC1NkBBRK.htm](equipment/equipment-07-ngz7dYysC1NkBBRK.htm)|Retrieval Belt|Ceinture de récupération|libre|
|[equipment-07-nI5x32JWjcEqYCq6.htm](equipment/equipment-07-nI5x32JWjcEqYCq6.htm)|Sealing Chest (Moderate)|Coffre scellé modéré|libre|
|[equipment-07-nOI2irf1OifSdp9P.htm](equipment/equipment-07-nOI2irf1OifSdp9P.htm)|Cloak of Immolation|Cape d'immolation|libre|
|[equipment-07-nv9riwCAkAXUJYX2.htm](equipment/equipment-07-nv9riwCAkAXUJYX2.htm)|Ring of Ravenousness|Anneau de voracité|libre|
|[equipment-07-o65PFCSOyMje6fwi.htm](equipment/equipment-07-o65PFCSOyMje6fwi.htm)|Reinforcing Rune (Lesser)|Renforcement inférieur (rune)|libre|
|[equipment-07-oqqHMA3eHBwJ2BlB.htm](equipment/equipment-07-oqqHMA3eHBwJ2BlB.htm)|Barding Saddle|Selle de barde|libre|
|[equipment-07-Oudcra1CGDmUq0BH.htm](equipment/equipment-07-Oudcra1CGDmUq0BH.htm)|Unmemorable Mantle|Manteau insignifiant|libre|
|[equipment-07-OVyGHaHjYI2DBV0h.htm](equipment/equipment-07-OVyGHaHjYI2DBV0h.htm)|Doctrine of Blissful Eternity|Doctrine de l'éternité bienheureuse|libre|
|[equipment-07-peoheZMxdPHUNo93.htm](equipment/equipment-07-peoheZMxdPHUNo93.htm)|Alacritous Horseshoes|Fers à cheval d'alacrité|libre|
|[equipment-07-QgNp6uyKuV2PiNbf.htm](equipment/equipment-07-QgNp6uyKuV2PiNbf.htm)|Wand of Fey Flames|Baguette des feux féeriques|libre|
|[equipment-07-qtT0cgt7uUSKY2qK.htm](equipment/equipment-07-qtT0cgt7uUSKY2qK.htm)|Maestro's Chair|Fauteuil de maestro|libre|
|[equipment-07-qzgi4PCcvpH0QPVj.htm](equipment/equipment-07-qzgi4PCcvpH0QPVj.htm)|Ethersight Ring|Anneau de vision éthérée|libre|
|[equipment-07-rJan8OyK6gmtxXmt.htm](equipment/equipment-07-rJan8OyK6gmtxXmt.htm)|Queasy Lantern (Lesser)|Lanterne barbouilleuse inférieur|libre|
|[equipment-07-RjJw7iHantxqeJu1.htm](equipment/equipment-07-RjJw7iHantxqeJu1.htm)|Wondrous Figurine (Jade Serpent)|Figurine merveilleuse (Serpent de jade)|officielle|
|[equipment-07-skBa6D1uxb0b2USn.htm](equipment/equipment-07-skBa6D1uxb0b2USn.htm)|Slippers of Spider Climbing|Chaussons de l'araignée|officielle|
|[equipment-07-SW6JVF5SP59W8B0Q.htm](equipment/equipment-07-SW6JVF5SP59W8B0Q.htm)|Warding Statuette|Statuette de protection|libre|
|[equipment-07-tf69NMnUoUAYrWtj.htm](equipment/equipment-07-tf69NMnUoUAYrWtj.htm)|Wand of the Spider (2nd-Level Spell)|Baguette de l'araignée (rang 2)|libre|
|[equipment-07-TxB1a2DrhRZABRjV.htm](equipment/equipment-07-TxB1a2DrhRZABRjV.htm)|Cloak of Thirsty Fronds|Cape des frondaisons assoiffées|libre|
|[equipment-07-UMTeKNfyj95JMwMM.htm](equipment/equipment-07-UMTeKNfyj95JMwMM.htm)|Vaporous Pipe|Pipe vaporeuse|libre|
|[equipment-07-V0ryBu9M5OPd4Wub.htm](equipment/equipment-07-V0ryBu9M5OPd4Wub.htm)|Warding Tattoo (Fiend)|Tatouage de protection (Fiélons)|libre|
|[equipment-07-VMIwKvpQHh91cQoa.htm](equipment/equipment-07-VMIwKvpQHh91cQoa.htm)|Clockwork Heels|Talons mécanisés|libre|
|[equipment-07-VMYiP4xRd1hPa5ny.htm](equipment/equipment-07-VMYiP4xRd1hPa5ny.htm)|Ghost Scarf|Écharpe fantomatique|libre|
|[equipment-07-WJTtO3mI3BlYyBMg.htm](equipment/equipment-07-WJTtO3mI3BlYyBMg.htm)|Morphing Weapon|Arme morphique|libre|
|[equipment-07-WPhrH1zCbP2v10aP.htm](equipment/equipment-07-WPhrH1zCbP2v10aP.htm)|Energy Robe of Fire|Robe d'énergie de feu|libre|
|[equipment-07-WPSp5MLb0VOfmUqH.htm](equipment/equipment-07-WPSp5MLb0VOfmUqH.htm)|Masquerade Scarf (Greater)|Foulard de mascarade supérieur|libre|
|[equipment-07-WvrPq0UGEX3wJg4a.htm](equipment/equipment-07-WvrPq0UGEX3wJg4a.htm)|Resonating Fork (Greater)|Diapason résonant supérieur|libre|
|[equipment-07-Yed5EEEWdJyf2AVX.htm](equipment/equipment-07-Yed5EEEWdJyf2AVX.htm)|Restful Tent|Tente de repos|libre|
|[equipment-07-yGgsoSA8EaaEsYZn.htm](equipment/equipment-07-yGgsoSA8EaaEsYZn.htm)|Rooting|Enracinante (rune)|libre|
|[equipment-07-YO2X0EE3txascuuP.htm](equipment/equipment-07-YO2X0EE3txascuuP.htm)|Wondrous Figurine (Rubber Bear)|Figurine merveilleuse (Ours en caoutchouc)|libre|
|[equipment-07-Z5FvYWLEpWVo3PUF.htm](equipment/equipment-07-Z5FvYWLEpWVo3PUF.htm)|Size-Changing|Changement de taille (rune)|libre|
|[equipment-07-znWf2DgWyElwzQwJ.htm](equipment/equipment-07-znWf2DgWyElwzQwJ.htm)|Detector Stone|Pierre de détection|libre|
|[equipment-07-zP7Eo57IYMUKxwPO.htm](equipment/equipment-07-zP7Eo57IYMUKxwPO.htm)|Eternal Eruption of Blackpeak|Éternelle éruption de Picnoir|libre|
|[equipment-07-Zun8aKbODnBFeut6.htm](equipment/equipment-07-Zun8aKbODnBFeut6.htm)|Necklace of Fireballs II|Collier à boules de feu II|officielle|
|[equipment-08-02q8s6sSicMkhs1l.htm](equipment/equipment-08-02q8s6sSicMkhs1l.htm)|Bands of Force|Bandes de force|libre|
|[equipment-08-04V1qwob0JGPEx3k.htm](equipment/equipment-08-04V1qwob0JGPEx3k.htm)|Wondrous Figurine (Bismuth Leopards)|Figurine merveilleuse (Léopard de bismuth)|libre|
|[equipment-08-0OzL3DdtTnZE91MU.htm](equipment/equipment-08-0OzL3DdtTnZE91MU.htm)|Smoky Hag Eye|Oeil de guenaude fumant|libre|
|[equipment-08-25WDsZPxt4tjgovE.htm](equipment/equipment-08-25WDsZPxt4tjgovE.htm)|Wand of Mental Purification (3rd-level)|Baguette de purification mentale (rang 3)|libre|
|[equipment-08-2APHVWkqnFvecrfx.htm](equipment/equipment-08-2APHVWkqnFvecrfx.htm)|Bootstrap Respirator|Appareil respiratoire à sangles|libre|
|[equipment-08-2cWs7heYDt9jCSJ4.htm](equipment/equipment-08-2cWs7heYDt9jCSJ4.htm)|Fortune's Coin|Pièce porte-bonheur|libre|
|[equipment-08-3LJ3l5QKyjxiVBbu.htm](equipment/equipment-08-3LJ3l5QKyjxiVBbu.htm)|Thorn Triad (Greater)|Triade d'épines supérieure|libre|
|[equipment-08-4TJf8zJ2yRzBy74v.htm](equipment/equipment-08-4TJf8zJ2yRzBy74v.htm)|Gossip's Eye|Oeil de la commère|libre|
|[equipment-08-57vUQS7HSln47EH8.htm](equipment/equipment-08-57vUQS7HSln47EH8.htm)|Harrow Spellcards|Cartes de sort du Tourment|libre|
|[equipment-08-5Dk8NQMW8NXFEKPy.htm](equipment/equipment-08-5Dk8NQMW8NXFEKPy.htm)|Book of Warding Prayers|Livre des prières de protection|libre|
|[equipment-08-5W86t6I92rRNSmwR.htm](equipment/equipment-08-5W86t6I92rRNSmwR.htm)|Trinity Geode (Greater)|Géode de la trinité supérieure|libre|
|[equipment-08-5YZmX9AdhQxXaOzX.htm](equipment/equipment-08-5YZmX9AdhQxXaOzX.htm)|Dragon's Breath (3rd Level Spell)|Souffle de dragon de sort de rang 3 (rune)|libre|
|[equipment-08-6llILJtsTPgtYXgx.htm](equipment/equipment-08-6llILJtsTPgtYXgx.htm)|Astral|Astrale (rune)|libre|
|[equipment-08-6wVWwpL9pYr3yQtt.htm](equipment/equipment-08-6wVWwpL9pYr3yQtt.htm)|Rod of Wonder|Sceptre merveilleux|officielle|
|[equipment-08-7eggb7exEAbZABrd.htm](equipment/equipment-08-7eggb7exEAbZABrd.htm)|Bagpipes of Turmoil (Greater)|Cornemuse du tumulte supérieure|libre|
|[equipment-08-8c51yvwHqXqoHsEl.htm](equipment/equipment-08-8c51yvwHqXqoHsEl.htm)|Globe of Shrouds|Globe de dissimulation|libre|
|[equipment-08-8Rels5G8re0MXFAS.htm](equipment/equipment-08-8Rels5G8re0MXFAS.htm)|Aquarium Lamp|Lampe aquarium|libre|
|[equipment-08-8x8zqH4pJKJRlyAj.htm](equipment/equipment-08-8x8zqH4pJKJRlyAj.htm)|Moonstone Diadem|Diadème pierre de lune|libre|
|[equipment-08-9wjpW7vZJw5xA1yG.htm](equipment/equipment-08-9wjpW7vZJw5xA1yG.htm)|Sigil of the First Clan|Symbole du Premier clan|libre|
|[equipment-08-A2Z7Mh8A59wZb5vv.htm](equipment/equipment-08-A2Z7Mh8A59wZb5vv.htm)|Gliding|Planante (rune)|libre|
|[equipment-08-ADruUHNWh2AuxJFb.htm](equipment/equipment-08-ADruUHNWh2AuxJFb.htm)|Waffle Iron (Mithral)|Gaufrier en aubargent|libre|
|[equipment-08-alV162EF3qZrbxt0.htm](equipment/equipment-08-alV162EF3qZrbxt0.htm)|Five-Feather Wreath (Greater)|Couronne à cinq plumes supérieure|libre|
|[equipment-08-auEm7vmpG9V8xpAJ.htm](equipment/equipment-08-auEm7vmpG9V8xpAJ.htm)|Jolt Coil (Greater)|Bobine d'électrochoc supérieure|libre|
|[equipment-08-AzLEUTp4RHYAoXIe.htm](equipment/equipment-08-AzLEUTp4RHYAoXIe.htm)|Wand of Reaching (3rd-level)|Baguette d'atteinte (rang 3)|libre|
|[equipment-08-bh3zC7WcVlM0qgYZ.htm](equipment/equipment-08-bh3zC7WcVlM0qgYZ.htm)|Fleshgem (Earthspeaker)|Gemme de chair d'orateur de la terre|libre|
|[equipment-08-bL7HnxNYGq6nTpgp.htm](equipment/equipment-08-bL7HnxNYGq6nTpgp.htm)|Madcap Top|Toupie exentrique|libre|
|[equipment-08-bv1bQGFywtNkYWWD.htm](equipment/equipment-08-bv1bQGFywtNkYWWD.htm)|Wand of Dumbfounding Doom (3rd-level)|Baguette de la fatalité abrutissante (rang 3)|libre|
|[equipment-08-c2Oa9UbhjwAsZaPp.htm](equipment/equipment-08-c2Oa9UbhjwAsZaPp.htm)|Wand of Smoldering Fireballs (3rd-Level Spell)|Baguette de boules de feu incendiaires (rang 3)|libre|
|[equipment-08-c8v8gWJ87xiUVnsb.htm](equipment/equipment-08-c8v8gWJ87xiUVnsb.htm)|Horrid Figurine|Figurine horrible|libre|
|[equipment-08-C9wOlvuVCjVbz1YQ.htm](equipment/equipment-08-C9wOlvuVCjVbz1YQ.htm)|Bloodbane|Vengeresse (rune)|libre|
|[equipment-08-cbbPoYOil7DZO4mk.htm](equipment/equipment-08-cbbPoYOil7DZO4mk.htm)|Stalk Goggles (Major)|Lunettes à tiges majeures|libre|
|[equipment-08-cHP1s9Ntki9B1HgM.htm](equipment/equipment-08-cHP1s9Ntki9B1HgM.htm)|Drums of War (Greater)|Tambour de guerre supérieur|libre|
|[equipment-08-Ck2nuiQXkoURspvy.htm](equipment/equipment-08-Ck2nuiQXkoURspvy.htm)|Tooth and Claw Tattoo (Greater)|Tatouage dent et griffe supérieur|libre|
|[equipment-08-cvb47A6K1w7RfNiv.htm](equipment/equipment-08-cvb47A6K1w7RfNiv.htm)|Fanged (Greater)|Crocs supérieurs (rune)|libre|
|[equipment-08-D7ARQDAr9tO61UqY.htm](equipment/equipment-08-D7ARQDAr9tO61UqY.htm)|Seer's Flute (Greater)|Flûte du devin supérieure|libre|
|[equipment-08-dFYEGk1nUlHqQ5qy.htm](equipment/equipment-08-dFYEGk1nUlHqQ5qy.htm)|Lightweave Scarf|Écharpe en tissu léger|libre|
|[equipment-08-dPQhe0tAa5LkPFoW.htm](equipment/equipment-08-dPQhe0tAa5LkPFoW.htm)|Swarmeater's Clasp|Fermoir du mange-nuée|libre|
|[equipment-08-DsZ48n6mn8uW5EIP.htm](equipment/equipment-08-DsZ48n6mn8uW5EIP.htm)|Tallowheart Mass|Agglomérat de suif|libre|
|[equipment-08-dTxbaa7HSiJbIuNN.htm](equipment/equipment-08-dTxbaa7HSiJbIuNN.htm)|Thundering|Tonnerre (rune)|officielle|
|[equipment-08-EiCCWG0Xta0ZBUfA.htm](equipment/equipment-08-EiCCWG0Xta0ZBUfA.htm)|Steam Winch|Treuil à vapeur|libre|
|[equipment-08-EjV3Pb13DLzGTCcY.htm](equipment/equipment-08-EjV3Pb13DLzGTCcY.htm)|Giant-Killing|Tueuse de géants (arme)|libre|
|[equipment-08-eSjQgsl3pYkirOwk.htm](equipment/equipment-08-eSjQgsl3pYkirOwk.htm)|Resilient|Résilience (rune)|libre|
|[equipment-08-FMtUVzTDiTbxXMN9.htm](equipment/equipment-08-FMtUVzTDiTbxXMN9.htm)|Flaming Star (Greater)|Étoile enflammée supérieure|libre|
|[equipment-08-fyiW23MFe8p06KL5.htm](equipment/equipment-08-fyiW23MFe8p06KL5.htm)|Decaying|Décomposante (rune)|libre|
|[equipment-08-GkQJfRZC749piiBr.htm](equipment/equipment-08-GkQJfRZC749piiBr.htm)|Tome of Scintillating Sleet|Tome de grésil scintillant|libre|
|[equipment-08-GM9Ky5LEkPHZqme0.htm](equipment/equipment-08-GM9Ky5LEkPHZqme0.htm)|Alluring Scarf|Écharpe séduisante|libre|
|[equipment-08-gxzlLfvXeZ8BGlf4.htm](equipment/equipment-08-gxzlLfvXeZ8BGlf4.htm)|Crown of the Fire Eater|Couronne du mangeur de feu|libre|
|[equipment-08-HnOWpyuCHwewPZMu.htm](equipment/equipment-08-HnOWpyuCHwewPZMu.htm)|Pickled Demon Tongue (Greater)|Langue de démon marinée supérieure|libre|
|[equipment-08-ImokJgCMIb0dFyKH.htm](equipment/equipment-08-ImokJgCMIb0dFyKH.htm)|Summoning Handscroll|Papyrus de convocation|libre|
|[equipment-08-j6laknUcsOA6utvf.htm](equipment/equipment-08-j6laknUcsOA6utvf.htm)|Warcaller's Chime of Refuge|Carillon de refuge du belliqueux|libre|
|[equipment-08-jfLMxToNvg2VzjSw.htm](equipment/equipment-08-jfLMxToNvg2VzjSw.htm)|Tyrant's Writs|Écrits du Tyran|libre|
|[equipment-08-JOswp4pSSevZFeNG.htm](equipment/equipment-08-JOswp4pSSevZFeNG.htm)|Stone of Encouragement (Greater)|Pierre d'encouragement supérieure|libre|
|[equipment-08-k9BYXA4b7Z4Hy6lZ.htm](equipment/equipment-08-k9BYXA4b7Z4Hy6lZ.htm)|Deafening Music Box|Boîte de musique assourdissante|libre|
|[equipment-08-k9DHki1JS7FBG0Jx.htm](equipment/equipment-08-k9DHki1JS7FBG0Jx.htm)|Spellbook of Redundant Enchantment|Grimoire d'enchantement redondant|libre|
|[equipment-08-kJSB09q2KTAcWZy9.htm](equipment/equipment-08-kJSB09q2KTAcWZy9.htm)|Bag of Cats|Sac des félins|libre|
|[equipment-08-Lafm0hXaAGRQTmN4.htm](equipment/equipment-08-Lafm0hXaAGRQTmN4.htm)|Extra Lung|Poumon supplémentaire|libre|
|[equipment-08-LD2cFdr3IntL71oP.htm](equipment/equipment-08-LD2cFdr3IntL71oP.htm)|Backfire Mantle (Greater)|Manteau de contre-feu supérieur|libre|
|[equipment-08-LiJMupjynmkM5Mld.htm](equipment/equipment-08-LiJMupjynmkM5Mld.htm)|Slick (Greater)|Glissante supérieure (rune)|officielle|
|[equipment-08-LKtbZiY5lmD7ohcD.htm](equipment/equipment-08-LKtbZiY5lmD7ohcD.htm)|Bleachguard Doll|Poupée garde affadissement|libre|
|[equipment-08-LqAf5RwxiZJredii.htm](equipment/equipment-08-LqAf5RwxiZJredii.htm)|Brightbloom Posy|Bouquet de fleurs lumineuses|libre|
|[equipment-08-Lw3B9DpnyrpXD9Di.htm](equipment/equipment-08-Lw3B9DpnyrpXD9Di.htm)|Energy-Resistant|Résistance à l'énergie (rune)|libre|
|[equipment-08-m3XmIRwYoGGxZQgu.htm](equipment/equipment-08-m3XmIRwYoGGxZQgu.htm)|Wand of Dazzling Rays (3rd-level)|Baguette des rayons éblouissants (rang 3)|libre|
|[equipment-08-M5M1WJ5KzbYfRGsY.htm](equipment/equipment-08-M5M1WJ5KzbYfRGsY.htm)|Frost|Froid (rune)|officielle|
|[equipment-08-MmQw3qu51AuqXKCC.htm](equipment/equipment-08-MmQw3qu51AuqXKCC.htm)|Frog Chair|Chaise grenouille|libre|
|[equipment-08-mOOJwTOxEFYfi9LV.htm](equipment/equipment-08-mOOJwTOxEFYfi9LV.htm)|Desolation Locket|Médaillon de la désolation|libre|
|[equipment-08-mUJc8M1gKG1HkH4m.htm](equipment/equipment-08-mUJc8M1gKG1HkH4m.htm)|Staring Skull|Tête de mort fixante|libre|
|[equipment-08-neanlQgRTZQh2zy7.htm](equipment/equipment-08-neanlQgRTZQh2zy7.htm)|Rime Crystal (Greater)|Cristal gelé supérieur|libre|
|[equipment-08-NiO3YZ6e9RmajCS7.htm](equipment/equipment-08-NiO3YZ6e9RmajCS7.htm)|Umbral Wings|Ailes ombrales|libre|
|[equipment-08-NmhKGflbT5NnKduz.htm](equipment/equipment-08-NmhKGflbT5NnKduz.htm)|Wand of Crackling Lightning (3rd-Level Spell)|Baguette d'électricité crépitante (rang 3)|libre|
|[equipment-08-nv1OBtriMXhjQdmk.htm](equipment/equipment-08-nv1OBtriMXhjQdmk.htm)|In the Shadows of Toil|Dans les ombres du labeur|libre|
|[equipment-08-NVst7e69agGG9Qwd.htm](equipment/equipment-08-NVst7e69agGG9Qwd.htm)|Shock|Foudre (rune)|libre|
|[equipment-08-nZE16PtiyY7DWlzd.htm](equipment/equipment-08-nZE16PtiyY7DWlzd.htm)|Lover's Gloves|Gants de l'Amant|libre|
|[equipment-08-o7JdR8SFXIf5dLIW.htm](equipment/equipment-08-o7JdR8SFXIf5dLIW.htm)|Empathic Cords|Cordons empathiques|libre|
|[equipment-08-OoYfDIq8VoVX38Ds.htm](equipment/equipment-08-OoYfDIq8VoVX38Ds.htm)|Catching|Capture (rune)|libre|
|[equipment-08-oU3dxIAnTYlGXJVC.htm](equipment/equipment-08-oU3dxIAnTYlGXJVC.htm)|Folding Boat|Bâteau pliable|libre|
|[equipment-08-pDNlJMlF4OotsVBh.htm](equipment/equipment-08-pDNlJMlF4OotsVBh.htm)|Entertainer's Lute (Greater)|Luth du saltimbanque supérieur|libre|
|[equipment-08-Pq7JhHw3QhqqxY9d.htm](equipment/equipment-08-Pq7JhHw3QhqqxY9d.htm)|Trickster's Mandolin (Greater)|Mandoline du mystificateur supérieure|libre|
|[equipment-08-PSITHs8LgR7OZGcA.htm](equipment/equipment-08-PSITHs8LgR7OZGcA.htm)|Everywhen Map|Carte partoutletemps|libre|
|[equipment-08-QDYPr19De3TBIysx.htm](equipment/equipment-08-QDYPr19De3TBIysx.htm)|Sinister Knight|Chevalier sinistre (rune)|libre|
|[equipment-08-qJW6SU0hvLrxVbnV.htm](equipment/equipment-08-qJW6SU0hvLrxVbnV.htm)|Preserving (Greater)|Conservatrice supérieure (rune)|libre|
|[equipment-08-QnuL1UEot8ptWNb1.htm](equipment/equipment-08-QnuL1UEot8ptWNb1.htm)|Encompassing Lockpick|Nécessaire de crochetage complet|libre|
|[equipment-08-qqT5PVPnjBST4fZb.htm](equipment/equipment-08-qqT5PVPnjBST4fZb.htm)|Wand of Paralytic Shock (3rd-level)|Baguette de choc paralysant (rang 3)|libre|
|[equipment-08-Qquz4V1hKndtXSvw.htm](equipment/equipment-08-Qquz4V1hKndtXSvw.htm)|Wand of Rolling Flames (3rd-level)|Baguette des flammes ronflantes (rang 3)|libre|
|[equipment-08-rAkxflw4pNrPAq4p.htm](equipment/equipment-08-rAkxflw4pNrPAq4p.htm)|Aeon Stone (Envisioning)|Pierre d'éternité d'imagination|libre|
|[equipment-08-rJcynWlGp05cLsrG.htm](equipment/equipment-08-rJcynWlGp05cLsrG.htm)|Portable Gaming Hall|Salle de jeu portable|libre|
|[equipment-08-rns5Uvpgf8rSbOEX.htm](equipment/equipment-08-rns5Uvpgf8rSbOEX.htm)|Wand of Hybrid Form (3rd-level)|Baguette de forme hybride (rang 3)|libre|
|[equipment-08-RRZGHR81cUOvu6Tx.htm](equipment/equipment-08-RRZGHR81cUOvu6Tx.htm)|Rhinoceros Mask (Greater)|Masque de rhinocéros supérieur|libre|
|[equipment-08-Rsoh0Y3RQD8x8ito.htm](equipment/equipment-08-Rsoh0Y3RQD8x8ito.htm)|Collar of Inconspicuousness|Collier de modération|libre|
|[equipment-08-S4w9D1naKennNO5T.htm](equipment/equipment-08-S4w9D1naKennNO5T.htm)|Eye of the Unseen|Oeil de l'invisible|libre|
|[equipment-08-SUgFSQJWnfTcM1ZJ.htm](equipment/equipment-08-SUgFSQJWnfTcM1ZJ.htm)|Tablet of Chained Souls|Tablette des âmes enchainées|libre|
|[equipment-08-SVEPGvNFj95HjlAE.htm](equipment/equipment-08-SVEPGvNFj95HjlAE.htm)|Wand of Teeming Ghosts (3rd-Level Spell)|Baguette d'esprits grouillants (rang 3)|libre|
|[equipment-08-TA3QxxYcaGy2MYCk.htm](equipment/equipment-08-TA3QxxYcaGy2MYCk.htm)|Anglerfish Lantern (Submersible)|Lanterne baudroie submersible|libre|
|[equipment-08-TJaumkbZy11sIAgR.htm](equipment/equipment-08-TJaumkbZy11sIAgR.htm)|Wand of Widening (3rd-Rank Spell)|Baguette d'élargissement (sort de rang 3)|libre|
|[equipment-08-tWgN8hQbHlHNMTcF.htm](equipment/equipment-08-tWgN8hQbHlHNMTcF.htm)|Sun Dazzler|Éblouisseur solaire|libre|
|[equipment-08-UBfAwXBU5iQ59Bz7.htm](equipment/equipment-08-UBfAwXBU5iQ59Bz7.htm)|Cordelia's Construct Key|La clé d'animation de Cordélia|libre|
|[equipment-08-VB9hU5JVAsvNXc1w.htm](equipment/equipment-08-VB9hU5JVAsvNXc1w.htm)|Wand of Mercy (3rd-level)|Baguette de miséricorde (rang 3)|libre|
|[equipment-08-VDudQ4x2ozosAbTb.htm](equipment/equipment-08-VDudQ4x2ozosAbTb.htm)|Invisibility|Invisibilité (rune)|libre|
|[equipment-08-VToK4BNsTPOMAhHH.htm](equipment/equipment-08-VToK4BNsTPOMAhHH.htm)|Robe of Feathers Fall|Robe de chutes de plume|libre|
|[equipment-08-wdnQb4WoO0ghkCeP.htm](equipment/equipment-08-wdnQb4WoO0ghkCeP.htm)|Energy Robe of Cold|Robe d'énergie du froid|libre|
|[equipment-08-Wm0X7Pfd1bfocPSv.htm](equipment/equipment-08-Wm0X7Pfd1bfocPSv.htm)|Corrosive|Corrosive (rune)|officielle|
|[equipment-08-wnWo9t2mNV4YaH37.htm](equipment/equipment-08-wnWo9t2mNV4YaH37.htm)|Spiny Lodestone (Greater)|Pierre aimantée épineuse supérieure|libre|
|[equipment-08-X1uTEyn6CRXCzgqr.htm](equipment/equipment-08-X1uTEyn6CRXCzgqr.htm)|Perfect Droplet (Greater)|Goutte parfaite supérieure|libre|
|[equipment-08-X42q1s1ZnxliirGW.htm](equipment/equipment-08-X42q1s1ZnxliirGW.htm)|Pipes of Compulsion (Greater)|Tuyaux de servitude supérieurs|libre|
|[equipment-08-X5OGyKvpF4wcIj7m.htm](equipment/equipment-08-X5OGyKvpF4wcIj7m.htm)|Mask of the Cursed Eye|Masque de l'oeil maudit|libre|
|[equipment-08-X5QdnhQyF5Lk1VWV.htm](equipment/equipment-08-X5QdnhQyF5Lk1VWV.htm)|Armory Bracelet (Moderate)|Bracelet arsenal modéré|libre|
|[equipment-08-XgO0MTOXdpbfdzr6.htm](equipment/equipment-08-XgO0MTOXdpbfdzr6.htm)|Right of Retribution|Droit de vengeance (Pacte infernal)|libre|
|[equipment-08-Xnhp7g1UOPVPZx7u.htm](equipment/equipment-08-Xnhp7g1UOPVPZx7u.htm)|Brooch of Inspiration|Broche d'inspiration|libre|
|[equipment-08-XPqKEI246hsr9R6P.htm](equipment/equipment-08-XPqKEI246hsr9R6P.htm)|Wand of Legerdemain (3rd-level)|Baguette de prestidigitation (rang 3)|libre|
|[equipment-08-XszNvxnymWYRaoTp.htm](equipment/equipment-08-XszNvxnymWYRaoTp.htm)|Flaming|Enflammée (rune)|libre|
|[equipment-08-XZleXDXQ5eThTLWd.htm](equipment/equipment-08-XZleXDXQ5eThTLWd.htm)|Campaign Stable|Étable de campagne|libre|
|[equipment-08-Y8kiHC9keql957uo.htm](equipment/equipment-08-Y8kiHC9keql957uo.htm)|Bottomless Purse|Bourse sans fond|libre|
|[equipment-08-y8T7k0kkqxNbMAAD.htm](equipment/equipment-08-y8T7k0kkqxNbMAAD.htm)|Skittering Mask (Greater)|Masque fuyant supérieur|libre|
|[equipment-08-yQTmIcXIDg7nVpJX.htm](equipment/equipment-08-yQTmIcXIDg7nVpJX.htm)|Grim Sandglass (Greater)|Sablier sinistre supérieur|libre|
|[equipment-08-ywsDnHaJSD9oHvh7.htm](equipment/equipment-08-ywsDnHaJSD9oHvh7.htm)|Faith Tattoo (Greater)|Tatouage du croyant supérieur|libre|
|[equipment-08-Z5XaC2iyi5GLKYrI.htm](equipment/equipment-08-Z5XaC2iyi5GLKYrI.htm)|Warding Tablets|Tablettes de protection|libre|
|[equipment-08-ZeXTbx8MhQQ6ylhU.htm](equipment/equipment-08-ZeXTbx8MhQQ6ylhU.htm)|Sunflower Censer|Encensoir tournesol|libre|
|[equipment-09-0GOHTXBxs6H6ARBz.htm](equipment/equipment-09-0GOHTXBxs6H6ARBz.htm)|Healer's Gloves (Greater)|Gants du guérisseur supérieur|libre|
|[equipment-09-0PPQl3TEr1yNhhN6.htm](equipment/equipment-09-0PPQl3TEr1yNhhN6.htm)|Persona Mask (Greater)|Masque de représentation supérieur|libre|
|[equipment-09-14LrP7bx8Q1jimHO.htm](equipment/equipment-09-14LrP7bx8Q1jimHO.htm)|Immovable Rod|Barre inamovible|libre|
|[equipment-09-1bvH8zFQvDYky9tr.htm](equipment/equipment-09-1bvH8zFQvDYky9tr.htm)|Pendant of the Occult (Greater)|Pendentif des sciences occultes supérieur|libre|
|[equipment-09-2QZbG6FUr2AWK6km.htm](equipment/equipment-09-2QZbG6FUr2AWK6km.htm)|Immovable Arm|Bras inamovible|libre|
|[equipment-09-3G2No6QaU1wSPTh6.htm](equipment/equipment-09-3G2No6QaU1wSPTh6.htm)|Urn of Ashes|Urne de cendres|libre|
|[equipment-09-3Lexs7KnhbV0HgFh.htm](equipment/equipment-09-3Lexs7KnhbV0HgFh.htm)|Wand of Shardstorm (3rd-Rank Spell)|Baguette de tempêtes d'éclats (rang 3)|libre|
|[equipment-09-3Ucc6E5V6bzdgFLE.htm](equipment/equipment-09-3Ucc6E5V6bzdgFLE.htm)|Bewitching Bloom (Lotus)|Fleur envoûtante (Lotus)|libre|
|[equipment-09-3x7YAs6nos79Dpt7.htm](equipment/equipment-09-3x7YAs6nos79Dpt7.htm)|Enveloping Light (Greater)|Lumière enveloppante supérieur|libre|
|[equipment-09-45zjE7pj6FUHuz3K.htm](equipment/equipment-09-45zjE7pj6FUHuz3K.htm)|Advancing|Progression (rune)|libre|
|[equipment-09-4CnzS57PB3Em02YN.htm](equipment/equipment-09-4CnzS57PB3Em02YN.htm)|Fulu Compendium|Recueil de fulus|libre|
|[equipment-09-4RBeMHw6u9pxNDpo.htm](equipment/equipment-09-4RBeMHw6u9pxNDpo.htm)|Vigilant Eye|Oeil vigilant|libre|
|[equipment-09-5EDJgEYfktSEa1pr.htm](equipment/equipment-09-5EDJgEYfktSEa1pr.htm)|Self-Emptying Pocket|Poche vidée|libre|
|[equipment-09-5g0rE5yMsTH3E2LJ.htm](equipment/equipment-09-5g0rE5yMsTH3E2LJ.htm)|Manacles (Good)|Menottes bonnes|officielle|
|[equipment-09-5XfTQjS0xm0TllOE.htm](equipment/equipment-09-5XfTQjS0xm0TllOE.htm)|Wyrm on the Wing|Ver sur l'aile|libre|
|[equipment-09-6MIBHKMJmD8uQVam.htm](equipment/equipment-09-6MIBHKMJmD8uQVam.htm)|Chain of the Stilled Spirit|Chaîne de l'esprit apaisé|libre|
|[equipment-09-6QS9p7IWfRoplIwk.htm](equipment/equipment-09-6QS9p7IWfRoplIwk.htm)|Cape of Grand Entrances|Cape des entrées fracassantes|libre|
|[equipment-09-6xDxnI4Z9syXq8Ec.htm](equipment/equipment-09-6xDxnI4Z9syXq8Ec.htm)|Bravery Baldric (Haste)|Baudrier de bravoure (Rapidité)|libre|
|[equipment-09-8jNsUCsNe6PyenNZ.htm](equipment/equipment-09-8jNsUCsNe6PyenNZ.htm)|Retrieval Belt (Greater)|Ceinture de récupération supérieure|libre|
|[equipment-09-9BvimbwCCrQw3kyD.htm](equipment/equipment-09-9BvimbwCCrQw3kyD.htm)|Affinity Stones|Pierres d'affinité|libre|
|[equipment-09-aaua3vcUXyBFpRe9.htm](equipment/equipment-09-aaua3vcUXyBFpRe9.htm)|Soaring Wings|Ailes volantes|libre|
|[equipment-09-aR0tsZ3xi1iwr53u.htm](equipment/equipment-09-aR0tsZ3xi1iwr53u.htm)|Shoony Shovel|Pelle shoony|libre|
|[equipment-09-B5ZfLr6Ox816FBEc.htm](equipment/equipment-09-B5ZfLr6Ox816FBEc.htm)|Crown of the Kobold King|Couronne du roi kobold|libre|
|[equipment-09-bdbBRucjvg0eYazY.htm](equipment/equipment-09-bdbBRucjvg0eYazY.htm)|Helm of Underwater Action|Casque d'action sous-marine|libre|
|[equipment-09-bJORQsO9E1JCJh6i.htm](equipment/equipment-09-bJORQsO9E1JCJh6i.htm)|Extending|Extensible (rune)|libre|
|[equipment-09-BR2vQLRif2Pgq5eN.htm](equipment/equipment-09-BR2vQLRif2Pgq5eN.htm)|Magnifying Scope (Greater)|Lunette de visée grossissante supérieure|libre|
|[equipment-09-bSm0Hki8N2L50OZw.htm](equipment/equipment-09-bSm0Hki8N2L50OZw.htm)|Shadow (Greater)|Ombre supérieure (rune)|libre|
|[equipment-09-BWxouItL1sfGTIUh.htm](equipment/equipment-09-BWxouItL1sfGTIUh.htm)|Pontoon|Ponton (rune)|libre|
|[equipment-09-byKAxLyN7AXySmw7.htm](equipment/equipment-09-byKAxLyN7AXySmw7.htm)|Lock (Good)|Serrure (Bonne)|officielle|
|[equipment-09-cO7ANYLkcmfCn9c9.htm](equipment/equipment-09-cO7ANYLkcmfCn9c9.htm)|Collar of Empathy|Collier d'empathie|libre|
|[equipment-09-dJoYIbM6GF6wgX0b.htm](equipment/equipment-09-dJoYIbM6GF6wgX0b.htm)|Gourd Home|Calebasse de camping|officielle|
|[equipment-09-dLTN4FU3qBoDZ5CJ.htm](equipment/equipment-09-dLTN4FU3qBoDZ5CJ.htm)|Eyes of the Cat|Yeux de chat|libre|
|[equipment-09-dolBtAdB5lpQpQpp.htm](equipment/equipment-09-dolBtAdB5lpQpQpp.htm)|Ventriloquist's Ring (Greater)|Anneau de ventriloquie supérieur|libre|
|[equipment-09-dRVDcmkaB2p8zPcs.htm](equipment/equipment-09-dRVDcmkaB2p8zPcs.htm)|Stole of Civility|Étole de civilité|libre|
|[equipment-09-e4lHxftAOoTmGXuG.htm](equipment/equipment-09-e4lHxftAOoTmGXuG.htm)|Unmemorable Mantle (Greater)|Manteau insignifiant supérieur|libre|
|[equipment-09-E5gjsRSO6SFFlKpB.htm](equipment/equipment-09-E5gjsRSO6SFFlKpB.htm)|Underwater firing Mechanism|Mécanisme sous-marin de mise à feu|libre|
|[equipment-09-eHfL8Apfx4fxGksT.htm](equipment/equipment-09-eHfL8Apfx4fxGksT.htm)|Malleable|Malléable (rune)|libre|
|[equipment-09-FB8EAFOz2hS6Jbic.htm](equipment/equipment-09-FB8EAFOz2hS6Jbic.htm)|Eternal Eruption of Pale Mountain|Éternelle éruption de la Montagne pâle|libre|
|[equipment-09-ferPzbD9Qsta71wK.htm](equipment/equipment-09-ferPzbD9Qsta71wK.htm)|Energy Robe of Acid|Robe d'énergie acide|libre|
|[equipment-09-FPDe2DQv8HbkN4tz.htm](equipment/equipment-09-FPDe2DQv8HbkN4tz.htm)|Tlil Mask (Greater)|Masque Tlil supérieur|libre|
|[equipment-09-FXdESP5mmP01tL2v.htm](equipment/equipment-09-FXdESP5mmP01tL2v.htm)|Humbug Pocket|Poche de falsification|libre|
|[equipment-09-G4luLo1IEtDben39.htm](equipment/equipment-09-G4luLo1IEtDben39.htm)|Thousand-Blade Thesis|Thèse des mille-lames|libre|
|[equipment-09-GwkgqHAYRrDXSW2E.htm](equipment/equipment-09-GwkgqHAYRrDXSW2E.htm)|Nemesis Name|Nom némésis|libre|
|[equipment-09-h96LHhdZhVYnjcYA.htm](equipment/equipment-09-h96LHhdZhVYnjcYA.htm)|Key to the Stomach|Clé vers l'estomac|libre|
|[equipment-09-HAtj6AGCIZHpD7Nl.htm](equipment/equipment-09-HAtj6AGCIZHpD7Nl.htm)|Messenger's Ring|Anneau du messager|libre|
|[equipment-09-idw9HdfR4QvseXsc.htm](equipment/equipment-09-idw9HdfR4QvseXsc.htm)|Cursed Dreamstone|Pierre onirique maudite|officielle|
|[equipment-09-ImedbomHGVFtE3D7.htm](equipment/equipment-09-ImedbomHGVFtE3D7.htm)|Tradecraft Tattoo|Tatouage professionnel|libre|
|[equipment-09-ioLWDzXp2dG7ZMHf.htm](equipment/equipment-09-ioLWDzXp2dG7ZMHf.htm)|Stanching (Greater)|Étanchement supérieur (rune)|libre|
|[equipment-09-IufO3dNPhC1c2ZcL.htm](equipment/equipment-09-IufO3dNPhC1c2ZcL.htm)|Necklace of Fireballs III|Collier à boules de feu III|officielle|
|[equipment-09-j392AMbPfkcRbCw6.htm](equipment/equipment-09-j392AMbPfkcRbCw6.htm)|Sanguine Fang|Canine sanguine|libre|
|[equipment-09-JC7do7p0jkkI0crM.htm](equipment/equipment-09-JC7do7p0jkkI0crM.htm)|Furnace of Endings (Greater)|Fournaise des terminaisons supérieure|libre|
|[equipment-09-JhDhFq2JGBGR2lwb.htm](equipment/equipment-09-JhDhFq2JGBGR2lwb.htm)|Elemental Wayfinder (Fire)|Guide élémentaire (feu)|libre|
|[equipment-09-JHq8r0fGo1rWF8NS.htm](equipment/equipment-09-JHq8r0fGo1rWF8NS.htm)|Linguist's Dictionary|Dictionnaire du linguiste|libre|
|[equipment-09-jsTc3wyD0gSdXILQ.htm](equipment/equipment-09-jsTc3wyD0gSdXILQ.htm)|Jann's Prism|Prisme du jann|libre|
|[equipment-09-kDbL1zbqf76odwlU.htm](equipment/equipment-09-kDbL1zbqf76odwlU.htm)|Juubun's One Thousand Poems|Les milles poèmes de Juubun|libre|
|[equipment-09-KLWjINVkWwJlOZEX.htm](equipment/equipment-09-KLWjINVkWwJlOZEX.htm)|Triton's Conch|Conque de triton|officielle|
|[equipment-09-kNJADasb0PatkMlc.htm](equipment/equipment-09-kNJADasb0PatkMlc.htm)|Aeon Stone (Cymophane Cabochon)|Pierre d'éternité (cabochon chrysobéryl)|libre|
|[equipment-09-kXK9szN2gcYoPSdN.htm](equipment/equipment-09-kXK9szN2gcYoPSdN.htm)|Printing Press|Presse d'imprimerie|libre|
|[equipment-09-kZKK4Va3e7n3p3tv.htm](equipment/equipment-09-kZKK4Va3e7n3p3tv.htm)|Earthsight Box|Boîte de vue terrestre|libre|
|[equipment-09-lkIzsqI7PcymcTEQ.htm](equipment/equipment-09-lkIzsqI7PcymcTEQ.htm)|Hunter's Hagbook|Le livre du chasseur de guenaude|libre|
|[equipment-09-lRdrk7Dh9eVlXFHi.htm](equipment/equipment-09-lRdrk7Dh9eVlXFHi.htm)|Dreamstone|Pierre onirique|officielle|
|[equipment-09-LuW0KcoGUJ49dFCJ.htm](equipment/equipment-09-LuW0KcoGUJ49dFCJ.htm)|Kinburi's Sandals of Bounding|Sandales de saut de Kinburi|libre|
|[equipment-09-LV8nFuV3NiNYlxpo.htm](equipment/equipment-09-LV8nFuV3NiNYlxpo.htm)|Thunderblast Slippers|Souliers d'explosion tonitruante|libre|
|[equipment-09-M1fRjN7E3P55zvmc.htm](equipment/equipment-09-M1fRjN7E3P55zvmc.htm)|Inspiring Spotlight|Projecteur inspirant|libre|
|[equipment-09-M7c4uXraP05HYNXs.htm](equipment/equipment-09-M7c4uXraP05HYNXs.htm)|Shining Symbol (Greater)|Symbole lumineux supérieur|libre|
|[equipment-09-MBF9bZCwkJp2RZoN.htm](equipment/equipment-09-MBF9bZCwkJp2RZoN.htm)|Bountiful Cauldron|Chaudron d'abondance|libre|
|[equipment-09-MjW8mZrCUw1lEhGP.htm](equipment/equipment-09-MjW8mZrCUw1lEhGP.htm)|Shard of the Third Seal|Fragment du troisième Sceau|libre|
|[equipment-09-MlrVt5CaZ85PvEm9.htm](equipment/equipment-09-MlrVt5CaZ85PvEm9.htm)|Diver's Gloves (Moderate)|Gants du plongeur modérés|libre|
|[equipment-09-MtSUaIZ1gThCrNNj.htm](equipment/equipment-09-MtSUaIZ1gThCrNNj.htm)|Amphibious Chair|Fauteuil amphibie|libre|
|[equipment-09-MV9tQYR5vsjPeyar.htm](equipment/equipment-09-MV9tQYR5vsjPeyar.htm)|Injigo's Loving Embrace|L'Étreinte aimante d'Injigo|libre|
|[equipment-09-nGZPhGpCxM8U1JXv.htm](equipment/equipment-09-nGZPhGpCxM8U1JXv.htm)|Bitter|Amertume|libre|
|[equipment-09-NjyKPRuq9siQe7gu.htm](equipment/equipment-09-NjyKPRuq9siQe7gu.htm)|Wraithweave Patch (Type I)|Étoffe tressée d'âme-en-peine (Type I)|libre|
|[equipment-09-NwyyVc5G6zUbcXRs.htm](equipment/equipment-09-NwyyVc5G6zUbcXRs.htm)|Tome of Restorative Cleansing (Moderate)|Tome du nettoyage restaurateur modéré|libre|
|[equipment-09-nza9skYTNtJe2Wd3.htm](equipment/equipment-09-nza9skYTNtJe2Wd3.htm)|Lucky Keepsake|Grigri porte-bonheur|libre|
|[equipment-09-O6deRhRldwTqMP97.htm](equipment/equipment-09-O6deRhRldwTqMP97.htm)|Diviner's Nose Chain|Chaîne de nez du devin|libre|
|[equipment-09-ogTmuTUt52b7x5Yu.htm](equipment/equipment-09-ogTmuTUt52b7x5Yu.htm)|Wand of Thundering Echoes (3rd-Level Spell)|Baguette des écho tonitruants (rang 3)|libre|
|[equipment-09-ojbsJu6Uq269xx3U.htm](equipment/equipment-09-ojbsJu6Uq269xx3U.htm)|Silent Bell|Cloche silencieuse|libre|
|[equipment-09-OYeYYJ4i66VtGY3O.htm](equipment/equipment-09-OYeYYJ4i66VtGY3O.htm)|Tracker's Goggles (Greater)|Lunettes de pisteur supérieures|libre|
|[equipment-09-p8kyiYyXDapAUyfE.htm](equipment/equipment-09-p8kyiYyXDapAUyfE.htm)|Portable Ram (Reinforced)|Bélier portable renforcé|libre|
|[equipment-09-PDEhZSibXvQdVsSq.htm](equipment/equipment-09-PDEhZSibXvQdVsSq.htm)|Oath of the Devoted|Serment du dévoué|libre|
|[equipment-09-PMmJ47MLUXAWmXFg.htm](equipment/equipment-09-PMmJ47MLUXAWmXFg.htm)|Elemental Wayfinder (Earth)|Guide élémentaire (terre)|libre|
|[equipment-09-pnUZKqJilb3aEVz5.htm](equipment/equipment-09-pnUZKqJilb3aEVz5.htm)|Anchor of Aquatic Exploration|Ancre d'exploration aquatique|libre|
|[equipment-09-pPl2b7fqfB6dyQwf.htm](equipment/equipment-09-pPl2b7fqfB6dyQwf.htm)|Phylactery of Faithfulness|Phylactère du croyant|officielle|
|[equipment-09-QJ1PhbtbLzwhRlY0.htm](equipment/equipment-09-QJ1PhbtbLzwhRlY0.htm)|Dancing Scarf (Greater)|Écharpe dansante supérieur|libre|
|[equipment-09-qQpcBE7ngv7TIAW3.htm](equipment/equipment-09-qQpcBE7ngv7TIAW3.htm)|Wand of Overflowing Life (3rd-Level Spell)|Baguette de vie débordante (rang 3)|libre|
|[equipment-09-QRtsLdNvTs2C1oI2.htm](equipment/equipment-09-QRtsLdNvTs2C1oI2.htm)|Starless Scope Lens|Lentille de la lunette sans étoile|libre|
|[equipment-09-qUnDHEXteUQGE8yp.htm](equipment/equipment-09-qUnDHEXteUQGE8yp.htm)|Grievous|Douloureuse (rune)|libre|
|[equipment-09-QVmITUIiWDWMEPgg.htm](equipment/equipment-09-QVmITUIiWDWMEPgg.htm)|Shell of Easy Breathing (Greater)|Coquillage de respiration facilitée supérieur|libre|
|[equipment-09-R88HWv9rw1VNMRer.htm](equipment/equipment-09-R88HWv9rw1VNMRer.htm)|Wand of Continuation (3rd-Rank Spell)|Baguette de prolongation (sort de rang 3)|libre|
|[equipment-09-rRAx62TLPTZSiaLD.htm](equipment/equipment-09-rRAx62TLPTZSiaLD.htm)|Arboreal Boots (Greater)|Bottes arboréennes supérieures|libre|
|[equipment-09-sls3pfkhgCbW723f.htm](equipment/equipment-09-sls3pfkhgCbW723f.htm)|Coyote Cloak (Greater)|Cape de coyotte supérieur|officielle|
|[equipment-09-SMBPvsZH7yAsGSQa.htm](equipment/equipment-09-SMBPvsZH7yAsGSQa.htm)|Elemental Wayfinder (Air)|Guide élémentaire (air)|libre|
|[equipment-09-t6SSQYruLsDWj5Tl.htm](equipment/equipment-09-t6SSQYruLsDWj5Tl.htm)|Crushing (Greater)|Écrasante supérieure (rune)|libre|
|[equipment-09-t8RXms7xq8atF9W7.htm](equipment/equipment-09-t8RXms7xq8atF9W7.htm)|Charlatan's Gloves (Greater)|Gants du prestidigitateur supérieurs|libre|
|[equipment-09-t8U01IpeOyKv4nDN.htm](equipment/equipment-09-t8U01IpeOyKv4nDN.htm)|Librarian's Baton|Bâton du bibliothécaire|libre|
|[equipment-09-tO5zXmyoBsa0lIDd.htm](equipment/equipment-09-tO5zXmyoBsa0lIDd.htm)|Bound Guardian|Gardien lié|libre|
|[equipment-09-tr2eOlJMmBKBZtI9.htm](equipment/equipment-09-tr2eOlJMmBKBZtI9.htm)|Mask of the Banshee|Masque de la Banshie|libre|
|[equipment-09-TuPCnRFdvOboUzx7.htm](equipment/equipment-09-TuPCnRFdvOboUzx7.htm)|Thrasher Tail|Queue de la buse|libre|
|[equipment-09-TYdFnzhhTlpUdtty.htm](equipment/equipment-09-TYdFnzhhTlpUdtty.htm)|Starless Scope|Lunette sans étoiles|libre|
|[equipment-09-u4mjXLioKytEZMTb.htm](equipment/equipment-09-u4mjXLioKytEZMTb.htm)|Minotaur Chair|Fauteuil du minotaure|libre|
|[equipment-09-uSwylNGP5We9uUw4.htm](equipment/equipment-09-uSwylNGP5We9uUw4.htm)|Smogger|Smogueur|libre|
|[equipment-09-uzNB9EVGujen0cnC.htm](equipment/equipment-09-uzNB9EVGujen0cnC.htm)|Wrestler's Armbands|Brassards du lutteur|libre|
|[equipment-09-vsTtpwdYANxP4EPZ.htm](equipment/equipment-09-vsTtpwdYANxP4EPZ.htm)|Cape of Illumination (Moderate)|Cape d'illumination modérée|libre|
|[equipment-09-VYXAdLJdF0XSeX5m.htm](equipment/equipment-09-VYXAdLJdF0XSeX5m.htm)|Portable|Portative (rune)|libre|
|[equipment-09-WAMDYE9tt3W8obzr.htm](equipment/equipment-09-WAMDYE9tt3W8obzr.htm)|Horn of Blasting|Cor de dévastation|officielle|
|[equipment-09-We11d1zelZ1wskvN.htm](equipment/equipment-09-We11d1zelZ1wskvN.htm)|Paired (Greater)|Appariée supérieure (rune)|libre|
|[equipment-09-Wf5KDIF4gauHQmy2.htm](equipment/equipment-09-Wf5KDIF4gauHQmy2.htm)|Cresset of Grisly Interrogation|Creuset d'interrogatoire sinistre|libre|
|[equipment-09-wI4Tj8bNwpZHequC.htm](equipment/equipment-09-wI4Tj8bNwpZHequC.htm)|Armbands of Athleticism|Brassards d'athlète|officielle|
|[equipment-09-WR8GOQSx7B5AlzNu.htm](equipment/equipment-09-WR8GOQSx7B5AlzNu.htm)|Handcuffs (Good)|Menottes à cliquet (bonnes)|libre|
|[equipment-09-xe6I5BIYHE7SXSax.htm](equipment/equipment-09-xe6I5BIYHE7SXSax.htm)|Magnetite Scope|Lunette de visée en magnétite|libre|
|[equipment-09-xldy59c3WDGYlAYs.htm](equipment/equipment-09-xldy59c3WDGYlAYs.htm)|Banner of the Restful|Bannière du reposé|libre|
|[equipment-09-xnnVMQZl4i2NuYVe.htm](equipment/equipment-09-xnnVMQZl4i2NuYVe.htm)|Ring of Bestial Friendship|Anneau d'amitié bestiale|libre|
|[equipment-09-XQFEjwQUCXl68Pva.htm](equipment/equipment-09-XQFEjwQUCXl68Pva.htm)|Taleteller's Ring|Anneau du conteur|libre|
|[equipment-09-xtSMGL3f9c5ZHUAm.htm](equipment/equipment-09-xtSMGL3f9c5ZHUAm.htm)|Violin of the Waves|Violon des vagues|libre|
|[equipment-09-XWWosdDg34KZhrEi.htm](equipment/equipment-09-XWWosdDg34KZhrEi.htm)|Dreadsmoke Thurible|Encensoir de fumée terrifiante|libre|
|[equipment-09-xzN8mFG2Z70SaXLa.htm](equipment/equipment-09-xzN8mFG2Z70SaXLa.htm)|Cloak of Repute (Greater)|Cape de renom supérieur|libre|
|[equipment-09-y9RUbQec9zGeqqcE.htm](equipment/equipment-09-y9RUbQec9zGeqqcE.htm)|Coating|Recouvrante (rune)|libre|
|[equipment-09-yAqGhT0GuZrkvWZl.htm](equipment/equipment-09-yAqGhT0GuZrkvWZl.htm)|Crimson Fulcrum Lens|Lentille cramoisie|libre|
|[equipment-09-YC6kyZGOKZmONbVM.htm](equipment/equipment-09-YC6kyZGOKZmONbVM.htm)|Anointed Waterskin|Gourde sanctifiée|libre|
|[equipment-09-YCjVrQnHfOtpmjYW.htm](equipment/equipment-09-YCjVrQnHfOtpmjYW.htm)|Belt of the Five Kings|Ceinture des cinq rois|libre|
|[equipment-09-yMEDmJWDPv2i78WO.htm](equipment/equipment-09-yMEDmJWDPv2i78WO.htm)|Ashen|Cendreuse (rune)|libre|
|[equipment-09-ySZgsSBBVf3uv8Nx.htm](equipment/equipment-09-ySZgsSBBVf3uv8Nx.htm)|Eye Slash (Major)|Entailles à l'oeil majeures|libre|
|[equipment-09-YXTrUT7Z7emAn2gb.htm](equipment/equipment-09-YXTrUT7Z7emAn2gb.htm)|Wondrous Figurine (Ruby Hippopotamus)|Figurine merveilleuse (Hippopotame de rubis)|libre|
|[equipment-09-z8nKK4rSUGQVT2t9.htm](equipment/equipment-09-z8nKK4rSUGQVT2t9.htm)|Swarming|De nuée (rune)|libre|
|[equipment-09-Z8RZfkIrsifYeWg4.htm](equipment/equipment-09-Z8RZfkIrsifYeWg4.htm)|Camouflage Suit (Superb)|Tenue de camouflage superbe|libre|
|[equipment-09-ZxOoQ4cHfJXXbP9F.htm](equipment/equipment-09-ZxOoQ4cHfJXXbP9F.htm)|Beastmaster's Sigil (Major)|Symbole du maître des bêtes majeur|libre|
|[equipment-10-0Oqf8wHY8PYJQQTx.htm](equipment/equipment-10-0Oqf8wHY8PYJQQTx.htm)|Trackless (Greater)|Sans trace supérieure (rune)|libre|
|[equipment-10-1T1TJB929nC7wBtC.htm](equipment/equipment-10-1T1TJB929nC7wBtC.htm)|Shadow Signet|Chevalière de l'ombre|libre|
|[equipment-10-24o5lk8aOcSrsas0.htm](equipment/equipment-10-24o5lk8aOcSrsas0.htm)|Rime Jar|Jarre de chaux|libre|
|[equipment-10-2idzJUvuy8Zu6y78.htm](equipment/equipment-10-2idzJUvuy8Zu6y78.htm)|Galvanic Mortal Coil|Bobine galvanique mortelle|libre|
|[equipment-10-2tSDgHfSkkaX4CA4.htm](equipment/equipment-10-2tSDgHfSkkaX4CA4.htm)|Ring of Wizardry (Type II)|Anneau des arcanes (Type II)|libre|
|[equipment-10-3GnTrwAfD00zW2cO.htm](equipment/equipment-10-3GnTrwAfD00zW2cO.htm)|Stone Circle|Cercle de pierre|libre|
|[equipment-10-3M3ZDW3MdfGLTZMC.htm](equipment/equipment-10-3M3ZDW3MdfGLTZMC.htm)|Warcaller's Chime of Dread|Carillon d'effroi du belliqueux|libre|
|[equipment-10-3mprh9aZ670HfNhT.htm](equipment/equipment-10-3mprh9aZ670HfNhT.htm)|Maestro's Instrument (Moderate)|Instrument de maestro modéré|libre|
|[equipment-10-3nv4JcajPAxDgGMb.htm](equipment/equipment-10-3nv4JcajPAxDgGMb.htm)|Grail of Twisted Desires|Calice des désirs interdits|libre|
|[equipment-10-4hsPZ6rBpLKOlDjm.htm](equipment/equipment-10-4hsPZ6rBpLKOlDjm.htm)|Wand of Legerdemain (4th-level)|Baguette de prestidigitation (rang 4)|libre|
|[equipment-10-4v8FIJSPaJUbccLz.htm](equipment/equipment-10-4v8FIJSPaJUbccLz.htm)|Crown of Witchcraft|Couronne de sorcellerie|libre|
|[equipment-10-5SKGqxahAGsJgzRF.htm](equipment/equipment-10-5SKGqxahAGsJgzRF.htm)|Aeon Stone (Pearlescent Pyramid)|Pierre d'éternité (pyramide nacrée)|libre|
|[equipment-10-64mxKuxc9k98FkUi.htm](equipment/equipment-10-64mxKuxc9k98FkUi.htm)|Fire-Jump Ring|Anneau Saute-feu|libre|
|[equipment-10-6jRXUXzYKIpm2uNp.htm](equipment/equipment-10-6jRXUXzYKIpm2uNp.htm)|Clandestine Cloak (Greater)|Cape de clandestinité supérieure|libre|
|[equipment-10-6MswPHUwvqPLtN5H.htm](equipment/equipment-10-6MswPHUwvqPLtN5H.htm)|Specialist's Ring (Evocation)|Anneau du spécialiste (Évocation)|libre|
|[equipment-10-76FyZmtpHi6OXQ0B.htm](equipment/equipment-10-76FyZmtpHi6OXQ0B.htm)|Wildwood Ink (Greater)|Encre du bois sauvage supérieure|libre|
|[equipment-10-7dkdtY9cTXM3gMon.htm](equipment/equipment-10-7dkdtY9cTXM3gMon.htm)|Phantasmal Doorknob (Greater)|Poignée de porte imaginaire supérieure|libre|
|[equipment-10-7q3B8wvZFltHTpPo.htm](equipment/equipment-10-7q3B8wvZFltHTpPo.htm)|Ring of the Tiger (Greater)|Anneau du Tigre supérieur|libre|
|[equipment-10-80HjjtcTF6DS1w30.htm](equipment/equipment-10-80HjjtcTF6DS1w30.htm)|Wand of Dumbfounding Doom (4th-level)|Baguette de la fatalité abrutissante (rang 4)|libre|
|[equipment-10-8E7qXnNOSm51UwsG.htm](equipment/equipment-10-8E7qXnNOSm51UwsG.htm)|Miter of Communion|Mitre de communion|libre|
|[equipment-10-8usnglqucr4Q0YO6.htm](equipment/equipment-10-8usnglqucr4Q0YO6.htm)|Midday Lantern (Moderate)|Lanterne de midi modérée|libre|
|[equipment-10-9ftc8XfloerPcJnI.htm](equipment/equipment-10-9ftc8XfloerPcJnI.htm)|Wand of Hopeless Night (4th-Level Spell)|Baguette de noir désespoir (rang 4)|libre|
|[equipment-10-9Y86NM6nt2WtYBOy.htm](equipment/equipment-10-9Y86NM6nt2WtYBOy.htm)|Explorer's Yurt|Yourte d'explorateur|libre|
|[equipment-10-a4RIB8GPOliFoN31.htm](equipment/equipment-10-a4RIB8GPOliFoN31.htm)|Instinct Crown (Dragon)|Couronne d'instinct (dragon)|libre|
|[equipment-10-AK4rHlYOHkcboCnq.htm](equipment/equipment-10-AK4rHlYOHkcboCnq.htm)|Voice From The Grave|Voix de la tombe|libre|
|[equipment-10-aKoMqPDmVzPI7Q20.htm](equipment/equipment-10-aKoMqPDmVzPI7Q20.htm)|Winged Sandals|Sandales ailées|libre|
|[equipment-10-AYVswE7nZuPOUjlU.htm](equipment/equipment-10-AYVswE7nZuPOUjlU.htm)|Aboutface Figurehead|Figure de proue de volte-face|libre|
|[equipment-10-BKFuMxi0dXu7yFxe.htm](equipment/equipment-10-BKFuMxi0dXu7yFxe.htm)|Ring of Truth|Anneau de vérité|officielle|
|[equipment-10-BNShQdbxiPMisV60.htm](equipment/equipment-10-BNShQdbxiPMisV60.htm)|Poisonous Cloak Type II|Cape empoisonnée (Type II)|officielle|
|[equipment-10-br4eClrcGeLTL6Ba.htm](equipment/equipment-10-br4eClrcGeLTL6Ba.htm)|Choker of Elocution (Greater)|Collier d'élocution supérieur|officielle|
|[equipment-10-bU731I8njNmSAQhQ.htm](equipment/equipment-10-bU731I8njNmSAQhQ.htm)|Aluum Charm|Charme d'Aluum|libre|
|[equipment-10-bvROhtzAez0KP7Vt.htm](equipment/equipment-10-bvROhtzAez0KP7Vt.htm)|Reinforcing Rune (Moderate)|Renforcement modéré (rune)|libre|
|[equipment-10-bxz885LMjLCkpDq3.htm](equipment/equipment-10-bxz885LMjLCkpDq3.htm)|Invisibility (Greater)|Invisibilité supérieure (rune)|libre|
|[equipment-10-CA4W0Dt4qeg8MLHA.htm](equipment/equipment-10-CA4W0Dt4qeg8MLHA.htm)|Energy Robe of Electricity|Robe d'énergie d'électricité|libre|
|[equipment-10-cBKrXbxM02MlycpV.htm](equipment/equipment-10-cBKrXbxM02MlycpV.htm)|Ring of Counterspells|Anneau de contresort|libre|
|[equipment-10-cH5uD83e3hC9MEvZ.htm](equipment/equipment-10-cH5uD83e3hC9MEvZ.htm)|Wand of Dazzling Rays (4th-level)|Baguette des rayons éblouissants (rang 4)|libre|
|[equipment-10-CmGfVmuUH1VGISPr.htm](equipment/equipment-10-CmGfVmuUH1VGISPr.htm)|Ghastly Cauldron|Chaudron épouvantable|libre|
|[equipment-10-D8VKhm7rSBozJkMN.htm](equipment/equipment-10-D8VKhm7rSBozJkMN.htm)|Accolade Robe|Robe de distinction|libre|
|[equipment-10-d8wTwHiA09HP45IM.htm](equipment/equipment-10-d8wTwHiA09HP45IM.htm)|Golem Stylus|Stylet du golem|libre|
|[equipment-10-Deyo6pk3OXthFx9Q.htm](equipment/equipment-10-Deyo6pk3OXthFx9Q.htm)|Wand of Rolling Flames (4th-level)|Baguette des flammes ronflantes (rang 4)|libre|
|[equipment-10-dipcMOtBFxtmsjkS.htm](equipment/equipment-10-dipcMOtBFxtmsjkS.htm)|Cloak of the Bat|Cape de la chauve-souris|libre|
|[equipment-10-divPto5fH5FdrV1V.htm](equipment/equipment-10-divPto5fH5FdrV1V.htm)|Scope of Limning|Lunette d'exposition|libre|
|[equipment-10-eoI3M6FXtcPWeg7i.htm](equipment/equipment-10-eoI3M6FXtcPWeg7i.htm)|Barding of the Zephyr|Barde du zéphir|libre|
|[equipment-10-FNNfdpu1SfZtLohQ.htm](equipment/equipment-10-FNNfdpu1SfZtLohQ.htm)|Star Chart Tattoo|Tatouage de carte stellaire|libre|
|[equipment-10-FTTrNjLsSlrPvkLi.htm](equipment/equipment-10-FTTrNjLsSlrPvkLi.htm)|Specialist's Ring (Abjuration)|Anneau du spécialiste (Abjuration)|libre|
|[equipment-10-fuDYKpDN9RmCX7do.htm](equipment/equipment-10-fuDYKpDN9RmCX7do.htm)|Razmiri Wayfinder|Guide Razmirien|libre|
|[equipment-10-G8Jy1VK6JIg1hfKp.htm](equipment/equipment-10-G8Jy1VK6JIg1hfKp.htm)|Wand of Tormented Slumber|Baguette du sommeil tourmenté|libre|
|[equipment-10-geSsIAYv85MZtppg.htm](equipment/equipment-10-geSsIAYv85MZtppg.htm)|Compass of Transpositional Awareness|Boussole d'acuité transpositionnelle|libre|
|[equipment-10-gKlxurDeLS95zuuY.htm](equipment/equipment-10-gKlxurDeLS95zuuY.htm)|Specialist's Ring (Enchantment)|Anneau du spécialiste (Enchantement)|libre|
|[equipment-10-GL1C4vC6mmXFm25L.htm](equipment/equipment-10-GL1C4vC6mmXFm25L.htm)|Wand of Choking Mist (Solid Fog)|Baguette de brume étouffante (Brouillard dense)|libre|
|[equipment-10-gtDHRMgsyERaxf2c.htm](equipment/equipment-10-gtDHRMgsyERaxf2c.htm)|Instinct Crown (Superstition)|Couronne d'instinct (superstitition)|libre|
|[equipment-10-Gw9z5RUNPOjLUdRm.htm](equipment/equipment-10-Gw9z5RUNPOjLUdRm.htm)|Dragon's Breath (4th Level Spell)|Souffle de dragon de sort de rang 4 (rune)|libre|
|[equipment-10-H44rJpMzx6JyoKoa.htm](equipment/equipment-10-H44rJpMzx6JyoKoa.htm)|Specialist's Ring (Illusion)|Anneau du spécialiste (Illusion)|libre|
|[equipment-10-H9qYN48voa2ZDy3i.htm](equipment/equipment-10-H9qYN48voa2ZDy3i.htm)|Impactful|Impactante (rune)|libre|
|[equipment-10-hA0o5qdKXcuPJVmn.htm](equipment/equipment-10-hA0o5qdKXcuPJVmn.htm)|Instinct Crown (Giant)|Couronne d'instinct (géant)|libre|
|[equipment-10-hgWCwA6Yfqiz9tyr.htm](equipment/equipment-10-hgWCwA6Yfqiz9tyr.htm)|Wand of Chromatic Burst (4th-level)|Baguette d'explosion chromatique (rang 4)|libre|
|[equipment-10-HhtZl2pr7xChKu2c.htm](equipment/equipment-10-HhtZl2pr7xChKu2c.htm)|Quenching (Greater)|Extinction supérieure (rune)|libre|
|[equipment-10-hpNsjNQn9Gg6N8Kh.htm](equipment/equipment-10-hpNsjNQn9Gg6N8Kh.htm)|Twilight Lantern (Moderate)|Lanterne crépusculaire modérée|libre|
|[equipment-10-htukDa8cAycwf7zr.htm](equipment/equipment-10-htukDa8cAycwf7zr.htm)|Instinct Crown (Fury)|Couronne d'instinct (furie)|libre|
|[equipment-10-hUhH5QeSZ8B7Yg65.htm](equipment/equipment-10-hUhH5QeSZ8B7Yg65.htm)|Lost Ember|Ambre perdue|libre|
|[equipment-10-hvlEFx25ogf1K1C2.htm](equipment/equipment-10-hvlEFx25ogf1K1C2.htm)|Living Mantle|Manteau vivant|libre|
|[equipment-10-HvmIoRCaQJmkwluG.htm](equipment/equipment-10-HvmIoRCaQJmkwluG.htm)|Talisman Cord|Cordon de talisman modéré|libre|
|[equipment-10-HY6ZJpn5t0FDMTKM.htm](equipment/equipment-10-HY6ZJpn5t0FDMTKM.htm)|Conundrum Spectacles (Greater)|Lunettes d'énigmes supérieures|libre|
|[equipment-10-IM9W5fovv44ZKDRQ.htm](equipment/equipment-10-IM9W5fovv44ZKDRQ.htm)|Entertainer's Cincture|Ceinture d'artiste|libre|
|[equipment-10-IS1P6er2hLyKXAss.htm](equipment/equipment-10-IS1P6er2hLyKXAss.htm)|Emerald Fulcrum Lens|Lentille à pivot émeraude|libre|
|[equipment-10-isWSWeuJR2cYVdfi.htm](equipment/equipment-10-isWSWeuJR2cYVdfi.htm)|Standard of the Primeval Howl|Étendard du hurlement primitif|libre|
|[equipment-10-jrjwukkie7Y7wkxu.htm](equipment/equipment-10-jrjwukkie7Y7wkxu.htm)|Magnetizing|Magnétisante (rune)|libre|
|[equipment-10-Jt3q4i2FlRPZ14VV.htm](equipment/equipment-10-Jt3q4i2FlRPZ14VV.htm)|Specialist's Ring (Transmutation)|Anneau du spécialiste (Transmutation)|libre|
|[equipment-10-jx1a74ytWBN2ylD9.htm](equipment/equipment-10-jx1a74ytWBN2ylD9.htm)|Wand of Hybrid Form (4th-level)|Baguette de forme hybride (rang 4)|libre|
|[equipment-10-kkFVSqaefWrVs50t.htm](equipment/equipment-10-kkFVSqaefWrVs50t.htm)|Menacing (Greater)|Menaçante supérieure (rune)|libre|
|[equipment-10-l0UMYlCZEUaHExKt.htm](equipment/equipment-10-l0UMYlCZEUaHExKt.htm)|Volcanic Vigor|Vigueur volcanique|libre|
|[equipment-10-mmdnWrQsh7vDspLK.htm](equipment/equipment-10-mmdnWrQsh7vDspLK.htm)|Daredevil Boots|Bottes de casse-cou|libre|
|[equipment-10-mO9VXq8UkQodAucJ.htm](equipment/equipment-10-mO9VXq8UkQodAucJ.htm)|Witch Token|Charme de sorcier|libre|
|[equipment-10-Mwwgn9WVH9WbrAT0.htm](equipment/equipment-10-Mwwgn9WVH9WbrAT0.htm)|Presentable (Greater)|Présentable supérieure (rune)|libre|
|[equipment-10-MxS9ifx1gr1PKxBl.htm](equipment/equipment-10-MxS9ifx1gr1PKxBl.htm)|Instinct Crown (Spirit)|Couronne d'instinct (esprit)|libre|
|[equipment-10-nE12I5dTvJFPr9at.htm](equipment/equipment-10-nE12I5dTvJFPr9at.htm)|Specialist's Ring (Conjuration)|Anneau du spécialiste (Invocation)|libre|
|[equipment-10-nowjVXddPXr6fMnR.htm](equipment/equipment-10-nowjVXddPXr6fMnR.htm)|Arctic Vigor|Vigueur arctique|libre|
|[equipment-10-ntpPBKRcihHyER5V.htm](equipment/equipment-10-ntpPBKRcihHyER5V.htm)|Wand of Mental Purification (4th-level)|Baguette de purification mentale (rang 4)|libre|
|[equipment-10-Nv4DjyBqIMXQ4KM4.htm](equipment/equipment-10-Nv4DjyBqIMXQ4KM4.htm)|Wand of Noisome Acid (4nd-Level Spell)|Baguette de pestilence acide (rang 4)|libre|
|[equipment-10-NYehHw93LSgGG9vD.htm](equipment/equipment-10-NYehHw93LSgGG9vD.htm)|Tooth and Claw Tattoo (Major)|Tatouage dent et griffe majeur|libre|
|[equipment-10-nyLV5YVh90kYGaBd.htm](equipment/equipment-10-nyLV5YVh90kYGaBd.htm)|Specialist's Ring (Divination)|Anneau du spécialiste (Divination)|libre|
|[equipment-10-ofhqc85Z3Rt7dcrQ.htm](equipment/equipment-10-ofhqc85Z3Rt7dcrQ.htm)|Wand of Teeming Ghosts (4th-Level Spell)|Baguette d'esprits grouillants (rang 4)|libre|
|[equipment-10-ooaErd0DHZYju9rF.htm](equipment/equipment-10-ooaErd0DHZYju9rF.htm)|Wand of Hawthorn (4th-level)|Baguette d'aubépine (rang 4)|libre|
|[equipment-10-OTeONq4r10Xm6gSy.htm](equipment/equipment-10-OTeONq4r10Xm6gSy.htm)|Ring of Lies|Anneau de mensonges|libre|
|[equipment-10-oyjDCHscjcLhzall.htm](equipment/equipment-10-oyjDCHscjcLhzall.htm)|Wand of Crackling Lightning (4th-Level Spell)|Baguette d'électricité crépitante (rang 4)|libre|
|[equipment-10-PjCowzEMV2aJnHkV.htm](equipment/equipment-10-PjCowzEMV2aJnHkV.htm)|Specialist's Ring (Necromancy)|Anneau du spécialiste (Nécromancie)|libre|
|[equipment-10-PMCk6NCDvJs1MBcg.htm](equipment/equipment-10-PMCk6NCDvJs1MBcg.htm)|Codebreaker's Parchment (Greater)|Parchemin de codage supérieur|libre|
|[equipment-10-pOoEiuEuITm4I2Il.htm](equipment/equipment-10-pOoEiuEuITm4I2Il.htm)|Weapon Potency (+2)|Puissance d'arme +2 (rune)|libre|
|[equipment-10-PUeRq04dCRkYRx5s.htm](equipment/equipment-10-PUeRq04dCRkYRx5s.htm)|Poison Concentrator (Moderate)|Concentrateur de poison modéré|libre|
|[equipment-10-pVjtw5KSQF1YF4Jp.htm](equipment/equipment-10-pVjtw5KSQF1YF4Jp.htm)|Everair Mask (Greater)|Masque à air continu supérieur|libre|
|[equipment-10-QbCraR5KFlkIJiVA.htm](equipment/equipment-10-QbCraR5KFlkIJiVA.htm)|Instinct Crown (Animal)|Couronne d'instinct (animal)|libre|
|[equipment-10-QEPx1fCf74xdpXBH.htm](equipment/equipment-10-QEPx1fCf74xdpXBH.htm)|Thurible of Revelation (Moderate)|Encensoir de révélation modéré|libre|
|[equipment-10-QsRmhxeuf9v4h3Dp.htm](equipment/equipment-10-QsRmhxeuf9v4h3Dp.htm)|Immovable Tripod|Trépied inamovible|libre|
|[equipment-10-QT1jTPlf3ATYUh4I.htm](equipment/equipment-10-QT1jTPlf3ATYUh4I.htm)|The Whispering Reeds|Les Roseaux chuchotants|libre|
|[equipment-10-QZliTmapMRrEJ6q8.htm](equipment/equipment-10-QZliTmapMRrEJ6q8.htm)|Elemental Wayfinder (Water)|Guide élémentaire (eau)|libre|
|[equipment-10-Rf9sn0afKTzyFgaH.htm](equipment/equipment-10-Rf9sn0afKTzyFgaH.htm)|Tome of Dripping Shadows|Tome des ombres dégoulinantes|libre|
|[equipment-10-rqwmqnCZ0xz7vF1b.htm](equipment/equipment-10-rqwmqnCZ0xz7vF1b.htm)|Dinosaur Boots|Bottes de dinosaure|libre|
|[equipment-10-RtQwbPiJs2FRyfNE.htm](equipment/equipment-10-RtQwbPiJs2FRyfNE.htm)|Specialist's Ring|Anneau du spécialiste|libre|
|[equipment-10-RTxne78VqPqTz2VN.htm](equipment/equipment-10-RTxne78VqPqTz2VN.htm)|Wondrous Figurine (Golden Lions)|Figurine merveilleuse (Lions dorés)|officielle|
|[equipment-10-shKPtYN0YUOe07K2.htm](equipment/equipment-10-shKPtYN0YUOe07K2.htm)|Demon Mask (Greater)|Masque du démon supérieur|libre|
|[equipment-10-SOZ7pMEzjVKbwxqe.htm](equipment/equipment-10-SOZ7pMEzjVKbwxqe.htm)|Jaathoom's Scarf|Écharpe du Jaathoom|libre|
|[equipment-10-SuKERInt6Z3I3bCa.htm](equipment/equipment-10-SuKERInt6Z3I3bCa.htm)|Anchoring|Ancrage (rune)|libre|
|[equipment-10-SV7W0lC2d8mfYuhy.htm](equipment/equipment-10-SV7W0lC2d8mfYuhy.htm)|Serrating|Dentelée (rune)|libre|
|[equipment-10-tIZ3Wi2JInY9abu8.htm](equipment/equipment-10-tIZ3Wi2JInY9abu8.htm)|Exploration Lens (Detecting)|Lentille d'exploration de détection|libre|
|[equipment-10-trKFKb5BKa0kxuTJ.htm](equipment/equipment-10-trKFKb5BKa0kxuTJ.htm)|Enigma Mirror (Greater)|Miroir énigmatique supérieur|libre|
|[equipment-10-U5MKmx60jAX4xV8x.htm](equipment/equipment-10-U5MKmx60jAX4xV8x.htm)|Book of Lingering Blaze|Livre du brasier persistant|libre|
|[equipment-10-uiJAR3jQbQHhiP3Q.htm](equipment/equipment-10-uiJAR3jQbQHhiP3Q.htm)|Charlatan's Cape|Cape de prestidigateur|libre|
|[equipment-10-uiTJWd8IJevpkjbq.htm](equipment/equipment-10-uiTJWd8IJevpkjbq.htm)|Palanquin of Night|Palanquin de nuit|libre|
|[equipment-10-VPjzEstF31PFSnyG.htm](equipment/equipment-10-VPjzEstF31PFSnyG.htm)|Open Mind|Esprit ouvert|libre|
|[equipment-10-vuvRsWXrzHbs0tF5.htm](equipment/equipment-10-vuvRsWXrzHbs0tF5.htm)|Replacement Filter (Level 10)|Filtre de remplacement (niveau 10)|libre|
|[equipment-10-vUXtEGa3eFLO5MCt.htm](equipment/equipment-10-vUXtEGa3eFLO5MCt.htm)|Retaliation (Moderate)|Représailles modérées (rune)|libre|
|[equipment-10-wc1T0d1mGSwJallY.htm](equipment/equipment-10-wc1T0d1mGSwJallY.htm)|Ring of Observation (Greater)|Anneau d'observation supérieur|libre|
|[equipment-10-whShgzN5YBzrXV3r.htm](equipment/equipment-10-whShgzN5YBzrXV3r.htm)|Alchemical Atomizer|Atomiseur alchimique|libre|
|[equipment-10-Wtm4JdxeTLQi3zQI.htm](equipment/equipment-10-Wtm4JdxeTLQi3zQI.htm)|Symbol of Conflict (Greater)|Symbole du conflit supérieur|libre|
|[equipment-10-X3Nfa7bByYFrg1lU.htm](equipment/equipment-10-X3Nfa7bByYFrg1lU.htm)|Charm of Resistance (Greater)|Charme de résistance aux énergies supérieur|libre|
|[equipment-10-XeiuCRFzxHA7mm0X.htm](equipment/equipment-10-XeiuCRFzxHA7mm0X.htm)|Endless Grimoire (Greater)|Grimoire sans fin supérieur|libre|
|[equipment-10-XgKwydoro5eaIWC8.htm](equipment/equipment-10-XgKwydoro5eaIWC8.htm)|Wand of Reaching (4th-level)|Baguette d'atteinte (rang 4)|libre|
|[equipment-10-yJAhgNvKFcCSPpG3.htm](equipment/equipment-10-yJAhgNvKFcCSPpG3.htm)|Kraken Figurehead|Figure de proue kraken|libre|
|[equipment-10-yjC2nrfqnKmFerHs.htm](equipment/equipment-10-yjC2nrfqnKmFerHs.htm)|Judgment Thurible|Encensoir du jugement|libre|
|[equipment-10-yo6kdcUhqv5YyMM5.htm](equipment/equipment-10-yo6kdcUhqv5YyMM5.htm)|Blast Foot (Greater)|Pied explosif supérieur|libre|
|[equipment-10-YU9FFzOsMDdmKW7x.htm](equipment/equipment-10-YU9FFzOsMDdmKW7x.htm)|Wand of Mercy (4th-level)|Baguette de miséricorde (rang 4)|libre|
|[equipment-10-zAnxjSoJBggguWFP.htm](equipment/equipment-10-zAnxjSoJBggguWFP.htm)|Jyoti's Feather|Plume de Jyoti|libre|
|[equipment-10-zYRzgETeR1Hs1ti1.htm](equipment/equipment-10-zYRzgETeR1Hs1ti1.htm)|Wand of Widening (4th-Rank Spell)|Baguette d'élargissement (sort de rang 4)|libre|
|[equipment-11-4Rc4arQ7p61MCb0p.htm](equipment/equipment-11-4Rc4arQ7p61MCb0p.htm)|Armory Bracelet (Greater)|Bracelet arsenal supérieur|libre|
|[equipment-11-5LfD4O0zakN0fw7b.htm](equipment/equipment-11-5LfD4O0zakN0fw7b.htm)|Bravery Baldric (Stoneskin)|Baudrier de bravoure (Résilience du rocher)|libre|
|[equipment-11-5PmuusQRWQBmexGy.htm](equipment/equipment-11-5PmuusQRWQBmexGy.htm)|Ice Forge|Forge de glace|libre|
|[equipment-11-5uFUIRw4YUIn0OR7.htm](equipment/equipment-11-5uFUIRw4YUIn0OR7.htm)|Alchemist's Haversack|Havresac d'alchimiste|libre|
|[equipment-11-62vH66XLehPiRwwo.htm](equipment/equipment-11-62vH66XLehPiRwwo.htm)|Cassock of Devotion|Soutane de dévotion|officielle|
|[equipment-11-85GqeSGXjSQwLy07.htm](equipment/equipment-11-85GqeSGXjSQwLy07.htm)|Wand of Overflowing Life (4th-Level Spell)|Baguette de vie débordante (rang 4)|libre|
|[equipment-11-8NgA4PFFXZPZ9SSg.htm](equipment/equipment-11-8NgA4PFFXZPZ9SSg.htm)|Boots of Elvenkind (Greater)|Bottes elfiques supérieures|officielle|
|[equipment-11-8XZqdeZStFAM4XnH.htm](equipment/equipment-11-8XZqdeZStFAM4XnH.htm)|Alchemist Goggles (Greater)|Lunettes d'alchimiste supérieur|officielle|
|[equipment-11-9YtYO8u5UOg4q64Y.htm](equipment/equipment-11-9YtYO8u5UOg4q64Y.htm)|Wand of the Spider (4th-Level Spell)|Baguette de l'araignée (rang 4)|libre|
|[equipment-11-a5dEzZPuxsmTvlWS.htm](equipment/equipment-11-a5dEzZPuxsmTvlWS.htm)|Insistent Door Knocker (Greater)|Heurtoir insistant supérieur|libre|
|[equipment-11-A7TCZXIwyxdqYSas.htm](equipment/equipment-11-A7TCZXIwyxdqYSas.htm)|Singing Bowl of the Versatile Stance|Bol chantant de la posture polyvalente|libre|
|[equipment-11-anZ6U7zFEAfiv3gh.htm](equipment/equipment-11-anZ6U7zFEAfiv3gh.htm)|Wand of Spiritual Warfare (4th-Level Spell)|Baguette de guerre spirituelle (rang 4)|libre|
|[equipment-11-as84dZ5Hliru148n.htm](equipment/equipment-11-as84dZ5Hliru148n.htm)|Devoted Vestments|Vêtements de fidèle|libre|
|[equipment-11-aU3vBG9EinYgYlWb.htm](equipment/equipment-11-aU3vBG9EinYgYlWb.htm)|Spirit-Singer (Incredible)|Esprit-chanteur incroyable|libre|
|[equipment-11-bCsdAkffuk29ssUg.htm](equipment/equipment-11-bCsdAkffuk29ssUg.htm)|Wand of Continuation (4th-Rank Spell)|Baguette de prolongation (sort de rang 4)|libre|
|[equipment-11-Dr9b08JhDThUenF9.htm](equipment/equipment-11-Dr9b08JhDThUenF9.htm)|Necklace of Fireballs IV|Collier à boules de feu IV|officielle|
|[equipment-11-dvHvLFTbjnsE0xoN.htm](equipment/equipment-11-dvHvLFTbjnsE0xoN.htm)|Countering Charm|Charme de contre|libre|
|[equipment-11-E0hJQdtwwbVrziRt.htm](equipment/equipment-11-E0hJQdtwwbVrziRt.htm)|Sandcastle|Château de sable|libre|
|[equipment-11-efm6thOteY7PWEvg.htm](equipment/equipment-11-efm6thOteY7PWEvg.htm)|Calamity Glass|Verre de calamité|libre|
|[equipment-11-eoQRjG2RjppnbYGL.htm](equipment/equipment-11-eoQRjG2RjppnbYGL.htm)|Eye of the Wise|Oeil du sage|officielle|
|[equipment-11-GHaUZqivF30WJHS3.htm](equipment/equipment-11-GHaUZqivF30WJHS3.htm)|Brightbloom Posy (Greater)|Bouquet de fleurs lumineuses supérieur|libre|
|[equipment-11-gRQXLqUuP4GWfDWI.htm](equipment/equipment-11-gRQXLqUuP4GWfDWI.htm)|Doubling Rings (Greater)|Anneaux de doublement supérieurs|officielle|
|[equipment-11-ibA3FykNQcoEPfPC.htm](equipment/equipment-11-ibA3FykNQcoEPfPC.htm)|Icy Disposition|Dispositions glaciales (Pacte infernal)|libre|
|[equipment-11-J4QuF3h13uFElZyX.htm](equipment/equipment-11-J4QuF3h13uFElZyX.htm)|Golden Goose|Oie dorée|libre|
|[equipment-11-JPVQ0qvTaKkTF2z4.htm](equipment/equipment-11-JPVQ0qvTaKkTF2z4.htm)|Sealing Chest (Greater)|Coffre scellé supérieur|libre|
|[equipment-11-JwHNSaiwwqF7VCC5.htm](equipment/equipment-11-JwHNSaiwwqF7VCC5.htm)|Fulcrum Lattice|Cadre à lentilles|libre|
|[equipment-11-kcRQT67sLes1cC72.htm](equipment/equipment-11-kcRQT67sLes1cC72.htm)|Boots of Free Running (Moderate)|Bottes de course libre modérées|libre|
|[equipment-11-klMBWHVZ5EFU4cTI.htm](equipment/equipment-11-klMBWHVZ5EFU4cTI.htm)|Vigilant Eye (Greater)|Oeil vigilant supérieur|libre|
|[equipment-11-KtpxwEfpbfDDEURF.htm](equipment/equipment-11-KtpxwEfpbfDDEURF.htm)|Dawnlight (Major)|Lumière de l'aube majeure|libre|
|[equipment-11-KwkZfSgS1z3XM0nl.htm](equipment/equipment-11-KwkZfSgS1z3XM0nl.htm)|Ash Gown (Greater)|Toge cendreuse supérieure|libre|
|[equipment-11-l68lJrjtjjkV7NiJ.htm](equipment/equipment-11-l68lJrjtjjkV7NiJ.htm)|Wand of Thundering Echoes (4th-Level Spell)|Baguette des échos tonitruants (rang 4)|libre|
|[equipment-11-M0tbJD3SEW4pClpb.htm](equipment/equipment-11-M0tbJD3SEW4pClpb.htm)|Brooch of Inspiration (Greater)|Broche d'inspiration supérieure|libre|
|[equipment-11-mfhcGeS0L7pPZnex.htm](equipment/equipment-11-mfhcGeS0L7pPZnex.htm)|Energy-Absorbing|Absorption d'énergie (rune)|libre|
|[equipment-11-mXfoYKagvi8z4xkk.htm](equipment/equipment-11-mXfoYKagvi8z4xkk.htm)|Words of Wisdom (Moderate)|Paroles de sagesse modérées|libre|
|[equipment-11-NCE7g1U3q3RYwCY2.htm](equipment/equipment-11-NCE7g1U3q3RYwCY2.htm)|Hopeful|Espoir (rune)|libre|
|[equipment-11-o0XXVVymB8kluwLK.htm](equipment/equipment-11-o0XXVVymB8kluwLK.htm)|Hauling (Greater)|Traction supérieure (rune)|libre|
|[equipment-11-OUC64caWaH6xayl1.htm](equipment/equipment-11-OUC64caWaH6xayl1.htm)|Robe of Stone|Robe de pierre|libre|
|[equipment-11-R4yGXp42qXaWEhLQ.htm](equipment/equipment-11-R4yGXp42qXaWEhLQ.htm)|Bravery Baldric (Flight)|Baudrier de bravoure (Vol)|libre|
|[equipment-11-RC3axCXEWwiSEEUl.htm](equipment/equipment-11-RC3axCXEWwiSEEUl.htm)|Gate Attenuator (Greater)|Atténuateur de portail supérieur|libre|
|[equipment-11-rj5tlZn0yGXG5KVH.htm](equipment/equipment-11-rj5tlZn0yGXG5KVH.htm)|Fan of Soothing Winds|Éventail de vents apaisants|libre|
|[equipment-11-SGbEZzm5MLSv1DDX.htm](equipment/equipment-11-SGbEZzm5MLSv1DDX.htm)|Envisioning Mask|Masque de projection psychique|libre|
|[equipment-11-SsXS70dp2UnFCtdw.htm](equipment/equipment-11-SsXS70dp2UnFCtdw.htm)|Spectral Opera Glasses|Lunettes d'opéra spectrales|libre|
|[equipment-11-T5KifnxLN3vWuUJa.htm](equipment/equipment-11-T5KifnxLN3vWuUJa.htm)|Rooting (Greater)|Enracinante supérieure (rune)|libre|
|[equipment-11-Tej30MsQo8Ek9aA6.htm](equipment/equipment-11-Tej30MsQo8Ek9aA6.htm)|Obsidian Goggles (Greater)|Lunettes d'obsidienne supérieures|libre|
|[equipment-11-Ua9KWFlVjXW8sSXm.htm](equipment/equipment-11-Ua9KWFlVjXW8sSXm.htm)|Gamepiece Chariot|Pion char|libre|
|[equipment-11-UQpbEAeOUkascA4L.htm](equipment/equipment-11-UQpbEAeOUkascA4L.htm)|Ebon Marionette|Marionnette en ébène|libre|
|[equipment-11-vncchGeyuAYRtfxr.htm](equipment/equipment-11-vncchGeyuAYRtfxr.htm)|Sparkshade Parasol|Parasol ombre d'étincelle|libre|
|[equipment-11-xdPU7qrl5hIvaArY.htm](equipment/equipment-11-xdPU7qrl5hIvaArY.htm)|Horn of Exorcism|Corne d'exorcisme|libre|
|[equipment-11-XkjOK05Gw0o3iycr.htm](equipment/equipment-11-XkjOK05Gw0o3iycr.htm)|Implacable|Implacable (rune)|libre|
|[equipment-11-YguYZuHT6k1jVBvy.htm](equipment/equipment-11-YguYZuHT6k1jVBvy.htm)|Eternal Eruption of Mhar Massif|Éternelle éruption du Massif de Mhar|libre|
|[equipment-11-ZEf04zjwyiq1pLPj.htm](equipment/equipment-11-ZEf04zjwyiq1pLPj.htm)|Shadow Manse|Manoir de l'ombre|libre|
|[equipment-11-ZR3Kekd3hrWmAv6d.htm](equipment/equipment-11-ZR3Kekd3hrWmAv6d.htm)|Blazons of Shared Power (Greater)|Blasons du pouvoir partagé supérieurs|libre|
|[equipment-12-0VtYhRt2BneBzh32.htm](equipment/equipment-12-0VtYhRt2BneBzh32.htm)|Splendid Floodlight|Magnifique projecteur|libre|
|[equipment-12-1erd18HS57aCyC6r.htm](equipment/equipment-12-1erd18HS57aCyC6r.htm)|Flying Broomstick|Balai aérien|libre|
|[equipment-12-1kWWhTpXf68BdCWe.htm](equipment/equipment-12-1kWWhTpXf68BdCWe.htm)|Wand of Traitorous Thoughts|Baguette des pensées traitresses|libre|
|[equipment-12-1oOalZRwSxcHewaJ.htm](equipment/equipment-12-1oOalZRwSxcHewaJ.htm)|Wraithweave Patch (Type II)|Étoffe tressée d'âme-en-peine (Type II)|libre|
|[equipment-12-3vKhyjBGjaN4rNyl.htm](equipment/equipment-12-3vKhyjBGjaN4rNyl.htm)|Rune of Sin (Wrath)|Rune du péché de colère|libre|
|[equipment-12-4cbe0WVSHn1FxygQ.htm](equipment/equipment-12-4cbe0WVSHn1FxygQ.htm)|Dread (Moderate)|Effroi modéré (rune)|libre|
|[equipment-12-708LeeyJMXFio80R.htm](equipment/equipment-12-708LeeyJMXFio80R.htm)|Rune of Sin (Lust)|Rune du péché de luxure|libre|
|[equipment-12-7U16hk57BHVM5QYs.htm](equipment/equipment-12-7U16hk57BHVM5QYs.htm)|Reading Ring (Greater)|Anneau de lecture supérieur|libre|
|[equipment-12-8CmzsexkT42I30Pl.htm](equipment/equipment-12-8CmzsexkT42I30Pl.htm)|Wand of Hybrid Form (5th-level)|Baguette de forme hybride (rang 5)|libre|
|[equipment-12-aBMTXPmHg9EDe2R6.htm](equipment/equipment-12-aBMTXPmHg9EDe2R6.htm)|Spiny Lodestone (Major)|Pierre aimantée épineuse majeure|libre|
|[equipment-12-aGZJ6dEHTHxUdnXz.htm](equipment/equipment-12-aGZJ6dEHTHxUdnXz.htm)|Warcaller's Chime of Restoration|Carillon de rétablissement du belliqueux|libre|
|[equipment-12-akVtwAW1b2TQRbRp.htm](equipment/equipment-12-akVtwAW1b2TQRbRp.htm)|Jabali's Dice|Dés du jabali|libre|
|[equipment-12-aVHz2tXvlNDFIneB.htm](equipment/equipment-12-aVHz2tXvlNDFIneB.htm)|Fossil Fragment (Eurypterid Paddle)|Fragment de fossile (Palme d'euryptéride)|libre|
|[equipment-12-aZ8HzjZCOJCV7ZWh.htm](equipment/equipment-12-aZ8HzjZCOJCV7ZWh.htm)|Marvelous Medicines|Remèdes merveilleux|libre|
|[equipment-12-bCOxPYXLL6uGuVCn.htm](equipment/equipment-12-bCOxPYXLL6uGuVCn.htm)|Rune of Sin (Greed)|Rune du péché d'avavrice|libre|
|[equipment-12-BcYUzXVZIiQ2eVPF.htm](equipment/equipment-12-BcYUzXVZIiQ2eVPF.htm)|Highhelm Drill Mark II|Foreuse de Heaume - Mark II|libre|
|[equipment-12-beHqICAYt9ZuyBay.htm](equipment/equipment-12-beHqICAYt9ZuyBay.htm)|Grim Sandglass (Major)|Sablier sinistre majeur|libre|
|[equipment-12-ciykvIC4SFFxIfUw.htm](equipment/equipment-12-ciykvIC4SFFxIfUw.htm)|Swallow-Spike (Greater)|Pique avalée supérieure (rune)|libre|
|[equipment-12-clcqWFuj42N28oeT.htm](equipment/equipment-12-clcqWFuj42N28oeT.htm)|Wand of Mercy (5th-level)|Baguette de miséricorde (rang 5)|libre|
|[equipment-12-CXjHjdGVlOoXeYpr.htm](equipment/equipment-12-CXjHjdGVlOoXeYpr.htm)|Wand of Dazzling Rays (5th-level)|Baguette des rayons éblouissants (rang 5)|libre|
|[equipment-12-cxVbGSUjmc2yxVh5.htm](equipment/equipment-12-cxVbGSUjmc2yxVh5.htm)|Grub Gloves (Moderate)|Gants à larve modérés|libre|
|[equipment-12-DdqGs9aSwjSl0XyY.htm](equipment/equipment-12-DdqGs9aSwjSl0XyY.htm)|Nosoi Charm|Charme Nosoi|libre|
|[equipment-12-dPwRgQKEFLLDF2iB.htm](equipment/equipment-12-dPwRgQKEFLLDF2iB.htm)|Wand of Reaching (5th-level)|Baguette d'atteinte (rang 5)|libre|
|[equipment-12-dtQnQn4LBSIAzQCZ.htm](equipment/equipment-12-dtQnQn4LBSIAzQCZ.htm)|Alchemical Chart (Moderate)|Table alchimique modérée|libre|
|[equipment-12-DwLaGtbBdCh2NFbT.htm](equipment/equipment-12-DwLaGtbBdCh2NFbT.htm)|Berserker's Cloak|Cape du Berseker|officielle|
|[equipment-12-EDIbewxih3ihNdQC.htm](equipment/equipment-12-EDIbewxih3ihNdQC.htm)|Rune of Sin (Pride)|Rune du péché de l'orgueil|libre|
|[equipment-12-ENF3Er7cJvb4oXMM.htm](equipment/equipment-12-ENF3Er7cJvb4oXMM.htm)|Old Tillimaquin|Vieux tillimaquin|libre|
|[equipment-12-f9ASlZXozFrknwd9.htm](equipment/equipment-12-f9ASlZXozFrknwd9.htm)|Rune of Sin (Envy)|Rune du péché de l'envie|libre|
|[equipment-12-fdntl5OOhtYIPSBi.htm](equipment/equipment-12-fdntl5OOhtYIPSBi.htm)|Sanguine Fang (Greater)|Canine sanguine supérieure|libre|
|[equipment-12-fFx6SEyZlHZtcLGX.htm](equipment/equipment-12-fFx6SEyZlHZtcLGX.htm)|Ring of Swimming|Anneau de nage|officielle|
|[equipment-12-FSo4D0bnDeRAURip.htm](equipment/equipment-12-FSo4D0bnDeRAURip.htm)|Ring of Stoneshifting|Anneau de déplacement terreux|libre|
|[equipment-12-fUzFWIPK4mdZSGyH.htm](equipment/equipment-12-fUzFWIPK4mdZSGyH.htm)|Stampede Medallion|Médaillon de la harde|libre|
|[equipment-12-GpuzMoZYehYvZ50E.htm](equipment/equipment-12-GpuzMoZYehYvZ50E.htm)|Ring of Climbing|Anneau d'escalade|officielle|
|[equipment-12-hLVRI4JS5vsyfPoV.htm](equipment/equipment-12-hLVRI4JS5vsyfPoV.htm)|Faith Tattoo (Major)|Tatouage du croyant majeur|libre|
|[equipment-12-Hu9n7JNutzFarHe7.htm](equipment/equipment-12-Hu9n7JNutzFarHe7.htm)|Hat of Many Minds|Châpeau des nombreux esprits|libre|
|[equipment-12-hwszIqJguVfoc3ZD.htm](equipment/equipment-12-hwszIqJguVfoc3ZD.htm)|Quick Runner's Shirt (Greater)|Chemise du coureur rapide supérieure|libre|
|[equipment-12-I0XiBnHFCXi64XiD.htm](equipment/equipment-12-I0XiBnHFCXi64XiD.htm)|Cloak of Illusions (Greater)|Cape des illusions supérieure|libre|
|[equipment-12-J7vQowV531Sz73Gw.htm](equipment/equipment-12-J7vQowV531Sz73Gw.htm)|Aeon Stone (Black Pearl)|Pierre d'éternité (perle noire)|officielle|
|[equipment-12-JWacChecDZbdo6sT.htm](equipment/equipment-12-JWacChecDZbdo6sT.htm)|Wand of Teeming Ghosts (5th-Level Spell)|Baguette d'esprits grouillants (rang 5)|libre|
|[equipment-12-jYQy2jFxbQL5y36L.htm](equipment/equipment-12-jYQy2jFxbQL5y36L.htm)|Cloak of Devouring Thorns|Cape des épines dévorantes|libre|
|[equipment-12-kakeEbGrFPwW6Rhu.htm](equipment/equipment-12-kakeEbGrFPwW6Rhu.htm)|Pipes of Compulsion (Major)|Tuyaux de servitude majeurs|libre|
|[equipment-12-KCQRFvUgbyclvOGi.htm](equipment/equipment-12-KCQRFvUgbyclvOGi.htm)|Striking (Greater)|Frappe supérieure (rune)|officielle|
|[equipment-12-kRemZF5AgTzHRHM6.htm](equipment/equipment-12-kRemZF5AgTzHRHM6.htm)|Wand of Rolling Flames (5th-level)|Baguette des flammes ronflantes (rang 5)|libre|
|[equipment-12-kvfdd4YiO3Lyywuv.htm](equipment/equipment-12-kvfdd4YiO3Lyywuv.htm)|Dragon's Breath (5th Level Spell)|Souffle de dragon de sort de rang 5 (rune)|libre|
|[equipment-12-lJ3BzUZkGTOCWleL.htm](equipment/equipment-12-lJ3BzUZkGTOCWleL.htm)|Aeon Stone (Pink Rhomboid)|Pierre d'éternité (rhombe rose vif)|officielle|
|[equipment-12-LKNsUuhm1CcUIWMh.htm](equipment/equipment-12-LKNsUuhm1CcUIWMh.htm)|Alluring Scarf (Greater)|Écharpe séduisante supérieure|libre|
|[equipment-12-lqzVHgmpCDnOVZs3.htm](equipment/equipment-12-lqzVHgmpCDnOVZs3.htm)|Queasy Lantern (Moderate)|Lanterne barbouilleuse modérée|libre|
|[equipment-12-MHvmgUSQlMAevvoF.htm](equipment/equipment-12-MHvmgUSQlMAevvoF.htm)|Spirit-Singer (Incredible Handheld)|Esprit-chanteur incroyable portable|libre|
|[equipment-12-mxI7Ah8eZNmonNKj.htm](equipment/equipment-12-mxI7Ah8eZNmonNKj.htm)|Spectacles of Inquiry|Lunettes d'enquête|libre|
|[equipment-12-n8nLwFR4VFFmAny5.htm](equipment/equipment-12-n8nLwFR4VFFmAny5.htm)|Immovable|Inamovible (rune)|libre|
|[equipment-12-NEbr7nKuiluJgBHT.htm](equipment/equipment-12-NEbr7nKuiluJgBHT.htm)|Lich Soul Cage|Phylactère de liche|libre|
|[equipment-12-nQIIO6LIot0ISQXH.htm](equipment/equipment-12-nQIIO6LIot0ISQXH.htm)|Codex of Unimpeded Sight (Greater)|Codex de vision sans entraves supérieur|libre|
|[equipment-12-nXq9pxB3RVnGb0rB.htm](equipment/equipment-12-nXq9pxB3RVnGb0rB.htm)|Drums of War (Major)|Tambour de guerre majeur|libre|
|[equipment-12-otCgznS0L14Z46rf.htm](equipment/equipment-12-otCgznS0L14Z46rf.htm)|Ring of Wizardry (Type III)|Anneau des arcanes (Type III)|libre|
|[equipment-12-pdsepgrBRgdZ4DWm.htm](equipment/equipment-12-pdsepgrBRgdZ4DWm.htm)|Wand of Legerdemain (5th-level)|Baguette de prestidigitation (rang 5)|libre|
|[equipment-12-PxHexTD0gzPtMFj6.htm](equipment/equipment-12-PxHexTD0gzPtMFj6.htm)|Wind-up Wings (Homing)|Ailes à manivelle de guidage|libre|
|[equipment-12-qFgIZQhyn5JOYkKr.htm](equipment/equipment-12-qFgIZQhyn5JOYkKr.htm)|Sharkskin Robe|Robe en galuchat|libre|
|[equipment-12-qFVPeeJ70B1u2n81.htm](equipment/equipment-12-qFVPeeJ70B1u2n81.htm)|Soul Cage|Phylactère|libre|
|[equipment-12-qHwbdYOPZ9mwmru1.htm](equipment/equipment-12-qHwbdYOPZ9mwmru1.htm)|Crown of the Fire Eater (Greater)|Couronne du mangeur de feu supérieure|libre|
|[equipment-12-qixSKcFT1OA4mIkp.htm](equipment/equipment-12-qixSKcFT1OA4mIkp.htm)|Goz Mask (Major)|Masque goz majeur|libre|
|[equipment-12-r81OfbBRPTXi0Fwd.htm](equipment/equipment-12-r81OfbBRPTXi0Fwd.htm)|Wyrm Claw|Griffe de ver|libre|
|[equipment-12-RpsT84dZxLWHcoFk.htm](equipment/equipment-12-RpsT84dZxLWHcoFk.htm)|Exploration Lens (Investigating)|Lentille d'exploration d'enquête|libre|
|[equipment-12-Shbg6XeBx94DXbcd.htm](equipment/equipment-12-Shbg6XeBx94DXbcd.htm)|Resonating Fork (Major)|Diapason résonant majeur|libre|
|[equipment-12-TGxZ3acyWjjTvfU9.htm](equipment/equipment-12-TGxZ3acyWjjTvfU9.htm)|Wand of Widening (5th-Rank Spell)|Baguette d'élargissement (sort de rang 5)|libre|
|[equipment-12-Ug0lQVTJlGKrHQgQ.htm](equipment/equipment-12-Ug0lQVTJlGKrHQgQ.htm)|Pickled Demon Tongue (Major)|Langue de démon marinée majeure|libre|
|[equipment-12-Vjyt4f9XK3HXtGl2.htm](equipment/equipment-12-Vjyt4f9XK3HXtGl2.htm)|Rune of Sin (Gluttony)|Rune du péché de gourmandise|libre|
|[equipment-12-wjrYnQcqimzFdswE.htm](equipment/equipment-12-wjrYnQcqimzFdswE.htm)|Linguist's Dictionary (Greater)|Dictionnaire du linguiste supérieur|libre|
|[equipment-12-xC4W6YAcu0rQFmgm.htm](equipment/equipment-12-xC4W6YAcu0rQFmgm.htm)|Wand of Dumbfounding Doom (5th-level)|Baguette de la fatalité abrutissante (rang 5)|libre|
|[equipment-12-XIQmnhUfmhojIZT7.htm](equipment/equipment-12-XIQmnhUfmhojIZT7.htm)|Thorn Triad (Major)|Triade d'épines majeure|libre|
|[equipment-12-xVQkxZJYx2tonOqO.htm](equipment/equipment-12-xVQkxZJYx2tonOqO.htm)|Wand of Mental Purification (5th-level)|Baguette de purification mentale (rang 5)|libre|
|[equipment-12-y8DbetZzNdng2LSi.htm](equipment/equipment-12-y8DbetZzNdng2LSi.htm)|Rune of Sin|Rune du péché|libre|
|[equipment-12-yl0r4LYPZK7DSZuW.htm](equipment/equipment-12-yl0r4LYPZK7DSZuW.htm)|Rune of Sin (Sloth)|Rune du péché de paresse|libre|
|[equipment-12-Yv6WDpfxcukReBl4.htm](equipment/equipment-12-Yv6WDpfxcukReBl4.htm)|Bring Me Near|Amène-moi à proximité|libre|
|[equipment-12-zToq18jKonWIp48U.htm](equipment/equipment-12-zToq18jKonWIp48U.htm)|Wand of Smoldering Fireballs (5th-Level Spell)|Baguette de boules de feu incendiaires (rang 5)|libre|
|[equipment-13-0Ju6ETQiDvz9qpi5.htm](equipment/equipment-13-0Ju6ETQiDvz9qpi5.htm)|Communication Pendants|Pendentif de communication|libre|
|[equipment-13-1cIPXV6brhfQW1cD.htm](equipment/equipment-13-1cIPXV6brhfQW1cD.htm)|Spellbreaking (Necromancy)|Brisesort : nécromancie (rune)|libre|
|[equipment-13-1n22FbWdDNC7tLT6.htm](equipment/equipment-13-1n22FbWdDNC7tLT6.htm)|Rock-Braced|Force du rocher (rune)|libre|
|[equipment-13-2HCiVszUnIPJwvQV.htm](equipment/equipment-13-2HCiVszUnIPJwvQV.htm)|Blightburn Necklace|Collier Anti-champibrûle|libre|
|[equipment-13-2jUWEHkn0XmNuGCh.htm](equipment/equipment-13-2jUWEHkn0XmNuGCh.htm)|Aeon Stone (Rainbow Prism)|Pierre d'éternité (prisme arc-en-ciel)|libre|
|[equipment-13-4tBK20Atzi5AgCd4.htm](equipment/equipment-13-4tBK20Atzi5AgCd4.htm)|Sandstorm Top|Toupie tempête de sable|libre|
|[equipment-13-6R3e78H9YwzmMbjF.htm](equipment/equipment-13-6R3e78H9YwzmMbjF.htm)|Fossil Fragment (Amber Mosquito)|Fragment de fossile (Moustique d'ambre)|libre|
|[equipment-13-9PoAibWdzGehHxKT.htm](equipment/equipment-13-9PoAibWdzGehHxKT.htm)|Spellbreaking (Enchantment)|Brisesort : enchantement (rune)|libre|
|[equipment-13-adFsNoFwHnWuMgPm.htm](equipment/equipment-13-adFsNoFwHnWuMgPm.htm)|Spell Bastion|Sort-rempart (rune)|libre|
|[equipment-13-aj2eTtEAgLu4f14b.htm](equipment/equipment-13-aj2eTtEAgLu4f14b.htm)|Wand of Overflowing Life (5th-Level Spell)|Baguette de vie débordante (rang 5)|libre|
|[equipment-13-B4R0LK8bgw0Vjtf0.htm](equipment/equipment-13-B4R0LK8bgw0Vjtf0.htm)|Aeon Stone (Pale Lavender Ellipsoid)|Pierre d'éternité (ellipsoïde lavande pâle)|libre|
|[equipment-13-DAWaXFtevHLUJxHB.htm](equipment/equipment-13-DAWaXFtevHLUJxHB.htm)|Energy Adaptive|Adaptation à l'énergie (rune)|libre|
|[equipment-13-enPo1dPzSQaybAfF.htm](equipment/equipment-13-enPo1dPzSQaybAfF.htm)|Guardian Aluum Charm|Charme de garde aluum|libre|
|[equipment-13-f8AI23xzAHyuU35m.htm](equipment/equipment-13-f8AI23xzAHyuU35m.htm)|Poisonous Cloak Type III|Cape empoisonnée (Type III)|officielle|
|[equipment-13-fv67rtvtBZyuduD8.htm](equipment/equipment-13-fv67rtvtBZyuduD8.htm)|Spellbreaking (Conjuration)|Brisesort : invocation (rune)|libre|
|[equipment-13-hDO0vRS8r9K2Zw87.htm](equipment/equipment-13-hDO0vRS8r9K2Zw87.htm)|Necklace of Fireballs V|Collier à boules de feu V|officielle|
|[equipment-13-hEIqSfSNBWwEkldc.htm](equipment/equipment-13-hEIqSfSNBWwEkldc.htm)|Broken Tusk Pendant (Greater)|Pendentif des Défenses brisées supérieur|libre|
|[equipment-13-hGreHAX3WuNwhaHU.htm](equipment/equipment-13-hGreHAX3WuNwhaHU.htm)|Wizard's Tower|Tour de magicien|libre|
|[equipment-13-keFeNqSR7W35aCeT.htm](equipment/equipment-13-keFeNqSR7W35aCeT.htm)|Blightburn Ward|Protection contre la maladie du champibrûle|libre|
|[equipment-13-kETKToR9pbO6fAsT.htm](equipment/equipment-13-kETKToR9pbO6fAsT.htm)|Spellbreaking (Transmutation)|Brisesort : transmutation (rune)|libre|
|[equipment-13-khzffn47ZUG0tHhl.htm](equipment/equipment-13-khzffn47ZUG0tHhl.htm)|Eternal Eruption of Droskar's Crag|Éternelle éruption de la Faille de Droskar|libre|
|[equipment-13-lWb2bzRja54wNXOH.htm](equipment/equipment-13-lWb2bzRja54wNXOH.htm)|Spellbreaking (Abjuration)|Brisesort : abjuration (rune)|libre|
|[equipment-13-mp20ggzn4790lalQ.htm](equipment/equipment-13-mp20ggzn4790lalQ.htm)|Helm of Underwater Action (Greater)|Casque d'action sous-marine supérieur|libre|
|[equipment-13-oSltacqbeotmzLNJ.htm](equipment/equipment-13-oSltacqbeotmzLNJ.htm)|Ring of the Ram (Greater)|Anneau du bélier supérieur|officielle|
|[equipment-13-p6tS5mzfGkQJLUDp.htm](equipment/equipment-13-p6tS5mzfGkQJLUDp.htm)|Rime Crystal (Major)|Cristal gelé majeur|libre|
|[equipment-13-PJZje7wucRqjuBNc.htm](equipment/equipment-13-PJZje7wucRqjuBNc.htm)|Spellbreaking (Illusion)|Brisesort : illusion (rune)|libre|
|[equipment-13-QNaCujl2faKRyLD1.htm](equipment/equipment-13-QNaCujl2faKRyLD1.htm)|Shockwave|Onde de choc (rune)|libre|
|[equipment-13-sFIKeflh6Jaac9FI.htm](equipment/equipment-13-sFIKeflh6Jaac9FI.htm)|Wand of Thundering Echoes (5th-Level Spell)|Baguette des échos tonitruants (rang 5)|libre|
|[equipment-13-sRpA9sGZe1P0EERB.htm](equipment/equipment-13-sRpA9sGZe1P0EERB.htm)|Homeward Wayfinder|Guide du retour|libre|
|[equipment-13-tDEi3zLVpxwA74qz.htm](equipment/equipment-13-tDEi3zLVpxwA74qz.htm)|Wand of Continuation (5th-Rank Spell)|Baguette de prolongation (sort de rang 5)|libre|
|[equipment-13-titA5C0yWJqO7coB.htm](equipment/equipment-13-titA5C0yWJqO7coB.htm)|Spellbreaking (Divination)|Brisesort : divination (rune)|libre|
|[equipment-13-TO36RKFoiPWJtuTA.htm](equipment/equipment-13-TO36RKFoiPWJtuTA.htm)|Wyrm on the Wing (Greater)|Ver sur l'aile supérieur|libre|
|[equipment-13-TXPQfQriNJqMYcfp.htm](equipment/equipment-13-TXPQfQriNJqMYcfp.htm)|Wondrous Figurine (Marble Elephant)|Figurine merveilleuse (Élephant de marbre)|officielle|
|[equipment-13-UASUklwtsoAVeK3O.htm](equipment/equipment-13-UASUklwtsoAVeK3O.htm)|Spellbreaking (Evocation)|Brisesort : évocation (rune)|libre|
|[equipment-13-uQNw11Osk2pwx6d8.htm](equipment/equipment-13-uQNw11Osk2pwx6d8.htm)|Horned Hand Rests (Greater)|Repose mains à cornes supérieur|libre|
|[equipment-13-V1XhLQPWjdQz759K.htm](equipment/equipment-13-V1XhLQPWjdQz759K.htm)|Retrieval Belt (Major)|Ceinture de récupération majeure|libre|
|[equipment-13-VhKukQF3cm04yiUm.htm](equipment/equipment-13-VhKukQF3cm04yiUm.htm)|Bloodburn Censer|Encensoir brûlesang|libre|
|[equipment-13-vK05bQ3gPAJvkjMx.htm](equipment/equipment-13-vK05bQ3gPAJvkjMx.htm)|Zuhra's Gloves|Gants du zhura|libre|
|[equipment-13-VqT8sHEf2gcY6arL.htm](equipment/equipment-13-VqT8sHEf2gcY6arL.htm)|Paired (Major)|Appariée majeure (rune)|libre|
|[equipment-13-vT1LG3uNuCCnZhPq.htm](equipment/equipment-13-vT1LG3uNuCCnZhPq.htm)|Reinforcing Rune (Greater)|Renforcement supérieur (rune)|libre|
|[equipment-13-WHwprq9Xym2DOr2x.htm](equipment/equipment-13-WHwprq9Xym2DOr2x.htm)|Extending (Greater)|Extensible supérieure (rune)|libre|
|[equipment-13-XPkhjxSh6tBWk50f.htm](equipment/equipment-13-XPkhjxSh6tBWk50f.htm)|Horn of the Sun Aurochs|Corne des aurochs du soleil|libre|
|[equipment-13-y6if8e8OB3RUywF8.htm](equipment/equipment-13-y6if8e8OB3RUywF8.htm)|Wand of Shardstorm (5th-Rank Spell)|Baguette de tempêtes d'éclats (rang 5)|libre|
|[equipment-13-yzgMOspvPTLDe6Ln.htm](equipment/equipment-13-yzgMOspvPTLDe6Ln.htm)|Ivory Baton|Bâton d'ivoire|libre|
|[equipment-13-Z0VXKrTDur1rgiPE.htm](equipment/equipment-13-Z0VXKrTDur1rgiPE.htm)|Furnace of Endings (Major)|Fournaise des terminaisons majeure|libre|
|[equipment-13-zEys8FeMMAwTqwgW.htm](equipment/equipment-13-zEys8FeMMAwTqwgW.htm)|Bloodbane (Greater)|Dague vengeresse supérieure|libre|
|[equipment-13-Zo3uztXAa7kEUQmC.htm](equipment/equipment-13-Zo3uztXAa7kEUQmC.htm)|Enigma Mirror (Major)|Miroir énigmatique majeur|libre|
|[equipment-13-ZTdRDRew1B0zTGiU.htm](equipment/equipment-13-ZTdRDRew1B0zTGiU.htm)|Stanching (Major)|Étanchement majeur (rune)|libre|
|[equipment-14-1SdtzFpjKIwuQ7Nh.htm](equipment/equipment-14-1SdtzFpjKIwuQ7Nh.htm)|Wand of Crackling Lightning (6th-Level Spell)|Baguette d'électricité crépitante (rang 6)|libre|
|[equipment-14-3rlu75EjB2SVAuOI.htm](equipment/equipment-14-3rlu75EjB2SVAuOI.htm)|Quenching (Major)|Extinction majeure (rune)|libre|
|[equipment-14-4Lhy6pAOIZdOufu3.htm](equipment/equipment-14-4Lhy6pAOIZdOufu3.htm)|Wand of Teeming Ghosts (6th-Level Spell)|Baguette d'esprits grouillants (rang 6)|libre|
|[equipment-14-4yUa5JicrhnHUggC.htm](equipment/equipment-14-4yUa5JicrhnHUggC.htm)|Undead Compendium|Compendium des morts-vivants|libre|
|[equipment-14-8jPsriZqWY1hAgob.htm](equipment/equipment-14-8jPsriZqWY1hAgob.htm)|Ring of Wizardry (Type IV)|Anneau des arcanes (Type IV)|libre|
|[equipment-14-8mhSUxEvNuXDP8Ki.htm](equipment/equipment-14-8mhSUxEvNuXDP8Ki.htm)|Bands of Force (Greater)|Bandes de force supérieures|libre|
|[equipment-14-dn53uqBi6MXg2gIM.htm](equipment/equipment-14-dn53uqBi6MXg2gIM.htm)|Wand of Legerdemain (6th-level)|Baguette de prestidigitation (rang 6)|libre|
|[equipment-14-fCdcyCkGlmp0c34A.htm](equipment/equipment-14-fCdcyCkGlmp0c34A.htm)|Resilient (Greater)|Résilience supérieure (rune)|libre|
|[equipment-14-fTnjEAHVuM0fGkL4.htm](equipment/equipment-14-fTnjEAHVuM0fGkL4.htm)|Fossil Fragment (Petrified Wood)|Fragment de fossile (Bois pétrifié)|libre|
|[equipment-14-gvMmQh9icawtnmlR.htm](equipment/equipment-14-gvMmQh9icawtnmlR.htm)|Midday Lantern (Greater)|Lanterne de midi supérieure|libre|
|[equipment-14-IA7ySNhJxINR7x2e.htm](equipment/equipment-14-IA7ySNhJxINR7x2e.htm)|Cavern's Heart|Coeur de grotte|libre|
|[equipment-14-IHybXd0JGwhMrOlr.htm](equipment/equipment-14-IHybXd0JGwhMrOlr.htm)|Pactmaster's Grace|Grâce du Maître du pacte|libre|
|[equipment-14-iwC6V10pTd6Sk0ML.htm](equipment/equipment-14-iwC6V10pTd6Sk0ML.htm)|Sealing Chest (Major)|Coffre scellé majeur|libre|
|[equipment-14-JDQ4jqp6O8SurQGe.htm](equipment/equipment-14-JDQ4jqp6O8SurQGe.htm)|Wand of Widening (6th-Rank Spell)|Baguette d'élargissement (sort de rang 6)|libre|
|[equipment-14-JzGuXGSjsfWDMu7P.htm](equipment/equipment-14-JzGuXGSjsfWDMu7P.htm)|Wand of the Ash Puppet|Baguette de la poupée de cendres|libre|
|[equipment-14-KyIYS1RDED6gL33N.htm](equipment/equipment-14-KyIYS1RDED6gL33N.htm)|Memory Palace|Palais de la mémoire|libre|
|[equipment-14-ljZwHU5BMnFafVa3.htm](equipment/equipment-14-ljZwHU5BMnFafVa3.htm)|Wand of Noisome Acid (6th-Level Spell)|Baguette de pestilence acide (rang 6)|libre|
|[equipment-14-m9v1YKUvQuFGVZ10.htm](equipment/equipment-14-m9v1YKUvQuFGVZ10.htm)|Tome of Restorative Cleansing (Greater)|Tome du nettoyage restaurateur supérieur|libre|
|[equipment-14-MjiZtQW7QBLFbq7W.htm](equipment/equipment-14-MjiZtQW7QBLFbq7W.htm)|Wand of Mercy (6th-level)|Baguette de miséricorde (rang 6)|libre|
|[equipment-14-NAQ7HcM826y9fsBH.htm](equipment/equipment-14-NAQ7HcM826y9fsBH.htm)|Exploration Lens (Searching)|Lentille exploratrice de recherche|libre|
|[equipment-14-nvYrpdzZzH50t52q.htm](equipment/equipment-14-nvYrpdzZzH50t52q.htm)|Kraken Figurehead (Wracking)|Figure de proue kraken brisante|libre|
|[equipment-14-nZygpiYsx4AlQKle.htm](equipment/equipment-14-nZygpiYsx4AlQKle.htm)|Everair Mask (Major)|Masque à air continu majeur|libre|
|[equipment-14-OmiMLervOVa8G8zK.htm](equipment/equipment-14-OmiMLervOVa8G8zK.htm)|Twilight Lantern (Greater)|Lanterne crépusculaire supérieure|libre|
|[equipment-14-OMoKdkWjmT59LCJo.htm](equipment/equipment-14-OMoKdkWjmT59LCJo.htm)|Tattletale Orb (Clear Quartz)|Orbe commère en cristal translucide|libre|
|[equipment-14-OzChAzd0UR37bEOR.htm](equipment/equipment-14-OzChAzd0UR37bEOR.htm)|Wand of Dazzling Rays (6th-level)|Baguette des rayons éblouissants (rang 6)|libre|
|[equipment-14-pCr0zPdJoXZW3I6y.htm](equipment/equipment-14-pCr0zPdJoXZW3I6y.htm)|Wand of Reaching (6th-level)|Baguette d'atteinte (rang 6)|libre|
|[equipment-14-pGZqqxYzZpe3iaiS.htm](equipment/equipment-14-pGZqqxYzZpe3iaiS.htm)|Aeon Stone (Olivine Pendeloque)|Pierre d'éternité (pendeloque olivine)|libre|
|[equipment-14-pRgnCoqOntDqZchB.htm](equipment/equipment-14-pRgnCoqOntDqZchB.htm)|Retaliation (Greater)|Représailles supérieures (rune)|libre|
|[equipment-14-QC4TXwjteVhwRSNO.htm](equipment/equipment-14-QC4TXwjteVhwRSNO.htm)|Wand of Pernicious Poison (Purple Worm Sting)|Baguette de poison pernicieux (dard du ver pourpre)|libre|
|[equipment-14-RBlbAZSGAJOBsmjv.htm](equipment/equipment-14-RBlbAZSGAJOBsmjv.htm)|Storm Chair|Fauteuil roulant de tempête|libre|
|[equipment-14-SNxXDIoD0XE9WVQ7.htm](equipment/equipment-14-SNxXDIoD0XE9WVQ7.htm)|Dragon's Breath (6th Level Spell)|Souffle de dragon de sort de rang 6 (rune)|libre|
|[equipment-14-t0pzdb54KD2JQcEa.htm](equipment/equipment-14-t0pzdb54KD2JQcEa.htm)|Alacritous Horseshoes (Greater)|Fers à cheval d'alacrité supérieur|libre|
|[equipment-14-tFhNIL308HpiDFtS.htm](equipment/equipment-14-tFhNIL308HpiDFtS.htm)|Wand of Hybrid Form (6th-level)|Baguette de forme hybride (rang 6)|libre|
|[equipment-14-tI2bx8U4Ibwq25ya.htm](equipment/equipment-14-tI2bx8U4Ibwq25ya.htm)|Wand of Mental Purification (6th-level)|Baguette de purification mentale (rang 6)|libre|
|[equipment-14-U2H9lboISyaYa3bP.htm](equipment/equipment-14-U2H9lboISyaYa3bP.htm)|Faydhaan's Dallah|Dallah de faydhaan|libre|
|[equipment-14-uo6fVL0r0S1vM61n.htm](equipment/equipment-14-uo6fVL0r0S1vM61n.htm)|Wand of Hawthorn (6th-level)|Baguette d'aubépine (rang 6)|libre|
|[equipment-14-ut83Grf73Z8ZTaV1.htm](equipment/equipment-14-ut83Grf73Z8ZTaV1.htm)|Wand of the Snowfields (5th-Level Spell)|Baguette des terres gelées (rang 5)|libre|
|[equipment-14-W6QXdPS9d0eoBAuw.htm](equipment/equipment-14-W6QXdPS9d0eoBAuw.htm)|Wand of Dumbfounding Doom (6th-level)|Baguette de la fatalité abrutissante (rang 6)|libre|
|[equipment-14-wbCw7hNyQZSuy4QL.htm](equipment/equipment-14-wbCw7hNyQZSuy4QL.htm)|Wind-Catcher (Greater)|Attrape-vent supérieur (rune)|libre|
|[equipment-14-WT9Yh0UNBHBlLNsJ.htm](equipment/equipment-14-WT9Yh0UNBHBlLNsJ.htm)|Endless Grimoire (Major)|Grimoire sans fin majeur|libre|
|[equipment-14-z5NBeuQezm3ABup2.htm](equipment/equipment-14-z5NBeuQezm3ABup2.htm)|Wand of Rolling Flames (6th-level)|Baguette des flammes ronflantes (rang 6)|libre|
|[equipment-14-ZTQLe0i6KHrhnSMm.htm](equipment/equipment-14-ZTQLe0i6KHrhnSMm.htm)|Rhyton of the Radiant Ifrit|Rhyton de l'ifrit radieux|libre|
|[equipment-14-zyAzx6fLsPurRFQO.htm](equipment/equipment-14-zyAzx6fLsPurRFQO.htm)|Charm of Resistance (Major)|Charme de résistance aux énergies majeur|libre|
|[equipment-15-0AxBSojJ2XhcqMvJ.htm](equipment/equipment-15-0AxBSojJ2XhcqMvJ.htm)|Sanguine Fang (Major)|Canine sanguine majeure|libre|
|[equipment-15-2TANKQXO6xntX5MT.htm](equipment/equipment-15-2TANKQXO6xntX5MT.htm)|Demon's Knot|Noeud du démon|libre|
|[equipment-15-35rLqxDWgiDIkL8e.htm](equipment/equipment-15-35rLqxDWgiDIkL8e.htm)|Wand of Continuation (6th-Rank Spell)|Baguette de prolongation (sort de rang 6)|libre|
|[equipment-15-3Q8nG1JW4FMMJ4l7.htm](equipment/equipment-15-3Q8nG1JW4FMMJ4l7.htm)|Cape of Illumination (Greater)|Cape d'illumination supérieure|libre|
|[equipment-15-7cOczTK4yQkuK5J9.htm](equipment/equipment-15-7cOczTK4yQkuK5J9.htm)|Aeon Stone (Mottled Ellipsoid)|Pierre d'éternité (ellipsoïde mouchetée)|libre|
|[equipment-15-7YkaMoB3YfYRZ21G.htm](equipment/equipment-15-7YkaMoB3YfYRZ21G.htm)|Unfathomable Stargazer|Observateur d'étoiles insondables|libre|
|[equipment-15-9IckeffVxmxX0cwV.htm](equipment/equipment-15-9IckeffVxmxX0cwV.htm)|Book of Lost Days|Livre des jours perdus|libre|
|[equipment-15-bjjp7rXqikby6NU5.htm](equipment/equipment-15-bjjp7rXqikby6NU5.htm)|Wand of Spiritual Warfare (6th-Level Spell)|Baguette de guerre spirituelle (rang 6)|libre|
|[equipment-15-bTLmJMATrrtq8NuT.htm](equipment/equipment-15-bTLmJMATrrtq8NuT.htm)|Dragonscale Amulet|Amulette en écailles de dragon|officielle|
|[equipment-15-bz052lqiBuGmD790.htm](equipment/equipment-15-bz052lqiBuGmD790.htm)|Armory Bracelet (Major)|Bracelet arsenal majeur|libre|
|[equipment-15-cEXLUGa9qTh8Y6kX.htm](equipment/equipment-15-cEXLUGa9qTh8Y6kX.htm)|Thunderblast Slippers (Greater)|Souliers d'explosion tonitruante supérieurs|libre|
|[equipment-15-CxadMTEjnuXyqmcQ.htm](equipment/equipment-15-CxadMTEjnuXyqmcQ.htm)|Giant-Killing (Greater)|Tueuse de géants supérieure (arme)|libre|
|[equipment-15-CxpixKu1opjv4n9K.htm](equipment/equipment-15-CxpixKu1opjv4n9K.htm)|Fan of Soothing Winds (Greater)|Éventail des vents apaisants supérieur|libre|
|[equipment-15-dgwgVWUvXGurnsQD.htm](equipment/equipment-15-dgwgVWUvXGurnsQD.htm)|Tattletale Orb (Selenite)|Orbe de révélation en sélénite|libre|
|[equipment-15-Eft7NqspkOtGv1Ov.htm](equipment/equipment-15-Eft7NqspkOtGv1Ov.htm)|Volcanic Vigor (Greater)|Vigueur volcanique supérieure|libre|
|[equipment-15-FbboejktKOMCm181.htm](equipment/equipment-15-FbboejktKOMCm181.htm)|Lini's Leafstick|Bâton feuille de Lini|libre|
|[equipment-15-FQnK379odYh4Tors.htm](equipment/equipment-15-FQnK379odYh4Tors.htm)|Folding Boat (Greater)|Bateau pliable supérieur|libre|
|[equipment-15-h4n9PdQrOkCrJ9sY.htm](equipment/equipment-15-h4n9PdQrOkCrJ9sY.htm)|Rooting (Major)|Enracinante majeure (rune)|libre|
|[equipment-15-hebf5k3cd7LO6luX.htm](equipment/equipment-15-hebf5k3cd7LO6luX.htm)|Astral (Greater)|Astrale supérieure (rune)|libre|
|[equipment-15-HGsA5gXtaAA65n9e.htm](equipment/equipment-15-HGsA5gXtaAA65n9e.htm)|Decaying (Greater)|Décomposante supérieure (rune)|libre|
|[equipment-15-iPgSLZ44804wrXQC.htm](equipment/equipment-15-iPgSLZ44804wrXQC.htm)|Darkvision Scope (Greater)|Lunette de visée dans le noir supérieure|libre|
|[equipment-15-l6bEZwKaxCGNgZQc.htm](equipment/equipment-15-l6bEZwKaxCGNgZQc.htm)|Fossil Fragment (Triceratops Frill)|Fragment de fossile (Collerette de tricératops)|libre|
|[equipment-15-Lb7F2BR9X9TF1vjX.htm](equipment/equipment-15-Lb7F2BR9X9TF1vjX.htm)|Thundering (Greater)|Tonnerre supérieur (rune)|libre|
|[equipment-15-LfiGXU03Khb1o6fs.htm](equipment/equipment-15-LfiGXU03Khb1o6fs.htm)|Heedless Spurs|Éperons inconsidérés|libre|
|[equipment-15-MNaLwkkyKx86NKKq.htm](equipment/equipment-15-MNaLwkkyKx86NKKq.htm)|Whistle of Calling|Sifflet d'appel|libre|
|[equipment-15-NLF4Z7jn44Sf3RGS.htm](equipment/equipment-15-NLF4Z7jn44Sf3RGS.htm)|Necklace of Strangulation|Collier étrangleur|officielle|
|[equipment-15-nSG5bwiZBEjj53Ya.htm](equipment/equipment-15-nSG5bwiZBEjj53Ya.htm)|Arctic Vigor (Greater)|Vigueur arctique supérieure|libre|
|[equipment-15-Oj3TuNNUT3ayL3Ea.htm](equipment/equipment-15-Oj3TuNNUT3ayL3Ea.htm)|Energy-Absorbing (Greater)|Absorption d'énergie supérieure (rune)|libre|
|[equipment-15-oL8G6OqITPJ5Fd6A.htm](equipment/equipment-15-oL8G6OqITPJ5Fd6A.htm)|Ancestral Echoing|Écho ancestral (rune)|libre|
|[equipment-15-qL1S3vGfv8Dh5yAE.htm](equipment/equipment-15-qL1S3vGfv8Dh5yAE.htm)|Fanged (Major)|Crocs majeurs (rune)|libre|
|[equipment-15-rrWSGLJVxXAMeP07.htm](equipment/equipment-15-rrWSGLJVxXAMeP07.htm)|Countering Charm (Greater)|Charme de contre supérieur|libre|
|[equipment-15-tFgzn2ATau3UJ8Fd.htm](equipment/equipment-15-tFgzn2ATau3UJ8Fd.htm)|Wraithweave Patch (Type III)|Étoffe tressée d'âme-en-peine (Type III)|libre|
|[equipment-15-TieVxNRNuXsU7m7R.htm](equipment/equipment-15-TieVxNRNuXsU7m7R.htm)|Poison Concentrator (Greater)|Concentrateur de poison supérieur|libre|
|[equipment-15-UBWWeT6u5uHD5vT9.htm](equipment/equipment-15-UBWWeT6u5uHD5vT9.htm)|Wand of Thundering Echoes (6th-Level Spell)|Baguette des échos tonitruants (rang 6)|libre|
|[equipment-15-vp2q2h2KoVjRs8nL.htm](equipment/equipment-15-vp2q2h2KoVjRs8nL.htm)|Stampede Medallion (Greater)|Médaillon de la harde supérieur|libre|
|[equipment-15-VtzChvlCG2TQRrgu.htm](equipment/equipment-15-VtzChvlCG2TQRrgu.htm)|Wand of Overflowing Life (6th-Level Spell)|Baguette de vie débordante (rang 6)|libre|
|[equipment-15-X9ZEqWNgHPQO1SCc.htm](equipment/equipment-15-X9ZEqWNgHPQO1SCc.htm)|Eternal Eruption of Ka|Éternelle éruption de Ka|libre|
|[equipment-15-XXz3Gt5IUb8QdnBS.htm](equipment/equipment-15-XXz3Gt5IUb8QdnBS.htm)|Diver's Gloves (Greater)|Gants du plongeur supérieurs|libre|
|[equipment-15-ZZcIhgiKuptXRGyK.htm](equipment/equipment-15-ZZcIhgiKuptXRGyK.htm)|Necklace of Fireballs VI|Collier à boules de feu VI|officielle|
|[equipment-16-0KaC1NryNfckdS7T.htm](equipment/equipment-16-0KaC1NryNfckdS7T.htm)|Wand of Chromatic Burst (7th-level)|Baguette d'explosion chromatique (rang 7)|libre|
|[equipment-16-1neYjXMc4srH7KQ0.htm](equipment/equipment-16-1neYjXMc4srH7KQ0.htm)|Advancing (Greater)|Progression supérieure (rune)|libre|
|[equipment-16-1Nhc0C2gdJvIM7Mv.htm](equipment/equipment-16-1Nhc0C2gdJvIM7Mv.htm)|Reinforcing Rune (Major)|Renforcement majeur (rune)|libre|
|[equipment-16-3Nb9R3lIOy1tiMqv.htm](equipment/equipment-16-3Nb9R3lIOy1tiMqv.htm)|Aeon Stone (Amber Sphere)|Pierre d'éternité (sphère d'ambre)|libre|
|[equipment-16-3xoboDAmMZcmvK59.htm](equipment/equipment-16-3xoboDAmMZcmvK59.htm)|Wand of Hybrid Form (7th-level)|Baguette de forme hybride (rang 7)|libre|
|[equipment-16-62ZOnGqCMlXCHngg.htm](equipment/equipment-16-62ZOnGqCMlXCHngg.htm)|Talisman Cord (Greater)|Cordon de talisman supérieur|libre|
|[equipment-16-68rHNRZmlnyaUbBF.htm](equipment/equipment-16-68rHNRZmlnyaUbBF.htm)|Misleading|Égarante (rune)|libre|
|[equipment-16-9imz3VgBXCg13RfT.htm](equipment/equipment-16-9imz3VgBXCg13RfT.htm)|Slick (Major)|Glissante majeure (rune)|libre|
|[equipment-16-AgDNThyJHtsp1Vjt.htm](equipment/equipment-16-AgDNThyJHtsp1Vjt.htm)|Bloodthirsty|Assoiffée de sang (rune)|libre|
|[equipment-16-AYIel6a1nARjqygh.htm](equipment/equipment-16-AYIel6a1nARjqygh.htm)|Wand of Legerdemain (7th-level)|Baguette de prestidigitation (rang 7)|libre|
|[equipment-16-bgCBFy3yybTYo4Ec.htm](equipment/equipment-16-bgCBFy3yybTYo4Ec.htm)|Dragon's Breath (7th Level Spell)|Souffle de dragon de sort de rang 7 (rune)|libre|
|[equipment-16-ElHAsHIzsVQZp8WW.htm](equipment/equipment-16-ElHAsHIzsVQZp8WW.htm)|Mountain to the Sky|Montagne gratte-ciel|libre|
|[equipment-16-EXXqJp8rU6HR5Ufg.htm](equipment/equipment-16-EXXqJp8rU6HR5Ufg.htm)|Instant Fortress|Forteresse instantanée|officielle|
|[equipment-16-k17YXdpqoVycF57F.htm](equipment/equipment-16-k17YXdpqoVycF57F.htm)|Curious Teardrop|Larme curieuse|libre|
|[equipment-16-K8T5OYXjmniOvhmA.htm](equipment/equipment-16-K8T5OYXjmniOvhmA.htm)|Wand of Mercy (7th-level)|Baguette de miséricorde (rang 7)|libre|
|[equipment-16-kNfdGNIGzF0fW7aq.htm](equipment/equipment-16-kNfdGNIGzF0fW7aq.htm)|Wand of Widening (7th-Rank Spell)|Baguette d'élargissement (sort de rang 7)|libre|
|[equipment-16-kSsjGICDkNRuD7fV.htm](equipment/equipment-16-kSsjGICDkNRuD7fV.htm)|Wand of Rolling Flames (7th-level)|Baguette des flammes ronflantes (rang 7)|libre|
|[equipment-16-lo5QOMA9VAUwUVl7.htm](equipment/equipment-16-lo5QOMA9VAUwUVl7.htm)|Ashen (Greater)|Cendreuse supérieure (rune)|libre|
|[equipment-16-LxaNamrRrGzJo6cL.htm](equipment/equipment-16-LxaNamrRrGzJo6cL.htm)|Pocket Gala|Gala de poche|libre|
|[equipment-16-MO8J2IcBhiTnm9D8.htm](equipment/equipment-16-MO8J2IcBhiTnm9D8.htm)|Aeon Stone (Amplifying)|Pierre d'éternité amplificatrice|libre|
|[equipment-16-oRQFgqcl8L5ix8h1.htm](equipment/equipment-16-oRQFgqcl8L5ix8h1.htm)|Alluring Scarf (Major)|Écharpe séduisante majeure|libre|
|[equipment-16-qeLAYEwUXNbri5eB.htm](equipment/equipment-16-qeLAYEwUXNbri5eB.htm)|Wand of Reaching (7th-level)|Baguette d'atteinte (rang 7)|libre|
|[equipment-16-r8i96DqaLYfWDW1T.htm](equipment/equipment-16-r8i96DqaLYfWDW1T.htm)|Aeon Stone (Peering)|Pierre d'éternité révélatrice|libre|
|[equipment-16-RFFUd06PQH4csvpr.htm](equipment/equipment-16-RFFUd06PQH4csvpr.htm)|Tattletale Orb (Moonstone)|Orbe de révélation en pierre de lune|libre|
|[equipment-16-RRFyASbHcdclympe.htm](equipment/equipment-16-RRFyASbHcdclympe.htm)|Swallow-Spike (Major)|Pique avalée majeure (rune)|libre|
|[equipment-16-rvItjGQaYT7Luwtm.htm](equipment/equipment-16-rvItjGQaYT7Luwtm.htm)|Weapon Potency (+3)|Puissance d'arme +3 (rune)|libre|
|[equipment-16-SkBZG0AO2wpuKBLT.htm](equipment/equipment-16-SkBZG0AO2wpuKBLT.htm)|Wand of Dumbfounding Doom (7th-level)|Baguette de la fatalité abrutissante (rang 7)|libre|
|[equipment-16-sLnkcAGiRQkwQrla.htm](equipment/equipment-16-sLnkcAGiRQkwQrla.htm)|Wand of Teeming Ghosts (7th-Level Spell)|Baguette d'esprits grouillants (rang 7)|libre|
|[equipment-16-son3QT3YfrBYlWPq.htm](equipment/equipment-16-son3QT3YfrBYlWPq.htm)|Headbands of Translocation|Bandeau de translocation|libre|
|[equipment-16-TGCzwDIrAqo3HUhr.htm](equipment/equipment-16-TGCzwDIrAqo3HUhr.htm)|Guiding Chisel|Ciseau d'orientation|officielle|
|[equipment-16-TPQjZzF9yioMr9Bu.htm](equipment/equipment-16-TPQjZzF9yioMr9Bu.htm)|Waffle Iron (High-grade Mithral)|Gaufrier en aubargent de haute qualité|libre|
|[equipment-16-vfVpgUuieLdPoqY7.htm](equipment/equipment-16-vfVpgUuieLdPoqY7.htm)|Crown of the Fire Eater (Major)|Couronne du mangeur de feu majeure|libre|
|[equipment-16-vkYWqXrHdUAggJIg.htm](equipment/equipment-16-vkYWqXrHdUAggJIg.htm)|Highhelm Drill Mark III|Foreuse de Heaume - Mark III|libre|
|[equipment-16-vVJy2xHuzOgm7e6Y.htm](equipment/equipment-16-vVJy2xHuzOgm7e6Y.htm)|Wand of Mental Purification (7th-level)|Baguette de purification mentale (rang 7)|libre|
|[equipment-16-xswNe3hrxy0f01QQ.htm](equipment/equipment-16-xswNe3hrxy0f01QQ.htm)|Radiant Prism|Prisme irradiant|libre|
|[equipment-16-YPBMrIvJOZ6zSZVm.htm](equipment/equipment-16-YPBMrIvJOZ6zSZVm.htm)|Wand of Dazzling Rays (7th-level)|Baguette des rayons éblouissants (rang 7)|libre|
|[equipment-16-yqAVU3Y9w4O19e24.htm](equipment/equipment-16-yqAVU3Y9w4O19e24.htm)|Faith Tattoo (True)|Tatouage du croyant véritable|libre|
|[equipment-16-YR8IAV94fPo0kfBz.htm](equipment/equipment-16-YR8IAV94fPo0kfBz.htm)|Wand of Smoldering Fireballs (7th-Level Spell)|Baguette de boules de feu incendiaires (rang 7)|libre|
|[equipment-16-ZjcRzYBjtPtb01MB.htm](equipment/equipment-16-ZjcRzYBjtPtb01MB.htm)|Wand of Paralytic Shock (7th-level)|Baguette de choc paralysant (rang 7)|libre|
|[equipment-17-0zBuCoOeNkIwi8bj.htm](equipment/equipment-17-0zBuCoOeNkIwi8bj.htm)|Tradecraft Tattoo (Greater)|Tatouage professionnel supérieur|libre|
|[equipment-17-1FKDq4Gfev5GObDT.htm](equipment/equipment-17-1FKDq4Gfev5GObDT.htm)|Crown of Intellect|Couronne d'intellect|libre|
|[equipment-17-3BTIKYHck3JIAPiH.htm](equipment/equipment-17-3BTIKYHck3JIAPiH.htm)|Insistent Door Knocker (Major)|Heurtoir insistant majeur|libre|
|[equipment-17-5zkoqnp6X5yPCXVy.htm](equipment/equipment-17-5zkoqnp6X5yPCXVy.htm)|Manacles (Superior)|Menottes supérieures|officielle|
|[equipment-17-755XG3gP2MWYBXy5.htm](equipment/equipment-17-755XG3gP2MWYBXy5.htm)|Avalanche Boots|Bottes d'avalanche|libre|
|[equipment-17-7utuH8VJjKEzKtNw.htm](equipment/equipment-17-7utuH8VJjKEzKtNw.htm)|Belt of Giant Strength|Ceinture de force de géant|officielle|
|[equipment-17-7uxALss5g34y49HF.htm](equipment/equipment-17-7uxALss5g34y49HF.htm)|Magnifying Scope (Major)|Lunette de visée grossissante majeure|libre|
|[equipment-17-8xWA0demi2yQsl3C.htm](equipment/equipment-17-8xWA0demi2yQsl3C.htm)|Lock (Superior)|Serrure supérieure|officielle|
|[equipment-17-9IwSktj0Xj7A2Ruh.htm](equipment/equipment-17-9IwSktj0Xj7A2Ruh.htm)|Anklets of Alacrity|Chaînes de chevilles d'alacrité|officielle|
|[equipment-17-A9po9KInIcQXMzyp.htm](equipment/equipment-17-A9po9KInIcQXMzyp.htm)|Wyrm on the Wing (Major)|Ver sur l'aile majeur|libre|
|[equipment-17-AMOs47C4WiVY5IW3.htm](equipment/equipment-17-AMOs47C4WiVY5IW3.htm)|Necklace of Allure|Collier de séduction|libre|
|[equipment-17-bnmfBLXOBd3ah6GK.htm](equipment/equipment-17-bnmfBLXOBd3ah6GK.htm)|Alchemist Goggles (Major)|Lunettes d'alchimiste majeur|officielle|
|[equipment-17-bQ68sHmEATwgmiyN.htm](equipment/equipment-17-bQ68sHmEATwgmiyN.htm)|Sash of Books|Ceinture des livres|libre|
|[equipment-17-dLYifig01WulSNVF.htm](equipment/equipment-17-dLYifig01WulSNVF.htm)|Stanching (True)|Étanchement ultime (rune)|libre|
|[equipment-17-DrjFwJhp66GfH5Wi.htm](equipment/equipment-17-DrjFwJhp66GfH5Wi.htm)|Entertainer's Cincture (Greater)|Ceinture d'artiste supérieure|libre|
|[equipment-17-etnis6HOeGZEbScD.htm](equipment/equipment-17-etnis6HOeGZEbScD.htm)|Shining Symbol (Major)|Symbole lumineux majeur|libre|
|[equipment-17-Fc8uMYwTDQkeyeOF.htm](equipment/equipment-17-Fc8uMYwTDQkeyeOF.htm)|Cowl of Keys|Cache-nez des clés|libre|
|[equipment-17-ffQwBOmyy2NyatnC.htm](equipment/equipment-17-ffQwBOmyy2NyatnC.htm)|Aeon Stone (Black Disc)|Pierre d'éternité (disque noir)|libre|
|[equipment-17-fhOnKVwftnOwayBp.htm](equipment/equipment-17-fhOnKVwftnOwayBp.htm)|Eternal Eruption of Sakalayo|Éternelle éruption du Sakalayo|libre|
|[equipment-17-FNccUmKsyXKmfe5c.htm](equipment/equipment-17-FNccUmKsyXKmfe5c.htm)|Armbands of Athleticism (Greater)|Brassards d'athlète supérieur|officielle|
|[equipment-17-grRxpX1iE3zOJA1q.htm](equipment/equipment-17-grRxpX1iE3zOJA1q.htm)|Wand of Overflowing Life (7th-Level Spell)|Baguette de vie débordante (rang 7)|libre|
|[equipment-17-GyDASe4CV3q3H6kh.htm](equipment/equipment-17-GyDASe4CV3q3H6kh.htm)|Eye Slash (True)|Entailles à l'oeil ultimes|libre|
|[equipment-17-H1XGrl6Z0bzXN2oi.htm](equipment/equipment-17-H1XGrl6Z0bzXN2oi.htm)|Wand of Continuation (7th-Rank Spell)|Baguette de prolongation (sort de rang 7)|libre|
|[equipment-17-HcjEb07UjWchysx5.htm](equipment/equipment-17-HcjEb07UjWchysx5.htm)|Unmemorable Mantle (Major)|Manteau insignifiant majeur|libre|
|[equipment-17-hrG2w4IfF1QZhSzw.htm](equipment/equipment-17-hrG2w4IfF1QZhSzw.htm)|Phylactery of Faithfulness (Greater)|Phylactère du croyant supérieur|officielle|
|[equipment-17-ifAp8wHKBZltgHG0.htm](equipment/equipment-17-ifAp8wHKBZltgHG0.htm)|Cloak of the Bat (Greater)|Cape de la chauve souris supérieur|libre|
|[equipment-17-IxcUx7trxUdhF9DQ.htm](equipment/equipment-17-IxcUx7trxUdhF9DQ.htm)|Brooch of Inspiration (Major)|Broche d'inspiration majeure|libre|
|[equipment-17-kjFFmqci69k2zMXF.htm](equipment/equipment-17-kjFFmqci69k2zMXF.htm)|Daredevil Boots (Greater)|Bottes de casse-cou supérieur|libre|
|[equipment-17-Lwvg9GECyIPsG8Xa.htm](equipment/equipment-17-Lwvg9GECyIPsG8Xa.htm)|Wildwood Ink (Major)|Encre du bois sauvage majeure|libre|
|[equipment-17-mOY0STwY5hx4UPCN.htm](equipment/equipment-17-mOY0STwY5hx4UPCN.htm)|Necklace of Fireballs VII|Collier à boules de feu VII|officielle|
|[equipment-17-Nd6h6E5HfsIXu0Rn.htm](equipment/equipment-17-Nd6h6E5HfsIXu0Rn.htm)|Gate Attenuator (Major)|Atténuateur de portail majeur|libre|
|[equipment-17-NFFgDfLBkCjkj0dc.htm](equipment/equipment-17-NFFgDfLBkCjkj0dc.htm)|Cloak of Repute (Major)|Cape de renom majeur|libre|
|[equipment-17-NQGVqxIWaa2fize3.htm](equipment/equipment-17-NQGVqxIWaa2fize3.htm)|Boots of Free Running (Greater)|Bottes de course libre supérieures|libre|
|[equipment-17-oPrV7aMAlBypaAMD.htm](equipment/equipment-17-oPrV7aMAlBypaAMD.htm)|Fossil Fragment (Tyrannosaur Tooth)|Fragment de fossile (Dent de tyrannosaure)|libre|
|[equipment-17-pLAggVa6iDN4cpsz.htm](equipment/equipment-17-pLAggVa6iDN4cpsz.htm)|Stone of Unrivaled Skill|Pierre de compétence sans égal|libre|
|[equipment-17-Q8cPvvyxbFYedlc8.htm](equipment/equipment-17-Q8cPvvyxbFYedlc8.htm)|Scapular of Shields|Scapulaire de boucliers|libre|
|[equipment-17-ri9QkRCD6cAbQ6t3.htm](equipment/equipment-17-ri9QkRCD6cAbQ6t3.htm)|Impactful (Greater)|Impactante supérieure (rune)|libre|
|[equipment-17-skQIdQ0RKXGCHJGM.htm](equipment/equipment-17-skQIdQ0RKXGCHJGM.htm)|Tattletale Orb (Peridot)|Orbe de révélation en péridot|libre|
|[equipment-17-tIta1GNQK6sSWnr6.htm](equipment/equipment-17-tIta1GNQK6sSWnr6.htm)|Bracers of Hammers|Bracelets des marteaux|libre|
|[equipment-17-ToSrWLocAD4AffSB.htm](equipment/equipment-17-ToSrWLocAD4AffSB.htm)|Queasy Lantern (Greater)|Lanterne barbouilleuse supérieure|libre|
|[equipment-17-UlvlWbC7iipuENa1.htm](equipment/equipment-17-UlvlWbC7iipuENa1.htm)|Codebreaker's Parchment (Major)|Parchemin de codage majeur|libre|
|[equipment-17-urneQVi0N613KnwP.htm](equipment/equipment-17-urneQVi0N613KnwP.htm)|Coronet of Stars|Couronne d'Étoiles|libre|
|[equipment-17-VcZ5VVYMvnQQXzX9.htm](equipment/equipment-17-VcZ5VVYMvnQQXzX9.htm)|Wand of Thundering Echoes (7th-Level Spell)|Baguette des échos tonitruants (rang 7)|libre|
|[equipment-17-WeX7rAO2kAyP0QnG.htm](equipment/equipment-17-WeX7rAO2kAyP0QnG.htm)|Wand of Shardstorm (7th-Rank Spell)|Baguette de tempêtes d'éclats (rang 7)|libre|
|[equipment-17-XbQrf5aYEWweje30.htm](equipment/equipment-17-XbQrf5aYEWweje30.htm)|Poisonous Cloak Type IV|Cape empoisonnée (type IV)|officielle|
|[equipment-17-XXALNl2JjJket1vr.htm](equipment/equipment-17-XXALNl2JjJket1vr.htm)|Handcuffs (Superior)|Menottes à cliquet supérieures|libre|
|[equipment-17-YBmrRRh0jJTOZoUe.htm](equipment/equipment-17-YBmrRRh0jJTOZoUe.htm)|Magnetite Scope (Greater)|Lunette de visée en magnétite supérieure|libre|
|[equipment-17-ZdAZ1ALZ28QOeCN7.htm](equipment/equipment-17-ZdAZ1ALZ28QOeCN7.htm)|Accolade Robe (Greater)|Robe de distinction supérieure|libre|
|[equipment-18-0Bp6uVAX4AvjhsOI.htm](equipment/equipment-18-0Bp6uVAX4AvjhsOI.htm)|Twilight Lantern (Major)|Lanterne crépusculaire majeure|libre|
|[equipment-18-1U382qwyTktH9h3j.htm](equipment/equipment-18-1U382qwyTktH9h3j.htm)|Wand of Wearing Dance|Baguette de danse épuisante|libre|
|[equipment-18-20nQTcGzpUv8jJ6R.htm](equipment/equipment-18-20nQTcGzpUv8jJ6R.htm)|Wand of Widening (8th-Rank Spell)|Baguette d'élargissement (sort de rang 8)|libre|
|[equipment-18-2LO9eMNpQiku5UDn.htm](equipment/equipment-18-2LO9eMNpQiku5UDn.htm)|Taljjae's Mask (The Hero)|Masque de Taljjae (le héros)|libre|
|[equipment-18-34D6lFZ2gpZiyUU6.htm](equipment/equipment-18-34D6lFZ2gpZiyUU6.htm)|Wand of Legerdemain (8th-level)|Baguette de prestidigitation (rang 8)|libre|
|[equipment-18-39wTBxfaHkAHwXhw.htm](equipment/equipment-18-39wTBxfaHkAHwXhw.htm)|Countering Charm (Major)|Charme de contre majeur|libre|
|[equipment-18-3DGLuHRME4e2XgvX.htm](equipment/equipment-18-3DGLuHRME4e2XgvX.htm)|Wand of Mercy (8th-level)|Baguette de miséricorde (rang 8)|libre|
|[equipment-18-4jzASWzF2riszO3U.htm](equipment/equipment-18-4jzASWzF2riszO3U.htm)|Taljjae's Mask (The Nobleman)|Masque de Taljjae (le noble)|libre|
|[equipment-18-4v850xbHgUdlvamn.htm](equipment/equipment-18-4v850xbHgUdlvamn.htm)|Crown of Witchcraft (Greater)|Couronne de sorcier supérieur|libre|
|[equipment-18-6PW3zAn8fWW3IYA0.htm](equipment/equipment-18-6PW3zAn8fWW3IYA0.htm)|Dread (Greater)|Effroi supérieur (rune)|libre|
|[equipment-18-8QValKGj7oZKnqw1.htm](equipment/equipment-18-8QValKGj7oZKnqw1.htm)|Wand of Dazzling Rays (8th-level)|Baguette des rayons éblouissants (rang 8)|libre|
|[equipment-18-aR4rXlJnDg61SUBL.htm](equipment/equipment-18-aR4rXlJnDg61SUBL.htm)|Stampede Medallion (Major)|Médaillon de la harde majeur|libre|
|[equipment-18-ASroFtAWLwn6HJJH.htm](equipment/equipment-18-ASroFtAWLwn6HJJH.htm)|Wand of Teeming Ghosts (8th-Level Spell)|Baguette d'esprits grouillants (rang 8)|libre|
|[equipment-18-atWlXr78lWJSNbws.htm](equipment/equipment-18-atWlXr78lWJSNbws.htm)|Mantle of the Grogrisant|Manteau de Grogrisant|libre|
|[equipment-18-C7FeT7eVuwRnLhJy.htm](equipment/equipment-18-C7FeT7eVuwRnLhJy.htm)|Symbol of Conflict (Major)|Symbole du conflit majeur|libre|
|[equipment-18-dFG2yWRg5yNCfugI.htm](equipment/equipment-18-dFG2yWRg5yNCfugI.htm)|Mercurial Mantle|Cape de mercuriel|libre|
|[equipment-18-dPg0hrkIaCdMKQoK.htm](equipment/equipment-18-dPg0hrkIaCdMKQoK.htm)|Taljjae's Mask (The Beast)|Masque de Taljjae (la bête)|libre|
|[equipment-18-eFGpWmM8ehW9mkI4.htm](equipment/equipment-18-eFGpWmM8ehW9mkI4.htm)|Wand of Reaching (8th-level)|Baguette d'atteinte (rang 8)|libre|
|[equipment-18-erP57YMboay9Ns4L.htm](equipment/equipment-18-erP57YMboay9Ns4L.htm)|Titan's Grasp|Poigne de titan|libre|
|[equipment-18-He5idiYCqLJJU83h.htm](equipment/equipment-18-He5idiYCqLJJU83h.htm)|Dragon's Breath (8th Level Spell)|Souffle de dragon de sort de rang 8 (rune)|libre|
|[equipment-18-hGZNrPMdxsabNFLx.htm](equipment/equipment-18-hGZNrPMdxsabNFLx.htm)|Quenching (True)|Extinction ultime (rune)|libre|
|[equipment-18-hL6z9KnndTO2L12f.htm](equipment/equipment-18-hL6z9KnndTO2L12f.htm)|Mask of Allure|Masque d'allure|libre|
|[equipment-18-IdI7rfCk7WjGbJHN.htm](equipment/equipment-18-IdI7rfCk7WjGbJHN.htm)|Living Mantle (Greater)|Manteau vivant supérieur|libre|
|[equipment-18-JSbBiTCtFctuJFhS.htm](equipment/equipment-18-JSbBiTCtFctuJFhS.htm)|Taljjae's Mask (The General)|Masque de Taljjae (le général)|libre|
|[equipment-18-JvIqB42cTweMtxMO.htm](equipment/equipment-18-JvIqB42cTweMtxMO.htm)|Tornado Trompo|Toupie du vent|libre|
|[equipment-18-KY9gDuSeB1tgT3cp.htm](equipment/equipment-18-KY9gDuSeB1tgT3cp.htm)|Wand of Mental Purification (8th-level)|Baguette de purification mentale (rang 8)|libre|
|[equipment-18-MnKL30xS3FHlwdS1.htm](equipment/equipment-18-MnKL30xS3FHlwdS1.htm)|Alchemical Chart (Greater)|Table alchimique supérieure|libre|
|[equipment-18-ocEK7WNH9t3ZPIfd.htm](equipment/equipment-18-ocEK7WNH9t3ZPIfd.htm)|Fossil Fragment (Brontosaurus Phalange)|Fragment de fossile (Phalange de brontosaure)|libre|
|[equipment-18-oEfcbqK68enZLOoR.htm](equipment/equipment-18-oEfcbqK68enZLOoR.htm)|Grub Gloves (Greater)|Gants à larve supérieurs|libre|
|[equipment-18-oJBJW19ulUBMEPEY.htm](equipment/equipment-18-oJBJW19ulUBMEPEY.htm)|Endless Grimoire (True)|Grimoire sans fin ultime|libre|
|[equipment-18-POKglmR5IdpEEefR.htm](equipment/equipment-18-POKglmR5IdpEEefR.htm)|Radiant Prism (Greater)|Prisme irradiant supérieur|libre|
|[equipment-18-q2kE0mEUAEL3gQv0.htm](equipment/equipment-18-q2kE0mEUAEL3gQv0.htm)|Wand of the Snowfields (7th-Level Spell)|Baguette des terres gelées (rang 7)|libre|
|[equipment-18-rlDIbl6EQYXQpWVs.htm](equipment/equipment-18-rlDIbl6EQYXQpWVs.htm)|Maestro's Instrument (Greater)|Instrument de maestro supérieur|libre|
|[equipment-18-rXXNw6dwVn96giDi.htm](equipment/equipment-18-rXXNw6dwVn96giDi.htm)|Obsidian Goggles (Major)|Lunettes d'obsidienne majeures|libre|
|[equipment-18-rZKiSwhCT5xkBVLy.htm](equipment/equipment-18-rZKiSwhCT5xkBVLy.htm)|Wand of Hawthorn (8th-level)|Baguette d'aubépine (rang 8)|libre|
|[equipment-18-S9IVIHLrjnRu1x3o.htm](equipment/equipment-18-S9IVIHLrjnRu1x3o.htm)|Wand of Hybrid Form (8th-level)|Baguette de forme hybride (rang 8)|libre|
|[equipment-18-SmMfd6rYUJ8vDGzM.htm](equipment/equipment-18-SmMfd6rYUJ8vDGzM.htm)|Wand of Rolling Flames (8th-level)|Baguette des flammes ronflantes (rang 8)|libre|
|[equipment-18-T1XSnU0bwrn3m520.htm](equipment/equipment-18-T1XSnU0bwrn3m520.htm)|Wand of Noisome Acid (8th-Level Spell)|Baguette de pestilence acide (rang 8)|libre|
|[equipment-18-U28jkj5ZDl2drtEH.htm](equipment/equipment-18-U28jkj5ZDl2drtEH.htm)|Wand of Crackling Lightning (8th-Level Spell)|Baguette d'électricité crépitante (rang 8)|libre|
|[equipment-18-UC7YCJmKbuau54fS.htm](equipment/equipment-18-UC7YCJmKbuau54fS.htm)|Wand of Dumbfounding Doom (8th-level)|Baguette de la fatalité abrutissante (rang 8)|libre|
|[equipment-18-VBCk7JXGsuG0cug1.htm](equipment/equipment-18-VBCk7JXGsuG0cug1.htm)|Thurible of Revelation (Greater)|Encensoir de révélation supérieur|libre|
|[equipment-18-W0tqTUmRE5lEhWUl.htm](equipment/equipment-18-W0tqTUmRE5lEhWUl.htm)|Midday Lantern (Major)|Lanterne de midi majeur|libre|
|[equipment-18-wOBIx55awoePSEIz.htm](equipment/equipment-18-wOBIx55awoePSEIz.htm)|Words of Wisdom (Greater)|Paroles de Sagesse supérieures|libre|
|[equipment-18-wYpWQCD2IYqDtqpc.htm](equipment/equipment-18-wYpWQCD2IYqDtqpc.htm)|Marvelous Medicines (Greater)|Remèdes merveilleux supérieur|libre|
|[equipment-18-x0FUkGzr6vBZk8m4.htm](equipment/equipment-18-x0FUkGzr6vBZk8m4.htm)|Nosoi Charm (Greater)|Charme nosoi supérieur|libre|
|[equipment-19-0iPTcAgtbtMj6Lwn.htm](equipment/equipment-19-0iPTcAgtbtMj6Lwn.htm)|Reinforcing Rune (Supreme)|Renforcement suprême (rune)|libre|
|[equipment-19-4j5tJ27qfBN7Xd6d.htm](equipment/equipment-19-4j5tJ27qfBN7Xd6d.htm)|Tattletale Orb (Obsidian)|Orbe de révélation en obsidienne|libre|
|[equipment-19-5sMAAIymln2yIl4q.htm](equipment/equipment-19-5sMAAIymln2yIl4q.htm)|Aeon Stone (Lavender and Green Ellipsoid)|Pierre d'éternité (ellipsoïde vert et lavande)|libre|
|[equipment-19-7MTjAlCVVLsNFo7w.htm](equipment/equipment-19-7MTjAlCVVLsNFo7w.htm)|Third Eye|Troisième oeil|libre|
|[equipment-19-adqiLRzIEHiG356b.htm](equipment/equipment-19-adqiLRzIEHiG356b.htm)|Stone Circle (Greater)|Cercle de pierre supérieur|libre|
|[equipment-19-aJ3mYuV0rjFBPOsg.htm](equipment/equipment-19-aJ3mYuV0rjFBPOsg.htm)|Mantle of Amazing Health|Manteau de santé incroyable|libre|
|[equipment-19-aXqCFjLSSjC3a1Mq.htm](equipment/equipment-19-aXqCFjLSSjC3a1Mq.htm)|Rooting (True)|Enracinante ultime (rune)|libre|
|[equipment-19-dR6d7wAWP9KtpWLA.htm](equipment/equipment-19-dR6d7wAWP9KtpWLA.htm)|Wand of Spiritual Warfare (8th-Level Spell)|Baguette de guerre spirituelle (rang 8)|libre|
|[equipment-19-KMqHzKfpPq5H8GOo.htm](equipment/equipment-19-KMqHzKfpPq5H8GOo.htm)|Wand of Continuation (8th-Rank Spell)|Baguette de prolongation (sort de rang 8)|libre|
|[equipment-19-OGKI8NS8Er3qumJS.htm](equipment/equipment-19-OGKI8NS8Er3qumJS.htm)|Berserker's Cloak (Greater)|Cape du berseker supérieur|officielle|
|[equipment-19-ogvooWFhMgB50iXE.htm](equipment/equipment-19-ogvooWFhMgB50iXE.htm)|Umbraex Eye|Oeil umbraex|libre|
|[equipment-19-orChmPy2wJ6DWwHU.htm](equipment/equipment-19-orChmPy2wJ6DWwHU.htm)|Wyrm Claw (Major)|Griffe de ver majeure|libre|
|[equipment-19-qnj1999r0BXo4C31.htm](equipment/equipment-19-qnj1999r0BXo4C31.htm)|Cube of Recall|Cube de rappel|libre|
|[equipment-19-tOYitD9BanOPovcD.htm](equipment/equipment-19-tOYitD9BanOPovcD.htm)|Poison Concentrator (Major)|Concentrateur de poison majeur|libre|
|[equipment-19-U1GAAGhbzX9Lynq0.htm](equipment/equipment-19-U1GAAGhbzX9Lynq0.htm)|Aeon Stone (Pale Orange Rhomboid)|Pierre d'éternité (rhombe orange pâle)|libre|
|[equipment-19-woxl2FrrgAcJDu0t.htm](equipment/equipment-19-woxl2FrrgAcJDu0t.htm)|Striking (Major)|Frappe majeure (rune)|officielle|
|[equipment-19-XpS6X7QKtMQCVkdY.htm](equipment/equipment-19-XpS6X7QKtMQCVkdY.htm)|Retaliation (Major)|Représailles majeures (rune)|libre|
|[equipment-19-xZFbFeJckiQS7smT.htm](equipment/equipment-19-xZFbFeJckiQS7smT.htm)|Wand of Overflowing Life (8th-Level Spell)|Baguette de vie débordante (rang 8)|libre|
|[equipment-19-ZxsPHkzzn6QwfPEz.htm](equipment/equipment-19-ZxsPHkzzn6QwfPEz.htm)|Eternal Eruption of Barrowsiege|Éternelle éruption de Barrowsiege|libre|
|[equipment-20-1wrRaLsT6nuz8gj6.htm](equipment/equipment-20-1wrRaLsT6nuz8gj6.htm)|Dullahan Codex|Codex dullahan|libre|
|[equipment-20-28euk1w2rSWTcj5Y.htm](equipment/equipment-20-28euk1w2rSWTcj5Y.htm)|The Demon's Lantern|La Lanterne du démon|libre|
|[equipment-20-2HByGOXi57zbP5VP.htm](equipment/equipment-20-2HByGOXi57zbP5VP.htm)|The Juggler|Le Jongleur|libre|
|[equipment-20-3twiNkpDG862fnnY.htm](equipment/equipment-20-3twiNkpDG862fnnY.htm)|The Midwife|La Sage-femme|libre|
|[equipment-20-4BDf1LVyQyNyOwka.htm](equipment/equipment-20-4BDf1LVyQyNyOwka.htm)|Beguiling Crown|Couronne séduisante|libre|
|[equipment-20-5Q5MfHX43Sjm8C6Z.htm](equipment/equipment-20-5Q5MfHX43Sjm8C6Z.htm)|Final Blade|Lame finale|officielle|
|[equipment-20-8htAYiRxpAmVn9uK.htm](equipment/equipment-20-8htAYiRxpAmVn9uK.htm)|Ring Of Recalcitrant Wishes|Anneau des souhaits récalcitrants|libre|
|[equipment-20-8wjCvABdXIYQJ7bI.htm](equipment/equipment-20-8wjCvABdXIYQJ7bI.htm)|The Wanderer|Le Vagabond|libre|
|[equipment-20-9JfZz5TpX0Kt6XdE.htm](equipment/equipment-20-9JfZz5TpX0Kt6XdE.htm)|The Winged Serpent|Le Serpent ailé|libre|
|[equipment-20-9o8DDJMWoaZiC5N2.htm](equipment/equipment-20-9o8DDJMWoaZiC5N2.htm)|Drum of Upheaval|Tambour de soulèvement|libre|
|[equipment-20-9RxwktkuKHF9ud2L.htm](equipment/equipment-20-9RxwktkuKHF9ud2L.htm)|The Unicorn|La Licorne|libre|
|[equipment-20-a2yYccCg6ZWvJkdo.htm](equipment/equipment-20-a2yYccCg6ZWvJkdo.htm)|Soulspark Candle|Cierge lueur d'âme|libre|
|[equipment-20-AVBhZdhq6qCp96Se.htm](equipment/equipment-20-AVBhZdhq6qCp96Se.htm)|Codex of Destruction and Renewal|Codex de la destruction et du renouveau|libre|
|[equipment-20-BdQjSNSItoruWMul.htm](equipment/equipment-20-BdQjSNSItoruWMul.htm)|The Uprising|La révolte|libre|
|[equipment-20-FagkWG1cXRucHcQ6.htm](equipment/equipment-20-FagkWG1cXRucHcQ6.htm)|Wand of Dumbfounding Doom (9th-level)|Baguette de la fatalité abrutissante (rang 9)|libre|
|[equipment-20-FfuB86HyfwQyCgSF.htm](equipment/equipment-20-FfuB86HyfwQyCgSF.htm)|Deck of Harrowed Tales|Paquet de contes du Tourment|libre|
|[equipment-20-FiCTRh8rXOXlKxQB.htm](equipment/equipment-20-FiCTRh8rXOXlKxQB.htm)|The Carnival|Le Carnaval|libre|
|[equipment-20-FposU3NmF0IbrOdz.htm](equipment/equipment-20-FposU3NmF0IbrOdz.htm)|The Brass Dwarf|Le Nain d'airain|libre|
|[equipment-20-fqVYXvfhHIclVnu2.htm](equipment/equipment-20-fqVYXvfhHIclVnu2.htm)|The Marriage|Le Mariage|libre|
|[equipment-20-gv5IRHrmGoSu7Dzv.htm](equipment/equipment-20-gv5IRHrmGoSu7Dzv.htm)|Wand of Dazzling Rays (9th-level)|Baguette des rayons éblouissants (rang 9)|libre|
|[equipment-20-h2aVryWbNP24gC05.htm](equipment/equipment-20-h2aVryWbNP24gC05.htm)|Planar Ribbon|Ruban planaire|libre|
|[equipment-20-HaakvCTFbCnwFHVY.htm](equipment/equipment-20-HaakvCTFbCnwFHVY.htm)|The Vision|La Vision|libre|
|[equipment-20-HuGZspUvJqR09Y8u.htm](equipment/equipment-20-HuGZspUvJqR09Y8u.htm)|Rod of Cancellation|Bâton d'annulation|libre|
|[equipment-20-HXvS7TIFFcioESqA.htm](equipment/equipment-20-HXvS7TIFFcioESqA.htm)|The Waxworks|Les Cires|libre|
|[equipment-20-imBDGoQJnvfWYuLC.htm](equipment/equipment-20-imBDGoQJnvfWYuLC.htm)|Talisman of the Sphere|Talisman de la sphère|officielle|
|[equipment-20-jwEAO4hCOKrM6i6c.htm](equipment/equipment-20-jwEAO4hCOKrM6i6c.htm)|The Big Sky|Le Grand ciel|libre|
|[equipment-20-JwglAlCKM7FRRUxq.htm](equipment/equipment-20-JwglAlCKM7FRRUxq.htm)|The Twin|Le Jumeau|libre|
|[equipment-20-JX3jtgTsVORseYKz.htm](equipment/equipment-20-JX3jtgTsVORseYKz.htm)|The Avalanche|L'Avalanche|libre|
|[equipment-20-kd4iBsB3pLkc0Lfc.htm](equipment/equipment-20-kd4iBsB3pLkc0Lfc.htm)|The Fool|Le Fou|libre|
|[equipment-20-LSf18TMRtHV6SMs9.htm](equipment/equipment-20-LSf18TMRtHV6SMs9.htm)|Wand of Mercy (9th-level)|Baguette de miséricorde (rang 9)|libre|
|[equipment-20-MBMiRXM0xC4aS6Xj.htm](equipment/equipment-20-MBMiRXM0xC4aS6Xj.htm)|Wand of Rolling Flames (9th-level)|Baguette des flammes ronflantes (rang 9)|libre|
|[equipment-20-mFweSeoHRIiAjcqz.htm](equipment/equipment-20-mFweSeoHRIiAjcqz.htm)|The Lost|L'Égaré|libre|
|[equipment-20-O1VdFSf81VZQg428.htm](equipment/equipment-20-O1VdFSf81VZQg428.htm)|The Trumpet|La Trompette|libre|
|[equipment-20-owR8clECtufA8q9h.htm](equipment/equipment-20-owR8clECtufA8q9h.htm)|The Empty Throne|Le Trône vide|libre|
|[equipment-20-qoNaajuoAnKRrFyb.htm](equipment/equipment-20-qoNaajuoAnKRrFyb.htm)|Wand of Legerdemain (9th-level)|Baguette de prestidigitation (rang 9)|libre|
|[equipment-20-qPpfc0srQodOiEga.htm](equipment/equipment-20-qPpfc0srQodOiEga.htm)|The Eclipse|L'Éclipse|libre|
|[equipment-20-QuGCW9Q4n56VORV6.htm](equipment/equipment-20-QuGCW9Q4n56VORV6.htm)|The Trader|Le Marchand|libre|
|[equipment-20-RCtvSgzBCsyaztOj.htm](equipment/equipment-20-RCtvSgzBCsyaztOj.htm)|The Snakebite|La Morsure du serpent|libre|
|[equipment-20-rqJzQawe3CbXiWnG.htm](equipment/equipment-20-rqJzQawe3CbXiWnG.htm)|Bands of Force (Major)|Bandes de force majeures|libre|
|[equipment-20-rzcqiPZHpv0I1Jab.htm](equipment/equipment-20-rzcqiPZHpv0I1Jab.htm)|The Dance|La Danse|libre|
|[equipment-20-sa9UGUMWYiZkTPjA.htm](equipment/equipment-20-sa9UGUMWYiZkTPjA.htm)|Wand of Reaching (9th-level)|Baguette d'atteinte (rang 9)|libre|
|[equipment-20-t5978mZ6CqfUDCP6.htm](equipment/equipment-20-t5978mZ6CqfUDCP6.htm)|Wand of Widening (9th-Rank Spell)|Baguette d'élargissement (sort de rang 9)|libre|
|[equipment-20-tBwMPimZ6A93XpHf.htm](equipment/equipment-20-tBwMPimZ6A93XpHf.htm)|Wand of Smoldering Fireballs (9th-Level Spell)|Baguette de boules de feu incendiaires (rang 9)|libre|
|[equipment-20-tV0vr2oVVl9fWyD9.htm](equipment/equipment-20-tV0vr2oVVl9fWyD9.htm)|Radiant Prism (Major)|Prisme irradiant majeur|libre|
|[equipment-20-uU4VC8OlhDHslT4i.htm](equipment/equipment-20-uU4VC8OlhDHslT4i.htm)|Impossible|Impossible (rune)|libre|
|[equipment-20-Vmyrv2H5cs4H64H1.htm](equipment/equipment-20-Vmyrv2H5cs4H64H1.htm)|The Teamster|L'Équipier|libre|
|[equipment-20-WKcvvaZ0LxwYreb7.htm](equipment/equipment-20-WKcvvaZ0LxwYreb7.htm)|Resilient (Major)|Résilience majeure (rune)|libre|
|[equipment-20-wUBJ6osUKvNnOtft.htm](equipment/equipment-20-wUBJ6osUKvNnOtft.htm)|Golden Rod Memento|Memento du sceptre doré|libre|
|[equipment-20-XDiBtW7A9lelBGeY.htm](equipment/equipment-20-XDiBtW7A9lelBGeY.htm)|The Rabbit Prince|Le Prince lapin|libre|
|[equipment-20-xdNAAIVFQsAEiXmL.htm](equipment/equipment-20-xdNAAIVFQsAEiXmL.htm)|The Desert|Le Désert|libre|
|[equipment-20-XshcAhe0ZbhpqIq0.htm](equipment/equipment-20-XshcAhe0ZbhpqIq0.htm)|The Survivor|Le Survivant|libre|
|[equipment-20-XyoYrGEAhJ3iCahe.htm](equipment/equipment-20-XyoYrGEAhJ3iCahe.htm)|The Publican|L'Aubergiste|libre|
|[equipment-20-Y12bRscT79LCpz4k.htm](equipment/equipment-20-Y12bRscT79LCpz4k.htm)|Gauntlight|Gauntlight|libre|
|[equipment-20-y1vxwZhnvjBz9HRn.htm](equipment/equipment-20-y1vxwZhnvjBz9HRn.htm)|The Crows|Les Corbeaux|libre|
|[equipment-20-Y7xHJEyX5Mm3gpq3.htm](equipment/equipment-20-Y7xHJEyX5Mm3gpq3.htm)|Wand of Teeming Ghosts (9th-Level Spell)|Baguette d'esprits grouillants (rang 9)|libre|
|[equipment-20-YBWZ1Qexhe6PuiuO.htm](equipment/equipment-20-YBWZ1Qexhe6PuiuO.htm)|Wand of Mental Purification (9th-level)|Baguette de purification mentale (rang 9)|libre|
|[equipment-20-ZcLscdldjI8xJnC7.htm](equipment/equipment-20-ZcLscdldjI8xJnC7.htm)|The Paladin|Le Paladin|libre|
|[equipment-21-JM5qwlv2bi7Zgm7n.htm](equipment/equipment-21-JM5qwlv2bi7Zgm7n.htm)|Jahan Waystone|Borne jahan|libre|
|[equipment-22-Gyi4IVrAVJRPJF2s.htm](equipment/equipment-22-Gyi4IVrAVJRPJF2s.htm)|Deck of Many Things|Paquet de cartes merveilleuses|libre|
|[equipment-22-jqaSdm5zp8STSW27.htm](equipment/equipment-22-jqaSdm5zp8STSW27.htm)|Shadewither Key|Clé flétrissante|libre|
|[equipment-22-kOanU8MQt6KFmRy0.htm](equipment/equipment-22-kOanU8MQt6KFmRy0.htm)|Shadowed Scale, the Jungle Secret|Écaille ombragée, le secret de la jungle|libre|
|[equipment-25-1ZOTwnqA9ccfdrey.htm](equipment/equipment-25-1ZOTwnqA9ccfdrey.htm)|Aroden's Hearthstone|Cardioline d'Aroden|libre|
|[equipment-25-9Ptn7yy2QeM8taU8.htm](equipment/equipment-25-9Ptn7yy2QeM8taU8.htm)|Flawed Orb of Gold Dragonkind|L'orbe imparfait du dragon d'or|libre|
|[equipment-25-YeSWRltYzLYBUJbN.htm](equipment/equipment-25-YeSWRltYzLYBUJbN.htm)|Passage Pane|Glace de passage|libre|
|[equipment-27-B4DnQNcGl6nFVKHl.htm](equipment/equipment-27-B4DnQNcGl6nFVKHl.htm)|Sphere of Annihilation|Sphère d'annihilation|officielle|
|[equipment-28-kwo4VKC9Qkplusxs.htm](equipment/equipment-28-kwo4VKC9Qkplusxs.htm)|Essence Prism|Prisme des essences|officielle|
|[kit-2req0jGaxz8hScdB.htm](equipment/kit-2req0jGaxz8hScdB.htm)|Adventurer's Pack|Panoplie d'aventurier|libre|
|[kit-5WT7gdzHNb5BG1J0.htm](equipment/kit-5WT7gdzHNb5BG1J0.htm)|Class Kit (Witch)|Panoplie de classe (Sorcier)|libre|
|[kit-9RxYSGp86HKtK8R8.htm](equipment/kit-9RxYSGp86HKtK8R8.htm)|Class Kit (Swashbuckler)|Panoplie de classe (Bretteur)|libre|
|[kit-Ac0BSiDJhtMmPnge.htm](equipment/kit-Ac0BSiDJhtMmPnge.htm)|Class Kit (Oracle)|Panoplie de classe (Oracle)|libre|
|[kit-adNgRqqmkDDKhMRT.htm](equipment/kit-adNgRqqmkDDKhMRT.htm)|Class Kit (Barbarian)|Panoplie de classe (Barbare)|officielle|
|[kit-asQAvJXm3DSzPQtA.htm](equipment/kit-asQAvJXm3DSzPQtA.htm)|Class Kit (Bard)|Panoplie de classe (Barde)|officielle|
|[kit-bJKgKrIQ5puuifcz.htm](equipment/kit-bJKgKrIQ5puuifcz.htm)|Class Kit (Sorcerer)|Panoplie de classe (Ensorceleur)|officielle|
|[kit-cozhl69heIF0vjUt.htm](equipment/kit-cozhl69heIF0vjUt.htm)|Class Kit (Fighter)|Panoplie de classe (Guerrier)|officielle|
|[kit-drY7DJ9rMQEP0et8.htm](equipment/kit-drY7DJ9rMQEP0et8.htm)|Class Kit (Monk)|Panoplie de Classe (Moine)|officielle|
|[kit-gB4kCTgR3b6SJw7n.htm](equipment/kit-gB4kCTgR3b6SJw7n.htm)|Class Kit (Champion)|Panoplie de classe (Champion)|officielle|
|[kit-gCvGa77UZjhsbuOJ.htm](equipment/kit-gCvGa77UZjhsbuOJ.htm)|Class Kit (Druid)|Panoplie de classe (Druide)|officielle|
|[kit-iKnFPR1X8mOAeVCV.htm](equipment/kit-iKnFPR1X8mOAeVCV.htm)|Class Kit (Cleric)|Panoplie de classe (Prêtre)|officielle|
|[kit-KVXcrw446rzYRdpy.htm](equipment/kit-KVXcrw446rzYRdpy.htm)|Class Kit (Alchemist)|Panoplie de classe (Alchimiste)|officielle|
|[kit-nPGrNslTdc6VBuCB.htm](equipment/kit-nPGrNslTdc6VBuCB.htm)|Class Kit (Rogue)|Panoplie de classe (Roublard)|officielle|
|[kit-tHWnYpDZlscldOX0.htm](equipment/kit-tHWnYpDZlscldOX0.htm)|Class Kit (Wizard)|Panoplie de classe (Magicien)|officielle|
|[kit-YQLWR9cCXQY5xaaG.htm](equipment/kit-YQLWR9cCXQY5xaaG.htm)|Cartographer's Kit|Kit du cartographe|libre|
|[kit-zcNBG8cMnILqXMFd.htm](equipment/kit-zcNBG8cMnILqXMFd.htm)|Class Kit (Investigator)|Panoplie de classe (Enquêteur)|libre|
|[shield-00-2RrKSkj27NDREOKQ.htm](equipment/shield-00-2RrKSkj27NDREOKQ.htm)|Gauntlet Buckler|Targe gantée|libre|
|[shield-00-3RUL5Oief3hdyUNU.htm](equipment/shield-00-3RUL5Oief3hdyUNU.htm)|Harnessed Shield|Bouclier harnaché|libre|
|[shield-00-7GEV9zi2bz1iSdaW.htm](equipment/shield-00-7GEV9zi2bz1iSdaW.htm)|Heavy Rondache|Rondache lourde|libre|
|[shield-00-9WcvCnh6SbIw5Cfw.htm](equipment/shield-00-9WcvCnh6SbIw5Cfw.htm)|Caster's Targe|Targe d'incantateur|libre|
|[shield-00-hcoBRMF0zIA986Of.htm](equipment/shield-00-hcoBRMF0zIA986Of.htm)|Razor Disc|Disque rasoir|libre|
|[shield-00-kZYyXUX8dVXnAIVp.htm](equipment/shield-00-kZYyXUX8dVXnAIVp.htm)|Salvo Shield|Bouclier de salves|libre|
|[shield-00-L3qye4mR9I0fU1M3.htm](equipment/shield-00-L3qye4mR9I0fU1M3.htm)|Hide Shield|Bouclier de peau|libre|
|[shield-00-lbAZmrRnV7OZvuol.htm](equipment/shield-00-lbAZmrRnV7OZvuol.htm)|Swordstealer Shield|Bouclier voleur d'épée|libre|
|[shield-00-Li0kDAfCuw7DAiSM.htm](equipment/shield-00-Li0kDAfCuw7DAiSM.htm)|Klar|Klar|libre|
|[shield-00-lV1BKms3SxTvsn3N.htm](equipment/shield-00-lV1BKms3SxTvsn3N.htm)|Dart Shield|Bouclier à fléchettes|libre|
|[shield-00-XnD9zqlwRFFK1ltc.htm](equipment/shield-00-XnD9zqlwRFFK1ltc.htm)|Meteor Shield|Bouclier météore|libre|
|[shield-01-tugYMolpupNz05cE.htm](equipment/shield-01-tugYMolpupNz05cE.htm)|Fortress Shield|Bouclier forteresse|libre|
|[shield-02-AXewacUHP6rs980k.htm](equipment/shield-02-AXewacUHP6rs980k.htm)|Mycoweave Shield (Lesser)|Bouclier tissélium inférieur|libre|
|[shield-03-w0SXrrYSH1EL088n.htm](equipment/shield-03-w0SXrrYSH1EL088n.htm)|Sapling Shield (Minor)|Bouclier baliveau mineur|libre|
|[shield-04-7jolv0cuttvjI1JD.htm](equipment/shield-04-7jolv0cuttvjI1JD.htm)|Wovenwood Shield (Minor)|Bouclier de bois tissé mineur|libre|
|[shield-04-f9ygr5Cjrmop8LWV.htm](equipment/shield-04-f9ygr5Cjrmop8LWV.htm)|Sturdy Shield (Minor)|Bouclier renforcé mineur|officielle|
|[shield-05-BPZPA9Y8kPewVYoW.htm](equipment/shield-05-BPZPA9Y8kPewVYoW.htm)|Magnetic Shield|Bouclier magnétique|libre|
|[shield-06-dMaWOT9su0Aw5NCc.htm](equipment/shield-06-dMaWOT9su0Aw5NCc.htm)|Broadleaf Shield|Bouclier largefeuille|libre|
|[shield-06-eMDt5vCQztp7cC6B.htm](equipment/shield-06-eMDt5vCQztp7cC6B.htm)|Lion's Shield|Bouclier du lion|libre|
|[shield-06-nS4ev4BZn9OUPYgt.htm](equipment/shield-06-nS4ev4BZn9OUPYgt.htm)|Sapling Shield (Lesser)|Bouclier baliveau inférieur|libre|
|[shield-06-OqDAx4HJ39ojVtvg.htm](equipment/shield-06-OqDAx4HJ39ojVtvg.htm)|Spellguard Shield|Bouclier Gardesort|officielle|
|[shield-07-HWQEGY2X0QXj2zfN.htm](equipment/shield-07-HWQEGY2X0QXj2zfN.htm)|Martyr's Shield|Bouclier du martyr|officielle|
|[shield-07-LZAwOTKk3dKwsGDd.htm](equipment/shield-07-LZAwOTKk3dKwsGDd.htm)|Wovenwood Shield (Lesser)|Bouclier de bois tissé inférieur|libre|
|[shield-07-nDZX25OwoN0Imrq6.htm](equipment/shield-07-nDZX25OwoN0Imrq6.htm)|Sturdy Shield (Lesser)|Bouclier renforcé inférieur|libre|
|[shield-07-smKlMa76B6vBPNqG.htm](equipment/shield-07-smKlMa76B6vBPNqG.htm)|Limestone Shield|Pavoir calcaire|libre|
|[shield-07-WDh4fb9N86mNLfDV.htm](equipment/shield-07-WDh4fb9N86mNLfDV.htm)|Spined Shield|Bouclier de la manticore|officielle|
|[shield-08-i8UuRf5yLztt43TN.htm](equipment/shield-08-i8UuRf5yLztt43TN.htm)|Mycoweave Shield (Greater)|Bouclier tissélium supérieur|libre|
|[shield-09-3yeRZNUF4qZzCzHp.htm](equipment/shield-09-3yeRZNUF4qZzCzHp.htm)|Sapling Shield (Moderate)|Bouclier baliveau modéré|libre|
|[shield-09-6bC6RAZddiv8Io6w.htm](equipment/shield-09-6bC6RAZddiv8Io6w.htm)|Broadleaf Shield (Greater)|Bouclier largefeuille supérieur|libre|
|[shield-09-DIzZr0K20eCbNzQo.htm](equipment/shield-09-DIzZr0K20eCbNzQo.htm)|Force Shield|Bouclier de force|libre|
|[shield-09-fQcPx1CmWwPpnkyI.htm](equipment/shield-09-fQcPx1CmWwPpnkyI.htm)|Highhelm War Shield (Lesser)|Bouclier de guerre de Heaume inférieur|libre|
|[shield-09-kFe3JyiO27YBboJy.htm](equipment/shield-09-kFe3JyiO27YBboJy.htm)|Sanguine Klar|Klar sanguin|libre|
|[shield-09-TxQvgyLziolSVfqY.htm](equipment/shield-09-TxQvgyLziolSVfqY.htm)|Warding Escutcheon|Huis protecteur inférieur|libre|
|[shield-09-uVmE0wsLw9tMv7hB.htm](equipment/shield-09-uVmE0wsLw9tMv7hB.htm)|Turnabout Shield|Bouclier volte-face|libre|
|[shield-10-nRLMg1NB2OSmzSqX.htm](equipment/shield-10-nRLMg1NB2OSmzSqX.htm)|Wovenwood Shield (Moderate)|Bouclier de bois tissé modéré|libre|
|[shield-10-pNQJ9PTOEHxEZCgp.htm](equipment/shield-10-pNQJ9PTOEHxEZCgp.htm)|Sturdy Shield (Moderate)|Bouclier renforcé modéré|libre|
|[shield-11-9ZGkQo739t9utj37.htm](equipment/shield-11-9ZGkQo739t9utj37.htm)|Arrow-Catching Shield|Bouclier intercepteur de projectiles|officielle|
|[shield-11-jiC2ufyGZmmxiEAm.htm](equipment/shield-11-jiC2ufyGZmmxiEAm.htm)|Kizidhar's Shield|Bouclier de Kizidhar|libre|
|[shield-11-kHLvz8icxAW9EAIj.htm](equipment/shield-11-kHLvz8icxAW9EAIj.htm)|Lodestone Shield|Bouclier aimanté|libre|
|[shield-11-S5CMsB7AyYC8iSa0.htm](equipment/shield-11-S5CMsB7AyYC8iSa0.htm)|Clockwork Shield|Bouclier mécanique inférieur|libre|
|[shield-12-QJNyQlmPNzIrnuLf.htm](equipment/shield-12-QJNyQlmPNzIrnuLf.htm)|Jawbreaker Shield|Bouclier Brise-mâchoire|libre|
|[shield-12-QR7rqHMuU72NA47A.htm](equipment/shield-12-QR7rqHMuU72NA47A.htm)|Sapling Shield (Greater)|Bouclier baliveau supérieur|libre|
|[shield-13-Fr75q2wqO9AFG53z.htm](equipment/shield-13-Fr75q2wqO9AFG53z.htm)|Wovenwood Shield (Greater)|Bouclier de bois tissé supérieur|libre|
|[shield-13-rrnWORxT2Ch4pUFb.htm](equipment/shield-13-rrnWORxT2Ch4pUFb.htm)|Sturdy Shield (Greater)|Bouclier renforcé supérieur|libre|
|[shield-13-yPeNtJVMJmJupcM2.htm](equipment/shield-13-yPeNtJVMJmJupcM2.htm)|Broadleaf Shield (Major)|Bouclier largefeuille majeur|libre|
|[shield-14-Lwso6G0uaVawoRMK.htm](equipment/shield-14-Lwso6G0uaVawoRMK.htm)|Highhelm War Shield (Moderate)|Bouclier de guerre de Heaume modéré|libre|
|[shield-14-vkPNIDJPt2RHpS6T.htm](equipment/shield-14-vkPNIDJPt2RHpS6T.htm)|Sanguine Klar (Greater)|Klar sanguin supérieur|libre|
|[shield-15-aPD0z9dBsHqgiCW0.htm](equipment/shield-15-aPD0z9dBsHqgiCW0.htm)|Reforging Shield|Bouclier réparateur|officielle|
|[shield-15-Qm9mmqMNKBaMn5so.htm](equipment/shield-15-Qm9mmqMNKBaMn5so.htm)|Sapling Shield (Major)|Bouclier baliveau majeur|libre|
|[shield-16-BWQzaHbGVqlBuMww.htm](equipment/shield-16-BWQzaHbGVqlBuMww.htm)|Sturdy Shield (Major)|Bouclier renforcé majeur|libre|
|[shield-16-e9Xiei2wduOEGI5r.htm](equipment/shield-16-e9Xiei2wduOEGI5r.htm)|Wovenwood Shield (Major)|Bouclier de bois tissé majeur|libre|
|[shield-16-EKXRigmEZgqmFA62.htm](equipment/shield-16-EKXRigmEZgqmFA62.htm)|Nethysian Bulwark|Rempart néthysien|libre|
|[shield-16-Ruul32xBBYYVwFHC.htm](equipment/shield-16-Ruul32xBBYYVwFHC.htm)|Mycoweave Shield (Major)|Bouclier tissélium majeur|libre|
|[shield-17-wkmn8UQ0nUeVtz5J.htm](equipment/shield-17-wkmn8UQ0nUeVtz5J.htm)|Broadleaf Shield (True)|Bouclier largefeuille ultime|libre|
|[shield-18-ayST5rFSIKy2ynYk.htm](equipment/shield-18-ayST5rFSIKy2ynYk.htm)|Clockwork Shield (Greater)|Bouclier mécanique supérieur|libre|
|[shield-18-VJiKPxewJNLhimEi.htm](equipment/shield-18-VJiKPxewJNLhimEi.htm)|Sapling Shield (True)|Bouclier baliveau ultime|libre|
|[shield-19-7Z8XXGiUiyyisKOD.htm](equipment/shield-19-7Z8XXGiUiyyisKOD.htm)|Sturdy Shield (Supreme)|Bouclier renforcé ultime|libre|
|[shield-19-puPx1rmbhjNM2lcz.htm](equipment/shield-19-puPx1rmbhjNM2lcz.htm)|Starfall Shield|Bouclier chute d'étoiles|libre|
|[shield-19-qXzP7yH72fxAChNU.htm](equipment/shield-19-qXzP7yH72fxAChNU.htm)|Wovenwood Shield (True)|Bouclier en bois tissé ultime|libre|
|[shield-20-K5IWk98rsEMyJ1Xb.htm](equipment/shield-20-K5IWk98rsEMyJ1Xb.htm)|Warding Escutcheon (Greater)|Huis protecteur supérieur|libre|
|[shield-20-ODpis1NRtTWohcqS.htm](equipment/shield-20-ODpis1NRtTWohcqS.htm)|Highhelm War Shield (Greater)|Bouclier de guerre de Heaume supérieur|libre|
|[shield-20-PlYr5AhYwHIztMw2.htm](equipment/shield-20-PlYr5AhYwHIztMw2.htm)|Shield of the Unified Legion|Bouclier de la Légion unifiée|libre|
|[treasure-00-02sF1QlxEx6H6zEP.htm](equipment/treasure-00-02sF1QlxEx6H6zEP.htm)|Hematite|Hématite|libre|
|[treasure-00-0b6BUtoFD4DotZHR.htm](equipment/treasure-00-0b6BUtoFD4DotZHR.htm)|Gilded ceremonial armor|Armure cérémonielle dorée|libre|
|[treasure-00-0zVMeeklYZ7hcgXw.htm](equipment/treasure-00-0zVMeeklYZ7hcgXw.htm)|Emerald, brilliant green|Émeraude, vert brillant|libre|
|[treasure-00-2MPfYdRWfUNnt5JK.htm](equipment/treasure-00-2MPfYdRWfUNnt5JK.htm)|Gold dragon statuette|Statuette de dragon en or|libre|
|[treasure-00-2PXwkWD3ymM7s0Ul.htm](equipment/treasure-00-2PXwkWD3ymM7s0Ul.htm)|Ivory|Ivoire|libre|
|[treasure-00-2Rb9MOLENbZ7yyRL.htm](equipment/treasure-00-2Rb9MOLENbZ7yyRL.htm)|Star sapphire|Saphir étoilé|libre|
|[treasure-00-2T33bWTWXj1v2J03.htm](equipment/treasure-00-2T33bWTWXj1v2J03.htm)|Simple sculpture|Sculpture simple|libre|
|[treasure-00-3l30rcsNpDM7moKu.htm](equipment/treasure-00-3l30rcsNpDM7moKu.htm)|Onyx|Onyx|libre|
|[treasure-00-3UOS4sien8IQbPNV.htm](equipment/treasure-00-3UOS4sien8IQbPNV.htm)|Ruby, large|Rubis, gros|libre|
|[treasure-00-5CqtgygdvqecMoRU.htm](equipment/treasure-00-5CqtgygdvqecMoRU.htm)|Thought Lens of Astral Essence|Lentille de pensée d'essence astrale|libre|
|[treasure-00-5dibOpDYOhJY6Ssn.htm](equipment/treasure-00-5dibOpDYOhJY6Ssn.htm)|Silver mirror with gilded frame|Miroir en argent avec un cadre doré|libre|
|[treasure-00-5Ew82vBF9YfaiY9f.htm](equipment/treasure-00-5Ew82vBF9YfaiY9f.htm)|Silver Pieces|Pièces d'argent|libre|
|[treasure-00-5EYlhm8ZbR6dLUHF.htm](equipment/treasure-00-5EYlhm8ZbR6dLUHF.htm)|Silver statuette of a raven|Statuette de corbeau en argent|libre|
|[treasure-00-5ix9inqdHLksqeoR.htm](equipment/treasure-00-5ix9inqdHLksqeoR.htm)|Gold necklace with peridots|Collier en or serti de péridots|libre|
|[treasure-00-69IWSryF5BWkwWXY.htm](equipment/treasure-00-69IWSryF5BWkwWXY.htm)|Set of six ivory dice|Jeu de dés à six faces en ivoire|libre|
|[treasure-00-6vb56WClb06JrpuJ.htm](equipment/treasure-00-6vb56WClb06JrpuJ.htm)|Shell|Coquillage|libre|
|[treasure-00-6wCA8h0jpMnqPbib.htm](equipment/treasure-00-6wCA8h0jpMnqPbib.htm)|Enormous tapestry of a major battle|Tapisserie immense représentant une bataille majeure|libre|
|[treasure-00-7iVFlnr2ZbBdtOx8.htm](equipment/treasure-00-7iVFlnr2ZbBdtOx8.htm)|Pearl, saltwater|Perle d'eau salée|libre|
|[treasure-00-7OuVXvNRbNoKzDSy.htm](equipment/treasure-00-7OuVXvNRbNoKzDSy.htm)|Diamond, large|Diamant, gros|libre|
|[treasure-00-7SPJO9xr89N8E23s.htm](equipment/treasure-00-7SPJO9xr89N8E23s.htm)|Agate|Agate|officielle|
|[treasure-00-7Uk6LMmzsCxuhhA6.htm](equipment/treasure-00-7Uk6LMmzsCxuhhA6.htm)|Ancient dragon skull etched with mystic sigils|Crâne de dragon vénérable gravé de symboles mystiques|libre|
|[treasure-00-8aPckmJTsyUfBndP.htm](equipment/treasure-00-8aPckmJTsyUfBndP.htm)|Turquoise|Turquoise|libre|
|[treasure-00-8P87jwOAC3zMuzoF.htm](equipment/treasure-00-8P87jwOAC3zMuzoF.htm)|Scrimshaw whale bone|Gravure en os de baleine|libre|
|[treasure-00-8RH0e9UqXIxAPJJG.htm](equipment/treasure-00-8RH0e9UqXIxAPJJG.htm)|Silk mask decorated with citrines|Masque en soie décoré de citrines|libre|
|[treasure-00-9FsyNDq3VOsFumhz.htm](equipment/treasure-00-9FsyNDq3VOsFumhz.htm)|Gold rapier with amethysts|Rapière en or avec des améthystes|libre|
|[treasure-00-Ab8yDhtYDJnFrjI4.htm](equipment/treasure-00-Ab8yDhtYDJnFrjI4.htm)|Colorful velvet half mask|Loup de velours coloré (loup = demi-masque)|libre|
|[treasure-00-aGJJwRpFRMgneROG.htm](equipment/treasure-00-aGJJwRpFRMgneROG.htm)|Quality sculpture by an unknown|Sculpture de qualité (artiste inconnu)|libre|
|[treasure-00-AgsZScsqv3puTmZx.htm](equipment/treasure-00-AgsZScsqv3puTmZx.htm)|Sardonyx|Sardonyx|libre|
|[treasure-00-aqhEATP0vu4kx15I.htm](equipment/treasure-00-aqhEATP0vu4kx15I.htm)|Saint's bone with lost scriptures|Os de saint avec des écritures perdues|libre|
|[treasure-00-B6B7tBWJSqOBz5zz.htm](equipment/treasure-00-B6B7tBWJSqOBz5zz.htm)|Gold Pieces|Pièces d'or|libre|
|[treasure-00-bgswgiXV12HW0aEz.htm](equipment/treasure-00-bgswgiXV12HW0aEz.htm)|Simple painting|Peinture simple|libre|
|[treasure-00-CNLt74pUxM0GGjyl.htm](equipment/treasure-00-CNLt74pUxM0GGjyl.htm)|Simple silver circlet|Bandeau simple en argent|libre|
|[treasure-00-CRTNNgrmssD6bQqQ.htm](equipment/treasure-00-CRTNNgrmssD6bQqQ.htm)|Jet|Jais|libre|
|[treasure-00-cSBupUqjsHJwivbp.htm](equipment/treasure-00-cSBupUqjsHJwivbp.htm)|Etched copper ewer|Aiguière de cuivre gravée|libre|
|[treasure-00-D8gzHTvP0uFxjwyA.htm](equipment/treasure-00-D8gzHTvP0uFxjwyA.htm)|Bloodstone|Héliotrope|libre|
|[treasure-00-dBQQZUXaMhCAFkpY.htm](equipment/treasure-00-dBQQZUXaMhCAFkpY.htm)|Aquamarine|Aigue-marine|libre|
|[treasure-00-DE4880DRtsRqax1N.htm](equipment/treasure-00-DE4880DRtsRqax1N.htm)|Lapis lazuli|Lapis lazuli|libre|
|[treasure-00-DFNmzduKX6NSuPrT.htm](equipment/treasure-00-DFNmzduKX6NSuPrT.htm)|Tiger's‑eye|Oeil de tigre|libre|
|[treasure-00-dloHmPtGDr8zSwJu.htm](equipment/treasure-00-dloHmPtGDr8zSwJu.htm)|Leather flagon with Caydenite symbol|Gourde en cuir avec un symbole de Cayden|libre|
|[treasure-00-Ds8GJEQyRYamRIiB.htm](equipment/treasure-00-Ds8GJEQyRYamRIiB.htm)|Malachite|Malachite|libre|
|[treasure-00-DWzoqVeEamX118Qk.htm](equipment/treasure-00-DWzoqVeEamX118Qk.htm)|Jet and white gold game set|Pièces de jeu en or blanc et en jais|libre|
|[treasure-00-dzsJfE1JIWUMCiK4.htm](equipment/treasure-00-dzsJfE1JIWUMCiK4.htm)|Plain brass censer|Encensoir en laiton massif|libre|
|[treasure-00-e6arpbDxMlmogfee.htm](equipment/treasure-00-e6arpbDxMlmogfee.htm)|Gold and aquamarine diadem|Diadème d'or serti d'aigue-marines|libre|
|[treasure-00-EKgLX0TCyFik3gPS.htm](equipment/treasure-00-EKgLX0TCyFik3gPS.htm)|Coral idol of an elemental lord|Idole en corail d'un seigneur élémentaire|libre|
|[treasure-00-epTuWsVDuPzUXMk9.htm](equipment/treasure-00-epTuWsVDuPzUXMk9.htm)|Amethyst|Amethyste|officielle|
|[treasure-00-eUXQTjezsuJ6EB4u.htm](equipment/treasure-00-eUXQTjezsuJ6EB4u.htm)|Towering sculpture by a master|Sculpture monumentale (par un maître)|libre|
|[treasure-00-F4YgvB74p8kwif1t.htm](equipment/treasure-00-F4YgvB74p8kwif1t.htm)|Ceremonial shortsword with spinels|Épée courte cérémonielle avec des spinelles|libre|
|[treasure-00-FI8PLxBFmHF6o0pH.htm](equipment/treasure-00-FI8PLxBFmHF6o0pH.htm)|Silver flagon inscribed with fields|Chope en argent gravée de champs|libre|
|[treasure-00-fUENXLX3tzi33CcB.htm](equipment/treasure-00-fUENXLX3tzi33CcB.htm)|Silk ceremonial armor|Armure de soie cérémonielle|libre|
|[treasure-00-FW1AEh214U1mMWh7.htm](equipment/treasure-00-FW1AEh214U1mMWh7.htm)|Chrysoprase|Chrysoprase|libre|
|[treasure-00-FwYqacDEAmXk5jB5.htm](equipment/treasure-00-FwYqacDEAmXk5jB5.htm)|Star sapphire necklace|Collier de saphirs étoilés|libre|
|[treasure-00-G5WuYX1ghrZcJ1J1.htm](equipment/treasure-00-G5WuYX1ghrZcJ1J1.htm)|Illustrated book|Livre illustré|libre|
|[treasure-00-GgwlIaNPJVsyRwzz.htm](equipment/treasure-00-GgwlIaNPJVsyRwzz.htm)|Parade armor with flourishes|Armure de parade avec des fioritures|libre|
|[treasure-00-Gi4vx84LhppOQAxr.htm](equipment/treasure-00-Gi4vx84LhppOQAxr.htm)|Silver coronet with peridots|Diadème d'argent avec des péridots|libre|
|[treasure-00-H8BXvZ1DUg3uJ53K.htm](equipment/treasure-00-H8BXvZ1DUg3uJ53K.htm)|Gold urn with scenes of judgment|Urne doré portant des scènes de jugement|libre|
|[treasure-00-hEBNs4mYaVWz3UsX.htm](equipment/treasure-00-hEBNs4mYaVWz3UsX.htm)|Jeweled orrery of the planes|Planétaire des plans composé de gemmes|libre|
|[treasure-00-hhJfZgLhPaPh5V7e.htm](equipment/treasure-00-hhJfZgLhPaPh5V7e.htm)|Pearl, black|Perle noire|libre|
|[treasure-00-HIxtCuH5YXNqit0W.htm](equipment/treasure-00-HIxtCuH5YXNqit0W.htm)|Tankard owned by Cayden Cailean|Chope ayant appartenu à Cayden Cailean|libre|
|[treasure-00-hJc0dOcHJ2nkJcik.htm](equipment/treasure-00-hJc0dOcHJ2nkJcik.htm)|Spinel, deep blue|Spinelle, bleu profond|libre|
|[treasure-00-HMdfNM7ibp1XtcVT.htm](equipment/treasure-00-HMdfNM7ibp1XtcVT.htm)|Platinum dragon statuette|Statuette de dragon en platine|libre|
|[treasure-00-hMIG86aqKcVYJGsg.htm](equipment/treasure-00-hMIG86aqKcVYJGsg.htm)|Emerald|Émeraude|officielle|
|[treasure-00-hnzNKdD5hjQc1NUx.htm](equipment/treasure-00-hnzNKdD5hjQc1NUx.htm)|Alabaster and obsidian game set|Pièces d'un jeu en albâtre et en obsidienne|officielle|
|[treasure-00-I3KlOUruQ2CZdcJ6.htm](equipment/treasure-00-I3KlOUruQ2CZdcJ6.htm)|Bronze bowl with wave imagery|Bol en bronze avec des motifs de vagues|libre|
|[treasure-00-ilnc82SKDYHcoEMH.htm](equipment/treasure-00-ilnc82SKDYHcoEMH.htm)|Opal|Opale|libre|
|[treasure-00-iz7Mv2xGwqyicNiA.htm](equipment/treasure-00-iz7Mv2xGwqyicNiA.htm)|Quality painting by an unknown|Peinture de qualité (auteur inconnu)|libre|
|[treasure-00-j4DU62QY4lJ4WEPQ.htm](equipment/treasure-00-j4DU62QY4lJ4WEPQ.htm)|Jasper|Jaspe|libre|
|[treasure-00-ja5rYjVRwwUXWtOd.htm](equipment/treasure-00-ja5rYjVRwwUXWtOd.htm)|Bronze chalice with bloodstones|Calice en bronze avec des héliotropes|libre|
|[treasure-00-JECXvZ0bKTDr79mo.htm](equipment/treasure-00-JECXvZ0bKTDr79mo.htm)|Porcelain vase inlaid with gold|Vase de porcelaine incrusté d'or|libre|
|[treasure-00-JP7G2gOS3OuMNzeE.htm](equipment/treasure-00-JP7G2gOS3OuMNzeE.htm)|Hand mirror with decorated frame|Miroir à main au cadre décoré|libre|
|[treasure-00-JuNPeK5Qm1w6wpb4.htm](equipment/treasure-00-JuNPeK5Qm1w6wpb4.htm)|Platinum Pieces|Pièce de platine|officielle|
|[treasure-00-JYxnuXg9MV6y1gpQ.htm](equipment/treasure-00-JYxnuXg9MV6y1gpQ.htm)|Amphora with lavish scenes|Amphore portant des scènes somptueuses|officielle|
|[treasure-00-K44zDGRTSecRjEv0.htm](equipment/treasure-00-K44zDGRTSecRjEv0.htm)|Gilded scepter with sapphire|Sceptre doré surmonté d'un saphir|libre|
|[treasure-00-K7KzNh3YsXBrDlyj.htm](equipment/treasure-00-K7KzNh3YsXBrDlyj.htm)|Inscribed crocodile skull|Crâne de crcodile inscrit|libre|
|[treasure-00-l6Eifu4DScg0OBxb.htm](equipment/treasure-00-l6Eifu4DScg0OBxb.htm)|Brass scepter with amethyst head|Sceptre de laiton à pommeau d'améthyste|libre|
|[treasure-00-l8dFZIYYngDOIYHb.htm](equipment/treasure-00-l8dFZIYYngDOIYHb.htm)|Illuminated manuscript|Manuscrit enluminé|libre|
|[treasure-00-L8IzHSz2jYGFpp4N.htm](equipment/treasure-00-L8IzHSz2jYGFpp4N.htm)|Elegant cloth doll|Poupée de chiffon élégante|libre|
|[treasure-00-laXnb43oznxeSLT9.htm](equipment/treasure-00-laXnb43oznxeSLT9.htm)|Previously lost volume from a legendary author|Ouvrage perdu (auteur légandaire)|libre|
|[treasure-00-LEfLVcrbhZPmyUyI.htm](equipment/treasure-00-LEfLVcrbhZPmyUyI.htm)|Famous portrait by a master|Portrait fameux par un maître peintre|libre|
|[treasure-00-LJxNB3Yy2sTJaUbd.htm](equipment/treasure-00-LJxNB3Yy2sTJaUbd.htm)|Splendid lyre of world‑famous lyrist|Lyre splendide (luthier mondialement connu)|libre|
|[treasure-00-lvrvCGrmnMmE4TxU.htm](equipment/treasure-00-lvrvCGrmnMmE4TxU.htm)|Bronze brazier with Asmodean artwork|Braséro en bronze de style asmodéen|libre|
|[treasure-00-LyQQvO7COYAvFRNw.htm](equipment/treasure-00-LyQQvO7COYAvFRNw.htm)|Jade|Jade|officielle|
|[treasure-00-LzHxy9YPEaCueLB9.htm](equipment/treasure-00-LzHxy9YPEaCueLB9.htm)|Platinum image of a fey noble with a bit of orichalcum|Image en platine d'une noble fée avec un morceau d'orichalque|libre|
|[treasure-00-lzJ8AVhRcbFul5fh.htm](equipment/treasure-00-lzJ8AVhRcbFul5fh.htm)|Copper Pieces|Pièces de cuivre|officielle|
|[treasure-00-m26FikfAdZouHzhq.htm](equipment/treasure-00-m26FikfAdZouHzhq.htm)|Tourmaline|Tourmaline|libre|
|[treasure-00-mixyMNYUClKfqURh.htm](equipment/treasure-00-mixyMNYUClKfqURh.htm)|Sard|Sardoine|libre|
|[treasure-00-MK6nnMZNWHmChAXZ.htm](equipment/treasure-00-MK6nnMZNWHmChAXZ.htm)|Chandelier crafted from dreams|Chandelier confectionné à partir de songes|libre|
|[treasure-00-Mm3xgz27Q94wZhti.htm](equipment/treasure-00-Mm3xgz27Q94wZhti.htm)|Engraved copper ring|Anneau de cuivre gravé|libre|
|[treasure-00-Mp6StcWjlKCpD8Jx.htm](equipment/treasure-00-Mp6StcWjlKCpD8Jx.htm)|Moonstone and onyx game set|Pièces de jeu en onyx et orichalque|libre|
|[treasure-00-nBY7ueX18tcSiica.htm](equipment/treasure-00-nBY7ueX18tcSiica.htm)|Platinum‑framed monocle|Monocle à la monture de platine|libre|
|[treasure-00-nIlx1IQhYJfQtpVF.htm](equipment/treasure-00-nIlx1IQhYJfQtpVF.htm)|Alabaster|Albâtre|officielle|
|[treasure-00-NLcnptEjjiY9teq7.htm](equipment/treasure-00-NLcnptEjjiY9teq7.htm)|Intricate silver and gold music box|Boîte à musique chargée d'or et d'argent|libre|
|[treasure-00-noLtrW7wFfRFxSUt.htm](equipment/treasure-00-noLtrW7wFfRFxSUt.htm)|Colorful pastoral tapestry|Tapisserie pastorale colorée|libre|
|[treasure-00-NuX85j3bYLYVcINr.htm](equipment/treasure-00-NuX85j3bYLYVcINr.htm)|Garnet|Grenat|officielle|
|[treasure-00-nzT4kWzAdLMcESWl.htm](equipment/treasure-00-nzT4kWzAdLMcESWl.htm)|Virtuoso silver flute|Flûte de virtuose en argent|libre|
|[treasure-00-oH7dJGkgAB0Y2Mad.htm](equipment/treasure-00-oH7dJGkgAB0Y2Mad.htm)|Obsidian|Obsidienne|libre|
|[treasure-00-ony7u60cfBMtLG8F.htm](equipment/treasure-00-ony7u60cfBMtLG8F.htm)|Gold chalice with black pearls|Calice en or serti de perles noires|libre|
|[treasure-00-Oru7cHMGULLXRLV3.htm](equipment/treasure-00-Oru7cHMGULLXRLV3.htm)|Original manuscript from a world‑famous author|Manuscrit original (auteur mondialement connu)|libre|
|[treasure-00-oyGXxUmS6QcthfRc.htm](equipment/treasure-00-oyGXxUmS6QcthfRc.htm)|Topaz|Topaze|libre|
|[treasure-00-OzxNhgRFEgdTybFl.htm](equipment/treasure-00-OzxNhgRFEgdTybFl.htm)|Major Painting by a Legend|Peinture majeure par un peintre légendaire|libre|
|[treasure-00-oZYu6GMIIoOYW2Nh.htm](equipment/treasure-00-oZYu6GMIIoOYW2Nh.htm)|Peridot|Péridot|libre|
|[treasure-00-P2MIJxEQiIWJmV8n.htm](equipment/treasure-00-P2MIJxEQiIWJmV8n.htm)|Carnelian|Cornaline|libre|
|[treasure-00-p3mg6gK5KhpS7plo.htm](equipment/treasure-00-p3mg6gK5KhpS7plo.htm)|Zircon|Zircon|libre|
|[treasure-00-P7jMpOYmusKse1Yu.htm](equipment/treasure-00-P7jMpOYmusKse1Yu.htm)|Gold and garnet ring|Anneau d'or avec un grenat|libre|
|[treasure-00-PGlNIOpo8yedgTFR.htm](equipment/treasure-00-PGlNIOpo8yedgTFR.htm)|Quartz, milky, rose, or smoky|Quartz rose, laiteux ou fumé|libre|
|[treasure-00-PNnmcBhEwZmAKsq9.htm](equipment/treasure-00-PNnmcBhEwZmAKsq9.htm)|Copper and spinel puzzle box|Boîte de puzzle en cuivre ornée de spinelles|libre|
|[treasure-00-PzWETSKeVKisKCyl.htm](equipment/treasure-00-PzWETSKeVKisKCyl.htm)|Living flame shaped into a phoenix|Flamme vivante ayant la forme d'un phénix|libre|
|[treasure-00-q72jL8PRcjw3DVoF.htm](equipment/treasure-00-q72jL8PRcjw3DVoF.htm)|Phasing ether silk tapestry|Tapisserie de soie d'araignée de phase|libre|
|[treasure-00-qFkhYAeCxYci8jgk.htm](equipment/treasure-00-qFkhYAeCxYci8jgk.htm)|Amber|Ambre|officielle|
|[treasure-00-qgKOCdtdybxwmiEv.htm](equipment/treasure-00-qgKOCdtdybxwmiEv.htm)|Chrysoberyl symbol of an evil eye|Symbole de mauvais oeil en chrysobéryl|libre|
|[treasure-00-qnK4cIHZ2irCV4BL.htm](equipment/treasure-00-qnK4cIHZ2irCV4BL.htm)|Quartz, rock crystal|Quartz, cristal de roche|libre|
|[treasure-00-r7QGx0YFZCR0W5iu.htm](equipment/treasure-00-r7QGx0YFZCR0W5iu.htm)|Alabaster idol|Idôle en albâtre|officielle|
|[treasure-00-rgPjAtKt6J1AZ1Pj.htm](equipment/treasure-00-rgPjAtKt6J1AZ1Pj.htm)|Enormous chryselephantine sculpture by a legend|Sculpture chryséléphantine énorme par un sculpteur légendaire|libre|
|[treasure-00-rIhvmrRVr51VgfKa.htm](equipment/treasure-00-rIhvmrRVr51VgfKa.htm)|Solidified Moment of Time|Moment de temps solidifié|libre|
|[treasure-00-rjHqUOFqUeyTQyin.htm](equipment/treasure-00-rjHqUOFqUeyTQyin.htm)|Lapis lazuli pendant|Pendentif en lapis lazuli|libre|
|[treasure-00-RKX4vLWyczPwfsKS.htm](equipment/treasure-00-RKX4vLWyczPwfsKS.htm)|Set of decorated ceramic plates|Collection d'assiettes en céramique décorées|libre|
|[treasure-00-RLFxTy0TjMkN6rM2.htm](equipment/treasure-00-RLFxTy0TjMkN6rM2.htm)|Pearl, irregular freshwater|Perle d'eau douce irrégulière|libre|
|[treasure-00-RNnVb83lRRUXg8hG.htm](equipment/treasure-00-RNnVb83lRRUXg8hG.htm)|Azurite|Azurite|libre|
|[treasure-00-rOMPvaCeLqNvT6rJ.htm](equipment/treasure-00-rOMPvaCeLqNvT6rJ.htm)|Brass statuette of a bull|Statuette de taureau en laiton|libre|
|[treasure-00-rVfFUBb0r423IPg8.htm](equipment/treasure-00-rVfFUBb0r423IPg8.htm)|Set of decorated porcelain plates|Collection d'assiettes en porcelaine décorées|libre|
|[treasure-00-s6wRFNG5lVEkGGdJ.htm](equipment/treasure-00-s6wRFNG5lVEkGGdJ.htm)|Wide landscape by an expert|Large paysage (par un expert)|libre|
|[treasure-00-SmN0vRjkPaSaAJZt.htm](equipment/treasure-00-SmN0vRjkPaSaAJZt.htm)|Divine art piece created by Shelyn|Oeuvre d'art divine (par Shélyn)|libre|
|[treasure-00-svhZ61CaSgatn5by.htm](equipment/treasure-00-svhZ61CaSgatn5by.htm)|Crystallized dragon heart|Coeur de dragon cristallisé|libre|
|[treasure-00-tcP4ZKGZr5i4586v.htm](equipment/treasure-00-tcP4ZKGZr5i4586v.htm)|Gold mask of a high priest|Masque en or d'un grand prêtre|libre|
|[treasure-00-tMMYXdHps9QakCVn.htm](equipment/treasure-00-tMMYXdHps9QakCVn.htm)|Silver and jade censer|Encensoir d'argent et de jade|libre|
|[treasure-00-TTuRgoLxVR2c6NEx.htm](equipment/treasure-00-TTuRgoLxVR2c6NEx.htm)|Marble altar|Autel en marbre|libre|
|[treasure-00-u7YrZzA2yem780cP.htm](equipment/treasure-00-u7YrZzA2yem780cP.htm)|Coral|Corail|officielle|
|[treasure-00-uAQKNkgub7KNOHoR.htm](equipment/treasure-00-uAQKNkgub7KNOHoR.htm)|Brass anklet|Chaîne de cheville en laiton|libre|
|[treasure-00-ugOCvoAZnVyEuIiN.htm](equipment/treasure-00-ugOCvoAZnVyEuIiN.htm)|Crystal dinner set, fine silverware|Couverts d'argenterie fine et verres en cristal|libre|
|[treasure-00-Ujaoyu3WE6V7Y8Vg.htm](equipment/treasure-00-Ujaoyu3WE6V7Y8Vg.htm)|Rhodochrosite|Rhodochrosite|libre|
|[treasure-00-uMxXncL2bQI04GZ9.htm](equipment/treasure-00-uMxXncL2bQI04GZ9.htm)|Chrysoberyl|Chrysoberyl|libre|
|[treasure-00-VHJM3jYtpzMqZ1wc.htm](equipment/treasure-00-VHJM3jYtpzMqZ1wc.htm)|Ceremonial dagger with onyx hilt|Dague cérémonielle avec un pommeau en onyx|libre|
|[treasure-00-vJLb8wwipdwFQDH3.htm](equipment/treasure-00-vJLb8wwipdwFQDH3.htm)|Silk fan decorated with turquoise|Éventail en soie rehaussé de turquoises|libre|
|[treasure-00-vKdjLDfL75SfFGuL.htm](equipment/treasure-00-vKdjLDfL75SfFGuL.htm)|Moonstone|Pierre de lune|libre|
|[treasure-00-wBoGy4DfKHAvqAko.htm](equipment/treasure-00-wBoGy4DfKHAvqAko.htm)|Spinel, red or green|Spinelle, rouge ou verte|libre|
|[treasure-00-WEnrPya5A8CKi91A.htm](equipment/treasure-00-WEnrPya5A8CKi91A.htm)|Citrine|Citrine|libre|
|[treasure-00-WuW89GD0TG7RYTLF.htm](equipment/treasure-00-WuW89GD0TG7RYTLF.htm)|Ruby, small|Rubis, petit|libre|
|[treasure-00-x46wlAbSDKFEumvg.htm](equipment/treasure-00-x46wlAbSDKFEumvg.htm)|Fine gold spyglass|Belle longue vue en or|libre|
|[treasure-00-x7rEpjFkUNTqmt1t.htm](equipment/treasure-00-x7rEpjFkUNTqmt1t.htm)|Gold and opal bracelet|Bracelet en or avec des opales|libre|
|[treasure-00-xXo7KXnhp1TfvxRc.htm](equipment/treasure-00-xXo7KXnhp1TfvxRc.htm)|Copper statuette of a salamander|Statuette de salamandre en cuivre|libre|
|[treasure-00-xzmYwmvj61br2bRp.htm](equipment/treasure-00-xzmYwmvj61br2bRp.htm)|Carved wooden game set|Pièces de jeu en bois sculpté|libre|
|[treasure-00-YeMi3otmwQ34W2we.htm](equipment/treasure-00-YeMi3otmwQ34W2we.htm)|Iron cauldron with gargoyle faces|Chaudron en fer avec des visages de gargouille|libre|
|[treasure-00-YFokzLQ0HK74Vf6n.htm](equipment/treasure-00-YFokzLQ0HK74Vf6n.htm)|Pyrite|Pyrite|libre|
|[treasure-00-yrVJKmpBS0mqFuO1.htm](equipment/treasure-00-yrVJKmpBS0mqFuO1.htm)|Jeweled gold puzzle box|Boîte de puzzle en or ornée de gemmes|libre|
|[treasure-00-yTYPPB7WrhanBPbu.htm](equipment/treasure-00-yTYPPB7WrhanBPbu.htm)|Sapphire|Saphir|officielle|
|[treasure-00-yv7OfuEaXTm6YCAc.htm](equipment/treasure-00-yv7OfuEaXTm6YCAc.htm)|Small cold iron cauldron with onyx|Petit chaudron en fer froid avec des onyx|libre|
|[treasure-00-YZsZ6stG0i9GBCZw.htm](equipment/treasure-00-YZsZ6stG0i9GBCZw.htm)|Iron and rock crystal brazier|Braséro en fer et en cristal de roche|libre|
|[treasure-00-z3PT7OgX34TQKAwH.htm](equipment/treasure-00-z3PT7OgX34TQKAwH.htm)|Diamond, small|Diamant, petit|libre|
|[treasure-00-z84EQt8kcPf0RXwx.htm](equipment/treasure-00-z84EQt8kcPf0RXwx.htm)|Porcelain doll with amber eyes|Poupée de porcelaine avec des yeux d'ambre|libre|
|[treasure-00-zdiHlKtd4Fy1aIPC.htm](equipment/treasure-00-zdiHlKtd4Fy1aIPC.htm)|Life‑size sculpture by an expert|Sculpture à l'échelle (par un expert)|libre|
|[treasure-00-ZQXCEq8IyGf3WnEj.htm](equipment/treasure-00-ZQXCEq8IyGf3WnEj.htm)|Diamond ring with platinum band|Anneau de platine avec un diamant|libre|
|[treasure-00-zRsMLEqh2t2hqSw4.htm](equipment/treasure-00-zRsMLEqh2t2hqSw4.htm)|Jewel‑encrusted gold altar|Autel en or incrusté de gemmes|libre|
|[weapon-00-0E9ADJkQUVsz7A4G.htm](equipment/weapon-00-0E9ADJkQUVsz7A4G.htm)|Gakgung|Gakgung|libre|
|[weapon-00-0GEXLXh8M5Ce6oYT.htm](equipment/weapon-00-0GEXLXh8M5Ce6oYT.htm)|Chakri (Lost Omens)|Chakri (Prédictions perdues)|libre|
|[weapon-00-0lCXehyFlXdYxDfA.htm](equipment/weapon-00-0lCXehyFlXdYxDfA.htm)|Bladed Diabolo|Diabolo à lames|libre|
|[weapon-00-17MQKaMIHQAJNtuu.htm](equipment/weapon-00-17MQKaMIHQAJNtuu.htm)|Fighting Stick|Canne de combat|libre|
|[weapon-00-1U7Laa7Yt7i3G77L.htm](equipment/weapon-00-1U7Laa7Yt7i3G77L.htm)|Gnome Hooked Hammer|Marteau-piolet gnome|officielle|
|[weapon-00-1Uvz5xJpE5UvOFjJ.htm](equipment/weapon-00-1Uvz5xJpE5UvOFjJ.htm)|Wish Knife|Couteau à souhait|libre|
|[weapon-00-27CO2NIr5yMgA6sa.htm](equipment/weapon-00-27CO2NIr5yMgA6sa.htm)|Leiomano|Leiomano|libre|
|[weapon-00-2P9jItR1sV20OqmD.htm](equipment/weapon-00-2P9jItR1sV20OqmD.htm)|Thorn Whip|Fouet à épines|libre|
|[weapon-00-3NQj5gtHIYFYlAch.htm](equipment/weapon-00-3NQj5gtHIYFYlAch.htm)|Thundermace|Masse-d'arme tonnerre|libre|
|[weapon-00-3Zv5hSXXtlaDatUv.htm](equipment/weapon-00-3Zv5hSXXtlaDatUv.htm)|Greatpick|Grand pic de guerre|officielle|
|[weapon-00-4gWvfeDGTSAf9LAQ.htm](equipment/weapon-00-4gWvfeDGTSAf9LAQ.htm)|Dragon Mouth Pistol|Pistolet Gueule de dragon|libre|
|[weapon-00-4LJEpZ2HkCu9BvHI.htm](equipment/weapon-00-4LJEpZ2HkCu9BvHI.htm)|Hand Cannon|Couleuvrine|libre|
|[weapon-00-4lO2uNA70HqTuSAD.htm](equipment/weapon-00-4lO2uNA70HqTuSAD.htm)|Piercing Wind|Vent perçant|libre|
|[weapon-00-5fu6dCtqhdBnHNqh.htm](equipment/weapon-00-5fu6dCtqhdBnHNqh.htm)|Morningstar|Morgenstern|officielle|
|[weapon-00-5qW06ylMkJ0wToDd.htm](equipment/weapon-00-5qW06ylMkJ0wToDd.htm)|Combat Lure|Leurre de Combat|libre|
|[weapon-00-5v2mhiBbYQDhlsw5.htm](equipment/weapon-00-5v2mhiBbYQDhlsw5.htm)|Dandpatta|Épée patta|libre|
|[weapon-00-62nnVQvGhoVLLl2K.htm](equipment/weapon-00-62nnVQvGhoVLLl2K.htm)|Crossbow|Arbalète|officielle|
|[weapon-00-6I4YJAQUbTAqbpsI.htm](equipment/weapon-00-6I4YJAQUbTAqbpsI.htm)|Pick|Pic de guerre|officielle|
|[weapon-00-6KWYmeRMxsQfWhhJ.htm](equipment/weapon-00-6KWYmeRMxsQfWhhJ.htm)|Bastard Sword|Épée bâtarde|officielle|
|[weapon-00-6qJkA6fnHHiF4CEV.htm](equipment/weapon-00-6qJkA6fnHHiF4CEV.htm)|Tengu Gale Blade|Lame tempête tengu|libre|
|[weapon-00-6v4aedCiIAoSJfiL.htm](equipment/weapon-00-6v4aedCiIAoSJfiL.htm)|Kalis|Kalis|libre|
|[weapon-00-76AqNZB5xuFWxgJI.htm](equipment/weapon-00-76AqNZB5xuFWxgJI.htm)|Fighting Fan|Éventail de combat|libre|
|[weapon-00-79krBL334Kp1RMNB.htm](equipment/weapon-00-79krBL334Kp1RMNB.htm)|Battle Lute|Luth de combat|libre|
|[weapon-00-7mXZyzL5v3K6Buu2.htm](equipment/weapon-00-7mXZyzL5v3K6Buu2.htm)|Battle Saddle|Selle de bataille|libre|
|[weapon-00-7tKkkF8eZ4iCLJtp.htm](equipment/weapon-00-7tKkkF8eZ4iCLJtp.htm)|Shortsword|Épée courte|officielle|
|[weapon-00-7WRzbBTOCsJx5f7L.htm](equipment/weapon-00-7WRzbBTOCsJx5f7L.htm)|Buugeng|Buugeng|libre|
|[weapon-00-80G0z7iFUCjHeYGf.htm](equipment/weapon-00-80G0z7iFUCjHeYGf.htm)|Nightstick|Matraque télescopique|libre|
|[weapon-00-8COlYvHe6hKCXY8x.htm](equipment/weapon-00-8COlYvHe6hKCXY8x.htm)|Greataxe|Grande hache|officielle|
|[weapon-00-8eGV6fXbmt2v74uB.htm](equipment/weapon-00-8eGV6fXbmt2v74uB.htm)|Black Powder Knuckle Dusters|Coup-de-poing à poudre noire|libre|
|[weapon-00-8V4mgecGASsQ7fjl.htm](equipment/weapon-00-8V4mgecGASsQ7fjl.htm)|Adze|Adze/herminette|libre|
|[weapon-00-8XwE8hsWBFoIdoDC.htm](equipment/weapon-00-8XwE8hsWBFoIdoDC.htm)|Karambit|Karambit|libre|
|[weapon-00-9iDqOLNFKxiTcFKE.htm](equipment/weapon-00-9iDqOLNFKxiTcFKE.htm)|Mace|Masse d'armes|officielle|
|[weapon-00-a5DlDpB3RuSIvLIC.htm](equipment/weapon-00-a5DlDpB3RuSIvLIC.htm)|Hongali Hornbow|Arc à cornes Hongali|libre|
|[weapon-00-ADvVuMwIWDZZF9cv.htm](equipment/weapon-00-ADvVuMwIWDZZF9cv.htm)|Kris|Kris|libre|
|[weapon-00-Ak3oHoPDJoMR14yg.htm](equipment/weapon-00-Ak3oHoPDJoMR14yg.htm)|Lion Scythe|Faux du lion|libre|
|[weapon-00-aXuJh4i8HqSu6NYV.htm](equipment/weapon-00-aXuJh4i8HqSu6NYV.htm)|Longspear|Pique|officielle|
|[weapon-00-aYzWoFt5uYY31zEQ.htm](equipment/weapon-00-aYzWoFt5uYY31zEQ.htm)|Dwarven Dorn-Dergar|Dorn-dergar naine|libre|
|[weapon-00-bCPxjuqVusM7Qlpk.htm](equipment/weapon-00-bCPxjuqVusM7Qlpk.htm)|Tonfa|Tonfa|libre|
|[weapon-00-BPQRgrJwr5GuT00g.htm](equipment/weapon-00-BPQRgrJwr5GuT00g.htm)|Dagger Pistol|Pistolet dague|libre|
|[weapon-00-Brs4linGy3GAod5y.htm](equipment/weapon-00-Brs4linGy3GAod5y.htm)|Three-Section Naginata|Naginata en trois parties|libre|
|[weapon-00-BtncTx8EfxTsHqQI.htm](equipment/weapon-00-BtncTx8EfxTsHqQI.htm)|Clan Pistol|Pistolet de clan|libre|
|[weapon-00-bzhJNnkIXdVRbjAA.htm](equipment/weapon-00-bzhJNnkIXdVRbjAA.htm)|Broadspear|Lance large|libre|
|[weapon-00-c58wczIzH2gzeXQL.htm](equipment/weapon-00-c58wczIzH2gzeXQL.htm)|Club|Gourdin|libre|
|[weapon-00-ChTaE7jhvCjcS6jI.htm](equipment/weapon-00-ChTaE7jhvCjcS6jI.htm)|Arquebus|Arquebuse|libre|
|[weapon-00-cKbiv1dUMViikKOS.htm](equipment/weapon-00-cKbiv1dUMViikKOS.htm)|Monkey's Fist|Poing de singe|libre|
|[weapon-00-cSGWhw4Wn6Z98tVK.htm](equipment/weapon-00-cSGWhw4Wn6Z98tVK.htm)|Lancer|Lancier|libre|
|[weapon-00-csXSDzgZASX4RWr4.htm](equipment/weapon-00-csXSDzgZASX4RWr4.htm)|Blunderbuss|Tromblon|libre|
|[weapon-00-CVTgOpNuRE7hsnc1.htm](equipment/weapon-00-CVTgOpNuRE7hsnc1.htm)|Filcher's Fork|Fourchette du chapardeur|officielle|
|[weapon-00-d3Xv6QDWkpeg7VZn.htm](equipment/weapon-00-d3Xv6QDWkpeg7VZn.htm)|Dueling Spear|Lance de duel|libre|
|[weapon-00-D6E4VaKVG05G26Rm.htm](equipment/weapon-00-D6E4VaKVG05G26Rm.htm)|Kusarigama|Kusarigama|libre|
|[weapon-00-DFtbWoL6yBnDbtB3.htm](equipment/weapon-00-DFtbWoL6yBnDbtB3.htm)|Panabas|Panabas|libre|
|[weapon-00-dfum7DpOEkwxwTsT.htm](equipment/weapon-00-dfum7DpOEkwxwTsT.htm)|Shield Boss|Ombon|officielle|
|[weapon-00-dgWxsYm0DWHb27h6.htm](equipment/weapon-00-dgWxsYm0DWHb27h6.htm)|Halberd|Hallebarde|officielle|
|[weapon-00-dgXjUcaoQDAY6ZpO.htm](equipment/weapon-00-dgXjUcaoQDAY6ZpO.htm)|Knuckle Duster|Coup-de-poing|libre|
|[weapon-00-djSCel13i1p8JeXv.htm](equipment/weapon-00-djSCel13i1p8JeXv.htm)|Rope Dart|Corde fléchette|libre|
|[weapon-00-DP03AwxQxCOAxch0.htm](equipment/weapon-00-DP03AwxQxCOAxch0.htm)|Asp Coil|Bobine Aspic|libre|
|[weapon-00-dUC8Fsa6FZtVikS3.htm](equipment/weapon-00-dUC8Fsa6FZtVikS3.htm)|Composite Longbow|Arc long composite|libre|
|[weapon-00-DV4qelKHrviM0O5i.htm](equipment/weapon-00-DV4qelKHrviM0O5i.htm)|Halfling Sling Staff|Fustibale de halfelin|officielle|
|[weapon-00-e4NwsnPnpQKbDZ9F.htm](equipment/weapon-00-e4NwsnPnpQKbDZ9F.htm)|Composite Shortbow|Arc court composite|officielle|
|[weapon-00-EPhsyiO4jYl0jWoC.htm](equipment/weapon-00-EPhsyiO4jYl0jWoC.htm)|Flingflenser|Dépeceur|libre|
|[weapon-00-EtfVzTsCYULkEzFk.htm](equipment/weapon-00-EtfVzTsCYULkEzFk.htm)|Chakram|Chakram|libre|
|[weapon-00-f1gwoTkf3Nn0v3PN.htm](equipment/weapon-00-f1gwoTkf3Nn0v3PN.htm)|Whip|Fouet|officielle|
|[weapon-00-FbquJr6FuXL3K373.htm](equipment/weapon-00-FbquJr6FuXL3K373.htm)|Gill Hook|Harpon à branchies|libre|
|[weapon-00-fcWvG7jAZDLMRBWn.htm](equipment/weapon-00-fcWvG7jAZDLMRBWn.htm)|Earthbreaker|Briseterre|libre|
|[weapon-00-Fg3GkCDkszj5WtgQ.htm](equipment/weapon-00-Fg3GkCDkszj5WtgQ.htm)|Katar|Katar|officielle|
|[weapon-00-FibwLZ12EIEwLGhw.htm](equipment/weapon-00-FibwLZ12EIEwLGhw.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[weapon-00-fjkwYZ0hRmBztwBG.htm](equipment/weapon-00-fjkwYZ0hRmBztwBG.htm)|Shuriken|Shuriken|officielle|
|[weapon-00-FJrsDoaIXksVjld9.htm](equipment/weapon-00-FJrsDoaIXksVjld9.htm)|Trident|Trident|officielle|
|[weapon-00-FPwsiGqMCNPLHmjX.htm](equipment/weapon-00-FPwsiGqMCNPLHmjX.htm)|Blowgun|Sarbacane|officielle|
|[weapon-00-FuS6F91Rhd4m3T6d.htm](equipment/weapon-00-FuS6F91Rhd4m3T6d.htm)|Fauchard|Fauchard|libre|
|[weapon-00-FVjTuBCIefAgloUU.htm](equipment/weapon-00-FVjTuBCIefAgloUU.htm)|Staff|Bâton|officielle|
|[weapon-00-fvvfZxfGV9i3urkd.htm](equipment/weapon-00-fvvfZxfGV9i3urkd.htm)|Claw Blade|Lames griffues|libre|
|[weapon-00-fX63Af9Bf2XVarJu.htm](equipment/weapon-00-fX63Af9Bf2XVarJu.htm)|Dancer's Spear|Lance de danseur|libre|
|[weapon-00-fz8XDaJ7n0zeOH1f.htm](equipment/weapon-00-fz8XDaJ7n0zeOH1f.htm)|Shield Bow|Arc bouclier|libre|
|[weapon-00-fZxI5GkBFfAUUf1z.htm](equipment/weapon-00-fZxI5GkBFfAUUf1z.htm)|Wrecker|Démolisseur|libre|
|[weapon-00-giO4LwIKGzJZoaxa.htm](equipment/weapon-00-giO4LwIKGzJZoaxa.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[weapon-00-GntaZfIfKj5MRyA5.htm](equipment/weapon-00-GntaZfIfKj5MRyA5.htm)|Wheel Blades|Roues à lames|libre|
|[weapon-00-grmaV4GdoGD7sKbn.htm](equipment/weapon-00-grmaV4GdoGD7sKbn.htm)|Scimitar|Cimeterre|officielle|
|[weapon-00-gTTJuNgwTcNmkDx2.htm](equipment/weapon-00-gTTJuNgwTcNmkDx2.htm)|Boarding Axe|Hache d'abordage|libre|
|[weapon-00-GuWKXErLL5R43sIy.htm](equipment/weapon-00-GuWKXErLL5R43sIy.htm)|Talwar|Talwar|libre|
|[weapon-00-H74vYwHJ8XT4qOPI.htm](equipment/weapon-00-H74vYwHJ8XT4qOPI.htm)|Scythe|Faux|officielle|
|[weapon-00-HaquLIE0SFc85vWY.htm](equipment/weapon-00-HaquLIE0SFc85vWY.htm)|Shield Pistol|Pistolet bouclier|libre|
|[weapon-00-Hc4qCwyO9i0Avlzt.htm](equipment/weapon-00-Hc4qCwyO9i0Avlzt.htm)|Long Air Repeater|Fusil long à air comprimé|libre|
|[weapon-00-hIgqLgH3YcLZBeoT.htm](equipment/weapon-00-hIgqLgH3YcLZBeoT.htm)|Shortbow|Arc court|officielle|
|[weapon-00-hMYdSFmMWzidzHih.htm](equipment/weapon-00-hMYdSFmMWzidzHih.htm)|Bo Staff|Bô|officielle|
|[weapon-00-hqMtsTwmOShdAdQW.htm](equipment/weapon-00-hqMtsTwmOShdAdQW.htm)|Flintlock Musket|Mousquet à silex|libre|
|[weapon-00-I5DJUbAFGMwa6qCz.htm](equipment/weapon-00-I5DJUbAFGMwa6qCz.htm)|Scourge|Chat à neuf queues|officielle|
|[weapon-00-iCdvNfzvsTZ0s5vg.htm](equipment/weapon-00-iCdvNfzvsTZ0s5vg.htm)|Bladed Scarf|Écharpe à lame|libre|
|[weapon-00-IEA7M6GGv4rnGLdW.htm](equipment/weapon-00-IEA7M6GGv4rnGLdW.htm)|Bladed Hoop|Cerceau à lames|libre|
|[weapon-00-IF6qUrR3i030v0dH.htm](equipment/weapon-00-IF6qUrR3i030v0dH.htm)|Shears|Cisailles|libre|
|[weapon-00-IoJ0WpfUPWGuaqBG.htm](equipment/weapon-00-IoJ0WpfUPWGuaqBG.htm)|Gada|Masse gada|libre|
|[weapon-00-iWcw1sFLAzByVLBP.htm](equipment/weapon-00-iWcw1sFLAzByVLBP.htm)|Main-Gauche|Main-gauche|officielle|
|[weapon-00-Ix2vicchE79d6Cl3.htm](equipment/weapon-00-Ix2vicchE79d6Cl3.htm)|Gauntlet|Gantelet|officielle|
|[weapon-00-J6H5UbLVsSXShoTs.htm](equipment/weapon-00-J6H5UbLVsSXShoTs.htm)|Orc Necksplitter|Coupecou orc|officielle|
|[weapon-00-jbSgs5Tq0bkmHUab.htm](equipment/weapon-00-jbSgs5Tq0bkmHUab.htm)|Sansetsukon|Sansetsukon|libre|
|[weapon-00-jkYn89XREERA3V2e.htm](equipment/weapon-00-jkYn89XREERA3V2e.htm)|Shauth Blade|Lame de Shauth|libre|
|[weapon-00-JlakyeGJjNbryq6v.htm](equipment/weapon-00-JlakyeGJjNbryq6v.htm)|Chain Sword|Épée de chaîne|libre|
|[weapon-00-jm5gQnfvVeUiVfdW.htm](equipment/weapon-00-jm5gQnfvVeUiVfdW.htm)|Bayonet|Baïonnette|libre|
|[weapon-00-JNt7GmLCCVz5BiEI.htm](equipment/weapon-00-JNt7GmLCCVz5BiEI.htm)|Javelin|Javeline|officielle|
|[weapon-00-JuR0cug2xq4sWxHR.htm](equipment/weapon-00-JuR0cug2xq4sWxHR.htm)|Butterfly Sword|Épée papillon|libre|
|[weapon-00-K3qNqWRvBhnlwE3Q.htm](equipment/weapon-00-K3qNqWRvBhnlwE3Q.htm)|Arbalest|Arbaleste|libre|
|[weapon-00-k8V3wTG1gMU5ksUr.htm](equipment/weapon-00-k8V3wTG1gMU5ksUr.htm)|Rhoka Sword|Épée rhoka|libre|
|[weapon-00-K9ndlHU5JRHj0ivP.htm](equipment/weapon-00-K9ndlHU5JRHj0ivP.htm)|Bow Staff|Bâton arc|libre|
|[weapon-00-Ka49l6JYNi41tgVi.htm](equipment/weapon-00-Ka49l6JYNi41tgVi.htm)|Atlatl|Atlatl|libre|
|[weapon-00-KaYvuSNXZaOammij.htm](equipment/weapon-00-KaYvuSNXZaOammij.htm)|Crescent Cross|Arbalune|libre|
|[weapon-00-kbKAFqsEDWy5mN1y.htm](equipment/weapon-00-kbKAFqsEDWy5mN1y.htm)|Flyssa|Flissa|libre|
|[weapon-00-kdGqnqbrwPzQfTsm.htm](equipment/weapon-00-kdGqnqbrwPzQfTsm.htm)|Greatclub|Massue|officielle|
|[weapon-00-kedgBVNDRAdmseRe.htm](equipment/weapon-00-kedgBVNDRAdmseRe.htm)|Tamchal Chakram|Chakram tamchal|libre|
|[weapon-00-KgKSX95uVSg8iBBA.htm](equipment/weapon-00-KgKSX95uVSg8iBBA.htm)|Khakkhara|Khakkara|libre|
|[weapon-00-kJJvKm80KwWXPukV.htm](equipment/weapon-00-kJJvKm80KwWXPukV.htm)|Clan Dagger|Dague de clan|officielle|
|[weapon-00-KtarUneAx0hibGtW.htm](equipment/weapon-00-KtarUneAx0hibGtW.htm)|Nodachi|Nodachi|libre|
|[weapon-00-kxrlIT6mcZ4OEAjQ.htm](equipment/weapon-00-kxrlIT6mcZ4OEAjQ.htm)|Elven Branched Spear|Lance ramifiée elfique|libre|
|[weapon-00-LA8wwpQBi6tylE6z.htm](equipment/weapon-00-LA8wwpQBi6tylE6z.htm)|Nunchaku|Nunchaku|officielle|
|[weapon-00-LBSRvTsvrFofbB2c.htm](equipment/weapon-00-LBSRvTsvrFofbB2c.htm)|Piranha Kiss|Bise-piranha|libre|
|[weapon-00-LcIfvbMPnOGYXVge.htm](equipment/weapon-00-LcIfvbMPnOGYXVge.htm)|Gauntlet Bow|Arbalète gantelet|libre|
|[weapon-00-LDHGjtQHRM8bA8ZZ.htm](equipment/weapon-00-LDHGjtQHRM8bA8ZZ.htm)|Rapier Pistol|Pistolet rapière|libre|
|[weapon-00-LdOXiIVgRWpnOtcd.htm](equipment/weapon-00-LdOXiIVgRWpnOtcd.htm)|Zulfikar|Zulfikar|libre|
|[weapon-00-LGgvev6AV0So8tP9.htm](equipment/weapon-00-LGgvev6AV0So8tP9.htm)|Hatchet|Hachette|officielle|
|[weapon-00-LJdbVTOZog39EEbi.htm](equipment/weapon-00-LJdbVTOZog39EEbi.htm)|Longsword|Épée longue|libre|
|[weapon-00-LLYD2GEhzhdxoCAx.htm](equipment/weapon-00-LLYD2GEhzhdxoCAx.htm)|Coat Pistol|Pistolet de poche|libre|
|[weapon-00-loueS11Tfa9WD320.htm](equipment/weapon-00-loueS11Tfa9WD320.htm)|Alchemical Crossbow|Arbalète alchimique|libre|
|[weapon-00-LxqA4VC4np2RLkQb.htm](equipment/weapon-00-LxqA4VC4np2RLkQb.htm)|Butchering Axe|Hachoir de boucher|libre|
|[weapon-00-m3QMdlNxFyDFrkQX.htm](equipment/weapon-00-m3QMdlNxFyDFrkQX.htm)|Mace Multipistol|Masse multi-canons|libre|
|[weapon-00-MccX4AlmqYDjAN2J.htm](equipment/weapon-00-MccX4AlmqYDjAN2J.htm)|Hand Adze|Essette|libre|
|[weapon-00-MeA0g9xvWCbGxf2g.htm](equipment/weapon-00-MeA0g9xvWCbGxf2g.htm)|Mambele|Mambele (Hunga munga)|libre|
|[weapon-00-mgrabKkyLmvxgLBK.htm](equipment/weapon-00-mgrabKkyLmvxgLBK.htm)|Elven Curve Blade|Lame courbe elfique|officielle|
|[weapon-00-mlrmkpOlwpnGkw4I.htm](equipment/weapon-00-mlrmkpOlwpnGkw4I.htm)|Maul|Maillet|officielle|
|[weapon-00-mSOYAQb8iq1N1S1I.htm](equipment/weapon-00-mSOYAQb8iq1N1S1I.htm)|Corset Knife|Couteau de chemise|libre|
|[weapon-00-Mv2I6M70bagbaBPn.htm](equipment/weapon-00-Mv2I6M70bagbaBPn.htm)|Starknife|Lamétoile|officielle|
|[weapon-00-MVAWttmT0QDa7LsV.htm](equipment/weapon-00-MVAWttmT0QDa7LsV.htm)|Longbow|Arc long|officielle|
|[weapon-00-mVxA9E2ernBL6fM6.htm](equipment/weapon-00-mVxA9E2ernBL6fM6.htm)|War Flail|Fléau de guerre|officielle|
|[weapon-00-MvzR9nTnvKTeNjvQ.htm](equipment/weapon-00-MvzR9nTnvKTeNjvQ.htm)|Double-Barreled Pistol|Pistolet à double barillet|libre|
|[weapon-00-mXBOqphNfUCK7L8c.htm](equipment/weapon-00-mXBOqphNfUCK7L8c.htm)|Long Hammer|Marteau long|libre|
|[weapon-00-MYnh7w7EL3AcQT41.htm](equipment/weapon-00-MYnh7w7EL3AcQT41.htm)|Dwarven War Axe|Hache de guerre naine|officielle|
|[weapon-00-N3nNqO5Nw2DIFhrv.htm](equipment/weapon-00-N3nNqO5Nw2DIFhrv.htm)|Flintlock Pistol|Pistolet à silex|libre|
|[weapon-00-NApB2Fl7i8wzp7RX.htm](equipment/weapon-00-NApB2Fl7i8wzp7RX.htm)|Gaff|Gaffe|libre|
|[weapon-00-NhKIUGMcxVusTJ6y.htm](equipment/weapon-00-NhKIUGMcxVusTJ6y.htm)|Harmona Gun|Fusil harmona|libre|
|[weapon-00-NIsxR5zXtVa3PuyU.htm](equipment/weapon-00-NIsxR5zXtVa3PuyU.htm)|Temple Sword|Épée du temple|officielle|
|[weapon-00-nOFcCidD5AwVZWTv.htm](equipment/weapon-00-nOFcCidD5AwVZWTv.htm)|War Razor|Rasoir de combat|libre|
|[weapon-00-nSO0Z662LkkLfa2u.htm](equipment/weapon-00-nSO0Z662LkkLfa2u.htm)|Shield Spikes|Pointes de bouclier|officielle|
|[weapon-00-NufKswitJjxDCX8f.htm](equipment/weapon-00-NufKswitJjxDCX8f.htm)|Naginata|Naginata|libre|
|[weapon-00-O3XAH3VkOx9a0FyD.htm](equipment/weapon-00-O3XAH3VkOx9a0FyD.htm)|Hook Sword|Épée crochet|libre|
|[weapon-00-O7IZBvVoe7W2XnBa.htm](equipment/weapon-00-O7IZBvVoe7W2XnBa.htm)|Machete|Machette|libre|
|[weapon-00-Oiq3QgLrM4i3W5Hg.htm](equipment/weapon-00-Oiq3QgLrM4i3W5Hg.htm)|Ogre Hook|Crochet ogre|officielle|
|[weapon-00-okEnjp0JgUzbVauY.htm](equipment/weapon-00-okEnjp0JgUzbVauY.htm)|Wheel Spikes|Roues à piques|libre|
|[weapon-00-olwngGXM3hpgoLEP.htm](equipment/weapon-00-olwngGXM3hpgoLEP.htm)|Dogslicer|Tranchechien|officielle|
|[weapon-00-ONHFPXNNw9BkNAKm.htm](equipment/weapon-00-ONHFPXNNw9BkNAKm.htm)|Pantograph Gauntlet|Gantelet pantographe|libre|
|[weapon-00-OPMzQdM9rZznMGhW.htm](equipment/weapon-00-OPMzQdM9rZznMGhW.htm)|Lance|Lance d'arçon|libre|
|[weapon-00-oSQET5hKn9q4xlrl.htm](equipment/weapon-00-oSQET5hKn9q4xlrl.htm)|Gnome Flickmace|Masse-yoyo gnome|officielle|
|[weapon-00-oTQKyZ8Vd2RPzaWK.htm](equipment/weapon-00-oTQKyZ8Vd2RPzaWK.htm)|Harpoon|Harpon|libre|
|[weapon-00-oW9neeVcpHjEvjcN.htm](equipment/weapon-00-oW9neeVcpHjEvjcN.htm)|Mikazuki|Mikazuki|libre|
|[weapon-00-PIzBQehx975vMsvZ.htm](equipment/weapon-00-PIzBQehx975vMsvZ.htm)|Throwing Knife|Couteau de lancer|libre|
|[weapon-00-PJboj72eI9JXjtbn.htm](equipment/weapon-00-PJboj72eI9JXjtbn.htm)|Bladed Gauntlet|Gantelet à lame|libre|
|[weapon-00-pKiuN3wnAxlJCBuK.htm](equipment/weapon-00-pKiuN3wnAxlJCBuK.htm)|Reinforced Wheels|Roues renforcées|libre|
|[weapon-00-PlXEjK5nqIxEYXQa.htm](equipment/weapon-00-PlXEjK5nqIxEYXQa.htm)|Forked Bipod|Bipied à fourche|libre|
|[weapon-00-pnA6uENYDDduWmAn.htm](equipment/weapon-00-pnA6uENYDDduWmAn.htm)|Thunder Sling|Fronde tonnerre|libre|
|[weapon-00-PnFfW5u24xZV6mOH.htm](equipment/weapon-00-PnFfW5u24xZV6mOH.htm)|Poi|Poï|libre|
|[weapon-00-Po1oeTDUaSUxeZxG.htm](equipment/weapon-00-Po1oeTDUaSUxeZxG.htm)|Donchak|Donchak|libre|
|[weapon-00-puQ90OZgQpVhxLRe.htm](equipment/weapon-00-puQ90OZgQpVhxLRe.htm)|Wish Blade|Lame à souhaits|libre|
|[weapon-00-QAEVaSQ3SCT8AuBX.htm](equipment/weapon-00-QAEVaSQ3SCT8AuBX.htm)|Whip Claw|Fouet griffu|libre|
|[weapon-00-qQJguPUWpYY8z6BD.htm](equipment/weapon-00-qQJguPUWpYY8z6BD.htm)|Umbrella Injector|Parapluie injecteur|libre|
|[weapon-00-R2mDnYVqewmx3dV0.htm](equipment/weapon-00-R2mDnYVqewmx3dV0.htm)|Exquisite Sword Cane Sheath|Fourreau de canne épée raffinée|libre|
|[weapon-00-RbM4HvPyrZ4YJxRc.htm](equipment/weapon-00-RbM4HvPyrZ4YJxRc.htm)|Light Pick|Pic de guerre léger|officielle|
|[weapon-00-rfP9e1fnwjnIQSJK.htm](equipment/weapon-00-rfP9e1fnwjnIQSJK.htm)|Fire Poi|Poï enflammé|libre|
|[weapon-00-RhfhVSfiH23v4U7k.htm](equipment/weapon-00-RhfhVSfiH23v4U7k.htm)|Juggling Club|Massue de jongleur|libre|
|[weapon-00-rhHGULVS5tumszGP.htm](equipment/weapon-00-rhHGULVS5tumszGP.htm)|Chakri|Chakri|libre|
|[weapon-00-RN3m5Hb5LPbdZGoO.htm](equipment/weapon-00-RN3m5Hb5LPbdZGoO.htm)|Daikyu|Daikyu|libre|
|[weapon-00-rQWaJhI5Bko5x14Z.htm](equipment/weapon-00-rQWaJhI5Bko5x14Z.htm)|Dagger|Dague|officielle|
|[weapon-00-rXt4629QSg7KDTgJ.htm](equipment/weapon-00-rXt4629QSg7KDTgJ.htm)|Warhammer|Marteau de guerre|officielle|
|[weapon-00-RytXxlJJ7dib8seN.htm](equipment/weapon-00-RytXxlJJ7dib8seN.htm)|Triggerbrand|Pistolame|libre|
|[weapon-00-sAjzLXgA2LnIwpBM.htm](equipment/weapon-00-sAjzLXgA2LnIwpBM.htm)|Orc Knuckle Dagger|Dague coup-de-poing orque|libre|
|[weapon-00-Sarxo8kSrJs2qJEw.htm](equipment/weapon-00-Sarxo8kSrJs2qJEw.htm)|Urumi|Urumi|libre|
|[weapon-00-ssDxSpQTYORoeCFA.htm](equipment/weapon-00-ssDxSpQTYORoeCFA.htm)|Frying Pan|Poêle à frire|libre|
|[weapon-00-sY3UJ34xJ4MA0FMK.htm](equipment/weapon-00-sY3UJ34xJ4MA0FMK.htm)|Visap|Visap|libre|
|[weapon-00-SzUynRs4HVtnpnel.htm](equipment/weapon-00-SzUynRs4HVtnpnel.htm)|Air Repeater|Fusil à air comprimé|libre|
|[weapon-00-t5FbyZtRL4qV0V7k.htm](equipment/weapon-00-t5FbyZtRL4qV0V7k.htm)|Flail|Fléau|officielle|
|[weapon-00-T5ojw0tuXiAajhZx.htm](equipment/weapon-00-T5ojw0tuXiAajhZx.htm)|Rotary Bow|Arbalète rotative|libre|
|[weapon-00-TDrO7Xdyn7juFy3c.htm](equipment/weapon-00-TDrO7Xdyn7juFy3c.htm)|Kukri|Kukri|officielle|
|[weapon-00-tH5GirEy7YB3ZgCk.htm](equipment/weapon-00-tH5GirEy7YB3ZgCk.htm)|Rapier|Rapière|officielle|
|[weapon-00-tHIb3ynOHj7dGiCH.htm](equipment/weapon-00-tHIb3ynOHj7dGiCH.htm)|Reinforced Stock|Crosse renforcée|libre|
|[weapon-00-tk4cfktEnMrp4K6m.htm](equipment/weapon-00-tk4cfktEnMrp4K6m.htm)|Pepperbox|Poivrière|libre|
|[weapon-00-TLQErnOpM9Luy7rL.htm](equipment/weapon-00-TLQErnOpM9Luy7rL.htm)|Sap|Matraque|officielle|
|[weapon-00-tOhoGvmCMw4JpWcS.htm](equipment/weapon-00-tOhoGvmCMw4JpWcS.htm)|Spear|Lance|libre|
|[weapon-00-TQ3kT9pUFQNkH2dG.htm](equipment/weapon-00-TQ3kT9pUFQNkH2dG.htm)|Spiral Rapier|Rapière spiralée|libre|
|[weapon-00-tsvqlHxWfLwLQorQ.htm](equipment/weapon-00-tsvqlHxWfLwLQorQ.htm)|Wrist Launcher|Lanceur de poignet|libre|
|[weapon-00-TT0pJz9odKApfq6D.htm](equipment/weapon-00-TT0pJz9odKApfq6D.htm)|Guisarme|Guisarme|officielle|
|[weapon-00-Tt4Qw64fwrxhr5gT.htm](equipment/weapon-00-Tt4Qw64fwrxhr5gT.htm)|Dart|Fléchette|officielle|
|[weapon-00-u2u6dkr01AB34tyA.htm](equipment/weapon-00-u2u6dkr01AB34tyA.htm)|Sai|Saï|officielle|
|[weapon-00-UCH4myuFnokGv0vF.htm](equipment/weapon-00-UCH4myuFnokGv0vF.htm)|Sling|Fronde|officielle|
|[weapon-00-UCK8zXKb0Gi6uyum.htm](equipment/weapon-00-UCK8zXKb0Gi6uyum.htm)|Injection Spear|Lance à injection|libre|
|[weapon-00-UfurZQK6H6SgOjqe.htm](equipment/weapon-00-UfurZQK6H6SgOjqe.htm)|Ranseur|Corsèque|officielle|
|[weapon-00-uM0OlCC1Sh6OLdNn.htm](equipment/weapon-00-uM0OlCC1Sh6OLdNn.htm)|Throwing Knife (Extinction Curse)|Couteau de lancer (Sentence d'extinction)|libre|
|[weapon-00-UnTUQ9w8yswupr9m.htm](equipment/weapon-00-UnTUQ9w8yswupr9m.htm)|Cane Pistol|Canne pistolet|libre|
|[weapon-00-UX71GkWBL9g41VwM.htm](equipment/weapon-00-UX71GkWBL9g41VwM.htm)|Greatsword|Épée à deux mains|libre|
|[weapon-00-UXjKXqsfWYiayeMD.htm](equipment/weapon-00-UXjKXqsfWYiayeMD.htm)|Scorpion Whip|Fouet scorpion|libre|
|[weapon-00-v8C4hkqMNeUk60Db.htm](equipment/weapon-00-v8C4hkqMNeUk60Db.htm)|Rungu|Rungu|libre|
|[weapon-00-VitLIpdIAmKlGb7i.htm](equipment/weapon-00-VitLIpdIAmKlGb7i.htm)|Sword Cane|Canne épée|libre|
|[weapon-00-vlnzTSsRmWeSkt9O.htm](equipment/weapon-00-vlnzTSsRmWeSkt9O.htm)|Glaive|Coutille|libre|
|[weapon-00-vnT33Z3V101FOUuV.htm](equipment/weapon-00-vnT33Z3V101FOUuV.htm)|Whipstaff|Bian gan|libre|
|[weapon-00-vsJ0zvYZzduP7rtD.htm](equipment/weapon-00-vsJ0zvYZzduP7rtD.htm)|Tekko-Kagi|Tekko-kagi|libre|
|[weapon-00-VTFTlP8xmPKKV4S6.htm](equipment/weapon-00-VTFTlP8xmPKKV4S6.htm)|Bec de Corbin|Bec de corbin|libre|
|[weapon-00-War0uyLBx1jA0Ge7.htm](equipment/weapon-00-War0uyLBx1jA0Ge7.htm)|Battle Axe|Hache d'armes|officielle|
|[weapon-00-wbJPWXKzQBKYZ74s.htm](equipment/weapon-00-wbJPWXKzQBKYZ74s.htm)|Taw Launcher|Lance calots|libre|
|[weapon-00-WiPcevdeD4YoTLRa.htm](equipment/weapon-00-WiPcevdeD4YoTLRa.htm)|Shauth Lash|Chaîne de Shauth|libre|
|[weapon-00-wkzxLpSe7LN6c5Ld.htm](equipment/weapon-00-wkzxLpSe7LN6c5Ld.htm)|Sawtooth Saber|Sabre dentelé|officielle|
|[weapon-00-wpvfykcSSbg27DgU.htm](equipment/weapon-00-wpvfykcSSbg27DgU.htm)|Dart Umbrella|Parapluie à fléchettes|libre|
|[weapon-00-WUA40bb01pSWv88I.htm](equipment/weapon-00-WUA40bb01pSWv88I.htm)|Fire Lance|Lance de feu|libre|
|[weapon-00-WuzK2R5ra5SZdbij.htm](equipment/weapon-00-WuzK2R5ra5SZdbij.htm)|Tri-bladed Katar|Katar aux trois lames|libre|
|[weapon-00-WxczDmQ5qOaD5IxB.htm](equipment/weapon-00-WxczDmQ5qOaD5IxB.htm)|Jiu Huan Dao|Jiu Huan Dao|libre|
|[weapon-00-wYruARP1YLfTQBsc.htm](equipment/weapon-00-wYruARP1YLfTQBsc.htm)|Fangwire|Filacroc|libre|
|[weapon-00-x1TOpwH755Ami5bC.htm](equipment/weapon-00-x1TOpwH755Ami5bC.htm)|Light Mace|Masse d'armes légère|officielle|
|[weapon-00-XGtIUZ4ZNKuFx1uL.htm](equipment/weapon-00-XGtIUZ4ZNKuFx1uL.htm)|Falchion|Cimeterre à deux mains|officielle|
|[weapon-00-XguFCTUBh1yqJXd1.htm](equipment/weapon-00-XguFCTUBh1yqJXd1.htm)|Spraysling|Fronde à aspersion|libre|
|[weapon-00-xHdbwPOUgLPUtqLj.htm](equipment/weapon-00-xHdbwPOUgLPUtqLj.htm)|Falcata|Falcata|libre|
|[weapon-00-xmOGKAuCNYO4tJkM.htm](equipment/weapon-00-xmOGKAuCNYO4tJkM.htm)|Griffon Cane|Canne griffon|libre|
|[weapon-00-XQ57G0xuxu3nSzcD.htm](equipment/weapon-00-XQ57G0xuxu3nSzcD.htm)|Probing Cane|Canne d'aveugle|libre|
|[weapon-00-xu3azdMCIa53Oe1f.htm](equipment/weapon-00-xu3azdMCIa53Oe1f.htm)|Meteor Hammer|Marteau météore|libre|
|[weapon-00-XyA6PKV46aNlLXOd.htm](equipment/weapon-00-XyA6PKV46aNlLXOd.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[weapon-00-Y1dkRsRd1Z7Jf2y6.htm](equipment/weapon-00-Y1dkRsRd1Z7Jf2y6.htm)|Katana|Katana|libre|
|[weapon-00-y5bkwzClxfs6gpDn.htm](equipment/weapon-00-y5bkwzClxfs6gpDn.htm)|Boomerang|Boomerang|libre|
|[weapon-00-ye7cYmornhgnnOlu.htm](equipment/weapon-00-ye7cYmornhgnnOlu.htm)|Sickle-saber|Sabre-faucille|libre|
|[weapon-00-YFfG2fMyaIAXkmtr.htm](equipment/weapon-00-YFfG2fMyaIAXkmtr.htm)|Spiked Gauntlet|Gantelet clouté|officielle|
|[weapon-00-yIY0voZkwMoff5b3.htm](equipment/weapon-00-yIY0voZkwMoff5b3.htm)|Scizore|Scizore|libre|
|[weapon-00-YLb6XnT7OxAVGL5m.htm](equipment/weapon-00-YLb6XnT7OxAVGL5m.htm)|Khopesh|Khopesh|libre|
|[weapon-00-ymyPCjfyXCNyDcnn.htm](equipment/weapon-00-ymyPCjfyXCNyDcnn.htm)|Breaching Pike|Pique de brèche|libre|
|[weapon-00-ynnBwzkzsR6B73iO.htm](equipment/weapon-00-ynnBwzkzsR6B73iO.htm)|Sickle|Serpe|officielle|
|[weapon-00-YnPYSKCQBLIOtm0J.htm](equipment/weapon-00-YnPYSKCQBLIOtm0J.htm)|Aklys|Aklys|officielle|
|[weapon-00-YnwEttFE1bJacLDh.htm](equipment/weapon-00-YnwEttFE1bJacLDh.htm)|Horsechopper|Coupecheval|officielle|
|[weapon-00-YOpopjoWgU7bkmwh.htm](equipment/weapon-00-YOpopjoWgU7bkmwh.htm)|Wakizashi|Wakizashi|libre|
|[weapon-00-yUgOLSyb4pGGxfCR.htm](equipment/weapon-00-yUgOLSyb4pGGxfCR.htm)|Feng Huo Lun|Roues de vent et de feu|libre|
|[weapon-00-YUzPv0i8d8p2J9yx.htm](equipment/weapon-00-YUzPv0i8d8p2J9yx.htm)|Bola|Bolas|libre|
|[weapon-00-zi9ovfoRp2fMhfpO.htm](equipment/weapon-00-zi9ovfoRp2fMhfpO.htm)|Spiked Chain|Chaîne cloutée|officielle|
|[weapon-00-znIsDYwM5rLK2aVD.htm](equipment/weapon-00-znIsDYwM5rLK2aVD.htm)|Nine-Ring Sword|Épée aux neufs anneaux|libre|
|[weapon-00-zO6Fvq7ghEXpJgJR.htm](equipment/weapon-00-zO6Fvq7ghEXpJgJR.htm)|Tricky Pick|Pic trompeur|libre|
|[weapon-00-zSAoyzcf3nSeZCiF.htm](equipment/weapon-00-zSAoyzcf3nSeZCiF.htm)|Kama|Kama|officielle|
|[weapon-00-zWp7Kcn7mhKyGFxW.htm](equipment/weapon-00-zWp7Kcn7mhKyGFxW.htm)|Sun Sling|Fronde solaire|libre|
|[weapon-00-zXbZn5HJf81tquPm.htm](equipment/weapon-00-zXbZn5HJf81tquPm.htm)|War Lance|Lance de guerre|libre|
|[weapon-01-10J0WjnfgRvriLXc.htm](equipment/weapon-01-10J0WjnfgRvriLXc.htm)|Gun Sword|Épée-pistolet|libre|
|[weapon-01-2dHdOVBySillcOGM.htm](equipment/weapon-01-2dHdOVBySillcOGM.htm)|Backpack Ballista|Sac à dos baliste|libre|
|[weapon-01-3Sij5RwC6Z1ZVuFp.htm](equipment/weapon-01-3Sij5RwC6Z1ZVuFp.htm)|Skunk Bomb (Lesser)|Bombe pestilentielle inférieure|libre|
|[weapon-01-3T3ayvgw9HDn05Mz.htm](equipment/weapon-01-3T3ayvgw9HDn05Mz.htm)|Combat Grapnel|Grappin de combat|libre|
|[weapon-01-68bqnb84kst3hFjP.htm](equipment/weapon-01-68bqnb84kst3hFjP.htm)|Backpack Catapult|Sac à dos catapulte|libre|
|[weapon-01-6mgB6Wv8X65pFMRL.htm](equipment/weapon-01-6mgB6Wv8X65pFMRL.htm)|Three Peaked Tree|Arbre à trois piques|libre|
|[weapon-01-841G24r5ITFYk5zQ.htm](equipment/weapon-01-841G24r5ITFYk5zQ.htm)|Pernicious Spore Bomb (Lesser)|Bombe de spores pernicieuses inférieure|libre|
|[weapon-01-8ncDuZSIZAHl7PW8.htm](equipment/weapon-01-8ncDuZSIZAHl7PW8.htm)|Repeating Crossbow|Arbalète à répétition|libre|
|[weapon-01-AFR01HVd7DcZvkpP.htm](equipment/weapon-01-AFR01HVd7DcZvkpP.htm)|Bottled Lightning (Lesser)|Foudre en bouteille inférieur|officielle|
|[weapon-01-Bb3cvysXG8rCM5FM.htm](equipment/weapon-01-Bb3cvysXG8rCM5FM.htm)|Vexing Vapor (Lesser)|Vapeur contrariante inférieure|libre|
|[weapon-01-BG04L9GjWerSgWX3.htm](equipment/weapon-01-BG04L9GjWerSgWX3.htm)|Water Bomb (Lesser)|Bombe à eau inférieure|libre|
|[weapon-01-bP608Wb1l9ZY1zwb.htm](equipment/weapon-01-bP608Wb1l9ZY1zwb.htm)|Repeating Heavy Crossbow|Arbalète lourde à répétition|libre|
|[weapon-01-c124j3cpv8rl5MLp.htm](equipment/weapon-01-c124j3cpv8rl5MLp.htm)|Peshpine Grenade (Lesser)|Grenade d'épines de pesh inférieure|libre|
|[weapon-01-caWhNJ2ahCzZjr55.htm](equipment/weapon-01-caWhNJ2ahCzZjr55.htm)|Gnome Amalgam Musket|Mousquet amalgame gnome|libre|
|[weapon-01-cFHUt5tTA7jVPtRh.htm](equipment/weapon-01-cFHUt5tTA7jVPtRh.htm)|Explosive Dogslicer|Tranchechien explosif|libre|
|[weapon-01-DFZj7RCrk6rk9fhf.htm](equipment/weapon-01-DFZj7RCrk6rk9fhf.htm)|Granny's Hedge Trimmer|Taille-haie de grand-mère|libre|
|[weapon-01-DHKCkxAGveLE3Jnf.htm](equipment/weapon-01-DHKCkxAGveLE3Jnf.htm)|Shobhad Longrifle|Fusil long shobhad|libre|
|[weapon-01-EpxmUtLpCkE8R6KJ.htm](equipment/weapon-01-EpxmUtLpCkE8R6KJ.htm)|Frost Vial (Lesser)|Fiole de givre inférieure|officielle|
|[weapon-01-EvBQcbkiYUusdFKY.htm](equipment/weapon-01-EvBQcbkiYUusdFKY.htm)|Phalanx Piercer|Perceur de phalange|libre|
|[weapon-01-GG7XnMngd9MnxhHb.htm](equipment/weapon-01-GG7XnMngd9MnxhHb.htm)|Big Boom Gun|Fusil grand boum|libre|
|[weapon-01-gO5dOlPBk57bg2x5.htm](equipment/weapon-01-gO5dOlPBk57bg2x5.htm)|Slide Pistol|Pistolet semi-automatique|libre|
|[weapon-01-Hh3F4DEP6aVulwQN.htm](equipment/weapon-01-Hh3F4DEP6aVulwQN.htm)|Stiletto Pen|Stylo styletto|libre|
|[weapon-01-HtBqmBFYuxJp2QiZ.htm](equipment/weapon-01-HtBqmBFYuxJp2QiZ.htm)|Mud Bomb (Lesser)|Bombe de boue inférieure|libre|
|[weapon-01-HYxnpGzoDL70FrVs.htm](equipment/weapon-01-HYxnpGzoDL70FrVs.htm)|Pressure Bomb (Lesser)|Bombe à pression inférieure|libre|
|[weapon-01-iSceAVMkVv1uKK7t.htm](equipment/weapon-01-iSceAVMkVv1uKK7t.htm)|Aldori Dueling Sword|Épée de duel aldorie|officielle|
|[weapon-01-j8ajvNqyyQGBpBch.htm](equipment/weapon-01-j8ajvNqyyQGBpBch.htm)|Blight Bomb (Lesser)|Bombe fléau inférieure|libre|
|[weapon-01-JAOzIeqKr5hULumB.htm](equipment/weapon-01-JAOzIeqKr5hULumB.htm)|Alchemical Gauntlet|Gantelet alchimique|libre|
|[weapon-01-jcIabnkJgjwzK6Og.htm](equipment/weapon-01-jcIabnkJgjwzK6Og.htm)|Dwarven Scattergun|Éparpilleur nain|libre|
|[weapon-01-MMYL2rfbYNWT0Rs1.htm](equipment/weapon-01-MMYL2rfbYNWT0Rs1.htm)|Spoon Gun|Canon à cuiller|libre|
|[weapon-01-ndVATyJ2c8Ka3HKm.htm](equipment/weapon-01-ndVATyJ2c8Ka3HKm.htm)|Sulfur Bomb (Lesser)|Bombe de soufre inférieure|libre|
|[weapon-01-NEZfXSasFdLdQFBi.htm](equipment/weapon-01-NEZfXSasFdLdQFBi.htm)|Jezail|Jezaïl|libre|
|[weapon-01-ogv5nwwrc3sU8DnP.htm](equipment/weapon-01-ogv5nwwrc3sU8DnP.htm)|Dueling Pistol|Pistolet de duel|libre|
|[weapon-01-OOqno1OwqJQBEtXe.htm](equipment/weapon-01-OOqno1OwqJQBEtXe.htm)|Tallow Bomb (Lesser)|Bombe de suif inférieure|libre|
|[weapon-01-oUaeLZLK7WfolqBb.htm](equipment/weapon-01-oUaeLZLK7WfolqBb.htm)|Axe Musket|Mousquet-hache|libre|
|[weapon-01-OUOSAKU1tSipWtty.htm](equipment/weapon-01-OUOSAKU1tSipWtty.htm)|Bioluminescence Bomb|Bombe bioluminescente|libre|
|[weapon-01-rePjr9KceepKIDuB.htm](equipment/weapon-01-rePjr9KceepKIDuB.htm)|Double-Barreled Musket|Mousquet à double barrilet|libre|
|[weapon-01-RsCLCgCgGAn67ry5.htm](equipment/weapon-01-RsCLCgCgGAn67ry5.htm)|Star Grenade (Lesser)|Grenade étoilée inférieure|libre|
|[weapon-01-S1TrEDE8JNkchMca.htm](equipment/weapon-01-S1TrEDE8JNkchMca.htm)|Sticky Algae Bomb (Lesser)|Bombe d'algues collantes inférieure|libre|
|[weapon-01-sF9uOLAjQiSmt9i3.htm](equipment/weapon-01-sF9uOLAjQiSmt9i3.htm)|Boarding Pike|Pique d'abordage|libre|
|[weapon-01-t2A5PzInAqmErLzK.htm](equipment/weapon-01-t2A5PzInAqmErLzK.htm)|Goo Grenade (Lesser)|Grenade gluante inférieure|libre|
|[weapon-01-T6Appwwl6nUl56Xj.htm](equipment/weapon-01-T6Appwwl6nUl56Xj.htm)|Glue Bomb (Lesser)|Bombe collante inférieure|libre|
|[weapon-01-tm5l8uXtQQGMTvgY.htm](equipment/weapon-01-tm5l8uXtQQGMTvgY.htm)|Twigjack Sack (Lesser)|Sac de ronces inférieur|libre|
|[weapon-01-ucz1WdBA0Ma34OsS.htm](equipment/weapon-01-ucz1WdBA0Ma34OsS.htm)|Flying Talon|Serre volante|libre|
|[weapon-01-udm5X6TXm5vdIo4h.htm](equipment/weapon-01-udm5X6TXm5vdIo4h.htm)|Sukgung|Sukgung|libre|
|[weapon-01-UogGzQfXDc32E1p7.htm](equipment/weapon-01-UogGzQfXDc32E1p7.htm)|Barricade Buster|Briseur de barricade|libre|
|[weapon-01-vRCH0cQJMllWvpfU.htm](equipment/weapon-01-vRCH0cQJMllWvpfU.htm)|Polytool|Outil universel|libre|
|[weapon-01-wdJ0AFtkJVycVRjX.htm](equipment/weapon-01-wdJ0AFtkJVycVRjX.htm)|Blood Bomb (Lesser)|Bombe hémorragique inférieure|libre|
|[weapon-01-WJMHv8xXowLNP2Di.htm](equipment/weapon-01-WJMHv8xXowLNP2Di.htm)|Junk Bomb (Lesser)|Bombe à fragmentation inférieure|libre|
|[weapon-01-WS78LUHzlpeONMRo.htm](equipment/weapon-01-WS78LUHzlpeONMRo.htm)|Scrollstaff|Bâton-parchemin|libre|
|[weapon-01-Xnqglykl3Cif8rN9.htm](equipment/weapon-01-Xnqglykl3Cif8rN9.htm)|Thunderstone (Lesser)|Pierre à tonnerre inférieure|officielle|
|[weapon-01-xZFT5JdEvDi7q467.htm](equipment/weapon-01-xZFT5JdEvDi7q467.htm)|Dwarven Daisy (Lesser)|Marguerite naine inférieure|libre|
|[weapon-01-yd3kEK21YknZLlcT.htm](equipment/weapon-01-yd3kEK21YknZLlcT.htm)|Alchemist's Fire (Lesser)|Feu grégeois inférieur|libre|
|[weapon-01-yyZ3iD8IXMIrrWL1.htm](equipment/weapon-01-yyZ3iD8IXMIrrWL1.htm)|Switchscythe|Faux escamotable|libre|
|[weapon-01-zcrIuILnp6oh4dr9.htm](equipment/weapon-01-zcrIuILnp6oh4dr9.htm)|Repeating Hand Crossbow|Arbalète de poing à répétition|libre|
|[weapon-01-zHJEzmoybIfX1LaV.htm](equipment/weapon-01-zHJEzmoybIfX1LaV.htm)|Hammer Gun|Fusil marteau|libre|
|[weapon-02-FNDq4NFSN0g2HKWO.htm](equipment/weapon-02-FNDq4NFSN0g2HKWO.htm)|Handwraps of Mighty Blows|Bandelettes de coups puissants|officielle|
|[weapon-03-0U4qXSxfZDMIJDtA.htm](equipment/weapon-03-0U4qXSxfZDMIJDtA.htm)|Mud Bomb (Moderate)|Bombe de boue modérée|libre|
|[weapon-03-158vwM1andv8DbRI.htm](equipment/weapon-03-158vwM1andv8DbRI.htm)|Blight Bomb (Moderate)|Bombe fléau modérée|libre|
|[weapon-03-1MuIK1TnzdKuqZJO.htm](equipment/weapon-03-1MuIK1TnzdKuqZJO.htm)|Junk Bomb (Moderate)|Bombe à fragmentation modérée|libre|
|[weapon-03-2pfEtA97lpG93kG2.htm](equipment/weapon-03-2pfEtA97lpG93kG2.htm)|Sulfur Bomb (Moderate)|Bombe de soufre modérée|libre|
|[weapon-03-4pVGwkVi6izoUuLC.htm](equipment/weapon-03-4pVGwkVi6izoUuLC.htm)|Tallow Bomb (Moderate)|Bombe de suif modérée|libre|
|[weapon-03-5BU4hJ1oXMLtSkuA.htm](equipment/weapon-03-5BU4hJ1oXMLtSkuA.htm)|Sparkblade|Lame crépitante|libre|
|[weapon-03-7irlzyZA50KqTwce.htm](equipment/weapon-03-7irlzyZA50KqTwce.htm)|Boastful Hunter|Chasseur vantard|libre|
|[weapon-03-8TEundYBdonchDj1.htm](equipment/weapon-03-8TEundYBdonchDj1.htm)|Staff of Air|Bâton de l'air|libre|
|[weapon-03-97QyNEOAyYLdGaYc.htm](equipment/weapon-03-97QyNEOAyYLdGaYc.htm)|Bottled Lightning (Moderate)|Foudre en bouteille modérée|officielle|
|[weapon-03-AvGZqDfalwwbYFvA.htm](equipment/weapon-03-AvGZqDfalwwbYFvA.htm)|Peshpine Grenade (Moderate)|Grenade d'épines de pesh modérée|libre|
|[weapon-03-cbgJbCMU330TVmmO.htm](equipment/weapon-03-cbgJbCMU330TVmmO.htm)|Pernicious Spore Bomb (Moderate)|Bombe de spores pernicieuses modérée|libre|
|[weapon-03-da6nSPRUwGicvx3c.htm](equipment/weapon-03-da6nSPRUwGicvx3c.htm)|Pressure Bomb (Moderate)|Bombe à pression modérée|libre|
|[weapon-03-Do4rJuA1nhr2TTiF.htm](equipment/weapon-03-Do4rJuA1nhr2TTiF.htm)|Dwarven Daisy (Moderate)|Marguerite naine modérée|libre|
|[weapon-03-evBPzM1VsuYcoenn.htm](equipment/weapon-03-evBPzM1VsuYcoenn.htm)|Glue Bomb (Moderate)|Bombe collante modérée|libre|
|[weapon-03-frCSW1zLFYiYpjdR.htm](equipment/weapon-03-frCSW1zLFYiYpjdR.htm)|Staff of Water|Bâton de l'eau|libre|
|[weapon-03-gWr4q4HiyGhETA8H.htm](equipment/weapon-03-gWr4q4HiyGhETA8H.htm)|Alchemist's Fire (Moderate)|Feu grégeois modéré|libre|
|[weapon-03-h6O5uto0Ry1WDjXn.htm](equipment/weapon-03-h6O5uto0Ry1WDjXn.htm)|Sticky Algae Bomb (Moderate)|Bombe d'algues collantes modérée|libre|
|[weapon-03-hZ0s1gz0Cr0LGOLG.htm](equipment/weapon-03-hZ0s1gz0Cr0LGOLG.htm)|Water Bomb (Moderate)|Bombe à eau modérée|libre|
|[weapon-03-Ix5w4UNElK0ncBm2.htm](equipment/weapon-03-Ix5w4UNElK0ncBm2.htm)|Twigjack Sack (Moderate)|Sac de ronces modéré|libre|
|[weapon-03-JOaELkzLWTywhn5Z.htm](equipment/weapon-03-JOaELkzLWTywhn5Z.htm)|Thunderstone (Moderate)|Pierre à tonnerre modérée|officielle|
|[weapon-03-KFTkiLXWENatK9XY.htm](equipment/weapon-03-KFTkiLXWENatK9XY.htm)|Star Grenade (Moderate)|Grenade étoilée modérée|libre|
|[weapon-03-Kx6FQS5GyVB6jlrW.htm](equipment/weapon-03-Kx6FQS5GyVB6jlrW.htm)|Fighter's Fork|Fourche de guerre|libre|
|[weapon-03-m091zbWD9EoH6kWC.htm](equipment/weapon-03-m091zbWD9EoH6kWC.htm)|Skunk Bomb (Moderate)|Bombe pestilentielle modérée|libre|
|[weapon-03-nVvH1ZcM7OwIVIs8.htm](equipment/weapon-03-nVvH1ZcM7OwIVIs8.htm)|Frost Vial (Moderate)|Fiole de givre modérée|officielle|
|[weapon-03-ODXNXvcLQGBw6kXj.htm](equipment/weapon-03-ODXNXvcLQGBw6kXj.htm)|Blood Bomb (Moderate)|Bombe hémorragique modérée|libre|
|[weapon-03-onFxlajKyWpHZcXt.htm](equipment/weapon-03-onFxlajKyWpHZcXt.htm)|Smoking Sword|Épée fumante|libre|
|[weapon-03-pX3rpVDBLqClcL9M.htm](equipment/weapon-03-pX3rpVDBLqClcL9M.htm)|Staff of Earth|Bâton de la terre|libre|
|[weapon-03-SgtqZxt26BdjUmEB.htm](equipment/weapon-03-SgtqZxt26BdjUmEB.htm)|Acid Flask (Moderate)|Fiole d'acide modérée|libre|
|[weapon-03-VSxLv1JRSo10waL0.htm](equipment/weapon-03-VSxLv1JRSo10waL0.htm)|Ghast Stiletto|Stiletto blême|libre|
|[weapon-03-wHfhyAqJgKP9v3Dv.htm](equipment/weapon-03-wHfhyAqJgKP9v3Dv.htm)|Wordreaper|Moissonneur de mots|libre|
|[weapon-03-wi97vRj8IXx3wLmZ.htm](equipment/weapon-03-wi97vRj8IXx3wLmZ.htm)|Goo Grenade (Moderate)|Grenade gluante modérée|libre|
|[weapon-03-Z9TThSU2KtCX9gfD.htm](equipment/weapon-03-Z9TThSU2KtCX9gfD.htm)|Vexing Vapor (Moderate)|Vapeur contrariante modérée|libre|
|[weapon-04-3OkOKxCee9WruGU5.htm](equipment/weapon-04-3OkOKxCee9WruGU5.htm)|Staff of Healing|Bâton de guérison|libre|
|[weapon-04-6TC4FywUTzVAK97v.htm](equipment/weapon-04-6TC4FywUTzVAK97v.htm)|Composer Staff|Bâton du compositeur|libre|
|[weapon-04-99ffrZOWek8ePE4n.htm](equipment/weapon-04-99ffrZOWek8ePE4n.htm)|Ugly Cute's Gift|Don de Moche mignon|libre|
|[weapon-04-bTwZNckINJgiT8Ef.htm](equipment/weapon-04-bTwZNckINJgiT8Ef.htm)|Spore Shephard's Staff|Bâton du pâtre des spores|libre|
|[weapon-04-HarOsJI1p5oHt2Vd.htm](equipment/weapon-04-HarOsJI1p5oHt2Vd.htm)|Atmospheric Staff (Lesser)|Bâton atmosphérique inférieur|libre|
|[weapon-04-JMu3VUwhQu1SLSFd.htm](equipment/weapon-04-JMu3VUwhQu1SLSFd.htm)|Aether Marbles (Lesser)|Billes éthérées inférieures|libre|
|[weapon-04-mQyhSg9M3VdGEUrN.htm](equipment/weapon-04-mQyhSg9M3VdGEUrN.htm)|Skysunder|Briseciel|libre|
|[weapon-04-RN6rEc8eSkruNLPW.htm](equipment/weapon-04-RN6rEc8eSkruNLPW.htm)|Exquisite Sword Cane|Canne épée raffinée|libre|
|[weapon-04-SeZfwtBYwmxKrphR.htm](equipment/weapon-04-SeZfwtBYwmxKrphR.htm)|Poisoner's Staff|Bâton de l'empoisonneur|libre|
|[weapon-04-V4wgOWYmHlbSZsVG.htm](equipment/weapon-04-V4wgOWYmHlbSZsVG.htm)|Crystal Shards (Moderate)|Éclats de cristal modérés|libre|
|[weapon-04-ZwJXdOdnE8TcSHK1.htm](equipment/weapon-04-ZwJXdOdnE8TcSHK1.htm)|Fiend's Hunger|Faim du fiélon|libre|
|[weapon-05-1eiaTQo9SKiybP8G.htm](equipment/weapon-05-1eiaTQo9SKiybP8G.htm)|Scizore of the Crab|Scizore du crabe|libre|
|[weapon-05-1S3RgLWa1DBitB9V.htm](equipment/weapon-05-1S3RgLWa1DBitB9V.htm)|Dagger of Eternal Sleep|Dague du sommeil éternel|libre|
|[weapon-05-6HOQwVUQNplJsrhq.htm](equipment/weapon-05-6HOQwVUQNplJsrhq.htm)|Reaper's Lancet|Lancette de la faucheuse|libre|
|[weapon-05-6rWcLbV82KYwaHo1.htm](equipment/weapon-05-6rWcLbV82KYwaHo1.htm)|Dog-Bone Knife|Couteau os-de-chien|libre|
|[weapon-05-CcQDscF79qs9HfJg.htm](equipment/weapon-05-CcQDscF79qs9HfJg.htm)|Silver Orb (Lesser)|Orbe d'argent inférieur|libre|
|[weapon-05-Dti9PZZoty6We8OV.htm](equipment/weapon-05-Dti9PZZoty6We8OV.htm)|Cinderclaw Gauntlet|Gantelet de griffe de braise|officielle|
|[weapon-05-enBDzoapUkvk4WVu.htm](equipment/weapon-05-enBDzoapUkvk4WVu.htm)|Thundercrasher|Coup de tonnerre|libre|
|[weapon-05-hK5xPjOFd2hb81mi.htm](equipment/weapon-05-hK5xPjOFd2hb81mi.htm)|Poisonous Dagger|Dague empoisonnée|libre|
|[weapon-05-mf5eeIdKjfSZ0K3p.htm](equipment/weapon-05-mf5eeIdKjfSZ0K3p.htm)|Solar Shellflower|Explosifleur solaire|libre|
|[weapon-05-NzVp3RKhBkAPbe3g.htm](equipment/weapon-05-NzVp3RKhBkAPbe3g.htm)|Metronomic Hammer|Marteau métronomique|libre|
|[weapon-05-ZvIEJCY60fHqzl6r.htm](equipment/weapon-05-ZvIEJCY60fHqzl6r.htm)|Serpent Dagger|Dague serpent|libre|
|[weapon-06-17Oza88ts0ASngdw.htm](equipment/weapon-06-17Oza88ts0ASngdw.htm)|Acrobat's Staff|Bâton de l'acrobate|libre|
|[weapon-06-39bdYkGlqjFR0AOL.htm](equipment/weapon-06-39bdYkGlqjFR0AOL.htm)|Obsidian Edge|Bord d'obsidienne|libre|
|[weapon-06-3qn56dTO5TYJHjbF.htm](equipment/weapon-06-3qn56dTO5TYJHjbF.htm)|Spike Launcher|Lanceur de piquants|libre|
|[weapon-06-4jZqYIyouoK7xe8u.htm](equipment/weapon-06-4jZqYIyouoK7xe8u.htm)|Dragonscale Staff|Bâton en écaille de dragon|libre|
|[weapon-06-4O00N0HoPz5ywNyz.htm](equipment/weapon-06-4O00N0HoPz5ywNyz.htm)|Staff of Protection|Bâton de protection|libre|
|[weapon-06-B71BgdfFApkYmAjc.htm](equipment/weapon-06-B71BgdfFApkYmAjc.htm)|Fluid Form Staff|Bâton de forme fluide|libre|
|[weapon-06-c40Zn2TCRr3inIBA.htm](equipment/weapon-06-c40Zn2TCRr3inIBA.htm)|Staff of Necromancy|Bâton de la mort|libre|
|[weapon-06-C7ca35oypTp2xJbj.htm](equipment/weapon-06-C7ca35oypTp2xJbj.htm)|Lady's Knife|Couteau de la dame|libre|
|[weapon-06-HVgolLqbXUeRHXVN.htm](equipment/weapon-06-HVgolLqbXUeRHXVN.htm)|Staff of Phantasms|Bâton de fantasme|libre|
|[weapon-06-hvMIJKY1mKXDmg1V.htm](equipment/weapon-06-hvMIJKY1mKXDmg1V.htm)|Windlass Bola|Bolas extensibles|libre|
|[weapon-06-iNC3uS24FSc40AQ4.htm](equipment/weapon-06-iNC3uS24FSc40AQ4.htm)|Staff of Metal|Bâton de métal|libre|
|[weapon-06-KXoJIPHbdz746Rec.htm](equipment/weapon-06-KXoJIPHbdz746Rec.htm)|Cooperative Blade|Lame coopérative|libre|
|[weapon-06-p1pysEaGypaNxTEL.htm](equipment/weapon-06-p1pysEaGypaNxTEL.htm)|Staff of Transmutation|Bâton de transmutation|officielle|
|[weapon-06-Q2QHZdBaoOLkE1lX.htm](equipment/weapon-06-Q2QHZdBaoOLkE1lX.htm)|Staff of Illusion|Bâton d'illusion|officielle|
|[weapon-06-rsqPU9IRM6tk2eHc.htm](equipment/weapon-06-rsqPU9IRM6tk2eHc.htm)|Chatterer of Follies|Bavardeur de folies|libre|
|[weapon-06-uhsVSBF4f3D5oym0.htm](equipment/weapon-06-uhsVSBF4f3D5oym0.htm)|Staff of the Unblinking Eye|Bâton de l'oeil qui ne cligne pas|libre|
|[weapon-06-uVcePmysXUFhOebP.htm](equipment/weapon-06-uVcePmysXUFhOebP.htm)|Soul Chain|Chaîne d'âme|libre|
|[weapon-06-WA5eoFFuAsyx7A2t.htm](equipment/weapon-06-WA5eoFFuAsyx7A2t.htm)|Staff of Impossible Visions|Bâton de visions impossibles|libre|
|[weapon-06-wUFQ8cqkwn2xTCma.htm](equipment/weapon-06-wUFQ8cqkwn2xTCma.htm)|Staff of Control|Bâton de contrôle|libre|
|[weapon-07-1IicJ7ESDNhGUqDE.htm](equipment/weapon-07-1IicJ7ESDNhGUqDE.htm)|Tentacle Cannon|Canon tentacule|libre|
|[weapon-07-8LEKZdpipKp3jYfx.htm](equipment/weapon-07-8LEKZdpipKp3jYfx.htm)|Liar's Gun|Fusil du menteur|libre|
|[weapon-07-atB2ewbcBMWvhjNT.htm](equipment/weapon-07-atB2ewbcBMWvhjNT.htm)|Guiding Star|Étoile guide|libre|
|[weapon-07-h7RtjyqHouB3pGgr.htm](equipment/weapon-07-h7RtjyqHouB3pGgr.htm)|Branch of the Great Sugi|Branche du grand sugi|libre|
|[weapon-07-HoxkzrS7jhqmWpla.htm](equipment/weapon-07-HoxkzrS7jhqmWpla.htm)|Beast Staff|Bâton de la bête|libre|
|[weapon-07-jwa10xel66Wc7U3p.htm](equipment/weapon-07-jwa10xel66Wc7U3p.htm)|Staff of Nature's Cunning|Bâton de la nature astucieuse|libre|
|[weapon-07-KoyxJDibsKJh24am.htm](equipment/weapon-07-KoyxJDibsKJh24am.htm)|Azarim|Azarim|libre|
|[weapon-07-KQRrAVdcRqtd0Lq2.htm](equipment/weapon-07-KQRrAVdcRqtd0Lq2.htm)|Fulminating Spear|Lance fulminante|libre|
|[weapon-07-QyzNxesxaSdPo3Pd.htm](equipment/weapon-07-QyzNxesxaSdPo3Pd.htm)|Alghollthu Lash|Fouet alghollthu|libre|
|[weapon-07-SNF8g7u8JYzqvsU1.htm](equipment/weapon-07-SNF8g7u8JYzqvsU1.htm)|Spirit Fan|Éventail spectral|libre|
|[weapon-07-UGqhitKOWCsqs9UQ.htm](equipment/weapon-07-UGqhitKOWCsqs9UQ.htm)|Bellicose Dagger|Dague belliqueuse|libre|
|[weapon-07-USzeQdvsXz6P5L2k.htm](equipment/weapon-07-USzeQdvsXz6P5L2k.htm)|Slime Whip|Fouet gluant|libre|
|[weapon-07-v6LpzpIA0BmKvEtK.htm](equipment/weapon-07-v6LpzpIA0BmKvEtK.htm)|Spellguard Blade|Lame contre-sort|libre|
|[weapon-08-2UX3tlC5vKeQjG7t.htm](equipment/weapon-08-2UX3tlC5vKeQjG7t.htm)|Spiritsight Crossbow|Arbalète de vision spirituelle|libre|
|[weapon-08-9GUSQFJZarLs5tQC.htm](equipment/weapon-08-9GUSQFJZarLs5tQC.htm)|Staff of Air (Greater)|Bâton de l'air supérieur|libre|
|[weapon-08-9jn87Bnp2kGnDFPQ.htm](equipment/weapon-08-9jn87Bnp2kGnDFPQ.htm)|Breath Blaster|Crache-souffle|libre|
|[weapon-08-BdNRM9VTxs5BYq0C.htm](equipment/weapon-08-BdNRM9VTxs5BYq0C.htm)|Jax|Jax|libre|
|[weapon-08-BJuqdqbflAcuCJkt.htm](equipment/weapon-08-BJuqdqbflAcuCJkt.htm)|Clockwork Macuahuitl|Macuahitl mécanique|libre|
|[weapon-08-CPZC4rAHGkQqPLJ9.htm](equipment/weapon-08-CPZC4rAHGkQqPLJ9.htm)|Atmospheric Staff|Bâton atmosphérique|libre|
|[weapon-08-dfheZ5lK0thQUTVT.htm](equipment/weapon-08-dfheZ5lK0thQUTVT.htm)|Flaying Knife|Couteau à écorcher|libre|
|[weapon-08-DwNELc8YIKTb2uj1.htm](equipment/weapon-08-DwNELc8YIKTb2uj1.htm)|Staff of Water (Greater)|Bâton de l'eau supérieur|libre|
|[weapon-08-FCA7CE4mK85SVLz3.htm](equipment/weapon-08-FCA7CE4mK85SVLz3.htm)|Eclipse|Éclipse|officielle|
|[weapon-08-GFwfZrBGjCZTDcuw.htm](equipment/weapon-08-GFwfZrBGjCZTDcuw.htm)|Vampire-Fang Morningstar|Morgenstern croc-de-vampire|libre|
|[weapon-08-jaM12dWpIozRiIhk.htm](equipment/weapon-08-jaM12dWpIozRiIhk.htm)|Staff of Earth (Greater)|Bâton de la terre supérieur|libre|
|[weapon-08-JLoMkMq727XeMmlR.htm](equipment/weapon-08-JLoMkMq727XeMmlR.htm)|Vine Whip|Fouet-liane|libre|
|[weapon-08-KSHZBPFxPD6uCZXM.htm](equipment/weapon-08-KSHZBPFxPD6uCZXM.htm)|Composer Staff (Greater)|Bâton du compositeur supérieur|libre|
|[weapon-08-O5da0gpQqR9MwGLF.htm](equipment/weapon-08-O5da0gpQqR9MwGLF.htm)|Mindlance|Arquebuse dissuasive|libre|
|[weapon-08-UEIwFJMx3wYt0Blp.htm](equipment/weapon-08-UEIwFJMx3wYt0Blp.htm)|Spore Shephard's Staff (Greater)|Bâton du pâtre des spores supérieur|libre|
|[weapon-08-XSwEE8wjHr6UXzpw.htm](equipment/weapon-08-XSwEE8wjHr6UXzpw.htm)|Staff of Healing (Greater)|Bâton de guérison supérieur|libre|
|[weapon-08-zakdImp4CqahnFfc.htm](equipment/weapon-08-zakdImp4CqahnFfc.htm)|Habu's Cudgel|Gourdin d'Habu|libre|
|[weapon-09-8pFegnc8KTHsvQZ0.htm](equipment/weapon-09-8pFegnc8KTHsvQZ0.htm)|Vashu's Ninth Life|Neuvième vie de Vashu|libre|
|[weapon-09-Il75ytwHrdwAAOwe.htm](equipment/weapon-09-Il75ytwHrdwAAOwe.htm)|Reaper's Crescent|Croissant du faucheur|libre|
|[weapon-09-PVdl7StKFu6QPTVl.htm](equipment/weapon-09-PVdl7StKFu6QPTVl.htm)|Screech Shooter|Lance hurlement|libre|
|[weapon-09-ssS1yEAZwZun8yFw.htm](equipment/weapon-09-ssS1yEAZwZun8yFw.htm)|Growth Gun|Fusil de croissance|libre|
|[weapon-09-WFumHA7dT5s3jCRT.htm](equipment/weapon-09-WFumHA7dT5s3jCRT.htm)|Nexian Sealing Blade|Lame scellante nexienne|libre|
|[weapon-09-XVxnCBQEW5idfAPw.htm](equipment/weapon-09-XVxnCBQEW5idfAPw.htm)|Obsidian Edge (Greater)|Bord d'obsidienne supérieur|libre|
|[weapon-09-yHtlXoXKPis7eTR0.htm](equipment/weapon-09-yHtlXoXKPis7eTR0.htm)|Chaplain's Cudgel|Bourdon de l'aumônier|libre|
|[weapon-10-4nte7LbGGPwViu1U.htm](equipment/weapon-10-4nte7LbGGPwViu1U.htm)|North Wind's Night Verse|Verset de la nuit du vent du nord|libre|
|[weapon-10-5SVttGODx1TkyhTU.htm](equipment/weapon-10-5SVttGODx1TkyhTU.htm)|Immolation Clan Pistol|Pistolet de clan d'immolation|libre|
|[weapon-10-66Rc9QH11IpZOwZp.htm](equipment/weapon-10-66Rc9QH11IpZOwZp.htm)|Staff of the Dead (Greater)|Bâton de la mort supérieur|libre|
|[weapon-10-8S81RvynBS09rIOF.htm](equipment/weapon-10-8S81RvynBS09rIOF.htm)|South Wind's Scorch Song|Chanson brûlante du vent du sud|libre|
|[weapon-10-Ab5EjJyoRSec5YrW.htm](equipment/weapon-10-Ab5EjJyoRSec5YrW.htm)|Staff of the Dead (Greater)|Bâton de la mort supérieur|libre|
|[weapon-10-bQynfb23iexSu8zU.htm](equipment/weapon-10-bQynfb23iexSu8zU.htm)|Fluid Form Staff (Greater)|Bâton de forme fluide supérieur|libre|
|[weapon-10-d5dnhOf5RHFFnsiB.htm](equipment/weapon-10-d5dnhOf5RHFFnsiB.htm)|Staff of Control (Greater)|Bâton de contrôle supérieur|libre|
|[weapon-10-ER9gH5A7elGPdHag.htm](equipment/weapon-10-ER9gH5A7elGPdHag.htm)|Grounding Spike|Pique de mise à la terre|libre|
|[weapon-10-jUL4ZdztH8VODBuv.htm](equipment/weapon-10-jUL4ZdztH8VODBuv.htm)|Staff of the Unblinking Eye (Greater)|Bâton de l'oeil qui ne cligne pas supérieur|libre|
|[weapon-10-n4p8VFMKYACf200W.htm](equipment/weapon-10-n4p8VFMKYACf200W.htm)|Polarizing Mace|Masse d'armes polarisante|libre|
|[weapon-10-q1mt1F9q1XmJWAKY.htm](equipment/weapon-10-q1mt1F9q1XmJWAKY.htm)|Pact-Bound Pistol|Pistolet du pacte|libre|
|[weapon-10-qvl1OmV5F60ZOdcC.htm](equipment/weapon-10-qvl1OmV5F60ZOdcC.htm)|Staff of Metal (Greater)|Bâton de métal supérieur|libre|
|[weapon-10-rnoP6uPxauJiSGPK.htm](equipment/weapon-10-rnoP6uPxauJiSGPK.htm)|Rustbringer|Rouilleur|libre|
|[weapon-10-Svq02kCaFadVPOSq.htm](equipment/weapon-10-Svq02kCaFadVPOSq.htm)|Staff of Phantasms (Greater)|Bâton de fantasmes supérieur|libre|
|[weapon-10-XE8AxkahYPVzeLCW.htm](equipment/weapon-10-XE8AxkahYPVzeLCW.htm)|Staff of Protection (Greater)|Bâton de protection supérieur|libre|
|[weapon-10-YxLC2Np7cEBnZzGX.htm](equipment/weapon-10-YxLC2Np7cEBnZzGX.htm)|Staff of Transmutation (Greater)|Bâton de transmutation supérieur|officielle|
|[weapon-11-0liiZqvhcM69tKl9.htm](equipment/weapon-11-0liiZqvhcM69tKl9.htm)|Carver-cutter|Graveur coupeur|libre|
|[weapon-11-0MPBjp2lOYVCsgeg.htm](equipment/weapon-11-0MPBjp2lOYVCsgeg.htm)|Vexing Vapor (Greater)|Vapeur contrariante supérieure|libre|
|[weapon-11-2GngLv6m3uHObFGZ.htm](equipment/weapon-11-2GngLv6m3uHObFGZ.htm)|Sulfur Bomb (Greater)|Bombe de soufre supérieure|libre|
|[weapon-11-4DnjM0PNivT2dlf9.htm](equipment/weapon-11-4DnjM0PNivT2dlf9.htm)|Hunter's Anthem|Anathème du chasseur|libre|
|[weapon-11-5IUyrJItXkMASgmq.htm](equipment/weapon-11-5IUyrJItXkMASgmq.htm)|Tallow Bomb (Greater)|Bombe de suif supérieure|libre|
|[weapon-11-5Qkz4RVJr2Kx3RL6.htm](equipment/weapon-11-5Qkz4RVJr2Kx3RL6.htm)|Glue Bomb (Greater)|Bombe collante supérieure|libre|
|[weapon-11-73JDNoUQPmUqjz97.htm](equipment/weapon-11-73JDNoUQPmUqjz97.htm)|Shattered Plan|Plan fracassé|libre|
|[weapon-11-7haFJ3s6G6K1TQFj.htm](equipment/weapon-11-7haFJ3s6G6K1TQFj.htm)|Gloaming Arc|Croissant lugubre|libre|
|[weapon-11-7WwKI53IcXVIHbkh.htm](equipment/weapon-11-7WwKI53IcXVIHbkh.htm)|Skunk Bomb (Greater)|Bombe pestilentielle supérieure|libre|
|[weapon-11-9j3sqH9sG5mUe6rI.htm](equipment/weapon-11-9j3sqH9sG5mUe6rI.htm)|Mud Bomb (Greater)|Bombe de boue supérieure|libre|
|[weapon-11-aBfyzL0WBFMBAbuI.htm](equipment/weapon-11-aBfyzL0WBFMBAbuI.htm)|Buzzsaw Axe|Hache tourbillonnante|libre|
|[weapon-11-cVUhP1qWxe9USvVp.htm](equipment/weapon-11-cVUhP1qWxe9USvVp.htm)|Sticky Algae Bomb (Greater)|Bombe d'algues collantes supérieure|libre|
|[weapon-11-DheIz1WCPXwUhxQD.htm](equipment/weapon-11-DheIz1WCPXwUhxQD.htm)|Pressure Bomb (Greater)|Bombe à pression supérieure|libre|
|[weapon-11-dsMkvuLgpOOGLWDy.htm](equipment/weapon-11-dsMkvuLgpOOGLWDy.htm)|Thunderstone (Greater)|Pierre à tonnerre supérieure|officielle|
|[weapon-11-HKcfpawD4CesJtFy.htm](equipment/weapon-11-HKcfpawD4CesJtFy.htm)|Hex Blaster|Cracheur de maléfice|libre|
|[weapon-11-HWVORFebjWAPQ2NI.htm](equipment/weapon-11-HWVORFebjWAPQ2NI.htm)|Gloaming Shard|Éclat du crépuscule|libre|
|[weapon-11-intVPcEzbE9o4NQd.htm](equipment/weapon-11-intVPcEzbE9o4NQd.htm)|Frost Vial (Greater)|Fiole de givre supérieure|officielle|
|[weapon-11-itn6l0yJGRw7EFwu.htm](equipment/weapon-11-itn6l0yJGRw7EFwu.htm)|Water Bomb (Greater)|Bombe à eau supérieure|libre|
|[weapon-11-kP51e5Ul3Q1fusSg.htm](equipment/weapon-11-kP51e5Ul3Q1fusSg.htm)|Dezullon Fountain|Fontaine dézullone|libre|
|[weapon-11-kuHr6dEOxN0WAnUP.htm](equipment/weapon-11-kuHr6dEOxN0WAnUP.htm)|Blood Bomb (Greater)|Bombe hémorragique supérieure|libre|
|[weapon-11-LhMXIGelSrFPYild.htm](equipment/weapon-11-LhMXIGelSrFPYild.htm)|Blight Bomb (Greater)|Bombe fléau supérieure|libre|
|[weapon-11-n5yTlTs63o53n3k6.htm](equipment/weapon-11-n5yTlTs63o53n3k6.htm)|Alchemist's Fire (Greater)|Feu grégeois supérieur|libre|
|[weapon-11-QhUTsTnbehv1ft9u.htm](equipment/weapon-11-QhUTsTnbehv1ft9u.htm)|Junk Bomb (Greater)|Bombe à fragmentation supérieure|libre|
|[weapon-11-QT1H3f9u44IZ3TmT.htm](equipment/weapon-11-QT1H3f9u44IZ3TmT.htm)|Scalding Gauntlets|Gantelets échaudeurs|libre|
|[weapon-11-r2iTRbt1zpkAqHj2.htm](equipment/weapon-11-r2iTRbt1zpkAqHj2.htm)|Bottled Lightning (Greater)|Foudre en bouteille supérieure|officielle|
|[weapon-11-SAtG9jrqey2y8EGb.htm](equipment/weapon-11-SAtG9jrqey2y8EGb.htm)|Goo Grenade (Greater)|Grenade gluante supérieure|libre|
|[weapon-11-tE2IarA29mYsgrxj.htm](equipment/weapon-11-tE2IarA29mYsgrxj.htm)|Spore Sap|Matraque de spores|libre|
|[weapon-11-TvEnLYsYjijBe1Cg.htm](equipment/weapon-11-TvEnLYsYjijBe1Cg.htm)|Pernicious Spore Bomb (Greater)|Bombe de spores pernicieuses supérieure|libre|
|[weapon-11-ud3pqCquYA5UecaS.htm](equipment/weapon-11-ud3pqCquYA5UecaS.htm)|Stoneraiser Javelin|Javelot dresseur de pierres|libre|
|[weapon-11-W1XG0qStVh8fmeJ6.htm](equipment/weapon-11-W1XG0qStVh8fmeJ6.htm)|Star Grenade (Greater)|Grenade étoilée supérieure|libre|
|[weapon-11-xt4sb8vz8VRlJoKm.htm](equipment/weapon-11-xt4sb8vz8VRlJoKm.htm)|Staff of Nature's Cunning (Greater)|Bâton de la nature astucieuse supérieur|libre|
|[weapon-11-yq5BoByEscKRZsto.htm](equipment/weapon-11-yq5BoByEscKRZsto.htm)|Twigjack Sack (Greater)|Sac de ronces supérieur|libre|
|[weapon-11-ZbZMaY8B8dinYhHK.htm](equipment/weapon-11-ZbZMaY8B8dinYhHK.htm)|Lionfish Spear|Lance poisson lion|libre|
|[weapon-11-ZVhSxudgfnSO4AMd.htm](equipment/weapon-11-ZVhSxudgfnSO4AMd.htm)|Peshpine Grenade (Greater)|Grenades d'épines de pesh supérieure|libre|
|[weapon-12-0mCj6HZcwFzVxVyM.htm](equipment/weapon-12-0mCj6HZcwFzVxVyM.htm)|Staff of Arcane Might|Bâton de puissance arcanique|libre|
|[weapon-12-2wUR0XVYONWrBVa8.htm](equipment/weapon-12-2wUR0XVYONWrBVa8.htm)|Staff of Air (Major)|Bâton de l'air majeur|libre|
|[weapon-12-3oexArva2aEm69WV.htm](equipment/weapon-12-3oexArva2aEm69WV.htm)|Four-Ways Dogslicer|Tranchechien quadruple|libre|
|[weapon-12-67pe75mLVekaVqZH.htm](equipment/weapon-12-67pe75mLVekaVqZH.htm)|Aether Marbles (Moderate)|Billes éthérées modérées|libre|
|[weapon-12-a8YCTjKnXySKpU5C.htm](equipment/weapon-12-a8YCTjKnXySKpU5C.htm)|Staff of Earth (Major)|Bâton de la terre majeur|libre|
|[weapon-12-DBQ68Evl8PdLAVmt.htm](equipment/weapon-12-DBQ68Evl8PdLAVmt.htm)|Lodestone Bomb|Bombe aimantée|libre|
|[weapon-12-IKuvON8e4o7ARe4p.htm](equipment/weapon-12-IKuvON8e4o7ARe4p.htm)|Gambler's Staff|Bâton du joueur|libre|
|[weapon-12-ivaL0xt33k6QNwQK.htm](equipment/weapon-12-ivaL0xt33k6QNwQK.htm)|Crystal Shards (Greater)|Éclats de cristal supérieurs|libre|
|[weapon-12-kG26J0AfQAVe9FYs.htm](equipment/weapon-12-kG26J0AfQAVe9FYs.htm)|Silver Orb (Powder)|Orbe d'argent|libre|
|[weapon-12-KRQDhu2XVmOlaVRu.htm](equipment/weapon-12-KRQDhu2XVmOlaVRu.htm)|Obsidian Edge (Major)|Bord d'obsidienne majeur|libre|
|[weapon-12-lBITo8JcWfGxKmYT.htm](equipment/weapon-12-lBITo8JcWfGxKmYT.htm)|Spellsap Grenade|Grenade pompesort|libre|
|[weapon-12-MSzFbb4Jswvrqjru.htm](equipment/weapon-12-MSzFbb4Jswvrqjru.htm)|Pirate Staff|Bâton du pirate|libre|
|[weapon-12-pBPVm5JjKfKX8gaX.htm](equipment/weapon-12-pBPVm5JjKfKX8gaX.htm)|Composer Staff (Major)|Bâton du compositeur majeur|libre|
|[weapon-12-PNs3SqMhJ3rTxpSI.htm](equipment/weapon-12-PNs3SqMhJ3rTxpSI.htm)|Trueshape Bomb|Bombe de forme véritable|libre|
|[weapon-12-QMqPGk7oL4kESGc3.htm](equipment/weapon-12-QMqPGk7oL4kESGc3.htm)|Staff of Water (Major)|Bâton de l'eau majeur|libre|
|[weapon-12-rmGfBLRTfOqhwhHW.htm](equipment/weapon-12-rmGfBLRTfOqhwhHW.htm)|Socialite Staff|Bâton mondain|libre|
|[weapon-12-sBtfUPTLuyRPYI7g.htm](equipment/weapon-12-sBtfUPTLuyRPYI7g.htm)|Shatterstone|Pierre fracas|libre|
|[weapon-12-sjOkiftTv4KbR2c0.htm](equipment/weapon-12-sjOkiftTv4KbR2c0.htm)|Boulder Seed|Graine de rocher|libre|
|[weapon-12-SKKVjloic8hQbYo7.htm](equipment/weapon-12-SKKVjloic8hQbYo7.htm)|Atmospheric Staff (Greater)|Bâton atmosphérique supérieur|libre|
|[weapon-12-tWoW1BeFrWm35hmV.htm](equipment/weapon-12-tWoW1BeFrWm35hmV.htm)|Blink Blade|Lame clignotante|libre|
|[weapon-12-tYnFIqjvxT9bWaoR.htm](equipment/weapon-12-tYnFIqjvxT9bWaoR.htm)|Mammoth Bow|Arc mammouth|libre|
|[weapon-12-YCGMVbqlWT8f1F6v.htm](equipment/weapon-12-YCGMVbqlWT8f1F6v.htm)|Tidal Fishhook|Hameçon taldorien|libre|
|[weapon-12-zzLGE9zmSu3yMEOq.htm](equipment/weapon-12-zzLGE9zmSu3yMEOq.htm)|Beast Staff (Greater)|Bâton de la bête supérieure|libre|
|[weapon-13-1lZNaqqGOrza9BsG.htm](equipment/weapon-13-1lZNaqqGOrza9BsG.htm)|Screech Shooter (Greater)|Lance hurlement supérieur|libre|
|[weapon-13-297RUMeetOWyciDe.htm](equipment/weapon-13-297RUMeetOWyciDe.htm)|Vampiric Scythe|Faux vampirique|libre|
|[weapon-13-A6Bavci9ngWPln4l.htm](equipment/weapon-13-A6Bavci9ngWPln4l.htm)|Piston Gauntlets|Gantelets à piston|libre|
|[weapon-13-CH2t4lYE6IbE3F3Q.htm](equipment/weapon-13-CH2t4lYE6IbE3F3Q.htm)|Splintering Spear|Lance à échardes|libre|
|[weapon-13-EmTK4nPOORXtb58X.htm](equipment/weapon-13-EmTK4nPOORXtb58X.htm)|Knight Captain's Lance|Lance du capitaine des chevaliers|libre|
|[weapon-13-G3a60Ug3YNCMyMVR.htm](equipment/weapon-13-G3a60Ug3YNCMyMVR.htm)|Searing Blade|Lame brûlante|libre|
|[weapon-13-kSLHMQjKL77CtvBx.htm](equipment/weapon-13-kSLHMQjKL77CtvBx.htm)|Anchor Spear|Lance-ancre|libre|
|[weapon-13-lenQyWTYsMrcC3Vg.htm](equipment/weapon-13-lenQyWTYsMrcC3Vg.htm)|Scalding Gauntlets (Greater)|Gantelets échaudeurs supérieurs|libre|
|[weapon-13-MNx8UznusoyRy06k.htm](equipment/weapon-13-MNx8UznusoyRy06k.htm)|Carver-cutter (Greater)|Graveur coupeur supérieur|libre|
|[weapon-13-SQrkh42q3EYgygx8.htm](equipment/weapon-13-SQrkh42q3EYgygx8.htm)|Dragontooth Club|Gourdin dent de dragon|libre|
|[weapon-13-U8P9aRhjFpptHJCd.htm](equipment/weapon-13-U8P9aRhjFpptHJCd.htm)|Tentacle Cannon (Greater)|Canon tentacule supérieur|libre|
|[weapon-13-XK4DM8wOtcuOsji6.htm](equipment/weapon-13-XK4DM8wOtcuOsji6.htm)|Dwarven Thrower|Marteau de lancer nain|officielle|
|[weapon-13-ZuyzSxmg4QdnZqLy.htm](equipment/weapon-13-ZuyzSxmg4QdnZqLy.htm)|Ankylostar|Ankylostern|libre|
|[weapon-14-4UHTZGZosYcP28lD.htm](equipment/weapon-14-4UHTZGZosYcP28lD.htm)|Breath Blaster (Greater)|Crache-souffle supérieur|libre|
|[weapon-14-7RBY90RmMtZjvnGW.htm](equipment/weapon-14-7RBY90RmMtZjvnGW.htm)|Staff of Phantasms (Major)|Bâton de fantasmes majeur|libre|
|[weapon-14-8FKxOiKAoQqPiqPA.htm](equipment/weapon-14-8FKxOiKAoQqPiqPA.htm)|Staff of Metal (Major)|Bâton de métal majeur|libre|
|[weapon-14-APjEozp9ibSFV7zM.htm](equipment/weapon-14-APjEozp9ibSFV7zM.htm)|Staff of the Unblinking Eye (Major)|Bâton de l'oeil qui ne cligne pas majeur|libre|
|[weapon-14-cnfsOL7Pes47WPpR.htm](equipment/weapon-14-cnfsOL7Pes47WPpR.htm)|Nexian Sealing Blade (Greater)|Lame scellante nexienne supérieure|libre|
|[weapon-14-IIV5L4y8zLH17mAK.htm](equipment/weapon-14-IIV5L4y8zLH17mAK.htm)|Staff of Necromancy (Major)|Bâton de la mort majeur|libre|
|[weapon-14-J5It6yq3ZkN059zP.htm](equipment/weapon-14-J5It6yq3ZkN059zP.htm)|Glaive of the Artist|Coutille de l'artiste|libre|
|[weapon-14-mkFrHOwWJaHF0aGp.htm](equipment/weapon-14-mkFrHOwWJaHF0aGp.htm)|Stargazer|Astronome|libre|
|[weapon-14-N7cH1SS57NNaOtf5.htm](equipment/weapon-14-N7cH1SS57NNaOtf5.htm)|Ouroboros Flail|Fléau ouroboros|libre|
|[weapon-14-ndF4yxyM5ci1DcrO.htm](equipment/weapon-14-ndF4yxyM5ci1DcrO.htm)|Staff of Control (Major)|Bâton de contrôle majeur|libre|
|[weapon-14-OIs6WPuCRh2UJTOe.htm](equipment/weapon-14-OIs6WPuCRh2UJTOe.htm)|Holy Avenger|Épée de la justice|officielle|
|[weapon-14-qgpufYE8gHRWUPiQ.htm](equipment/weapon-14-qgpufYE8gHRWUPiQ.htm)|Songbird's Brush|Pinceau de l'oiseau chanteur|libre|
|[weapon-14-qtlkftQXfO4wuCVu.htm](equipment/weapon-14-qtlkftQXfO4wuCVu.htm)|Hardened Harrow Deck|Jeu du Tourment renforcé|libre|
|[weapon-14-R4nk2W7rTvMDqyUR.htm](equipment/weapon-14-R4nk2W7rTvMDqyUR.htm)|Lionfish Spear (Greater)|Lance poisson lion supérieure|libre|
|[weapon-14-SatFUtE2UaPvC394.htm](equipment/weapon-14-SatFUtE2UaPvC394.htm)|Skyrider Sword|Épée du chevaucheur de ciel|libre|
|[weapon-14-tEI1WOo4ZRJ5cI0K.htm](equipment/weapon-14-tEI1WOo4ZRJ5cI0K.htm)|Dragonprism Staff|Bâton dracoprismatique|libre|
|[weapon-14-TLLZIFIPbhW8kjyL.htm](equipment/weapon-14-TLLZIFIPbhW8kjyL.htm)|Staff of Protection (Major)|Bâton de protection majeur|libre|
|[weapon-14-tVcI2ypPhfhchwMp.htm](equipment/weapon-14-tVcI2ypPhfhchwMp.htm)|Singing Shortbow|Arc court chantant|libre|
|[weapon-15-1yZRZeVAVxrb5HM2.htm](equipment/weapon-15-1yZRZeVAVxrb5HM2.htm)|Staff of Nature's Cunning (Major)|Bâton de la nature astucieuse majeur|libre|
|[weapon-15-APZJC0A469gBozf1.htm](equipment/weapon-15-APZJC0A469gBozf1.htm)|Blade of the Rabbit Prince|Lame du prince lapin|libre|
|[weapon-15-aYFelpLdqrvZMx2Q.htm](equipment/weapon-15-aYFelpLdqrvZMx2Q.htm)|Godsbreath Bow|Arc souffle de dieu|libre|
|[weapon-15-ChLxhFpo4WSAKgLh.htm](equipment/weapon-15-ChLxhFpo4WSAKgLh.htm)|Petrification Cannon|Canon de pétrification|libre|
|[weapon-15-f0R9al9iggvAQ59p.htm](equipment/weapon-15-f0R9al9iggvAQ59p.htm)|Scalding Gauntlets (Major)|Gantelets échaudeurs majeurs|libre|
|[weapon-15-IuL8O6sR8st1mevo.htm](equipment/weapon-15-IuL8O6sR8st1mevo.htm)|Mountebank's Passage|Passage de Mountebanque|libre|
|[weapon-15-kIExO59KkiXZUmto.htm](equipment/weapon-15-kIExO59KkiXZUmto.htm)|Blightburn Bomb|Bombe fléau brûlant|libre|
|[weapon-15-mXlVJIxv6EpzwTJs.htm](equipment/weapon-15-mXlVJIxv6EpzwTJs.htm)|Buzzsaw Axe (Greater)|Hache tourbillonnante supérieure|libre|
|[weapon-15-O5PgCWNhFtTFL7S5.htm](equipment/weapon-15-O5PgCWNhFtTFL7S5.htm)|Carver-cutter (Major)|Graveur coupeur majeur|libre|
|[weapon-15-UmteC2OgKGJBkMDz.htm](equipment/weapon-15-UmteC2OgKGJBkMDz.htm)|Mindrender Baton|Bâtonnet éventresprit|libre|
|[weapon-16-87IvbaQnkUOBgdD0.htm](equipment/weapon-16-87IvbaQnkUOBgdD0.htm)|Frost Brand|Épée de givre|officielle|
|[weapon-16-L7FuscHIuzQ7FjnB.htm](equipment/weapon-16-L7FuscHIuzQ7FjnB.htm)|Staff of Arcane Might (Greater)|Bâton de puissance arcanique supérieur|libre|
|[weapon-16-NARrXbTyQ2X6hA7e.htm](equipment/weapon-16-NARrXbTyQ2X6hA7e.htm)|Icicle|Stalactite de glace|libre|
|[weapon-16-nz8Cx3SiRWANdlJE.htm](equipment/weapon-16-nz8Cx3SiRWANdlJE.htm)|Rowan Rifle|Fusil de Rowan|libre|
|[weapon-16-YMz8ZKeOmSU9VuKp.htm](equipment/weapon-16-YMz8ZKeOmSU9VuKp.htm)|Atmospheric Staff (Major)|Bâton atmosphérique majeur|libre|
|[weapon-17-1AHntZrWzp5e31SX.htm](equipment/weapon-17-1AHntZrWzp5e31SX.htm)|Gearblade|Lame mécanique|libre|
|[weapon-17-3SZAUzXG6MWYWoXZ.htm](equipment/weapon-17-3SZAUzXG6MWYWoXZ.htm)|Blood Bomb (Major)|Bombe hémorragique majeure|libre|
|[weapon-17-4DJQID8GIlxQ7b9C.htm](equipment/weapon-17-4DJQID8GIlxQ7b9C.htm)|Frost Vial (Major)|Fiole de givre majeure|officielle|
|[weapon-17-4Thv3O2smZ8AHNjP.htm](equipment/weapon-17-4Thv3O2smZ8AHNjP.htm)|Pernicious Spore Bomb (Major)|Bombe de spores pernicieuses majeure|libre|
|[weapon-17-8h5UFFk5EodKZh19.htm](equipment/weapon-17-8h5UFFk5EodKZh19.htm)|Twigjack Sack (Major)|Sac de ronces majeur|libre|
|[weapon-17-AAoAQCBwAITvHJSu.htm](equipment/weapon-17-AAoAQCBwAITvHJSu.htm)|Sticky Algae Bomb (Major)|Bombe d'algues collantes majeure|libre|
|[weapon-17-b1gx7ZhuFJAyJOo8.htm](equipment/weapon-17-b1gx7ZhuFJAyJOo8.htm)|Peshpine Grenade (Major)|Grenade d'épines de pesh majeure|libre|
|[weapon-17-BxJNWWvpxacDIsdt.htm](equipment/weapon-17-BxJNWWvpxacDIsdt.htm)|Glue Bomb (Major)|Bombe collante majeure|libre|
|[weapon-17-cLZrOihPht5M7eTw.htm](equipment/weapon-17-cLZrOihPht5M7eTw.htm)|Alchemist's Fire (Major)|Feu grégeois majeur|libre|
|[weapon-17-Djp91D21I6QW6dlw.htm](equipment/weapon-17-Djp91D21I6QW6dlw.htm)|Mud Bomb (Major)|Bombe de boue majeure|libre|
|[weapon-17-Dv1bozSbR39McpWd.htm](equipment/weapon-17-Dv1bozSbR39McpWd.htm)|Skunk Bomb (Major)|Bombe pestilentielle majeure|libre|
|[weapon-17-EjJUCXG4Yr3T6DTM.htm](equipment/weapon-17-EjJUCXG4Yr3T6DTM.htm)|Water Bomb (Major)|Bombe à eau majeure|libre|
|[weapon-17-Fye3IZN9FVDYm4KJ.htm](equipment/weapon-17-Fye3IZN9FVDYm4KJ.htm)|Rustbringer (Greater)|Rouilleur supérieur|libre|
|[weapon-17-IuGydh0En8LbfnWo.htm](equipment/weapon-17-IuGydh0En8LbfnWo.htm)|Thunderstone (Major)|Pierre à tonnerre majeure|officielle|
|[weapon-17-K07yZnXG1DQwFjX7.htm](equipment/weapon-17-K07yZnXG1DQwFjX7.htm)|Splintering Spear (Greater)|Lance à échardes supérieure|libre|
|[weapon-17-K291ECjrRssmH93Q.htm](equipment/weapon-17-K291ECjrRssmH93Q.htm)|Junk Bomb (Major)|Bombe à fragmentation majeure|libre|
|[weapon-17-l29fzEJ6SSIP2wJn.htm](equipment/weapon-17-l29fzEJ6SSIP2wJn.htm)|Tallow Bomb (Major)|Bombe de suif majeure|libre|
|[weapon-17-Otd9pV58s2LFK1gW.htm](equipment/weapon-17-Otd9pV58s2LFK1gW.htm)|Pressure Bomb (Major)|Bombe à pression majeure|libre|
|[weapon-17-pIYlenaADKnxdp11.htm](equipment/weapon-17-pIYlenaADKnxdp11.htm)|Luck Blade|Lame chanceuse|officielle|
|[weapon-17-PkqzU1Ap3O3IicUv.htm](equipment/weapon-17-PkqzU1Ap3O3IicUv.htm)|Star Grenade (Major)|Grenade étoilée majeure|libre|
|[weapon-17-pvxRcuBexbFawjCg.htm](equipment/weapon-17-pvxRcuBexbFawjCg.htm)|Searing Blade (Greater)|Lame brûlante supérieure|libre|
|[weapon-17-rKCiDIoozZTkyE4j.htm](equipment/weapon-17-rKCiDIoozZTkyE4j.htm)|Tentacle Cannon (Major)|Canon tentacule majeur|libre|
|[weapon-17-rpbbfkexLhtadBDV.htm](equipment/weapon-17-rpbbfkexLhtadBDV.htm)|Bottled Lightning (Major)|Foudre en bouteille majeure|officielle|
|[weapon-17-RXpM5Kbuf3BYfK6f.htm](equipment/weapon-17-RXpM5Kbuf3BYfK6f.htm)|Vexing Vapor (Major)|Vapeur contrariante majeure|libre|
|[weapon-17-sJjuv1991SZ7DWWD.htm](equipment/weapon-17-sJjuv1991SZ7DWWD.htm)|Blight Bomb (Major)|Bombe fléau majeure|libre|
|[weapon-17-sLuU3V5Hv9bYrl2a.htm](equipment/weapon-17-sLuU3V5Hv9bYrl2a.htm)|Ouroboros Flail (Greater)|Fléau ouroboros supérieur|libre|
|[weapon-17-v9smWWOsWFwhCZoB.htm](equipment/weapon-17-v9smWWOsWFwhCZoB.htm)|Screech Shooter (Major)|Lance hurlement majeur|libre|
|[weapon-17-W0RWd2E0jkF2ZXhf.htm](equipment/weapon-17-W0RWd2E0jkF2ZXhf.htm)|Goo Grenade (Major)|Grenade gluante majeure|libre|
|[weapon-17-XBn3aapPC0x7X13W.htm](equipment/weapon-17-XBn3aapPC0x7X13W.htm)|Chronomancer Staff|Bâton du chronomancien|libre|
|[weapon-17-yPndyLkx3Nj2GMiz.htm](equipment/weapon-17-yPndyLkx3Nj2GMiz.htm)|Sulfur Bomb (Major)|Bombe de soufre majeure|libre|
|[weapon-18-5XImTvAN58GhnNIE.htm](equipment/weapon-18-5XImTvAN58GhnNIE.htm)|Aether Marbles (Greater)|Billes éthérées supérieures|libre|
|[weapon-18-8W8NvYlRM2lQPJf0.htm](equipment/weapon-18-8W8NvYlRM2lQPJf0.htm)|Obsidian Edge (True)|Bord d'obsidienne ultime|libre|
|[weapon-18-APJ0NWJ4DYUXNpNm.htm](equipment/weapon-18-APJ0NWJ4DYUXNpNm.htm)|Lodestone Bomb (Greater)|Bombe aimantée supérieure|libre|
|[weapon-18-CJesyj3lklJVujh0.htm](equipment/weapon-18-CJesyj3lklJVujh0.htm)|Shatterstone (Greater)|Pierre fracas supérieure|libre|
|[weapon-18-ePmUVkh4NgM4eGdR.htm](equipment/weapon-18-ePmUVkh4NgM4eGdR.htm)|Carver-cutter (True)|Graveur coupeur ultime|libre|
|[weapon-18-OHwsumgfO1oyH7KL.htm](equipment/weapon-18-OHwsumgfO1oyH7KL.htm)|Boulder Seed (Greater)|Graine de rocher supérieure|libre|
|[weapon-18-ry4In8lT0xB8Dc17.htm](equipment/weapon-18-ry4In8lT0xB8Dc17.htm)|Spellsap Grenade (Greater)|Grenade pompesort supérieure|libre|
|[weapon-18-trs1C4DEkETh9bio.htm](equipment/weapon-18-trs1C4DEkETh9bio.htm)|Scalding Gauntlets (True)|Gantelets échaudeurs ultimes|libre|
|[weapon-18-ul4LuDff24Kz9n7e.htm](equipment/weapon-18-ul4LuDff24Kz9n7e.htm)|Singing Shortbow (Greater)|Arc court chantant supérieur|libre|
|[weapon-18-V2knI4GpdJYLupjg.htm](equipment/weapon-18-V2knI4GpdJYLupjg.htm)|Crystal Shards (Major)|Éclats de cristal majeurs|libre|
|[weapon-18-VwNmUeRHomup8VoI.htm](equipment/weapon-18-VwNmUeRHomup8VoI.htm)|Shadow's Heart|Coeur d'ombre|libre|
|[weapon-18-WnzQy5naKy9Gj4pY.htm](equipment/weapon-18-WnzQy5naKy9Gj4pY.htm)|Breath Blaster (Major)|Crache-souffle majeur|libre|
|[weapon-18-wtRDteH3D3ME4FU9.htm](equipment/weapon-18-wtRDteH3D3ME4FU9.htm)|Trueshape Bomb (Greater)|Bombe de forme véritable supérieure|libre|
|[weapon-18-YBXcse5TSHyv9rvX.htm](equipment/weapon-18-YBXcse5TSHyv9rvX.htm)|Silver Orb (Greater)|Orbe en argent supérieur|libre|
|[weapon-19-C3vAFRWoeGbMQTAH.htm](equipment/weapon-19-C3vAFRWoeGbMQTAH.htm)|Luck Blade (Wishing)|Lame chanceuse de souhait|officielle|
|[weapon-19-jBn5TdnxOvzf6a1F.htm](equipment/weapon-19-jBn5TdnxOvzf6a1F.htm)|Sky-Piercing Bow|Arc Perce-ciel|libre|
|[weapon-19-WCWdJmR5tYO7Aulb.htm](equipment/weapon-19-WCWdJmR5tYO7Aulb.htm)|Mattock of the Titans|Pioche des titans|officielle|
|[weapon-19-y49tnkAWZqJRMkba.htm](equipment/weapon-19-y49tnkAWZqJRMkba.htm)|Skyrider Sword (Greater)|Chevaucheur d'épée supérieur|libre|
|[weapon-20-D2CnWGQ84bSjh5bz.htm](equipment/weapon-20-D2CnWGQ84bSjh5bz.htm)|Buzzsaw Axe (Major)|Hache tourbillonnante majeure|libre|
|[weapon-20-inyiBwK0zNootR6G.htm](equipment/weapon-20-inyiBwK0zNootR6G.htm)|Dragonprism Staff (Greater)|Bâton dracoprismatique supérieur|libre|
|[weapon-20-iQM5xKCkfpfdPrzL.htm](equipment/weapon-20-iQM5xKCkfpfdPrzL.htm)|Splintering Spear (Major)|Lancé à échardes majeure|libre|
|[weapon-20-j9Z8TrUDHhdArJgi.htm](equipment/weapon-20-j9Z8TrUDHhdArJgi.htm)|Ouroboros Flail (Major)|Fléau ouroboros majeur|libre|
|[weapon-20-urcBTs4AgSq8u5FJ.htm](equipment/weapon-20-urcBTs4AgSq8u5FJ.htm)|Kaldemash's Lament|Complainte de Kaldemash|libre|
|[weapon-20-VJbZuJFTooFckp26.htm](equipment/weapon-20-VJbZuJFTooFckp26.htm)|Ridill|Ridill|libre|
|[weapon-20-w2AxS7q8bjh77pp2.htm](equipment/weapon-20-w2AxS7q8bjh77pp2.htm)|Staff of Arcane Might (Major)|Bâton de puissance arcanique majeur|libre|
|[weapon-20-YjaXxg9uQ02IbwLi.htm](equipment/weapon-20-YjaXxg9uQ02IbwLi.htm)|Orb Shard|Éclat d'orbe|libre|
|[weapon-20-ZrXw7hAH8dQOGAYL.htm](equipment/weapon-20-ZrXw7hAH8dQOGAYL.htm)|Blightburn Bomb (Greater)|Bombe fléau brûlant supérieure|libre|
|[weapon-22-6RkxhrJqBpFQlRBD.htm](equipment/weapon-22-6RkxhrJqBpFQlRBD.htm)|Blade of Fallen Stars|Lame des étoiles déchues|libre|
