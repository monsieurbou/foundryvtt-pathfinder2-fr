# État de la traduction (extinction-curse-bestiary)

 * **officielle**: 110
 * **libre**: 57
 * **changé**: 1


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[CQ2682vd4bUtvKQX.htm](extinction-curse-bestiary/CQ2682vd4bUtvKQX.htm)|Runkrunk|Runkrunk|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0fe7PVMIq92fkUJK.htm](extinction-curse-bestiary/0fe7PVMIq92fkUJK.htm)|Dream Pollen Pod|Cosses de pollen onirique|officielle|
|[0pw0r1w8vO7aRocN.htm](extinction-curse-bestiary/0pw0r1w8vO7aRocN.htm)|Resin-seep Xulgath|Xulgath suintant de résine|officielle|
|[0VzVpqN3Yp0IY3C1.htm](extinction-curse-bestiary/0VzVpqN3Yp0IY3C1.htm)|Nihiris|Nihiris|officielle|
|[16cZVZxsXVRHfuuQ.htm](extinction-curse-bestiary/16cZVZxsXVRHfuuQ.htm)|Jellico Bounce-Bounce|Jellico Bond-Bond|officielle|
|[18Y3yoYcEGoLcmDy.htm](extinction-curse-bestiary/18Y3yoYcEGoLcmDy.htm)|Death Drider|Drider de la Mort|libre|
|[1CjTIaMYUvQUkQI2.htm](extinction-curse-bestiary/1CjTIaMYUvQUkQI2.htm)|Mukradi Summoning Runes|Runes de convocation de Mukradi|officielle|
|[1HIqZAGVSR2pAiY6.htm](extinction-curse-bestiary/1HIqZAGVSR2pAiY6.htm)|Axiomatic Polymorph Trap|Piège de métamorphose axiomatique|officielle|
|[1HYH5BgFGtFxVMpc.htm](extinction-curse-bestiary/1HYH5BgFGtFxVMpc.htm)|Krooth Summoning Rune|Rune de convocation de Krooth|officielle|
|[1vOre5O8t3pQPUp6.htm](extinction-curse-bestiary/1vOre5O8t3pQPUp6.htm)|Herecite of Zevgavizeb|Hérécite de Zevgavizeb|libre|
|[25BUnECSaTWkHqZQ.htm](extinction-curse-bestiary/25BUnECSaTWkHqZQ.htm)|Convergence Lattice|Réseau convergent|officielle|
|[29NHB8DNNAbEk5Va.htm](extinction-curse-bestiary/29NHB8DNNAbEk5Va.htm)|Counteflora|Contreflore|officielle|
|[2bu9oGbB7UUTq2sR.htm](extinction-curse-bestiary/2bu9oGbB7UUTq2sR.htm)|Kirosthrek|Kirosthrek|libre|
|[2Ey2VZ3aQlN4FGHJ.htm](extinction-curse-bestiary/2Ey2VZ3aQlN4FGHJ.htm)|Pin Tingwheely|Pin Tintenboucle|libre|
|[2PxSX2cqEbSFjj3D.htm](extinction-curse-bestiary/2PxSX2cqEbSFjj3D.htm)|Aukashungi Swarm|Nuée d'Aukashungis|libre|
|[2XRzA5GZeDk88Y2z.htm](extinction-curse-bestiary/2XRzA5GZeDk88Y2z.htm)|War Sauropelta|Sauropelta de guerre|officielle|
|[3Fxih1eU4IXABxpy.htm](extinction-curse-bestiary/3Fxih1eU4IXABxpy.htm)|Mechanical Carny|Forain mécanique|officielle|
|[4Abc5gg8ac5ixGx1.htm](extinction-curse-bestiary/4Abc5gg8ac5ixGx1.htm)|Hallowed Wheel|La roue sacrée|libre|
|[4eQVYZ7sH7O8mw3R.htm](extinction-curse-bestiary/4eQVYZ7sH7O8mw3R.htm)|Brughadatch|Brughadatch|libre|
|[4L38nr9t17thMZrB.htm](extinction-curse-bestiary/4L38nr9t17thMZrB.htm)|Caustic Dart Trap|Piège à fléchette corrosive|officielle|
|[4lJiW36oO2iG7jCB.htm](extinction-curse-bestiary/4lJiW36oO2iG7jCB.htm)|Xulgath Deepmouth|Gueule-profonde xulgath|libre|
|[4QgC23j1wzfaCecR.htm](extinction-curse-bestiary/4QgC23j1wzfaCecR.htm)|Pruana Two-punch|Pruana Deux-Beignes|officielle|
|[5G8Iokror8QtBhbx.htm](extinction-curse-bestiary/5G8Iokror8QtBhbx.htm)|Urdefhan Dominator|Dominateur Urdefhan|libre|
|[5tlPKUZroqzycuzD.htm](extinction-curse-bestiary/5tlPKUZroqzycuzD.htm)|Shanchek|Shanchek|libre|
|[7p4RWS26W5k6vCkH.htm](extinction-curse-bestiary/7p4RWS26W5k6vCkH.htm)|Cat Sith|Chat sith|officielle|
|[7YJHi9niIKpFXXrf.htm](extinction-curse-bestiary/7YJHi9niIKpFXXrf.htm)|Celestial Menagerie Bruiser|Cogneur de la Ménagerie Céleste|officielle|
|[88VcnxbKPF2QkPiF.htm](extinction-curse-bestiary/88VcnxbKPF2QkPiF.htm)|Bitter Truth Bandit|Bandits de l'Âpre Vérité|officielle|
|[9cwg4cOgvFqtxIQ1.htm](extinction-curse-bestiary/9cwg4cOgvFqtxIQ1.htm)|Gluttondark Babau|Babau de Noir-Glouton|officielle|
|[a2FCggU8UCQl6RDx.htm](extinction-curse-bestiary/a2FCggU8UCQl6RDx.htm)|Juvenile Boar|Jeune sanglier|officielle|
|[aIT5S2fKgMZ6pVP2.htm](extinction-curse-bestiary/aIT5S2fKgMZ6pVP2.htm)|Ginjana Mindkeeper|Ginjana Gardesprit|libre|
|[aqcDvkIaKwJdMEAO.htm](extinction-curse-bestiary/aqcDvkIaKwJdMEAO.htm)|Iffdahsil|Iffdahsil|libre|
|[aUDIi3Z0N7IhzaT0.htm](extinction-curse-bestiary/aUDIi3Z0N7IhzaT0.htm)|Giant Aukashungi|Aukashungi géant|officielle|
|[B18UXeicuV8RF8kP.htm](extinction-curse-bestiary/B18UXeicuV8RF8kP.htm)|Ghost Crystal Cloud|Nuage de cristaux fantômes|officielle|
|[B2jQKaAXO7LofE7L.htm](extinction-curse-bestiary/B2jQKaAXO7LofE7L.htm)|Muurfeli|Muurfeli|libre|
|[b5creqUAlBl0Tmmc.htm](extinction-curse-bestiary/b5creqUAlBl0Tmmc.htm)|Leandrus|Léandrus|officielle|
|[b7ADAguVQLHCauWO.htm](extinction-curse-bestiary/b7ADAguVQLHCauWO.htm)|Poisoned Secret Door Trap|Piège empoisonné de la porte secrète|officielle|
|[B9fl34W1jENDoYqc.htm](extinction-curse-bestiary/B9fl34W1jENDoYqc.htm)|Ammut|Ammut|officielle|
|[BBamjhcpvwXGhlbM.htm](extinction-curse-bestiary/BBamjhcpvwXGhlbM.htm)|Cavnakash|Cavnakash|libre|
|[beb9LqlOBFseROnY.htm](extinction-curse-bestiary/beb9LqlOBFseROnY.htm)|Adrivallo|Adrivallo|libre|
|[bJUlb2DxT1xyaYAp.htm](extinction-curse-bestiary/bJUlb2DxT1xyaYAp.htm)|Darklands Alchemical Golem|Golem alchimique de l'Ombreterre|libre|
|[BORxkpaFBSCyB1f1.htm](extinction-curse-bestiary/BORxkpaFBSCyB1f1.htm)|Tallow Ooze|Vase de graisse|officielle|
|[bpTQfx4UixMV3Fja.htm](extinction-curse-bestiary/bpTQfx4UixMV3Fja.htm)|Kharostan|Kharostan|libre|
|[BsnU2Hf4a3MuXVPn.htm](extinction-curse-bestiary/BsnU2Hf4a3MuXVPn.htm)|Dyzallin's Golem|Golem de Dyzallin|officielle|
|[C07vwQbz6mKwLRGY.htm](extinction-curse-bestiary/C07vwQbz6mKwLRGY.htm)|Gahlepod|Gahlepod|officielle|
|[CknWKRO1xUHBL5Km.htm](extinction-curse-bestiary/CknWKRO1xUHBL5Km.htm)|Host of Spirits|Armée d'esprits|officielle|
|[CNO54boXvXg7xSP6.htm](extinction-curse-bestiary/CNO54boXvXg7xSP6.htm)|Tanessa Fleer|Tanessa Fleer|officielle|
|[CYXoXHr9BoPInIbe.htm](extinction-curse-bestiary/CYXoXHr9BoPInIbe.htm)|Thessekka|Thessekka|officielle|
|[D0laesU2eB1VOZTX.htm](extinction-curse-bestiary/D0laesU2eB1VOZTX.htm)|Shoony Tiller|Cultivateur Shouni|officielle|
|[DJyAXPHQy7OUUw00.htm](extinction-curse-bestiary/DJyAXPHQy7OUUw00.htm)|Headless Xulgath|Xulgath sans tête|officielle|
|[dP6sDHTZrDcV2I9w.htm](extinction-curse-bestiary/dP6sDHTZrDcV2I9w.htm)|Abberton Ruffian|Voyou d'Abberville|officielle|
|[DXNDZNHSZxlNXJnk.htm](extinction-curse-bestiary/DXNDZNHSZxlNXJnk.htm)|Convergent Soldier|Soldat convergent|officielle|
|[EBDDeBHGGZ8xvIM6.htm](extinction-curse-bestiary/EBDDeBHGGZ8xvIM6.htm)|Corrosive Lizard|Lézard corrosif|officielle|
|[f42l4cQyhNOLs19j.htm](extinction-curse-bestiary/f42l4cQyhNOLs19j.htm)|Thunderstone Cascade Trap|Piège cascade de pierres à tonnerre|officielle|
|[f4H9d0b1vJvxeFqs.htm](extinction-curse-bestiary/f4H9d0b1vJvxeFqs.htm)|Sodden Sentinel|Sentinelle spongieuse|libre|
|[f6uVOvKEkojOf9Ab.htm](extinction-curse-bestiary/f6uVOvKEkojOf9Ab.htm)|Swardlands Delinquent|Délinquant des Terres Vertes|officielle|
|[Ffj8PyKkBkYNV6pd.htm](extinction-curse-bestiary/Ffj8PyKkBkYNV6pd.htm)|Raptor Guard Wight|Nécrophage Garde raptor|libre|
|[fsPWWmIao1eFTAV4.htm](extinction-curse-bestiary/fsPWWmIao1eFTAV4.htm)|Sarvel Ever-Hunger|Sarvel Famine-Éternelle|libre|
|[fvGqCYfVAI3KXnSl.htm](extinction-curse-bestiary/fvGqCYfVAI3KXnSl.htm)|Xulgath Bomber|Artificier Xulgath|officielle|
|[FZqrluaaz5vhXEZ9.htm](extinction-curse-bestiary/FZqrluaaz5vhXEZ9.htm)|Xulgath Gutrager|Xulgath vomentrailles|officielle|
|[G2i7CUiYmyl1yjQo.htm](extinction-curse-bestiary/G2i7CUiYmyl1yjQo.htm)|Buzzing Latch Rune|Rune de loquet bourdonnant|officielle|
|[G2ICeUU6br5Xem3P.htm](extinction-curse-bestiary/G2ICeUU6br5Xem3P.htm)|Zinogyvaz|Zinogyvaz|libre|
|[gN8VuDZ8b9dp0Ep0.htm](extinction-curse-bestiary/gN8VuDZ8b9dp0Ep0.htm)|Giant Flea|Puce géante|officielle|
|[GuJJJzmjLKbkZUur.htm](extinction-curse-bestiary/GuJJJzmjLKbkZUur.htm)|Deghuun (Child of Mhar)|Deghuun (Enfant de Mhar)|officielle|
|[gZBayD2gJu7iZrud.htm](extinction-curse-bestiary/gZBayD2gJu7iZrud.htm)|Ruanna Nyamma|Ruanna Nyamma|libre|
|[HbxPY2GSxhRu4rVi.htm](extinction-curse-bestiary/HbxPY2GSxhRu4rVi.htm)|Lion Visitant|Revenant lion|libre|
|[HCxhltvoCdy3RXlH.htm](extinction-curse-bestiary/HCxhltvoCdy3RXlH.htm)|Saurian Warmonger|Saurien belliciste|officielle|
|[HEeRO5IF4lAGfDqE.htm](extinction-curse-bestiary/HEeRO5IF4lAGfDqE.htm)|Barking Stag|Cerf brameur|officielle|
|[HFAhDmrkxg6YLhdF.htm](extinction-curse-bestiary/HFAhDmrkxg6YLhdF.htm)|Blood Wolf|Loup de sang|officielle|
|[HHy2GazURA6Cx1ee.htm](extinction-curse-bestiary/HHy2GazURA6Cx1ee.htm)|Starved Staff|Employé affamé|libre|
|[hz2uoBS8DFOWshko.htm](extinction-curse-bestiary/hz2uoBS8DFOWshko.htm)|Darricus Stallit|Darricus Stallit|officielle|
|[I31kJb0DRkzTn8iX.htm](extinction-curse-bestiary/I31kJb0DRkzTn8iX.htm)|Xulgath Spinesnapper|Xulgath brise-vertèbres|officielle|
|[i3kQzeKcSoxkNYJb.htm](extinction-curse-bestiary/i3kQzeKcSoxkNYJb.htm)|Evora Yarket|Evora Yarket|officielle|
|[I6i1uvFI7VCTyVbM.htm](extinction-curse-bestiary/I6i1uvFI7VCTyVbM.htm)|Xulgath Herd-Tender|Éleveur Xulgath|libre|
|[i6t819VnEr1ESCAd.htm](extinction-curse-bestiary/i6t819VnEr1ESCAd.htm)|Vavakia|Vavakia|libre|
|[IacftcXDDMBNcKbY.htm](extinction-curse-bestiary/IacftcXDDMBNcKbY.htm)|Nemmia Bramblecloak|Nemmia Roncecape|officielle|
|[igd75rbnUbYBNyYw.htm](extinction-curse-bestiary/igd75rbnUbYBNyYw.htm)|Doblagub|Doblagub|libre|
|[IRon2oG76IG28Z4m.htm](extinction-curse-bestiary/IRon2oG76IG28Z4m.htm)|Urdefhan High Tormentor|Grand bourreau Urdefhan|libre|
|[iUA3E379TDoEAXz7.htm](extinction-curse-bestiary/iUA3E379TDoEAXz7.htm)|Qurashith|Qurashith|officielle|
|[IVpSJcoRLGgUfqW7.htm](extinction-curse-bestiary/IVpSJcoRLGgUfqW7.htm)|Harrow Doll|Poupée du tourment|officielle|
|[j31HXlZiUqQrAHSB.htm](extinction-curse-bestiary/j31HXlZiUqQrAHSB.htm)|Tashlock Banyan|Tashlock Banyan|libre|
|[J3mRwgqkOlOi44Xv.htm](extinction-curse-bestiary/J3mRwgqkOlOi44Xv.htm)|Stabbing Sentinel|Sentinelle tranchante|officielle|
|[jaUUH2i5UQZYQqab.htm](extinction-curse-bestiary/jaUUH2i5UQZYQqab.htm)|Muse Phantom|Muse spectrale|libre|
|[jCcA2ca8VnnDtVU9.htm](extinction-curse-bestiary/jCcA2ca8VnnDtVU9.htm)|Bugaboo|Fouettard|officielle|
|[JRvLtSt9cda4pc8z.htm](extinction-curse-bestiary/JRvLtSt9cda4pc8z.htm)|Obsidian Golem|Golem d'obsidienne|libre|
|[JvSCGGnexk7CmVke.htm](extinction-curse-bestiary/JvSCGGnexk7CmVke.htm)|Viktor Volkano|Viktor Volkano|officielle|
|[jwcMb71QRhMw94Id.htm](extinction-curse-bestiary/jwcMb71QRhMw94Id.htm)|Convergent Kendley Nathrael|Kendley Nathrael convergente|officielle|
|[jxGA1C8xX0WkJGI4.htm](extinction-curse-bestiary/jxGA1C8xX0WkJGI4.htm)|Yaganty|Yaganty|officielle|
|[k6mTbQPIlez0QhYg.htm](extinction-curse-bestiary/k6mTbQPIlez0QhYg.htm)|Xulgath Demon-Caller|Invocateur de démon xulgath|officielle|
|[KXNSBeUWLxMfd2Zg.htm](extinction-curse-bestiary/KXNSBeUWLxMfd2Zg.htm)|Pinacosaurus|Pinacosaure|officielle|
|[KXY9CRct0VKhiNob.htm](extinction-curse-bestiary/KXY9CRct0VKhiNob.htm)|Raving Spirit|Esprit délirant|officielle|
|[L25SQceNMS8IstYI.htm](extinction-curse-bestiary/L25SQceNMS8IstYI.htm)|Horba|Horba|officielle|
|[lh3pcyJlUUtNpWcI.htm](extinction-curse-bestiary/lh3pcyJlUUtNpWcI.htm)|Ledorick Banyan|Lédoric Banyan|officielle|
|[lM9j6lc5MkBlGfzD.htm](extinction-curse-bestiary/lM9j6lc5MkBlGfzD.htm)|Violet|Violet|officielle|
|[M3pYUyhrmylFqS0B.htm](extinction-curse-bestiary/M3pYUyhrmylFqS0B.htm)|Guthallath Rockslide|Éboulement de Guthallath|officielle|
|[mlktvPVq3mb5DTpP.htm](extinction-curse-bestiary/mlktvPVq3mb5DTpP.htm)|Suffering Xulgaths|Xulgaths souffrants|officielle|
|[MLYkh9pZpjBI1wpN.htm](extinction-curse-bestiary/MLYkh9pZpjBI1wpN.htm)|Imperious Darkside Mirror|Miroir aux sombres reflets impérieux|libre|
|[mTQYkgy9lfrv2yGG.htm](extinction-curse-bestiary/mTQYkgy9lfrv2yGG.htm)|Mask of Aroden's Guises|Masque des déguisements d'Aroden|officielle|
|[N8ekzW9snBA2YAKv.htm](extinction-curse-bestiary/N8ekzW9snBA2YAKv.htm)|Cu Sith|Cu sith|officielle|
|[NBCqA1NLRlhMuqGl.htm](extinction-curse-bestiary/NBCqA1NLRlhMuqGl.htm)|Daring Danika|Danika l'Audacieuse|officielle|
|[nFYpYIL5OUCFZCr9.htm](extinction-curse-bestiary/nFYpYIL5OUCFZCr9.htm)|Bogeyman|Croque-mitaine|officielle|
|[NgmbMRekIHiA44lg.htm](extinction-curse-bestiary/NgmbMRekIHiA44lg.htm)|Dyzallin Shraen|Dyzallin Shraen|libre|
|[NITxPebTwserqASe.htm](extinction-curse-bestiary/NITxPebTwserqASe.htm)|Xilvirek|Xilvirek|officielle|
|[nJBpLZFgEuF7uZAZ.htm](extinction-curse-bestiary/nJBpLZFgEuF7uZAZ.htm)|Witch-Priests' Curse|Malédiction des Prêtres-sorciers|officielle|
|[OCrQtfKDFpLedE13.htm](extinction-curse-bestiary/OCrQtfKDFpLedE13.htm)|Shoony Militia Member|Membre de la milice Shouni|officielle|
|[oJJspO9P2vDdtMYd.htm](extinction-curse-bestiary/oJJspO9P2vDdtMYd.htm)|The Vanish Man|Le Volatilisateur|officielle|
|[OqThOmNrdW3HVa7k.htm](extinction-curse-bestiary/OqThOmNrdW3HVa7k.htm)|Zashathal Head-Taker|Zashathal Coupe-la-Tête|officielle|
|[otSfn7djr37Bdejj.htm](extinction-curse-bestiary/otSfn7djr37Bdejj.htm)|Arskuva the Gnasher|Arskuva Grince-dents|officielle|
|[oYstle5FAR800UQT.htm](extinction-curse-bestiary/oYstle5FAR800UQT.htm)|Andera Paldreen|Andera Paldreen|officielle|
|[Plig7vUF9cEY1VBJ.htm](extinction-curse-bestiary/Plig7vUF9cEY1VBJ.htm)|Lakkai One-Fang|Lakkai Simple-Croc|officielle|
|[pOlP9ZR0eFS57c27.htm](extinction-curse-bestiary/pOlP9ZR0eFS57c27.htm)|Aives The Smoke Dragon|Aives le Dragon de fumée|officielle|
|[PpAOxz6ayvkn0fMK.htm](extinction-curse-bestiary/PpAOxz6ayvkn0fMK.htm)|Ararda|Ararda|libre|
|[PYvtflsrA9ITevEq.htm](extinction-curse-bestiary/PYvtflsrA9ITevEq.htm)|Urdefhan Hunter|Chasseur Urdefhan|libre|
|[Q3Z0rIINoWYxVSrS.htm](extinction-curse-bestiary/Q3Z0rIINoWYxVSrS.htm)|Corrupted Retainer|Serviteur corrompu|officielle|
|[QcJtBai5JViNFqUC.htm](extinction-curse-bestiary/QcJtBai5JViNFqUC.htm)|Viskithrel|Viskithrel|officielle|
|[qCTU0ywtCOgiUM0q.htm](extinction-curse-bestiary/qCTU0ywtCOgiUM0q.htm)|Skarja|Skarja|libre|
|[QFScy7QFB7PIeTEa.htm](extinction-curse-bestiary/QFScy7QFB7PIeTEa.htm)|Helg Eats-The-Eaters|Helg la Dévoreuse-des-dévoreurs|libre|
|[qKHkamxIPbqxEiwp.htm](extinction-curse-bestiary/qKHkamxIPbqxEiwp.htm)|Delamar Gianvin|Delamar Gianvin|officielle|
|[QMFlW9qrUKWBOF1Q.htm](extinction-curse-bestiary/QMFlW9qrUKWBOF1Q.htm)|Mistress Dusklight|Madame Lueur-du-Crépuscule|libre|
|[qXC4lGSWxqIhO5tr.htm](extinction-curse-bestiary/qXC4lGSWxqIhO5tr.htm)|Hooklimb Xulgath|Xulgath Croch'bras|officielle|
|[QZHvPM0Yatn8oxsj.htm](extinction-curse-bestiary/QZHvPM0Yatn8oxsj.htm)|Bugul Noz|Bugul Noz|libre|
|[R3SbxfWLp8xUvXUP.htm](extinction-curse-bestiary/R3SbxfWLp8xUvXUP.htm)|Elysian Sheep|Mouton Élysien|officielle|
|[RbrUXZyhqbsglVrr.htm](extinction-curse-bestiary/RbrUXZyhqbsglVrr.htm)|Qormintur|Qormintur|officielle|
|[RIZDryL3Wnk6ucks.htm](extinction-curse-bestiary/RIZDryL3Wnk6ucks.htm)|Luminous Ooze|Vase lumineuse|officielle|
|[Rma6WvkTkRW1H0ro.htm](extinction-curse-bestiary/Rma6WvkTkRW1H0ro.htm)|Angry Vegetation|Végétation en colère|officielle|
|[RVPH12vtOWMuJx0L.htm](extinction-curse-bestiary/RVPH12vtOWMuJx0L.htm)|Spiked Barricade Trap|Barricade piégée|officielle|
|[rxGByemLAAeM4h29.htm](extinction-curse-bestiary/rxGByemLAAeM4h29.htm)|Smoldering Leopard|Léopard ardent|officielle|
|[S5pcyiSXhMKrMjcC.htm](extinction-curse-bestiary/S5pcyiSXhMKrMjcC.htm)|Shraen Graveknight|Chevalier sépulcre Shraen|libre|
|[s6Yfy1AgPp4ky7QI.htm](extinction-curse-bestiary/s6Yfy1AgPp4ky7QI.htm)|Drunken Brawler|Bagarreur ivre|officielle|
|[s7447jKTaAd4r3Oa.htm](extinction-curse-bestiary/s7447jKTaAd4r3Oa.htm)|Ciza|Ciza|libre|
|[SAqbWugp2NcdEtrr.htm](extinction-curse-bestiary/SAqbWugp2NcdEtrr.htm)|Bone Croupier|Croupier osseux|libre|
|[SBz0ylPxEaUhG2aZ.htm](extinction-curse-bestiary/SBz0ylPxEaUhG2aZ.htm)|Xulgath Roughrider|Dompteur Xulgath|officielle|
|[sPuh6Ahatn0Vhn7P.htm](extinction-curse-bestiary/sPuh6Ahatn0Vhn7P.htm)|Urushil|Urushil|libre|
|[sQvm52N0E5ulBaaq.htm](extinction-curse-bestiary/sQvm52N0E5ulBaaq.htm)|Faceless Butcher|Boucher sans visage|officielle|
|[Sxip3Rmi0PFpzHNw.htm](extinction-curse-bestiary/Sxip3Rmi0PFpzHNw.htm)|Convergent Giant Eagle|Aigle géant convergent|officielle|
|[TdqGpBasgDOUqmNp.htm](extinction-curse-bestiary/TdqGpBasgDOUqmNp.htm)|Zuipnyrn|Zuipnyrn (taupe lunaire)|officielle|
|[tg5SrKu228LZkoC0.htm](extinction-curse-bestiary/tg5SrKu228LZkoC0.htm)|Xulgath Hardscale|Xulgath Dure-écaille|officielle|
|[tPvr9zvUfktMIvYU.htm](extinction-curse-bestiary/tPvr9zvUfktMIvYU.htm)|Stirvyn Banyan|Stirvyn Banyan|libre|
|[u6b7tlXDXMxmkdLO.htm](extinction-curse-bestiary/u6b7tlXDXMxmkdLO.htm)|Bogey|Cauchemarh|officielle|
|[U6jOmV5RAkj32qOi.htm](extinction-curse-bestiary/U6jOmV5RAkj32qOi.htm)|Smiler|Sourieur|libre|
|[UKBR2GdXIdg66Nqm.htm](extinction-curse-bestiary/UKBR2GdXIdg66Nqm.htm)|Ulthadar|Ulthadar|libre|
|[UNXMz1Q7hinlaxek.htm](extinction-curse-bestiary/UNXMz1Q7hinlaxek.htm)|Maze of Mirrors|Labyrinthe de miroirs|officielle|
|[uPWqYc05YQ0Q4i0g.htm](extinction-curse-bestiary/uPWqYc05YQ0Q4i0g.htm)|Xulgath Stoneliege|Xulgath suzerain de la pierre|officielle|
|[uUcqr8gPQIw5govC.htm](extinction-curse-bestiary/uUcqr8gPQIw5govC.htm)|Xulgath Skirmisher|Xulgath Tirailleur|officielle|
|[V4LPaPABVm9YYqdH.htm](extinction-curse-bestiary/V4LPaPABVm9YYqdH.htm)|Crushing Gate Trap|Piège de la porte écrasante|officielle|
|[vBUYl8mREwhO2Vst.htm](extinction-curse-bestiary/vBUYl8mREwhO2Vst.htm)|Catacomb Cave-In|Effondrement dans les catacombes|officielle|
|[viTyDNdkTo0JPLb8.htm](extinction-curse-bestiary/viTyDNdkTo0JPLb8.htm)|Kalkek|Kalkek|officielle|
|[vxFQrDnQqUpGpQmK.htm](extinction-curse-bestiary/vxFQrDnQqUpGpQmK.htm)|Kimilekki|Kimilekki|libre|
|[wBlnbLj8FfgxArQm.htm](extinction-curse-bestiary/wBlnbLj8FfgxArQm.htm)|Lyrt Cozurn|Lyrt Cozurn|libre|
|[wi94WddixQEID9Jl.htm](extinction-curse-bestiary/wi94WddixQEID9Jl.htm)|Corrupted Priest|Prêtre corrompu|libre|
|[wiNeCuB0DXt2ReAx.htm](extinction-curse-bestiary/wiNeCuB0DXt2ReAx.htm)|Xulgath Mage|Xulgath Mage|libre|
|[WOeSP1KLzo0qTpZu.htm](extinction-curse-bestiary/WOeSP1KLzo0qTpZu.htm)|Chimpanzee Visitant|Revenant chimpanzé|libre|
|[wOzDhkm9sd7IygoS.htm](extinction-curse-bestiary/wOzDhkm9sd7IygoS.htm)|Wight Cultist|Cultiste nécrophage|libre|
|[wQHcJGJ9rSyFQFPB.htm](extinction-curse-bestiary/wQHcJGJ9rSyFQFPB.htm)|Envenomed Thorns Trap|Piège d'épines envenimées|officielle|
|[wS2SN0dnQMybLzSA.htm](extinction-curse-bestiary/wS2SN0dnQMybLzSA.htm)|Flea Swarm|Nuée de puces|officielle|
|[x2XcPDPLeCAXITlZ.htm](extinction-curse-bestiary/x2XcPDPLeCAXITlZ.htm)|Iridescent Elephant|Éléphant irrisé|officielle|
|[xGTl3DVCD0etE6MU.htm](extinction-curse-bestiary/xGTl3DVCD0etE6MU.htm)|Ledorick Banyan (Possessed)|Lédoric Banyan (Possédé)|libre|
|[Xva8XyRrsHB6S7e8.htm](extinction-curse-bestiary/Xva8XyRrsHB6S7e8.htm)|Saurian Worldwatcher|Saurien observateur du monde|officielle|
|[xWsfcDcgbubVpjow.htm](extinction-curse-bestiary/xWsfcDcgbubVpjow.htm)|Xulgath Bilebearer|Xulgath biliaire|officielle|
|[XyHk8ChyuOkJuhVZ.htm](extinction-curse-bestiary/XyHk8ChyuOkJuhVZ.htm)|Guardian of the Faithful|Protecteur des fidèles|officielle|
|[YaKW9i3v9nn8Blij.htm](extinction-curse-bestiary/YaKW9i3v9nn8Blij.htm)|Xulgath Thoughtmaw|Xulgath Gueule-pensante|libre|
|[YAKZUBOth75W2mWT.htm](extinction-curse-bestiary/YAKZUBOth75W2mWT.htm)|Vermlek|Vermlek|officielle|
|[yqH59ltUd0f3kLLL.htm](extinction-curse-bestiary/yqH59ltUd0f3kLLL.htm)|Drow Bodyguard Golem|Golem garde du corps Drow|libre|
|[YS8UvVGFgHP2TrY3.htm](extinction-curse-bestiary/YS8UvVGFgHP2TrY3.htm)|Shoony Hierarch|Hiérarque Shouni|officielle|
|[YZcw33uhsPHWKcHM.htm](extinction-curse-bestiary/YZcw33uhsPHWKcHM.htm)|Hollow Hush|Abîme silencieux|libre|
|[Z3VrS25uw9vD2sek.htm](extinction-curse-bestiary/Z3VrS25uw9vD2sek.htm)|Vitalia|Vitalie|libre|
|[Z5E6UrUkiVW2UQRO.htm](extinction-curse-bestiary/Z5E6UrUkiVW2UQRO.htm)|Explosive Furniture Trap|Piège d'explosion de meubles|officielle|
|[ZG9ZJ5BZrZ9MS3h3.htm](extinction-curse-bestiary/ZG9ZJ5BZrZ9MS3h3.htm)|Echoes of Faith|Réminiscences pieuses|officielle|
|[zPXXUNE3m9VdrF9z.htm](extinction-curse-bestiary/zPXXUNE3m9VdrF9z.htm)|Thief's Trap|Piège du Voleur|officielle|
