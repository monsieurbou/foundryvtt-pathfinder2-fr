# État de la traduction (fists-of-the-ruby-phoenix-bestiary)

 * **aucune**: 141


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0N4ugHFs5GYllcMA.htm](fists-of-the-ruby-phoenix-bestiary/0N4ugHFs5GYllcMA.htm)|Shino Hakusa (Level 16)|
|[1y2FBsV4g80KNac1.htm](fists-of-the-ruby-phoenix-bestiary/1y2FBsV4g80KNac1.htm)|Takatorra (Level 13)|
|[2ryxyUYlf8lY6o1D.htm](fists-of-the-ruby-phoenix-bestiary/2ryxyUYlf8lY6o1D.htm)|Golarion's Finest (Mingyu)|
|[2ZnZ0d61CQHMHtHN.htm](fists-of-the-ruby-phoenix-bestiary/2ZnZ0d61CQHMHtHN.htm)|Arms of Balance (Ranya Shibhatesh)|
|[4RaeyGxw5EgagDZE.htm](fists-of-the-ruby-phoenix-bestiary/4RaeyGxw5EgagDZE.htm)|Surjit Hamelan|
|[526cj9mVkwQ9gDn6.htm](fists-of-the-ruby-phoenix-bestiary/526cj9mVkwQ9gDn6.htm)|Tino (Oni Form)|
|[56U7JoNKbci67vRE.htm](fists-of-the-ruby-phoenix-bestiary/56U7JoNKbci67vRE.htm)|Rivka (Kujiba)|
|[5cMbN10QKGS06Zhy.htm](fists-of-the-ruby-phoenix-bestiary/5cMbN10QKGS06Zhy.htm)|Arms of Balance (Jivati Rovat)|
|[65LJVPu9DFkF0YNX.htm](fists-of-the-ruby-phoenix-bestiary/65LJVPu9DFkF0YNX.htm)|Arms of Balance (Usvani)|
|[6Eh9w3FY129KXm7k.htm](fists-of-the-ruby-phoenix-bestiary/6Eh9w3FY129KXm7k.htm)|Kas Xi Rai|
|[6wRicVhIx07i94s3.htm](fists-of-the-ruby-phoenix-bestiary/6wRicVhIx07i94s3.htm)|Taiga Yai|
|[6z4xzi3yZuGr1Iv7.htm](fists-of-the-ruby-phoenix-bestiary/6z4xzi3yZuGr1Iv7.htm)|Spirit Turtle|
|[7MiWOVxjocxNr0dh.htm](fists-of-the-ruby-phoenix-bestiary/7MiWOVxjocxNr0dh.htm)|Laruhao|
|[7pCof1aDBZ1XlsXO.htm](fists-of-the-ruby-phoenix-bestiary/7pCof1aDBZ1XlsXO.htm)|Tino Tung (Level 13)|
|[7qyR096MhufccSiM.htm](fists-of-the-ruby-phoenix-bestiary/7qyR096MhufccSiM.htm)|Yabin the Just (Level 13)|
|[80tgHHqfxA5lUfjS.htm](fists-of-the-ruby-phoenix-bestiary/80tgHHqfxA5lUfjS.htm)|Bul-Gae|
|[8jNxwdzrSMFyoHJ5.htm](fists-of-the-ruby-phoenix-bestiary/8jNxwdzrSMFyoHJ5.htm)|Swatting Tail|
|[9ksOukuecb43zqOd.htm](fists-of-the-ruby-phoenix-bestiary/9ksOukuecb43zqOd.htm)|Razu|
|[9WcpNMJJqF3cLTqT.htm](fists-of-the-ruby-phoenix-bestiary/9WcpNMJJqF3cLTqT.htm)|Ran-to (Level 16)|
|[AeWPn9gJMgDg7WTo.htm](fists-of-the-ruby-phoenix-bestiary/AeWPn9gJMgDg7WTo.htm)|Lophiithu|
|[aiiUNGDPljyMuFu2.htm](fists-of-the-ruby-phoenix-bestiary/aiiUNGDPljyMuFu2.htm)|Syu Tak-nwa (Level 16)|
|[AxVNn9nyobosLEAq.htm](fists-of-the-ruby-phoenix-bestiary/AxVNn9nyobosLEAq.htm)|Agile Warrior|
|[AYQEkPFyTuGlxNg3.htm](fists-of-the-ruby-phoenix-bestiary/AYQEkPFyTuGlxNg3.htm)|Mage of Many Styles|
|[BcRxQjtyMNgiEpvb.htm](fists-of-the-ruby-phoenix-bestiary/BcRxQjtyMNgiEpvb.htm)|Floating Flamethrower|
|[blm15iXMR3Lbwdom.htm](fists-of-the-ruby-phoenix-bestiary/blm15iXMR3Lbwdom.htm)|Mafika Ayuwari|
|[cg1kQPO3FBSCFDVt.htm](fists-of-the-ruby-phoenix-bestiary/cg1kQPO3FBSCFDVt.htm)|Shadow Yai|
|[CKS4OsoyI2QI8RUn.htm](fists-of-the-ruby-phoenix-bestiary/CKS4OsoyI2QI8RUn.htm)|Golarion's Finest (Han)|
|[d7fBEPfzXSeCZZXE.htm](fists-of-the-ruby-phoenix-bestiary/d7fBEPfzXSeCZZXE.htm)|Canopy Elder|
|[dTcsY3v9lg4BRrsj.htm](fists-of-the-ruby-phoenix-bestiary/dTcsY3v9lg4BRrsj.htm)|Anugobu Apprentice|
|[DW4UFHXzExWwvEuH.htm](fists-of-the-ruby-phoenix-bestiary/DW4UFHXzExWwvEuH.htm)|Dread Roc|
|[EDeiF3PNtXUeBU6P.htm](fists-of-the-ruby-phoenix-bestiary/EDeiF3PNtXUeBU6P.htm)|Air Rift|
|[EItI5u34FIIuZM9W.htm](fists-of-the-ruby-phoenix-bestiary/EItI5u34FIIuZM9W.htm)|Angoyang|
|[eVKjtiQtDaJpk9Ra.htm](fists-of-the-ruby-phoenix-bestiary/eVKjtiQtDaJpk9Ra.htm)|Sixth Pillar Student|
|[ew2WOdIU5njPMzok.htm](fists-of-the-ruby-phoenix-bestiary/ew2WOdIU5njPMzok.htm)|Watchtower Shadow|
|[EYr9GBleLFezHqBk.htm](fists-of-the-ruby-phoenix-bestiary/EYr9GBleLFezHqBk.htm)|Rivka (Yorak)|
|[Ezm1FlUk4JXeqPlC.htm](fists-of-the-ruby-phoenix-bestiary/Ezm1FlUk4JXeqPlC.htm)|Rai Sho Disciple|
|[f0nafAQA1tSXLqwL.htm](fists-of-the-ruby-phoenix-bestiary/f0nafAQA1tSXLqwL.htm)|Abbot Tsujon|
|[fOdNBYgjTveLzpbd.htm](fists-of-the-ruby-phoenix-bestiary/fOdNBYgjTveLzpbd.htm)|Agile Warrior (Nightmares)|
|[FWfKJ7e8UgbBXTkU.htm](fists-of-the-ruby-phoenix-bestiary/FWfKJ7e8UgbBXTkU.htm)|Weapon Master (Under the Pale Sun Dervishes)|
|[FwIJpQn1FIpGgvIY.htm](fists-of-the-ruby-phoenix-bestiary/FwIJpQn1FIpGgvIY.htm)|Artus Rodrivan|
|[Fwjqlg5gsZwx1FuC.htm](fists-of-the-ruby-phoenix-bestiary/Fwjqlg5gsZwx1FuC.htm)|Quaking Footfall|
|[fxukt1qqVaQG9KcY.htm](fists-of-the-ruby-phoenix-bestiary/fxukt1qqVaQG9KcY.htm)|Syu Tak-nwa (Level 20)|
|[g1ms3xQfS6Ws7KGD.htm](fists-of-the-ruby-phoenix-bestiary/g1ms3xQfS6Ws7KGD.htm)|Sthira|
|[gGDWBzon2T2mrSqf.htm](fists-of-the-ruby-phoenix-bestiary/gGDWBzon2T2mrSqf.htm)|Hummingbird|
|[ghAlcSfpO03JLvwG.htm](fists-of-the-ruby-phoenix-bestiary/ghAlcSfpO03JLvwG.htm)|Ji-yook (Level 13)|
|[gSlkZ86P3QrbM874.htm](fists-of-the-ruby-phoenix-bestiary/gSlkZ86P3QrbM874.htm)|Weapon Master|
|[H5koEzKL8qtfX4fK.htm](fists-of-the-ruby-phoenix-bestiary/H5koEzKL8qtfX4fK.htm)|Urnak Lostwind|
|[haUIlixea4iwy0GZ.htm](fists-of-the-ruby-phoenix-bestiary/haUIlixea4iwy0GZ.htm)|Ki Adept (Ahmoza Twins)|
|[hCHAr6Mp0AxSxj3h.htm](fists-of-the-ruby-phoenix-bestiary/hCHAr6Mp0AxSxj3h.htm)|Wronged Monk's Wrath|
|[HGtyyMucLkYlqZ8i.htm](fists-of-the-ruby-phoenix-bestiary/HGtyyMucLkYlqZ8i.htm)|Jin-hae|
|[hkGMvgL9IOZcAKyc.htm](fists-of-the-ruby-phoenix-bestiary/hkGMvgL9IOZcAKyc.htm)|Hana's Hundreds|
|[HqN4nUnl75foBKLZ.htm](fists-of-the-ruby-phoenix-bestiary/HqN4nUnl75foBKLZ.htm)|Nai Yan Fei|
|[htshGim6Q1y75TPt.htm](fists-of-the-ruby-phoenix-bestiary/htshGim6Q1y75TPt.htm)|Desecrated Guardian|
|[i3dC41mOjfoBxQTk.htm](fists-of-the-ruby-phoenix-bestiary/i3dC41mOjfoBxQTk.htm)|Juspix Rammel|
|[IGEht7ysUSZndtgp.htm](fists-of-the-ruby-phoenix-bestiary/IGEht7ysUSZndtgp.htm)|Old Man Statue|
|[IlDWyCnxA6gS2UZm.htm](fists-of-the-ruby-phoenix-bestiary/IlDWyCnxA6gS2UZm.htm)|Syndara the Sculptor|
|[iQ1wsuosURrA6tUI.htm](fists-of-the-ruby-phoenix-bestiary/iQ1wsuosURrA6tUI.htm)|Portal Eater|
|[J7s3MTb3eZP6u3LT.htm](fists-of-the-ruby-phoenix-bestiary/J7s3MTb3eZP6u3LT.htm)|Takatorra (Level 9)|
|[JFa9WD0nXOTgFGha.htm](fists-of-the-ruby-phoenix-bestiary/JFa9WD0nXOTgFGha.htm)|Golarion's Finest (Numoriz)|
|[jnN7fYYndv3oobJJ.htm](fists-of-the-ruby-phoenix-bestiary/jnN7fYYndv3oobJJ.htm)|Butterfly Blade Warrior|
|[k5UjbiYr87NVK7qp.htm](fists-of-the-ruby-phoenix-bestiary/k5UjbiYr87NVK7qp.htm)|Rivka (Cimurlian)|
|[ka41qAFCgGFBXUPJ.htm](fists-of-the-ruby-phoenix-bestiary/ka41qAFCgGFBXUPJ.htm)|Watchtower Wraith|
|[KfpYMKJ1ka9volP6.htm](fists-of-the-ruby-phoenix-bestiary/KfpYMKJ1ka9volP6.htm)|Grandfather Mantis|
|[khAJHftnWe21eTm2.htm](fists-of-the-ruby-phoenix-bestiary/khAJHftnWe21eTm2.htm)|Shino Hakusa (Level 14)|
|[KHPOm5KHtbMNzcx7.htm](fists-of-the-ruby-phoenix-bestiary/KHPOm5KHtbMNzcx7.htm)|Sigbin|
|[koRKlywSbwttifEq.htm](fists-of-the-ruby-phoenix-bestiary/koRKlywSbwttifEq.htm)|Amihan|
|[l2oapyRhfCFRsCHX.htm](fists-of-the-ruby-phoenix-bestiary/l2oapyRhfCFRsCHX.htm)|Umbasi|
|[L2OWZ5Afvnmaif83.htm](fists-of-the-ruby-phoenix-bestiary/L2OWZ5Afvnmaif83.htm)|Tamikan|
|[lemXhFkFAUbZGtL6.htm](fists-of-the-ruby-phoenix-bestiary/lemXhFkFAUbZGtL6.htm)|Kun|
|[Lk4LPjGsidzuD6Vy.htm](fists-of-the-ruby-phoenix-bestiary/Lk4LPjGsidzuD6Vy.htm)|Huldrin Skolsdottir|
|[lqHn5wSzxvdllGgH.htm](fists-of-the-ruby-phoenix-bestiary/lqHn5wSzxvdllGgH.htm)|Flying Mountain Kaminari|
|[Lsa0Q8aU8aPWjFYv.htm](fists-of-the-ruby-phoenix-bestiary/Lsa0Q8aU8aPWjFYv.htm)|Kannitri|
|[MdkbAYfKgIvtTxBO.htm](fists-of-the-ruby-phoenix-bestiary/MdkbAYfKgIvtTxBO.htm)|Rivka (Mogaru)|
|[mdOASi6f7CTVH1ys.htm](fists-of-the-ruby-phoenix-bestiary/mdOASi6f7CTVH1ys.htm)|Dimensional Darkside Mirror|
|[mwovQpf278O1vdCR.htm](fists-of-the-ruby-phoenix-bestiary/mwovQpf278O1vdCR.htm)|Watchtower Poltergeist|
|[N9Qp7x6n6ighaXOY.htm](fists-of-the-ruby-phoenix-bestiary/N9Qp7x6n6ighaXOY.htm)|Golarion's Finest (Jun)|
|[nejHZ9ccKcDXZWQM.htm](fists-of-the-ruby-phoenix-bestiary/nejHZ9ccKcDXZWQM.htm)|Gomwai|
|[o8UMCrKWf89xNvSE.htm](fists-of-the-ruby-phoenix-bestiary/o8UMCrKWf89xNvSE.htm)|Ki Adept|
|[ofYZYIZTkakVj2R0.htm](fists-of-the-ruby-phoenix-bestiary/ofYZYIZTkakVj2R0.htm)|Ji-yook (Gumiho Form)|
|[PhFDdGFXaaiLe4k4.htm](fists-of-the-ruby-phoenix-bestiary/PhFDdGFXaaiLe4k4.htm)|Joon-seo|
|[pi3njPvz8AsOrVGZ.htm](fists-of-the-ruby-phoenix-bestiary/pi3njPvz8AsOrVGZ.htm)|Golarion's Finest (Rajna)|
|[PiO3DnxtaT3W4728.htm](fists-of-the-ruby-phoenix-bestiary/PiO3DnxtaT3W4728.htm)|Peng|
|[PooDdJo4wmkBvETW.htm](fists-of-the-ruby-phoenix-bestiary/PooDdJo4wmkBvETW.htm)|Freezing Floor Tiles|
|[PsipHKpczW9FT2Jk.htm](fists-of-the-ruby-phoenix-bestiary/PsipHKpczW9FT2Jk.htm)|Planar Terra-cotta Soldier|
|[Q6gjfL2rha7tRS0B.htm](fists-of-the-ruby-phoenix-bestiary/Q6gjfL2rha7tRS0B.htm)|Ji-yook (Level 9)|
|[QhFUhlZl1bvUrtPs.htm](fists-of-the-ruby-phoenix-bestiary/QhFUhlZl1bvUrtPs.htm)|Rai Sho Postulant|
|[qpbFSs0Z68z58T4m.htm](fists-of-the-ruby-phoenix-bestiary/qpbFSs0Z68z58T4m.htm)|Tino Tung (Level 9)|
|[qV1bsHLfgI8xh0Nr.htm](fists-of-the-ruby-phoenix-bestiary/qV1bsHLfgI8xh0Nr.htm)|Lantondo|
|[qW8stNdPqQwibYQz.htm](fists-of-the-ruby-phoenix-bestiary/qW8stNdPqQwibYQz.htm)|Ran-to (Level 20)|
|[rAsJXFWr1IJatEUm.htm](fists-of-the-ruby-phoenix-bestiary/rAsJXFWr1IJatEUm.htm)|Mammoth Turtle|
|[RCbSoJ0Kg6ovGnRG.htm](fists-of-the-ruby-phoenix-bestiary/RCbSoJ0Kg6ovGnRG.htm)|Hantu Belian|
|[RKkIczqYUJkij0Oh.htm](fists-of-the-ruby-phoenix-bestiary/RKkIczqYUJkij0Oh.htm)|Blue Viper (Level 14)|
|[RVaEIyjBl44etuvL.htm](fists-of-the-ruby-phoenix-bestiary/RVaEIyjBl44etuvL.htm)|Shino Hakusa (Level 20)|
|[se8SrwtuaamXIEo5.htm](fists-of-the-ruby-phoenix-bestiary/se8SrwtuaamXIEo5.htm)|Blue Viper (Level 20)|
|[SF6OlCfNiHvtJQjw.htm](fists-of-the-ruby-phoenix-bestiary/SF6OlCfNiHvtJQjw.htm)|Blue Viper (Level 16)|
|[snrGRRYXD8mUG4Y1.htm](fists-of-the-ruby-phoenix-bestiary/snrGRRYXD8mUG4Y1.htm)|Golarion's Finest (Krankkiss)|
|[sVXMz65SI2rla17T.htm](fists-of-the-ruby-phoenix-bestiary/sVXMz65SI2rla17T.htm)|Sand Whirlwind|
|[Sx7Ob64O0e9aIhpO.htm](fists-of-the-ruby-phoenix-bestiary/Sx7Ob64O0e9aIhpO.htm)|Syu Tak-nwa (Level 14)|
|[sXtwJyM7sWWDQDOU.htm](fists-of-the-ruby-phoenix-bestiary/sXtwJyM7sWWDQDOU.htm)|Muckish Creep|
|[T5KMiSE8W1R8ChbV.htm](fists-of-the-ruby-phoenix-bestiary/T5KMiSE8W1R8ChbV.htm)|Grave Spinosaurus|
|[TbImjEpxYO5tsJIA.htm](fists-of-the-ruby-phoenix-bestiary/TbImjEpxYO5tsJIA.htm)|Gumiho|
|[tBQgipDUVhamBNrZ.htm](fists-of-the-ruby-phoenix-bestiary/tBQgipDUVhamBNrZ.htm)|Dromornis|
|[TEHxsglxWi1i3jSN.htm](fists-of-the-ruby-phoenix-bestiary/TEHxsglxWi1i3jSN.htm)|Elder Cauthooj|
|[tfBczpCPNVnRE8pJ.htm](fists-of-the-ruby-phoenix-bestiary/tfBczpCPNVnRE8pJ.htm)|Yoh Souran|
|[TjmD89wG7qQ0tRXj.htm](fists-of-the-ruby-phoenix-bestiary/TjmD89wG7qQ0tRXj.htm)|Speakers to the Wind (Gnoll Cascade Bearer)|
|[TqKFvTmJnhdclCgt.htm](fists-of-the-ruby-phoenix-bestiary/TqKFvTmJnhdclCgt.htm)|Ghost Monk|
|[TTEcqxuMqzIdkX6q.htm](fists-of-the-ruby-phoenix-bestiary/TTEcqxuMqzIdkX6q.htm)|Troff Frostknuckles|
|[tu64qeuTiIGHtvgR.htm](fists-of-the-ruby-phoenix-bestiary/tu64qeuTiIGHtvgR.htm)|Golarion's Finest (Brartork)|
|[Twt8z6nTPgYQ0mTv.htm](fists-of-the-ruby-phoenix-bestiary/Twt8z6nTPgYQ0mTv.htm)|Berberoka|
|[u3vdt2J02rw8RyRq.htm](fists-of-the-ruby-phoenix-bestiary/u3vdt2J02rw8RyRq.htm)|Yabin the Just (Level 9)|
|[UbXvVA3ZdSN6pj1j.htm](fists-of-the-ruby-phoenix-bestiary/UbXvVA3ZdSN6pj1j.htm)|Cloudsplitter|
|[uvBZm8b7Vtzn1U2d.htm](fists-of-the-ruby-phoenix-bestiary/uvBZm8b7Vtzn1U2d.htm)|Spinel Leviathan Syndara|
|[v85RucvwE8lGq7oY.htm](fists-of-the-ruby-phoenix-bestiary/v85RucvwE8lGq7oY.htm)|Manananggal|
|[VEnxFwTAhyO03Yse.htm](fists-of-the-ruby-phoenix-bestiary/VEnxFwTAhyO03Yse.htm)|Inmyeonjo|
|[vf59nmtYaF0se9Ui.htm](fists-of-the-ruby-phoenix-bestiary/vf59nmtYaF0se9Ui.htm)|Dancing Night Parade|
|[VGJF0jWWnA4ljfZE.htm](fists-of-the-ruby-phoenix-bestiary/VGJF0jWWnA4ljfZE.htm)|Melodic Squall|
|[vgMFPC9SoC3eyNSs.htm](fists-of-the-ruby-phoenix-bestiary/vgMFPC9SoC3eyNSs.htm)|Maalya|
|[vjE1lUpklp4slNbd.htm](fists-of-the-ruby-phoenix-bestiary/vjE1lUpklp4slNbd.htm)|Planar Terra-cotta Squadron|
|[vjONecsFtSFpjNaN.htm](fists-of-the-ruby-phoenix-bestiary/vjONecsFtSFpjNaN.htm)|Hantu Denai|
|[wfp1cV5xCdpElkoU.htm](fists-of-the-ruby-phoenix-bestiary/wfp1cV5xCdpElkoU.htm)|Yabin (White Serpent Form)|
|[wFswIWjySnPqVNVT.htm](fists-of-the-ruby-phoenix-bestiary/wFswIWjySnPqVNVT.htm)|Golarion's Finest (Paunnima)|
|[WqkV7xd2KOmNQd67.htm](fists-of-the-ruby-phoenix-bestiary/WqkV7xd2KOmNQd67.htm)|Broken Rebus Attack|
|[xBSLIoVsZ6I4LOZc.htm](fists-of-the-ruby-phoenix-bestiary/xBSLIoVsZ6I4LOZc.htm)|Arms of Balance (Pravan Majinapti)|
|[XiO3tJlYmuZwOBaA.htm](fists-of-the-ruby-phoenix-bestiary/XiO3tJlYmuZwOBaA.htm)|Koto Zekora|
|[xiQIw7iFjOizo3Fm.htm](fists-of-the-ruby-phoenix-bestiary/xiQIw7iFjOizo3Fm.htm)|Akila Stormheel|
|[XN2uksGMgndh1jSP.htm](fists-of-the-ruby-phoenix-bestiary/XN2uksGMgndh1jSP.htm)|Ran-to (Level 14)|
|[Y1Px08o90xG5dWTq.htm](fists-of-the-ruby-phoenix-bestiary/Y1Px08o90xG5dWTq.htm)|Takatorra (Daitengu Form)|
|[ybPcnDqULaojoU8a.htm](fists-of-the-ruby-phoenix-bestiary/ybPcnDqULaojoU8a.htm)|Drake Courser|
|[YCfpTifMs2minJrF.htm](fists-of-the-ruby-phoenix-bestiary/YCfpTifMs2minJrF.htm)|Mogaru's Breath|
|[Yg0lmfxyBSggOkUp.htm](fists-of-the-ruby-phoenix-bestiary/Yg0lmfxyBSggOkUp.htm)|Orochi|
|[YHbBs2Jq6ukngOra.htm](fists-of-the-ruby-phoenix-bestiary/YHbBs2Jq6ukngOra.htm)|Sanzuwu|
|[YHSV9DfaqoaSggLi.htm](fists-of-the-ruby-phoenix-bestiary/YHSV9DfaqoaSggLi.htm)|Archery Specialist|
|[yNqeqEULKA536K4r.htm](fists-of-the-ruby-phoenix-bestiary/yNqeqEULKA536K4r.htm)|Anugobu Wondercrafter|
|[YVP3pM7jxY9Gyouy.htm](fists-of-the-ruby-phoenix-bestiary/YVP3pM7jxY9Gyouy.htm)|Caustic Monitor|
|[zFye7hJzzwgajovA.htm](fists-of-the-ruby-phoenix-bestiary/zFye7hJzzwgajovA.htm)|Halspin the Stung|
|[zj3s5CUsTjRRiROd.htm](fists-of-the-ruby-phoenix-bestiary/zj3s5CUsTjRRiROd.htm)|Collapsing Structure|
|[zmE4irZj6a2WpB6f.htm](fists-of-the-ruby-phoenix-bestiary/zmE4irZj6a2WpB6f.htm)|Yarrika Mulandez|
|[zNOSSDaaCozimqaS.htm](fists-of-the-ruby-phoenix-bestiary/zNOSSDaaCozimqaS.htm)|Jaiban|
|[ZoBq423Q9PMa3rDV.htm](fists-of-the-ruby-phoenix-bestiary/ZoBq423Q9PMa3rDV.htm)|Master Xun|
|[Zp6lrCF3JwpdfKyK.htm](fists-of-the-ruby-phoenix-bestiary/Zp6lrCF3JwpdfKyK.htm)|Tyrannosaurus Imperator|
|[zpXQHbe037haBVmD.htm](fists-of-the-ruby-phoenix-bestiary/zpXQHbe037haBVmD.htm)|Rivka (Igroon)|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
