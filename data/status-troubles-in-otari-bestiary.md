# État de la traduction (troubles-in-otari-bestiary)

 * **officielle**: 16
 * **libre**: 2


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Fvulq5Zv4BQPvYV.htm](troubles-in-otari-bestiary/0Fvulq5Zv4BQPvYV.htm)|Collapsing Porch|Effondrement du porche|officielle|
|[3Pa5JoY9PYdV5x1e.htm](troubles-in-otari-bestiary/3Pa5JoY9PYdV5x1e.htm)|Brimstone Rat|Rat sulfureux|officielle|
|[4GZsTkvuYADpcdmV.htm](troubles-in-otari-bestiary/4GZsTkvuYADpcdmV.htm)|Kurnugian Jackal|Chacal Kurnugian|libre|
|[5iOgKbhW9zVnV1cr.htm](troubles-in-otari-bestiary/5iOgKbhW9zVnV1cr.htm)|Bugbear Marauder|Gobelours maraudeur|officielle|
|[6Ut7YjjLZADMU7NY.htm](troubles-in-otari-bestiary/6Ut7YjjLZADMU7NY.htm)|Hargrit Leadbuster|Hargrit Brise-négoce|officielle|
|[aqDFxmLMx6cOnGlx.htm](troubles-in-otari-bestiary/aqDFxmLMx6cOnGlx.htm)|Tongues of Flame|Langues de feu|officielle|
|[b8Wgun7T7sh7NVrd.htm](troubles-in-otari-bestiary/b8Wgun7T7sh7NVrd.htm)|Web Lurker Noose|Noeud coulant du Rôdeur des toiles|officielle|
|[fhItdltCmcAZkvcI.htm](troubles-in-otari-bestiary/fhItdltCmcAZkvcI.htm)|Kobold Trapmaster|Kobold Maître-des-pièges|officielle|
|[hv50ovLKV6lu49dF.htm](troubles-in-otari-bestiary/hv50ovLKV6lu49dF.htm)|Nightmare Terrain|Terrain cauchemardesque|officielle|
|[l7wng1EXY7np8Hcu.htm](troubles-in-otari-bestiary/l7wng1EXY7np8Hcu.htm)|Scalliwing|Ailécaille|officielle|
|[lTVgdXuSv2B0yVVW.htm](troubles-in-otari-bestiary/lTVgdXuSv2B0yVVW.htm)|Morgrym Leadbuster|Morgrym Brise-négoce|libre|
|[m4jitI3J1QkjJq3H.htm](troubles-in-otari-bestiary/m4jitI3J1QkjJq3H.htm)|Omblin Leadbuster|Omblin Brise-négoce|officielle|
|[MTI298bhLGYktyZX.htm](troubles-in-otari-bestiary/MTI298bhLGYktyZX.htm)|Orc Scrapper|Orque bagarreur|officielle|
|[NeFSJTGROSgGIKNd.htm](troubles-in-otari-bestiary/NeFSJTGROSgGIKNd.htm)|Viper Urn|Urne de la vipère|officielle|
|[PhLxqnAIhCovfcXy.htm](troubles-in-otari-bestiary/PhLxqnAIhCovfcXy.htm)|Kotgar Leadbuster|Kotgar Brise-négoce|officielle|
|[rRSIqZ5g3QRfaAQ0.htm](troubles-in-otari-bestiary/rRSIqZ5g3QRfaAQ0.htm)|Orc Commander|Orque commandant|officielle|
|[TIX9yDasefwJ4PxI.htm](troubles-in-otari-bestiary/TIX9yDasefwJ4PxI.htm)|Stinkweed Shambler|Grand tertre putréfeuille|officielle|
|[vg6fEjWSj4jilXRn.htm](troubles-in-otari-bestiary/vg6fEjWSj4jilXRn.htm)|Magic Starknives Trap|Piège de lamétoiles magiques|officielle|
