# État de la traduction (equipment-effects)

 * **libre**: 504
 * **changé**: 73
 * **aucune**: 13


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0TZLR0PO5TJm9F4l.htm](equipment-effects/0TZLR0PO5TJm9F4l.htm)|Effect: Forgefather's Seal|
|[A4FWOpQ8G45LMocX.htm](equipment-effects/A4FWOpQ8G45LMocX.htm)|Effect: Force Shield|
|[AYLthNtyUlWmKqhW.htm](equipment-effects/AYLthNtyUlWmKqhW.htm)|Effect: Saurian Spike - Armor|
|[BaosUEePGMqw0nWn.htm](equipment-effects/BaosUEePGMqw0nWn.htm)|Effect: Glow Rod|
|[Dg19mPbwX5kBr10t.htm](equipment-effects/Dg19mPbwX5kBr10t.htm)|Effect: Saurian Spike - Armor (Greater)|
|[dmLcrEtn48rBajt0.htm](equipment-effects/dmLcrEtn48rBajt0.htm)|Effect: Ebon Fulcrum Lens (Incorporeal)|
|[dVvMomZnSTq1wOJd.htm](equipment-effects/dVvMomZnSTq1wOJd.htm)|Effect: Oil of Keen Edges|
|[Elj69wSjv91ouC7v.htm](equipment-effects/Elj69wSjv91ouC7v.htm)|Effect: Forgefather's Seal (Stalwart Sacrifice)|
|[FTxP7qt4SqZXAkJu.htm](equipment-effects/FTxP7qt4SqZXAkJu.htm)|Effect: Saurian Spike - Armor (Major)|
|[IwcnAH2a7JVwskO7.htm](equipment-effects/IwcnAH2a7JVwskO7.htm)|Effect: Forge Warden|
|[MFbOXncTwYaskdfG.htm](equipment-effects/MFbOXncTwYaskdfG.htm)|Effect: Shining Symbol Weakness|
|[nLwtvyLZZTZgFmKq.htm](equipment-effects/nLwtvyLZZTZgFmKq.htm)|Effect: Tactician's Helm Charges|
|[Qf7ksK6sjC4x7SWw.htm](equipment-effects/Qf7ksK6sjC4x7SWw.htm)|Effect: Cloak of Swiftness|
|[XgIY3j2VFUSQ5Or9.htm](equipment-effects/XgIY3j2VFUSQ5Or9.htm)|Effect: Staff of Illumination|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0qJEtpXWPb7JJBbY.htm](equipment-effects/0qJEtpXWPb7JJBbY.htm)|Effect: Aromatic Ammunition|Effet : Munition aromatique|changé|
|[2iR5uP6vgPzgKKNO.htm](equipment-effects/2iR5uP6vgPzgKKNO.htm)|Effect: Red-Rib Gill Mask (Moderate)|Effet : Masque branchies de côte rouge modéré|changé|
|[2ytxPqhGyLtEjYxW.htm](equipment-effects/2ytxPqhGyLtEjYxW.htm)|Effect: Static Snare|Effet : piège artisanal statique|changé|
|[5dhm66yN0LQTOePw.htm](equipment-effects/5dhm66yN0LQTOePw.htm)|Effect: Holy Steam Ball|Effet : Bille de vapeur sacrée|changé|
|[5mQ51m1lqQlvfi8n.htm](equipment-effects/5mQ51m1lqQlvfi8n.htm)|Effect: Phantasmal Doorknob - Weapon|Effet : Poignée de porte imaginaire - Arme|changé|
|[5xgapIXn5DwbXHKh.htm](equipment-effects/5xgapIXn5DwbXHKh.htm)|Effect: Serene Mutagen (Lesser)|Effet : Mutagène de sérénité inférieur|changé|
|[6A8jsLR7upLGuRiv.htm](equipment-effects/6A8jsLR7upLGuRiv.htm)|Effect: Lastwall Soup|Effet : Soupe de Dernier-Rempart|changé|
|[7UL8belWmo7U5YGM.htm](equipment-effects/7UL8belWmo7U5YGM.htm)|Effect: Darkvision Elixir (Lesser)|Effet : Élixir de vision dans le noir inférieur|changé|
|[7vCenP9j6FuHRv5C.htm](equipment-effects/7vCenP9j6FuHRv5C.htm)|Effect: Darkvision Elixir (Greater)|Effet : Élixir de Vision dans le noir supérieur|changé|
|[8ersuvNJXX00XaIQ.htm](equipment-effects/8ersuvNJXX00XaIQ.htm)|Effect: Euryale (Curse) Card|Effet : Carte Euryale (Malédiction)|changé|
|[8kfSF8P4NOh09YvZ.htm](equipment-effects/8kfSF8P4NOh09YvZ.htm)|Effect: Grim Sandglass - Weapon (Greater)|Effet : Sablier sinistre supérieur - arme|changé|
|[9e6iVkPpGqJYwMyb.htm](equipment-effects/9e6iVkPpGqJYwMyb.htm)|Effect: Brewer's Regret|Effet : Regret du brasseur|changé|
|[AwpSNXaKloq2KQNy.htm](equipment-effects/AwpSNXaKloq2KQNy.htm)|Effect: Sanguine Fang - Armor (Greater)|Effet : Canine sanguine supérieure - Armure|changé|
|[bcxVvIbuZWOvsKcA.htm](equipment-effects/bcxVvIbuZWOvsKcA.htm)|Effect: Darkvision Elixir (Moderate)|Effet : Élixir de Vision dans le noir modéré|changé|
|[bP40jr6wE6MCsRvY.htm](equipment-effects/bP40jr6wE6MCsRvY.htm)|Effect: Golden Legion Epaulet|Effet : Épaulette de la Légion dorée|changé|
|[cOcHWeogJFIkEI0d.htm](equipment-effects/cOcHWeogJFIkEI0d.htm)|Effect: Energizing Lattice|Effet : Maille énérgisante|changé|
|[dMbRTs2YvnDwtUEj.htm](equipment-effects/dMbRTs2YvnDwtUEj.htm)|Effect: Phantasmal Doorknob - Weapon (Major)|Effet : Poignée de porte imaginaire majeure - Arme|changé|
|[e6dXfbKzv5sNr1zh.htm](equipment-effects/e6dXfbKzv5sNr1zh.htm)|Effect: Vermin Repellent Agent (Major)|Effet : Agent répulsif de vermine majeur|changé|
|[ehYmO1rFBt35zoOw.htm](equipment-effects/ehYmO1rFBt35zoOw.htm)|Effect: Server's Stew|Effet : Ragoût du serveur|changé|
|[exwQF6E1FWmuxwBc.htm](equipment-effects/exwQF6E1FWmuxwBc.htm)|Effect: Protective Barrier|Effet : Barrière protectrice|changé|
|[ft5LjQSa8mZkklhM.htm](equipment-effects/ft5LjQSa8mZkklhM.htm)|Effect: Polished Demon Horn - Armor|Effet : Corne de démon polie - Armure|changé|
|[gaEXUewHgPpM3zfW.htm](equipment-effects/gaEXUewHgPpM3zfW.htm)|Effect: Jyoti's Feather - Armor (Greater)|Effet : Plume de jyoti supérieure - Armure|changé|
|[gDefAEEMXVVZgqXH.htm](equipment-effects/gDefAEEMXVVZgqXH.htm)|Effect: Celestial Armor|Effet : Armure céleste|changé|
|[hexHnjCuq6vaEZEx.htm](equipment-effects/hexHnjCuq6vaEZEx.htm)|Effect: Sairazul Blue|Effet : Bleu Sairazul|changé|
|[Hnt3Trd7TiFICB06.htm](equipment-effects/Hnt3Trd7TiFICB06.htm)|Effect: Vermin Repellent Agent (Moderate)|Effet : Agent répulsif de vermine modéré|changé|
|[hozXQvKqp62DnawX.htm](equipment-effects/hozXQvKqp62DnawX.htm)|Effect: Jyoti's Feather - Armor|Effet : Plume de jyoti - Armure|changé|
|[IiPqlP4C7YTjkE9w.htm](equipment-effects/IiPqlP4C7YTjkE9w.htm)|Effect: Phantasmal Doorknob - Weapon (Greater)|Effet : Poignée de porte imaginaire supérieure - Arme|changé|
|[Ik2fSeOnaKYy3mkP.htm](equipment-effects/Ik2fSeOnaKYy3mkP.htm)|Effect: Aeon Stone Resonance (Pearly White Spindle)|Effet : Résonance de pierre d'éternité (fuseau blanc perle)|changé|
|[IlNjAwsIZShlVsCT.htm](equipment-effects/IlNjAwsIZShlVsCT.htm)|Effect: Architect's Pattern Book|Effet : Livre des croquis de l'architecte|changé|
|[IxhKGd3nK2lcIpMw.htm](equipment-effects/IxhKGd3nK2lcIpMw.htm)|Effect: Demon's Knot|Effet : Noeur du démon|changé|
|[Ja3PlqLuD9aSaPNZ.htm](equipment-effects/Ja3PlqLuD9aSaPNZ.htm)|Effect: Brewer's Regret (Greater)|Effet : Regret du brasseur supérieur|changé|
|[JnnyamqQrAEcyI6F.htm](equipment-effects/JnnyamqQrAEcyI6F.htm)|Effect: Grim Sandglass - Armor (Greater)|Effet : Sablier sinistre supérieur - Armure|changé|
|[jw6Tr9FbErjLAFLQ.htm](equipment-effects/jw6Tr9FbErjLAFLQ.htm)|Effect: Serene Mutagen (Greater)|Effet : Mutagène de sérénité supérieur|changé|
|[lBMhT2W2raYMa8JS.htm](equipment-effects/lBMhT2W2raYMa8JS.htm)|Effect: Spellguard Shield|Effet : Bouclier Gardesort|changé|
|[lgvjbbQiHBGKR3C6.htm](equipment-effects/lgvjbbQiHBGKR3C6.htm)|Effect: Rhino Shot|Effet : Tir de rhino|changé|
|[M0hhLRC86sASVOk7.htm](equipment-effects/M0hhLRC86sASVOk7.htm)|Effect: Tteokguk of Time Advancement|Effet : Tteokguk du déroulement du temps|changé|
|[m4WpxepWRV1u1Kcw.htm](equipment-effects/m4WpxepWRV1u1Kcw.htm)|Effect: Grim Sandglass - Weapon|Effet : Sablier sinistre - Arme|changé|
|[MCny5ohCGf09a7Wl.htm](equipment-effects/MCny5ohCGf09a7Wl.htm)|Effect: Salve of Slipperiness|Effet : Onguent d'insaisissabilité|changé|
|[mQgA7XmjVI6WG6oq.htm](equipment-effects/mQgA7XmjVI6WG6oq.htm)|Effect: Sanguine Fang - Armor|Effet : Canine sanguine - Armure|changé|
|[NBxTbdCCqmilAxqA.htm](equipment-effects/NBxTbdCCqmilAxqA.htm)|Effect: Whispering Staff (Enemy)|Effet : Bâton de chuchotement - Ennemi|changé|
|[NZAMd9wCof7xxzUr.htm](equipment-effects/NZAMd9wCof7xxzUr.htm)|Effect: Aeon Stone Resonance (Black Disc)|Effet Résonance de pierre d'éternité (disque noir)|changé|
|[OAN5Fj21PJPhIqRU.htm](equipment-effects/OAN5Fj21PJPhIqRU.htm)|Effect: Vermin Repellent Agent (Lesser)|Effet : Agent répulsif de vermine inférieur|changé|
|[oiO3cQfqp8MuxR82.htm](equipment-effects/oiO3cQfqp8MuxR82.htm)|Effect: Blast Boots (Major)|Effet : Bottes d'explosion majeures|changé|
|[omyZyfTnx3uYVgiP.htm](equipment-effects/omyZyfTnx3uYVgiP.htm)|Effect: Arachnid Harness|Effet : Harnais arachnide|changé|
|[ORdhj3IAvYACNGkJ.htm](equipment-effects/ORdhj3IAvYACNGkJ.htm)|Effect: Shrine Inarizushi|Effet : Autel d'Inarizushi|changé|
|[P7Y7pO2ulZ5wBgxU.htm](equipment-effects/P7Y7pO2ulZ5wBgxU.htm)|Effect: Barding of the Zephyr|Effet : Barde du zéphir|changé|
|[P882YXPpESinSvrJ.htm](equipment-effects/P882YXPpESinSvrJ.htm)|Effect: Polished Demon Horn - Weapon (Major)|Effet : Corne de démon polie majeure - Arme|changé|
|[pr12dSHV4nIyVG5n.htm](equipment-effects/pr12dSHV4nIyVG5n.htm)|Effect: Polished Demon Horn - Armor (Greater)|Effet : Corne de démon polie supérieure - Armure|changé|
|[qOBdeZ4FXYc5qHsm.htm](equipment-effects/qOBdeZ4FXYc5qHsm.htm)|Effect: Private Workshop (Using for Crafting)|Effet : Atelier privé (utilisation pour l'artisanat)|changé|
|[QrsPKOFuo3qzgxw5.htm](equipment-effects/QrsPKOFuo3qzgxw5.htm)|Effect: Red-Rib Gill Mask (Greater)|Effet : Masque branchies de côte rouge supérieur|changé|
|[QXJLvL2k3WqlF0SN.htm](equipment-effects/QXJLvL2k3WqlF0SN.htm)|Effect: Grim Sandglass - Armor (Major)|Effet : Sablier sinistre majeur - Armure|changé|
|[Rfw1T5NXIoeUbJzt.htm](equipment-effects/Rfw1T5NXIoeUbJzt.htm)|Effect: Polished Demon Horn - Armor (Major)|Effet : Corne de démon polie majeure - Armure|changé|
|[rH6RHxy6sNTLusKX.htm](equipment-effects/rH6RHxy6sNTLusKX.htm)|Effect: Emerald Fulcrum Lens (Saving Throw)|Effet : Lentille à pivot émeraude (jet de sauvegarde)|changé|
|[s95P3L72BDKvzYhn.htm](equipment-effects/s95P3L72BDKvzYhn.htm)|Effect: Curse of Potent Poison|Effet : Malédiction du poison puissant|changé|
|[t7VUJHSUT6bkVUjg.htm](equipment-effects/t7VUJHSUT6bkVUjg.htm)|Effect: Serene Mutagen (Major)|Effet : Mutagène de sérénité majeur|changé|
|[tcHG8NlsYmHdziko.htm](equipment-effects/tcHG8NlsYmHdziko.htm)|Effect: Grim Sandglass - Weapon (Major)|Effet : Sablier sinistre majeur - Arme|changé|
|[tNaFPSbNkcyHS50y.htm](equipment-effects/tNaFPSbNkcyHS50y.htm)|Effect: Polished Demon Horn - Weapon (Greater)|Effet : Corne de démon polie supérieure - Arme|changé|
|[u35Qzft0c84UySq2.htm](equipment-effects/u35Qzft0c84UySq2.htm)|Effect: Ebon Fulcrum Lens (2 Action)|Effet : Lentille à pivot ébène (2 Actions)|changé|
|[UDfVCATxdLdSzJYJ.htm](equipment-effects/UDfVCATxdLdSzJYJ.htm)|Effect: Red-Rib Gill Mask (Lesser)|Effet : Masque branchies de côte rouge inférieur|changé|
|[uVxs1qFMQsGWXNs6.htm](equipment-effects/uVxs1qFMQsGWXNs6.htm)|Effect: Potion of Stable Form|Effet : Potion de forme stable|changé|
|[V4JoVnOfKze8cRan.htm](equipment-effects/V4JoVnOfKze8cRan.htm)|Effect: Grim Sandglass - Armor|Effet : Sablier sinistre - Armure|changé|
|[VrYfR2WuyA15zFhq.htm](equipment-effects/VrYfR2WuyA15zFhq.htm)|Effect: Vermin Repellent Agent (Greater)|Effet : Agent répulsif de vermine supérieur|changé|
|[W9tKQlA7tVIcAuzw.htm](equipment-effects/W9tKQlA7tVIcAuzw.htm)|Effect: Greater Potion of Stable Form|Effet : Potion de forme stable supérieure|changé|
|[wacGBDbbQ1HaNZbX.htm](equipment-effects/wacGBDbbQ1HaNZbX.htm)|Effect: Hyldarf's Fang|Effet : Croc de Hyldarf|changé|
|[WARLTi8unmPgmnNw.htm](equipment-effects/WARLTi8unmPgmnNw.htm)|Effect: Polished Demon Horn - Weapon|Effet : Corne de démon polie - Arme|changé|
|[wFP3SqPoO0bCPmyK.htm](equipment-effects/wFP3SqPoO0bCPmyK.htm)|Effect: Kraken's Guard|Effet : Gardien kraken|changé|
|[xMG5PrT6NvCFYGqI.htm](equipment-effects/xMG5PrT6NvCFYGqI.htm)|Effect: Sanguine Fang - Armor (Major)|Effet : Canine sanguine majeure - Armure|changé|
|[YAZ1iri403S8XcrH.htm](equipment-effects/YAZ1iri403S8XcrH.htm)|Effect: Jyoti's Feather - Armor (Major)|Effet : Plume de jyoti majeure - Armure|changé|
|[yrbz0rZzp8aZEqbv.htm](equipment-effects/yrbz0rZzp8aZEqbv.htm)|Effect: Serene Mutagen (Moderate)|Effet : Mutagène de sérénité modéré|changé|
|[z3ATL8DcRVrT0Uzt.htm](equipment-effects/z3ATL8DcRVrT0Uzt.htm)|Effect: Disarm (Success)|Effet : Désarmer (succès)|changé|
|[zlSNbMDIlTOpcO8R.htm](equipment-effects/zlSNbMDIlTOpcO8R.htm)|Effect: Skinstitch Salve|Effet : Pommade suture|changé|
|[zqKzWGLODgIvtiKf.htm](equipment-effects/zqKzWGLODgIvtiKf.htm)|Effect: Spellguard Blade|Effet : Lame contre-sort|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[02LNcIl70TTy9XS1.htm](equipment-effects/02LNcIl70TTy9XS1.htm)|Effect: Ghostbane Fulu|Effet : Fulu chasse-spectre|libre|
|[05mIW5FMX2socBY9.htm](equipment-effects/05mIW5FMX2socBY9.htm)|Effect: Vashu's Ninth Life|Effet: Neuvième vie de Vashu|libre|
|[0eO7QaOjKDxKv65V.htm](equipment-effects/0eO7QaOjKDxKv65V.htm)|Effect: Therapeutic Snap Peas|Effet : Pois mange-tout thérapeutique|libre|
|[0YbNzbW0HSKtgStQ.htm](equipment-effects/0YbNzbW0HSKtgStQ.htm)|Effect: Dragon's Blood Pudding (Major)|Effet : Poudding de sang de dragon majeur|libre|
|[15qCI8sWaEwPkWiQ.htm](equipment-effects/15qCI8sWaEwPkWiQ.htm)|Effect: Alloy Orb (High-Grade)|Effet : Orbe d'alliage de haute qualité|libre|
|[16tOZk4qy329s2aK.htm](equipment-effects/16tOZk4qy329s2aK.htm)|Effect: Shielding Salve|Effet : Onguent de bouclier|libre|
|[187xZfkgmYqQ0jnV.htm](equipment-effects/187xZfkgmYqQ0jnV.htm)|Effect: Soothing Tonic (Major)|Effet : Tonique apaisant majeur|libre|
|[19ECULG5Zp593jQX.htm](equipment-effects/19ECULG5Zp593jQX.htm)|Effect: Dragon's Blood Pudding (Moderate)|Effet : Poudding de sang de dragon modéré|libre|
|[1ihy7Jvw5PY4WYbP.htm](equipment-effects/1ihy7Jvw5PY4WYbP.htm)|Effect: Eye of the Unseen (Greater)|Effet : Oeil de l'invisible supérieur|libre|
|[1l139A2Qik4lBHKO.htm](equipment-effects/1l139A2Qik4lBHKO.htm)|Effect: Juggernaut Mutagen (Lesser)|Effet : Mutagène de juggernaut inférieur|libre|
|[1mKjaWC65KWPuFR4.htm](equipment-effects/1mKjaWC65KWPuFR4.htm)|Effect: Antidote (Major)|Effet : Antidote majeur|libre|
|[1N28rGPbAl2IkGUf.htm](equipment-effects/1N28rGPbAl2IkGUf.htm)|Effect: Slime Whip|Effet : Fouet gluant|libre|
|[1ouUo8lLK6H79Rqh.htm](equipment-effects/1ouUo8lLK6H79Rqh.htm)|Effect: Bestial Mutagen (Major)|Effet : Mutagène bestial majeur|libre|
|[1S51uIRb9bnZtpFU.htm](equipment-effects/1S51uIRb9bnZtpFU.htm)|Effect: Winged Sandals|Effet : Sandales ailées|libre|
|[1tweTwYQuQUV45wJ.htm](equipment-effects/1tweTwYQuQUV45wJ.htm)|Effect: Rowan Rifle (Cold)|Effet : Fusil de Rowan (froid)|libre|
|[1xHHvQlW4pRR89qj.htm](equipment-effects/1xHHvQlW4pRR89qj.htm)|Effect: Stone Body Mutagen (Moderate)|Effet : Mutagène corps-de-pierre modéré|libre|
|[25hZRK3gPvZHMIah.htm](equipment-effects/25hZRK3gPvZHMIah.htm)|Effect: Resonating Fork - Armor (Major)|Effet : Diapason résonant majeur - Armure|libre|
|[2Bds6d4UGQZqYSZM.htm](equipment-effects/2Bds6d4UGQZqYSZM.htm)|Effect: Quicksilver Mutagen (Greater)|Effet : Mutagène de vif-argent supérieur|libre|
|[2C1HuKDQDGFZuv7l.htm](equipment-effects/2C1HuKDQDGFZuv7l.htm)|Effect: Boulderhead Bock|Effet : Bock têtederoc|libre|
|[2gRnUwzPylifTrUO.htm](equipment-effects/2gRnUwzPylifTrUO.htm)|Effect: Wounding Oil|Effet : Huile sanglante|libre|
|[2MzNdLHge0l8B6sQ.htm](equipment-effects/2MzNdLHge0l8B6sQ.htm)|Effect: Thorn Triad - Weapon (Major)|Effet : Triade d'épines majeure - Arme|libre|
|[2PNo8u4wxSbz5WEs.htm](equipment-effects/2PNo8u4wxSbz5WEs.htm)|Effect: Juggernaut Mutagen (Major)|Effet : Mutagène de juggernaut majeur|libre|
|[2SzLumWCqZQAetcx.htm](equipment-effects/2SzLumWCqZQAetcx.htm)|Effect: Spirit Fan|Effet : Éventail spectral|libre|
|[2YgXoHvJfrDHucMr.htm](equipment-effects/2YgXoHvJfrDHucMr.htm)|Effect: Raise a Shield|Effet : Lever un bouclier|libre|
|[3LhreroLRmI4atE6.htm](equipment-effects/3LhreroLRmI4atE6.htm)|Effect: Clockwork Cloak|Effet : Cape mécanique|libre|
|[3O5lvuX4VHqtpCkU.htm](equipment-effects/3O5lvuX4VHqtpCkU.htm)|Effect: Lover's Gloves|Effet : Gants de l'amant|libre|
|[4aSqtBgvQr2TI3XT.htm](equipment-effects/4aSqtBgvQr2TI3XT.htm)|Effect: Grit (Stage 2)|Effet : Grit (stade 2)|libre|
|[4G9qnI0oRyL6eKFQ.htm](equipment-effects/4G9qnI0oRyL6eKFQ.htm)|Effect: Frost Vial (Major)|Effet : Fiole de givre majeure|libre|
|[4JULykNCgQoypsu8.htm](equipment-effects/4JULykNCgQoypsu8.htm)|Effect: Spiderfoot Brew (Greater)|Effet : Infusion de pattes d'araignée majeure|libre|
|[4tepFOJLhZSelPoa.htm](equipment-effects/4tepFOJLhZSelPoa.htm)|Effect: Dragon Turtle Scale|Effet : Écaille de tortue dragon|libre|
|[4uy4Ygf5KD2WrtGW.htm](equipment-effects/4uy4Ygf5KD2WrtGW.htm)|Effect: Nosoi Charm (Diplomacy)|Effet : Charme de nosoi (Diplomatie)|libre|
|[4XO5mkjnh5riwZPM.htm](equipment-effects/4XO5mkjnh5riwZPM.htm)|Aura: Searing Blade (Greater)|Aura : Lame brûlante supérieure|libre|
|[5fPfP74t4NYnRnnc.htm](equipment-effects/5fPfP74t4NYnRnnc.htm)|Effect: Deck of Destiny|Effet : Jeu du destin|libre|
|[5Gof60StUppR2Xn9.htm](equipment-effects/5Gof60StUppR2Xn9.htm)|Effect: Skeptic's Elixir (Lesser)|Effet : Élixir de l'incrédule inférieur|libre|
|[5JYchreCttBg7RcD.htm](equipment-effects/5JYchreCttBg7RcD.htm)|Effect: Goo Grenade|Effet : Grenade gluante|libre|
|[5KXsyN9J78glG25I.htm](equipment-effects/5KXsyN9J78glG25I.htm)|Effect: Ochre Fulcrum Lens|Effet : Lentille à pivot ocre|libre|
|[5lZWAvm0oGxvF4bm.htm](equipment-effects/5lZWAvm0oGxvF4bm.htm)|Effect: Exsanguinating Ammunition (Greater)|Effet : Munition exsangue supérieure|libre|
|[5o33sch67Z8j5Vom.htm](equipment-effects/5o33sch67Z8j5Vom.htm)|Effect: Lastwall Soup (Improved)|Effet : Soupe de Dernier-Rempart améliorée|libre|
|[5OABp099y6w3didN.htm](equipment-effects/5OABp099y6w3didN.htm)|Effect: Soulspark Candle|Effet : Cierge lueur d'âme|libre|
|[5oYKYXAexr0vhx84.htm](equipment-effects/5oYKYXAexr0vhx84.htm)|Effect: Treat Disease (Critical Failure)|Effet : Soigner les blessures (échec critique)|libre|
|[5uK3fmGlfJrbWQz4.htm](equipment-effects/5uK3fmGlfJrbWQz4.htm)|Effect: Stalwart's Ring|Effet : Anneau de résolution|libre|
|[5WLda1tGUiKoSj1K.htm](equipment-effects/5WLda1tGUiKoSj1K.htm)|Effect: Stalk Goggles (Major)|Effet : Lunettes à tiges majeures|libre|
|[5XdkHIUgc6psBxQq.htm](equipment-effects/5XdkHIUgc6psBxQq.htm)|Effect: Bloodseeker Beak|Effet : Trompe de cherchesang|libre|
|[6alqXfVq0qWQC359.htm](equipment-effects/6alqXfVq0qWQC359.htm)|Effect: Energy Robe of Fire|Effet : Robe d'énergie de feu|libre|
|[6Ao5NDu2J7qGwZxW.htm](equipment-effects/6Ao5NDu2J7qGwZxW.htm)|Effect: Fury Cocktail (Lesser)|Effet : Cocktail de la fureur inférieur|libre|
|[6c4o7ypWWnZKPy5b.htm](equipment-effects/6c4o7ypWWnZKPy5b.htm)|Effect: Ooze Ammunition (Moderate)|Effet : Munition de vase modérée|libre|
|[6dsPjRKjCPd9BWPt.htm](equipment-effects/6dsPjRKjCPd9BWPt.htm)|Effect: Greater Healer's Gel|Effet : Gel du guérisseur supérieur|libre|
|[6enQGnnlgGY6ZWgS.htm](equipment-effects/6enQGnnlgGY6ZWgS.htm)|Effect: Ugly Cute's Gift|Effet : Don de Moche mignon|libre|
|[6fIGdO15P8EbUBWR.htm](equipment-effects/6fIGdO15P8EbUBWR.htm)|Effect: Potion of Sonic Resistance (Lesser)|Effet : Potion de résistance au son inférieure|libre|
|[6lzmzeWRo6mv6g2C.htm](equipment-effects/6lzmzeWRo6mv6g2C.htm)|Effect: Fury Cocktail (Greater)|Effet : Cocktail de la fureur supérieure|libre|
|[6p2Sjl7XxCc55ft4.htm](equipment-effects/6p2Sjl7XxCc55ft4.htm)|Effect: Mudrock Snare (Success)|Effet : Piège artisanal de boue solidifiée (Succès)|libre|
|[6PNLBIdlqqWNCFMy.htm](equipment-effects/6PNLBIdlqqWNCFMy.htm)|Effect: Quicksilver Mutagen (Lesser)|Effet : Mutagène de vif-argent inférieur|libre|
|[6Pwdim01PH447CeS.htm](equipment-effects/6Pwdim01PH447CeS.htm)|Effect: Ooze Ammunition (Greater)|Effet : Munition de vase supérieure|libre|
|[7BHF5iUHtCvfHAie.htm](equipment-effects/7BHF5iUHtCvfHAie.htm)|Effect: Peace in Dreams Tea|Effet : Paix dans le thé des rêves|libre|
|[7dLsA9PAb5ij7Bc6.htm](equipment-effects/7dLsA9PAb5ij7Bc6.htm)|Effect: Dueling Cape|Effet : Cape de duel|libre|
|[7eCUXYunzGVEsaKh.htm](equipment-effects/7eCUXYunzGVEsaKh.htm)|Effect: Deadweight Mutagen (Major)|Effet : Mutagène de poids-mort majeur|libre|
|[7MgpgF8tOXOiDEwv.htm](equipment-effects/7MgpgF8tOXOiDEwv.htm)|Effect: Vaultbreaker's Harness|Effet : Harnais du briseur de coffre|libre|
|[7z1iY4AaNEAIKuAU.htm](equipment-effects/7z1iY4AaNEAIKuAU.htm)|Effect: Antidote (Lesser)|Effet : Antidote inférieur|libre|
|[80JoiuYxrzoEPfVw.htm](equipment-effects/80JoiuYxrzoEPfVw.htm)|Effect: Statue Skin Salve|Effet : Pommade peau de statue|libre|
|[88kqcDmsoAEddzUt.htm](equipment-effects/88kqcDmsoAEddzUt.htm)|Effect: Boots of Elvenkind|Effet : Bottes elfiques|libre|
|[8RNPIAuV7ixaXeq5.htm](equipment-effects/8RNPIAuV7ixaXeq5.htm)|Effect: War Blood Mutagen (Greater)|Effet : Mutagène de sang guerrier supérieur|libre|
|[8YZX34sJOIH32VwI.htm](equipment-effects/8YZX34sJOIH32VwI.htm)|Effect: Illuminated Folio|Effet : Folio enluminé|libre|
|[8zKGaGrkTd8ALqJO.htm](equipment-effects/8zKGaGrkTd8ALqJO.htm)|Effect: Potion of Fire Resistance (Moderate)|Effet : Potion de résistance au feu modérée|libre|
|[988f6NpOo4YzFzIr.htm](equipment-effects/988f6NpOo4YzFzIr.htm)|Effect: Quicksilver Mutagen (Major)|Effet : Mutagène de vif-argent majeur|libre|
|[9BunTo2HasWB0vwc.htm](equipment-effects/9BunTo2HasWB0vwc.htm)|Aura: Demon's Knot|Aura : Noeud du démon|libre|
|[9FfFhu2kl2wMTsiI.htm](equipment-effects/9FfFhu2kl2wMTsiI.htm)|Effect: Silvertongue Mutagen (Major)|Effet : Mutagène de langue dorée majeur|libre|
|[9j1uTGBGAc7GIhjm.htm](equipment-effects/9j1uTGBGAc7GIhjm.htm)|Effect: Dragonfly Potion|Effet : Potion libellule|libre|
|[9keegq0GdS1eSrNr.htm](equipment-effects/9keegq0GdS1eSrNr.htm)|Effect: Sea Touch Elixir (Moderate)|Effet : Élixir de caresse marine modéré|libre|
|[9kOgG7BPEfIyWyqm.htm](equipment-effects/9kOgG7BPEfIyWyqm.htm)|Effect: Rowan Rifle (Electricity)|Effet : Fusil de Rowan (électricité)|libre|
|[9MeHc072G4L8AJkp.htm](equipment-effects/9MeHc072G4L8AJkp.htm)|Effect: Elixir of Life (True)|Effet : Élixir de vie ultime|libre|
|[9mIS76oZkxXQ4g3T.htm](equipment-effects/9mIS76oZkxXQ4g3T.htm)|Effect: Jolt Coil - Armor|Effet : Bobine d'électrochoc - Armure|libre|
|[9MU9d8tmJg3thkSc.htm](equipment-effects/9MU9d8tmJg3thkSc.htm)|Effect: Alloy Orb (Low-Grade)|Effet : Orbe d'alliage de basse qualité|libre|
|[9PASRixhNM0ogqmG.htm](equipment-effects/9PASRixhNM0ogqmG.htm)|Effect: Triton's Conch|Effet : Conque de triton|libre|
|[Acox3S5hpJAqq1jc.htm](equipment-effects/Acox3S5hpJAqq1jc.htm)|Effect: Potency Crystal (Greater)|Effet : Cristal de puissance supérieure|libre|
|[agDVcRyoS4NTHkht.htm](equipment-effects/agDVcRyoS4NTHkht.htm)|Effect: Trinity Geode - Armor (Major)|Effet : Géode de la trinité majeure - Armure|libre|
|[ah41XCrV4LFsVyzl.htm](equipment-effects/ah41XCrV4LFsVyzl.htm)|Effect: Shield of the Unified Legion|Effet : Bouclier de la Légion unifiée|libre|
|[aIZsC56OdotiGb9M.htm](equipment-effects/aIZsC56OdotiGb9M.htm)|Effect: War Blood Mutagen (Lesser)|Effet : Mutagène de sang guerrier inférieur|libre|
|[AJLURqXGLalmV9wN.htm](equipment-effects/AJLURqXGLalmV9wN.htm)|Effect: Energizing Tea|Effet : Thé énergisant|libre|
|[AJx8i8QX35vsG5Q4.htm](equipment-effects/AJx8i8QX35vsG5Q4.htm)|Effect: Stonethroat Ammunition (Success)|Effet : Munition coince-gueule (Succès)|libre|
|[AMhUb42NAJ1aisZp.htm](equipment-effects/AMhUb42NAJ1aisZp.htm)|Effect: Stone Fist Elixir|Effet : Élixir de poing de pierre|libre|
|[ApGnHnZEK7nv3IqL.htm](equipment-effects/ApGnHnZEK7nv3IqL.htm)|Effect: Greater Codex of Unimpeded Sight|Effet : Codex de vision sans entraves supérieur|libre|
|[AStWCzJcQCwO9Fjr.htm](equipment-effects/AStWCzJcQCwO9Fjr.htm)|Effect: Longnight Tea|Effet : Thé de la longue nuit|libre|
|[AUoiLqENVZlZohsn.htm](equipment-effects/AUoiLqENVZlZohsn.htm)|Effect: Spined Shield Spines|Effet : Pointes de bouclier de la manticore|libre|
|[AvXNZ9I6s1H8C4wd.htm](equipment-effects/AvXNZ9I6s1H8C4wd.htm)|Effect: War Blood Mutagen (Moderate)|Effet : Mutagène de sang guerrier modéré|libre|
|[aXDtl9vMp1vIznya.htm](equipment-effects/aXDtl9vMp1vIznya.htm)|Effect: Eye of the Unseen|Effet : Oeil de l'invisible|libre|
|[b9DTIJyBT8kvIBpj.htm](equipment-effects/b9DTIJyBT8kvIBpj.htm)|Effect: Stone Body Mutagen (Greater)|Effet : Mutagène corps-de-pierre supérieur|libre|
|[BADJFyRYRxxxI16e.htm](equipment-effects/BADJFyRYRxxxI16e.htm)|Effect: Resonating Fork - Armor (Greater)|Effet : Diapason résonant supérieur - Armure|libre|
|[Bg4hNMqBx0yqmWYJ.htm](equipment-effects/Bg4hNMqBx0yqmWYJ.htm)|Effect: Clockwork Goggles (Major)|Effet : Lunettes mécaniques majeures|libre|
|[bHBehLniirjNAnVh.htm](equipment-effects/bHBehLniirjNAnVh.htm)|Effect: Spiny Lodestone - Armor|Effet : Pierre aimantée épineuse - armure|libre|
|[bIOHtDiqtJZB86tV.htm](equipment-effects/bIOHtDiqtJZB86tV.htm)|Effect: Numbing Tonic (True)|Effet : tonique anesthésiant ultime|libre|
|[BjF0eOao5UuOOKMP.htm](equipment-effects/BjF0eOao5UuOOKMP.htm)|Effect: Alloy Orb (Exquisite Standard-Grade)|Effet : Orbe d'alliage raffiné de qualité standard|libre|
|[bKEx53h6lrFOYvpu.htm](equipment-effects/bKEx53h6lrFOYvpu.htm)|Effect: Sanguine Fang - Weapon|Effet : Canine sanguine - Arme|libre|
|[bMZ6Gw58sK8sFp5n.htm](equipment-effects/bMZ6Gw58sK8sFp5n.htm)|Effect: Crackling Bubble Gum (Failure)|Effet : Chewing-gum crépitant (échec)|libre|
|[bri7UVNCfHhCIvXN.htm](equipment-effects/bri7UVNCfHhCIvXN.htm)|Effect: Immolation Clan Pistol|Effet : Pistolet de clan d'immolation|libre|
|[BTWuGksjSU1SYUcf.htm](equipment-effects/BTWuGksjSU1SYUcf.htm)|Effect: Numbing Tonic (Minor)|Effet : Tonique anesthésiant mineur|libre|
|[buJnkFBzL4e22ASp.htm](equipment-effects/buJnkFBzL4e22ASp.htm)|Effect: Gecko Potion|Effet : Potion de Gecko|libre|
|[BUTZM5XEhAtAu7OB.htm](equipment-effects/BUTZM5XEhAtAu7OB.htm)|Effect: Potion of Cold Resistance (Lesser)|Effet : Potion de résistance au froid inférieure|libre|
|[BV8RPntjc9FUzD3g.htm](equipment-effects/BV8RPntjc9FUzD3g.htm)|Effect: Drakeheart Mutagen (Moderate)|Effet : Mutagène de coeur de drake modéré|libre|
|[Bwh3kiNX8nxEwVZ5.htm](equipment-effects/Bwh3kiNX8nxEwVZ5.htm)|Effect: Sanguine Fang - Weapon (Greater)|Effet : Canine sanguine supérieure - Arme|libre|
|[c0URo81HpSmCkuQc.htm](equipment-effects/c0URo81HpSmCkuQc.htm)|Effect: Energy Robe of Electricity|Effet : Robe d'énergie d'électricité|libre|
|[C9Tnl6Q7Z5Sbw5EY.htm](equipment-effects/C9Tnl6Q7Z5Sbw5EY.htm)|Effect: Energy Mutagen (Lesser)|Effet : Mutagène d'énergie inférieur|libre|
|[ccMa75bqXo3ZnlHM.htm](equipment-effects/ccMa75bqXo3ZnlHM.htm)|Effect: Five-Feather Wreath - Armor (Major)|Effet : Couronne à cinq plumes majeure - Armure|libre|
|[CDfixYwjfjoXkOVq.htm](equipment-effects/CDfixYwjfjoXkOVq.htm)|Effect: Storm Breath|Effet : Souffle tempête|libre|
|[ceIvpmxWqBJpBHIn.htm](equipment-effects/ceIvpmxWqBJpBHIn.htm)|Effect: Jolt Coil - Weapon (Major)|Effet : Bobine d'éléctrochoc majeure - Arme|libre|
|[cg5qyeMJUh6b4fta.htm](equipment-effects/cg5qyeMJUh6b4fta.htm)|Effect: Belt of the Five Kings (Wearer)|Effet : Ceinture des cinq rois (Porteur)|libre|
|[CIfqUEC0mITBjwmL.htm](equipment-effects/CIfqUEC0mITBjwmL.htm)|Effect: Sarkorian God-Caller Garb|Effet : Tenue d'invocateur divin Sarkorien|libre|
|[cjQHrvoXDCGOsptN.htm](equipment-effects/cjQHrvoXDCGOsptN.htm)|Effect: Flask of Fellowship|Effet : Gourde de fraternité|libre|
|[COVEnItnyRmx42EY.htm](equipment-effects/COVEnItnyRmx42EY.htm)|Effect: Hunter's Brooch|Effet : Broche du chasseur|libre|
|[cozi2kUELY40Dcv3.htm](equipment-effects/cozi2kUELY40Dcv3.htm)|Effect: Malleable Mixture (Lesser)|Effet : Mixture de mollesse inférieure|libre|
|[csA4UAD2tQq7RjT8.htm](equipment-effects/csA4UAD2tQq7RjT8.htm)|Effect: Tanglefoot Bag (Greater)|Effet : Bombe collante supérieure|libre|
|[Cxa7MdgMCUoMqbKm.htm](equipment-effects/Cxa7MdgMCUoMqbKm.htm)|Effect: Bronze Bull Pendant|Effet : Pendentif de taureau de bronze|libre|
|[cy42NXgx1vjYzSxN.htm](equipment-effects/cy42NXgx1vjYzSxN.htm)|Effect: Suit of Armoire Frustration|Effet : Frustration de la panoplie d'armoire|libre|
|[czdEHtLsrUcZxSDx.htm](equipment-effects/czdEHtLsrUcZxSDx.htm)|Effect: Crushing (Greater)|Effet : Écrasante supérieure|libre|
|[d6SzOHtupw1I5EFQ.htm](equipment-effects/d6SzOHtupw1I5EFQ.htm)|Effect: Blood Booster (Greater)|Effet : Activateur sanguin supérieur|libre|
|[d7BDxmsnM1BUoEeT.htm](equipment-effects/d7BDxmsnM1BUoEeT.htm)|Effect: Obsidian Goggles (Greater)|Effet : Lunettes d'obsidienne supérieures|libre|
|[D7teqZ68L21aZCpd.htm](equipment-effects/D7teqZ68L21aZCpd.htm)|Effect: Glittering Snare (Failure)|Effet : Piège scintillant (Échec)|libre|
|[D8cI0uhI3YFFQssp.htm](equipment-effects/D8cI0uhI3YFFQssp.htm)|Effect: Phantasmal Doorknob - Armor (Major)|Effet : Poignée de porte imaginaire majeure - Armure|libre|
|[DaN3N9bOzqDhOng0.htm](equipment-effects/DaN3N9bOzqDhOng0.htm)|Effect: Pickled Demon Tongue - Weapon (Major)|Effet : Langue de démon marinée majeure - Arme|libre|
|[dchlrZqQ2oEmgNlN.htm](equipment-effects/dchlrZqQ2oEmgNlN.htm)|Effect: Silkspinner's Shield (Animated Strike)|Effet : Bouclier de la fileuse de soie (Frappe animée)|libre|
|[dEsaVzTWOctpl8XP.htm](equipment-effects/dEsaVzTWOctpl8XP.htm)|Effect: Numbing Tonic (Major)|Effet : Tonique anesthésiant majeur|libre|
|[DfAyZW2vkhTygZVC.htm](equipment-effects/DfAyZW2vkhTygZVC.htm)|Effect: Prepared Camouflage Suit (Superb)|Effet : Tenue de camouflage superbe préparée|libre|
|[dfnhwI5pJgLtXh2k.htm](equipment-effects/dfnhwI5pJgLtXh2k.htm)|Effect: Silver Salve|Effet : Apprêt d'argent|libre|
|[DlqcczhwjfaEf7G1.htm](equipment-effects/DlqcczhwjfaEf7G1.htm)|Effect: Ablative Armor Plating (Greater)|Effet : Blindage ablatif d'armure supérieur|libre|
|[doyduaLONE2FVxAc.htm](equipment-effects/doyduaLONE2FVxAc.htm)|Effect: Numbing Tonic (Greater)|Effet : Tonique anesthésiant supérieur|libre|
|[dpIrjd1UPY7EnWUD.htm](equipment-effects/dpIrjd1UPY7EnWUD.htm)|Effect: Silvertongue Mutagen (Lesser)|Effet : Mutagène de langue dorée inférieur|libre|
|[dv0IKm5syOdP759w.htm](equipment-effects/dv0IKm5syOdP759w.htm)|Effect: Frost Vial (Moderate)|Effet : Fiole de givre modérée|libre|
|[E2uy6gqOXi1HRVBU.htm](equipment-effects/E2uy6gqOXi1HRVBU.htm)|Effect: Clockwork Goggles (Greater)|Effet : Lunettes mécaniques supérieures|libre|
|[e3RzlURndODzBnMt.htm](equipment-effects/e3RzlURndODzBnMt.htm)|Effect: Grit (Stage 1)|Effet : Grit (stade 1)|libre|
|[E4B02mJmNexQLa8F.htm](equipment-effects/E4B02mJmNexQLa8F.htm)|Effect: Inspiring Spotlight|Effet : Projecteur inspirant|libre|
|[ECGvrM0eAaJlm1VC.htm](equipment-effects/ECGvrM0eAaJlm1VC.htm)|Effect: Desolation Locket - Armor (Greater)|Effet : Médaillon de la désolation supérieure - Armure|libre|
|[Ee2xfKX1yyqGIDZj.htm](equipment-effects/Ee2xfKX1yyqGIDZj.htm)|Effect: Treat Disease (Success)|Effet : Soigner une maladie (succès)|libre|
|[eeGWTG9ZAha4IIOY.htm](equipment-effects/eeGWTG9ZAha4IIOY.htm)|Effect: Cloak of Elvenkind|Effet : Cape elfique|libre|
|[EeU33F3eBq5OPYp0.htm](equipment-effects/EeU33F3eBq5OPYp0.htm)|Effect: Injigo's Loving Embrace|Effet: L'Etreinte aimante d'Injigo|libre|
|[Eh4ezYhfUlNc775v.htm](equipment-effects/Eh4ezYhfUlNc775v.htm)|Effect: Grindlegrub Steak|Effet : Steak d'hachticot|libre|
|[eh7EqmDBDW30ShCu.htm](equipment-effects/eh7EqmDBDW30ShCu.htm)|Effect: Bravo's Brew (Lesser)|Effet : Breuvage de bravoure inférieur|libre|
|[EjLVjt3GMeHM0Ai3.htm](equipment-effects/EjLVjt3GMeHM0Ai3.htm)|Effect: Ghostcaller's Planchette - Armor|Effet : Planchette de l'invocateur de fantôme - Armure|libre|
|[eLQABqabYp41Mw1R.htm](equipment-effects/eLQABqabYp41Mw1R.htm)|Effect: Immortal Bastion|Effet : Bastion immortel|libre|
|[Empv55xGFN2Mk3cu.htm](equipment-effects/Empv55xGFN2Mk3cu.htm)|Effect: Warding Statuette - Armor|Effet : Statuette de protection - Armure|libre|
|[eNVSBXuOiAaN152C.htm](equipment-effects/eNVSBXuOiAaN152C.htm)|Effect: Energized Cartridge (Electricity)|Effet : Cartouche énergisée (électricité)|libre|
|[EpB7yJPEuG6ez4z3.htm](equipment-effects/EpB7yJPEuG6ez4z3.htm)|Effect: Elixir of Life (Lesser)|Effet : Élixir de vie inférieur|libre|
|[EpNflrkmWzQ0lEb4.htm](equipment-effects/EpNflrkmWzQ0lEb4.htm)|Effect: Glaive of the Artist|Effet : Glaive de l'artiste|libre|
|[EPqnA5OlNpwr41Os.htm](equipment-effects/EPqnA5OlNpwr41Os.htm)|Effect: Beastmaster's Sigil - Ranged Weapon|Effet : Symbole du maître des bêtes - Arme à distance|libre|
|[eQqi3tWSHwV4SHqK.htm](equipment-effects/eQqi3tWSHwV4SHqK.htm)|Effect: South Wind's Scorch Song (Speed Boost)|Effet : Chanson brûlante du vent du sud (Boost de vitesse)|libre|
|[EqXWI80FBz59VC6v.htm](equipment-effects/EqXWI80FBz59VC6v.htm)|Effect: Numbing Tonic (Moderate)|Effet : Tonique anesthésiant modéré|libre|
|[eSIYyxi6uTKiP6W5.htm](equipment-effects/eSIYyxi6uTKiP6W5.htm)|Effect: Improvised Weapon|Effet : Arme improvisée|libre|
|[ESuBosh3t1pXEcBj.htm](equipment-effects/ESuBosh3t1pXEcBj.htm)|Effect: Treat Poison (Critical Failure)|Effet : Soigner l'empoisonnement (échec critique)|libre|
|[etJW0w4CiSFgMrWP.htm](equipment-effects/etJW0w4CiSFgMrWP.htm)|Effect: Aeon Stone (Orange Prism) (Nature)|Effet : Pierre d'éternité amplificatrice (Nature)|libre|
|[EwHufLQI1z1QzqZU.htm](equipment-effects/EwHufLQI1z1QzqZU.htm)|Effect: Jolt Coil - Weapon (Greater)|Effet : Bobine d'électrochoc supérieure - Arme|libre|
|[eyXUwWx5omiQWGZw.htm](equipment-effects/eyXUwWx5omiQWGZw.htm)|Effect: Aeon Stone (Sprouting)|Effet : Pierre d'éternité d'éclosion|libre|
|[EZsHuGGSLER67CiM.htm](equipment-effects/EZsHuGGSLER67CiM.htm)|Effect: Potion of Emergency Escape|Effet : Potion de retraite accélérée|libre|
|[f2U0pvTwqrLYyOlC.htm](equipment-effects/f2U0pvTwqrLYyOlC.htm)|Effect: Azarim|Effet : Azarim|libre|
|[F6YMKMbhzWBd9A07.htm](equipment-effects/F6YMKMbhzWBd9A07.htm)|Effect: Shimmering Dust|Effet : Poussière chatoyante|libre|
|[F8nQOLVWmpp9G5hZ.htm](equipment-effects/F8nQOLVWmpp9G5hZ.htm)|Effect: Dragon's Blood Pudding (Greater)|Effet : Poudding de sang de dragon supérieur|libre|
|[Fb4QI4zlmoqfwHY0.htm](equipment-effects/Fb4QI4zlmoqfwHY0.htm)|Effect: Wyrm Claw - Armor (Major)|Effet : Griffe de ver majeure - Armure|libre|
|[FbFl95WRpzrrijh3.htm](equipment-effects/FbFl95WRpzrrijh3.htm)|Effect: Aeon Stone (Orange Prism) (Religion)|Effet : Pierre d'éternité amplificatrice (Religion)|libre|
|[fbSFwwp60AuDDKpK.htm](equipment-effects/fbSFwwp60AuDDKpK.htm)|Effect: Belt of the Five Kings (Allies)|Effet : Ceinture des cinq rois (Alliés)|libre|
|[FDSl6DFblUjITOgP.htm](equipment-effects/FDSl6DFblUjITOgP.htm)|Effect: Choker-Arm Mutagen (Lesser)|Effet : Mutagène d'assouplissement inférieur|libre|
|[fIpzDpuwLdIS4tW5.htm](equipment-effects/fIpzDpuwLdIS4tW5.htm)|Effect: Bestial Mutagen (Lesser)|Effet : Mutagène bestial inférieur|libre|
|[FnCC5RNvnlWB2bmb.htm](equipment-effects/FnCC5RNvnlWB2bmb.htm)|Effect: Stormbreaker Fulu|Effet : Fulu Brise-tempête|libre|
|[Fngb79C1VDGLJ1EQ.htm](equipment-effects/Fngb79C1VDGLJ1EQ.htm)|Effect: Feyfoul (Lesser)|Effet : Cafouillefée inférieur|libre|
|[FOZXp7QQDnny1600.htm](equipment-effects/FOZXp7QQDnny1600.htm)|Effect: Fire and Iceberg|Effet : Feu et iceberg|libre|
|[FPvMbKX8sXM9fGJT.htm](equipment-effects/FPvMbKX8sXM9fGJT.htm)|Effect: Ooze Ammunition (Lesser)|Effet : Munition de vase inférieure|libre|
|[FqSsAjkaHfVFCw0P.htm](equipment-effects/FqSsAjkaHfVFCw0P.htm)|Effect: Soothing Tonic (Lesser)|Effet : Tonique apaisant inférieur|libre|
|[fRlvmul3LbLo2xvR.htm](equipment-effects/fRlvmul3LbLo2xvR.htm)|Effect: Parry|Effet : Parade|libre|
|[fszJ9ZaslnrW5m5z.htm](equipment-effects/fszJ9ZaslnrW5m5z.htm)|Effect: Nimbus Breath|Effet : Souffle nimbus|libre|
|[FTj94xTqZbaCs4jT.htm](equipment-effects/FTj94xTqZbaCs4jT.htm)|Effect: Thorn Triad - Weapon|Effet : Triade d'épines - Arme|libre|
|[fuQVJiPPUsvL6fi5.htm](equipment-effects/fuQVJiPPUsvL6fi5.htm)|Effect: Sulfur Bomb (Failure)|Effet : Bombe de soufre (Échec)|libre|
|[fUrZ4xcMJz0CfTyG.htm](equipment-effects/fUrZ4xcMJz0CfTyG.htm)|Effect: Juggernaut Mutagen (Moderate)|Effet : Mutagène de juggernaut modéré|libre|
|[FV5d8zdo8CGZuRbg.htm](equipment-effects/FV5d8zdo8CGZuRbg.htm)|Effect: Spun Cloud (Blue)|Effet : Nuage tourbillonant bleu|libre|
|[fYe48HmFgfmcqbvL.htm](equipment-effects/fYe48HmFgfmcqbvL.htm)|Effect: Taljjae's Mask (The Hero)|Effet : Masque de Taljjae (le héros)|libre|
|[fYjvLx9DHIdCHdDx.htm](equipment-effects/fYjvLx9DHIdCHdDx.htm)|Effect: Applereed Mutagen (Moderate)|Effet : Mutagène pousse-roseau modéré|libre|
|[fYZIanbYu0Vc4JEL.htm](equipment-effects/fYZIanbYu0Vc4JEL.htm)|Effect: Tanglefoot Bag (Lesser)|Effet : Bombe collante inférieure|libre|
|[Fz3cSffzDAxhCh2D.htm](equipment-effects/Fz3cSffzDAxhCh2D.htm)|Effect: Exsanguinating Ammunition|Effet : Munition exsangue|libre|
|[G0lG7IIZnCZtYi6v.htm](equipment-effects/G0lG7IIZnCZtYi6v.htm)|Effect: Breastplate of Command|Effet : Cuirasse de commandement|libre|
|[g1EdALRxZmmEkSd7.htm](equipment-effects/g1EdALRxZmmEkSd7.htm)|Effect: Hongrui's Gratitude (Umbrella)|Effett : Gratitude d'Hongrui - Parapluie|libre|
|[g8JS6wsw5sRWOJLg.htm](equipment-effects/g8JS6wsw5sRWOJLg.htm)|Effect: Stalk Goggles|Effet : Lunettes à tiges|libre|
|[gAQaizpMbZLDbzg7.htm](equipment-effects/gAQaizpMbZLDbzg7.htm)|Effect: Rime Crystal - Armor|Effet : Cristal gelé - Armure|libre|
|[GBBjw61g4ekJymT0.htm](equipment-effects/GBBjw61g4ekJymT0.htm)|Effect: Drakeheart Mutagen (Lesser)|Effet : Mutagène de coeur de drake inférieur|libre|
|[giTLR7zUHTs9ysBQ.htm](equipment-effects/giTLR7zUHTs9ysBQ.htm)|Effect: Potion of Cold Resistance (Greater)|Effet : Potion de résistance au froid|libre|
|[Gj6u2Za5okFlsTvT.htm](equipment-effects/Gj6u2Za5okFlsTvT.htm)|Effect: Deadweight Snare (Failure/Critical Failure)|Effet : Piège du poids-mort (Échec/Échec critique)|libre|
|[gn7kNxKGUsKwnv1L.htm](equipment-effects/gn7kNxKGUsKwnv1L.htm)|Effect: Shining Symbol|Effet : Symbole lumineux|libre|
|[GNFNDyx8nfNXrgV6.htm](equipment-effects/GNFNDyx8nfNXrgV6.htm)|Effect: Glittering Snare (Critical Failure)|Effet : Piège scintillant (Échec critique)|libre|
|[GqXIV46JqB8x8eEN.htm](equipment-effects/GqXIV46JqB8x8eEN.htm)|Effect: Book of Warding Prayers|Effet : Livre des prières de protection|libre|
|[grXFmNl8Zy3VRVpR.htm](equipment-effects/grXFmNl8Zy3VRVpR.htm)|Effect: Ghostcaller's Planchette - Weapon|Effet : Planchette de l'invocateur de fantôme - Arme|libre|
|[gU3uZE2ihLnpQN0b.htm](equipment-effects/gU3uZE2ihLnpQN0b.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Greater)|Effet : Symbole du maître des bêtes supérieur - Arme au corps-à-corps|libre|
|[GUHNFlNYiR38sTDE.htm](equipment-effects/GUHNFlNYiR38sTDE.htm)|Effect: Crackling Bubble Gum|Effet : Chewing-gum crépitant|libre|
|[gwiyabYi92R97bXZ.htm](equipment-effects/gwiyabYi92R97bXZ.htm)|Effect: Warding Statuette - Weapon|Effet : Statuette de protection - arme|libre|
|[GXEkmsf4Wre6zqG1.htm](equipment-effects/GXEkmsf4Wre6zqG1.htm)|Effect: Alloy Orb (Standard-Grade)|Effet : Orbe d'alliage de qualité standard|libre|
|[gZOED4T3o6giterN.htm](equipment-effects/gZOED4T3o6giterN.htm)|Effect: Beastmaster's Sigil - Armor (Greater)|Effet : Symbole du maître des bâtes supérieur - Armure|libre|
|[h0Zh8tDF9zJBHZXA.htm](equipment-effects/h0Zh8tDF9zJBHZXA.htm)|Effect: Flaming Star - Weapon (Major)|Effet : Étoile enflammée majeure - arme|libre|
|[H29JukjrSpHe5DXR.htm](equipment-effects/H29JukjrSpHe5DXR.htm)|Effect: Impossible Cake|Effet : Gâteau de l'impossible|libre|
|[h9ukqhz7qgNuOlxh.htm](equipment-effects/h9ukqhz7qgNuOlxh.htm)|Effect: Potion of Electricity Resistance (Lesser)|Effet : Potion de résistancee à l'électricité inférieure|libre|
|[hAdyMxuWJu7piQSS.htm](equipment-effects/hAdyMxuWJu7piQSS.htm)|Effect: Ichthyosis Mutagen|Effet : Mutagène de peau squameuse|libre|
|[haywlcUtG6hV1LAy.htm](equipment-effects/haywlcUtG6hV1LAy.htm)|Effect: Trinity Geode - Armor (Greater)|Effet : Géode de la trinité supérieure - Armure|libre|
|[HaZ5LB1wh1LY5wUy.htm](equipment-effects/HaZ5LB1wh1LY5wUy.htm)|Effect: Potion of Minute Echoes|Effet : Potion de l'écho-minute|libre|
|[hD0dUWYKM8FrVDZY.htm](equipment-effects/hD0dUWYKM8FrVDZY.htm)|Effect: Crown of the Kobold King|Effet : Couronne du roi kobold|libre|
|[HeRHBo2NaKy5IxhU.htm](equipment-effects/HeRHBo2NaKy5IxhU.htm)|Effect: Antiplague (Moderate)|Effet : Antimaladie modéré|libre|
|[HlUL7FmGLyS35Jpp.htm](equipment-effects/HlUL7FmGLyS35Jpp.htm)|Effect: Choker-Arm Mutagen (Greater)|Effet : Mutagène d'assouplissement supérieur|libre|
|[HoZWmT4yvGso7pHM.htm](equipment-effects/HoZWmT4yvGso7pHM.htm)|Effect: Flaming Star - Weapon (Greater)|Effet : Étoile enflammée supérieure - arme|libre|
|[hPxrIpuL54XRlA2h.htm](equipment-effects/hPxrIpuL54XRlA2h.htm)|Effect: Earplugs|Effet : bouchons d'oreilles|libre|
|[Hx4MOTujp5z6SlQu.htm](equipment-effects/Hx4MOTujp5z6SlQu.htm)|Effect: Arboreal's Revenge (Speed Penalty)|Effet : Vengeance de l'arboréen (pénalité à la vitesse)|libre|
|[hy6LAC13QIJNDYXm.htm](equipment-effects/hy6LAC13QIJNDYXm.htm)|Effect: South Wind's Scorch Song (Damage)|Effet : Chanson brûlante du vent du sud (Dégâts)|libre|
|[i0tm2ZHekp7rGGR3.htm](equipment-effects/i0tm2ZHekp7rGGR3.htm)|Effect: Stole of Civility|Effet : Étole de civilité|libre|
|[i5agc4lBE6GfeCXq.htm](equipment-effects/i5agc4lBE6GfeCXq.htm)|Effect: Cold Iron Blanch (Lesser)|Effet : Blanchis en fer froid inférieur|libre|
|[IBu1RWKW5JX70nse.htm](equipment-effects/IBu1RWKW5JX70nse.htm)|Effect: Choker-Arm Mutagen (Moderate)|Effet : Mutagène d'assouplissement modéré|libre|
|[id20P4pj7zDKeLmy.htm](equipment-effects/id20P4pj7zDKeLmy.htm)|Effect: Treat Disease (Critical Success)|Effet : Soigner la maladie (succès critique)|libre|
|[iEkH8BKLMUa2wxLX.htm](equipment-effects/iEkH8BKLMUa2wxLX.htm)|Effect: Glamorous Buckler|Effet : Targe étincelante|libre|
|[IFHqrbQUOT2LzNDB.htm](equipment-effects/IFHqrbQUOT2LzNDB.htm)|Effect: Deadweight Mutagen (Greater)|Effet : Mutagène de poids-mort supérieur|libre|
|[IiDpW99zrh7zHxmQ.htm](equipment-effects/IiDpW99zrh7zHxmQ.htm)|Effect: Rime Crystal - Weapon|Effet : Cristal gelé - Arme|libre|
|[iK6JeCsZwm5Vakks.htm](equipment-effects/iK6JeCsZwm5Vakks.htm)|Effect: Anklets of Alacrity|Effet : Chaînes de chevilles d'alacrité|libre|
|[IlTS2LTwYTyGXY49.htm](equipment-effects/IlTS2LTwYTyGXY49.htm)|Effect: Feyfoul (Moderate)|Effet : Cafouillefée modéré|libre|
|[inqXnzrqYzbUBuOj.htm](equipment-effects/inqXnzrqYzbUBuOj.htm)|Effect: Diplomat's Charcuterie|Effet : Charcuterie du diplomate|libre|
|[ioGzmVSmMGXWWBYb.htm](equipment-effects/ioGzmVSmMGXWWBYb.htm)|Effect: Cloak of the Bat|Effet : Cape de la chauve-souris|libre|
|[iOY4XqVZNiQ5esdu.htm](equipment-effects/iOY4XqVZNiQ5esdu.htm)|Effect: Pickled Demon Tongue - Armor (Major)|Effet : Langue de démon marinée majeure - Armure|libre|
|[ITAFsW3dQPupJ3DW.htm](equipment-effects/ITAFsW3dQPupJ3DW.htm)|Effect: Tanglefoot Bag (Major)|Effet : Bombe collante majeure|libre|
|[IZkHdaqWBJIIWO7F.htm](equipment-effects/IZkHdaqWBJIIWO7F.htm)|Effect: Ebon Fulcrum Lens (Reaction)|Effet : Lentille à pivot ébène (Réaction)|libre|
|[J0YS8mQsQ1BmT6Xv.htm](equipment-effects/J0YS8mQsQ1BmT6Xv.htm)|Effect: Emberheart|Effet : Coeur de braise|libre|
|[j9zVZwRBVAcnpEkE.htm](equipment-effects/j9zVZwRBVAcnpEkE.htm)|Effect: Cheetah's Elixir (Moderate)|Effet : Élixir du guépard modéré|libre|
|[jaBMZKdoywOTrQvP.htm](equipment-effects/jaBMZKdoywOTrQvP.htm)|Effect: Cognitive Mutagen (Lesser)|Effet : Mutagène cognitif inférieur|libre|
|[JaPNzmgD7p7hHH8o.htm](equipment-effects/JaPNzmgD7p7hHH8o.htm)|Effect: Thorn Triad - Armor|Effet : Triade d'épines - Armure|libre|
|[JbJykktAYMR4BRav.htm](equipment-effects/JbJykktAYMR4BRav.htm)|Effect: Energy Mutagen (Greater)|Effet : Mutagène d'énergie supérieur|libre|
|[JCMACUq7FkEksV0Q.htm](equipment-effects/JCMACUq7FkEksV0Q.htm)|Effect: Glowing Lantern Fruit (Lantern Light)|Effet : Fruit lanterne brillant - Lumière de lanterne|libre|
|[jgaDboqENQJaS1sW.htm](equipment-effects/jgaDboqENQJaS1sW.htm)|Effect: Prepared Camouflage Suit|Effet : Tenue de camouflage préparée|libre|
|[JGlkg3US7Tk1SirV.htm](equipment-effects/JGlkg3US7Tk1SirV.htm)|Effect: Thorn Triad - Armor (Greater)|Effet : Triade d'épines supérieure - Armure|libre|
|[jlVYoiPVRRVGBj5G.htm](equipment-effects/jlVYoiPVRRVGBj5G.htm)|Effect: Ablative Armor Plating (Lesser)|Effet : Blindage ablatif d'armure inférieur|libre|
|[jlYPMOHplgkvzLa9.htm](equipment-effects/jlYPMOHplgkvzLa9.htm)|Effect: Standard of the Primeval Howl|Effet : Étendard du hurlement primitif|libre|
|[jqbEIA6249vqgqwq.htm](equipment-effects/jqbEIA6249vqgqwq.htm)|Effect: Searing Blade (Greater)|Effet : Lame brûlante supérieure|libre|
|[jrBkGqJyjPTtuuZV.htm](equipment-effects/jrBkGqJyjPTtuuZV.htm)|Effect: Life-Boosting Oil (Moderate)|Effet : Huile stimulatrice de vie modérée|libre|
|[JvwzM4rJWwtB9HAP.htm](equipment-effects/JvwzM4rJWwtB9HAP.htm)|Effect: Wolfjaw Armor|Effet : Armure Gueule de loup|libre|
|[K21XQMoDVSPqzRla.htm](equipment-effects/K21XQMoDVSPqzRla.htm)|Effect: Sage's Lash (Turquoise)|Effet : Ceinture du sage (turquoise)|libre|
|[KAEWiyE8TQwofNj9.htm](equipment-effects/KAEWiyE8TQwofNj9.htm)|Effect: Impossible Cake (Greater)|Effet : Gâteau de l'impossible supérieur|libre|
|[KC8mrGFXs5hIhjXt.htm](equipment-effects/KC8mrGFXs5hIhjXt.htm)|Effect: Sharkskin Robe|Effet : Robe en galuchat|libre|
|[kdcVvwVVsttZgX1Y.htm](equipment-effects/kdcVvwVVsttZgX1Y.htm)|Effect: Aeon Stone Resonance (Amplifying)|Effet : Résonance de pierre d'éternité amplificatrice|libre|
|[KFnOWk5e7nwXT8IE.htm](equipment-effects/KFnOWk5e7nwXT8IE.htm)|Effect: Feyfoul (Greater)|Effet : Cafouillefée supérieur|libre|
|[kgEOxqF1q4Sy6r97.htm](equipment-effects/kgEOxqF1q4Sy6r97.htm)|Effect: Flaming Star - Armor (Major)|Effet : Étoile enflammée majeure - armure|libre|
|[kgotU0sFmtAHYySB.htm](equipment-effects/kgotU0sFmtAHYySB.htm)|Effect: Eagle Eye Elixir (Greater)|Effet : Élixir d'oeil du faucon supérieur|libre|
|[KiurLemTV8GV7OyM.htm](equipment-effects/KiurLemTV8GV7OyM.htm)|Effect: Rowan Rifle (Sonic)|Effet : Fusil de Rowan (sonique)|libre|
|[KJXNLvJAl0mNnGvn.htm](equipment-effects/KJXNLvJAl0mNnGvn.htm)|Effect: Jyoti's Feather - Weapon|Effet : Plume de jyoti - Arme|libre|
|[kkDbalYEavzRpYlp.htm](equipment-effects/kkDbalYEavzRpYlp.htm)|Effect: Antiplague (Lesser)|Effet : Antimaladie inférieur|libre|
|[kMPPl4AqFb6GclOL.htm](equipment-effects/kMPPl4AqFb6GclOL.htm)|Effect: Malleable Mixture (Greater)|Effet : Mixture de mollesse supérieure|libre|
|[KSvkfMqMQ8mlGLiz.htm](equipment-effects/KSvkfMqMQ8mlGLiz.htm)|Effect: Obsidian Goggles|Effet : Lunettes d'obsidienne|libre|
|[KtQXsv7mCvMMGXdK.htm](equipment-effects/KtQXsv7mCvMMGXdK.htm)|Effect: Life-Boosting Oil (Major)|Effet : Huile stimulatrice de vie majeure|libre|
|[kwD0wuW5Ndkc9YXB.htm](equipment-effects/kwD0wuW5Ndkc9YXB.htm)|Effect: Bestial Mutagen (Greater)|Effet : Mutagène bestial supérieur|libre|
|[kwOtHtmlH69ctK0O.htm](equipment-effects/kwOtHtmlH69ctK0O.htm)|Effect: Sun Orchid Poultice|Effet : Cataplasme d'orchidée solaire|libre|
|[kyLLXUQ9zSEvC4py.htm](equipment-effects/kyLLXUQ9zSEvC4py.htm)|Effect: Stalk Goggles (Greater)|Effet : Lunettes à tiges supérieures|libre|
|[KYzuzY8TNZmwqgBx.htm](equipment-effects/KYzuzY8TNZmwqgBx.htm)|Effect: Potion of Cold Resistance (Moderate)|Effet : Potion de résistance au froid modérée|libre|
|[LbaYzs0dQuFj8FXJ.htm](equipment-effects/LbaYzs0dQuFj8FXJ.htm)|Effect: Pickled Demon Tongue - Weapon|Effet : Langue de démon marinée - Arme|libre|
|[lcB7s9zxqoB42eG1.htm](equipment-effects/lcB7s9zxqoB42eG1.htm)|Effect: Pucker Pickle|Effet : Cornichon pucker|libre|
|[LH0IDLLF4RsT3KvM.htm](equipment-effects/LH0IDLLF4RsT3KvM.htm)|Effect: Energized Cartridge (Fire)|Effet : Cartouche énergisée (feu)|libre|
|[lh2YM9yNJixOC8RI.htm](equipment-effects/lh2YM9yNJixOC8RI.htm)|Effect: Clawed Bracers|Effet : Bracelets griffus|libre|
|[LhNZNHvmSs54c3Lj.htm](equipment-effects/LhNZNHvmSs54c3Lj.htm)|Effect: Potion of Acid Resistance (Moderate)|Effet : Potion de résistance à l'acide modérée|libre|
|[LIhJbq72TiDu5kGs.htm](equipment-effects/LIhJbq72TiDu5kGs.htm)|Effect: Journeybread (Power)|Effet : Pain de voyage puissant|libre|
|[LjaEu7gAGO77uVs2.htm](equipment-effects/LjaEu7gAGO77uVs2.htm)|Effect: Hexing Jar|Effet : Jarre à maléfices|libre|
|[lLP56tbG689TNKW5.htm](equipment-effects/lLP56tbG689TNKW5.htm)|Effect: Bracelet of Dashing|Effet : Bracelets de fougue|libre|
|[lNWACCNe9RYgaFxb.htm](equipment-effects/lNWACCNe9RYgaFxb.htm)|Effect: Cheetah's Elixir (Lesser)|Effet : Élixir du guépard inférieur|libre|
|[lO95TwgihBdTilAB.htm](equipment-effects/lO95TwgihBdTilAB.htm)|Effect: Thurible of Revelation|Effet : Encensoir de révélation inférieur|libre|
|[lPRuIRbu0rHBkoKY.htm](equipment-effects/lPRuIRbu0rHBkoKY.htm)|Effect: Elixir of Life (Minor)|Effet : Élixir de vie mineur|libre|
|[lU8IO9FIGK1DXVMy.htm](equipment-effects/lU8IO9FIGK1DXVMy.htm)|Effect: Conduct Energy|Effet : Conduire l'énergie|libre|
|[LVy8SfUF8Jrd6X18.htm](equipment-effects/LVy8SfUF8Jrd6X18.htm)|Effect: Leopard's Armor|Effet : Armure du léopard|libre|
|[LZ8q9ctapWVrGxhE.htm](equipment-effects/LZ8q9ctapWVrGxhE.htm)|Effect: Spiny Lodestone - Weapon (Major)|Effet : Pierre aimantée épineuse majeure - arme|libre|
|[M1HKNPpqkjFI9A4q.htm](equipment-effects/M1HKNPpqkjFI9A4q.htm)|Effect: Beastmaster's Sigil - Armor|Effet : Symbole du maître des bêtes - Armure|libre|
|[M3EFomnN5xArdQmV.htm](equipment-effects/M3EFomnN5xArdQmV.htm)|Effect: Moderate Healer's Gel|Effet : Gel du guérisseur modéré|libre|
|[M5veiDPQNQBevg7m.htm](equipment-effects/M5veiDPQNQBevg7m.htm)|Effect: Numbing Tonic (Lesser)|Effet : Tonique anesthésiant inférieur|libre|
|[m6z7OrJgF4XQFNpa.htm](equipment-effects/m6z7OrJgF4XQFNpa.htm)|Effect: Spiny Lodestone - Weapon|Effet : Pierre aimantée épineuse - arme|libre|
|[MEreOgnjoRiXPEuq.htm](equipment-effects/MEreOgnjoRiXPEuq.htm)|Effect: Tanglefoot Bag (Moderate)|Effet : Bombe collante modérée|libre|
|[Mf9EBLhYmZerf0nS.htm](equipment-effects/Mf9EBLhYmZerf0nS.htm)|Effect: Potion of Flying (Standard)|Effet : Potion de vol de base|libre|
|[mG6S6zm6hxaF7Tla.htm](equipment-effects/mG6S6zm6hxaF7Tla.htm)|Effect: Skeptic's Elixir (Moderate)|Effet : Élixir de l'incrédule modéré|libre|
|[mHIdEC7RX6isILiM.htm](equipment-effects/mHIdEC7RX6isILiM.htm)|Effect: Jolt Coil - Weapon|Effet : Bobine d'électrochoc - Arme|libre|
|[mi4Md1fB2XThCand.htm](equipment-effects/mi4Md1fB2XThCand.htm)|Effect: Antidote (Moderate)|Effet : Antidote modéré|libre|
|[MI5OCkF9IXmD2lPF.htm](equipment-effects/MI5OCkF9IXmD2lPF.htm)|Effect: Bloodhound Mask (Greater)|Effet : Masque du limier supérieur|libre|
|[mkjcgwDBeaOUolVe.htm](equipment-effects/mkjcgwDBeaOUolVe.htm)|Effect: Major Fanged Rune Animal Form|Effet : Forme d'animal de rune de crocs majeur|libre|
|[mn39aML7EWKbttKT.htm](equipment-effects/mn39aML7EWKbttKT.htm)|Effect: Ablative Armor Plating (Moderate)|Effet : Blindage ablatif d'armure modéré|libre|
|[ModBoFdCi7YQU4gP.htm](equipment-effects/ModBoFdCi7YQU4gP.htm)|Effect: Potion of Swimming (Greater)|Effet : Potion de nage supérieure|libre|
|[mQJk8R0vHzvpTz0e.htm](equipment-effects/mQJk8R0vHzvpTz0e.htm)|Effect: Chatterer of Follies|Effet : Bavardeur de folies|libre|
|[mrwg2XftLtSLj197.htm](equipment-effects/mrwg2XftLtSLj197.htm)|Effect: Exsanguinating Ammunition (Major)|Effet : Munition exsangue majeure|libre|
|[MvvAy8oi9ITEqsG0.htm](equipment-effects/MvvAy8oi9ITEqsG0.htm)|Effect: Silver Crescent (Greater)|Effet : Croissant d'argent supérieur|libre|
|[N2XR9v6Vo13SJZXV.htm](equipment-effects/N2XR9v6Vo13SJZXV.htm)|Effect: Blight Breath|Effet : Souffle fléau|libre|
|[N54jx6GEz2NpGobK.htm](equipment-effects/N54jx6GEz2NpGobK.htm)|Effect: Breastplate of the Mountain|Effet : Cuirasse de la montagne|libre|
|[na2gf5mSkilFoHXk.htm](equipment-effects/na2gf5mSkilFoHXk.htm)|Effect: Energy Mutagen (Moderate)|Effet : Mutagène d'énergie modéré|libre|
|[NAWKV7G4kBpPHWqL.htm](equipment-effects/NAWKV7G4kBpPHWqL.htm)|Effect: Covenant Tea|Effet : Thé d'engagement|libre|
|[Nbgf8zvHimdQqIu6.htm](equipment-effects/Nbgf8zvHimdQqIu6.htm)|Effect: Skyrider Sword|Effet : Chevaucheur d'épée|libre|
|[NddLhLIQYgZYrPTR.htm](equipment-effects/NddLhLIQYgZYrPTR.htm)|Effect: Pickled Demon Tongue - Weapon (Greater)|Effet : Langue de démon marinée supérieure - Arme|libre|
|[NdfhpKCjSS80LiUz.htm](equipment-effects/NdfhpKCjSS80LiUz.htm)|Effect: Nosoi Charm (Greater) (Diplomacy)|Effet : Charme nosoi supérieur (Diplomatie)|libre|
|[NE7Fm5YnUhD4ySX3.htm](equipment-effects/NE7Fm5YnUhD4ySX3.htm)|Effect: Earplugs (PFS Guide)|Effet : bouchons d'oreilles (Guide de la PFS)|libre|
|[neZPoQF4hW3A31dd.htm](equipment-effects/neZPoQF4hW3A31dd.htm)|Effect: Lastwall Soup (Greater)|Effet : Soupe de Dernier-Rempart supérieure|libre|
|[niFS8JHJFwnLQ2Gg.htm](equipment-effects/niFS8JHJFwnLQ2Gg.htm)|Effect: Irondust Stew|Effet : Ragoût poudrefer|libre|
|[nJRoiSyd67eQ1dYj.htm](equipment-effects/nJRoiSyd67eQ1dYj.htm)|Effect: Frost Vial (Greater)|Effet : Fiole de givre supérieure|libre|
|[NNxguHneoM3NvW1S.htm](equipment-effects/NNxguHneoM3NvW1S.htm)|Aura: Shining Symbol|Aura : Symbole lumineux|libre|
|[no7vnIiNBwWjh3w8.htm](equipment-effects/no7vnIiNBwWjh3w8.htm)|Effect: Grasp of Droskar|Effet : Poigne de Droskar|libre|
|[nQ6vM1CRLyvQdGLG.htm](equipment-effects/nQ6vM1CRLyvQdGLG.htm)|Effect: Five-Feather Wreath - Armor|Effet : Couronne à cinq plumes - Armure|libre|
|[NwEVRZmLbM9QKoIH.htm](equipment-effects/NwEVRZmLbM9QKoIH.htm)|Effect: Desolation Locket - Weapon|Effet : Médaillon de la désolation - Arme|libre|
|[NYOi1F9cW3axHrdc.htm](equipment-effects/NYOi1F9cW3axHrdc.htm)|Effect: Deadweight Snare (Success)|Effet : Piège du poids-mort (Succès)|libre|
|[o0baLY5VdjLhdsS7.htm](equipment-effects/o0baLY5VdjLhdsS7.htm)|Effect: Shrinking Potion|Effet : Potion de rétrécissement|libre|
|[o1Fzi94EODK3fa4A.htm](equipment-effects/o1Fzi94EODK3fa4A.htm)|Effect: Hongrui's Gratitude (Lantern)|Effet : Gratitude d'Hongui - Lanterne|libre|
|[o6GSF420m6cdTkZo.htm](equipment-effects/o6GSF420m6cdTkZo.htm)|Effect: Alloy Orb (Exquisite High-Grade)|Effet : Orbe d'alliage raffiné de haute qualité|libre|
|[o9N5EUN5ajuXSTVX.htm](equipment-effects/o9N5EUN5ajuXSTVX.htm)|Effect: Spiny Lodestone - Armor (Greater)|Effet : Pierre aimantée épineuse supérieure (armure)|libre|
|[oAewXfq9c0ecaSfw.htm](equipment-effects/oAewXfq9c0ecaSfw.htm)|Effect: Silvertongue Mutagen (Greater)|Effet : Mutagène de langue dorée supérieur|libre|
|[oaR6YGiZKg8a2971.htm](equipment-effects/oaR6YGiZKg8a2971.htm)|Effect: Clockwork Goggles|Effet : Lunettes mécaniques|libre|
|[OFJVaPxdafc4ezWB.htm](equipment-effects/OFJVaPxdafc4ezWB.htm)|Effect: Cold Iron Blanch (Greater)|Effet : Blanchis en fer froid supérieur|libre|
|[ogtcaPeypLDiUXMS.htm](equipment-effects/ogtcaPeypLDiUXMS.htm)|Effect: Strum of Thunder|Effet : Guitare résonante - Souffle du tonnerre|libre|
|[ohMdE8BmQHuLs40b.htm](equipment-effects/ohMdE8BmQHuLs40b.htm)|Effect: Impossible Cake (Major)|Effet : Gâteau de l'impossible majeur|libre|
|[oKtMGJa0qbxuZD7w.htm](equipment-effects/oKtMGJa0qbxuZD7w.htm)|Effect: Guardian Staff|Effet : Bâton du gardien|libre|
|[okxiX9E2IrwLXCrK.htm](equipment-effects/okxiX9E2IrwLXCrK.htm)|Effect: Potion of Sonic Resistance (Moderate)|Effet : Potion de résistance au son modérée|libre|
|[OMW71UJzYCUr4ubh.htm](equipment-effects/OMW71UJzYCUr4ubh.htm)|Effect: Ablative Armor Plating (Major)|Effet : Blindage ablatif d'armure majeur|libre|
|[oqwrw6XztVlS9tEG.htm](equipment-effects/oqwrw6XztVlS9tEG.htm)|Effect: Trinity Geode - Armor|Effet : Géode de la trinité - Armure|libre|
|[OrZfOsrh2qYMPBNo.htm](equipment-effects/OrZfOsrh2qYMPBNo.htm)|Effect: Potion of Electricity Resistance (Greater)|Effet : Potion de résistance à l'électricité supérieure|libre|
|[OVEn6314ZAul4bbi.htm](equipment-effects/OVEn6314ZAul4bbi.htm)|Effect: Spiny Lodestone - Armor (Major)|Effet : Pierre aimantée épineusee majeure - armure|libre|
|[owA1eQU6LTP3A3of.htm](equipment-effects/owA1eQU6LTP3A3of.htm)|Effect: Wyrm Claw - Armor (Greater)|Effet : Griffe de ver supérieure - Armure|libre|
|[OwHOtzI31nrfYKQ9.htm](equipment-effects/OwHOtzI31nrfYKQ9.htm)|Effect: Potion of Fire Resistance (Lesser)|Effet : Potion de résistance au feu inférieure|libre|
|[OxCVZSvWVJsOGAZN.htm](equipment-effects/OxCVZSvWVJsOGAZN.htm)|Effect: Flaming Star - Weapon|Effet : Étoile enflammée - Arme|libre|
|[p0piyFHae9uBfNRa.htm](equipment-effects/p0piyFHae9uBfNRa.htm)|Effect: Vashu's Ninth Life (Diehard)|Effet: Neuvième vie de Vashu (Dur à cuire)|libre|
|[p2aGtovaY1feytws.htm](equipment-effects/p2aGtovaY1feytws.htm)|Effect: Aeon Stone (Black Pearl)|Effet : Pierre d'éternité (Perle noire)|libre|
|[P99n3BdPkaemfhWf.htm](equipment-effects/P99n3BdPkaemfhWf.htm)|Effect: Dragon Turtle Scale (Greater)|Effet : Écaille de tortue supérieure|libre|
|[p9jROgkqozXB52UJ.htm](equipment-effects/p9jROgkqozXB52UJ.htm)|Effect: Trinity Geode - Weapon (Greater)|Effet : Géode de la trinité supérieure - arme|libre|
|[pAMyEbJzWBoYoGhs.htm](equipment-effects/pAMyEbJzWBoYoGhs.htm)|Effect: Diplomat's Badge|Effet : Insigne du diplomate|libre|
|[parbYdReJWd08fGl.htm](equipment-effects/parbYdReJWd08fGl.htm)|Effect: Spiritsight Tea|Effet : Thé de détection spectrale|libre|
|[PBvLrztlLIfr2dlV.htm](equipment-effects/PBvLrztlLIfr2dlV.htm)|Effect: Phantasmal Doorknob - Armor|Effet : Poignée de porte imaginaire - Armure|libre|
|[pDErD7SHcOxYAbqf.htm](equipment-effects/pDErD7SHcOxYAbqf.htm)|Effect: Voice of Oceans|Effect : Voix des océans|libre|
|[PeiuJ951kkBPTCSM.htm](equipment-effects/PeiuJ951kkBPTCSM.htm)|Effect: Bracers of Missile Deflection|Effet : Protège-bras de déviation de projectiles|libre|
|[PEPOd38VfVzQMKG5.htm](equipment-effects/PEPOd38VfVzQMKG5.htm)|Effect: Stone Body Mutagen (Lesser)|Effet : Mutagène corps-de-pierre inférieur|libre|
|[PeuUz7JaabCgl6Yh.htm](equipment-effects/PeuUz7JaabCgl6Yh.htm)|Effect: Cheetah's Elixir (Greater)|Effet : Élixir du guépard supérieur|libre|
|[PFtYVamw7de2cozU.htm](equipment-effects/PFtYVamw7de2cozU.htm)|Effect: Blast Suit|Effet : Armure anti-explosion|libre|
|[pgWhwSGZd8JT5IlF.htm](equipment-effects/pgWhwSGZd8JT5IlF.htm)|Effect: Ghostcaller's Planchette - Weapon (Greater)|Effet : Planchette de l'invocateur de fantôme supérieure - Arme|libre|
|[Pkk8m79MoT1RgtfW.htm](equipment-effects/Pkk8m79MoT1RgtfW.htm)|Effect: Succubus Kiss (Stage 1)|Effet : Baiser de la succube (Stade 1)|libre|
|[Plga97M3LVFZqhiP.htm](equipment-effects/Plga97M3LVFZqhiP.htm)|Effect: Resonating Fork - Armor|Effet : Diapason résonant - Armure|libre|
|[pmWJxDjz3gqL29OM.htm](equipment-effects/pmWJxDjz3gqL29OM.htm)|Effect: Bottled Omen|Effet : Présage en bouteille|libre|
|[pnBdSjOtQb9T1ajL.htm](equipment-effects/pnBdSjOtQb9T1ajL.htm)|Effect: Jolt Coil - Armor (Major)|Effet : Bobine d'électrochoc majeure - Armure|libre|
|[PpLxndUSgzgs6dd0.htm](equipment-effects/PpLxndUSgzgs6dd0.htm)|Effect: Elixir of Life (Major)|Effet : Élixir de vie majeur|libre|
|[Prn00DwJJHbfIp74.htm](equipment-effects/Prn00DwJJHbfIp74.htm)|Effect: Ginger Chew|Effet : Gingembre à mâcher|libre|
|[PT1g0Ar47FVo2O4D.htm](equipment-effects/PT1g0Ar47FVo2O4D.htm)|Effect: Batsbreath Cane|Effet : Canne souffle de chauve-souris|libre|
|[PuWZyFzJCkbq1Inj.htm](equipment-effects/PuWZyFzJCkbq1Inj.htm)|Effect: Flaming Star - Armor|Effet : Étoile enflammée - Armure|libre|
|[pZjJv8r28uASg6zp.htm](equipment-effects/pZjJv8r28uASg6zp.htm)|Effect: Choker-Arm Mutagen (Major)|Effet : Mutagène d'assouplissement majeur|libre|
|[pzR7ohtc1KKCSWIS.htm](equipment-effects/pzR7ohtc1KKCSWIS.htm)|Effect: Thorn Triad - Armor (Major)|Effet : Triade d'épines majeure - armure|libre|
|[q1EhQ716bPSgJVnC.htm](equipment-effects/q1EhQ716bPSgJVnC.htm)|Effect: Bravo's Brew (Greater)|Effet : Breuvage de bravoure supérieur|libre|
|[q58ahUEjUzTXffRN.htm](equipment-effects/q58ahUEjUzTXffRN.htm)|Effect: Perfect Droplet - Armor (Greater)|Effet : Goutte parfaite supérieure - Armure|libre|
|[QaiSmpjSUhORG800.htm](equipment-effects/QaiSmpjSUhORG800.htm)|Effect: Fury Cocktail (Moderate)|Effet : Cocktail de la fureur modéré|libre|
|[QapoFh0tbUgMwSIB.htm](equipment-effects/QapoFh0tbUgMwSIB.htm)|Effect: Thurible of Revelation (Greater)|Effet : Encensoir de révélation supérieur|libre|
|[QCqYiSIR9DVPAHgR.htm](equipment-effects/QCqYiSIR9DVPAHgR.htm)|Effect: Assassin Vine Wine|Effet : Vin de liane meurtrière|libre|
|[qFT6TJU5EObpoixe.htm](equipment-effects/qFT6TJU5EObpoixe.htm)|Effect: Ghostcaller's Planchette - Armor (Greater)|Effet : Planchette de l'invocateur de fantôme supérieure - Armure|libre|
|[qit1mLbJUyRTYcPU.htm](equipment-effects/qit1mLbJUyRTYcPU.htm)|Effect: Cognitive Mutagen (Greater)|Effet : Mutagène cognitif supérieur|libre|
|[qLl1jwybXY6EbOoI.htm](equipment-effects/qLl1jwybXY6EbOoI.htm)|Effect: Bewitching Bloom (Magnolia)|Effet : Fleur envoûtante - Magnolia|libre|
|[qoV03Fk6HSzZUCmv.htm](equipment-effects/qoV03Fk6HSzZUCmv.htm)|Effect: Sanguine Fang - Weapon (Major)|Effet : Canine sanguine majeure - Arme|libre|
|[QTmnUgiJnPli1dS0.htm](equipment-effects/QTmnUgiJnPli1dS0.htm)|Effect: Potion of Acid Resistance (Lesser)|Effet : Potion de résistance à l'acide inférieure|libre|
|[QuZ5frBMJF3gi7RY.htm](equipment-effects/QuZ5frBMJF3gi7RY.htm)|Effect: Antidote (Greater)|Effet : Antidote supérieur|libre|
|[Qvjw5RYhglGcVRhF.htm](equipment-effects/Qvjw5RYhglGcVRhF.htm)|Effect: Rime Crystal - Weapon (Major)|Effet : Cristal gelé majeur - Arme|libre|
|[qVKrrKpTghgMIuGH.htm](equipment-effects/qVKrrKpTghgMIuGH.htm)|Effect: Antiplague (Major)|Effet : Antimaladie majeur|libre|
|[qWeEmDft3tRWlIwy.htm](equipment-effects/qWeEmDft3tRWlIwy.htm)|Effect: Silver Crescent (Moderate)|Effet : Croissant argenté modéré|libre|
|[qwoLV4awdezlEJ60.htm](equipment-effects/qwoLV4awdezlEJ60.htm)|Effect: Drakeheart Mutagen (Greater)|Effet : Mutagène de coeur de drake supérieur|libre|
|[qYa5BDssyIfPQg2r.htm](equipment-effects/qYa5BDssyIfPQg2r.htm)|Effect: Silver Crescent (Lesser)|Effet : Croissant argenté inférieur|libre|
|[qzRcSQ0HTTp58hV2.htm](equipment-effects/qzRcSQ0HTTp58hV2.htm)|Effect: Sixfingers Elixir (Moderate)|Effet : Élixir Six-doigts modéré|libre|
|[R106i7WCXvHLGMTu.htm](equipment-effects/R106i7WCXvHLGMTu.htm)|Effect: Antiplague (Greater)|Effet : Antimaladie supérieur|libre|
|[R1kZsBMZdGZ3ATkA.htm](equipment-effects/R1kZsBMZdGZ3ATkA.htm)|Effect: Flaming Star - Armor (Greater)|Effet : Étoile enflammée supérieure - armure|libre|
|[r4nq5JzY8U1lXsAY.htm](equipment-effects/r4nq5JzY8U1lXsAY.htm)|Effect: Frost Breath|Effet : Souffle frigorifiant|libre|
|[R5ugeFK3MPwkbv0s.htm](equipment-effects/R5ugeFK3MPwkbv0s.htm)|Effect: Potency Crystal|Effet : Cristal de puissance|libre|
|[R5ywXEYZFV1WBe8t.htm](equipment-effects/R5ywXEYZFV1WBe8t.htm)|Effect: Energizing Rune|Effet : Rune énergisante|libre|
|[r6hDgfVLod0AmU7J.htm](equipment-effects/r6hDgfVLod0AmU7J.htm)|Effect: Heartripper Blade|Effet : Lame arrachecoeur|libre|
|[rdHzCYZEWpy2rTfI.htm](equipment-effects/rdHzCYZEWpy2rTfI.htm)|Effect: Beastmaster's Sigil - Melee Weapon|Effet : Symbole du maître des bêtes - Arme de corps-à-corps|libre|
|[REgzMqcWgJfMULmJ.htm](equipment-effects/REgzMqcWgJfMULmJ.htm)|Effect: Potion of Sonic Resistance (Greater)|Effet : Potion de résistance au son supérieure|libre|
|[ri5qxyVViva60ilN.htm](equipment-effects/ri5qxyVViva60ilN.htm)|Effect: Bewitching Bloom (Lotus)|Effet : Fleur envoûtante - Lotus|libre|
|[RIozNOntRJok5ZJt.htm](equipment-effects/RIozNOntRJok5ZJt.htm)|Effect: Energy Mutagen (Major)|Effet : Mutagène d'énergie majeur|libre|
|[RLsdvhmTh64Mmty9.htm](equipment-effects/RLsdvhmTh64Mmty9.htm)|Effect: Frost Vial (Lesser)|Effet : Fiole de givre inférieure|libre|
|[Rn2Nl2iXlRMijTNb.htm](equipment-effects/Rn2Nl2iXlRMijTNb.htm)|Effect: Juubun's One Thousand Poems|Effet: Les milles poèmes de Juubun|libre|
|[rnVjsjav50CHwMVr.htm](equipment-effects/rnVjsjav50CHwMVr.htm)|Effect: Blood Booster (Lesser)|Effet : Activateur sanguin inférieur|libre|
|[rQV8Azb3FeUJJ3fG.htm](equipment-effects/rQV8Azb3FeUJJ3fG.htm)|Effect: Delve Scale|Effet : Écaille de plongée|libre|
|[RRusoN3HEGnDO1Dg.htm](equipment-effects/RRusoN3HEGnDO1Dg.htm)|Effect: Sea Touch Elixir (Greater)|Effet : Élixir de caresse marine supérieur|libre|
|[RT1BxXrbbGgk40Ti.htm](equipment-effects/RT1BxXrbbGgk40Ti.htm)|Effect: Cognitive Mutagen (Major)|Effet : Mutagène cognitif majeur|libre|
|[rVFDLzYrJVYLiQBL.htm](equipment-effects/rVFDLzYrJVYLiQBL.htm)|Effect: Qat (Stage 1)|Effet : Qat (Stade 1)|libre|
|[rXM6njevpwqSMNRt.htm](equipment-effects/rXM6njevpwqSMNRt.htm)|Effect: Tallowheart Mass|Effet : Agglomérat de suif|libre|
|[RxtpVyOywdrt29Q6.htm](equipment-effects/RxtpVyOywdrt29Q6.htm)|Effect: Desolation Locket - Armor (Major)|Effet : Médaillon de la désolation majeur - Armure|libre|
|[S3Sv7SYwxozbG554.htm](equipment-effects/S3Sv7SYwxozbG554.htm)|Effect: War Blood Mutagen (Major)|Effet : Mutagène de sang guerrier majeur|libre|
|[S4MZzALqFoXJsr6o.htm](equipment-effects/S4MZzALqFoXJsr6o.htm)|Effect: Bloodhound Mask (Lesser)|Effet : Masque du limier inférieur|libre|
|[sbxGrVdEsYZ5KiCj.htm](equipment-effects/sbxGrVdEsYZ5KiCj.htm)|Effect: Resolute Mind Wrap|Effet : Turban de l'esprit résolu|libre|
|[SbYcOry1cxbndSve.htm](equipment-effects/SbYcOry1cxbndSve.htm)|Effect: Silkspinner's Shield (Climb)|Effet : Bouclier de la fileuse de soie (Escalade)|libre|
|[sEa4TGXGBxku1V7o.htm](equipment-effects/sEa4TGXGBxku1V7o.htm)|Effect: Potion of Acid Resistance (Greater)|Effet : Potion de résistance à l'acide supérieure|libre|
|[Sf6UO6vgCeicggOK.htm](equipment-effects/Sf6UO6vgCeicggOK.htm)|Effect: Shining Ammunition|Effet : Munition luisante|libre|
|[sk5LgfXestDKxCnK.htm](equipment-effects/sk5LgfXestDKxCnK.htm)|Effect: Ghost Oil|Effet : Huile fantôme|libre|
|[sOwAqyQ6MaoSqaY1.htm](equipment-effects/sOwAqyQ6MaoSqaY1.htm)|Effect: Lesser Healer's Gel|Effet : Gel du guérisseur inférieur|libre|
|[SpUT3mCa2NVKruJ9.htm](equipment-effects/SpUT3mCa2NVKruJ9.htm)|Effect: Life-Boosting Oil (Greater)|Effet : Huile stimulatrice de vie supérieure|libre|
|[SqN1FGSgdNlyvRu9.htm](equipment-effects/SqN1FGSgdNlyvRu9.htm)|Effect: Containment Contraption|Effet : Appareil de confinement|libre|
|[T27uDXdMVc5ZFwKw.htm](equipment-effects/T27uDXdMVc5ZFwKw.htm)|Effect: Enhanced Hearing Aids|Effet : Appareil auditif augmenté|libre|
|[T38SHe842S43a8bB.htm](equipment-effects/T38SHe842S43a8bB.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Major)|Effet : Symbole du maître des bêtes majeur - Arme à distance|libre|
|[T7C0HaGu1Cn6Pt5j.htm](equipment-effects/T7C0HaGu1Cn6Pt5j.htm)|Effect: Deadweight Mutagen (Lesser)|Effet : Mutagène de poids-mort inférieur|libre|
|[TD9divhkTjGIegop.htm](equipment-effects/TD9divhkTjGIegop.htm)|Effect: Life-Boosting Oil (Lesser)|Effet : Huile stimulatrice de vie inférieure|libre|
|[tGeMT4iHJcsjVbl3.htm](equipment-effects/tGeMT4iHJcsjVbl3.htm)|Effect: Metuak's Pendant|Effet : Pendentif de Metuak|libre|
|[Thd0XXhunYNk6jD7.htm](equipment-effects/Thd0XXhunYNk6jD7.htm)|Effect: Cinnamon Seers|Effet : Voyants à la cannelle|libre|
|[thOpQunbQr77XWdF.htm](equipment-effects/thOpQunbQr77XWdF.htm)|Effect: Sea Touch Elixir (Lesser)|Effet : Élixir de caresse marine inférieur|libre|
|[Tioloj3bTlFnQDqa.htm](equipment-effects/Tioloj3bTlFnQDqa.htm)|Effect: Perfect Droplet - Armor (Major)|Effet : Goutte parfaite majeure (Armure)|libre|
|[TkRuKKYyPHTGPfgf.htm](equipment-effects/TkRuKKYyPHTGPfgf.htm)|Effect: Sixfingers Elixir (Greater)|Effet : Élixir six-doigts supérieur|libre|
|[tLGzSCcfxflLSqzw.htm](equipment-effects/tLGzSCcfxflLSqzw.htm)|Effect: Energized Cartridge (Cold)|Effet : Cartouche énergisée (froid)|libre|
|[TM0LFTy30FG2wwI2.htm](equipment-effects/TM0LFTy30FG2wwI2.htm)|Effect: Star of Cynosure|Effet : Étoile de Cynosure|libre|
|[TOqm2yCf1IE1DKtF.htm](equipment-effects/TOqm2yCf1IE1DKtF.htm)|Effect: Blood Booster (Moderate)|Effet : Activateur sanguin modéré|libre|
|[TsWUTODTVi487SEz.htm](equipment-effects/TsWUTODTVi487SEz.htm)|Effect: Skeptic's Elixir (Greater)|Effet : Élixir de l'incrédule supérieur|libre|
|[tsxBwSRFqMoorMJ3.htm](equipment-effects/tsxBwSRFqMoorMJ3.htm)|Effect: Matsuki's Medicinal Wine|Effet : Vin Médicinal de Matstuki|libre|
|[tTBJ33UGtzXjWOJp.htm](equipment-effects/tTBJ33UGtzXjWOJp.htm)|Effect: Applereed Mutagen (Greater)|Effet : Mutagène pousse-roseau supérieur|libre|
|[TU67AK08CUsP7pl4.htm](equipment-effects/TU67AK08CUsP7pl4.htm)|Effect: Beastmaster's Sigil - Ranged Weapon (Greater)|Effet : Symbole du maître des bêtes supérieur - Arme à distance|libre|
|[U3wQTWZ3FiLJPA0O.htm](equipment-effects/U3wQTWZ3FiLJPA0O.htm)|Effect: Potion of Fire Resistance (Greater)|Effet : Potion de résistance au feu supérieure|libre|
|[u7200u7lh40am0jb.htm](equipment-effects/u7200u7lh40am0jb.htm)|Effect: Trinity Geode - Weapon (Major)|Effet : Géode de la trinité majeure - arme|libre|
|[Uadsb25G18pKdZ2e.htm](equipment-effects/Uadsb25G18pKdZ2e.htm)|Effect: Clandestine Cloak|Effet : Cape de clandestinité|libre|
|[uC6KjfiWrTBXYtP8.htm](equipment-effects/uC6KjfiWrTBXYtP8.htm)|Effect: Storm Chair|Effet : Chaise de tempête|libre|
|[uf3KI1n1MkP7htbJ.htm](equipment-effects/uf3KI1n1MkP7htbJ.htm)|Effect: Potion of Electricity Resistance (Moderate)|Effet : Potion de résistance à l'électricité modérée|libre|
|[uHZ23fBG9HIdK5ht.htm](equipment-effects/uHZ23fBG9HIdK5ht.htm)|Effect: Tremorsensors|Effet : Senseurs de vibrations|libre|
|[UihgHdEj0GsaRaAL.htm](equipment-effects/UihgHdEj0GsaRaAL.htm)|Effect: Phantasmal Doorknob - Armor (Greater)|Effet : Poignée de porte imaginaire supérieure - Armure|libre|
|[uijpoXaiKXcCYrSD.htm](equipment-effects/uijpoXaiKXcCYrSD.htm)|Effect: Auric Noodles|Effet : Nouilles auriques|libre|
|[UjAJUFMfH0OWWwQS.htm](equipment-effects/UjAJUFMfH0OWWwQS.htm)|Effect: Ooze Ammunition (Major)|Effet : Munition de vase majeure|libre|
|[uJyFG2gxW29bXNfu.htm](equipment-effects/uJyFG2gxW29bXNfu.htm)|Effect: Jann's Prism (Jann's Light)|Effet : Prisme du jann (Lumière du jann)|libre|
|[uK2vXk4WnleihqYI.htm](equipment-effects/uK2vXk4WnleihqYI.htm)|Effect: Cold Iron Blanch (Moderate)|Effet : Blanchis en fer froid modéré|libre|
|[UlalLihKzDxcOdXL.htm](equipment-effects/UlalLihKzDxcOdXL.htm)|Effect: Thurible of Revelation (Moderate)|Effet : Encensoir de révélation modéré|libre|
|[uOr8APVmEJ0E3lgz.htm](equipment-effects/uOr8APVmEJ0E3lgz.htm)|Effect: Bloodseeker Beak (Greater)|Effet : Trompe de cherchesang supérieure|libre|
|[UPMZe0oKVpUgDaOE.htm](equipment-effects/UPMZe0oKVpUgDaOE.htm)|Effect: Pickled Demon Tongue - Armor (Greater)|Effet : Langue de démon marinée supérieure - Armure|libre|
|[UTtX0xLGYci6P43I.htm](equipment-effects/UTtX0xLGYci6P43I.htm)|Effect: Mudrock Snare (Failure)|Effet : Piège artisanal de boue solidifiée (Échec)|libre|
|[uXEp1rPU5fY4OiBf.htm](equipment-effects/uXEp1rPU5fY4OiBf.htm)|Effect: Clandestine Cloak (Greater)|Effet : Cape de clandestinité supérieure|libre|
|[uYWi2vxFQVBJgJR6.htm](equipment-effects/uYWi2vxFQVBJgJR6.htm)|Effect: Sparkler|Effet : Bougie magique|libre|
|[UzSrsR9S2pgMDbbp.htm](equipment-effects/UzSrsR9S2pgMDbbp.htm)|Effect: Cape of the Open Sky|Effet : Cape du ciel ouvert|libre|
|[v5Ht1V4MZvRKRBjL.htm](equipment-effects/v5Ht1V4MZvRKRBjL.htm)|Effect: Silvertongue Mutagen (Moderate)|Effet : Mutagène de langue dorée modéré|libre|
|[VCypzSu659eC6jNi.htm](equipment-effects/VCypzSu659eC6jNi.htm)|Effect: Eagle Eye Elixir (Lesser)|Effet : Élixir d'oeil du faucon inférieur|libre|
|[vFOr2JAJxiVvvn2E.htm](equipment-effects/vFOr2JAJxiVvvn2E.htm)|Effect: Drakeheart Mutagen (Major)|Effet : Mutagène de coeur de drake majeur|libre|
|[vgsV7eEYQ7W1fHqR.htm](equipment-effects/vgsV7eEYQ7W1fHqR.htm)|Effect: Tactician's Helm (Move It!)|Effet : Heaume du tacticien (bouge le !)|libre|
|[vH4bEu3EnAhNpKEQ.htm](equipment-effects/vH4bEu3EnAhNpKEQ.htm)|Effect: Dragonscale Amulet|Effet : Amulette en écailles de dragons|libre|
|[viCX9fZzTWGuoO85.htm](equipment-effects/viCX9fZzTWGuoO85.htm)|Effect: Cloak of Elvenkind (Greater)|Effet : Cape elfique supérieure|libre|
|[VIzeuA9tQEQ7V1Ib.htm](equipment-effects/VIzeuA9tQEQ7V1Ib.htm)|Effect: Wand of Fey Flames|Effet : Baguette de feux féeriques|libre|
|[vj2hkcSbwwRYNLk5.htm](equipment-effects/vj2hkcSbwwRYNLk5.htm)|Effect: Jack's Tattered Cape|Effet : Cape en loques de Jack|libre|
|[Vj2vytSr9jEtesYu.htm](equipment-effects/Vj2vytSr9jEtesYu.htm)|Effect: Soothing Tonic (Greater)|Effet : Tonique apaisant supérieur|libre|
|[VKdiRnhrsgQTFSCM.htm](equipment-effects/VKdiRnhrsgQTFSCM.htm)|Effect: Whispering Staff (Ally)|Effet : Bâton de chuchotement - Allié|libre|
|[vKooOkXHvtqCgZYg.htm](equipment-effects/vKooOkXHvtqCgZYg.htm)|Effect: Energized Cartridge (Acid)|Effet : Cartouche énergisée (acide)|libre|
|[VlfuBfWkygsG8u5h.htm](equipment-effects/VlfuBfWkygsG8u5h.htm)|Effect: Blaze|Effet : Brûlure|libre|
|[vlTiBE0h2kyw5J9o.htm](equipment-effects/vlTiBE0h2kyw5J9o.htm)|Effect: Emerald Fulcrum Lens|Effet : Lentille à pivot émeraude|libre|
|[VMVrJA4SkyOfklmj.htm](equipment-effects/VMVrJA4SkyOfklmj.htm)|Effect: Potency Crystal (Major)|Effet : Cristal de puissance majeure|libre|
|[vOgD9wfStLX1utte.htm](equipment-effects/vOgD9wfStLX1utte.htm)|Effect: Skyrider Sword (Greater)|Effet : Chevaucheur d'épée supérieur|libre|
|[vOU4Yv2MyAfYBbmF.htm](equipment-effects/vOU4Yv2MyAfYBbmF.htm)|Effect: Aeon Stone (Orange Prism) (Occultism)|Effet : Pierre d'éternité amplificatrice (Occultisme)|libre|
|[VPDBv6t8nBt3Vfp7.htm](equipment-effects/VPDBv6t8nBt3Vfp7.htm)|Effect: Soothing Tonic (Moderate)|Effet : Tonique apaisant modéré|libre|
|[VPtsrpbP0AE642al.htm](equipment-effects/VPtsrpbP0AE642al.htm)|Effect: Quicksilver Mutagen (Moderate)|Effet : Mutagène de vif-argent modéré|libre|
|[VsHhBBLApZsOCJRL.htm](equipment-effects/VsHhBBLApZsOCJRL.htm)|Effect: Fire and Iceberg (Greater)|Effet : Feu et iceberg supérieur|libre|
|[VVWvXiNudYYGV9sJ.htm](equipment-effects/VVWvXiNudYYGV9sJ.htm)|Effect: Nosoi Charm (Lifesense)|Effet : Charme nosoi (Perception de la vie)|libre|
|[VVXjPCummVHQp7hG.htm](equipment-effects/VVXjPCummVHQp7hG.htm)|Effect: Bloodhound Olfactory Stimulators|Effet : Stimulateurs olfactifs de la meute sanglante|libre|
|[vw6BbXYsEgCR3dPt.htm](equipment-effects/vw6BbXYsEgCR3dPt.htm)|Effect: Rime Crystal - Armor (Greater)|Effet : Cristal gelé supérieur - Armure|libre|
|[VZCcjwsQX1wnYlTn.htm](equipment-effects/VZCcjwsQX1wnYlTn.htm)|Effect: Perfect Droplet - Armor|Effet : Goutte parfaite - Armure|libre|
|[vZPyQAt5T2L0Dfmq.htm](equipment-effects/vZPyQAt5T2L0Dfmq.htm)|Effect: Topology Protoplasm|Effet : Morphologie protoplasmique|libre|
|[w0LUnfS2whVhDBUF.htm](equipment-effects/w0LUnfS2whVhDBUF.htm)|Effect: Glittering Snare (Success)|Effet : Piège scintillant (Succès)|libre|
|[W3BCLbX6j1IqL0uB.htm](equipment-effects/W3BCLbX6j1IqL0uB.htm)|Effect: Slippers of Spider Climbing|Effet : Chaussons de l'araignée|libre|
|[W3xQBLj5hLOtb6Tj.htm](equipment-effects/W3xQBLj5hLOtb6Tj.htm)|Effect: Potion of Swimming|Effet : Potion de nage|libre|
|[Wa4317cqU4lJ8vAQ.htm](equipment-effects/Wa4317cqU4lJ8vAQ.htm)|Effect: Eagle Eye Elixir (Moderate)|Effet : Élixir d'oeil du faucon modéré|libre|
|[Wa5s0Perw2DGHGCA.htm](equipment-effects/Wa5s0Perw2DGHGCA.htm)|Effect: Resurrection Tea|Effet : Thé de résurrection|libre|
|[WBqZA9z7ZbhcR2ye.htm](equipment-effects/WBqZA9z7ZbhcR2ye.htm)|Effect: Horn of Exorcism (Sacred Seeds)|Effet : Corne d'exorcisme (Graines sacrées)|libre|
|[wcjEjFKLcPisk4jK.htm](equipment-effects/wcjEjFKLcPisk4jK.htm)|Effect: Jyoti's Feather - Weapon (Major)|Effet : Plume de jyoti supérieure - majeure|libre|
|[wdN3sa1Qs2rEdiDC.htm](equipment-effects/wdN3sa1Qs2rEdiDC.htm)|Effect: Cape of Justice (Distracted)|Effet : Cape de justice - Distrait|libre|
|[wFF0SZs1Hcf87Kk1.htm](equipment-effects/wFF0SZs1Hcf87Kk1.htm)|Effect: Bloodhound Mask (Moderate)|Effet : Masque du limier modéré|libre|
|[WGmrfhdQzlNzyMrq.htm](equipment-effects/WGmrfhdQzlNzyMrq.htm)|Effect: Jyoti's Feather - Weapon (Greater)|Effet : Plume de jyoti supérieure - Arme|libre|
|[WHAp9cDOqnJ1VCcg.htm](equipment-effects/WHAp9cDOqnJ1VCcg.htm)|Effect: Orchestral Brooch|Effet : Broche orchestrale|libre|
|[WJ9L6rgUTZVV7vEE.htm](equipment-effects/WJ9L6rgUTZVV7vEE.htm)|Effect: Desolation Locket - Armor|Effet : Médaillon de la désolation - Armure|libre|
|[WLvFC2eE80SEZpUg.htm](equipment-effects/WLvFC2eE80SEZpUg.htm)|Effect: Wyrm Claw - Armor|Effet : Griffe de ver - Armure|libre|
|[WMKjWH4gyUrky4Hy.htm](equipment-effects/WMKjWH4gyUrky4Hy.htm)|Effect: Demon Dust (Stage 1)|Effet : Poussière de démon (Stade 1)|libre|
|[wNCxSxruzLVGtLE4.htm](equipment-effects/wNCxSxruzLVGtLE4.htm)|Effect: Spiderfoot Brew (Lesser)|Effet : Infusion de pattes d'araignée inférieure|libre|
|[WOblH0tb7WQ8qgM2.htm](equipment-effects/WOblH0tb7WQ8qgM2.htm)|Effect: Thorn Triad - Weapon (Greater)|Effet : Triade d'épines supérieure - Arme|libre|
|[WQ0DxUzMgAvo0zy9.htm](equipment-effects/WQ0DxUzMgAvo0zy9.htm)|Effect: Apricot of Bestial Might|Effet : Abricot de puissance bestiale|libre|
|[WRV0XjiEHdlBpduS.htm](equipment-effects/WRV0XjiEHdlBpduS.htm)|Effect: Trinity Geode - Weapon|Effet : Géode de la trinité - Arme|libre|
|[WRvZ2Nq3wquisD4Y.htm](equipment-effects/WRvZ2Nq3wquisD4Y.htm)|Effect: Pickled Demon Tongue - Armor|Effet : Langue de démon marinée - Armure|libre|
|[wTZnKkT0K4Tdy8mD.htm](equipment-effects/wTZnKkT0K4Tdy8mD.htm)|Effect: Bravo's Brew (Moderate)|Effet : Breuvage de bravoure modéré|libre|
|[wvzrq1Lh9Y8PpxBs.htm](equipment-effects/wvzrq1Lh9Y8PpxBs.htm)|Effect: Thawing Candle|Effet : Bougie de dégel|libre|
|[WXrqEuLT4uP48Bvo.htm](equipment-effects/WXrqEuLT4uP48Bvo.htm)|Effect: Obsidian Goggles (Major)|Effet : Lunettes d'obsidienne majeures|libre|
|[WXsWkFosSGBrptwF.htm](equipment-effects/WXsWkFosSGBrptwF.htm)|Effect: Ivory Baton|Effet : Bâton d'ivoire|libre|
|[wyLEew86nhNUXASu.htm](equipment-effects/wyLEew86nhNUXASu.htm)|Effect: Eagle Eye Elixir (Major)|Effet : Élixir d'oeil du faucon majeur|libre|
|[Wylo8ttAkExaX6Gs.htm](equipment-effects/Wylo8ttAkExaX6Gs.htm)|Effect: Rime Crystal - Armor (Major)|Effet : Cristal gelé majeur - Armure|libre|
|[X2ZZgTqanpoCMDmd.htm](equipment-effects/X2ZZgTqanpoCMDmd.htm)|Effect: Taljjae's Mask (The General)|Effet : Masque de Taljjae (le général)|libre|
|[xFQRiVU6h8EA6Lw9.htm](equipment-effects/xFQRiVU6h8EA6Lw9.htm)|Effect: Bestial Mutagen (Moderate)|Effet : Mutagène bestial modéré|libre|
|[XGnaMNt1iBkOwENY.htm](equipment-effects/XGnaMNt1iBkOwENY.htm)|Effect: Galvanic Chew|Effet : Gomme galvanique|libre|
|[xiG3kmPJBpX2KA7l.htm](equipment-effects/xiG3kmPJBpX2KA7l.htm)|Effect: Oil of Corpse Restoration|Effet : Huile de restauration de cadavre|libre|
|[XKsqQrabRlg9klGp.htm](equipment-effects/XKsqQrabRlg9klGp.htm)|Effect: Draft of Stellar Radiance|Effet : Ébauche d'éclat stellaire|libre|
|[XlHbUOTbK6PfBfCv.htm](equipment-effects/XlHbUOTbK6PfBfCv.htm)|Effect: Demon Dust (Stage 2)|Effet : Poussière de démon (Stade 2)|libre|
|[xLilBqqf34ZJYO9i.htm](equipment-effects/xLilBqqf34ZJYO9i.htm)|Effect: Juggernaut Mutagen (Greater)|Effet : Mutagène de juggernaut supérieur|libre|
|[XNtUmlnvHujFw4dq.htm](equipment-effects/XNtUmlnvHujFw4dq.htm)|Effect: Warding Statuette - Armor (Greater)|Effet : Statuette de protection supérieure - Armure|libre|
|[XrlChFETfe8avLsX.htm](equipment-effects/XrlChFETfe8avLsX.htm)|Effect: Sixfingers Elixir (Lesser)|Effet : Élixir six-doigts inférieur|libre|
|[xSw7cTboMvP8sJAq.htm](equipment-effects/xSw7cTboMvP8sJAq.htm)|Effect: Beastmaster's Sigil - Melee Weapon (Major)|Effet : Symbole du maître des bêtes majeur - Arme au corps-à-corps|libre|
|[xV8fmSn12652usGC.htm](equipment-effects/xV8fmSn12652usGC.htm)|Effect: Deadweight Mutagen (Moderate)|Effet : Mutagène de poids-mort modéré|libre|
|[xVAdPzFaSvJXPMKv.htm](equipment-effects/xVAdPzFaSvJXPMKv.htm)|Effect: Applereed Mutagen (Lesser)|Effet : Mutagène pousse-roseau inférieur|libre|
|[XwCBalKJf3CiEiFa.htm](equipment-effects/XwCBalKJf3CiEiFa.htm)|Effect: Treat Poison (Critical Success)|Effet : Soigner l'empoisonnement (succès critique)|libre|
|[XWenziR7J3mwKV4W.htm](equipment-effects/XWenziR7J3mwKV4W.htm)|Effect: Treat Poison (Success)|Effet : Soigner l'empoisonnement (succès)|libre|
|[XzxADtNpUlRff972.htm](equipment-effects/XzxADtNpUlRff972.htm)|Effect: Greater Fanged Rune Animal Form|Effet : Forme d'animal à runes de crocs supérieure|libre|
|[ycKxoeZwg7g9Ivh5.htm](equipment-effects/ycKxoeZwg7g9Ivh5.htm)|Effect: Merciful Balm|Effet : Baume miséricordieux|libre|
|[yd0qnTSjz6d9rgxU.htm](equipment-effects/yd0qnTSjz6d9rgxU.htm)|Effect: Spiny Lodestone - Weapon (Greater)|Effet : Pierre aimantée épineuse supérieure - arme|libre|
|[YflZZ7EG7JJkdX0d.htm](equipment-effects/YflZZ7EG7JJkdX0d.htm)|Effect: Jolt Coil - Armor (Greater)|Effet : Bobine d'électrochoc supérieure - Armure|libre|
|[YI7QQqXO6nosaAKr.htm](equipment-effects/YI7QQqXO6nosaAKr.htm)|Effect: Spiderfoot Brew (Moderate)|Effet : Infusion de pattes d'araignée modérée|libre|
|[yP45Rqu4jvCfXBkp.htm](equipment-effects/yP45Rqu4jvCfXBkp.htm)|Effect: Fire and Iceberg (Major)|Effet : Feu et iceberg majeur|libre|
|[YsV7tB15XrSCKNnB.htm](equipment-effects/YsV7tB15XrSCKNnB.htm)|Effect: Rime Crystal - Weapon (Greater)|Effet : Cristal gelé supérieur - Arme|libre|
|[yt8meGTS7wLa6Fg2.htm](equipment-effects/yt8meGTS7wLa6Fg2.htm)|Effect: Ablative Armor Plating (True)|Effet : Blindage ablatif d'armure ultime|libre|
|[ytqvHyF0oMKbo65P.htm](equipment-effects/ytqvHyF0oMKbo65P.htm)|Effect: Crimson Fulcrum Lens|Effet : Lentille cramoisie|libre|
|[yvabfuAO74pvH8hh.htm](equipment-effects/yvabfuAO74pvH8hh.htm)|Effect: Aeon Stone (Orange Prism) (Arcana)|Effet : Pierre d'éternité amplificatrice (Arcanes)|libre|
|[Yxssrnh9UZJAM0V7.htm](equipment-effects/Yxssrnh9UZJAM0V7.htm)|Effect: Elixir of Life (Moderate)|Effet : Élixir de vie modéré|libre|
|[yykiQBIGqwxIDRZq.htm](equipment-effects/yykiQBIGqwxIDRZq.htm)|Effect: Viper Rapier|Effet : Rapière vipère|libre|
|[yzENPvcYIxegPflt.htm](equipment-effects/yzENPvcYIxegPflt.htm)|Effect: Beastmaster's Sigil - Armor (Major)|Symbole du maître des bêtes majeur - Armure|libre|
|[yZW8I6G2Di6gdd16.htm](equipment-effects/yZW8I6G2Di6gdd16.htm)|Effect: Codex of Destruction and Renewal|Effet : Codex de la destruction et du renouveau|libre|
|[z8FvdsKEY4lB2L8b.htm](equipment-effects/z8FvdsKEY4lB2L8b.htm)|Effect: Phoenix Flask|Effet : Flasque de phénix|libre|
|[Z9oPh462q82IYIZ6.htm](equipment-effects/Z9oPh462q82IYIZ6.htm)|Effect: Elixir of Life (Greater)|Effet : Élixir de vie supérieur|libre|
|[Zb8RYgmzCI6fQE0o.htm](equipment-effects/Zb8RYgmzCI6fQE0o.htm)|Effect: Throne Card|Effet : Carte trône|libre|
|[zd85Ny1RS46OL0TD.htm](equipment-effects/zd85Ny1RS46OL0TD.htm)|Effect: Shrinking Potion (Greater)|Effet : Potion de rétrécissement supérieur|libre|
|[Zdh2uO1vVYJmaqld.htm](equipment-effects/Zdh2uO1vVYJmaqld.htm)|Effect: Potion of Flying (Greater)|Effet : Potion de vol supérieure|libre|
|[zDuuHVeHgd175pGf.htm](equipment-effects/zDuuHVeHgd175pGf.htm)|Effect: Succubus Kiss (Stage 2)|Effet : Baiser de la succube (Stade 2)|libre|
|[zNHvhwHsC8ckhKVp.htm](equipment-effects/zNHvhwHsC8ckhKVp.htm)|Effect: Crushing|Effet : Écrasante|libre|
|[zoMGcJxA4kH20Zzw.htm](equipment-effects/zoMGcJxA4kH20Zzw.htm)|Effect: Moon Blossom Tea|Effet : Thé de floraison lunaire|libre|
|[ZP9Uq4PVTgzJ3wEi.htm](equipment-effects/ZP9Uq4PVTgzJ3wEi.htm)|Effect: Five-Feather Wreath - Armor (Greater)|Effet : Couronne à cinq plumes supérieure|libre|
|[Zqs1VvRm5Oq7DwX0.htm](equipment-effects/Zqs1VvRm5Oq7DwX0.htm)|Effect: Brass Dwarf Weakness|Effet : Faiblesse du nain d'airain|libre|
|[ztxW3lBPRcesF7wK.htm](equipment-effects/ztxW3lBPRcesF7wK.htm)|Effect: Cognitive Mutagen (Moderate)|Effet : Mutagène cognitif modéré|libre|
|[ZV9rtVOD1eDTwcY4.htm](equipment-effects/ZV9rtVOD1eDTwcY4.htm)|Effect: Grit (Stage 3)|Effet : Grit (stade 3)|libre|
|[zY7cemRcFD2zAVbC.htm](equipment-effects/zY7cemRcFD2zAVbC.htm)|Effect: Oath of the Devoted|Effet : Serment du dévoué|libre|
|[zZsdex5orF5Odpus.htm](equipment-effects/zZsdex5orF5Odpus.htm)|Effect: Mask of Allure|Effet : Masque d'allure|libre|
