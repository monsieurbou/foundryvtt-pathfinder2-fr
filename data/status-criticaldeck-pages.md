# État de la traduction (criticaldeck-pages)

 * **libre**: 106


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0UfzLjrNTkWSDZKb.htm](criticaldeck-pages/0UfzLjrNTkWSDZKb.htm)|Critical Fumble Deck #50|Carte d'échec critique #50|libre|
|[14krStxw1Eirik24.htm](criticaldeck-pages/14krStxw1Eirik24.htm)|Critical Hit Deck #21|Carte de coup critique #21|libre|
|[1twrhbUvK9iTGLkw.htm](criticaldeck-pages/1twrhbUvK9iTGLkw.htm)|Critical Fumble Deck #15|Carte d'échec critique #15|libre|
|[1Xh3F6JINcq67Avv.htm](criticaldeck-pages/1Xh3F6JINcq67Avv.htm)|Critical Fumble Deck #16|Carte d'échec critique #16|libre|
|[26fg43UMXWHaV5Ee.htm](criticaldeck-pages/26fg43UMXWHaV5Ee.htm)|Critical Fumble Deck #37|Carte d'échec critique #37|libre|
|[4YX173FiGFHEW0YY.htm](criticaldeck-pages/4YX173FiGFHEW0YY.htm)|Critical Hit Deck #53|Carte de coup critique #53|libre|
|[5UIGH23EhwTfoNSM.htm](criticaldeck-pages/5UIGH23EhwTfoNSM.htm)|Critical Fumble Deck #40|Carte d'échec critique #40|libre|
|[61SADGhFPc7Ra59P.htm](criticaldeck-pages/61SADGhFPc7Ra59P.htm)|Critical Hit Deck #33|Carte de coup critique #33|libre|
|[7shwLAHvCpn9nNyP.htm](criticaldeck-pages/7shwLAHvCpn9nNyP.htm)|Critical Hit Deck #18|Carte de coup critique #18|libre|
|[8catEKrHLlFFcnKK.htm](criticaldeck-pages/8catEKrHLlFFcnKK.htm)|Critical Hit Deck #15|Carte de coup critique #15|libre|
|[8Qpc1cbM8YliNeWt.htm](criticaldeck-pages/8Qpc1cbM8YliNeWt.htm)|Critical Hit Deck #24|Carte de coup critique #24|libre|
|[97Rw9jJrOJa7NM0s.htm](criticaldeck-pages/97Rw9jJrOJa7NM0s.htm)|Critical Fumble Deck #1|Carte d'échec critique #1|libre|
|[aAQ9VezKVsEVvSZP.htm](criticaldeck-pages/aAQ9VezKVsEVvSZP.htm)|Critical Hit Deck #44|Carte de coup critique #44|libre|
|[AHnMn8nDzBU8XiDZ.htm](criticaldeck-pages/AHnMn8nDzBU8XiDZ.htm)|Critical Fumble Deck #31|Carte d'échec critique #31|libre|
|[aJziqJXJbAHFMnJi.htm](criticaldeck-pages/aJziqJXJbAHFMnJi.htm)|Critical Hit Deck #9|Carte de coup critique #9|libre|
|[bLrBNA83wIHBP2Y0.htm](criticaldeck-pages/bLrBNA83wIHBP2Y0.htm)|Critical Hit Deck #30|Carte de coup critique #30|libre|
|[bZJiyaGJWhgfmrqF.htm](criticaldeck-pages/bZJiyaGJWhgfmrqF.htm)|Critical Fumble Deck #27|Carte d'échec critique #27|libre|
|[C0t1CYEnkINf6jhr.htm](criticaldeck-pages/C0t1CYEnkINf6jhr.htm)|Critical Fumble Deck #41|Carte d'échec critique #41|libre|
|[cAf08LJb1eMwAUR9.htm](criticaldeck-pages/cAf08LJb1eMwAUR9.htm)|Critical Hit Deck #27|Carte de coup critique #27|libre|
|[ChyeVeGrXiP83SMI.htm](criticaldeck-pages/ChyeVeGrXiP83SMI.htm)|Critical Fumble Deck #22|Carte d'échec critique #22|libre|
|[Clcu2H0uVyozvbI1.htm](criticaldeck-pages/Clcu2H0uVyozvbI1.htm)|Critical Hit Deck #17|Carte de coup critique #17|libre|
|[CRAeRs5IsNZ6UvY1.htm](criticaldeck-pages/CRAeRs5IsNZ6UvY1.htm)|Critical Fumble Deck #20|Carte d'échec critique #20|libre|
|[CvQ1EVSIxYPukyF5.htm](criticaldeck-pages/CvQ1EVSIxYPukyF5.htm)|Critical Fumble Deck #43|Carte d'échec critique #43|libre|
|[d0OhvD2MJiKwDp6d.htm](criticaldeck-pages/d0OhvD2MJiKwDp6d.htm)|Critical Fumble Deck #30|Carte d'échec critique #30|libre|
|[d0sh4NZCQ5cKu4ae.htm](criticaldeck-pages/d0sh4NZCQ5cKu4ae.htm)|Critical Hit Deck #7|Carte de coup critique #7|libre|
|[DbGY1HRle0RP8yQZ.htm](criticaldeck-pages/DbGY1HRle0RP8yQZ.htm)|Critical Fumble Deck #11|Carte d'échec critique #11|libre|
|[dg5t9bt48deLA8IH.htm](criticaldeck-pages/dg5t9bt48deLA8IH.htm)|Critical Fumble Deck #44|Carte d'échec critique #44|libre|
|[DlZqvNFlAdXu740m.htm](criticaldeck-pages/DlZqvNFlAdXu740m.htm)|Critical Hit Deck #31|Carte de coup critique #31|libre|
|[dXEWEkV4N8QgAodD.htm](criticaldeck-pages/dXEWEkV4N8QgAodD.htm)|Critical Fumble Deck #4|Carte d'échec critique #4|libre|
|[E06pVpFdVXuRKgLT.htm](criticaldeck-pages/E06pVpFdVXuRKgLT.htm)|Critical Hit Deck #52|Carte de coup critique #52|libre|
|[e9OAyablRyqsYAaP.htm](criticaldeck-pages/e9OAyablRyqsYAaP.htm)|Critical Fumble Deck #36|Carte d'échec critique #36|libre|
|[EAYQXMZmvqXhDVKX.htm](criticaldeck-pages/EAYQXMZmvqXhDVKX.htm)|Critical Hit Deck #48|Carte de coup critique #48|libre|
|[EkWqEdr5GVmYQOKx.htm](criticaldeck-pages/EkWqEdr5GVmYQOKx.htm)|Critical Hit Deck #45|Carte de coup critique #45|libre|
|[ftmugY4kI4X2OHtJ.htm](criticaldeck-pages/ftmugY4kI4X2OHtJ.htm)|Critical Hit Deck #14|Carte de coup critique #14|libre|
|[GHS4SvuvWoDaWa0x.htm](criticaldeck-pages/GHS4SvuvWoDaWa0x.htm)|Critical Fumble Deck #12|Carte d'échec critique #12|libre|
|[GM8KhEJHweIyOXpQ.htm](criticaldeck-pages/GM8KhEJHweIyOXpQ.htm)|Critical Fumble Deck #47|Carte d'échec critique #47|libre|
|[hbbqXuMIjP1O5ivM.htm](criticaldeck-pages/hbbqXuMIjP1O5ivM.htm)|Critical Fumble Deck #48|Carte d'échec critique #48|libre|
|[hfnqwRiA1F8yFxO2.htm](criticaldeck-pages/hfnqwRiA1F8yFxO2.htm)|Critical Hit Deck #38|Carte de coup critique #38|libre|
|[HJNQmrXcBXqxpU8d.htm](criticaldeck-pages/HJNQmrXcBXqxpU8d.htm)|Critical Fumble Deck #51|Carte d'échec critique #51|libre|
|[hRnoJuHZgvnm0y4Y.htm](criticaldeck-pages/hRnoJuHZgvnm0y4Y.htm)|Critical Fumble Deck #14|Carte d'échec critique #14|libre|
|[IKTn2I00FjQ8CFT5.htm](criticaldeck-pages/IKTn2I00FjQ8CFT5.htm)|Critical Fumble Deck #9|Carte d'échec critique #9|libre|
|[jCcFyy09k0lf4wEl.htm](criticaldeck-pages/jCcFyy09k0lf4wEl.htm)|Critical Fumble Deck #3|Carte d'échec critique #3|libre|
|[jE2DwXGkwsJABkA1.htm](criticaldeck-pages/jE2DwXGkwsJABkA1.htm)|Critical Fumble Deck #45|Carte d'échec critique #45|libre|
|[jON5ZXJSqO78xFNT.htm](criticaldeck-pages/jON5ZXJSqO78xFNT.htm)|Critical Hit Deck #10|Carte de coup critique #10|libre|
|[jU2mfBSZkV0Ek5gE.htm](criticaldeck-pages/jU2mfBSZkV0Ek5gE.htm)|Critical Hit Deck #20|Carte de coup critique #20|libre|
|[jvnfW76s6YjUo71n.htm](criticaldeck-pages/jvnfW76s6YjUo71n.htm)|Critical Fumble Deck #2|Carte d'échec critique #2|libre|
|[jy3B3GW2BfOzG0AZ.htm](criticaldeck-pages/jy3B3GW2BfOzG0AZ.htm)|Critical Hit Deck #22|Carte de coup critique #22|libre|
|[k7BzBti00xCRMnZq.htm](criticaldeck-pages/k7BzBti00xCRMnZq.htm)|Critical Fumble Deck #35|Carte d'échec critique #35|libre|
|[KAlZA2xLfbcGEm4E.htm](criticaldeck-pages/KAlZA2xLfbcGEm4E.htm)|Critical Hit Deck #40|Carte de coup critique #40|libre|
|[KAnlflMVazFiPclF.htm](criticaldeck-pages/KAnlflMVazFiPclF.htm)|Critical Fumble Deck #26|Carte d'échec critique #26|libre|
|[KAsdzGsp84DphbcZ.htm](criticaldeck-pages/KAsdzGsp84DphbcZ.htm)|Critical Hit Deck #23|Carte de coup critique #23|libre|
|[klRXj66DUAy1d1V5.htm](criticaldeck-pages/klRXj66DUAy1d1V5.htm)|Critical Fumble Deck #39|Carte d'échec critique #39|libre|
|[kQHLnBkfwg7mgZAD.htm](criticaldeck-pages/kQHLnBkfwg7mgZAD.htm)|Critical Hit Deck #13|Carte de coup critique #13|libre|
|[Lqeke3pTNSHcyAlS.htm](criticaldeck-pages/Lqeke3pTNSHcyAlS.htm)|Critical Hit Deck #16|Carte de coup critique #16|libre|
|[LXVN3PrglHFeUUQI.htm](criticaldeck-pages/LXVN3PrglHFeUUQI.htm)|Critical Fumble Deck #5|Carte d'échec critique #5|libre|
|[MAjmNAKgJd4UqEfQ.htm](criticaldeck-pages/MAjmNAKgJd4UqEfQ.htm)|Critical Fumble Deck #34|Carte d'échec critique #34|libre|
|[MuU2oRlPMEGf5SKT.htm](criticaldeck-pages/MuU2oRlPMEGf5SKT.htm)|Critical Hit Deck #43|Carte de coup critique #43|libre|
|[nAQt4JAeqobILZOF.htm](criticaldeck-pages/nAQt4JAeqobILZOF.htm)|Critical Fumble Deck #53|Carte d'échec critique #53|libre|
|[O54GgNHACD44rViW.htm](criticaldeck-pages/O54GgNHACD44rViW.htm)|Critical Fumble Deck #13|Carte d'échec critique #13|libre|
|[OhyEcqPV8lo4hCwQ.htm](criticaldeck-pages/OhyEcqPV8lo4hCwQ.htm)|Critical Hit Deck #25|Carte de coup critique #25|libre|
|[Okqxk51eZT2JWJnw.htm](criticaldeck-pages/Okqxk51eZT2JWJnw.htm)|Critical Hit Deck #41|Carte de coup critique #41|libre|
|[OSj1vOhjm71h9Km6.htm](criticaldeck-pages/OSj1vOhjm71h9Km6.htm)|Critical Hit Deck #8|Carte de coup critique #8|libre|
|[pdKVSF828hMb0ruS.htm](criticaldeck-pages/pdKVSF828hMb0ruS.htm)|Critical Fumble Deck #8|Carte d'échec critique #8|libre|
|[pLB1M1oHTnFnzSCR.htm](criticaldeck-pages/pLB1M1oHTnFnzSCR.htm)|Critical Hit Deck #5|Carte de coup critique #5|libre|
|[q3wbDpopfALf7tJr.htm](criticaldeck-pages/q3wbDpopfALf7tJr.htm)|Critical Fumble Deck #6|Carte d'échec critique #6|libre|
|[QeRwmIUgBtzxElzk.htm](criticaldeck-pages/QeRwmIUgBtzxElzk.htm)|Critical Fumble Deck #49|Carte d'échec critique #49|libre|
|[qFcMTp2HirNwCf5g.htm](criticaldeck-pages/qFcMTp2HirNwCf5g.htm)|Critical Fumble Deck #38|Carte d'échec critique #38|libre|
|[qJaP33VzLsKefpSc.htm](criticaldeck-pages/qJaP33VzLsKefpSc.htm)|Critical Hit Deck #36|Carte de coup critique #36|libre|
|[qJgQ0vw4mcqDyYVT.htm](criticaldeck-pages/qJgQ0vw4mcqDyYVT.htm)|Critical Fumble Deck #19|Carte d'échec critique #19|libre|
|[qsF4BXJlET5zW5n6.htm](criticaldeck-pages/qsF4BXJlET5zW5n6.htm)|Critical Fumble Deck #33|Carte d'échec critique #33|libre|
|[r9iBZ8NsIFSvgStR.htm](criticaldeck-pages/r9iBZ8NsIFSvgStR.htm)|Critical Hit Deck #1|Carte de coup critique #1|libre|
|[R9KUWluXi5sFMfii.htm](criticaldeck-pages/R9KUWluXi5sFMfii.htm)|Critical Fumble Deck #52|Carte d'échec critique #52|libre|
|[RksHFLAckI3WzZjx.htm](criticaldeck-pages/RksHFLAckI3WzZjx.htm)|Critical Hit Deck #4|Carte de coup critique #4|libre|
|[RMK1WoNdESOAfpUW.htm](criticaldeck-pages/RMK1WoNdESOAfpUW.htm)|Critical Fumble Deck #21|Carte d'échec critique #21|libre|
|[RwmPxeyYxpqhNpKj.htm](criticaldeck-pages/RwmPxeyYxpqhNpKj.htm)|Critical Fumble Deck #10|Carte d'échec critique #10|libre|
|[RYUmc3RaviKhGkaI.htm](criticaldeck-pages/RYUmc3RaviKhGkaI.htm)|Critical Hit Deck #47|Carte de coup critique #47|libre|
|[rZHd27zct5D3dN6w.htm](criticaldeck-pages/rZHd27zct5D3dN6w.htm)|Critical Fumble Deck #18|Carte d'échec critique #18|libre|
|[SVt7DN4eRBljHvWX.htm](criticaldeck-pages/SVt7DN4eRBljHvWX.htm)|Critical Hit Deck #51|Carte de coup critique #51|libre|
|[TLsZved7H5lQtKWl.htm](criticaldeck-pages/TLsZved7H5lQtKWl.htm)|Critical Fumble Deck #24|carte d'échec critique #24|libre|
|[tn6geVSwpDWKhMFC.htm](criticaldeck-pages/tn6geVSwpDWKhMFC.htm)|Critical Hit Deck #26|Carte de coup critique #26|libre|
|[TQtz26fCkZNSwPf2.htm](criticaldeck-pages/TQtz26fCkZNSwPf2.htm)|Critical Fumble Deck #28|Carte d'ééchec critique #28|libre|
|[tUgtvtXyPyK1XHIt.htm](criticaldeck-pages/tUgtvtXyPyK1XHIt.htm)|Critical Hit Deck #32|Carte de coup critique #32|libre|
|[UnOv3yIG2BFWBvOo.htm](criticaldeck-pages/UnOv3yIG2BFWBvOo.htm)|Critical Hit Deck #34|Carte de coup critique #34|libre|
|[UQ3MU5ltnZlq1Ljs.htm](criticaldeck-pages/UQ3MU5ltnZlq1Ljs.htm)|Critical Hit Deck #37|Carte de coup critique #37|libre|
|[URrgkZ8uRkzt2Zhf.htm](criticaldeck-pages/URrgkZ8uRkzt2Zhf.htm)|Critical Hit Deck #11|Carte de coup critique #11|libre|
|[uxrtM7Zg7HSzLogm.htm](criticaldeck-pages/uxrtM7Zg7HSzLogm.htm)|Critical Fumble Deck #46|Carte d'échec critique #46|libre|
|[uYcfVZMr3ykQgRXN.htm](criticaldeck-pages/uYcfVZMr3ykQgRXN.htm)|Critical Hit Deck #35|Carte de coup critique #35|libre|
|[vbCWhbKx24g0Q40U.htm](criticaldeck-pages/vbCWhbKx24g0Q40U.htm)|Critical Hit Deck #46|Carte de coup critique #46|libre|
|[vFjWSlbJ9kWr7dhQ.htm](criticaldeck-pages/vFjWSlbJ9kWr7dhQ.htm)|Critical Fumble Deck #17|Carte d'échec critique #17|libre|
|[VWlXcYzq9XEBOA3u.htm](criticaldeck-pages/VWlXcYzq9XEBOA3u.htm)|Critical Hit Deck #28|Carte de coup critique #28|libre|
|[WBUwp076Zb1XZceP.htm](criticaldeck-pages/WBUwp076Zb1XZceP.htm)|Critical Fumble Deck #42|Carte d'échec critique #42|libre|
|[WheD4j70ekVgpFb7.htm](criticaldeck-pages/WheD4j70ekVgpFb7.htm)|Critical Hit Deck #50|Carte de coup critique #50|libre|
|[wVneA33rSDvAtNPZ.htm](criticaldeck-pages/wVneA33rSDvAtNPZ.htm)|Critical Fumble Deck #29|Carte d'échec critique #29|libre|
|[XTAKgmqj1ZFepfHx.htm](criticaldeck-pages/XTAKgmqj1ZFepfHx.htm)|Critical Hit Deck #49|Carte de coup critique #49|libre|
|[XW36TQZYvQ9S7T4v.htm](criticaldeck-pages/XW36TQZYvQ9S7T4v.htm)|Critical Hit Deck #3|Carte de coup critique #3|libre|
|[XX7shh3pqdG5v6Lm.htm](criticaldeck-pages/XX7shh3pqdG5v6Lm.htm)|Critical Fumble Deck #25|Carte d'échec critique #25|libre|
|[Y3etmaTpdsMT3kKp.htm](criticaldeck-pages/Y3etmaTpdsMT3kKp.htm)|Critical Hit Deck #2|Carte de coup critique #2|libre|
|[ymqdYEJyGiqAOijp.htm](criticaldeck-pages/ymqdYEJyGiqAOijp.htm)|Critical Hit Deck #19|Carte de coup critique #19|libre|
|[YrNP8Havb1BQ6akO.htm](criticaldeck-pages/YrNP8Havb1BQ6akO.htm)|Critical Hit Deck #29|Carte de coup critique #29|libre|
|[Yw5SzkTazJSsn3db.htm](criticaldeck-pages/Yw5SzkTazJSsn3db.htm)|Critical Hit Deck #12|Carte de coup critique #12|libre|
|[yy6uVnmPsQNaxks9.htm](criticaldeck-pages/yy6uVnmPsQNaxks9.htm)|Critical Hit Deck #6|Carte de coup critique|libre|
|[z1dQl3xXyLg6xakc.htm](criticaldeck-pages/z1dQl3xXyLg6xakc.htm)|Critical Fumble Deck #32|Carte d'échec critique #32|libre|
|[Z6uHNA2pcuTdNc4L.htm](criticaldeck-pages/Z6uHNA2pcuTdNc4L.htm)|Critical Fumble Deck #23|Carte d'échec critique #23|libre|
|[znDpd5Z0k6bq6WCr.htm](criticaldeck-pages/znDpd5Z0k6bq6WCr.htm)|Critical Hit Deck #39|Carte de coup critique #39|libre|
|[ZNh4PvHcmQEOa65W.htm](criticaldeck-pages/ZNh4PvHcmQEOa65W.htm)|Critical Hit Deck #42|Carte de coup critique #42|libre|
|[zW0b5OCcFi5TPYp5.htm](criticaldeck-pages/zW0b5OCcFi5TPYp5.htm)|Critical Fumble Deck #7|Carte d'échec critique #7|libre|
