# État de la traduction (ac-construct-companions)

 * **libre**: 6


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[5v2Kj6Jh2YYGaSQq.htm](ac-construct-companions/5v2Kj6Jh2YYGaSQq.htm)|Advanced Construct|Créature artificielle avancée|libre|
|[B3XfI8hQKdr7WG4R.htm](ac-construct-companions/B3XfI8hQKdr7WG4R.htm)|Paragon Construct|Parangon créature artificielle|libre|
|[f9ydomcD8DnSEXYV.htm](ac-construct-companions/f9ydomcD8DnSEXYV.htm)|Incredible Construct|Créature artificielle incroyable|libre|
|[i4Fz5yg9JufpyVns.htm](ac-construct-companions/i4Fz5yg9JufpyVns.htm)|Unarmed Strike 1|Frappe à mains nues 1|libre|
|[JG6DlMcAdHpL2Hr7.htm](ac-construct-companions/JG6DlMcAdHpL2Hr7.htm)|Prototype Construct|Prototype créature artificielle|libre|
|[mjDtydpNNfI9SuFq.htm](ac-construct-companions/mjDtydpNNfI9SuFq.htm)|Unarmed Strike 2|Frappe à mains nues 2|libre|
