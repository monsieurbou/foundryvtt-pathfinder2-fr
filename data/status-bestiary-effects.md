# État de la traduction (bestiary-effects)

 * **libre**: 278
 * **officielle**: 42
 * **aucune**: 1


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[SLVQVIE1AjOc73jU.htm](bestiary-effects/SLVQVIE1AjOc73jU.htm)|Effect: Forgiven Defaced Naiad Queen's Thanks|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0ifqiVhhKULXJyf9.htm](bestiary-effects/0ifqiVhhKULXJyf9.htm)|Effect: Rune of Flames (Longspear)|Effet : Rune de Flammes (Pique)|libre|
|[0jAT2TJoqC1z6NCf.htm](bestiary-effects/0jAT2TJoqC1z6NCf.htm)|Effect: Consume Memories|Effet : Absorption de la mémoire|libre|
|[0jo8CUzw5lWehNg3.htm](bestiary-effects/0jo8CUzw5lWehNg3.htm)|Effect: Oceanic Armor|Effet : Armure océanique|libre|
|[0vJlwvjFghLdfvn5.htm](bestiary-effects/0vJlwvjFghLdfvn5.htm)|Effect: Nature's Infusion|Effet : Imprégnation naturelle|libre|
|[1bOSJ2LbEC28aI9f.htm](bestiary-effects/1bOSJ2LbEC28aI9f.htm)|Effect: Despair|Effet : Désespoir|officielle|
|[1dwMVgBHfT4qO4OS.htm](bestiary-effects/1dwMVgBHfT4qO4OS.htm)|Effect: Resonance|Effet : Résonance|libre|
|[1jrrnMwsfO97LXi4.htm](bestiary-effects/1jrrnMwsfO97LXi4.htm)|Effect: Absorb Memories|Effet : Absorber les souvenirs|libre|
|[1toVzNVJZx0RwG1v.htm](bestiary-effects/1toVzNVJZx0RwG1v.htm)|Effect: Darivan's Bloodline Magic|Effet : Magie du lignage de Darivan|libre|
|[1wCgwFLJByW8YKyM.htm](bestiary-effects/1wCgwFLJByW8YKyM.htm)|Effect: Sacrifice Armor|Effet : Armure sacrifiée|libre|
|[2ccLQxmTlTPySnOR.htm](bestiary-effects/2ccLQxmTlTPySnOR.htm)|Effect: Technology Control|Effet : Contrôle technologique|libre|
|[2DlOfFoYLGSlfugH.htm](bestiary-effects/2DlOfFoYLGSlfugH.htm)|Effect: Manipulate Luck (Good)|Effet : Manipuler la chance (Bonne)|libre|
|[31nnjHZqiaqaWBUi.htm](bestiary-effects/31nnjHZqiaqaWBUi.htm)|Effect: Harmonizing Aura (Allies)|Effet : Aura d'harmonie (Alliés)|libre|
|[361dIAAhiZE0wg8v.htm](bestiary-effects/361dIAAhiZE0wg8v.htm)|Effect: Adaptive Strike|Effet : Frappe adaptative|libre|
|[37wbBpPBi5eBtNqM.htm](bestiary-effects/37wbBpPBi5eBtNqM.htm)|Effect: Lantern of Hope|Effet : Lanterne d'espoir|libre|
|[3MIZf42EhhKbIwLQ.htm](bestiary-effects/3MIZf42EhhKbIwLQ.htm)|Effect: Aura of Corruption|Effet : Aura de corruption|libre|
|[3np7EyJ8pyrVlpie.htm](bestiary-effects/3np7EyJ8pyrVlpie.htm)|Effect: Claim Corpse - Skeletal|Effet : Réclamer un cadavre - Squelettique|libre|
|[3PiE6Wka0ylO5V1x.htm](bestiary-effects/3PiE6Wka0ylO5V1x.htm)|Effect: Demon-Touched Weapon|Effet : Touché par le démon|libre|
|[3QUaxNvpHDmf9cTo.htm](bestiary-effects/3QUaxNvpHDmf9cTo.htm)|Effect: Rune of Flames (Rock)|Effet : Rune de Flammes (Rocher)|libre|
|[3Wtzyb0ZgkaC7vHY.htm](bestiary-effects/3Wtzyb0ZgkaC7vHY.htm)|Effect: Fanatical Frenzy|Effet : Frénésie fanatique|libre|
|[4bR1i7qzmSJ5No6O.htm](bestiary-effects/4bR1i7qzmSJ5No6O.htm)|Effect: Bond in Light|Effet : Lié à la lumière|libre|
|[4FginnDcOt4wfedf.htm](bestiary-effects/4FginnDcOt4wfedf.htm)|Effect: Bittersweet Dreams|Effet : Rêves aigres-doux|libre|
|[4FMFRlEC923CnLI7.htm](bestiary-effects/4FMFRlEC923CnLI7.htm)|Effect: Fed by Wood|Effet : Renforcé par le bois|libre|
|[4GAJHkurmZ1ttKDv.htm](bestiary-effects/4GAJHkurmZ1ttKDv.htm)|Effect: Blood Magic|Effet : Magie du sang|libre|
|[4HJJdogS751Z04lD.htm](bestiary-effects/4HJJdogS751Z04lD.htm)|Effect: Tail Lash (Skill Check)|Effet : Coup de queue (Test de compétence)|libre|
|[4M2K16mH4gndHAKa.htm](bestiary-effects/4M2K16mH4gndHAKa.htm)|Effect: Undead Mastery|Effet : maîtrise des morts vivants|libre|
|[4q8Of8NM9DC8kWyK.htm](bestiary-effects/4q8Of8NM9DC8kWyK.htm)|Effect: Pry|Effet : Forcer|libre|
|[4tGVIEqwH4TQoF0O.htm](bestiary-effects/4tGVIEqwH4TQoF0O.htm)|Effect: Bastion Aura|Effet : Aura de bastion|libre|
|[513Y2ZSzvm8rijkp.htm](bestiary-effects/513Y2ZSzvm8rijkp.htm)|Effect: Fold Form|Effet : Forme pliée|libre|
|[5M3RGpOuxizPM5Iy.htm](bestiary-effects/5M3RGpOuxizPM5Iy.htm)|Effect: Caustic Drool|Effet : bave caustique|libre|
|[5NSWRxAsJuvwyl0E.htm](bestiary-effects/5NSWRxAsJuvwyl0E.htm)|Effect: Commander's Aura|Effet : Aura de commandement|libre|
|[5roCV7EbPx1G5xOd.htm](bestiary-effects/5roCV7EbPx1G5xOd.htm)|Effect: Mangling Rend|Effet : Mutilation acharnée|libre|
|[5SLAqVPYa0kliRDt.htm](bestiary-effects/5SLAqVPYa0kliRDt.htm)|Effect: Mist Cloud|Effet : Nappe de brume|libre|
|[5syHrFGAE6lo0FUr.htm](bestiary-effects/5syHrFGAE6lo0FUr.htm)|Effect: Invigorating Passion|Effet : Passion stimulante|libre|
|[5x0XpNftvx9uGbXt.htm](bestiary-effects/5x0XpNftvx9uGbXt.htm)|Effect: Death Gasp (Etioling)|Effet : Soupir de mort (Étiolant)|libre|
|[5ZK22sNW7o26aST0.htm](bestiary-effects/5ZK22sNW7o26aST0.htm)|Effect: Dirty Bomb|Effet : Bombe sale|libre|
|[64wrP9IbfHbj1mrA.htm](bestiary-effects/64wrP9IbfHbj1mrA.htm)|Effect: War Leader|Effet : Chef de guerre|libre|
|[68oispwYGLUUxjzt.htm](bestiary-effects/68oispwYGLUUxjzt.htm)|Effect: Breach the Abyss|Effet : Brèche des Failles extérieures|libre|
|[6E8bOkwFzFuQ3ZAw.htm](bestiary-effects/6E8bOkwFzFuQ3ZAw.htm)|Effect: Lurker's Glow (Critical Failure)|Effet : Lueur du rôdeur (Échec critique)|libre|
|[6GC248hHu3LdOAtS.htm](bestiary-effects/6GC248hHu3LdOAtS.htm)|Effect: Sting of the Lash|Effet : Piqûre du fouet|libre|
|[6KebCyP5O7Gl9baR.htm](bestiary-effects/6KebCyP5O7Gl9baR.htm)|Effect: Beacon of the Rowan Guard|Effet : Phare de la Garde Rowan|libre|
|[6OpwHz0f55wPawHI.htm](bestiary-effects/6OpwHz0f55wPawHI.htm)|Effect: Powder Burst|Effet : Nuage de poudre|libre|
|[6rnB7nK6J6zF4vea.htm](bestiary-effects/6rnB7nK6J6zF4vea.htm)|Effect: Graveknight's Curse|Effet : Malédiction du Chevalier sépulcre|libre|
|[6TKI9EZKvusrPLHU.htm](bestiary-effects/6TKI9EZKvusrPLHU.htm)|Effect: Distracting Giggle|Effet : Rire distrayant|libre|
|[75B7z49jfQbWcSy9.htm](bestiary-effects/75B7z49jfQbWcSy9.htm)|Effect: Spray Toxic Oil|Effet : Aspersion d'huile toxique|libre|
|[7OJrBAEah6YFLzcK.htm](bestiary-effects/7OJrBAEah6YFLzcK.htm)|Effect: Crush Chitin|Effet : Brise-chitine|libre|
|[7PjBr9LsVB0Jjzyu.htm](bestiary-effects/7PjBr9LsVB0Jjzyu.htm)|Effect: Interpose|Effet : Interposition|libre|
|[7PYhxmQxCD5kzMlw.htm](bestiary-effects/7PYhxmQxCD5kzMlw.htm)|Effect: Harrowing Misfortune|Effet : Infortune du tourment|officielle|
|[7qoZauizAKfPIXeu.htm](bestiary-effects/7qoZauizAKfPIXeu.htm)|Effect: Volcanic Purge|Effet : Purge volcanique|libre|
|[7txhqKeWFyTJhrEE.htm](bestiary-effects/7txhqKeWFyTJhrEE.htm)|Effect: Viscous Breath|Effet : Souffle visqueux|libre|
|[7wDH2q0UcFdu2w58.htm](bestiary-effects/7wDH2q0UcFdu2w58.htm)|Effect: Focused Assault|Effet : Assaut concentré|libre|
|[7x0O2GqWBJiAk5PF.htm](bestiary-effects/7x0O2GqWBJiAk5PF.htm)|Effect: Brutal Rally|Effet : Mobilisation par la violence|officielle|
|[81XYZZ3H0GL8thdQ.htm](bestiary-effects/81XYZZ3H0GL8thdQ.htm)|Effect: Countered by Water|Effet : Contré par l'eau|libre|
|[86uYNsVXzYWxqLTV.htm](bestiary-effects/86uYNsVXzYWxqLTV.htm)|Effect: Splintered Ground|Effet : Champ d'esquilles|libre|
|[8sXm5FX6qsMDYcba.htm](bestiary-effects/8sXm5FX6qsMDYcba.htm)|Effect: Aura of Smoke|Effet : Aura de fumée|libre|
|[8tCNgEE9OsQE9Xnh.htm](bestiary-effects/8tCNgEE9OsQE9Xnh.htm)|Effect: Read the Cards|Effet : Lecture des cartes|libre|
|[8z4Q84UlS8cMYVWu.htm](bestiary-effects/8z4Q84UlS8cMYVWu.htm)|Effect: Black Cat Curse|Effet : Malédiction du chat noir|officielle|
|[9iL6CeZlmtIuAZOJ.htm](bestiary-effects/9iL6CeZlmtIuAZOJ.htm)|Effect: Surgical Rend|Effet : Éventration chirurgicale|libre|
|[9Qyu0HN5j6DO8Izc.htm](bestiary-effects/9Qyu0HN5j6DO8Izc.htm)|Effect: Sinful Bite|Effet : Morsure du péché|libre|
|[a0nJK2mKHwWYzpdG.htm](bestiary-effects/a0nJK2mKHwWYzpdG.htm)|Effect: Bully's Rage|Effet : Rage de la brute|libre|
|[a4bZrBNL17zPjDiH.htm](bestiary-effects/a4bZrBNL17zPjDiH.htm)|Effect: Countered by Fire|Effet : Contré par le feu|libre|
|[AL14FXSxpKyyfnOn.htm](bestiary-effects/AL14FXSxpKyyfnOn.htm)|Effect: Noxious Smog|Effet : Nuage nocif de pollution|libre|
|[AL7E03DYahfDhbcR.htm](bestiary-effects/AL7E03DYahfDhbcR.htm)|Effect: Guardian's Aegis|Effet : Égide du gardien|libre|
|[aMQxiTCdTxVcradv.htm](bestiary-effects/aMQxiTCdTxVcradv.htm)|Effect: Blade Barrage|Effet : Barrage de lame|libre|
|[aOpSI5tApXF5xHCM.htm](bestiary-effects/aOpSI5tApXF5xHCM.htm)|Effect: Unsettled Aura|Effet : Aura de perturbations|libre|
|[APJJCYdq4gNe0PF4.htm](bestiary-effects/APJJCYdq4gNe0PF4.htm)|Effect: Susceptible to Mockery (Critical Failure)|Effet : Susceptible à la moquerie (échec critique)|libre|
|[AThOnEYzPO9ssYTB.htm](bestiary-effects/AThOnEYzPO9ssYTB.htm)|Effect: Disturbing Vision|Effet : Vision troublante|libre|
|[auJ02rr1Jr88oD3Z.htm](bestiary-effects/auJ02rr1Jr88oD3Z.htm)|Effect: Claim Corpse - Fleshy|Effet : Réclamer un cadavre - charnu|libre|
|[BKZS6xpUndRSnxK1.htm](bestiary-effects/BKZS6xpUndRSnxK1.htm)|Effect: Bloom Regeneration|Effet : Régénération de la fleur|libre|
|[BmAw66zEkifSvOtg.htm](bestiary-effects/BmAw66zEkifSvOtg.htm)|Effect: Brand of the Impenitent|Effet : Marque de l'impénitent|officielle|
|[BsuYuFxE20mnRxbs.htm](bestiary-effects/BsuYuFxE20mnRxbs.htm)|Effect: Defensive Slam|Effet : Claque défensive|libre|
|[BTSstQAn4Xlm1PWV.htm](bestiary-effects/BTSstQAn4Xlm1PWV.htm)|Effect: Uncanny Tinker|Effet : bricolage ingénieux (Succès critique)|libre|
|[bV70ZxnUAoYnfXaZ.htm](bestiary-effects/bV70ZxnUAoYnfXaZ.htm)|Effect: Rune of Smiting (Rock)|Effet : Rune de choc (Rocher)|libre|
|[c6SiBB3mzV8lZUQr.htm](bestiary-effects/c6SiBB3mzV8lZUQr.htm)|Effect: Protean Anatomy|Effet : Anatomie protéenne|libre|
|[C9nb9XbnQgbnXpTq.htm](bestiary-effects/C9nb9XbnQgbnXpTq.htm)|Effect: Crystalline Dust Form|Effet : Forme de poussière cristalline|libre|
|[Cc5WsKkbOPOzopCY.htm](bestiary-effects/Cc5WsKkbOPOzopCY.htm)|Effect: Dire Warning|Effet : Avertissement funeste|officielle|
|[ceOkHxhJNTcvZkCy.htm](bestiary-effects/ceOkHxhJNTcvZkCy.htm)|Effect: Blood Siphon (Critical Failure)|Effet : Siphon de sang (Échec critique)|libre|
|[CiCG3r7SHYMJeUxz.htm](bestiary-effects/CiCG3r7SHYMJeUxz.htm)|Effect: Inspirational Presence|Effet : Présence inspirante|libre|
|[ckL8iL7BKBPteEVL.htm](bestiary-effects/ckL8iL7BKBPteEVL.htm)|Effect: Goblin Song|Effet : Chant gobelin|libre|
|[CM14G9kLI97aIC5h.htm](bestiary-effects/CM14G9kLI97aIC5h.htm)|Effect: Pest Haven|Effet : Havre de vermine|libre|
|[CoPPOCyfADvybcxV.htm](bestiary-effects/CoPPOCyfADvybcxV.htm)|Effect: Gleaming Armor|Effet : Armure brillante|libre|
|[CUgmb7g65vYNT2hn.htm](bestiary-effects/CUgmb7g65vYNT2hn.htm)|Effect: Curse of Boiling Blood|Effet : Malédiction du sang bouillonnant|libre|
|[dNs30jfFOJqhjFW7.htm](bestiary-effects/dNs30jfFOJqhjFW7.htm)|Effect: Entangling Train|Effet : Ficelles enchevêtrantes|libre|
|[dRe8n1nBWMIXd8jh.htm](bestiary-effects/dRe8n1nBWMIXd8jh.htm)|Effect: Moon Frenzy|Effet : Frénésie lunaire|libre|
|[dslTaCkliOH5lZ6k.htm](bestiary-effects/dslTaCkliOH5lZ6k.htm)|Effect: Zinba Restoration|Effet : Restauration zinba|libre|
|[dVVc5MYx4G3QPHoa.htm](bestiary-effects/dVVc5MYx4G3QPHoa.htm)|Effect: Guildmaster's Lead|Effet : L'exemple de la maîtresse de guilde|officielle|
|[E0fYyli4aCVmevgJ.htm](bestiary-effects/E0fYyli4aCVmevgJ.htm)|Effect: Capture Magic|Effet : Magie capturée|libre|
|[E10qJgInN8Fx3yhW.htm](bestiary-effects/E10qJgInN8Fx3yhW.htm)|Effect: Distracting Frolic|Effet : Cabrioles distrayantes|libre|
|[e4HoYakV7SvwvcrH.htm](bestiary-effects/e4HoYakV7SvwvcrH.htm)|Effect: Divine Aegis|Effet : Égide divine|libre|
|[eE3FEpjk2HdZDzSA.htm](bestiary-effects/eE3FEpjk2HdZDzSA.htm)|Effect: Stoke the Fervent|Effet : Raviver le fervent|libre|
|[ekpt4mFw1kJkZ2pK.htm](bestiary-effects/ekpt4mFw1kJkZ2pK.htm)|Effect: Tenacious Flames|Effet : Flammes tenaces|libre|
|[EM0VP31B8CyjFD15.htm](bestiary-effects/EM0VP31B8CyjFD15.htm)|Effect: Inspired Feast|Effet : Festin inspirant|libre|
|[EqdXFH2l0TyeOTQz.htm](bestiary-effects/EqdXFH2l0TyeOTQz.htm)|Effect: Bloodline Magic (Stirvyn Banyan)|Effet : Magie de lignage (Stirvyn Banyan)|libre|
|[eqzDPseLIfWJMxAR.htm](bestiary-effects/eqzDPseLIfWJMxAR.htm)|Effect: Harrow Burst|Effet : Explosion du Tourment|libre|
|[EwhWYPCNM8bIIYEI.htm](bestiary-effects/EwhWYPCNM8bIIYEI.htm)|Effect: Defensive Slice|Effet : Attaque sur la défensive|libre|
|[eZL4B1P4H8hr1CAn.htm](bestiary-effects/eZL4B1P4H8hr1CAn.htm)|Effect: Glowing Spores|Effets : Spores brillantes|libre|
|[f8rncEs06bqcn3LZ.htm](bestiary-effects/f8rncEs06bqcn3LZ.htm)|Effect: Sandstorm|Effet : Tempête de sable|libre|
|[fGI77mVeCQKBAP0f.htm](bestiary-effects/fGI77mVeCQKBAP0f.htm)|Effect: Drain Luck|Effet : Drain de chance|libre|
|[FISKjI7bal26AnyL.htm](bestiary-effects/FISKjI7bal26AnyL.htm)|Effect: Timely Advice|Effet : Conseil opportun|officielle|
|[FjcvJWPFwkyy5vEk.htm](bestiary-effects/FjcvJWPFwkyy5vEk.htm)|Effect: Artificer's Command|Effet : Ordre de l'artificier|libre|
|[fPhbDSTPuT01f0Kn.htm](bestiary-effects/fPhbDSTPuT01f0Kn.htm)|Effect: Ensnare|Effet : Prise au piège|libre|
|[FQrNDisx6noJnFaQ.htm](bestiary-effects/FQrNDisx6noJnFaQ.htm)|Effect: Fettering Fedora|Effet : Fédora entravant|libre|
|[g4CRjb2zS1pkVDbk.htm](bestiary-effects/g4CRjb2zS1pkVDbk.htm)|Effect: Sticky Spores|Effet : Spores gluants|libre|
|[g5Yen1FhEMCzCJkv.htm](bestiary-effects/g5Yen1FhEMCzCJkv.htm)|Effect: Profane Gift|Effet : Don profane|libre|
|[GC5441z8i7N8swk1.htm](bestiary-effects/GC5441z8i7N8swk1.htm)|Effect: Pulse of Rage|Effet : Pulsion de rage|libre|
|[gHczZWA34WQx4px0.htm](bestiary-effects/gHczZWA34WQx4px0.htm)|Effect: Bully the Departed|Effet : Harcèlement des défunts|libre|
|[gjo8LR6uOhDM5W7L.htm](bestiary-effects/gjo8LR6uOhDM5W7L.htm)|Effect: Impaling Chain|Effet : Chaîne empaleuse|libre|
|[gORyINQLvplfthlm.htm](bestiary-effects/gORyINQLvplfthlm.htm)|Effect: Pollution Infusion|Effet : Imprégnation de pollution|officielle|
|[GstmxSP75eKmHtCQ.htm](bestiary-effects/GstmxSP75eKmHtCQ.htm)|Effect: Hallucinogenic Pollen (Failure)|Effet : Pollen hallucinogène (Échec)|officielle|
|[gxBjyPaYDdhjAImf.htm](bestiary-effects/gxBjyPaYDdhjAImf.htm)|Effect: Ancestral Response|Effet : réponse ancestrale|libre|
|[H99PJXU5cvJ4Oycm.htm](bestiary-effects/H99PJXU5cvJ4Oycm.htm)|Effect: Fiery Form|Effet : Forme embrasée|libre|
|[HafP5vrdGdoqoFMo.htm](bestiary-effects/HafP5vrdGdoqoFMo.htm)|Effect: Barbed Net|Effet : Filet barbelé|libre|
|[HArljmKc2IR7rtfc.htm](bestiary-effects/HArljmKc2IR7rtfc.htm)|Effect: Swig|Effet : Rasade|libre|
|[hc7kR6quUE9ryRyj.htm](bestiary-effects/hc7kR6quUE9ryRyj.htm)|Effect: Splintered Ground (Critical Failure)|Effet : Champ d'esquilles (Échec critique)|officielle|
|[hdsl0XexqEL2emE3.htm](bestiary-effects/hdsl0XexqEL2emE3.htm)|Effect: Lantern of Hope (Gestalt)|Effet : Lanterne d'espoir (Gestalt)|officielle|
|[he0QiJSgJWNeIoxt.htm](bestiary-effects/he0QiJSgJWNeIoxt.htm)|Effect: Wrath of Spurned Hospitality|Effet : Courroux de l'hospitalité refusée|libre|
|[HF2kOaqrjCkaZnK9.htm](bestiary-effects/HF2kOaqrjCkaZnK9.htm)|Effect: Prepare Fangs|Effet : Préparation des crochets|libre|
|[hftFxE8JGJGzXAtU.htm](bestiary-effects/hftFxE8JGJGzXAtU.htm)|Effect: Song of the Swamp|Effet : Chant des marais|libre|
|[HGP0c0tOK2WnC4p5.htm](bestiary-effects/HGP0c0tOK2WnC4p5.htm)|Effect: Breach the Abyss - Fire|Effet : Brèche des Failles extérieures - feu|libre|
|[HKyMZobjOsd6WFuo.htm](bestiary-effects/HKyMZobjOsd6WFuo.htm)|Effect: Hurtful Critique|Effet : Critique acerbe|libre|
|[HpX3bGlOkSp1PeWg.htm](bestiary-effects/HpX3bGlOkSp1PeWg.htm)|Effect: Vent Energy|Effet : Évacuation d'énergie|libre|
|[hQsqr2sRPp9rNm4U.htm](bestiary-effects/hQsqr2sRPp9rNm4U.htm)|Effect: Putrid Blast|Effet : Éruption nauséabonde|officielle|
|[Hv4NO0HADyAAdS4F.htm](bestiary-effects/Hv4NO0HADyAAdS4F.htm)|Effect: Bark Orders|Effet : Aboyer des ordres|libre|
|[i3942RucZD9OHbAE.htm](bestiary-effects/i3942RucZD9OHbAE.htm)|Effect: Void Shroud|Effet : Linceul de néant|libre|
|[i4S6h5c1KK4FLq6w.htm](bestiary-effects/i4S6h5c1KK4FLq6w.htm)|Effect: Feed|Effet : Alimentation|officielle|
|[I5Fd4TkIKRJT6WXf.htm](bestiary-effects/I5Fd4TkIKRJT6WXf.htm)|Effect: Aura of Righteousness (Planetar)|Effet : Aura de vertu (Planétar)|libre|
|[iDLu83vhWoNIE7xt.htm](bestiary-effects/iDLu83vhWoNIE7xt.htm)|Effect: Commanding Aura (Quara)|Effet : Aura de commandement (Quara)|libre|
|[IfUod2VxNmZMGGPq.htm](bestiary-effects/IfUod2VxNmZMGGPq.htm)|Effect: Bodyguard's Defense|Effet : Défense du garde du corps|libre|
|[Il7YN8zlu3dfw3IE.htm](bestiary-effects/Il7YN8zlu3dfw3IE.htm)|Effect: Pinpoint Weakness|Effet : Identifier la faiblesse|libre|
|[inLBsuLRE5xmVHdn.htm](bestiary-effects/inLBsuLRE5xmVHdn.htm)|Effect: Infernal Wound|Effet : Blessure infernale|libre|
|[insc09aslLptd0dN.htm](bestiary-effects/insc09aslLptd0dN.htm)|Effect: Flammable|Effet : Inflammable|libre|
|[irSKPLF5oLfnqUNW.htm](bestiary-effects/irSKPLF5oLfnqUNW.htm)|Effect: Spotlight|Effet : Lumière des spots|libre|
|[IrXb05caCTkP792e.htm](bestiary-effects/IrXb05caCTkP792e.htm)|Effect: Reminder of Doom|Effet : Souvenir du destin|libre|
|[iSyyR4QVOaY6mxtg.htm](bestiary-effects/iSyyR4QVOaY6mxtg.htm)|Effect: Nymph Queen's Inspiration|Effet : Inspiration de la nymphe souveraine|libre|
|[itdoXvcDo8wwmTME.htm](bestiary-effects/itdoXvcDo8wwmTME.htm)|Effect: Despoiler|Effet : Affaiblissement|libre|
|[itVZwwvaXYN2zOR8.htm](bestiary-effects/itVZwwvaXYN2zOR8.htm)|Effect: Parry Dance|Effet : Danse de parade|libre|
|[iuy6AA6sQZUZkN9X.htm](bestiary-effects/iuy6AA6sQZUZkN9X.htm)|Effect: Rapid Evolution - Husk|Effet : Évolution rapide - Dépouille|libre|
|[iWLAcND0iyW4NUZm.htm](bestiary-effects/iWLAcND0iyW4NUZm.htm)|Effect: Viscous Trap|Effet : Piège visqueux|libre|
|[iXVsVqttUGlfdIHS.htm](bestiary-effects/iXVsVqttUGlfdIHS.htm)|Effect: Share Pain|Effet : Partager la douleur|libre|
|[IySCRGYxxLFDlIZI.htm](bestiary-effects/IySCRGYxxLFDlIZI.htm)|Effect: Roast|Effet : Rôtir|libre|
|[J6EwMsXtXg30eaiB.htm](bestiary-effects/J6EwMsXtXg30eaiB.htm)|Effect: Stoke the Fervent (High Tormentor)|Effet : Raviver le fervent - Grand Bourreau|officielle|
|[JgJ4Iw8BvJ4tWkah.htm](bestiary-effects/JgJ4Iw8BvJ4tWkah.htm)|Effect: Soul Lock (Healing)|Effet : Verrou d'âme (Guérison)|libre|
|[JlaO7fgm53Om1DxB.htm](bestiary-effects/JlaO7fgm53Om1DxB.htm)|Effect: Swell|Effet : Gonfler|libre|
|[jQmfZcPD6xVYx5nb.htm](bestiary-effects/jQmfZcPD6xVYx5nb.htm)|Effect: Entangling Slime|Effet : Mucosité enchevêtrante|libre|
|[jrfuplUEq3PIo6j1.htm](bestiary-effects/jrfuplUEq3PIo6j1.htm)|Effect: Blasphemous Utterances|Effet : Paroles blasphématoires|libre|
|[K3r1lfbtCTf3dFhV.htm](bestiary-effects/K3r1lfbtCTf3dFhV.htm)|Effect: Stick a Fork in It|Effet : Pique une fourchette dedans|libre|
|[KdV8UtN8Af5oCerj.htm](bestiary-effects/KdV8UtN8Af5oCerj.htm)|Effect: Cite Precedent|Effet : Citer un précédent|libre|
|[kdZEZ6iBYQPVGzYD.htm](bestiary-effects/kdZEZ6iBYQPVGzYD.htm)|Effect: Call to Action|Effet : Appel à la mobilisation|officielle|
|[kHRbOHKEedKaBFH5.htm](bestiary-effects/kHRbOHKEedKaBFH5.htm)|Effect: Transfer Protection|Effet : Transfert de protection|libre|
|[kjP0J97hhoIhbF3M.htm](bestiary-effects/kjP0J97hhoIhbF3M.htm)|Effect: Necrotic Field|Effet : Champ nécrotique|libre|
|[KjWPSJonzrZ8jv7X.htm](bestiary-effects/KjWPSJonzrZ8jv7X.htm)|Effect: Phalanx Fighter|Effet : Guerrier de phalange|libre|
|[kkP0tagXmAYuSH3u.htm](bestiary-effects/kkP0tagXmAYuSH3u.htm)|Effect: Impersonated Ability|Effet : Capacité d'imposteur|libre|
|[kOGRUuyK04MjLc3m.htm](bestiary-effects/kOGRUuyK04MjLc3m.htm)|Effect: Spiritual Warden|Effet : Gardien spirituel|libre|
|[KpRRcLsAiIhJFE03.htm](bestiary-effects/KpRRcLsAiIhJFE03.htm)|Effect: Furious Possession|Effet : Possession de fureur|libre|
|[KuCh1JGrAidkKBav.htm](bestiary-effects/KuCh1JGrAidkKBav.htm)|Effect: Challenge Foe|Effet : Défier l'ennemi|libre|
|[l62iAFL3EO7wSsLL.htm](bestiary-effects/l62iAFL3EO7wSsLL.htm)|Effect: Form a Phalanx|Effet : Formation en phalange|libre|
|[L7SiTshdimUPWfnB.htm](bestiary-effects/L7SiTshdimUPWfnB.htm)|Effect: Witchflame (Failure)|Effet : Flammes ensorcelées (échec)|libre|
|[lBpprC8VD4GRzTtg.htm](bestiary-effects/lBpprC8VD4GRzTtg.htm)|Effect: Wild Shape (Oaksteward)|Effet : Forme indomptée (Oaksteward)|libre|
|[LgZx5xotO08JzhVc.htm](bestiary-effects/LgZx5xotO08JzhVc.htm)|Effect: Mutilating Bite|Effet : Morsure estropiante|libre|
|[lIEc8Lx3ROm17Dqa.htm](bestiary-effects/lIEc8Lx3ROm17Dqa.htm)|Effect: Fetid Screech (Critical Failure)|Effet : Cri fétide (échec critique)|libre|
|[lJGAJF9QiXCOPd2I.htm](bestiary-effects/lJGAJF9QiXCOPd2I.htm)|Effect: Draconic Breath Weapon Aura|Effet : Aura d'arme de souffle draconique|libre|
|[LohrnRXCQ0yVt8MK.htm](bestiary-effects/LohrnRXCQ0yVt8MK.htm)|Effect: Raise Guard|Effet : Lever sa garde|libre|
|[LOLoQpaAp5cC0hbm.htm](bestiary-effects/LOLoQpaAp5cC0hbm.htm)|Effect: Blood Fury|Effet : Furie du sang|libre|
|[lR49i3wO6TxJAK9o.htm](bestiary-effects/lR49i3wO6TxJAK9o.htm)|Effect: Dust Eternal|Effet : Poussière éternelle|libre|
|[lt2t24E4hiByHhi3.htm](bestiary-effects/lt2t24E4hiByHhi3.htm)|Effect: Lost Plates|Effet : Plaques perdues|libre|
|[lyeRL7khixdlJsaf.htm](bestiary-effects/lyeRL7khixdlJsaf.htm)|Effect: Death Gasp|Effet : Soupir de mort|libre|
|[McawF8weCe7O81um.htm](bestiary-effects/McawF8weCe7O81um.htm)|Effect: Witchflame (Critical Failure)|Effet : Flammes ensorcelées (échec critique)|libre|
|[mD44D9I05NQaXiED.htm](bestiary-effects/mD44D9I05NQaXiED.htm)|Effect: Fetid Fumes|Effet : Vapeurs fétides|libre|
|[Me16QQGxpivWE2WW.htm](bestiary-effects/Me16QQGxpivWE2WW.htm)|Effect: Gloom Aura|Effet : Aura de morosité|libre|
|[mFnSXW6HTwS7Iu1k.htm](bestiary-effects/mFnSXW6HTwS7Iu1k.htm)|Effect: Hunted Fear|Effet : Chasse terrifiante|libre|
|[MKC2ne315YiCUiWd.htm](bestiary-effects/MKC2ne315YiCUiWd.htm)|Effect: Inspire Defense (Love Siktempora)|Effet : Inspiration défensive (Siktempora de l'amour)|libre|
|[mo4IRyv7GGRBJihU.htm](bestiary-effects/mo4IRyv7GGRBJihU.htm)|Effect: Discerning Aura|Effet : Aura de discernement|libre|
|[MT5gxuuxBaN2RdYf.htm](bestiary-effects/MT5gxuuxBaN2RdYf.htm)|Effect: Inspired Recitation|Effet : Discours inspiré|libre|
|[MxArXuo8JRCm1hus.htm](bestiary-effects/MxArXuo8JRCm1hus.htm)|Effect: Unsettling Attention|Effet : Attention déconcertante|libre|
|[MxFFGo4WJpHh1e70.htm](bestiary-effects/MxFFGo4WJpHh1e70.htm)|Effect: Lightning Powered|Effet : Puissance électrique|libre|
|[Mz1ihIHaHngdFhsA.htm](bestiary-effects/Mz1ihIHaHngdFhsA.htm)|Effect: Musk|Effet : Musc|libre|
|[n96DB8vpPr1Fu6GZ.htm](bestiary-effects/n96DB8vpPr1Fu6GZ.htm)|Effect: Smash Kneecaps|Effet : Pêter les rotules|libre|
|[NHwmrSyF0e8HUE3m.htm](bestiary-effects/NHwmrSyF0e8HUE3m.htm)|Effect: Witchflame (Success)|Effet : Flammes ensorcelées (Succès)|libre|
|[NLjzI7gt6djXSoXc.htm](bestiary-effects/NLjzI7gt6djXSoXc.htm)|Effect: Remove Face|Effet : Ablation de visage|libre|
|[No817gKxdliCVQ9K.htm](bestiary-effects/No817gKxdliCVQ9K.htm)|Effect: Energy Ward|Effet : Protection énergétique|libre|
|[nP1KEogCiRdVq0UB.htm](bestiary-effects/nP1KEogCiRdVq0UB.htm)|Effect: Sanguine Rain|Effet : Pluie de sang|libre|
|[o0zqsH5kYopRanav.htm](bestiary-effects/o0zqsH5kYopRanav.htm)|Effect: Rope Snare|Effet : Corde d'enchevêtrement|libre|
|[o6ZI01zZBQDASuaw.htm](bestiary-effects/o6ZI01zZBQDASuaw.htm)|Effect: Wolf Shape|Effet : Forme de loup|officielle|
|[ob00D61F0c4PIvj1.htm](bestiary-effects/ob00D61F0c4PIvj1.htm)|Effect: Utter Despair|Effet : Grand désespoir|officielle|
|[oKNFSWJW2jat8Dli.htm](bestiary-effects/oKNFSWJW2jat8Dli.htm)|Effect: Soul Crush (Healing)|Effet : Broyage d'âme (Soins)|libre|
|[OMJnZspk5YHai8yn.htm](bestiary-effects/OMJnZspk5YHai8yn.htm)|Effect: Fed by Metal|Effet : Renforcé par le métal|libre|
|[oORIapm28xioxPDp.htm](bestiary-effects/oORIapm28xioxPDp.htm)|Effect: Blood Wake|Effet : Sillage de sang|libre|
|[otUESSH8BDdPKHtb.htm](bestiary-effects/otUESSH8BDdPKHtb.htm)|Effect: Hamstring|Effet : Tranche tendon|libre|
|[OxOMYmlPtjsEkRtY.htm](bestiary-effects/OxOMYmlPtjsEkRtY.htm)|Effect: Aura of Command|Effet : Aura du commandant|officielle|
|[P4bNtDVyknQwjTPo.htm](bestiary-effects/P4bNtDVyknQwjTPo.htm)|Effect: Runic Resistance|Effet : Résistance runique|libre|
|[PFeGCDFOo2AjI6ib.htm](bestiary-effects/PFeGCDFOo2AjI6ib.htm)|Effect: Ganzi Resistance|Effet : Résistance ganzi|libre|
|[pfG1S7yShwkHT9e0.htm](bestiary-effects/pfG1S7yShwkHT9e0.htm)|Effect: Calming Bioluminescence|Effet : Bioluminescence apaisante|officielle|
|[PJxUFFeAvQJ4l52f.htm](bestiary-effects/PJxUFFeAvQJ4l52f.htm)|Effect: Choose Weakness|Effet : Choix des faiblesses|libre|
|[pMPe01GWvfwcYwUR.htm](bestiary-effects/pMPe01GWvfwcYwUR.htm)|Effect: Defensive Assault|Effet : Assaut défensif|libre|
|[PR0YzItS9R60X3Zc.htm](bestiary-effects/PR0YzItS9R60X3Zc.htm)|Effect: Aura of Growth|Effet : Aura de croissance|libre|
|[pvA97TXnq7wCbXp6.htm](bestiary-effects/pvA97TXnq7wCbXp6.htm)|Effect: Call to Halt (Success)|Effet : Halte-là (Succès)|officielle|
|[PxLWRiCbgpqOTLO9.htm](bestiary-effects/PxLWRiCbgpqOTLO9.htm)|Effect: Towering Stance (Disbelieved)|Effet : Grande prestance (Percer l'illusion)|libre|
|[q2D1QBalqBQfKzTc.htm](bestiary-effects/q2D1QBalqBQfKzTc.htm)|Effect: Hurl Net|Effet : Lancé de filet|libre|
|[Qa0HP5uCuqh4vxBh.htm](bestiary-effects/Qa0HP5uCuqh4vxBh.htm)|Effect: Consume Organ|Effet : Consommation d'organe|libre|
|[qddahkHks1iLZ4sM.htm](bestiary-effects/qddahkHks1iLZ4sM.htm)|Effect: Praise Adachros|Effet : Louange à l'adachros|libre|
|[QDs8t1U1IepzYlyi.htm](bestiary-effects/QDs8t1U1IepzYlyi.htm)|Effect: Sylvan Wine|Effet : Vin sylvestre|officielle|
|[qFDiWpzk8cuwQGyH.htm](bestiary-effects/qFDiWpzk8cuwQGyH.htm)|Effect: Hypnotic Stare|Effet : Regard hypnotisant|officielle|
|[QjgLHIgRXIk3KAOY.htm](bestiary-effects/QjgLHIgRXIk3KAOY.htm)|Effect: Curl Up|Effet : S'enrouler|libre|
|[QMcmhifNQOPguv2t.htm](bestiary-effects/QMcmhifNQOPguv2t.htm)|Effect: Ectoplasmic Form (Physical)|Effet : Forme ectoplasmique (physique)|libre|
|[Qomp2EujVCbzJb4X.htm](bestiary-effects/Qomp2EujVCbzJb4X.htm)|Effect: Filth Wave|Effet : Vague de fange|libre|
|[QoneHsjZKtGHWlam.htm](bestiary-effects/QoneHsjZKtGHWlam.htm)|Effect: Aura of Misfortune|Aura d'infortune|officielle|
|[QpG9AaYpXJzDEjZA.htm](bestiary-effects/QpG9AaYpXJzDEjZA.htm)|Effect: Devour Soul|Effet : Engloutissement d'âme|officielle|
|[QqC9xymSXSVunDgl.htm](bestiary-effects/QqC9xymSXSVunDgl.htm)|Effect: Tail Lash (Attack Roll)|Effet : Coup de queue (jet d'attaque)|libre|
|[QSy3KaLI1AYTR7rP.htm](bestiary-effects/QSy3KaLI1AYTR7rP.htm)|Effect: Wrath of Fate|Effet : Courroux du destin|libre|
|[QzJv3TybABeCjwQc.htm](bestiary-effects/QzJv3TybABeCjwQc.htm)|Effect: Riding Zogototaru|Effet : Monter zogototaru|libre|
|[R8clbf7gtIlU3fIj.htm](bestiary-effects/R8clbf7gtIlU3fIj.htm)|Effect: Crush of Hundreds|Effet : Foule écrasante|officielle|
|[RAKgTTFKc2YEbvrc.htm](bestiary-effects/RAKgTTFKc2YEbvrc.htm)|Effect: Hallucinogenic Pollen (Critical Failure)|Effet : Pollen hallucinogène (Échec critique)|officielle|
|[RBfd5qwFkuKxNb57.htm](bestiary-effects/RBfd5qwFkuKxNb57.htm)|Effect: Rapid Evolution - Energy Gland|Effet : Évolution rapide - Glande énergétique|libre|
|[rbgXQqXbpaQCcNNx.htm](bestiary-effects/rbgXQqXbpaQCcNNx.htm)|Effect: Read the Stars (Critical Success)|Effet : Lecture des astres (succès critique)|libre|
|[RbyXD99lybxPK6yS.htm](bestiary-effects/RbyXD99lybxPK6yS.htm)|Effect: Warning Hoot|Effet : Hululement d'avertissement|libre|
|[RjrsiEloXs7ze1TZ.htm](bestiary-effects/RjrsiEloXs7ze1TZ.htm)|Effect: Aura of Vitality|Effet : Aura de vitalité|libre|
|[Rn1aE4uvq30iiH0Y.htm](bestiary-effects/Rn1aE4uvq30iiH0Y.htm)|Effect: Regression|Effet : Régression|libre|
|[rpxdrOlzY2SOtMWB.htm](bestiary-effects/rpxdrOlzY2SOtMWB.htm)|Effect: Whirlwind Form|Effet : Forme de tornade|libre|
|[rRgVlTHiNEJ86T7N.htm](bestiary-effects/rRgVlTHiNEJ86T7N.htm)|Effect: Nanite Surge (Glow)|Effet : Poussée nanite (briller)|libre|
|[ryY6fdqpC5Ztnagd.htm](bestiary-effects/ryY6fdqpC5Ztnagd.htm)|Effect: Dirty Bomb (Critical Failure)|Effet : Bombe sale (échec critique)|officielle|
|[s16XQpDz2HNzR9BB.htm](bestiary-effects/s16XQpDz2HNzR9BB.htm)|Effect: Aura of Protection (Solar)|Effet : Aura de protection (Solar)|libre|
|[S38CM3hyr7DnsmRR.htm](bestiary-effects/S38CM3hyr7DnsmRR.htm)|Effect: Devour Soul (Victim Level 15 or Higher)|Effet : Engloutissement d'âme (niveau de la victime 15 ou supérieur)|officielle|
|[s4XFUgK8UgKnpKrX.htm](bestiary-effects/s4XFUgK8UgKnpKrX.htm)|Effect: Chemical Fertilizer|Effet : Fertilisant chimique|libre|
|[s5mS7CyE0oOYlecv.htm](bestiary-effects/s5mS7CyE0oOYlecv.htm)|Effect: Lost Red Cap|Effet : Bonnet rouge perdu|libre|
|[SA5f54t1dVah5fJm.htm](bestiary-effects/SA5f54t1dVah5fJm.htm)|Effect: Revert Form|Effet : Reprendre sa forme|libre|
|[ScZ8tlV5zS0Jwnqn.htm](bestiary-effects/ScZ8tlV5zS0Jwnqn.htm)|Effect: Blood Frenzy|Effet : Frénésie de sang|libre|
|[seWuw1GMSm3kzCWV.htm](bestiary-effects/seWuw1GMSm3kzCWV.htm)|Effect: Fan Bolt|Effet : Carreau arbalète|libre|
|[sI73mEP9wZRvMpWw.htm](bestiary-effects/sI73mEP9wZRvMpWw.htm)|Effect: Curse of Frost|Effet : Malédiction du givre|libre|
|[SjrM6XVTLySToS12.htm](bestiary-effects/SjrM6XVTLySToS12.htm)|Effect: Soul Siphon|Effet : Siphon d'âme|officielle|
|[St2QzqQvKN0IaXxG.htm](bestiary-effects/St2QzqQvKN0IaXxG.htm)|Effect: Kharna's Blessing|Effet : Bénédiction de Kharnas|officielle|
|[svXRrZvixRInmabj.htm](bestiary-effects/svXRrZvixRInmabj.htm)|Effect: Rune of Destruction (Greatsword)|Effet : Rune de destruction (épée à deux mains)|libre|
|[SyLqg8SffVlA7fi7.htm](bestiary-effects/SyLqg8SffVlA7fi7.htm)|Effect: Overdrive Engine|Effet : Surcharge moteur|libre|
|[T2tfacwoOozQfsIz.htm](bestiary-effects/T2tfacwoOozQfsIz.htm)|Effect: Infectious Aura|Effet : Aura infectieuse|libre|
|[t8Yxrx3XgjS78Hxt.htm](bestiary-effects/t8Yxrx3XgjS78Hxt.htm)|Effect: Assimilate Lava|Effet : Assimiler la lave|libre|
|[T9wQ1LvsvPWTefQR.htm](bestiary-effects/T9wQ1LvsvPWTefQR.htm)|Effect: Under Command|Effet : Sous les ordres|libre|
|[TbmkcfpKIs558skY.htm](bestiary-effects/TbmkcfpKIs558skY.htm)|Effect: Caustic Mucus|Effet : Mucus corrosif|officielle|
|[TcgvhgpUkJFjVqBx.htm](bestiary-effects/TcgvhgpUkJFjVqBx.htm)|Effect: Stink Sap|Effet : Sève puante|libre|
|[TEzLEXBZzNCp6flg.htm](bestiary-effects/TEzLEXBZzNCp6flg.htm)|Effect: Instinctual Tinker (Critical Success)|Effet : Bricoleur instinctif (Succès critique)|libre|
|[TjRZbd52qWPjTbNT.htm](bestiary-effects/TjRZbd52qWPjTbNT.htm)|Effect: No Quarter!|Effet : Pas de quartier !|libre|
|[tJx9B2e3AET6PbJD.htm](bestiary-effects/tJx9B2e3AET6PbJD.htm)|Effect: Pain Frenzy|Effet : Frénésie de la douleur|officielle|
|[Tkf33YglEZcZ6gOT.htm](bestiary-effects/Tkf33YglEZcZ6gOT.htm)|Effect: Curse of the Baneful Venom|Effet : Malédiction du venin maléfique|libre|
|[TSdrv1FmLHJnk9AD.htm](bestiary-effects/TSdrv1FmLHJnk9AD.htm)|Effect: Shadow Rapier|Effet : Rapière des ombres|libre|
|[tSF9z5VTeevxoww3.htm](bestiary-effects/tSF9z5VTeevxoww3.htm)|Effect: Harmonizing Aura (Enemies)|Effet : Aura d'harmonisation (Ennemis)|libre|
|[tvybMInVm415jA3p.htm](bestiary-effects/tvybMInVm415jA3p.htm)|Effect: Bloodline Magic|Effet : Magie du lignage|libre|
|[U05L8bPBcRHTtTB3.htm](bestiary-effects/U05L8bPBcRHTtTB3.htm)|Effect: Crushing Spirits|Effets : Esprits écrasants|libre|
|[u1Z7i2RyL6Sd9OVU.htm](bestiary-effects/u1Z7i2RyL6Sd9OVU.htm)|Effect: Take Them Down!|Effet : Abattez-les !|libre|
|[u2z1BgpZsxl7Zc2z.htm](bestiary-effects/u2z1BgpZsxl7Zc2z.htm)|Effect: Poison Frenzy|Effet : Déchaînement de poison|officielle|
|[uB3CVn3momZO9h0L.htm](bestiary-effects/uB3CVn3momZO9h0L.htm)|Effect: Xulgath Stench|Effet : Puanteur xulgath|officielle|
|[ucAwSIZsGrpBLg6G.htm](bestiary-effects/ucAwSIZsGrpBLg6G.htm)|Effect: Alchemical Crossbow|Effet : Arbalète alchimique|libre|
|[uFi8GhPkYY3ti40N.htm](bestiary-effects/uFi8GhPkYY3ti40N.htm)|Effect: Inspire Courage (Love Siktempora)|Effet : Hymne de courage (Siktempora de l'amour)|libre|
|[UgoM6FBiZfc7KqCV.htm](bestiary-effects/UgoM6FBiZfc7KqCV.htm)|Effect: Curse of Stone|Effet : Malédiction de la pierre|libre|
|[UiISDbCjK6AUDsll.htm](bestiary-effects/UiISDbCjK6AUDsll.htm)|Effect: Guide's Warning|Effet : Avertissement du guide|officielle|
|[uqjx1bwYduHwwC7d.htm](bestiary-effects/uqjx1bwYduHwwC7d.htm)|Effect: Guiding Words|Effet : Indiquer la faiblesse|officielle|
|[us3KiMrcGM8e6ri3.htm](bestiary-effects/us3KiMrcGM8e6ri3.htm)|Effect: Sylvan Wine (Horned Hunter)|Effet : Vin sylvestre (Chasseur cornu)|libre|
|[USMuxi8ACBJRUrdS.htm](bestiary-effects/USMuxi8ACBJRUrdS.htm)|Effect: Crocodile Form|Effet : Forme de crocodile|libre|
|[UuEUZU1fSUb23yOk.htm](bestiary-effects/UuEUZU1fSUb23yOk.htm)|Effect: Totems of the Past|Effet : Totems du passé|libre|
|[UxeoSRmPbD5hjiEi.htm](bestiary-effects/UxeoSRmPbD5hjiEi.htm)|Effect: Inspiring Presence|Effet : Présence inspirante|libre|
|[UxNq9uo1GpDYTi5Z.htm](bestiary-effects/UxNq9uo1GpDYTi5Z.htm)|Effect: Waterlogged|Effet : Gorgé d'eau|libre|
|[uZJOdounIHaFDC1t.htm](bestiary-effects/uZJOdounIHaFDC1t.htm)|Effect: Hydra Heads|Effet : Têtes de l'hydre|officielle|
|[v1oNhc9xa8nWxmFQ.htm](bestiary-effects/v1oNhc9xa8nWxmFQ.htm)|Effect: Breach the Abyss - Acid|Effet : Brèche dans les Failles extérieures - Acide|libre|
|[v54mj5jknAynlCM9.htm](bestiary-effects/v54mj5jknAynlCM9.htm)|Effect: Call to Blood|Effet : Appel du sang|libre|
|[V60TWjo1G1r5ZtPM.htm](bestiary-effects/V60TWjo1G1r5ZtPM.htm)|Effect: Vulnerable to Curved Space|Effet : Vulnérable aux espaces courbes|libre|
|[VAy6IZ0ck7ALVL4b.htm](bestiary-effects/VAy6IZ0ck7ALVL4b.htm)|Effect: Rune of Smiting (Greatsword)|Effet : Rune de destruction (épée à deux mains)|libre|
|[vbxcQIjLVWa43sdt.htm](bestiary-effects/vbxcQIjLVWa43sdt.htm)|Effect: Rune of Destruction (Rock)|Effet : Rune de destruction (Rocher)|libre|
|[vc3AOrJpacJ7T1hO.htm](bestiary-effects/vc3AOrJpacJ7T1hO.htm)|Effect: Deceitful Feast (Failure)|Effet : Festin illusoire (Échec)|libre|
|[VFIVdYmS3XTdd3hj.htm](bestiary-effects/VFIVdYmS3XTdd3hj.htm)|Effect: Curse of Fire|Effet : Malédiction du feu|libre|
|[vGL5LlFxVJqjy1gM.htm](bestiary-effects/vGL5LlFxVJqjy1gM.htm)|Effect: Breach the Abyss - Cold|Effet : Brèche dans les Failles extérieures - froid|libre|
|[vJAHjBoEuFKGQoGs.htm](bestiary-effects/vJAHjBoEuFKGQoGs.htm)|Effect: Rune of Destruction (Longspear)|Effet : Rune de destruction (Pique)|libre|
|[vKTvlFD2OSgTPuvl.htm](bestiary-effects/vKTvlFD2OSgTPuvl.htm)|Effect: Breach the Abyss - Electricity|Effet : Brèche dans les Failles extérieures - électricité|libre|
|[VLOvyQiZKeXppS2L.htm](bestiary-effects/VLOvyQiZKeXppS2L.htm)|Effect: Gird in Prayer|Effet : Ceint de prières|libre|
|[VOorielY4wCSfqOr.htm](bestiary-effects/VOorielY4wCSfqOr.htm)|Effect: Bolster Torag's Chosen|Effet : Renforcement de l'Élu de Torag|libre|
|[vuaungtl9VCBoHDK.htm](bestiary-effects/vuaungtl9VCBoHDK.htm)|Effect: Rockfall Failure|Effet : Échec à l'éboulement|libre|
|[vUFIbQ74jz22rOXw.htm](bestiary-effects/vUFIbQ74jz22rOXw.htm)|Effect: Mortal Reflection|Effet : Reflet mortel|libre|
|[Vz9Cb8FEzNwfH3mE.htm](bestiary-effects/Vz9Cb8FEzNwfH3mE.htm)|Effect: Breach the Abyss - Negative|Effet : Brèche dans les Failles extérieures - vide|libre|
|[wcNGbhwn3AuamtsX.htm](bestiary-effects/wcNGbhwn3AuamtsX.htm)|Effect: Scraping Clamor|Effet : Grattage assourdissant|officielle|
|[wE4S9lwOhtaRAm9n.htm](bestiary-effects/wE4S9lwOhtaRAm9n.htm)|Effect: Active Mutagen|Effet : Mutagène actif|libre|
|[wEFRo8enunRLFsVE.htm](bestiary-effects/wEFRo8enunRLFsVE.htm)|Effect: Psychic Daze|effet : Hébètement psychique|libre|
|[WfiaKdXNSxC3POcs.htm](bestiary-effects/WfiaKdXNSxC3POcs.htm)|Effect: Bosun's Command - Attack Bonus|Effet : Ordre de bosco - Bonus d'attaque|libre|
|[wHQZO70bSECtSUZn.htm](bestiary-effects/wHQZO70bSECtSUZn.htm)|Effect: Ancestral Journey|Effet : Voyage ancestral|libre|
|[wJsOZsYI2ZUVcGxc.htm](bestiary-effects/wJsOZsYI2ZUVcGxc.htm)|Effect: Fiddle|Effet : Stridulation|libre|
|[wL4QI1h4YvdHoOuG.htm](bestiary-effects/wL4QI1h4YvdHoOuG.htm)|Effect: Drink Emotions|Effet : Boire les émotions|libre|
|[wPIGBcpCqCWgrCiq.htm](bestiary-effects/wPIGBcpCqCWgrCiq.htm)|Effect: Blood Soak|Effet : Imbibition de sang|libre|
|[wPW6kVscJgBLPVKZ.htm](bestiary-effects/wPW6kVscJgBLPVKZ.htm)|Effect: Ghonhatine Feed|Effet : Nourrir la ghonhatine|libre|
|[wX9L6fbqVMLP05hn.htm](bestiary-effects/wX9L6fbqVMLP05hn.htm)|Effect: Stench|Effet : Puanteur|officielle|
|[WYJdjmvfm8J6sG6g.htm](bestiary-effects/WYJdjmvfm8J6sG6g.htm)|Effect: Jury-Rig|Effet : Bricolé|libre|
|[X1Q1uBiylZoJg3Co.htm](bestiary-effects/X1Q1uBiylZoJg3Co.htm)|Effect: Howl of Vengeful Fury|Effet : Hurlement de la furie vengeresse|libre|
|[x8e0MXrPgdRYQqBm.htm](bestiary-effects/x8e0MXrPgdRYQqBm.htm)|Effect: Field of Undeath|Effet : Champ de morts-vivants|libre|
|[XaYM6Td0yhx2POau.htm](bestiary-effects/XaYM6Td0yhx2POau.htm)|Effect: Haywire|Effet : Détraqué|officielle|
|[XE2YhBMl7wc6nQAZ.htm](bestiary-effects/XE2YhBMl7wc6nQAZ.htm)|Effect: Replenishment of War (Endlo)|Effet : Récupération martiale|libre|
|[XH49blfNDjkqnH5N.htm](bestiary-effects/XH49blfNDjkqnH5N.htm)|Effect: Motivating Assault|Effet : Assaut motivant|libre|
|[xiOi7btey4AS6jhe.htm](bestiary-effects/xiOi7btey4AS6jhe.htm)|Effect: Overtake Soul|Effet : Submersion d'âme|libre|
|[xmES7HVxM7L3j1EN.htm](bestiary-effects/xmES7HVxM7L3j1EN.htm)|Effect: Putrid Stench|Effet : Odeur putride|libre|
|[xOD3ufpzA8H7W4sP.htm](bestiary-effects/xOD3ufpzA8H7W4sP.htm)|Effect: Aura of Good Cheer|Effet : Aura de bonne humeur|libre|
|[XSAmMuMgMvdjdpHc.htm](bestiary-effects/XSAmMuMgMvdjdpHc.htm)|Effect: Slime Trap|Effet : Piège de gluant|libre|
|[XSXMGb5h4Um6YXiT.htm](bestiary-effects/XSXMGb5h4Um6YXiT.htm)|Effect: Air of Sickness|Effet : Air nauséabond|libre|
|[xXfzzNaBOqDUbYBD.htm](bestiary-effects/xXfzzNaBOqDUbYBD.htm)|Effect: Rotting Stench|Effet : Odeur de pourriture|libre|
|[XyhPcnlkg3mGqEwT.htm](bestiary-effects/XyhPcnlkg3mGqEwT.htm)|Effect: Rockfall Critical Failure|Effet : Échec critique à l'éboulement|libre|
|[Y5vsF5Kb8oNIccWo.htm](bestiary-effects/Y5vsF5Kb8oNIccWo.htm)|Effect: Rune of Smiting (Longspear)|Effet : Rune de destruction (Pique)|libre|
|[ya8r3IdzQ7QGNH8N.htm](bestiary-effects/ya8r3IdzQ7QGNH8N.htm)|Effect: Activate Defenses|Effet : Activation des défenses|libre|
|[Yb5rg3MEojIHQX8H.htm](bestiary-effects/Yb5rg3MEojIHQX8H.htm)|Effect: Compelled Performance|Effet : Représentation forcée|libre|
|[YDJnC3Bdc2GI6hX5.htm](bestiary-effects/YDJnC3Bdc2GI6hX5.htm)|Effect: Inspire Envoy|Effet : Inspiration de l'émissaire|libre|
|[yDut6pczQVRJqt2L.htm](bestiary-effects/yDut6pczQVRJqt2L.htm)|Effect: PFS Level Bump|Effet : Ajustement de niveau de la PFS|libre|
|[yG27Ixwdpxqbrhzf.htm](bestiary-effects/yG27Ixwdpxqbrhzf.htm)|Effect: Bosun's Command - Speed Bonus|Effet : Ordre de bosco - Bonus de vitesse|libre|
|[YIXeQFdrt0vPvmsW.htm](bestiary-effects/YIXeQFdrt0vPvmsW.htm)|Effect: Snake Form|Effet : Forme de serpent|libre|
|[YKCsmlMgI0aS7joO.htm](bestiary-effects/YKCsmlMgI0aS7joO.htm)|Effect: Silent Aura|Effet : Aura silencieuse|libre|
|[YOH7YsdlsLzlMu69.htm](bestiary-effects/YOH7YsdlsLzlMu69.htm)|Effect: Consume Soul|Effet : Dévorer les âmes|libre|
|[yQd2Yoht8libCMCv.htm](bestiary-effects/yQd2Yoht8libCMCv.htm)|Effect: Fortune's Favor|Effet : Faveur de la Fortune|officielle|
|[Yqq4AkZ9lrm4CcID.htm](bestiary-effects/Yqq4AkZ9lrm4CcID.htm)|Effect: Battle Cry|Effet : Cri de guerre|libre|
|[yrenENpzVgBKsnNi.htm](bestiary-effects/yrenENpzVgBKsnNi.htm)|Effect: Alchemical Strike|Effet : Frappe alchimique|libre|
|[yxgHWK2rTZKKAzDN.htm](bestiary-effects/yxgHWK2rTZKKAzDN.htm)|Effect: Unluck Aura|Effet : Aura de malchance|libre|
|[Z6cOCgyVwTr5t3yQ.htm](bestiary-effects/Z6cOCgyVwTr5t3yQ.htm)|Effect: Incendiary Dollop|Effet : Dollop incendiaire|libre|
|[zBbmuudbWY8FaOwZ.htm](bestiary-effects/zBbmuudbWY8FaOwZ.htm)|Effect: Chug|Effet : Descendre d'un trait|libre|
|[zBQM4rzLfkLzpET6.htm](bestiary-effects/zBQM4rzLfkLzpET6.htm)|Effect: Rune of Flames (Greatsword)|Effet : Rune de flammes (épée à deux mains)|libre|
|[zGWc4q0rXW9eMoYT.htm](bestiary-effects/zGWc4q0rXW9eMoYT.htm)|Effect: Phantasmagoric Fog|Effet : Brouillard fantasmagorique|libre|
|[zjVBG6uDd4WKWeKU.htm](bestiary-effects/zjVBG6uDd4WKWeKU.htm)|Effect: Downcast|Effet : Abattu|libre|
|[zNOomUXHZHcc1PWD.htm](bestiary-effects/zNOomUXHZHcc1PWD.htm)|Effect: Vengeful Fibers|Effet : Fibres vengeresses|libre|
|[zqgntQXIEbulBgZX.htm](bestiary-effects/zqgntQXIEbulBgZX.htm)|Effect: Share Defenses|Effet : Partager les défenses|libre|
|[zTrZozbWDd1A5L5S.htm](bestiary-effects/zTrZozbWDd1A5L5S.htm)|Effect: Cat Sith's Mark|Effet : Marque du chat sith|libre|
|[zXIpCkTtFRkhwI4x.htm](bestiary-effects/zXIpCkTtFRkhwI4x.htm)|Effect: Sixfold Flurry|Effet : Déluge sextuplé|libre|
|[zXzXDlh0HkyPJu8f.htm](bestiary-effects/zXzXDlh0HkyPJu8f.htm)|Effect: Ill Omen|Effet : Mauvais augure|libre|
|[zzdOof9hHUf9s13H.htm](bestiary-effects/zzdOof9hHUf9s13H.htm)|Effect: Manipulate Luck (Bad)|Effet : Manipulation de la chance (Mauvaise)|libre|
