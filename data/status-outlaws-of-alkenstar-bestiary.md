# État de la traduction (outlaws-of-alkenstar-bestiary)

 * **libre**: 58
 * **changé**: 39


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[3BFECcR5ggVH69Gu.htm](outlaws-of-alkenstar-bestiary/3BFECcR5ggVH69Gu.htm)|Skeletal Crocodile|Crocodile squelette|changé|
|[3dL5oBOLaFaAmDR4.htm](outlaws-of-alkenstar-bestiary/3dL5oBOLaFaAmDR4.htm)|Powderkeg Punk Bombardier|Bombardier des Punks Poudrenoire|changé|
|[3ibFfE1X8thaWb8A.htm](outlaws-of-alkenstar-bestiary/3ibFfE1X8thaWb8A.htm)|The Claws of Time|Les Griffes du temps|changé|
|[3SvdfSjrr5jXMrjF.htm](outlaws-of-alkenstar-bestiary/3SvdfSjrr5jXMrjF.htm)|Gold Tank Broker|Videur de la Choppe d'or|changé|
|[4tXjSKUChH1W9I6P.htm](outlaws-of-alkenstar-bestiary/4tXjSKUChH1W9I6P.htm)|Anjelique Loveless|Anjelique Loveless|changé|
|[5B6jpwqblxrqJHog.htm](outlaws-of-alkenstar-bestiary/5B6jpwqblxrqJHog.htm)|Shoma Lyzerius|Shoma Lyzérius|changé|
|[5jRhf7NP9thQtYCe.htm](outlaws-of-alkenstar-bestiary/5jRhf7NP9thQtYCe.htm)|Ogre Slug|Ogre Limace|changé|
|[7qaFFvXAYHp2G0CX.htm](outlaws-of-alkenstar-bestiary/7qaFFvXAYHp2G0CX.htm)|Powderkeg Punk Gunner|Artilleur des Punks Poudrenoire|changé|
|[8KUB92Ljms7hS7vq.htm](outlaws-of-alkenstar-bestiary/8KUB92Ljms7hS7vq.htm)|Kreeth-Ni|Kreeth-Ni|changé|
|[aT6c5oEPV8U5zfRD.htm](outlaws-of-alkenstar-bestiary/aT6c5oEPV8U5zfRD.htm)|Pyronite Ooze|Vase pyronite|changé|
|[BbTS7MMkz0Ob7qAF.htm](outlaws-of-alkenstar-bestiary/BbTS7MMkz0Ob7qAF.htm)|Clockwork Handler|Agent mécanique|changé|
|[c1v4Xb3x6nvpFmth.htm](outlaws-of-alkenstar-bestiary/c1v4Xb3x6nvpFmth.htm)|Atticus|Atticus|changé|
|[CNFAfmVoVebnj7bY.htm](outlaws-of-alkenstar-bestiary/CNFAfmVoVebnj7bY.htm)|Corrupt Shieldmarshal (Clan Pistol)|Marshal protecteur corrompu (Pistolet de clan)|changé|
|[dPVX37fMzAzb3mkv.htm](outlaws-of-alkenstar-bestiary/dPVX37fMzAzb3mkv.htm)|Clockwork Puppeteer|Marionnettiste mécanique|changé|
|[e2YnAE6qJUkGRS6p.htm](outlaws-of-alkenstar-bestiary/e2YnAE6qJUkGRS6p.htm)|Clockwork Fabricator|Contructeur mécanique|changé|
|[e9pKPMzzgT2qINEq.htm](outlaws-of-alkenstar-bestiary/e9pKPMzzgT2qINEq.htm)|Color Spray Trap|Piège Couleurs vertigineuses|changé|
|[eBM6yUAHDyEp8Ewi.htm](outlaws-of-alkenstar-bestiary/eBM6yUAHDyEp8Ewi.htm)|"Lucky" Lanks|"Lucky" Lanks|changé|
|[GmK9VzDtB1i7pmGI.htm](outlaws-of-alkenstar-bestiary/GmK9VzDtB1i7pmGI.htm)|Parsus|Parsus|changé|
|[hm6S6xdvn0pL2t3D.htm](outlaws-of-alkenstar-bestiary/hm6S6xdvn0pL2t3D.htm)|Headless Rustler|Voleur sans tête|changé|
|[irkdc3CACxbhd2MN.htm](outlaws-of-alkenstar-bestiary/irkdc3CACxbhd2MN.htm)|Cranium Preserver|Conteneur de crânes|changé|
|[IX2EDNagTD0YEfS5.htm](outlaws-of-alkenstar-bestiary/IX2EDNagTD0YEfS5.htm)|Dewey Daystar|Dewey Daystar|changé|
|[JsAj2gTuZbJJDA72.htm](outlaws-of-alkenstar-bestiary/JsAj2gTuZbJJDA72.htm)|Stink-Sap Trap|Piège de sève-puante|changé|
|[jXiIATJxyBLgjkm7.htm](outlaws-of-alkenstar-bestiary/jXiIATJxyBLgjkm7.htm)|Spellscar Sky Marauder|Rôdeur volant de la Cicatrice magique|changé|
|[KCnloN7xxY0UOhML.htm](outlaws-of-alkenstar-bestiary/KCnloN7xxY0UOhML.htm)|Alethsia|Aléthsia|changé|
|[kJNnY2a4bu0Q7hnl.htm](outlaws-of-alkenstar-bestiary/kJNnY2a4bu0Q7hnl.htm)|Captured Dezullon|Dézullone capturée|changé|
|[ktnfNng6W5Welsn3.htm](outlaws-of-alkenstar-bestiary/ktnfNng6W5Welsn3.htm)|Glaz Nixbrix|Glaz Nixbrix|changé|
|[LhjQnXhGFWhWVdmg.htm](outlaws-of-alkenstar-bestiary/LhjQnXhGFWhWVdmg.htm)|Steaming Kingdom Bartender|Tavernier du Royaume bouillant|changé|
|[lM7vowMNtvLnz8Q6.htm](outlaws-of-alkenstar-bestiary/lM7vowMNtvLnz8Q6.htm)|Clockwork Sphinx|Sphinx mécanisé|changé|
|[pgYXyo7Qs9b84bQh.htm](outlaws-of-alkenstar-bestiary/pgYXyo7Qs9b84bQh.htm)|Disgorged Zombie|Zombie régurgité|changé|
|[q7bUVljOk9Xy55Bt.htm](outlaws-of-alkenstar-bestiary/q7bUVljOk9Xy55Bt.htm)|Scarecophagus|Épouvantophage|changé|
|[rN4Q66R0601txrHg.htm](outlaws-of-alkenstar-bestiary/rN4Q66R0601txrHg.htm)|Hansin|Hansin|changé|
|[RpGReUxsaT8wCz2u.htm](outlaws-of-alkenstar-bestiary/RpGReUxsaT8wCz2u.htm)|Corrupt Shieldmarshal (Jezail)|Marshal protecteur corrompu (Jezaïl)|changé|
|[SuFVqL1LRxsGWB1B.htm](outlaws-of-alkenstar-bestiary/SuFVqL1LRxsGWB1B.htm)|Shock Zombie|Zombie électrique|changé|
|[TDLiOXhnBxC0n3jr.htm](outlaws-of-alkenstar-bestiary/TDLiOXhnBxC0n3jr.htm)|Gilded Gunner Safecracker|Perceur de coffre du Canon doré|changé|
|[TLF3QsSKubSBsSOG.htm](outlaws-of-alkenstar-bestiary/TLF3QsSKubSBsSOG.htm)|Chemical Zombie|Zombie chimique|changé|
|[UdgI6p5FlE7Xcrl7.htm](outlaws-of-alkenstar-bestiary/UdgI6p5FlE7Xcrl7.htm)|Vewslog|Vewslog|changé|
|[UgaIxXmbPe7qcrNk.htm](outlaws-of-alkenstar-bestiary/UgaIxXmbPe7qcrNk.htm)|Smog Wraith|Âme-en-peine du Smog|changé|
|[WtpeHbOQdK5CiaLf.htm](outlaws-of-alkenstar-bestiary/WtpeHbOQdK5CiaLf.htm)|Drela|Drela|changé|
|[zNlQRFJcijptkQQ4.htm](outlaws-of-alkenstar-bestiary/zNlQRFJcijptkQQ4.htm)|Leadsmith|Forge-Indice|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0hy54Fyobwdb7AGN.htm](outlaws-of-alkenstar-bestiary/0hy54Fyobwdb7AGN.htm)|Follower of Shumfallow|Partisan de Shumfallow|libre|
|[2IjwfgmGutHhpg7B.htm](outlaws-of-alkenstar-bestiary/2IjwfgmGutHhpg7B.htm)|Gilded Gunner Assassin|Assassin du Canon doré|libre|
|[2o6t3FioEbPT3M6U.htm](outlaws-of-alkenstar-bestiary/2o6t3FioEbPT3M6U.htm)|Mutant Bat|Chauve-souris mutante|libre|
|[34HKPYSau0ugfMTv.htm](outlaws-of-alkenstar-bestiary/34HKPYSau0ugfMTv.htm)|Prairie Drake|Drake des prairies|libre|
|[5GgDdZKPHB0uJYEu.htm](outlaws-of-alkenstar-bestiary/5GgDdZKPHB0uJYEu.htm)|Broken Centurion|Centurion spirale brisé|libre|
|[64BpT3gs4q1n187w.htm](outlaws-of-alkenstar-bestiary/64BpT3gs4q1n187w.htm)|Wooden Bullets Trap|Piège de balles en bois|libre|
|[661SdyR6HhzsmYJN.htm](outlaws-of-alkenstar-bestiary/661SdyR6HhzsmYJN.htm)|Clockwork Door Warden|Concierge mécanique|libre|
|[8I7Hu4LXhVcLky8Z.htm](outlaws-of-alkenstar-bestiary/8I7Hu4LXhVcLky8Z.htm)|False Door Trap|Fausse porte piégée|libre|
|[8jYqoyei7X2Bikb0.htm](outlaws-of-alkenstar-bestiary/8jYqoyei7X2Bikb0.htm)|Brighite Herexen|Hérexen de Brigh|libre|
|[8zInanrJXHN6zmdq.htm](outlaws-of-alkenstar-bestiary/8zInanrJXHN6zmdq.htm)|Sludgespine Killer|Tueur Bourbépuration|libre|
|[9Fmj4LRJ40803rEK.htm](outlaws-of-alkenstar-bestiary/9Fmj4LRJ40803rEK.htm)|Bristlebane|Bristlebane|libre|
|[A1o0TVBwnrdFyKBz.htm](outlaws-of-alkenstar-bestiary/A1o0TVBwnrdFyKBz.htm)|Slick|Slick|libre|
|[AAkT7x3eYV1hn1I6.htm](outlaws-of-alkenstar-bestiary/AAkT7x3eYV1hn1I6.htm)|Smog Giant|Géant du smog|libre|
|[aJCzNXYDKsTGyaaN.htm](outlaws-of-alkenstar-bestiary/aJCzNXYDKsTGyaaN.htm)|Gilded Gunner Goon|Homme de main du Canon doré|libre|
|[BO72SUjG83p8p9UU.htm](outlaws-of-alkenstar-bestiary/BO72SUjG83p8p9UU.htm)|Mutant Giant Toad|Crapaud géant mutant|libre|
|[ceKqkc0c86gh6cI1.htm](outlaws-of-alkenstar-bestiary/ceKqkc0c86gh6cI1.htm)|Mutant Desert Drake|Drake mutant du désert|libre|
|[ChBr4WYIeCnRUbHL.htm](outlaws-of-alkenstar-bestiary/ChBr4WYIeCnRUbHL.htm)|Ibrium|Ibrium|libre|
|[CMTeUYpFwhN0CKs2.htm](outlaws-of-alkenstar-bestiary/CMTeUYpFwhN0CKs2.htm)|Precarious Thunderstone Trap|Piège de pierre à tonnerre instable|libre|
|[EjQYuMP3t0J4OI3I.htm](outlaws-of-alkenstar-bestiary/EjQYuMP3t0J4OI3I.htm)|Gunmarshal|Marshal tireur|libre|
|[ENCh6fX1xGwwrjCf.htm](outlaws-of-alkenstar-bestiary/ENCh6fX1xGwwrjCf.htm)|Trapjaw Tangle|Enchevêtrement de pièges-mâchoires|libre|
|[FMnoyAlVppHvcu8I.htm](outlaws-of-alkenstar-bestiary/FMnoyAlVppHvcu8I.htm)|Ioton (F6)|Ioton (F6)|libre|
|[fsO6qulg049kdo12.htm](outlaws-of-alkenstar-bestiary/fsO6qulg049kdo12.htm)|Gremlin Horde|Horde de gremlins|libre|
|[GcgnvVY0TYSFloLo.htm](outlaws-of-alkenstar-bestiary/GcgnvVY0TYSFloLo.htm)|Iron Dart Launcher|Lanceur de fléchettes en acier|libre|
|[GyrL5Q66shXthhZL.htm](outlaws-of-alkenstar-bestiary/GyrL5Q66shXthhZL.htm)|Irkem Dresh|Irkem Dresh|libre|
|[jaJxIGaNxmcLZ0jK.htm](outlaws-of-alkenstar-bestiary/jaJxIGaNxmcLZ0jK.htm)|Compromised Door Warden|Gardien de porte compromis|libre|
|[jONy6jEyCGSUxj0k.htm](outlaws-of-alkenstar-bestiary/jONy6jEyCGSUxj0k.htm)|Explosive Steam Trap|Piège à vapeur explosif|libre|
|[JuN7m62ohqdb4mL5.htm](outlaws-of-alkenstar-bestiary/JuN7m62ohqdb4mL5.htm)|Palzu|Palzu|libre|
|[kaG7k0ZPTsUubAXw.htm](outlaws-of-alkenstar-bestiary/kaG7k0ZPTsUubAXw.htm)|Purple Dye Trap|Piège du colorant violet|libre|
|[Lck0FIlp4kLFeLfg.htm](outlaws-of-alkenstar-bestiary/Lck0FIlp4kLFeLfg.htm)|Clockwork Brewer|Brasseur mécanique|libre|
|[NR74QIX0qE7AeZmg.htm](outlaws-of-alkenstar-bestiary/NR74QIX0qE7AeZmg.htm)|Clearwater Cleaner|Nettoyeur Eauclaire|libre|
|[o2nLVCZVOacqkWus.htm](outlaws-of-alkenstar-bestiary/o2nLVCZVOacqkWus.htm)|Clockwork Gunner|Artilleur mécanique|libre|
|[o9H7LrIqjOVyZTDH.htm](outlaws-of-alkenstar-bestiary/o9H7LrIqjOVyZTDH.htm)|Repeater Crossbow Trap|Piège de l'arbalète à répétition|libre|
|[omcS9Zvq7ZDd35A2.htm](outlaws-of-alkenstar-bestiary/omcS9Zvq7ZDd35A2.htm)|Glass Buccaneer|Boucanier de verre|libre|
|[oriWalYkx3CUSPdN.htm](outlaws-of-alkenstar-bestiary/oriWalYkx3CUSPdN.htm)|Clockwork Buccaneer|Boucanier mécanique|libre|
|[osS74hnE5VCSiJLs.htm](outlaws-of-alkenstar-bestiary/osS74hnE5VCSiJLs.htm)|Second Kiss Engine|Dispositif du "Second baiser"|libre|
|[Qbt1X2GTcPvmptgc.htm](outlaws-of-alkenstar-bestiary/Qbt1X2GTcPvmptgc.htm)|Lyzerium Bottles|Bouteilles de lyzérium|libre|
|[QQ2Ci8E2lkxG8QIV.htm](outlaws-of-alkenstar-bestiary/QQ2Ci8E2lkxG8QIV.htm)|Subduing Gas Chamber|Chambre du gaz somnifère|libre|
|[R9DlBcLJ5YbrXSuQ.htm](outlaws-of-alkenstar-bestiary/R9DlBcLJ5YbrXSuQ.htm)|Roxy|Roxy|libre|
|[RgGijwS8Z9fNFFO3.htm](outlaws-of-alkenstar-bestiary/RgGijwS8Z9fNFFO3.htm)|Glass Sentry|Sentinelle de verre|libre|
|[RNvLscZ5sMpYzExB.htm](outlaws-of-alkenstar-bestiary/RNvLscZ5sMpYzExB.htm)|Clockwork Hunter|Chasseur mécanique|libre|
|[RqV8n5CxdDQ6ZahH.htm](outlaws-of-alkenstar-bestiary/RqV8n5CxdDQ6ZahH.htm)|Clockwork Disposer|Ébroueur mécanique|libre|
|[sCqnGbntH125iWU4.htm](outlaws-of-alkenstar-bestiary/sCqnGbntH125iWU4.htm)|Volatile Reagents|Réactifs volatils|libre|
|[sUleYLWZG3O9LlKC.htm](outlaws-of-alkenstar-bestiary/sUleYLWZG3O9LlKC.htm)|Yeast Ooze|Vase de levure|libre|
|[t60zBqM2apOZtOaC.htm](outlaws-of-alkenstar-bestiary/t60zBqM2apOZtOaC.htm)|Glass Elephant|Éléphant en verre|libre|
|[T9vLIrNIbcNkYx1M.htm](outlaws-of-alkenstar-bestiary/T9vLIrNIbcNkYx1M.htm)|Masu|Masu|libre|
|[TfyWBoNpR1iNBcLV.htm](outlaws-of-alkenstar-bestiary/TfyWBoNpR1iNBcLV.htm)|Sabora Sharkosa|Sabora Sharkosa|libre|
|[TWicuDambQ2g5Zn3.htm](outlaws-of-alkenstar-bestiary/TWicuDambQ2g5Zn3.htm)|Falling Portcullis Trap|Piège de la herse tombante|libre|
|[V3la31QNaidyHUnU.htm](outlaws-of-alkenstar-bestiary/V3la31QNaidyHUnU.htm)|Clockwork Shambler|Titubeur zombie mécanique|libre|
|[VfFt5YAVe7d9fonE.htm](outlaws-of-alkenstar-bestiary/VfFt5YAVe7d9fonE.htm)|Swee Pup|Swee Pup|libre|
|[vnZFO1jNZSJieqpo.htm](outlaws-of-alkenstar-bestiary/vnZFO1jNZSJieqpo.htm)|Gold Tank Investor|Investisseur de la Choppe d'or|libre|
|[wHbf1jbqcHFY6rnj.htm](outlaws-of-alkenstar-bestiary/wHbf1jbqcHFY6rnj.htm)|Chimeric Manticore|Manticore chimérique|libre|
|[Wr08A69Iu5pXNWXj.htm](outlaws-of-alkenstar-bestiary/Wr08A69Iu5pXNWXj.htm)|Ambrost Mugland|Ambrost Mugland|libre|
|[WwDdD9FI3ololycu.htm](outlaws-of-alkenstar-bestiary/WwDdD9FI3ololycu.htm)|Clockwork Shambler Horde|Horde de titubeurs mécaniques|libre|
|[X4tOSkr3WLvh0NIN.htm](outlaws-of-alkenstar-bestiary/X4tOSkr3WLvh0NIN.htm)|Rust Ooze|Vase de rouille|libre|
|[YsSpSUB1jeLSifFg.htm](outlaws-of-alkenstar-bestiary/YsSpSUB1jeLSifFg.htm)|I|I|libre|
|[Zf9C8InVZGnDH8c2.htm](outlaws-of-alkenstar-bestiary/Zf9C8InVZGnDH8c2.htm)|Lonely Machine Spirit|Esprit solitaire de la machine|libre|
|[zLy98F3VPHwJPgih.htm](outlaws-of-alkenstar-bestiary/zLy98F3VPHwJPgih.htm)|Akrida|Akrida|libre|
|[zUHMrJZKcKIv6IpM.htm](outlaws-of-alkenstar-bestiary/zUHMrJZKcKIv6IpM.htm)|Daelum|Daélum|libre|
