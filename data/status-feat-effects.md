# État de la traduction (feat-effects)

 * **libre**: 323
 * **changé**: 95
 * **aucune**: 16


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0JvxhEv83bAD5PGv.htm](feat-effects/0JvxhEv83bAD5PGv.htm)|Effect: Familiar of Balanced Luck|
|[3xsComVKLuLn7etk.htm](feat-effects/3xsComVKLuLn7etk.htm)|Effect: Deity's Protection|
|[4wJM0OA9y2gsBAh7.htm](feat-effects/4wJM0OA9y2gsBAh7.htm)|Effect: Familiar of Keen Senses|
|[gDlOd7z3EUkjW7DU.htm](feat-effects/gDlOd7z3EUkjW7DU.htm)|Effect: Worm Form|
|[KanP6trNwPOggb7P.htm](feat-effects/KanP6trNwPOggb7P.htm)|Effect: Spirit Guide Form|
|[lFIUuFrNey4kD4Md.htm](feat-effects/lFIUuFrNey4kD4Md.htm)|Effect: Wish for Luck|
|[lPtt7PojWBwPOaYt.htm](feat-effects/lPtt7PojWBwPOaYt.htm)|Effect: Bespell Strikes|
|[nMMqJQsdV37TLfTu.htm](feat-effects/nMMqJQsdV37TLfTu.htm)|Effect: Monarch Wings|
|[OochfTTXnDLVXeSS.htm](feat-effects/OochfTTXnDLVXeSS.htm)|Effect: Spell Protection Array|
|[SZY4zxhxoeehYRdN.htm](feat-effects/SZY4zxhxoeehYRdN.htm)|Effect: Cyclonic Ascent|
|[twvhTLrLEfr7dz1m.htm](feat-effects/twvhTLrLEfr7dz1m.htm)|Effect: Familiar of Restored Spirit|
|[uG1EljvrnC9HGwUh.htm](feat-effects/uG1EljvrnC9HGwUh.htm)|Effect: Ocean's Balm|
|[Ve1CRFI8ikL6dqcL.htm](feat-effects/Ve1CRFI8ikL6dqcL.htm)|Effect: Cast Down|
|[WEP9Oe1D9WsWSOU8.htm](feat-effects/WEP9Oe1D9WsWSOU8.htm)|Effect: Energy Ablation|
|[xdcblhm5Ie3CG9wS.htm](feat-effects/xdcblhm5Ie3CG9wS.htm)|Effect: Glimpses to Beyond|
|[ycalVFivloJ14yhA.htm](feat-effects/ycalVFivloJ14yhA.htm)|Effect: Resounding Finale|
|[ZZXIUvZqqIxkMfYa.htm](feat-effects/ZZXIUvZqqIxkMfYa.htm)|Effect: Precise Debilitations|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0r2V1nK5pV31IUPY.htm](feat-effects/0r2V1nK5pV31IUPY.htm)|Effect: Protective Mentor Boon (Revered) (PFS)|Effet : Récompense du mentor protecteur (Révéré) (PFS)|changé|
|[2gVP04ZWYbQdX3uS.htm](feat-effects/2gVP04ZWYbQdX3uS.htm)|Effect: Spiral Sworn|Effet : Spirale assermentée|changé|
|[2GWZgsvMJF9DN0DO.htm](feat-effects/2GWZgsvMJF9DN0DO.htm)|Effect: Fresh Produce|Effet : Produit frais|changé|
|[2MIn8qyPTmz4ZyO1.htm](feat-effects/2MIn8qyPTmz4ZyO1.htm)|Effect: Smite Good|Effet : Châtiment du bien|changé|
|[5PIaLkys5ZqP2BUv.htm](feat-effects/5PIaLkys5ZqP2BUv.htm)|Effect: Primal Aegis|Effet : Égide primordiale|changé|
|[6fb15XuSV4TNuVAT.htm](feat-effects/6fb15XuSV4TNuVAT.htm)|Effect: Hag Blood Magic|Effet : Magie du sang guenaude|changé|
|[6IsZQpwRJQWIzdGx.htm](feat-effects/6IsZQpwRJQWIzdGx.htm)|Stance: Masquerade of Seasons Stance|Posture : Posture de la mascarade des saisons|changé|
|[939OHjW9y8uCmDk3.htm](feat-effects/939OHjW9y8uCmDk3.htm)|Effect: Unleash Psyche|Effet : Déchaîner la psyché|changé|
|[AclYG5JuBFrjCY3I.htm](feat-effects/AclYG5JuBFrjCY3I.htm)|Effect: Divine Weapon (Evil)|Effet : Arme Divine (Mauvais)|changé|
|[AlnxieIRjqNqsdVu.htm](feat-effects/AlnxieIRjqNqsdVu.htm)|Effect: Smite Evil|Effet : Châtiment du mal|changé|
|[BC92TyFzRCWq8fu0.htm](feat-effects/BC92TyFzRCWq8fu0.htm)|Effect: Great Tengu Form|Effet : Forme du grand tengu|changé|
|[bIU1q05vzkKBtFj2.htm](feat-effects/bIU1q05vzkKBtFj2.htm)|Effect: Divine Infusion (Harm)|Effet : Impregnation divine (Mise à mal)|changé|
|[bl4HXm1e4NQ0iJs5.htm](feat-effects/bl4HXm1e4NQ0iJs5.htm)|Effect: Align Armament (Good)|Effet : Arsenal aligné (Bon)|changé|
|[cA6ps8RKE0gysEWr.htm](feat-effects/cA6ps8RKE0gysEWr.htm)|Effect: Prayer-Touched Weapon|Effet : Arme touchée par la grâce|changé|
|[cH8JD3ub4eEKuIAD.htm](feat-effects/cH8JD3ub4eEKuIAD.htm)|Effect: Divine Infusion (Heal)|Effet : Imprégnation divine (Guérison)|changé|
|[CtrZFI3RV0yPNzTv.htm](feat-effects/CtrZFI3RV0yPNzTv.htm)|Effect: Bon Mot (Critical Success)|Effet : Bon Mot (Succès critique)|changé|
|[CUtvkuGSxq1raBIB.htm](feat-effects/CUtvkuGSxq1raBIB.htm)|Effect: Shared Clarity|Effet : Clarté partagée|changé|
|[dvOfGUuvG8ihcN8d.htm](feat-effects/dvOfGUuvG8ihcN8d.htm)|Effect: Divine Weapon (Good)|Effet : Arme Divine (Bon)|changé|
|[e6mv68aarIbQ3tXL.htm](feat-effects/e6mv68aarIbQ3tXL.htm)|Effect: Undying Conviction|Effet : Conviction immortelle|changé|
|[eA14bUF7xhNCzw2v.htm](feat-effects/eA14bUF7xhNCzw2v.htm)|Effect: Align Armament (Evil)|Effet : Arsenal aligné (Mal)|changé|
|[eeAlh6edygcZIz9c.htm](feat-effects/eeAlh6edygcZIz9c.htm)|Stance: Wild Winds Stance|Posture : Posture des vents violents|changé|
|[emSh1VxHVtTmt925.htm](feat-effects/emSh1VxHVtTmt925.htm)|Effect: Methodical Debilitations (Flanking)|Effet : Handicaps méthodiques|changé|
|[EQCnu8DGHDDNXch0.htm](feat-effects/EQCnu8DGHDDNXch0.htm)|Effect: Reanimator Dedication|Effet : Dévouement : Réanimateur|changé|
|[EtFMN1ZLkL7sUk01.htm](feat-effects/EtFMN1ZLkL7sUk01.htm)|Effect: Curse of Outpouring Life|Effet : Malédiction de la vie déversée|changé|
|[EzgW32MCOGov9h5C.htm](feat-effects/EzgW32MCOGov9h5C.htm)|Effect: Striking Retribution|Effet : Rétribution frappante|changé|
|[fh8TgCfiifVk0eqU.htm](feat-effects/fh8TgCfiifVk0eqU.htm)|Effect: Magical Mentor Boon (PFS)|Effet : Récompense de mentor magique (PFS)|changé|
|[fsjO5oTKttsbpaKl.htm](feat-effects/fsjO5oTKttsbpaKl.htm)|Stance: Arcane Cascade|Posture : Cascade arcanique|changé|
|[GCEOngH5zL0rRyle.htm](feat-effects/GCEOngH5zL0rRyle.htm)|Effect: Emblazon Energy (Weapon, Fire)|Effet : Énergie blasonnée (Arme, Feu)|changé|
|[GlpZyxAGhy5QNqkm.htm](feat-effects/GlpZyxAGhy5QNqkm.htm)|Effect: Divine Weapon (Lawful)|Effet : Arme Divine (Loyal)|changé|
|[GoSls6SKCFmSoDxT.htm](feat-effects/GoSls6SKCFmSoDxT.htm)|Effect: Bon Mot|Effet : Bon Mot (Succès)|changé|
|[h45sUZFs5jhuQdCE.htm](feat-effects/h45sUZFs5jhuQdCE.htm)|Stance: Vitality-Manipulation Stance|Posture : Posture de manipulation de la vitalité|changé|
|[I4Ozf6mTnd3X0Oax.htm](feat-effects/I4Ozf6mTnd3X0Oax.htm)|Effect: Predictable! (Critical Success)|Effet : Présivisible ! (Succès critique)|changé|
|[iqvurepX0zyu9OlI.htm](feat-effects/iqvurepX0zyu9OlI.htm)|Effect: Masterful Hunter's Edge, Outwit|Effet : Spécialité Maître chasseur, Ruse|changé|
|[iyONT1qgeRgoYHsZ.htm](feat-effects/iyONT1qgeRgoYHsZ.htm)|Effect: Liberating Step (vs. Dragon)|Effet : Pas libérateur (Dragon)|changé|
|[jACKRmVfr9ATsmwg.htm](feat-effects/jACKRmVfr9ATsmwg.htm)|Effect: Devrin's Cunning Stance|Effet : Posture astucieuse de Devrin|changé|
|[K1IgNCf3Hh2EJwQ9.htm](feat-effects/K1IgNCf3Hh2EJwQ9.htm)|Effect: Divine Aegis|Effet : Égide divine|changé|
|[KceTcamIZ4ZrQJLD.htm](feat-effects/KceTcamIZ4ZrQJLD.htm)|Effect: Educate Allies (Self)|Effet : Alliés instruits (Vous-même)|changé|
|[KgR1myc4OLzVxfxn.htm](feat-effects/KgR1myc4OLzVxfxn.htm)|Effect: Predictable! (Critical Failure)|Effet : Présivisible ! (Échec critique)|changé|
|[KiuBRoMFxL2Npt51.htm](feat-effects/KiuBRoMFxL2Npt51.htm)|Stance: Dueling Dance|Posture : Danse en duel|changé|
|[KkbFlNfcQQUfSVXd.htm](feat-effects/KkbFlNfcQQUfSVXd.htm)|Effect: Align Armament (Lawful)|Effet : Arsenal aligné (Loyal)|changé|
|[L0hDj8vFk1IWh01L.htm](feat-effects/L0hDj8vFk1IWh01L.htm)|Effect: Aura of Righteousness|Effet : Aura de vertu|changé|
|[LB0PTV5yqMlBmRFj.htm](feat-effects/LB0PTV5yqMlBmRFj.htm)|Effect: Legendary Monster Hunter|Effet : Chasseur de monstres légendaire|changé|
|[lbe8XDSZB8gwyg90.htm](feat-effects/lbe8XDSZB8gwyg90.htm)|Effect: Protective Mentor Boon (Admired) (PFS)|Effet : Récompense du mentor protecteur (Admiré) (PFS)|changé|
|[LF8xzzFsFJKxejqv.htm](feat-effects/LF8xzzFsFJKxejqv.htm)|Effect: Enforce Oath|Effet : Serment renforcé|changé|
|[Lt5iSfx8fxHSdYXz.htm](feat-effects/Lt5iSfx8fxHSdYXz.htm)|Effect: Masterful Hunter's Edge, Precision|Effet : Spécialité Maître chasseur, Précision|changé|
|[ltIvO9ZQlmqGD89Y.htm](feat-effects/ltIvO9ZQlmqGD89Y.htm)|Effect: Hunter's Edge, Outwit|Effet : Spécialité du chasseur, Ruse|changé|
|[LVPodfYEWKtK3fUW.htm](feat-effects/LVPodfYEWKtK3fUW.htm)|Effect: Formation Training|Effet : Combat en formation|changé|
|[m5xWMaDfV0PiTE6u.htm](feat-effects/m5xWMaDfV0PiTE6u.htm)|Effect: Ursine Avenger Form|Effet : Forme de vengeur ursin|changé|
|[mkIamZGtQaSsUWLk.htm](feat-effects/mkIamZGtQaSsUWLk.htm)|Effect: Control Tower|Effet : Tour de contrôle|changé|
|[mmaJYXBUxVncsOsx.htm](feat-effects/mmaJYXBUxVncsOsx.htm)|Effect: Ghosts in the Storm|Effet : fantôme dans la tempète|changé|
|[mNk0KxsZMFnDjUA0.htm](feat-effects/mNk0KxsZMFnDjUA0.htm)|Effect: Hunter's Edge, Precision|Effet : Spécialité du chasseur, Précision|changé|
|[nlaxROgSSLVHZ1hx.htm](feat-effects/nlaxROgSSLVHZ1hx.htm)|Effect: Monster Warden|Effet : Garde-monstre|changé|
|[nlMZCi8xi9YSvlYR.htm](feat-effects/nlMZCi8xi9YSvlYR.htm)|Effect: Engine of Destruction|Effet : Machine de destruction|changé|
|[nwkYZs6YwXYAJ4ps.htm](feat-effects/nwkYZs6YwXYAJ4ps.htm)|Stance: Crane Stance|Posture : Posture de la grue|changé|
|[O8qithYQCv3e7DUQ.htm](feat-effects/O8qithYQCv3e7DUQ.htm)|Effect: Elementalist Dedication|Effet : Dévouement : Élémentaliste|changé|
|[OkcblqWj4aHVAkrp.htm](feat-effects/OkcblqWj4aHVAkrp.htm)|Effect: Divine Weapon (Chaotic)|Effet : Arme Divine (Chaotique)|changé|
|[oKJr59FYdDORxLcR.htm](feat-effects/oKJr59FYdDORxLcR.htm)|Effect: Worldly Mentor Boon (PFS)|Effet : Récompense de mentor expérimenté (PFS)|changé|
|[OKOqC1wswrh9jXqP.htm](feat-effects/OKOqC1wswrh9jXqP.htm)|Effect: Protective Mentor Boon (Liked) (PFS)|Effet : Récompense de mentor protecteur (Aimé) (PFS)|changé|
|[oX51Db6IxnUI64dT.htm](feat-effects/oX51Db6IxnUI64dT.htm)|Effect: Emblazon Energy (Weapon, Electricity)|Effet : Énergie blasonnée (Arme, Électricité)|changé|
|[P80mwvCAEncR2snK.htm](feat-effects/P80mwvCAEncR2snK.htm)|Stance: Six Pillars Stance|Posture : Posture des six piliers|changé|
|[q2kY0TzXloJ8HLNO.htm](feat-effects/q2kY0TzXloJ8HLNO.htm)|Effect: Combat Mentor Boon (PFS)|Effet : Récompense de mentor de combat (PFS)|changé|
|[Q5FUu7yhWPJlcXei.htm](feat-effects/Q5FUu7yhWPJlcXei.htm)|Effect: Hydration|Effet : Hydratation|changé|
|[raoz523QRsj5WjcF.htm](feat-effects/raoz523QRsj5WjcF.htm)|Effect: Harsh Judgement|Effet : Jugement sévère|changé|
|[RcxDIOa68SUGyMun.htm](feat-effects/RcxDIOa68SUGyMun.htm)|Effect: Titan's Stature|Effet : Stature de titan|changé|
|[RozqjLocahvQWERr.htm](feat-effects/RozqjLocahvQWERr.htm)|Stance: Gorilla Stance|Posture : Posture du gorille|changé|
|[rp1YauUSULuqW8rs.htm](feat-effects/rp1YauUSULuqW8rs.htm)|Stance: Stoked Flame Stance|Posture : Posture de la flamme ravivée|changé|
|[rUKtp4q8y73AvCbo.htm](feat-effects/rUKtp4q8y73AvCbo.htm)|Effect: Clue In (Detective's Readiness)|Effet : Partager les indices (Vivacité du détective)|changé|
|[RxDDXK52lwyHXl7v.htm](feat-effects/RxDDXK52lwyHXl7v.htm)|Effect: Scout's Warning|Effet : Avertissement de l'éclaireur|changé|
|[RyGaB5hDRcOeb34Q.htm](feat-effects/RyGaB5hDRcOeb34Q.htm)|Effect: Emblazon Antimagic (Weapon)|Effet : Antimagie blasonnée (Arme)|changé|
|[Sb3ZdFs61atILypS.htm](feat-effects/Sb3ZdFs61atILypS.htm)|Effect: Ghosts in the Storm (Move)|Effet : Fantôme dans la tempête - activation|changé|
|[sCxi8lOH8tWQjLh0.htm](feat-effects/sCxi8lOH8tWQjLh0.htm)|Effect: Blade Ally|Effet : Allié lame|changé|
|[SKjVvQcRQmnDoouw.htm](feat-effects/SKjVvQcRQmnDoouw.htm)|Effect: Skillful Mentor Boon (PFS)|Effet : Récompense de mentor talentueux (PFS)|changé|
|[sPIaly8bgNxgcNvT.htm](feat-effects/sPIaly8bgNxgcNvT.htm)|Stance: Ghosts in the Storm|Posture : Fantômes dans la tempête|changé|
|[svVczVV174KfJRDf.htm](feat-effects/svVczVV174KfJRDf.htm)|Effect: Shared Avoidance|Effet : Évitement partagé|changé|
|[tAsFXMzNkpj964X4.htm](feat-effects/tAsFXMzNkpj964X4.htm)|Effect: Liberating Step (vs. Aberration)|Effet : Pas libérateur (Aberration)|changé|
|[U1MpMtRnFqEDBJwd.htm](feat-effects/U1MpMtRnFqEDBJwd.htm)|Effect: Emblazon Armament (Weapon)|Effet : Arsenal blasonné (Arme)|changé|
|[U2Pgm6B4nmdQ2Gpd.htm](feat-effects/U2Pgm6B4nmdQ2Gpd.htm)|Effect: Divine Castigation|Effet : Punition divine|changé|
|[uFYvW3kFP9iyNfVX.htm](feat-effects/uFYvW3kFP9iyNfVX.htm)|Stance: Clinging Shadows Stance|Posture : Posture des ombres tenaces|changé|
|[UzIamWcEJTOjwfoA.htm](feat-effects/UzIamWcEJTOjwfoA.htm)|Effect: Spin Tale|Effet : Broder un conte|changé|
|[UZKIKLuwpQu47feK.htm](feat-effects/UZKIKLuwpQu47feK.htm)|Stance: Gorilla Stance (Gorilla Pound)|Posture : Posture du gorille (Martèlement du gorille)|changé|
|[v2HDcrxQF2Dncjbs.htm](feat-effects/v2HDcrxQF2Dncjbs.htm)|Effect: Flamboyant Cruelty|Effet : Cruauté flamboyante|changé|
|[V6lnFOq998B76Rr0.htm](feat-effects/V6lnFOq998B76Rr0.htm)|Effect: Curse of Ancestral Meddling|Effet : Malédiction de l'incursion ancestrale|changé|
|[vkgmMBMd2e2BwiwJ.htm](feat-effects/vkgmMBMd2e2BwiwJ.htm)|Effect: Ancestral Influence|Effet : Influence ancestrale|changé|
|[VOOShYoB4gTopZtg.htm](feat-effects/VOOShYoB4gTopZtg.htm)|Effect: Aura of Faith|Effet : Aura de foi|changé|
|[W2tWq0gdAcnoz2MO.htm](feat-effects/W2tWq0gdAcnoz2MO.htm)|Effect: Monster Hunter|Effet : Chasseur de monstres|changé|
|[WrWSieH9Acy6XuzV.htm](feat-effects/WrWSieH9Acy6XuzV.htm)|Effect: Educate Allies (Ally)|Effet : Alliés instruits (Allié)|changé|
|[x4Sb3qaMJo8x1r3X.htm](feat-effects/x4Sb3qaMJo8x1r3X.htm)|Effect: Emblazon Energy (Weapon, Acid)|Effet : Énergie blasonnée (Arme, Acide)|changé|
|[XC3dRbwfu35vuvmM.htm](feat-effects/XC3dRbwfu35vuvmM.htm)|Effect: Align Armament (Chaotic)|Effet : Arsenal aligné (Chaotique)|changé|
|[xDT10fUWp8UStSZR.htm](feat-effects/xDT10fUWp8UStSZR.htm)|Effect: Cavalier's Banner|Effet : Bannière du Cavalier|changé|
|[z4pnE8KyUdEkJmru.htm](feat-effects/z4pnE8KyUdEkJmru.htm)|Effect: Clue In (Detective's Readiness, Expertise)|Effet : Partager les indices (Vivacité du détective et expertise de l'enquêteur)|changé|
|[ZMFgz4GYSsFeaKKK.htm](feat-effects/ZMFgz4GYSsFeaKKK.htm)|Effect: Rugged Mentor Boon (PFS)|Effet : Récompense de mentor robuste (PFS)|changé|
|[ZnKnOPPq3cG54PlG.htm](feat-effects/ZnKnOPPq3cG54PlG.htm)|Effect: Liberating Step (vs. Undead)|Effet : Pas libérateur (morts-vivants)|changé|
|[zZ25N1zpXA8GNhFL.htm](feat-effects/zZ25N1zpXA8GNhFL.htm)|Effect: Divine Weapon|Effet : Arme Divine (Force)|changé|
|[zzC2qZwEKf4Ja3xD.htm](feat-effects/zzC2qZwEKf4Ja3xD.htm)|Stance: Impassable Wall Stance|Posture : Posture du mur infranchissable|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09oP0FBBAhXOS4JW.htm](feat-effects/09oP0FBBAhXOS4JW.htm)|Effect: Earth Impulse Junction|Effet : Jonction d'impulsion de la terre|libre|
|[0AD7BiKjT8a6Uh92.htm](feat-effects/0AD7BiKjT8a6Uh92.htm)|Effect: Energetic Meltdown|Effet : Effondrement énergétique|libre|
|[0Ai0u9kNJrEudPnN.htm](feat-effects/0Ai0u9kNJrEudPnN.htm)|Effect: Forcible Energy|Effet : Énergie vigoureuse|libre|
|[0JrHvdUgJBl631En.htm](feat-effects/0JrHvdUgJBl631En.htm)|Effect: Juvenile Flight|Effet : Vol juvénile|libre|
|[0pq3MPLH0C9z4tj3.htm](feat-effects/0pq3MPLH0C9z4tj3.htm)|Effect: Victorious Vigor|Effet : Vigueur dans la victoire|libre|
|[18FHJoazfEmgNkfk.htm](feat-effects/18FHJoazfEmgNkfk.htm)|Effect: Aura of Preservation|Effet : Aura de préservation|libre|
|[1Azz9TSWXrX4aNX4.htm](feat-effects/1Azz9TSWXrX4aNX4.htm)|Effect: Harrow-Chosen|Effet : Choisi par le Tourment|libre|
|[1dxD3xsTzak6GNj5.htm](feat-effects/1dxD3xsTzak6GNj5.htm)|Stance: Monastic Archer Stance|Posture : Posture de l'archer monastique|libre|
|[1nCwQErK6hpkNvfw.htm](feat-effects/1nCwQErK6hpkNvfw.htm)|Effect: Dueling Parry|Effet : Parade en duel|libre|
|[1WqXbwhfT1f6OrPU.htm](feat-effects/1WqXbwhfT1f6OrPU.htm)|Effect: Thermal Nimbus (Cold)|Effet : Nimbe thermique - Froid|libre|
|[1XlJ9xLzL19GHoOL.htm](feat-effects/1XlJ9xLzL19GHoOL.htm)|Effect: Overdrive|Effet : Surrégime|libre|
|[263Cd5JMj8Lgc9yz.htm](feat-effects/263Cd5JMj8Lgc9yz.htm)|Effect: Radiant Circuitry|Effet : Circuiterie rayonnante|libre|
|[2c30Drdg84bWLcRn.htm](feat-effects/2c30Drdg84bWLcRn.htm)|Effect: Emblazon Energy (Weapon, Sonic)|Effet : Énergie blasonnée (Arme, Son)|libre|
|[2ca1ZuqQ7VkunAh3.htm](feat-effects/2ca1ZuqQ7VkunAh3.htm)|Effect: Accept Echo|Effet : Accepter l'écho|libre|
|[2EMak2C8x6pFwoUi.htm](feat-effects/2EMak2C8x6pFwoUi.htm)|Stance: Thermal Nimbus|Posture : Nimbe thermique|libre|
|[2Qpt0CHuOMeL48rN.htm](feat-effects/2Qpt0CHuOMeL48rN.htm)|Stance: Cobra Stance (Cobra Envenom)|Posture : Posture du cobra (Cobra envenimé)|libre|
|[2RwhJ9fbJtcQjW6s.htm](feat-effects/2RwhJ9fbJtcQjW6s.htm)|Effect: Arctic Endemic Herb|Effet : Herbes endémiques Arctique|libre|
|[2XEYQNZTCGpdkyR6.htm](feat-effects/2XEYQNZTCGpdkyR6.htm)|Effect: Battle Medicine Immunity|Effet : Immunité à Médecine militaire|libre|
|[3eHMqVx30JGiJqtM.htm](feat-effects/3eHMqVx30JGiJqtM.htm)|Stance: Twinned Defense|Posture : Défense jumelée|libre|
|[3gGBZHcUFsHLJeQH.htm](feat-effects/3gGBZHcUFsHLJeQH.htm)|Effect: Elemental Blood Magic (Self)|Effet : Magie du sang élémentaire (Soi)|libre|
|[3GPh6O3PJxORytAC.htm](feat-effects/3GPh6O3PJxORytAC.htm)|Effect: Shadow Sight|Effet : Vision d'ombre|libre|
|[3WzaQKb10AYLdTsQ.htm](feat-effects/3WzaQKb10AYLdTsQ.htm)|Effect: Corpse-Killer's Defiance|Effet : Défi du tueur de cadavre (niveau égal ou supérieur)|libre|
|[4alr9e8w9H0RCLwI.htm](feat-effects/4alr9e8w9H0RCLwI.htm)|Effect: Tiller's Drive|Effet : Conduite du Laboureur|libre|
|[4HNPQ6pnMDRe8jaN.htm](feat-effects/4HNPQ6pnMDRe8jaN.htm)|Effect: Methodical Debilitations (Cover)|Effet : Handicaps méthodiques (abri)|libre|
|[4QWayYR3JSL9bk2T.htm](feat-effects/4QWayYR3JSL9bk2T.htm)|Effect: Weapon Tampered With (Success)|Effet : Arme trafiquée (succès)|libre|
|[4UNQfMrwfWirdwoV.htm](feat-effects/4UNQfMrwfWirdwoV.htm)|Effect: Masterful Hunter's Edge, Flurry|Effet : Spécialité Maître chasseur, Déluge|libre|
|[4xtHFRGI05SNe9rA.htm](feat-effects/4xtHFRGI05SNe9rA.htm)|Effect: Hungry Goblin|Effet : Gobelin affamé|libre|
|[4Zj71naHbY6O9ggP.htm](feat-effects/4Zj71naHbY6O9ggP.htm)|Effect: Bristle|Effet : Hirsute|libre|
|[5bEnBqVOgdp4gROP.htm](feat-effects/5bEnBqVOgdp4gROP.htm)|Effect: Catfolk Dance|Effet : Danse homme-félin|libre|
|[5IGz4iheaiUWm5KR.htm](feat-effects/5IGz4iheaiUWm5KR.htm)|Effect: Eye of the Arclords|Effet : Oeil des Seigneurs de l'Arc|libre|
|[5TzKmEqFyLHBG2ua.htm](feat-effects/5TzKmEqFyLHBG2ua.htm)|Effect: Emblazon Energy (Weapon, Cold)|Effet : Énergie blasonnée (Arme, Froid)|libre|
|[5uMMLUvJOEmfMgeV.htm](feat-effects/5uMMLUvJOEmfMgeV.htm)|Effect: Shield of Faith|Effet : Bouclier de la foi|libre|
|[5v0ndPPMfZwhiVZF.htm](feat-effects/5v0ndPPMfZwhiVZF.htm)|Effect: Predictable!|Effet : Présivisible ! (Succès)|libre|
|[5veOBmMYQxywTudd.htm](feat-effects/5veOBmMYQxywTudd.htm)|Effect: Goblin Song (Success)|Effet : Chant gobelin - Succès|libre|
|[6ACbQIpmmemxmoBJ.htm](feat-effects/6ACbQIpmmemxmoBJ.htm)|Effect: Saoc Astrology|Effet : Astrologie saoc|libre|
|[6ctQFQfSZ6o1uyyZ.htm](feat-effects/6ctQFQfSZ6o1uyyZ.htm)|Stance: Bullet Dancer Stance|Posture : Posture du danseur de balle|libre|
|[6EDoy3OSFZ4L3Vs7.htm](feat-effects/6EDoy3OSFZ4L3Vs7.htm)|Stance: Paragon's Guard|Posture : Protection du parangon|libre|
|[6fObd480rDBkFwZ3.htm](feat-effects/6fObd480rDBkFwZ3.htm)|Effect: Curse of Living Death|Effet : Malédiction de la mort vivante|libre|
|[6hh788S8hznyD66m.htm](feat-effects/6hh788S8hznyD66m.htm)|Effect: Shattershields|Effet : Plaquesboucliers|libre|
|[6VrKQ0PhRXuteusQ.htm](feat-effects/6VrKQ0PhRXuteusQ.htm)|Effect: Giant's Stature|Effet : Stature de géant|libre|
|[6YhbQmOmbmy84W1C.htm](feat-effects/6YhbQmOmbmy84W1C.htm)|Effect: Crimson Shroud|Effet : Voile pourpre|libre|
|[72THfaqak0F4XnON.htm](feat-effects/72THfaqak0F4XnON.htm)|Effect: Didactic Strike|Effet : Frappe didactique|libre|
|[7BFd8A9HFrmg6vwL.htm](feat-effects/7BFd8A9HFrmg6vwL.htm)|Effect: Psychopomp Blood Magic (Self)|Effet : Magie du sang psychopompe (Soi)|libre|
|[7cG8kpQvh2oyBV8d.htm](feat-effects/7cG8kpQvh2oyBV8d.htm)|Effect: Stone Body|Effet : Corps de pierre|libre|
|[7hQnwwsixZmXzdIT.htm](feat-effects/7hQnwwsixZmXzdIT.htm)|Effect: Channel the Godmind|Effet : Canaliser l'esprit divin|libre|
|[7hRgBo0fRQBxMK7g.htm](feat-effects/7hRgBo0fRQBxMK7g.htm)|Effect: Distracting Feint|Effet : Feinte de diversion|libre|
|[7MQLLkQACZt8cspt.htm](feat-effects/7MQLLkQACZt8cspt.htm)|Effect: Purifying Breeze|Effet : Brise purifiante|libre|
|[7ogytOgDmh4h2g5d.htm](feat-effects/7ogytOgDmh4h2g5d.htm)|Effect: Heroic Recovery|Effet : Récupération héroïque|libre|
|[8E5SCmFndGAvgkTw.htm](feat-effects/8E5SCmFndGAvgkTw.htm)|Effect: Energize Wings|Effet : Énergiser les ailes|libre|
|[8hmw9L2ORKz6Z6Bc.htm](feat-effects/8hmw9L2ORKz6Z6Bc.htm)|Stance: Drifting Pollen|Posture : Pollen flottant|libre|
|[8rDbWcWmQL0N5FFG.htm](feat-effects/8rDbWcWmQL0N5FFG.htm)|Effect: Azarketi Purification|Effet : Purification azarketie|libre|
|[94MzLpLykQIWKcA1.htm](feat-effects/94MzLpLykQIWKcA1.htm)|Effect: Deteriorated|Effet : Détérioré|libre|
|[9AUcoY48H5LrVZiF.htm](feat-effects/9AUcoY48H5LrVZiF.htm)|Effect: Genie Blood Magic (Self)|Effet : Magie du sang génie (Soi)|libre|
|[9dCt0asv0kt7DR4q.htm](feat-effects/9dCt0asv0kt7DR4q.htm)|Effect: Liberating Step (vs. Fiend)|Effet : Pas libérateur (Fiélon)|libre|
|[9HPxAKpP3WJmICBx.htm](feat-effects/9HPxAKpP3WJmICBx.htm)|Stance: Point Blank Stance|Posture : Tir à bout portant|libre|
|[9kNbiZPOM2wy60ao.htm](feat-effects/9kNbiZPOM2wy60ao.htm)|Effect: Ceremony of Protection|Effet : Cérémonie de protection|libre|
|[A6i55HQ59lzsHXVQ.htm](feat-effects/A6i55HQ59lzsHXVQ.htm)|Effect: Raise Symbol|Effet : Lever le symbole|libre|
|[a7qiSYdlaIRPe57i.htm](feat-effects/a7qiSYdlaIRPe57i.htm)|Effect: Watchful Gaze|Effet : Regard vigilant|libre|
|[AAgoUuwMvHzqNhIN.htm](feat-effects/AAgoUuwMvHzqNhIN.htm)|Effect: Assisting Shot (Critical Hit)|Effet : Tir de soutien (coup critique)|libre|
|[aEuDaQY1GnrrnDRA.htm](feat-effects/aEuDaQY1GnrrnDRA.htm)|Effect: Aldori Parry|Effet : Parade aldorie|libre|
|[AggnVD5loQHLb7zj.htm](feat-effects/AggnVD5loQHLb7zj.htm)|Effect: Raise Menhir|Effet : Ériger un menhir|libre|
|[AJlunjfAIOq2Sg0p.htm](feat-effects/AJlunjfAIOq2Sg0p.htm)|Effect: Underground Endemic Herbs|Effet : Herbes endémiques Souterrains|libre|
|[AKKHagjg5bL1fMG5.htm](feat-effects/AKKHagjg5bL1fMG5.htm)|Effect: Overwatch Field|Effet : Champ d'observation|libre|
|[aKRo5TIhUtu0kyEr.htm](feat-effects/aKRo5TIhUtu0kyEr.htm)|Effect: Demonic Blood Magic (Self)|Effet : Magie du sang démoniaque (Soi)|libre|
|[aqnx6IDcB7ARLxS5.htm](feat-effects/aqnx6IDcB7ARLxS5.htm)|Effect: Wyrmblessed Blood Magic (Status Penalty - Target)|Effet : Magie du sang béni du ver (pénalité de statut - Cible)|libre|
|[aUpcWqaLBlmpnJgW.htm](feat-effects/aUpcWqaLBlmpnJgW.htm)|Effect: Legendary Monster Warden|Effet : Garde-monstres légendaire|libre|
|[aWOvmdaTK1jS3H72.htm](feat-effects/aWOvmdaTK1jS3H72.htm)|Effect: Lost in the Crowd (10 Creatures)|Effet : Se perdre dans la foule (10 créatures)|libre|
|[b2kWJuCPj1rDMdwz.htm](feat-effects/b2kWJuCPj1rDMdwz.htm)|Stance: Wolf Stance|Posture : Posture du loup|libre|
|[BBGg5gpMmuBSo7Mi.htm](feat-effects/BBGg5gpMmuBSo7Mi.htm)|Effect: Deep Freeze|Effet : Congélation profonde|libre|
|[BCyGDKcplkJiSAKJ.htm](feat-effects/BCyGDKcplkJiSAKJ.htm)|Stance: Stumbling Stance|Posture : Posture chancelante|libre|
|[BHnunYPROBG5lxv4.htm](feat-effects/BHnunYPROBG5lxv4.htm)|Effect: Heroes' Call|Effet : Appel des héros|libre|
|[BIHU3o499fsa1bwt.htm](feat-effects/BIHU3o499fsa1bwt.htm)|Effect: Psi Strikes|Effet : Frappes psy|libre|
|[bIRIS6mnynr72RDw.htm](feat-effects/bIRIS6mnynr72RDw.htm)|Effect: Goblin Song (Critical Success)|Effet : Chant gobelin - Succès critique|libre|
|[BJc494USeyM011p3.htm](feat-effects/BJc494USeyM011p3.htm)|Effect: Replenishment of War|Effet : Récupération martiale|libre|
|[bliWctLi7jlKUTUe.htm](feat-effects/bliWctLi7jlKUTUe.htm)|Effect: Forest Endemic Herbs|Effet : Herbes endémiques Forêt|libre|
|[bmVwaN0C4e9fE2Sz.htm](feat-effects/bmVwaN0C4e9fE2Sz.htm)|Effect: Bolera's Interrogation (failure)|Effet : Interrogatoire de Boléra (échec)|libre|
|[buCKRBDZi27shB3X.htm](feat-effects/buCKRBDZi27shB3X.htm)|Stance: Sea Glass Guardians|Posture : Gardiens de la mer de verre|libre|
|[bvk5rwYSoTtz8QGf.htm](feat-effects/bvk5rwYSoTtz8QGf.htm)|Effect: Accursed Clay Fist|Effet : Poing de glaise maudit|libre|
|[bWvOkRT3alzllsiG.htm](feat-effects/bWvOkRT3alzllsiG.htm)|Effect: Kindle Inner Flames|Effet : Flammes de la chandelle intérieure|libre|
|[C6H3gF5HTdsIKpOC.htm](feat-effects/C6H3gF5HTdsIKpOC.htm)|Effect: Improved Poison Weapon|Effet : Arme empoisonnée améliorée|libre|
|[cGwFYusGTsJR3x3P.htm](feat-effects/cGwFYusGTsJR3x3P.htm)|Effect: Under Control|Effet : Sous contrôle|libre|
|[CgxYa0lrLUjS2ZhI.htm](feat-effects/CgxYa0lrLUjS2ZhI.htm)|Stance: Cobra Stance|Posture : Posture du cobra|libre|
|[cmCLIMgtd4TLA23p.htm](feat-effects/cmCLIMgtd4TLA23p.htm)|Stance: Steam Knight|Posture : Chevalier vapeur|libre|
|[CNnIS8jWVj00nPwF.htm](feat-effects/CNnIS8jWVj00nPwF.htm)|Effect: Lost in the Crowd (100 Creatures)|Effet : Se perdre dans la foule (100 créatures)|libre|
|[COsdMolZraFRTdP8.htm](feat-effects/COsdMolZraFRTdP8.htm)|Effect: Prevailing Position|Effet : Position prédominante|libre|
|[CQfkyJkRHw4IHWhv.htm](feat-effects/CQfkyJkRHw4IHWhv.htm)|Stance: Sky and Heaven Stance|Posture : Posture du Ciel et du Paradis|libre|
|[cqgbTZCvqaSvtQdz.htm](feat-effects/cqgbTZCvqaSvtQdz.htm)|Effect: Encroaching Presence|Effet : Présence envahissante|libre|
|[ctiTtuRWFjAnWdYQ.htm](feat-effects/ctiTtuRWFjAnWdYQ.htm)|Effect: Corpse-Killer's Defiance (Lower Level)|Effet : Défi du tueur de cadavre (niveau inférieur)|libre|
|[Cumdy84uIkUHG9zF.htm](feat-effects/Cumdy84uIkUHG9zF.htm)|Effect: Resounding Bravery (vs. Fear)|Effet : Bravoure retentissante (contre la peur)|libre|
|[CW4zphOOpeaLJIWc.htm](feat-effects/CW4zphOOpeaLJIWc.htm)|Effect: Recall Under Pressure|Effet : Souvenir sous pression|libre|
|[DawVHfoPKbPJsz4k.htm](feat-effects/DawVHfoPKbPJsz4k.htm)|Effect: Champion's Resistance|Effet : Résistance du champion|libre|
|[Dbr5hInQXH904Ca7.htm](feat-effects/Dbr5hInQXH904Ca7.htm)|Effect: Psychic Rapport|Effet : Rapport psychique|libre|
|[DhvSMIFs6xifgQHX.htm](feat-effects/DhvSMIFs6xifgQHX.htm)|Effect: Current Spell (Water)|Effet : Courant de sort (Eau)|libre|
|[DjxZpQ4xJWWvYQqY.htm](feat-effects/DjxZpQ4xJWWvYQqY.htm)|Effect: Repair Module|Effet : Module de réparation|libre|
|[dTymNXgTtnjqgYP9.htm](feat-effects/dTymNXgTtnjqgYP9.htm)|Effect: Emotional Surge|Effet : Surcharge émotionnelle|libre|
|[DvyyA11a63FBwV7x.htm](feat-effects/DvyyA11a63FBwV7x.htm)|Effect: Known Weakness|Effet : Faiblesses connues|libre|
|[DWrsDJte9sez0Ppi.htm](feat-effects/DWrsDJte9sez0Ppi.htm)|Effect: Rampaging Form|Effet : Forme destructrice|libre|
|[DyX4E7KDkzRnDxzc.htm](feat-effects/DyX4E7KDkzRnDxzc.htm)|Effect: Perfect Resistance|Effet : Résistance parfaite|libre|
|[E8MiV00QEhH5n18L.htm](feat-effects/E8MiV00QEhH5n18L.htm)|Effect: Bespell Weapon|Effet : Frappe enchantée|libre|
|[ebCWQB5nfK19GpY5.htm](feat-effects/ebCWQB5nfK19GpY5.htm)|Stance: Geologic Attunement|Posture : Harmonisation géologique|libre|
|[ecWVnrjsjubZ4On6.htm](feat-effects/ecWVnrjsjubZ4On6.htm)|Effect: Stoney Deflection|Effet : Déviation rocheuse|libre|
|[ed9iJxdHuft6bDFF.htm](feat-effects/ed9iJxdHuft6bDFF.htm)|Effect: Deadly Poison Weapon|Effet : Arme empoisonnée mortelle|libre|
|[EDpjey6SCdvIYqEc.htm](feat-effects/EDpjey6SCdvIYqEc.htm)|Effect: Twin Parry (Parry Trait)|Effet : Parade jumelée (Trait parade)|libre|
|[EfMaI6AnROP4X9lN.htm](feat-effects/EfMaI6AnROP4X9lN.htm)|Effect: Mountain Stronghold|Effet : Bastion de la montagne|libre|
|[eMsI1lR0SuJBCYjn.htm](feat-effects/eMsI1lR0SuJBCYjn.htm)|Effect: Consume Energy (Augment Strike)|Effet : Consommer l'énergie (Augmenter la Frappe)|libre|
|[er5tvDNvpbcnlbHQ.htm](feat-effects/er5tvDNvpbcnlbHQ.htm)|Stance: Inspiring Marshal Stance|Posture : Posture du capitaine inspirant|libre|
|[ErLweSmVAN57QIpp.htm](feat-effects/ErLweSmVAN57QIpp.htm)|Effect: Nanite Surge|Effet : Poussée nanite|libre|
|[ESnzqtwSgahLcxg2.htm](feat-effects/ESnzqtwSgahLcxg2.htm)|Effect: Hamstringing Strike|Effet : Frappe aux ischio-jambiers|libre|
|[eu2HidLHaGKe4MPn.htm](feat-effects/eu2HidLHaGKe4MPn.htm)|Effect: Twin Parry|Effet : Parade jumelée}|libre|
|[EVRcdGt4awWPgXla.htm](feat-effects/EVRcdGt4awWPgXla.htm)|Effect: Arcane Propulsion|Effet : Propulsion arcanique|libre|
|[ewfHVi6NWBGM8B6f.htm](feat-effects/ewfHVi6NWBGM8B6f.htm)|Effect: Disrupting Strikes|Effet : Frappes vitalisantes|libre|
|[FHBYfq3w7ddLvzrK.htm](feat-effects/FHBYfq3w7ddLvzrK.htm)|Effect: Orchard's Endurance|Effet : Endurance du verger|libre|
|[FIgud5jqZgIjwkRE.htm](feat-effects/FIgud5jqZgIjwkRE.htm)|Effect: Maiden's Mending|Effet : Guérison de la vierge}|libre|
|[fILVhS5NuCtGXfri.htm](feat-effects/fILVhS5NuCtGXfri.htm)|Effect: Wyrmblessed Blood Magic (Status Bonus - Self)|Effet : Magie du sang Béni du ver (bonus de statut - Soi)|libre|
|[FNTTeJHiK6iOjrSq.htm](feat-effects/FNTTeJHiK6iOjrSq.htm)|Effect: Draconic Blood Magic|Effet : Magie du sang draconique|libre|
|[FPuICuxBLiDaEbDX.htm](feat-effects/FPuICuxBLiDaEbDX.htm)|Effect: Aura of Life|Effet : Aura de Vie|libre|
|[fRlvmul3LbLo2xvR.htm](feat-effects/fRlvmul3LbLo2xvR.htm)|Effect: Living Fortification|Effet : Fortification vivante|libre|
|[FyaekbWsazkJhJda.htm](feat-effects/FyaekbWsazkJhJda.htm)|Effect: Decry Thief (Success)|Effet : Fléau des voleurs (Succès)|libre|
|[G1IRkppxJCYdfqXo.htm](feat-effects/G1IRkppxJCYdfqXo.htm)|Effect: Bespell Weapon (Void)|Effet : Frappe enchantée (Vide)|libre|
|[G4L49aMxHqO2yqxi.htm](feat-effects/G4L49aMxHqO2yqxi.htm)|Effect: Curse of Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[GbbwJhwSNLw06XpO.htm](feat-effects/GbbwJhwSNLw06XpO.htm)|Effect: Bespell Weapon (Force)|Effet : Frappe enchantée (Force)|libre|
|[gcR66Xgi12ICOVt7.htm](feat-effects/gcR66Xgi12ICOVt7.htm)|Stance: Desert Wind|Posture : Vent du désert|libre|
|[GGebXpRPyONZB3eS.htm](feat-effects/GGebXpRPyONZB3eS.htm)|Stance: Everstand Stance|Posture : Posture toujours en position|libre|
|[ghZFZWUh5Z20vOlR.htm](feat-effects/ghZFZWUh5Z20vOlR.htm)|Effect: Fortify Shield|Effet : Bouclier fortifié|libre|
|[gN1LbKYQgi8Fx98V.htm](feat-effects/gN1LbKYQgi8Fx98V.htm)|Effect: Anadi Venom|Effet : Venin anadi|libre|
|[GvqB4M8LrHpzYEvl.htm](feat-effects/GvqB4M8LrHpzYEvl.htm)|Stance: Fane's Fourberie|Posture : Fourberie de Fane|libre|
|[gWwG7MNAesJgpmRW.htm](feat-effects/gWwG7MNAesJgpmRW.htm)|Effect: Cut from the Air|Effet : Découper en l'air|libre|
|[gYpy9XBPScIlY93p.htm](feat-effects/gYpy9XBPScIlY93p.htm)|Stance: Mountain Stance|Posture : Posture de la montagne|libre|
|[h6nyMp4dtPXBfCJc.htm](feat-effects/h6nyMp4dtPXBfCJc.htm)|Effect: Selfish Shield|Effet : Bouclier égoïste|libre|
|[HfXGhXc9D120gvl5.htm](feat-effects/HfXGhXc9D120gvl5.htm)|Effect: Divine Wings|Effet : Ailes divines (aasimar)|libre|
|[HjCXHDZT6GkCyiuG.htm](feat-effects/HjCXHDZT6GkCyiuG.htm)|Effect: Plains Endemic Herbs|Effet : Herbes endémiques Plaine|libre|
|[HKPmrxkZwHRND5Um.htm](feat-effects/HKPmrxkZwHRND5Um.htm)|Effect: Favored Terrain (Increase Swim Speed)|Effet : Environnement de prédilection (Augmenter la Vitesse de nage)|libre|
|[hqeR9faxHj0NDFFP.htm](feat-effects/hqeR9faxHj0NDFFP.htm)|Effect: Curse of Engulfing Flames|Effet : Malédiction du linceul de flammes|libre|
|[hrEG2smy3tZyGxIn.htm](feat-effects/hrEG2smy3tZyGxIn.htm)|Stance: Kindle Inner Flames|Posture : Flammes de la chandelle intérieure|libre|
|[I0g5oaSwaqZ8fFAV.htm](feat-effects/I0g5oaSwaqZ8fFAV.htm)|Effect: Curse of the Perpetual Storm|Effet : Malédiction de la tempête perpétuelle|libre|
|[iaZ6P59YVhdnFIN8.htm](feat-effects/iaZ6P59YVhdnFIN8.htm)|Effect: Guided Skill|Effet : Compétence guidée|libre|
|[IfRkgjyh0JzGalIy.htm](feat-effects/IfRkgjyh0JzGalIy.htm)|Effect: Armor Tampered With (Success)|Effet : Armure trafiquée avec succès|libre|
|[IfsglZ7fdegwem0E.htm](feat-effects/IfsglZ7fdegwem0E.htm)|Effect: Hydraulic Deflection|Effet : Manoeuvres hydrauliques|libre|
|[Im5JBInybWFbHEYS.htm](feat-effects/Im5JBInybWFbHEYS.htm)|Stance: Rain of Embers Stance|Posture : Posture de la pluie de charbons ardents|libre|
|[ImkjllInxmrdDCOq.htm](feat-effects/ImkjllInxmrdDCOq.htm)|Effect: Sanctify Armament|Effet : Arsenal sanctifié|libre|
|[IpRfT9lL3YR6MH6w.htm](feat-effects/IpRfT9lL3YR6MH6w.htm)|Effect: Favored Terrain (Increase Climb Speed)|Effet : Environnement de prédilection (Augmenter la Vitesse d'escalade)|libre|
|[ITvyvbB234bxceRK.htm](feat-effects/ITvyvbB234bxceRK.htm)|Effect: Mutate Weapon|Effet : Arme mutante|libre|
|[ivGiUp0EC5nWT9Hb.htm](feat-effects/ivGiUp0EC5nWT9Hb.htm)|Effect: Read Shibboleths|Effet : Lire les signes distinctifs|libre|
|[IYeAJ0sB2zCgus1b.htm](feat-effects/IYeAJ0sB2zCgus1b.htm)|Effect: Thermal Nimbus (Fire)|Effet : Nimbe thermique - Feu|libre|
|[JefXqvhzUeBArkAP.htm](feat-effects/JefXqvhzUeBArkAP.htm)|Stance: Whirling Blade Stance|Posture : Posture de la lame tournoyante|libre|
|[JF2xCqL6t4UJZtUi.htm](feat-effects/JF2xCqL6t4UJZtUi.htm)|Effect: Blizzard Evasion|Effet : Évasion du blizzard|libre|
|[jlZjUtrfcfIWumSe.htm](feat-effects/jlZjUtrfcfIWumSe.htm)|Effect: Renewed Vigor|Effet : Regain de vigueur|libre|
|[jMQi2kDirzGgddti.htm](feat-effects/jMQi2kDirzGgddti.htm)|Effect: Divine Rebuttal|Effet : Réfutation divine|libre|
|[jO7wMhnjT7yoAtQg.htm](feat-effects/jO7wMhnjT7yoAtQg.htm)|Effect: Root Magic|Effet : Magie des racines|libre|
|[JQUoBlZKT5N5zO5k.htm](feat-effects/JQUoBlZKT5N5zO5k.htm)|Effect: Avenge in Glory|Effet : Revanche dans la gloire|libre|
|[JUgx48XHMz4QM4Ir.htm](feat-effects/JUgx48XHMz4QM4Ir.htm)|Effect: Tactical Debilitations (No Flanking)|Effet : Handicaps tactiques (Pas de tenaille)|libre|
|[JW93FDTAOUNuCgIu.htm](feat-effects/JW93FDTAOUNuCgIu.htm)|Stance: Ravel Of Thorns|Posture : Enchevêtrement d'épines|libre|
|[JwDCoBIwyhOFnDGZ.htm](feat-effects/JwDCoBIwyhOFnDGZ.htm)|Effect: Augment Senses|Effet : Sens augmentés|libre|
|[jwxurN6JPQm9wXug.htm](feat-effects/jwxurN6JPQm9wXug.htm)|Effect: Defensive Recovery|Effet : Récupération défensive|libre|
|[JysvElDwGZ5ABQ6x.htm](feat-effects/JysvElDwGZ5ABQ6x.htm)|Effect: Emotional Fervor|Effet : Ferveur émotionnelle|libre|
|[jYxWTG5J171XVa5r.htm](feat-effects/jYxWTG5J171XVa5r.htm)|Effect: Restorative Strike|Effet : Frappe restauratrice|libre|
|[jZYRxGHyArCci6AF.htm](feat-effects/jZYRxGHyArCci6AF.htm)|Effect: Desert Endemic Herbs|Effet : Herbes endémiques Désert|libre|
|[K0Sv9AHgq245hSLC.htm](feat-effects/K0Sv9AHgq245hSLC.htm)|Effect: Inspired Stratagem|Effet : Stratagème inspiré|libre|
|[k1J2SaHPwZb2Y6Bp.htm](feat-effects/k1J2SaHPwZb2Y6Bp.htm)|Effect: Wings of Air|Effet : Ailes d'air|libre|
|[k8gB0eDuAlGRoeQj.htm](feat-effects/k8gB0eDuAlGRoeQj.htm)|Effect: Benevolent Spirit Deck|Effet : Jeu spirituel bienveillant|libre|
|[kAgUld9PcI4XkHbq.htm](feat-effects/kAgUld9PcI4XkHbq.htm)|Effect: Decry Thief (Critical Success)|Effet : Fléau des voleurs (Succès critique)|libre|
|[KBEJVRrie2JTHWIK.htm](feat-effects/KBEJVRrie2JTHWIK.htm)|Effect: Dread Marshal Stance|Posture : Posture du terrible Capitaine|libre|
|[kDTiRg9vVOYNnTyr.htm](feat-effects/kDTiRg9vVOYNnTyr.htm)|Stance: Powder Punch Stance|Posture : Posture du coup de poing à poudre|libre|
|[KpEtIFwjj0ZrSVbD.htm](feat-effects/KpEtIFwjj0ZrSVbD.htm)|Effect: Precious Arrow|Effet : Flèche précieuse|libre|
|[kui8yKIVsxfJnrYe.htm](feat-effects/kui8yKIVsxfJnrYe.htm)|Effect: Walking the Cardinal Paths|Effet : Arpenter les chemins cardinaux|libre|
|[KVbS7AbhQdeuA0J6.htm](feat-effects/KVbS7AbhQdeuA0J6.htm)|Effect: Genie Blood Magic (Target)|Effet : Magie du sang génie (Cible)|libre|
|[kyrvZfZfzKK1vx9b.htm](feat-effects/kyrvZfZfzKK1vx9b.htm)|Stance: Devrin's Cunning Stance|Posture : Posture astucieuse de Devrin|libre|
|[kZdVPBO58uq38KIR.htm](feat-effects/kZdVPBO58uq38KIR.htm)|Effect: Kneecap|Effet ! Rotule|libre|
|[kzEPq4aczYb6OD2h.htm](feat-effects/kzEPq4aczYb6OD2h.htm)|Effect: Inspiring Marshal Stance|Effet : Posture du Capitaine inspirant|libre|
|[kzSjzK72CQ67wfBH.htm](feat-effects/kzSjzK72CQ67wfBH.htm)|Effect: Protective Spirit Mask|Effet : Masque de l'esprit protecteur|libre|
|[l3S9i2UWGhSO58YX.htm](feat-effects/l3S9i2UWGhSO58YX.htm)|Effect: Cat Nap|Effet : Sieste féline|libre|
|[l4QUaedYofnfXig0.htm](feat-effects/l4QUaedYofnfXig0.htm)|Stance: Multishot Stance|Posture : Posture de tirs multiples|libre|
|[L9g3EMCT3imX650b.htm](feat-effects/L9g3EMCT3imX650b.htm)|Effect: Heaven's Thunder|Effet : Tonnerre du Paradis|libre|
|[Lb4q2bBAgxamtix5.htm](feat-effects/Lb4q2bBAgxamtix5.htm)|Effect: Treat Wounds Immunity|Effet : Immunité à Soigner les blessures|libre|
|[lge4vtZdfZOCWAch.htm](feat-effects/lge4vtZdfZOCWAch.htm)|Stance: Winter Sleet|Posture : Grésil hivernal|libre|
|[Ljrx4N5XACKSk1Ks.htm](feat-effects/Ljrx4N5XACKSk1Ks.htm)|Effect: Core Cannon|Effet : Noyau canon|libre|
|[LOql7Rc5anCse9Nx.htm](feat-effects/LOql7Rc5anCse9Nx.htm)|Effect: Sea Glass Guardians|Effet : Gardiens de la mer de verre|libre|
|[LxSev4GNKv26DbZw.htm](feat-effects/LxSev4GNKv26DbZw.htm)|Stance: Disarming Stance|Posture : Posture désarmante|libre|
|[lZPbv3nBRWmfbs3z.htm](feat-effects/lZPbv3nBRWmfbs3z.htm)|Effect: Strained Metabolism|Effet : Métabolisme sous tension|libre|
|[maBSHuVHyGwga9uC.htm](feat-effects/maBSHuVHyGwga9uC.htm)|Effect: Duelist's Challenge|Effet : Défi du duelliste|libre|
|[mark4VEQoynfYNBF.htm](feat-effects/mark4VEQoynfYNBF.htm)|Stance: Graceful Poise|Posture : Aisance gracieuse|libre|
|[MNkIxAishE22TqL3.htm](feat-effects/MNkIxAishE22TqL3.htm)|Effect: Aura of Despair|Effet : Aura de Désespoir|libre|
|[Mnvd3jV4EW6nAJKI.htm](feat-effects/Mnvd3jV4EW6nAJKI.htm)|Effect: Aura Junction (Air)|Effet : Aura de jonction - air|libre|
|[MrdT7LiOZMN8J4GK.htm](feat-effects/MrdT7LiOZMN8J4GK.htm)|Effect: Fiendish Wings|Effet : Ailes divines (tieffelin)|libre|
|[Ms6WPXRWfXb2KpG2.htm](feat-effects/Ms6WPXRWfXb2KpG2.htm)|Stance: Tenacious Stance|Posture : Posture tenace|libre|
|[MSkspeBsbXm6LQ19.htm](feat-effects/MSkspeBsbXm6LQ19.htm)|Effect: Harrow the Fiend|Effet : tourmenter le fiélon|libre|
|[MZDh3170EFIfOwTO.htm](feat-effects/MZDh3170EFIfOwTO.htm)|Effect: Overdrive (Success)|Effet : Surrégime (Succès)|libre|
|[n1vhmOd7aNiuR3nk.htm](feat-effects/n1vhmOd7aNiuR3nk.htm)|Effect: Diabolic Blood Magic (Self)|Effet : Magie du sang diabolique (Soi)|libre|
|[N2CSGvtPXloOEPrK.htm](feat-effects/N2CSGvtPXloOEPrK.htm)|Effect: Giant's Lunge|Effet : Fente de géant|libre|
|[ngwcN8u7f7CnqGXp.htm](feat-effects/ngwcN8u7f7CnqGXp.htm)|Effect: Distant Wandering|Effet : Errance à distance|libre|
|[Njb4eLx5VngDIDpo.htm](feat-effects/Njb4eLx5VngDIDpo.htm)|Stance: Shattershields|Posture : Plaquesboucliers|libre|
|[NMmsJyeMTawpgLVR.htm](feat-effects/NMmsJyeMTawpgLVR.htm)|Effect: Resounding Bravery|Effet : Bravoure retentissante|libre|
|[nnF7RSVlC6swbSw8.htm](feat-effects/nnF7RSVlC6swbSw8.htm)|Effect: Anoint Ally|Effet : Oindre un allié|libre|
|[Np3OSqKxuB9rTbij.htm](feat-effects/Np3OSqKxuB9rTbij.htm)|Effect: Harden Flesh|Effet : Peau durcie|libre|
|[Nv70aqcQgCBpDYp8.htm](feat-effects/Nv70aqcQgCBpDYp8.htm)|Effect: Shadow Blood Magic (Perception)|Effet : Magie du sang ombre (Perception)|libre|
|[NviQYIVZbPCSWLqT.htm](feat-effects/NviQYIVZbPCSWLqT.htm)|Effect: Aquatic Endemic Herbs|Effet : Herbes endémiques Aquatique|libre|
|[NWOmJ6WJFheaGhho.htm](feat-effects/NWOmJ6WJFheaGhho.htm)|Stance: Mobile Shot Stance|Posture : Posture de tir mobile|libre|
|[o04gwnrVtzWyIEs8.htm](feat-effects/o04gwnrVtzWyIEs8.htm)|Effect: Emblazon Armament (Shield)|Effet : Arsenal blasonné - bouclier|libre|
|[O7OO1kWFEFv6vl3E.htm](feat-effects/O7OO1kWFEFv6vl3E.htm)|Effect: Mountain Strategy (Critically Hit)|Effet : Stratégie de la montagne (Touché par un coup critique)|libre|
|[o7qm13OmaYOMwgib.htm](feat-effects/o7qm13OmaYOMwgib.htm)|Effect: Weapon Tampered With (Critical Success)|Effet : Arme trafiquée avec succès critique|libre|
|[OeZ0E1oUKyGPxPy0.htm](feat-effects/OeZ0E1oUKyGPxPy0.htm)|Effect: Push Back the Dead!|Effet : Repousser les morts-vivants|libre|
|[OhLcaJeQy4Nf5Mwo.htm](feat-effects/OhLcaJeQy4Nf5Mwo.htm)|Effect: Favored Terrain (Gain Swim Speed)|Effet : Environnement de prédilection (Obtenir une Vitesse de nage)|libre|
|[OK7zMlYy25JciBp6.htm](feat-effects/OK7zMlYy25JciBp6.htm)|Effect: Shed Tail|Effet : Autotomie caudale|libre|
|[OqH6IaeOwRWkGPrk.htm](feat-effects/OqH6IaeOwRWkGPrk.htm)|Effect: Shadow Blood Magic (Stealth)|Effet : Magie du sang ombre (Discrétion)|libre|
|[oSzUv21eN9VS9TC1.htm](feat-effects/oSzUv21eN9VS9TC1.htm)|Effect: Curse of Turbulent Moments|Effet : Malédiction des moments de turbulence|libre|
|[oXG7eX26FmePmwUF.htm](feat-effects/oXG7eX26FmePmwUF.htm)|Effect: Discordant Voice|Effet : Voix discordante|libre|
|[p0S3VHkRgMye7RSI.htm](feat-effects/p0S3VHkRgMye7RSI.htm)|Effect: Gathering Moss|Effet : Amasseur de mousse|libre|
|[P6druSuWIVoLrXJR.htm](feat-effects/P6druSuWIVoLrXJR.htm)|Effect: Calculate Threats|Effet : Calculer les menaces|libre|
|[PdFisHX9ZJmKEKCv.htm](feat-effects/PdFisHX9ZJmKEKCv.htm)|Stance: Magnetic Field|Posture : Champ magnétique|libre|
|[pf9yvKNg6jZLrE30.htm](feat-effects/pf9yvKNg6jZLrE30.htm)|Stance: Tiger Stance|Posture : Posture du tigre|libre|
|[pFo9DVyaDb4LdURY.htm](feat-effects/pFo9DVyaDb4LdURY.htm)|Effect: Nanite Surge (Bonus)|Effet : Poussée nanite - bonus|libre|
|[pkcr9w5x6bKzl3om.htm](feat-effects/pkcr9w5x6bKzl3om.htm)|Stance: Jellyfish Stance|Posture : Posture de la méduse|libre|
|[pLurcSPQb2gjAzoP.htm](feat-effects/pLurcSPQb2gjAzoP.htm)|Effect: Kinetic Aura|Effet : Aura kinétique|libre|
|[PMHwCrnh9W4sMu5b.htm](feat-effects/PMHwCrnh9W4sMu5b.htm)|Stance: Tangled Forest Stance|Posture : Posture de la forêt enchevêtrée|libre|
|[pQ3EjUm1lZW9t3el.htm](feat-effects/pQ3EjUm1lZW9t3el.htm)|Effect: Curse of the Hero's Burden|Effet : Malédiction du fardeau du héros|libre|
|[pQ9e5njvIOe5QzFa.htm](feat-effects/pQ9e5njvIOe5QzFa.htm)|Effect: Fleet Tempo|Effet : Tempo rapide|libre|
|[PS17dsXkTdQmOv7w.htm](feat-effects/PS17dsXkTdQmOv7w.htm)|Stance: Buckler Dance|Posture : Danse de la targe|libre|
|[pTYTanMHMwSgJ8TN.htm](feat-effects/pTYTanMHMwSgJ8TN.htm)|Effect: Defensive Instincts|Effet : Instinct défensif|libre|
|[pwbFFD6NzDooobKo.htm](feat-effects/pwbFFD6NzDooobKo.htm)|Effect: Reflexive Shield|Effet : Bouclier instinctif|libre|
|[Px4E3Hyx2pmvSWAA.htm](feat-effects/Px4E3Hyx2pmvSWAA.htm)|Effect: Undying Ferocity|Effet : Férocité immortelle|libre|
|[PX6WdrpzEdUzKRHx.htm](feat-effects/PX6WdrpzEdUzKRHx.htm)|Effect: Enduring Debilitating Strike|Effet : Frappe incapacitante persistante|libre|
|[Q0DKJRnDuuUnLpvn.htm](feat-effects/Q0DKJRnDuuUnLpvn.htm)|Effect: Tail Toxin|Effet : Queue à toxine|libre|
|[q6UokHWSEcEYWmvh.htm](feat-effects/q6UokHWSEcEYWmvh.htm)|Stance: Whirlwind Stance|Posture : Posture tourbillonnante|libre|
|[qBR3kqGCeKp3T2Be.htm](feat-effects/qBR3kqGCeKp3T2Be.htm)|Stance: Disruptive Stance|Posture : Posture perturbatrice|libre|
|[QcReJp7kgURdQCGz.htm](feat-effects/QcReJp7kgURdQCGz.htm)|Effect: Disruptive Stare|Effet : Regard perturbateur|libre|
|[QDQwHxNowRwzUx9R.htm](feat-effects/QDQwHxNowRwzUx9R.htm)|Stance: Reflective Ripple Stance|Effet : Posture de l'onde réfléchissante|libre|
|[qIOEe4kUN7FOBifb.htm](feat-effects/qIOEe4kUN7FOBifb.htm)|Effect: Hybrid Shape (Beastkin)|Effet : Forme hybride (animanthrope)|libre|
|[qM4bQfcwZ0EOS2M9.htm](feat-effects/qM4bQfcwZ0EOS2M9.htm)|Effect: Inspiring Resilience|Effet : Résistance inspirante|libre|
|[QOWjsM4GYUHw6pFA.htm](feat-effects/QOWjsM4GYUHw6pFA.htm)|Effect: Aura Junction (Fire)|Effet : Jonction d'aura - feu|libre|
|[QrJ06Sc2GiloQ6hB.htm](feat-effects/QrJ06Sc2GiloQ6hB.htm)|Stance: Assume Earth's Mantle|Posture : Endosser la croute terrestre|libre|
|[qSKVcw6brzrvfhUM.htm](feat-effects/qSKVcw6brzrvfhUM.htm)|Effect: Supercharge Prosthetic Eyes|Effet : Surcharge des yeux prothétiques|libre|
|[QTG73gxKSNkiEWdY.htm](feat-effects/QTG73gxKSNkiEWdY.htm)|Effect: Mountain Endemic Herbs|Effet : Herbes endémiques Montagnes|libre|
|[qUowHpn79Dpt1hVn.htm](feat-effects/qUowHpn79Dpt1hVn.htm)|Stance: Dragon Stance|Posture : Posture du dragon|libre|
|[qX62wJzDYtNxDbFv.htm](feat-effects/qX62wJzDYtNxDbFv.htm)|Stance: Dread Marshal Stance|Posture : Posture du terrible capitaine|libre|
|[r4kb2zDepFeczMsl.htm](feat-effects/r4kb2zDepFeczMsl.htm)|Effect: Bone Swarm|Effet : Nuée d'os|libre|
|[R6mx6EfLxSrQlrRa.htm](feat-effects/R6mx6EfLxSrQlrRa.htm)|Effect: Anchored|Effet : Ancré|libre|
|[raLQ458uiyd3lI2K.htm](feat-effects/raLQ458uiyd3lI2K.htm)|Effect: Guided by the Stars|Effet : Guidé par les étoiles|libre|
|[RATDyLyxXN3qmOas.htm](feat-effects/RATDyLyxXN3qmOas.htm)|Effect: Daydream Trance|Effet : Transe du rêve éveillé|libre|
|[rCsmv66TzQhte4Gp.htm](feat-effects/rCsmv66TzQhte4Gp.htm)|Effect: Wood Impulse Junction|Effet : Jonction d'impulsion du bois|libre|
|[RDKbWKphiD9ippAv.htm](feat-effects/RDKbWKphiD9ippAv.htm)|Stance: Stonestrike Stance|Posture : Posture de frappe de roche|libre|
|[Rgt9hH3W1oh9dvku.htm](feat-effects/Rgt9hH3W1oh9dvku.htm)|Effect: Vicious Debilitations|Effet : Handicaps cruels|libre|
|[rJpkKaPRGaH0pLse.htm](feat-effects/rJpkKaPRGaH0pLse.htm)|Effect: Fey Blood Magic|Effet : Magie du sang féerique|libre|
|[RoGEt7lrCdfaueB9.htm](feat-effects/RoGEt7lrCdfaueB9.htm)|Effect: Share Rage|Effet : Rage partagée|libre|
|[rp6hA52dWVwtuu5F.htm](feat-effects/rp6hA52dWVwtuu5F.htm)|Effect: Harrow Omen|Effet : Présage du Tourment|libre|
|[Ru4BNABCZ0hUbX7S.htm](feat-effects/Ru4BNABCZ0hUbX7S.htm)|Effect: Marshal's Aura|Effet : Aura de capitaine|libre|
|[RU6D7pNQSBt1zSuK.htm](feat-effects/RU6D7pNQSBt1zSuK.htm)|Effect: Propulsive Leap|Effet : Bond propulsif|libre|
|[ruRAfGJnik7lRavk.htm](feat-effects/ruRAfGJnik7lRavk.htm)|Effect: Nymph Blood Magic (Target)|Effet : Magie du sang nymphe (Cible)|libre|
|[rvyeOU7TQTLnKj03.htm](feat-effects/rvyeOU7TQTLnKj03.htm)|Effect: Reckless Abandon (Goblin)|Effet : Dangereux abandon (Gobelin)|libre|
|[rwDsr5XsrYcH7oFT.htm](feat-effects/rwDsr5XsrYcH7oFT.htm)|Effect: Curse of the Sky's Call|Effet : Malédiction de l'appel des cieux|libre|
|[RXbfq6oqzVnW6xOV.htm](feat-effects/RXbfq6oqzVnW6xOV.htm)|Stance: Shooting Stars Stance|Posture : Posture des étoiles lancées|libre|
|[rzcpTJU9MvW1x1gz.htm](feat-effects/rzcpTJU9MvW1x1gz.htm)|Effect: Armor Tampered With (Critical Success)|Effet : Armure trafiquée avec succès critique|libre|
|[s1tulrmW6teTFjVd.htm](feat-effects/s1tulrmW6teTFjVd.htm)|Effect: Angelic Blood Magic|Effet : Magie du sang angélique|libre|
|[s3Te8waFP3KEb2dN.htm](feat-effects/s3Te8waFP3KEb2dN.htm)|Effect: Shield Ally|Effet : Bouclier allié|libre|
|[s8LMK2zCQgUT3HoY.htm](feat-effects/s8LMK2zCQgUT3HoY.htm)|Effect: Aura Junction (Water)|Effet : Jonction d'aura - air|libre|
|[sDftJWPPSUeSZD3A.htm](feat-effects/sDftJWPPSUeSZD3A.htm)|Effect: Favored Terrain (Gain Climb Speed)|Effet : Environnement de prédilection (Obtenir une Vitesse d'escalade)|libre|
|[sfUsodcGb4atcSyN.htm](feat-effects/sfUsodcGb4atcSyN.htm)|Effect: Reckless Abandon (Barbarian)|Effet : Dangereux abandon (Barbare)|libre|
|[SiegLMJpVOGuoyWJ.htm](feat-effects/SiegLMJpVOGuoyWJ.htm)|Effect: Ghost Wrangler|Effet : Dresseur de fantôme|libre|
|[su5qLXoweaHxt6ZP.htm](feat-effects/su5qLXoweaHxt6ZP.htm)|Effect: Aura Junction (Wood)|Effet : Jonction d'aura - bois|libre|
|[SVGW8CLKwixFlnTv.htm](feat-effects/SVGW8CLKwixFlnTv.htm)|Effect: Nymph Blood Magic (Self)|Effet : Magie du sang nymphe (Soi)|libre|
|[SXYcrnGzWCuj8zq7.htm](feat-effects/SXYcrnGzWCuj8zq7.htm)|Effect: Poison Weapon|Effet : Arme empoisonnée|libre|
|[T5rsLTqS274B9Mly.htm](feat-effects/T5rsLTqS274B9Mly.htm)|Effect: Current Spell (Air)|Effet : Courant de sort (air)|libre|
|[T7AJQbfmlA57y625.htm](feat-effects/T7AJQbfmlA57y625.htm)|Effect: Vivacious Bravado|Effet : Bravade vivifiante|libre|
|[Tju9kpQlwcKkyKor.htm](feat-effects/Tju9kpQlwcKkyKor.htm)|Effect: Curse of Torrential Knowledge|Effet : Malédiction de l'afflux de connaissances|libre|
|[tl94WHJ2Hg0akK2o.htm](feat-effects/tl94WHJ2Hg0akK2o.htm)|Effect: Invigorating Breath|Effet : Souffle revigorant|libre|
|[tlft5vzk66iWCVRq.htm](feat-effects/tlft5vzk66iWCVRq.htm)|Effect: Safeguard Soul|Effet : Âme à l'abri|libre|
|[tPKXLtDJ3bzJcXlv.htm](feat-effects/tPKXLtDJ3bzJcXlv.htm)|Stance: Ironblood Stance|Posture : Posture du sang-de-fer|libre|
|[ttIvoQUl1yG5K62o.htm](feat-effects/ttIvoQUl1yG5K62o.htm)|Effect: Watch Your Back|Effet : Fais attention|libre|
|[Tw9MjeQHL3qFY1PO.htm](feat-effects/Tw9MjeQHL3qFY1PO.htm)|Effect: Furnace Form|Effet : Forme de four|libre|
|[tx0S0fnfZ6Q2o80H.htm](feat-effects/tx0S0fnfZ6Q2o80H.htm)|Effect: High-Speed Regeneration Speed Boost|Effet : Régénération à haute vitesse augmentation de vitesse|libre|
|[uA1Ofqoyi0UiZIPk.htm](feat-effects/uA1Ofqoyi0UiZIPk.htm)|Effect: Clue In (Expertise)|Effet : Partager les indices (Expertise de l'enquêteur)|libre|
|[UBC6HbfqbfPQYlMq.htm](feat-effects/UBC6HbfqbfPQYlMq.htm)|Effect: Tidal Shield|Effet : Bouclier de la marée|libre|
|[uBJsxCzNhje8m8jj.htm](feat-effects/uBJsxCzNhje8m8jj.htm)|Effect: Panache|Effet : Panache|libre|
|[UE0yky6aW0WCF0Qg.htm](feat-effects/UE0yky6aW0WCF0Qg.htm)|Effect: Fulminating Shot|Effet : Tir fulminant|libre|
|[ugeStF0Rj8phBPWL.htm](feat-effects/ugeStF0Rj8phBPWL.htm)|Effect: Witch's Charge|Effet : Fardeau du sorcier|libre|
|[Unfl4QQURWaX2zfd.htm](feat-effects/Unfl4QQURWaX2zfd.htm)|Stance: Ricochet Stance|Posture : Posture du ricochet|libre|
|[UQ7vZgmfK0VSFS8A.htm](feat-effects/UQ7vZgmfK0VSFS8A.htm)|Effect: Aberrant Blood Magic|Effet : Magie du sang aberrant|libre|
|[ut5SVyCSXel69nnd.htm](feat-effects/ut5SVyCSXel69nnd.htm)|Effect: Offensive Boost|Effet : Renfort offensif|libre|
|[uXCU8GgriUjuj5FV.htm](feat-effects/uXCU8GgriUjuj5FV.htm)|Effect: Hunter's Edge, Flurry|Effet : Spécialisation du chasseur, Déluge|libre|
|[vguxP8ukwVTWWWaA.htm](feat-effects/vguxP8ukwVTWWWaA.htm)|Effect: Imperial Blood Magic|Effet : Magie du sang impérial|libre|
|[vhSYlQiAQMLuXqoc.htm](feat-effects/vhSYlQiAQMLuXqoc.htm)|Effect: Clue In|Effet : Partager les indices|libre|
|[VIScVb6Hl7KwoWfH.htm](feat-effects/VIScVb6Hl7KwoWfH.htm)|Effect: Bolera's Interrogation (Critical Failure)|Effet : Interrogatoire de Boléra (échec critique)|libre|
|[vjvcccAwdkOLA1Fc.htm](feat-effects/vjvcccAwdkOLA1Fc.htm)|Stance: Peafowl Stance|Posture : Posture du paon|libre|
|[w6X7io56B2HHTOvs.htm](feat-effects/w6X7io56B2HHTOvs.htm)|Effect: Guardian's Deflection|Effet : Déviation du Gardien|libre|
|[W8CKuADdbODpBh6O.htm](feat-effects/W8CKuADdbODpBh6O.htm)|Stance: Lunging Stance|Posture : Posture de fente|libre|
|[W8HWQ47YNHWB8kj6.htm](feat-effects/W8HWQ47YNHWB8kj6.htm)|Effect: Topple Giants|Effet : Renverser les géants|libre|
|[wjNNHgX6ceKLbn8Q.htm](feat-effects/wjNNHgX6ceKLbn8Q.htm)|Effect: Rallying Charge|Effet : Charge de ralliement|libre|
|[wmBSuZPqiDyUNwXH.htm](feat-effects/wmBSuZPqiDyUNwXH.htm)|Effect: Dragon's Rage Wings|Effet : Ailes de rage du dragon|libre|
|[woKCbf1kXPrPjeZG.htm](feat-effects/woKCbf1kXPrPjeZG.htm)|Stance: Crowned in Tempest's Fury|Posture : Couronné par la furie de la tempête|libre|
|[wQDHpOKY3GZqvS2v.htm](feat-effects/wQDHpOKY3GZqvS2v.htm)|Effect: Seedpod|Effet : Cosse|libre|
|[WRe8qbemruWxkN8d.htm](feat-effects/WRe8qbemruWxkN8d.htm)|Effect: Rampaging Form (Frozen Winds Kitsune)|Effet : Forme destructrice (Kitsune du vent gelé)|libre|
|[WxE5S3KY1DR5Nbxm.htm](feat-effects/WxE5S3KY1DR5Nbxm.htm)|Effect: Living Fortification (Parry Trait)|Effet : Fortification vivante - trait parade|libre|
|[X19XgqqItqZ4tfmq.htm](feat-effects/X19XgqqItqZ4tfmq.htm)|Effect: Guardian's Embrace|Effet : Étreinte du gardien|libre|
|[X1pGyhMKrCTvHB0q.htm](feat-effects/X1pGyhMKrCTvHB0q.htm)|Effect: Favorable Winds|Effet : Vents favorables|libre|
|[XaZdQHF9GvaJINqH.htm](feat-effects/XaZdQHF9GvaJINqH.htm)|Effect: Elemental Assault|Effet : Assaut élémentaire|libre|
|[XFc3dVzTe7KnpjuP.htm](feat-effects/XFc3dVzTe7KnpjuP.htm)|Stance: Air Shroud|Posture : Enveloppe d'air|libre|
|[XIYWFGHBlcc79YI5.htm](feat-effects/XIYWFGHBlcc79YI5.htm)|Effect: Echoes in Stone|Effet : Échos dans la roche|libre|
|[XJtlvaqAHseq1yoz.htm](feat-effects/XJtlvaqAHseq1yoz.htm)|Effect: Towering Presence|Effet : Présence imposante|libre|
|[XM1AA8z5cHm8sJXM.htm](feat-effects/XM1AA8z5cHm8sJXM.htm)|Effect: Enlightened Presence|Effet : Présence éclairée|libre|
|[xPg5wzzKNxJy18rU.htm](feat-effects/xPg5wzzKNxJy18rU.htm)|Effect: Brightness Seeker|Effet : Aspirant à l'illumination|libre|
|[XQpTyjXFYYNexyOk.htm](feat-effects/XQpTyjXFYYNexyOk.htm)|Effect: Devise a Stratagem|Effet : Concevoir un stratagème|libre|
|[xtqOIXCe0Nsd0QCt.htm](feat-effects/xtqOIXCe0Nsd0QCt.htm)|Effect: Lock On|Effet : Verrouillage|libre|
|[Y96a1OedsU8PVf7z.htm](feat-effects/Y96a1OedsU8PVf7z.htm)|Effect: Starlight Armor|Effet : Armure de lumière stellaire|libre|
|[YaSxccYfE5ShFdFd.htm](feat-effects/YaSxccYfE5ShFdFd.htm)|Effect: Resurrectionist|Effet : Résurrecteur|libre|
|[ybc7tZwByenCzow8.htm](feat-effects/ybc7tZwByenCzow8.htm)|Effect: Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[yBTASi3FvnReAwHy.htm](feat-effects/yBTASi3FvnReAwHy.htm)|Effect: Debilitating Strike|Effet : Frappe incapacitante|libre|
|[yfbP64r4a9e5oyli.htm](feat-effects/yfbP64r4a9e5oyli.htm)|Effect: Demonic Blood Magic (Target)|Effet : Magie du sang démoniaque (Cible)|libre|
|[YIShkE3JCEuwCAxl.htm](feat-effects/YIShkE3JCEuwCAxl.htm)|Effect: Metallic Skin|Effet : Peau métallique|libre|
|[YkiTA74FrUUu5IvI.htm](feat-effects/YkiTA74FrUUu5IvI.htm)|Stance: Rough Terrain Stance|Posture : Posture du terrain accidenté|libre|
|[YKJhjkerCW0Jl6HP.htm](feat-effects/YKJhjkerCW0Jl6HP.htm)|Effect: Life-Giving Magic|Effet : Magie donneuse de vie|libre|
|[YMdRmEcOlM3uU9Em.htm](feat-effects/YMdRmEcOlM3uU9Em.htm)|Effect: Living for the Applause|Effet : Vivre pour les applaudissements|libre|
|[YNoBbHUu7enOSKyv.htm](feat-effects/YNoBbHUu7enOSKyv.htm)|Stance: Orchard's Endurance|Posture : Endurance du verger|libre|
|[yqCzUxrdLlk6Q9VW.htm](feat-effects/yqCzUxrdLlk6Q9VW.htm)|Effect: Envenom Fangs|Effet : Crocs empoisonnés|libre|
|[yr5ey5qC8dXH749T.htm](feat-effects/yr5ey5qC8dXH749T.htm)|Effect: Entity's Resurgence|Effet : Résurgence de l'entité|libre|
|[ytG5XJmkOnDOTjNN.htm](feat-effects/ytG5XJmkOnDOTjNN.htm)|Effect: Soaring Flight|Effet : Voler haut|libre|
|[z3uyCMBddrPK5umr.htm](feat-effects/z3uyCMBddrPK5umr.htm)|Effect: Rage|Effet : Rage|libre|
|[zcJii1XyOne9EvMr.htm](feat-effects/zcJii1XyOne9EvMr.htm)|Effect: Assisting Shot|Effet : Tir de soutien|libre|
|[zezKegTvOArcDQ0x.htm](feat-effects/zezKegTvOArcDQ0x.htm)|Effect: Weapon Infusion|Effet : Arsenal perfusé|libre|
|[zocU4IYIlWwRKUuE.htm](feat-effects/zocU4IYIlWwRKUuE.htm)|Effect: Energy Shot|Effet : Tir énergétique|libre|
|[zqgOjMU9TGoGwJWc.htm](feat-effects/zqgOjMU9TGoGwJWc.htm)|Stance: Rebirth In Living Stone|Posture : Renaissance en roche vivante|libre|
|[zQHF2kkhZRAcrQvR.htm](feat-effects/zQHF2kkhZRAcrQvR.htm)|Effect: Sniping Duo Dedication|Effet : Dévouement : Duo de tieur d'élite|libre|
|[ZSgB3imGveukWUxs.htm](feat-effects/ZSgB3imGveukWUxs.htm)|Effect: Bespell Weapon (Mental)|Effet : Frappe enchantée (Mental)|libre|
|[ZsO5juyylVoxUkXh.htm](feat-effects/ZsO5juyylVoxUkXh.htm)|Effect: Bone Spikes|Effet : Pointe d'os|libre|
|[zUvicEXd4OgCZ1cO.htm](feat-effects/zUvicEXd4OgCZ1cO.htm)|Effect: You're an Embarrassment|Effet : Tu es gênant !|libre|
