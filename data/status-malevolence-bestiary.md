# État de la traduction (malevolence-bestiary)

 * **aucune**: 20


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[1wb9Vi3V4GDeOBCt.htm](malevolence-bestiary/1wb9Vi3V4GDeOBCt.htm)|Ixirizmid|
|[1XrwqxEW8rUr8238.htm](malevolence-bestiary/1XrwqxEW8rUr8238.htm)|Tanglebones|
|[8zR1Gmv6rIlsWsTR.htm](malevolence-bestiary/8zR1Gmv6rIlsWsTR.htm)|Xarwin Caul|
|[bEFdPjTLtNboaTg2.htm](malevolence-bestiary/bEFdPjTLtNboaTg2.htm)|Esobok Ghoul|
|[ecdfItOKsZVc4v0L.htm](malevolence-bestiary/ecdfItOKsZVc4v0L.htm)|Xarwin Portraits|
|[etVnNsUFpgeOBFJO.htm](malevolence-bestiary/etVnNsUFpgeOBFJO.htm)|Carrion Vortex|
|[fQClrtmAe3AER0w2.htm](malevolence-bestiary/fQClrtmAe3AER0w2.htm)|Lured to Ashes|
|[Gywa8GSJtDCPSjbt.htm](malevolence-bestiary/Gywa8GSJtDCPSjbt.htm)|Architect and Hunter|
|[JMXNSC7M2SRGZbhy.htm](malevolence-bestiary/JMXNSC7M2SRGZbhy.htm)|Undead Brain Collector|
|[n3taMFiViyEQ5JAi.htm](malevolence-bestiary/n3taMFiViyEQ5JAi.htm)|Ioseff Xarwin|
|[sdpB7n2LbzVRqrnv.htm](malevolence-bestiary/sdpB7n2LbzVRqrnv.htm)|Yianyin|
|[TrX8Op3qZzFvX4Df.htm](malevolence-bestiary/TrX8Op3qZzFvX4Df.htm)|Mouth of Tchekuth|
|[vcFMNQf9vg7r11ca.htm](malevolence-bestiary/vcFMNQf9vg7r11ca.htm)|Xarwin's Manifestation|
|[VgBE6ow0CvsQxFkq.htm](malevolence-bestiary/VgBE6ow0CvsQxFkq.htm)|Algea|
|[w2EmOacJ2zGfygFu.htm](malevolence-bestiary/w2EmOacJ2zGfygFu.htm)|Wrathful Hatchet|
|[Wkn2wZbsNSifAnBh.htm](malevolence-bestiary/Wkn2wZbsNSifAnBh.htm)|Anitoli Nostraema|
|[x0RGlIHXDN9y0D08.htm](malevolence-bestiary/x0RGlIHXDN9y0D08.htm)|Corrupted Nosoi|
|[Y9GX6pST1P9Y3Okh.htm](malevolence-bestiary/Y9GX6pST1P9Y3Okh.htm)|Haunted Nosoi|
|[YSL27Oq2xFIzrq9u.htm](malevolence-bestiary/YSL27Oq2xFIzrq9u.htm)|Nils Kelveken|
|[ZhxhWosYW3tZfDqL.htm](malevolence-bestiary/ZhxhWosYW3tZfDqL.htm)|Xarwin Manor Phantasm|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
