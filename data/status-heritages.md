# État de la traduction (heritages)

 * **libre**: 215
 * **officielle**: 8
 * **changé**: 6


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[35k2aujXYvqUCSS1.htm](heritages/35k2aujXYvqUCSS1.htm)|Cavern Kobold|Kobold cavernicole|changé|
|[By1y7HCfVPgX2GmI.htm](heritages/By1y7HCfVPgX2GmI.htm)|Ragdyan Vanara|Vanara Radgyen|changé|
|[h2VKMYAlUIFAAXVG.htm](heritages/h2VKMYAlUIFAAXVG.htm)|Nyktera|Nyctère|changé|
|[kXp8qRh5AgtD4Izi.htm](heritages/kXp8qRh5AgtD4Izi.htm)|Witch Gnoll|Gnoll sorcier|changé|
|[mnhpCk9dIwMuFegM.htm](heritages/mnhpCk9dIwMuFegM.htm)|Paddler Shoony|Shoony Pagayeur|changé|
|[tarfuEXmi0E0Enfy.htm](heritages/tarfuEXmi0E0Enfy.htm)|Shadow Rat|Rat de l'ombre|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Iv6LfT3UEt8taj5.htm](heritages/0Iv6LfT3UEt8taj5.htm)|Warden Human (BB)|Humain gardien (BI)|libre|
|[0TFf82gcfxXG9A54.htm](heritages/0TFf82gcfxXG9A54.htm)|Spellkeeper Shisk|Shisk garde sort|libre|
|[0vaeOoECfVD5EGbq.htm](heritages/0vaeOoECfVD5EGbq.htm)|Warrenbred Hobgoblin|Hobgobelin Terrierné|libre|
|[1dYDucCIaZpCJqBc.htm](heritages/1dYDucCIaZpCJqBc.htm)|Arctic Elf|Elfe arctique|officielle|
|[1lv7RMp7t5iqeUFT.htm](heritages/1lv7RMp7t5iqeUFT.htm)|Hunting Catfolk|Homme-félin chasseur|libre|
|[1oLMOmLpurfWTTff.htm](heritages/1oLMOmLpurfWTTff.htm)|Nephilim|Néphilim|libre|
|[1wVDYY9Wue0G5R9Q.htm](heritages/1wVDYY9Wue0G5R9Q.htm)|Whisper Elf|Elfe des murmures|libre|
|[2cii5ZkBsJ4DYdd2.htm](heritages/2cii5ZkBsJ4DYdd2.htm)|Cactus Leshy|Léchi cactus|libre|
|[2hLDilS6qbjHxgVS.htm](heritages/2hLDilS6qbjHxgVS.htm)|Dogtooth Tengu|Tengu dent de chien|libre|
|[2jsWmnKtidCTpaQV.htm](heritages/2jsWmnKtidCTpaQV.htm)|Venomshield Nagaji|Nagaji boucliervenin|libre|
|[2kMltxs2rmxRSxfV.htm](heritages/2kMltxs2rmxRSxfV.htm)|Hunter Automaton|Automate chasseur|libre|
|[2kSzKDtwbcILZTIe.htm](heritages/2kSzKDtwbcILZTIe.htm)|Snaptongue Grippli|Grippli à longue langue|libre|
|[32oX6hHUY6K8N70Q.htm](heritages/32oX6hHUY6K8N70Q.htm)|Charhide Goblin|Gobelin peaud'charbon|officielle|
|[3F5ffk7cmnrBhPcT.htm](heritages/3F5ffk7cmnrBhPcT.htm)|Liminal Catfolk|Homme-félin liminaire|libre|
|[3reGfXH0S82hM7Gp.htm](heritages/3reGfXH0S82hM7Gp.htm)|Ganzi|Ganzi|libre|
|[5A1wMPdzN1OWE4cY.htm](heritages/5A1wMPdzN1OWE4cY.htm)|Caveclimber Kobold|Kobold grimpegrotte|libre|
|[5CqsBKCZuGON53Hk.htm](heritages/5CqsBKCZuGON53Hk.htm)|Forge Dwarf|Nain des forges|officielle|
|[6dMd4JG0ndrObEUj.htm](heritages/6dMd4JG0ndrObEUj.htm)|Winter Catfolk|Homme-félin hivernal|libre|
|[6JKdAZGa8odFzleS.htm](heritages/6JKdAZGa8odFzleS.htm)|Farsight Goloma|Goloma à vision de loin|libre|
|[6KhxY5ArGFhLF7r1.htm](heritages/6KhxY5ArGFhLF7r1.htm)|Thorned Rose|Rose piquante|libre|
|[6rIIsZg3tOyIU3g3.htm](heritages/6rIIsZg3tOyIU3g3.htm)|Frilled Lizardfolk|Homme-lézard à colerette|libre|
|[6xxXtgj3fcCi53lt.htm](heritages/6xxXtgj3fcCi53lt.htm)|Sandstrider Lizardfolk|Homme-lézard arpenteur des sables|libre|
|[7gGcpQMqnZhBDZLI.htm](heritages/7gGcpQMqnZhBDZLI.htm)|Adaptive Anadi|Anadi adaptée|libre|
|[7kHg780SAsu2FNfP.htm](heritages/7kHg780SAsu2FNfP.htm)|Stuffed Poppet|Poupée rembourrée|libre|
|[7lFPhRMAFXQsXUP2.htm](heritages/7lFPhRMAFXQsXUP2.htm)|Snow Rat|Rat Neige|libre|
|[7p9HtLzWBHc18JDW.htm](heritages/7p9HtLzWBHc18JDW.htm)|Deep Rat|Rat des profondeurs|libre|
|[7vHLPleFpSqKAjWG.htm](heritages/7vHLPleFpSqKAjWG.htm)|Stormtossed Tengu|Tengu battu par les vents|libre|
|[7wdeVadvchdM0aPK.htm](heritages/7wdeVadvchdM0aPK.htm)|Mistbreath Azarketi|Azarketi Soufflebrume|libre|
|[7ZDCShtRg5QZggrU.htm](heritages/7ZDCShtRg5QZggrU.htm)|Inured Azarketi|Azarketi endurci|libre|
|[85tRKGZUTFa6pKpG.htm](heritages/85tRKGZUTFa6pKpG.htm)|Oathkeeper Dwarf|Nain gardeserment|libre|
|[87h0jepQuzIbN7jN.htm](heritages/87h0jepQuzIbN7jN.htm)|Fungus Leshy|Léchi fongique|libre|
|[8Gsa8KFsHizEwSHU.htm](heritages/8Gsa8KFsHizEwSHU.htm)|Badlands Orc|Orc des badlands|libre|
|[8wGUh9RsMUamOKjh.htm](heritages/8wGUh9RsMUamOKjh.htm)|Tailed Goblin|Gobelin à queue|libre|
|[9Iu1gFEuvVz9zaYU.htm](heritages/9Iu1gFEuvVz9zaYU.htm)|Spined Azarketi|Azarketi épineux|libre|
|[9mS8EGLlGUOzSAzP.htm](heritages/9mS8EGLlGUOzSAzP.htm)|Nightglider Strix|Strix planeur nocturne|libre|
|[a6F2WjYU8D0suT8T.htm](heritages/a6F2WjYU8D0suT8T.htm)|Razortooth Goblin|Gobelin Dent'rasoir|libre|
|[AVZJI5wP2X5o0LL3.htm](heritages/AVZJI5wP2X5o0LL3.htm)|Trogloshi|Trogloshi|libre|
|[B89BCo6LtI3SJq54.htm](heritages/B89BCo6LtI3SJq54.htm)|Sweetbreath Gnoll|Gnoll à haleine douce|libre|
|[BFOsMnWfXL1oaWkY.htm](heritages/BFOsMnWfXL1oaWkY.htm)|Steelskin Hobgoblin|Hobgobelin Peaud'acier|libre|
|[BhbwjTFw2V67XF35.htm](heritages/BhbwjTFw2V67XF35.htm)|Keen-Venom Vishkanya|Vihskanya au venin vif|libre|
|[BjuZKA7lzFSjKbif.htm](heritages/BjuZKA7lzFSjKbif.htm)|Polyglot Android|Androïde polyglotte|libre|
|[bKr34Uvxc2XClr9q.htm](heritages/bKr34Uvxc2XClr9q.htm)|Strong Oak|Chêne dur|libre|
|[bLhIBwqdjTiVJ5qm.htm](heritages/bLhIBwqdjTiVJ5qm.htm)|Clawed Catfolk|Homme-félin griffu|libre|
|[bmA9JK06rnOKpNLr.htm](heritages/bmA9JK06rnOKpNLr.htm)|Poisonhide Grippli|Grippli à peau empoisonnée|libre|
|[CCwTBSNTw0caN1jd.htm](heritages/CCwTBSNTw0caN1jd.htm)|Mutated Fleshwarp|Distordu mutant|libre|
|[cCy8vsZENlwiAyZ6.htm](heritages/cCy8vsZENlwiAyZ6.htm)|Twilight Halfling|Halfelin du crépuscule|libre|
|[cDElVLonQvUK3vVk.htm](heritages/cDElVLonQvUK3vVk.htm)|Whipfang Nagaji|Nagaji fouetcrochets|libre|
|[ClshvrjvBTMm3INX.htm](heritages/ClshvrjvBTMm3INX.htm)|Bandaagee Vanara|Vanara Bandaagee|libre|
|[CMf0qluB0LXWReew.htm](heritages/CMf0qluB0LXWReew.htm)|Ancient Ash|Frêne ancien|libre|
|[cnbwtbDmlD0KoLqY.htm](heritages/cnbwtbDmlD0KoLqY.htm)|Insightful Goloma|Goloma perspicace|libre|
|[CnCFZfuKzAYqy61e.htm](heritages/CnCFZfuKzAYqy61e.htm)|Athamasi|Athamasi|libre|
|[Csezts78L4FMaskB.htm](heritages/Csezts78L4FMaskB.htm)|Lethoci|Lethoci|libre|
|[Cv7BOjuziOQ0PO9r.htm](heritages/Cv7BOjuziOQ0PO9r.htm)|Windup Poppet|Poupée remontée|libre|
|[cwOUw7kofcAiY01I.htm](heritages/cwOUw7kofcAiY01I.htm)|Snaring Anadi|Anadi piégeur|libre|
|[CzOHITB2ihLGqMuJ.htm](heritages/CzOHITB2ihLGqMuJ.htm)|Runtboss Hobgoblin|Hobgobelin commandant avorton|libre|
|[CZx9HMmoOwcpkLY8.htm](heritages/CZx9HMmoOwcpkLY8.htm)|Root Leshy|Léchi racinaire|libre|
|[d0bNxgGqvaCkFlhN.htm](heritages/d0bNxgGqvaCkFlhN.htm)|Umbral Gnome|Gnome ombral|libre|
|[D3hTAqgwSank8OyO.htm](heritages/D3hTAqgwSank8OyO.htm)|Fey-Touched Gnome|Gnome béni des fées|libre|
|[d7NC4C19AgkspQQg.htm](heritages/d7NC4C19AgkspQQg.htm)|Battle-Trained Human (BB)|Humain entrainé à la bataille (BI)|libre|
|[daaXga11ov9YQVNq.htm](heritages/daaXga11ov9YQVNq.htm)|Polychromatic Anadi|Anadi polychromatique|libre|
|[dJeiekfqGQ8dkwsO.htm](heritages/dJeiekfqGQ8dkwsO.htm)|Wetlander Lizardfolk|Homme-lézard des terres humides|libre|
|[dQqurQys37aJYb26.htm](heritages/dQqurQys37aJYb26.htm)|Leaf Leshy|Léchi feuillu|libre|
|[dwKCwwtWetvPmJks.htm](heritages/dwKCwwtWetvPmJks.htm)|Rainfall Orc|Orque des moussons|libre|
|[EEvA4uj8h3zDiAfP.htm](heritages/EEvA4uj8h3zDiAfP.htm)|Treedweller Goblin|Gobelin arboricole|libre|
|[eFsD7W6hnK33jlDQ.htm](heritages/eFsD7W6hnK33jlDQ.htm)|Sewer Rat|Rat d'égoûts|libre|
|[EHDYVhJcZ9uPUjfZ.htm](heritages/EHDYVhJcZ9uPUjfZ.htm)|Toy Poppet|Poupée jouet|libre|
|[EKY9v7SF1hVsUdbH.htm](heritages/EKY9v7SF1hVsUdbH.htm)|Changeling|Changelin|libre|
|[EoWwvDdoMqN5x0c9.htm](heritages/EoWwvDdoMqN5x0c9.htm)|Rite of Light|Rite de lumière|libre|
|[Eq42wZ5OTweJLnLU.htm](heritages/Eq42wZ5OTweJLnLU.htm)|Gutsy Halfling|Halfelin flegmatique|libre|
|[erZPj5701KiVAqoi.htm](heritages/erZPj5701KiVAqoi.htm)|Forge-Blessed Dwarf|Nain béni-de-la-forge|libre|
|[etkuQkjkNLPLnjkA.htm](heritages/etkuQkjkNLPLnjkA.htm)|Wellspring Gnome|Gnome source|libre|
|[evXJISqyhl3fHE9u.htm](heritages/evXJISqyhl3fHE9u.htm)|Vine Leshy|Léchi liane|libre|
|[faLb2rczsrxAuOTt.htm](heritages/faLb2rczsrxAuOTt.htm)|Rite of Knowing|Rite du savoir|libre|
|[Fgysc0A1pFQE8PMA.htm](heritages/Fgysc0A1pFQE8PMA.htm)|Lorekeeper Shisk|Shisk conservateur de savoir|libre|
|[FLWUYM2XxYwnIDQf.htm](heritages/FLWUYM2XxYwnIDQf.htm)|Xyloshi|Xyloshi|libre|
|[fROPRHGyUn4PgcER.htm](heritages/fROPRHGyUn4PgcER.htm)|Longsnout Rat|Rat à long museau|libre|
|[fWT7Mo2vFC10H4Wq.htm](heritages/fWT7Mo2vFC10H4Wq.htm)|Songbird Strix|Strix oiseau chanteur|libre|
|[g4FRxyuHndZu4KTo.htm](heritages/g4FRxyuHndZu4KTo.htm)|Jinxed Tengu|Tengu maudit|libre|
|[G8jfMayPv4vZvAVr.htm](heritages/G8jfMayPv4vZvAVr.htm)|Sylph|Sylphe|libre|
|[G9Gwfi8ZIva52uGp.htm](heritages/G9Gwfi8ZIva52uGp.htm)|Jinxed Halfling|Halfelin portepoisse|libre|
|[Ga7UEU186pjq7LBD.htm](heritages/Ga7UEU186pjq7LBD.htm)|Talos|Talos|libre|
|[GAn2cdhBE9Bqa85s.htm](heritages/GAn2cdhBE9Bqa85s.htm)|Beastkin|Animanthrope|libre|
|[gfXSF1TafBAmZo2u.htm](heritages/gfXSF1TafBAmZo2u.htm)|Grig|Grig|libre|
|[gKLTlzAVapXTQ86V.htm](heritages/gKLTlzAVapXTQ86V.htm)|Reflection|Reflet|libre|
|[GlejQr3rBh3sn8sL.htm](heritages/GlejQr3rBh3sn8sL.htm)|River Azarketi|Azarketi fluvial|libre|
|[Gmk7oNITvMVBy78Z.htm](heritages/Gmk7oNITvMVBy78Z.htm)|Undine|Ondin|libre|
|[GpnHIonrLN8TFZci.htm](heritages/GpnHIonrLN8TFZci.htm)|Rite of Invocation|Rite d'invocation|libre|
|[gQyPU441J3rGt8mD.htm](heritages/gQyPU441J3rGt8mD.htm)|Snow Goblin|Gobelin des neiges|officielle|
|[gyoN45SVfRZwHMkk.htm](heritages/gyoN45SVfRZwHMkk.htm)|Irongut Goblin|Gobelin Boyaud'fer|libre|
|[hFBwsVcSnNCJoimo.htm](heritages/hFBwsVcSnNCJoimo.htm)|Versatile Human|Humain polyvalent|libre|
|[HFHSh2RWuxa4GhhQ.htm](heritages/HFHSh2RWuxa4GhhQ.htm)|Benthic Azarketi|Azarketi des profondeurs|libre|
|[hOPOyyt7qZXYYCOU.htm](heritages/hOPOyyt7qZXYYCOU.htm)|Nine Lives Catfolk|Homme-félin aux neuf vies|libre|
|[HpqQ5VQ0w4HqYgVC.htm](heritages/HpqQ5VQ0w4HqYgVC.htm)|Jungle Catfolk|Homme-félin de la jungle|libre|
|[hTl3uc6y1kTuo9ac.htm](heritages/hTl3uc6y1kTuo9ac.htm)|Seaweed Leshy|Léchi algue|libre|
|[hzW7VoRDYsKJB8ku.htm](heritages/hzW7VoRDYsKJB8ku.htm)|Surgewise Fleshwarp|Distordu hypersensible|libre|
|[idGDjqi1q3Ft8bAZ.htm](heritages/idGDjqi1q3Ft8bAZ.htm)|Nomadic Halfling|Halfelin nomade|libre|
|[ievKYUc53q0mroGp.htm](heritages/ievKYUc53q0mroGp.htm)|Lotus Leshy|Léchi lotus|libre|
|[IFg2tqmAFFnU8UNU.htm](heritages/IFg2tqmAFFnU8UNU.htm)|Celestial Envoy Kitsune|Kitsune émissaire céleste|libre|
|[ikNJZRxUjcRLisO6.htm](heritages/ikNJZRxUjcRLisO6.htm)|Elfbane Hobgoblin|Hobgobelin fléau elfique|libre|
|[iOREr80Q0SsvPP8B.htm](heritages/iOREr80Q0SsvPP8B.htm)|Sacred Nagaji|Nagaji sacré|libre|
|[isJhIPhT4MsjJvoq.htm](heritages/isJhIPhT4MsjJvoq.htm)|Fishseeker Shoony|Shoony Traquepoisson|libre|
|[ITgkqfnAOJCbcIys.htm](heritages/ITgkqfnAOJCbcIys.htm)|Oread|Oréade|libre|
|[iY2CCqoMc2bRdoas.htm](heritages/iY2CCqoMc2bRdoas.htm)|Created Fleshwarp|Distordu créé|libre|
|[J0eAmntxXywr9sGt.htm](heritages/J0eAmntxXywr9sGt.htm)|Mage Automaton|Mage automate|libre|
|[j0R1SyJP8k4G2Hkn.htm](heritages/j0R1SyJP8k4G2Hkn.htm)|Scavenger Strix|Strix grapilleur|libre|
|[j1nzBc9Pui7vsJ9o.htm](heritages/j1nzBc9Pui7vsJ9o.htm)|Sharpshooter Automaton|Automate tireur de précision|libre|
|[jAX7yavR4lNKwDK8.htm](heritages/jAX7yavR4lNKwDK8.htm)|Ardande|Ardande|libre|
|[Je15UGsLWYaaGJSW.htm](heritages/Je15UGsLWYaaGJSW.htm)|Ghost Poppet|Poupée fantôme|libre|
|[jEtVesbqYcWGbBYk.htm](heritages/jEtVesbqYcWGbBYk.htm)|Seer Elf|Elfe visionnaire|libre|
|[K124fCpU03SJvmeP.htm](heritages/K124fCpU03SJvmeP.htm)|Warmarch Hobgoblin|Hobgobelin Marchebataille|libre|
|[k4AU5tjtngDOIqrB.htm](heritages/k4AU5tjtngDOIqrB.htm)|Deep Fetchling|Fetchelin des profondeurs|libre|
|[KaokXdiE3ewXprdL.htm](heritages/KaokXdiE3ewXprdL.htm)|Pine Leshy|Léchi pin|libre|
|[KbG2BZ3Sbr3xU1sW.htm](heritages/KbG2BZ3Sbr3xU1sW.htm)|Pixie|Pixie|libre|
|[KcozzlkFAqShDEzo.htm](heritages/KcozzlkFAqShDEzo.htm)|Stronggut Shisk|Shisk aux instestins solides|libre|
|[kHHcvJBJNiPJTuna.htm](heritages/kHHcvJBJNiPJTuna.htm)|Wisp Fetchling|Fetchelin menu|libre|
|[kHT9dFJt5yTjeYoB.htm](heritages/kHT9dFJt5yTjeYoB.htm)|Frozen Wind Kitsune|Kitsune du vent gelé|libre|
|[kiKxnKd7Dfegk9dM.htm](heritages/kiKxnKd7Dfegk9dM.htm)|Desert Elf|Elfe du désert|libre|
|[KJ2dSDXP9d5hJHzd.htm](heritages/KJ2dSDXP9d5hJHzd.htm)|Frightful Goloma|Goloma effrayant|libre|
|[KO33MNyY9VqNQmbZ.htm](heritages/KO33MNyY9VqNQmbZ.htm)|Wintertouched Human|Humain touché par l'hiver|libre|
|[Kq3k1Z6IWGVsLrmg.htm](heritages/Kq3k1Z6IWGVsLrmg.htm)|Lahkgyan Vanara|Vanara Lahkgyan|libre|
|[kRDsVbhdBVeSlpBa.htm](heritages/kRDsVbhdBVeSlpBa.htm)|Anvil Dwarf|Nain de l'enclume|libre|
|[kTlJqhC7ZSE8P8lu.htm](heritages/kTlJqhC7ZSE8P8lu.htm)|Venomous Anadi|Anadi venimeux|libre|
|[L6zfGzLMDLHbZ7VV.htm](heritages/L6zfGzLMDLHbZ7VV.htm)|Fruit Leshy|Léchi fruitier|libre|
|[lDT5h3f5GXRj42Ir.htm](heritages/lDT5h3f5GXRj42Ir.htm)|Stonestep Shisk|Shisk Démarchepierre|libre|
|[lj5iHaiY0IwCCptd.htm](heritages/lj5iHaiY0IwCCptd.htm)|Aphorite|Aphorite|libre|
|[LlUEmCDOLSZaGOyI.htm](heritages/LlUEmCDOLSZaGOyI.htm)|Titan Nagaji|Nagaji titan|libre|
|[Lp7ywxabmm88Gei6.htm](heritages/Lp7ywxabmm88Gei6.htm)|Observant Halfling|Halfelin observateur|libre|
|[LU4i3qXtyzeTGWZQ.htm](heritages/LU4i3qXtyzeTGWZQ.htm)|Luminous Sprite|Sprite lumineux|libre|
|[m9rrlchS10xHFA2G.htm](heritages/m9rrlchS10xHFA2G.htm)|Venomtail Kobold|Kobold à queue venimeuse|libre|
|[mBH1L01kYmB8EL56.htm](heritages/mBH1L01kYmB8EL56.htm)|Empty Sky Kitsune|Kitsune du ciel vide|libre|
|[MeMAAtUlZmFgKSMF.htm](heritages/MeMAAtUlZmFgKSMF.htm)|Elemental Heart Dwarf|Nain coeurélémentaire|libre|
|[MhXHEh7utEfxBwmc.htm](heritages/MhXHEh7utEfxBwmc.htm)|Thickcoat Shoony|Shoony à fourrure|libre|
|[Mj7uHxxVkRUlOFwJ.htm](heritages/Mj7uHxxVkRUlOFwJ.htm)|Hillock Halfling|Halfelin des collines|libre|
|[Mmezbef0c1fbJaVV.htm](heritages/Mmezbef0c1fbJaVV.htm)|Impersonator Android|Androïde imposteur|libre|
|[MQx7miBXUmOHycqJ.htm](heritages/MQx7miBXUmOHycqJ.htm)|Laborer Android|Androïde manoeuvre|libre|
|[MtH5bq0MhaMQbJEL.htm](heritages/MtH5bq0MhaMQbJEL.htm)|Murkeyed Azarketi|Azarketi Yeuxobscurs|libre|
|[MTTU2t7x6TjvUDnE.htm](heritages/MTTU2t7x6TjvUDnE.htm)|Hold-Scarred Orc|Orc scarifié|libre|
|[MUujYQYWg6PNVaaN.htm](heritages/MUujYQYWg6PNVaaN.htm)|Predator Strix|Strix prédateur|libre|
|[mZqaKQkvadBbNubM.htm](heritages/mZqaKQkvadBbNubM.htm)|Compact Skeleton|Squelette compact|libre|
|[n2DKA0OQQcfvZRly.htm](heritages/n2DKA0OQQcfvZRly.htm)|Technological Fleshwarp|Distordu technologique|libre|
|[n2eJEjA8pnOMiuCm.htm](heritages/n2eJEjA8pnOMiuCm.htm)|Smokeworker Hobgoblin|Hobgobelin sapeur|libre|
|[N36ZR4lh9eCazDaN.htm](heritages/N36ZR4lh9eCazDaN.htm)|Aiuvarin|Aiuvarin|libre|
|[Nd9hdX8rdYyRozw8.htm](heritages/Nd9hdX8rdYyRozw8.htm)|Ancient Elf|Elfe ancien|libre|
|[NfIAGatB1KIzt8G7.htm](heritages/NfIAGatB1KIzt8G7.htm)|Cavern Elf|Elfe des cavernes|libre|
|[nUCRd8tmz3C6LM0T.htm](heritages/nUCRd8tmz3C6LM0T.htm)|Wajaghand Vanara|Vanara Wajaghand|libre|
|[nW1gi13E62Feto2w.htm](heritages/nW1gi13E62Feto2w.htm)|Woodland Elf|Elfe des bois|libre|
|[NWbdAN5gDse0ad7C.htm](heritages/NWbdAN5gDse0ad7C.htm)|Dark Fields Kitsune|Kitsune des champs sombres|libre|
|[NWsZ0cIeghyzk9bU.htm](heritages/NWsZ0cIeghyzk9bU.htm)|Sharp-Eared Catfolk|Homme-félin aux oreilles pointues|libre|
|[nXQxlmjH24Eb8h2Q.htm](heritages/nXQxlmjH24Eb8h2Q.htm)|Battle-Ready Orc|Orc batailleur|libre|
|[OBxrlZKg0IC5n238.htm](heritages/OBxrlZKg0IC5n238.htm)|Venom-Resistant Vishkanya|Viskanya antivenin|libre|
|[ohOJHeFenX97sBHf.htm](heritages/ohOJHeFenX97sBHf.htm)|Scalekeeper Vishkanya|Vishkanya Conservécaille|libre|
|[OoUqJJB77VfWbWRM.htm](heritages/OoUqJJB77VfWbWRM.htm)|Cliffscale Lizardfolk|Homme-lézard écaille de falaise|libre|
|[OtqOC3ElpF444qMe.htm](heritages/OtqOC3ElpF444qMe.htm)|Discarded Fleshwarp|Distordu rebuté|libre|
|[P8BP1un5BTrwXoBy.htm](heritages/P8BP1un5BTrwXoBy.htm)|Dragonscaled Kobold|Kobold à écailles de dragon|libre|
|[P8Rl3dUsq8AzXLHC.htm](heritages/P8Rl3dUsq8AzXLHC.htm)|Sturdy Skeleton|Squelette robuste|libre|
|[PbXqlzRdQbKLQx1R.htm](heritages/PbXqlzRdQbKLQx1R.htm)|Old-Blood Vishkanya|Vishkanya sang-ancien|libre|
|[pJ395g22dKNoufIK.htm](heritages/pJ395g22dKNoufIK.htm)|Nascent|Naissant|libre|
|[PQuJEYI0UFl8W7fH.htm](heritages/PQuJEYI0UFl8W7fH.htm)|Cataphract Fleshwarp|Distordu cataphracte|libre|
|[ptpK6H1rM4Bu3ry4.htm](heritages/ptpK6H1rM4Bu3ry4.htm)|Mountainkeeper Tengu|Tengu gardemontagne|libre|
|[PwxbD5VSJ0Yroqvp.htm](heritages/PwxbD5VSJ0Yroqvp.htm)|Liminal Fetchling|Fetchelin liminal|libre|
|[pZ1u2ScWrBXSaAqQ.htm](heritages/pZ1u2ScWrBXSaAqQ.htm)|Winter Orc|Orc hivernal|libre|
|[q2omqJ9t0skGTYki.htm](heritages/q2omqJ9t0skGTYki.htm)|Rite of Reinforcement|Rite de renforcement|libre|
|[qbWaybAX1LK7kUyY.htm](heritages/qbWaybAX1LK7kUyY.htm)|Thalassic Azarketi|Azarketi thalassique|libre|
|[qM566kCXljkOpocA.htm](heritages/qM566kCXljkOpocA.htm)|Taloned Tengu|Tengu à serres|libre|
|[rFdVYKtHsZzRCsSd.htm](heritages/rFdVYKtHsZzRCsSd.htm)|Stickytoe Grippli|Grippli aux orteils collants|libre|
|[rKV11HWREwjjMIum.htm](heritages/rKV11HWREwjjMIum.htm)|Skyborn Tengu|Tengu enfant du ciel|libre|
|[RKz7Z5pefXKiv9JE.htm](heritages/RKz7Z5pefXKiv9JE.htm)|Suli|Suli|libre|
|[rQJBtQ9uKUzK9ktK.htm](heritages/rQJBtQ9uKUzK9ktK.htm)|Shortshanks Hobgoblin|Hobgobelin courtespattes|libre|
|[RuQSx0QsirIKxwKY.htm](heritages/RuQSx0QsirIKxwKY.htm)|Warrior Android|Androïde combattant|libre|
|[RxNBBMFZwPA3Vlg3.htm](heritages/RxNBBMFZwPA3Vlg3.htm)|Shoreline Strix|Strix des littoraux|libre|
|[RZHr0olieS6YdYE9.htm](heritages/RZHr0olieS6YdYE9.htm)|Fodder Skeleton|Squelette générique|libre|
|[S1062No0sYH35AhN.htm](heritages/S1062No0sYH35AhN.htm)|Tactile Azarketi|Azarketi tactile|libre|
|[sEnMG5zbnXdJvVPz.htm](heritages/sEnMG5zbnXdJvVPz.htm)|Cloudleaper Lizardfolk|Homme-lézard sautenuage|libre|
|[sGzhnQpgWErX1bmx.htm](heritages/sGzhnQpgWErX1bmx.htm)|Vigilant Goloma|Goloma vigilant|libre|
|[SqEcb1c3yeoJMxm0.htm](heritages/SqEcb1c3yeoJMxm0.htm)|Great Gnoll|Grand gnoll|libre|
|[Svk2CHwvurK1QQhD.htm](heritages/Svk2CHwvurK1QQhD.htm)|Naari|Naari|libre|
|[TDc9MXLXkgEFoKdD.htm](heritages/TDc9MXLXkgEFoKdD.htm)|Flexible Catfolk|Homme-félin flexible|libre|
|[tf3edMCyS15GhFPx.htm](heritages/tf3edMCyS15GhFPx.htm)|Hooded Nagaji|Nagaji coiffé|libre|
|[tLd8Qg82AwEbbmgX.htm](heritages/tLd8Qg82AwEbbmgX.htm)|Dromaar|Dromaar|libre|
|[TQFE10VFvh9wb8zb.htm](heritages/TQFE10VFvh9wb8zb.htm)|Woodstalker Lizardfolk|Homme-lézard arboricole|libre|
|[twayjFuXbsvyHUwy.htm](heritages/twayjFuXbsvyHUwy.htm)|Desert Rat|Rat des sables|libre|
|[tXC5Gwn9D5x0ouJh.htm](heritages/tXC5Gwn9D5x0ouJh.htm)|Sensate Gnome|Gnome sensitif|officielle|
|[TYvzNoL5ldmB5F76.htm](heritages/TYvzNoL5ldmB5F76.htm)|Melixie|Mélixie|libre|
|[U882U2NUUGL6u3rL.htm](heritages/U882U2NUUGL6u3rL.htm)|Tunnel Rat|Rat des tunnels|libre|
|[UaD5VDoFlILEmbyz.htm](heritages/UaD5VDoFlILEmbyz.htm)|Dhampir|Dhampir|libre|
|[udMXXjFirjARYr4p.htm](heritages/udMXXjFirjARYr4p.htm)|Ant Gnoll|Gnoll fourmi|libre|
|[ULj56ZoW7dWdnBvu.htm](heritages/ULj56ZoW7dWdnBvu.htm)|Unbreakable Goblin|Gobelin Incassable|officielle|
|[UV2sABrTC5teOXTE.htm](heritages/UV2sABrTC5teOXTE.htm)|Strong-Blooded Dwarf|Nain sang-fort|libre|
|[uxtcKTkD62SmrUoh.htm](heritages/uxtcKTkD62SmrUoh.htm)|Shapewrought Fleshwarp|Distordu forméforgé|libre|
|[VAo6NnrCEAAOUSkc.htm](heritages/VAo6NnrCEAAOUSkc.htm)|Resolute Fetchling|Fetchelin déterminé|libre|
|[vDEfNzjLpGJU54cz.htm](heritages/vDEfNzjLpGJU54cz.htm)|Quillcoat Shisk|Shisk à piquants|libre|
|[VgL18yU7pysdoZZG.htm](heritages/VgL18yU7pysdoZZG.htm)|Artisan Android|Artisan androïde|libre|
|[ViKRoVgog172r163.htm](heritages/ViKRoVgog172r163.htm)|Vivacious Gnome|Gnome vivace|libre|
|[VqgrYMaAwnNjT9Mn.htm](heritages/VqgrYMaAwnNjT9Mn.htm)|Enchanting Lily|Lys enchanté|libre|
|[vrU3lmDO7FYzmuQc.htm](heritages/vrU3lmDO7FYzmuQc.htm)|Prismatic Vishkanya|Vishkanya prismatique|libre|
|[VRyX00OuPGsJSurM.htm](heritages/VRyX00OuPGsJSurM.htm)|Spellscale Kobold|Kobold écaille de sort|libre|
|[VSyOvtgJ1ZNpIVgC.htm](heritages/VSyOvtgJ1ZNpIVgC.htm)|Rock Dwarf|Nain des roches|officielle|
|[VTtXwBxrfRUXSL38.htm](heritages/VTtXwBxrfRUXSL38.htm)|Death Warden Dwarf|Nain gardemort|libre|
|[VvEAFoxuddYNBmNc.htm](heritages/VvEAFoxuddYNBmNc.htm)|Grave Orc|Orc sépulcral|libre|
|[VYfpTUuXJM3iBOz0.htm](heritages/VYfpTUuXJM3iBOz0.htm)|Unseen Lizardfolk|Homme-lézard mimétique|libre|
|[w5801ArZQCU8IXnU.htm](heritages/w5801ArZQCU8IXnU.htm)|Wishborn Poppet|Poupée souhaitée|libre|
|[WaCn0mcivFv1omNK.htm](heritages/WaCn0mcivFv1omNK.htm)|Strongjaw Kobold|Kobold à forte mâchoire|libre|
|[wB8xiQB4RDbzOOvR.htm](heritages/wB8xiQB4RDbzOOvR.htm)|Monstrous Skeleton|Squelette monstrueux|libre|
|[WEzgrxBRFBGdj8Hx.htm](heritages/WEzgrxBRFBGdj8Hx.htm)|Wavediver Tengu|Tengu plonge vague|libre|
|[wHO5luJMODbDLXNi.htm](heritages/wHO5luJMODbDLXNi.htm)|Bright Fetchling|Fetchelin lumineux|libre|
|[Wk4HyaZtC1j221i1.htm](heritages/Wk4HyaZtC1j221i1.htm)|Earthly Wilds Kitsune|Kitsune des terres sauvages|libre|
|[wn4EbYk1QN3tyFhh.htm](heritages/wn4EbYk1QN3tyFhh.htm)|Deep Orc|Orc des profondeurs|libre|
|[wNnsjird4OQe0s6p.htm](heritages/wNnsjird4OQe0s6p.htm)|Gourd Leshy|Léchi calebasse|libre|
|[WxcbLvufI6JBpLt0.htm](heritages/WxcbLvufI6JBpLt0.htm)|Spindly Anadi|Anadi sautillant|libre|
|[x5S4MNQ0aqUmgHcC.htm](heritages/x5S4MNQ0aqUmgHcC.htm)|Vicious Goloma|Goloma vicieux|libre|
|[xaTTN5anLEBzWCzv.htm](heritages/xaTTN5anLEBzWCzv.htm)|Windweb Grippli|Grippli à palmes|libre|
|[XeXWsvcWU3Zaj5WC.htm](heritages/XeXWsvcWU3Zaj5WC.htm)|Chameleon Gnome|Gnome caméléon|officielle|
|[xtRIYizCjLg9qe1Z.htm](heritages/xtRIYizCjLg9qe1Z.htm)|Wildwood Halfling|Halfelin bois-sauvage|libre|
|[y24ykEUfpIu5Gp6D.htm](heritages/y24ykEUfpIu5Gp6D.htm)|Warrior Automaton|Automate combattant|libre|
|[yL6944LrPo2HNdEJ.htm](heritages/yL6944LrPo2HNdEJ.htm)|Ancient-Blooded Dwarf|Nain sang-ancien|libre|
|[yVtcyAbLmWCIHHZi.htm](heritages/yVtcyAbLmWCIHHZi.htm)|Rite of Passage|Rite de passage|libre|
|[ywNXVLZtwrAStyh1.htm](heritages/ywNXVLZtwrAStyh1.htm)|Elusive Vishkanya|Vishkanya insaisissable|libre|
|[z4cvqtpkkAYoFpHa.htm](heritages/z4cvqtpkkAYoFpHa.htm)|Bloodhound Shoony|Shoony limier|libre|
|[zcO93E8gAW1tDYKk.htm](heritages/zcO93E8gAW1tDYKk.htm)|Draxie|Draxie|libre|
|[zPhArF36ZVgLeVUU.htm](heritages/zPhArF36ZVgLeVUU.htm)|Ancient Scale Azarketi|Azarketi écaille antique|libre|
|[zVf0Hlp5xG0Q7kmc.htm](heritages/zVf0Hlp5xG0Q7kmc.htm)|Skilled Human|Humain talentueux|libre|
|[ZW8GX14n3ZGievK1.htm](heritages/ZW8GX14n3ZGievK1.htm)|Tunnelflood Kobold|Kobold Tunnelinondé|libre|
|[ZZKZkeSP5TuT62IA.htm](heritages/ZZKZkeSP5TuT62IA.htm)|Duskwalker|Crépusculaire|libre|
