# État de la traduction (mwangi-expanse-bestiary)

 * **changé**: 8
 * **libre**: 14


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[8K5v5q9Y01hcSNug.htm](mwangi-expanse-bestiary/8K5v5q9Y01hcSNug.htm)|Rompo|Rompo|changé|
|[bo3BDHT7P1IK7oLp.htm](mwangi-expanse-bestiary/bo3BDHT7P1IK7oLp.htm)|Anadi Elder|Ancien anadi|changé|
|[BxChohV8FBiyAnEc.htm](mwangi-expanse-bestiary/BxChohV8FBiyAnEc.htm)|Charau-ka Acolyte of Angazhan|Charau-ka acolyte d'Angazhan|changé|
|[IWk8dWUf4Q4D8iww.htm](mwangi-expanse-bestiary/IWk8dWUf4Q4D8iww.htm)|Anadi Sage|Sage anadi|changé|
|[j0t62tXPYhBzCLUO.htm](mwangi-expanse-bestiary/j0t62tXPYhBzCLUO.htm)|Mamlambo|Mamlambo|changé|
|[QUyqY6JiZNkEyTnd.htm](mwangi-expanse-bestiary/QUyqY6JiZNkEyTnd.htm)|Charau-ka Butcher|Boucher charau-ka|changé|
|[rVnJUXPgAPCeW1tS.htm](mwangi-expanse-bestiary/rVnJUXPgAPCeW1tS.htm)|Eloko|Biloko eloko|changé|
|[x2udXsqiLZachJHY.htm](mwangi-expanse-bestiary/x2udXsqiLZachJHY.htm)|Maliadi|Maliadi|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[As0M3PvmrR2055pk.htm](mwangi-expanse-bestiary/As0M3PvmrR2055pk.htm)|Asanbosam|Asanbosam|libre|
|[aXXvrcggR8EQJOdm.htm](mwangi-expanse-bestiary/aXXvrcggR8EQJOdm.htm)|Anadi Hunter|Chasseur anadi|libre|
|[EyGpKm5MkHepWuNM.htm](mwangi-expanse-bestiary/EyGpKm5MkHepWuNM.htm)|Pygmy Kaava|Pygmée kaava|libre|
|[hbvxQn0T0m4WoAmZ.htm](mwangi-expanse-bestiary/hbvxQn0T0m4WoAmZ.htm)|Karina|Karina|libre|
|[ld3hMGWfeUT7enNf.htm](mwangi-expanse-bestiary/ld3hMGWfeUT7enNf.htm)|Sié Goluo|Sié Goluo|libre|
|[N5pG7rsbN1lL1ysb.htm](mwangi-expanse-bestiary/N5pG7rsbN1lL1ysb.htm)|Aigamuxa|Aigamuxa|libre|
|[NvyAvzxxfb8XEQr2.htm](mwangi-expanse-bestiary/NvyAvzxxfb8XEQr2.htm)|Grootslang|Grootslang|libre|
|[rayMMWNm61ivtxuU.htm](mwangi-expanse-bestiary/rayMMWNm61ivtxuU.htm)|Biloko Warrior|Biloko combattant|libre|
|[rBlu1EsygXwCkOvd.htm](mwangi-expanse-bestiary/rBlu1EsygXwCkOvd.htm)|Zinba|Zinba|libre|
|[rMby02NQRHNrDcTj.htm](mwangi-expanse-bestiary/rMby02NQRHNrDcTj.htm)|Kaava Stalker|Traqueur kaava|libre|
|[rrJqsVeBKizuyqRY.htm](mwangi-expanse-bestiary/rrJqsVeBKizuyqRY.htm)|Biloko Veteran|Biloko vétéran|libre|
|[RVVSRRYAhQ3dVK2h.htm](mwangi-expanse-bestiary/RVVSRRYAhQ3dVK2h.htm)|Solar Ibis|Ibis solaire|libre|
|[TfEGDGqUk3LpokZZ.htm](mwangi-expanse-bestiary/TfEGDGqUk3LpokZZ.htm)|Charau-ka Warrior|Combattant charau-ka|libre|
|[XSWDwaLEyshEKSZE.htm](mwangi-expanse-bestiary/XSWDwaLEyshEKSZE.htm)|K'nonna|K'nonna|libre|
