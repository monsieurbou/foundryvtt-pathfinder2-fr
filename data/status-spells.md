# État de la traduction (spells)

 * **libre**: 1016
 * **officielle**: 238
 * **changé**: 288


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[common-01-6ZIKB0151LUR19Rw.htm](spells/common-01-6ZIKB0151LUR19Rw.htm)|Ill Omen|Mauvais présage|changé|
|[common-01-9WGeBwIIbbUuWKq0.htm](spells/common-01-9WGeBwIIbbUuWKq0.htm)|Summon Undead|Convocation de mort-vivant|changé|
|[common-01-AfOpnnwdZwHi2Tnc.htm](spells/common-01-AfOpnnwdZwHi2Tnc.htm)|Protect Companion|Protéger le compagnon|changé|
|[common-01-B0FZLkoHsiRgw7gv.htm](spells/common-01-B0FZLkoHsiRgw7gv.htm)|Summon Lesser Servitor|Convocation de serviteur inférieur|changé|
|[common-01-dgCH2E0gMLMUgyFl.htm](spells/common-01-dgCH2E0gMLMUgyFl.htm)|Shockwave|Onde de choc|changé|
|[common-01-dINQzhqGmIsqGMUY.htm](spells/common-01-dINQzhqGmIsqGMUY.htm)|Mending|Réparation|changé|
|[common-01-EUMjrJJwSgsqNidi.htm](spells/common-01-EUMjrJJwSgsqNidi.htm)|Anticipate Peril|Anticipation du danger|changé|
|[common-01-F1nlmqOIucch3Cmt.htm](spells/common-01-F1nlmqOIucch3Cmt.htm)|Pet Cache|Cachette du familier|changé|
|[common-01-hs7h8f4Z1ZNdUt3s.htm](spells/common-01-hs7h8f4Z1ZNdUt3s.htm)|Summon Fey|Convocation de fée|changé|
|[common-01-jSRAyd57kd4WZ4yE.htm](spells/common-01-jSRAyd57kd4WZ4yE.htm)|Summon Plant or Fungus|Convocation de plante ou de champignon|changé|
|[common-01-lKcsmeOrgHtK4xQa.htm](spells/common-01-lKcsmeOrgHtK4xQa.htm)|Summon Construct|Convocation de créature artificielle|changé|
|[common-01-RA7VKcen3p56rVyZ.htm](spells/common-01-RA7VKcen3p56rVyZ.htm)|Forbidding Ward|Sceau d'interdiction|changé|
|[common-01-rVANhQgB8Uqi9PTl.htm](spells/common-01-rVANhQgB8Uqi9PTl.htm)|Animate Rope|Animation de corde|changé|
|[common-01-T90ij2uu6ZaBaSXV.htm](spells/common-01-T90ij2uu6ZaBaSXV.htm)|Lament|Lamentation|changé|
|[common-01-W0YqtSVwfFImGgdK.htm](spells/common-01-W0YqtSVwfFImGgdK.htm)|Fashionista|Fashionista|changé|
|[common-01-WJOQryAODgYmrL6g.htm](spells/common-01-WJOQryAODgYmrL6g.htm)|Imprint Message|Implanter un message|changé|
|[common-02-2ZdHjnpEQJuqOYSG.htm](spells/common-02-2ZdHjnpEQJuqOYSG.htm)|Floating Flame|Flamme flottante|changé|
|[common-02-3ehSrqTAm7IPqbIZ.htm](spells/common-02-3ehSrqTAm7IPqbIZ.htm)|Spirit Sense|Perception des esprits|changé|
|[common-02-4GE2ZdODgIQtg51c.htm](spells/common-02-4GE2ZdODgIQtg51c.htm)|Darkness|Ténèbres|changé|
|[common-02-b515AZlB0sridKSq.htm](spells/common-02-b515AZlB0sridKSq.htm)|Calm|Apaisement|changé|
|[common-02-bgX4Zfhavahu8lyN.htm](spells/common-02-bgX4Zfhavahu8lyN.htm)|Burrow Ward|Protection contre les fouisseurs|changé|
|[common-02-EDwakYQTS1t6XHD4.htm](spells/common-02-EDwakYQTS1t6XHD4.htm)|Umbral Extraction|Extraction ombrale|changé|
|[common-02-Fq9yCbqI2RDt6Orw.htm](spells/common-02-Fq9yCbqI2RDt6Orw.htm)|Spiritual Weapon|Arme spirituelle|changé|
|[common-02-kcYCWMd1NVCJ5Zbu.htm](spells/common-02-kcYCWMd1NVCJ5Zbu.htm)|Helpful Wood Spirits|Esprits de bois amicaux|changé|
|[common-02-lpT6LotUaQPfinjj.htm](spells/common-02-lpT6LotUaQPfinjj.htm)|Summon Elemental|Convocation d'élémentaire|changé|
|[common-02-mMgLHTLHjjGa206q.htm](spells/common-02-mMgLHTLHjjGa206q.htm)|Thundering Dominance|Domination tonitruante|changé|
|[common-02-mS3p6BR3LcG1WmHs.htm](spells/common-02-mS3p6BR3LcG1WmHs.htm)|Pave Ground|Paver le sol|changé|
|[common-02-OhgfPzB5gymZ0IZM.htm](spells/common-02-OhgfPzB5gymZ0IZM.htm)|Timely Tutor|Tuteur opportun|changé|
|[common-02-SnaLVgxZ9ryUFmUr.htm](spells/common-02-SnaLVgxZ9ryUFmUr.htm)|Restoration|Restauration|changé|
|[common-02-WfINystwLv9ohYpS.htm](spells/common-02-WfINystwLv9ohYpS.htm)|Phantom Crowd|Foule fantomatique|changé|
|[common-02-WPu3UE3kTXSLqO40.htm](spells/common-02-WPu3UE3kTXSLqO40.htm)|Spiritual Armament|Arsenal spirituel|changé|
|[common-02-WqPhJNzLa8vSjrH6.htm](spells/common-02-WqPhJNzLa8vSjrH6.htm)|Animated Assault|Assaut animé|changé|
|[common-02-xRgU9rrhmGAgG4Rc.htm](spells/common-02-xRgU9rrhmGAgG4Rc.htm)|Peaceful Rest|Reposer en paix|changé|
|[common-02-ynm8JIU3sc3qUMpa.htm](spells/common-02-ynm8JIU3sc3qUMpa.htm)|Everlight|Lumière continue|changé|
|[common-03-0JWyMwVnLxX9CDYQ.htm](spells/common-03-0JWyMwVnLxX9CDYQ.htm)|Rouse Skeletons|Réveil de squelettes|changé|
|[common-03-28kgh0JzBO6pt38C.htm](spells/common-03-28kgh0JzBO6pt38C.htm)|Focusing Hum|Mantra de concentration|changé|
|[common-03-9iJaD4S7ptUeq5vO.htm](spells/common-03-9iJaD4S7ptUeq5vO.htm)|Positive Attunement|Harmonisation positive|changé|
|[common-03-AMEu5zzLN7uCX645.htm](spells/common-03-AMEu5zzLN7uCX645.htm)|Ghostly Weapon|Arme fantomatique|changé|
|[common-03-HcIAQZjNXHemoXSU.htm](spells/common-03-HcIAQZjNXHemoXSU.htm)|Shifting Sand|Sable mouvant|changé|
|[common-03-IFuEzfmmWyNwVbhY.htm](spells/common-03-IFuEzfmmWyNwVbhY.htm)|Safe Passage|Passage sûr|changé|
|[common-03-k1apwY0ROoMKlvLt.htm](spells/common-03-k1apwY0ROoMKlvLt.htm)|Scrying Ripples|Ondulations scrutatrices|changé|
|[common-03-K2WpC3FFoHrqX9Q5.htm](spells/common-03-K2WpC3FFoHrqX9Q5.htm)|Hypnotize|Hypnotiser|changé|
|[common-03-oUDNCArkQTdhllxD.htm](spells/common-03-oUDNCArkQTdhllxD.htm)|Aqueous Orb|Orbe aqueux|changé|
|[common-03-sbTxe4CGP4tn6y51.htm](spells/common-03-sbTxe4CGP4tn6y51.htm)|Lashing Rope|Corde fouettante|changé|
|[common-03-tFKJCPvOQZxKq6ON.htm](spells/common-03-tFKJCPvOQZxKq6ON.htm)|Mad Monkeys|Singes fous|changé|
|[common-04-2RhZkHNv8ajq0yLq.htm](spells/common-04-2RhZkHNv8ajq0yLq.htm)|Fallow Field|Champ inactif|changé|
|[common-04-avEutOjF2ox947fp.htm](spells/common-04-avEutOjF2ox947fp.htm)|Sliding Blocks|Blocs coulissants|changé|
|[common-04-bVvGpSOg2WaTUItS.htm](spells/common-04-bVvGpSOg2WaTUItS.htm)|Cinder Swarm|Nuée de cendres|changé|
|[common-04-D6BcAoWFxHYTKwVZ.htm](spells/common-04-D6BcAoWFxHYTKwVZ.htm)|Morass of Ages|Épreuve des âges|changé|
|[common-04-eCniO6INHNfc9Svr.htm](spells/common-04-eCniO6INHNfc9Svr.htm)|Overflowing Sorrow|Chagrin accablant|changé|
|[common-04-gzvRDpM6EvcfYHeu.htm](spells/common-04-gzvRDpM6EvcfYHeu.htm)|Tireless Worker|Travailleur infatigable|changé|
|[common-04-juzb53CW6Uzz5pFY.htm](spells/common-04-juzb53CW6Uzz5pFY.htm)|Umbral Graft|Greffe ombrale|changé|
|[common-04-kCgkreCT6g0dipMd.htm](spells/common-04-kCgkreCT6g0dipMd.htm)|Murderous Vine|Liane meurtrière|changé|
|[common-04-kHyjQbibRGPNCixx.htm](spells/common-04-kHyjQbibRGPNCixx.htm)|Ice Storm|Tempête de glace|changé|
|[common-04-ma3sndEAZdz0Cy2H.htm](spells/common-04-ma3sndEAZdz0Cy2H.htm)|Mercurial Stride|Démarche mercuriale|changé|
|[common-04-OmhnoYOzFhrp6rXv.htm](spells/common-04-OmhnoYOzFhrp6rXv.htm)|Waking Dream|Rêve éveillé|changé|
|[common-04-PtX16vbWzDehj8qc.htm](spells/common-04-PtX16vbWzDehj8qc.htm)|Pernicious Poltergeist|Poltergeist pernicieux|changé|
|[common-04-QE9f3OxvvBThymD4.htm](spells/common-04-QE9f3OxvvBThymD4.htm)|Ectoplasmic Interstice|Faille ectoplasmique|changé|
|[common-04-Wpgt5TYcTHLDei6J.htm](spells/common-04-Wpgt5TYcTHLDei6J.htm)|Grasping Earth|Terre accrocheuse|changé|
|[common-04-yy2K51kK3a60rRIe.htm](spells/common-04-yy2K51kK3a60rRIe.htm)|Rust Cloud|Nuage de rouille|changé|
|[common-05-29ytKctjg7qSW2ff.htm](spells/common-05-29ytKctjg7qSW2ff.htm)|Summon Fiend|Convocation de fiélon|changé|
|[common-05-9AZEObUdLI2fwmPl.htm](spells/common-05-9AZEObUdLI2fwmPl.htm)|Freezing Rain|Pluie glaciale|changé|
|[common-05-ba12fO37w7O37gim.htm](spells/common-05-ba12fO37w7O37gim.htm)|Summon Axiom|Convocation de l'axiome|changé|
|[common-05-CmZCq4htcZ6W0TKk.htm](spells/common-05-CmZCq4htcZ6W0TKk.htm)|Mirror Malefactors|Malfaisants miroirs|changé|
|[common-05-e9UJoVYUd5kJWUpi.htm](spells/common-05-e9UJoVYUd5kJWUpi.htm)|Summon Giant|Convocation de géant|changé|
|[common-05-forsqeofEszBNtLq.htm](spells/common-05-forsqeofEszBNtLq.htm)|Chromatic Wall|Mur chromatique|changé|
|[common-05-ftZ750SD2iIJKIy3.htm](spells/common-05-ftZ750SD2iIJKIy3.htm)|Glimmer of Charm|Lueur de charme|changé|
|[common-05-hghGRzOSzEl4UXdS.htm](spells/common-05-hghGRzOSzEl4UXdS.htm)|Invoke Spirits|Invoquer les esprits|changé|
|[common-05-i1TvBID5QLyXrUCa.htm](spells/common-05-i1TvBID5QLyXrUCa.htm)|Summon Entity|Convocation d'entité|changé|
|[common-05-jQdm301h6e8hIY4U.htm](spells/common-05-jQdm301h6e8hIY4U.htm)|Spiritual Guardian|Gardien spirituel|changé|
|[common-05-JyT346VmGtRLsDnV.htm](spells/common-05-JyT346VmGtRLsDnV.htm)|Lightning Storm|Tempête d'éclairs|changé|
|[common-05-kghwmH3tQjMIhdH1.htm](spells/common-05-kghwmH3tQjMIhdH1.htm)|Summon Dragon|Convocation de dragon|changé|
|[common-05-lTDixrrNKaCvLKwX.htm](spells/common-05-lTDixrrNKaCvLKwX.htm)|Summon Celestial|Convocation de céleste|changé|
|[common-05-n8ckecJpatSBEp7M.htm](spells/common-05-n8ckecJpatSBEp7M.htm)|Summon Anarch|Convocation de l'anarchie|changé|
|[common-05-rMGkk23qWvxTmgA8.htm](spells/common-05-rMGkk23qWvxTmgA8.htm)|Forceful Hand|Main de force|changé|
|[common-05-tcwT97RWKxsJiefG.htm](spells/common-05-tcwT97RWKxsJiefG.htm)|Shadow Siphon|Siphon d'ombre|changé|
|[common-05-tpLTLbJUrYcMWGld.htm](spells/common-05-tpLTLbJUrYcMWGld.htm)|Telekinetic Haul|Transport télékinésique|changé|
|[common-05-vJuaxTd6q11OjGqA.htm](spells/common-05-vJuaxTd6q11OjGqA.htm)|Abyssal Plague|Peste chthonienne|changé|
|[common-05-YWuyjhWbdoTiA1pw.htm](spells/common-05-YWuyjhWbdoTiA1pw.htm)|Temporary Glyph|Glyphe temporaire|changé|
|[common-05-ZbEHglw5tkJ3grQZ.htm](spells/common-05-ZbEHglw5tkJ3grQZ.htm)|Summon Monitor|Convocation de veilleur|changé|
|[common-05-zfn5RqAdF63neqpP.htm](spells/common-05-zfn5RqAdF63neqpP.htm)|Control Water|Contrôle de l'eau|changé|
|[common-06-7idCEYHnPpVNL9vX.htm](spells/common-06-7idCEYHnPpVNL9vX.htm)|Phantom Orchestra|Orchestre fantôme|changé|
|[common-06-Cpj05laa7ogqGwS3.htm](spells/common-06-Cpj05laa7ogqGwS3.htm)|Elemental Confluence|Confluence élémentaire|changé|
|[common-06-g2V4ID0pl1ZGAxZj.htm](spells/common-06-g2V4ID0pl1ZGAxZj.htm)|Vitrifying Blast|Déflagration vitrifiante|changé|
|[common-06-R5r4vtmlvOA8hDiF.htm](spells/common-06-R5r4vtmlvOA8hDiF.htm)|Frost Pillar|Pilier de givre|changé|
|[common-06-r7ihOgKv19eJQnik.htm](spells/common-06-r7ihOgKv19eJQnik.htm)|Disintegrate|Désintégration|changé|
|[common-06-RQjSQVZRG497cJhX.htm](spells/common-06-RQjSQVZRG497cJhX.htm)|Vibrant Pattern|Motif éclatant|changé|
|[common-06-uhEjKSFdEhXzszh6.htm](spells/common-06-uhEjKSFdEhXzszh6.htm)|Poltergeist's Fury|Furie du poltergeist|changé|
|[common-06-WPXzPl7YbMEIGWfi.htm](spells/common-06-WPXzPl7YbMEIGWfi.htm)|Mislead|Double illusoire|changé|
|[common-06-x5rGOmhDRDVQPrnW.htm](spells/common-06-x5rGOmhDRDVQPrnW.htm)|Field of Life|Champ de vie|changé|
|[common-06-ZMvplR106Jxl7B15.htm](spells/common-06-ZMvplR106Jxl7B15.htm)|Awaken Entropy|Entropie éveillée|changé|
|[common-07-0873MWM0qKDDv81O.htm](spells/common-07-0873MWM0qKDDv81O.htm)|Project Image|Projection d'image|changé|
|[common-07-0EzLXIpPPH0LOKqt.htm](spells/common-07-0EzLXIpPPH0LOKqt.htm)|Hungry Depths|Profondeurs affamés|changé|
|[common-07-73rToy0v5Ra9NvL6.htm](spells/common-07-73rToy0v5Ra9NvL6.htm)|Duplicate Foe|Dupliquer l'ennemi|changé|
|[common-07-HES5jvGiNZZnJycK.htm](spells/common-07-HES5jvGiNZZnJycK.htm)|Force Cage|Prison de force|changé|
|[common-07-kk7JKox6MdGAWmCH.htm](spells/common-07-kk7JKox6MdGAWmCH.htm)|Vacuum|Vide|changé|
|[common-07-smiVuoFMSgY2FTOO.htm](spells/common-07-smiVuoFMSgY2FTOO.htm)|Dancing Fountain|Fontaine dansante|changé|
|[common-07-WG91Z5TiR6oO5FOw.htm](spells/common-07-WG91Z5TiR6oO5FOw.htm)|Contingency|Contingence|changé|
|[common-08-0fjz8qc9NfkmWmJZ.htm](spells/common-08-0fjz8qc9NfkmWmJZ.htm)|Whirlpool|Tourbillon|changé|
|[common-08-8hKW4mWQyLnkHVta.htm](spells/common-08-8hKW4mWQyLnkHVta.htm)|Whirlwind|Tourbillon de vent|changé|
|[common-08-A0sMo1L0271yLeDA.htm](spells/common-08-A0sMo1L0271yLeDA.htm)|Clone Companion|Compagnon cloné|changé|
|[common-08-iL6TujgTCtRRa0Y0.htm](spells/common-08-iL6TujgTCtRRa0Y0.htm)|Prismatic Wall|Mur prismatique|changé|
|[common-08-kIRWUBxocERjIBni.htm](spells/common-08-kIRWUBxocERjIBni.htm)|Summon Deific Herald|Convocation de héraut divin|changé|
|[common-08-nsQvjNyg4Whw2mek.htm](spells/common-08-nsQvjNyg4Whw2mek.htm)|Divine Aura|Aura divine|changé|
|[common-08-uEyfLoFQsRKBRIcB.htm](spells/common-08-uEyfLoFQsRKBRIcB.htm)|Scintillating Pattern|Motif scintillant|changé|
|[common-08-wi405lBjPcbF1DeR.htm](spells/common-08-wi405lBjPcbF1DeR.htm)|Punishing Winds|Vents punitifs|changé|
|[common-09-4WS7HrFjwNvTn8T2.htm](spells/common-09-4WS7HrFjwNvTn8T2.htm)|Implosion|Implosion|changé|
|[common-09-AsRd1gNRSkHDq2Jx.htm](spells/common-09-AsRd1gNRSkHDq2Jx.htm)|Magnetic Dominion|Dominion magnétique|changé|
|[common-09-drmvQJETA3WZzXyw.htm](spells/common-09-drmvQJETA3WZzXyw.htm)|Voracious Gestalt|Gestalt vorace|changé|
|[common-09-FmNDwqMEjeTEGPrY.htm](spells/common-09-FmNDwqMEjeTEGPrY.htm)|Unfathomable Song|Chant énigmatique|changé|
|[common-09-r4HLQcYwB62bTayl.htm](spells/common-09-r4HLQcYwB62bTayl.htm)|Storm of Vengeance|Tempête vengeresse|changé|
|[common-09-yLJROsQtyrPIKcDx.htm](spells/common-09-yLJROsQtyrPIKcDx.htm)|Wrathful Storm|Tempête courroucée|changé|
|[common-10-h8zxY9hTeHtWsBVW.htm](spells/common-10-h8zxY9hTeHtWsBVW.htm)|Alter Reality|Altérer la réalité|changé|
|[common-10-HpIJTVqgXorH9X0L.htm](spells/common-10-HpIJTVqgXorH9X0L.htm)|Revival|Vivifier|changé|
|[common-10-KRcccPeNZOZ5Nweh.htm](spells/common-10-KRcccPeNZOZ5Nweh.htm)|Nullify|Annuler|changé|
|[common-10-MLNeD5sAunV0E23j.htm](spells/common-10-MLNeD5sAunV0E23j.htm)|Primal Phenomenon|Phénomène primordial|changé|
|[common-10-YfJTXyVGzLhM6V8U.htm](spells/common-10-YfJTXyVGzLhM6V8U.htm)|Miracle|Miracle|changé|
|[rare-01-09C2wvWZLCxwjINk.htm](spells/rare-01-09C2wvWZLCxwjINk.htm)|Invoke True Name|Invoquer le nom véritable|changé|
|[rare-01-BItahht2hEHvR9Bt.htm](spells/rare-01-BItahht2hEHvR9Bt.htm)|Buzzing Bites|Piqûres bourdonnantes|changé|
|[rare-01-qjUcAMgcSLIamjEq.htm](spells/rare-01-qjUcAMgcSLIamjEq.htm)|String of Fate|Fil du destin|changé|
|[rare-01-YNAthsgsJjQIXbc8.htm](spells/rare-01-YNAthsgsJjQIXbc8.htm)|Pact Broker|Briseur de pacte|changé|
|[rare-02-FQZaQXiKtHdVjSc5.htm](spells/rare-02-FQZaQXiKtHdVjSc5.htm)|Regale the Lost Ones|Régaler les perdus|changé|
|[rare-02-INEAWirvb8c1eAFD.htm](spells/rare-02-INEAWirvb8c1eAFD.htm)|Last Night's Vigil|Veille de la Dernière nuit|changé|
|[rare-02-wAJ8mXPYbyAg0qWX.htm](spells/rare-02-wAJ8mXPYbyAg0qWX.htm)|Sweetest Solstice|Solstice plus sucré|changé|
|[rare-02-WRmh1aKEo0dI0NJs.htm](spells/rare-02-WRmh1aKEo0dI0NJs.htm)|Corpse Communion|Communion de cadavre|changé|
|[rare-03-AkF4yFcSCdhoULyZ.htm](spells/rare-03-AkF4yFcSCdhoULyZ.htm)|Awaken Portal|Éveiller un portail|changé|
|[rare-03-IDkEuq5jLEDhJ31C.htm](spells/rare-03-IDkEuq5jLEDhJ31C.htm)|Cyclone Rondo|Ronde cyclonique|changé|
|[rare-03-ksJFGSGlNpUhwcsp.htm](spells/rare-03-ksJFGSGlNpUhwcsp.htm)|Ransack the Night|Saccager la nuit|changé|
|[rare-03-oYBIs8MICYkFwtXD.htm](spells/rare-03-oYBIs8MICYkFwtXD.htm)|Infectious Ennui|Ennui contagieux|changé|
|[rare-04-11P3KgDTMIeQIJD7.htm](spells/rare-04-11P3KgDTMIeQIJD7.htm)|Dawnflower's Light|Lumière de la Fleur de l'aube|changé|
|[rare-04-2TVc8NabmGpc092W.htm](spells/rare-04-2TVc8NabmGpc092W.htm)|Life's Fresh Bloom|Floraison de la vie|changé|
|[rare-04-a0MfDnUa33oWM74s.htm](spells/rare-04-a0MfDnUa33oWM74s.htm)|Tomorrow's Dawn|Aube de demain|changé|
|[rare-04-GabRQPc9qD86jDGI.htm](spells/rare-04-GabRQPc9qD86jDGI.htm)|Sacred Nimbus|Nimbus sacré|changé|
|[rare-04-IKb9EOfsrhScO3GO.htm](spells/rare-04-IKb9EOfsrhScO3GO.htm)|Phantasmal Protagonist|Protagoniste fantasmatique|changé|
|[rare-04-UVLiBXLLAoWhUU4R.htm](spells/rare-04-UVLiBXLLAoWhUU4R.htm)|Stonesense|Pierreception|changé|
|[rare-05-1aXX52MnVbBvT9S7.htm](spells/rare-05-1aXX52MnVbBvT9S7.htm)|Establish Nexus|Établir un nexus|changé|
|[rare-05-Pp88ueeq4AlkaL6g.htm](spells/rare-05-Pp88ueeq4AlkaL6g.htm)|Construct Mindscape|Paysage mental construit|changé|
|[rare-05-RWwiFnjxoJXn7H6D.htm](spells/rare-05-RWwiFnjxoJXn7H6D.htm)|Mind Swap|Échange d'esprit|changé|
|[rare-06-fRUxp4G9kG816XAt.htm](spells/rare-06-fRUxp4G9kG816XAt.htm)|Lure Dream|Attirer le rêve|changé|
|[rare-06-NeyyAPwa639WjYGG.htm](spells/rare-06-NeyyAPwa639WjYGG.htm)|Asmodean Wager|Pari asmodéen|changé|
|[rare-06-UJmKPm1FC6pf6txP.htm](spells/rare-06-UJmKPm1FC6pf6txP.htm)|Halcyon Infusion|Imprégnation paisible|changé|
|[rare-07-v8VnSMzaSYwkq6c7.htm](spells/rare-07-v8VnSMzaSYwkq6c7.htm)|Return To Essence|Retour à l'énergie|changé|
|[rare-07-XnpRRUVAVoY6z88H.htm](spells/rare-07-XnpRRUVAVoY6z88H.htm)|Empower Ley Line|Renforcer la ligne tellurique|changé|
|[rare-08-bRW61IbjaFea0wcv.htm](spells/rare-08-bRW61IbjaFea0wcv.htm)|Bathe in Blood|Baignade de sang|changé|
|[rare-08-My7FvAoLYgGDDBzy.htm](spells/rare-08-My7FvAoLYgGDDBzy.htm)|Antimagic Field|Champ d'antimagie|changé|
|[rare-08-TYydobaLN4U7Ol3M.htm](spells/rare-08-TYydobaLN4U7Ol3M.htm)|Unfettered Mark|Marque affranchie|changé|
|[rare-09-nA0XlPsnMNrQMpio.htm](spells/rare-09-nA0XlPsnMNrQMpio.htm)|Fantastic Facade|Façade fantastique|changé|
|[rare-09-T5Mt4jXFuh14uREv.htm](spells/rare-09-T5Mt4jXFuh14uREv.htm)|Divinity Leech|Sangsue de la divinité|changé|
|[uncommon-01-30BBep9U4BDV0EgQ.htm](spells/uncommon-01-30BBep9U4BDV0EgQ.htm)|Infernal Pact|Pacte infernal|changé|
|[uncommon-01-6nPc5CPRUbSSRDP1.htm](spells/uncommon-01-6nPc5CPRUbSSRDP1.htm)|Create Mycoguardian|Création de gardien fongique|changé|
|[uncommon-01-aEitTTb9PnOyidRf.htm](spells/uncommon-01-aEitTTb9PnOyidRf.htm)|Needle of Vengeance|Aiguillon de la vengeance|changé|
|[uncommon-01-aF7RiG7c8GzSQLYt.htm](spells/uncommon-01-aF7RiG7c8GzSQLYt.htm)|Word of Truth|Discours de vérité|changé|
|[uncommon-01-cE7PRAX8Up7fmYef.htm](spells/uncommon-01-cE7PRAX8Up7fmYef.htm)|Shroud of Night|Voile de nuit|changé|
|[uncommon-01-cNnHV97gPqxJ3Rrr.htm](spells/uncommon-01-cNnHV97gPqxJ3Rrr.htm)|Glimpse Weakness|Aperçu des faiblesses|changé|
|[uncommon-01-D8cWhrzcsd43OlIX.htm](spells/uncommon-01-D8cWhrzcsd43OlIX.htm)|Div Pact|Pacte du div|changé|
|[uncommon-01-dFejDNEmVj3CwYLL.htm](spells/uncommon-01-dFejDNEmVj3CwYLL.htm)|Blood Ward|Protection du sang|changé|
|[uncommon-01-dqaCLzINHBiKjh4J.htm](spells/uncommon-01-dqaCLzINHBiKjh4J.htm)|Call to Arms|Appel aux armes|changé|
|[uncommon-01-EeEgzWYcAY7ZQkS6.htm](spells/uncommon-01-EeEgzWYcAY7ZQkS6.htm)|Synchronize Steps|Pas synchronisés|changé|
|[uncommon-01-f45JpY7Ph2cAJGW2.htm](spells/uncommon-01-f45JpY7Ph2cAJGW2.htm)|Evil Eye|Mauvais oeil|changé|
|[uncommon-01-f9uqHnNBMU0774SF.htm](spells/uncommon-01-f9uqHnNBMU0774SF.htm)|Elemental Betrayal|Trahison élémentaire|changé|
|[uncommon-01-G0T1xv1FoZ23Jxvt.htm](spells/uncommon-01-G0T1xv1FoZ23Jxvt.htm)|Nudge Fate|Coup de pouce au destin|changé|
|[uncommon-01-GdN5YQE47gd79k7X.htm](spells/uncommon-01-GdN5YQE47gd79k7X.htm)|Wilding Word|Paroles sauvages|changé|
|[uncommon-01-GeUbPvwdZ4B4l0up.htm](spells/uncommon-01-GeUbPvwdZ4B4l0up.htm)|Stoke the Heart|Enflammer les coeurs|changé|
|[uncommon-01-gSUQlTDYoLDGAsCP.htm](spells/uncommon-01-gSUQlTDYoLDGAsCP.htm)|Hymn of Healing|Hymne de guérison|changé|
|[uncommon-01-HGmBY8KjgLV97nUp.htm](spells/uncommon-01-HGmBY8KjgLV97nUp.htm)|Scouring Sand|Sable décapant|changé|
|[uncommon-01-ho1jSoYKrHUNnM90.htm](spells/uncommon-01-ho1jSoYKrHUNnM90.htm)|Tempest Surge|Onde de tempête|changé|
|[uncommon-01-jks2h5pMsm8pCi8e.htm](spells/uncommon-01-jks2h5pMsm8pCi8e.htm)|Wildfire|Feu sauvage|changé|
|[uncommon-01-joEruBVz31Uxczzq.htm](spells/uncommon-01-joEruBVz31Uxczzq.htm)|Angelic Messenger|Messager angélique|changé|
|[uncommon-01-lV8FkHZtzZu7Cy6j.htm](spells/uncommon-01-lV8FkHZtzZu7Cy6j.htm)|Evolution Surge|Flux d'évolution|changé|
|[uncommon-01-lZx8jZfKrMEtyGY0.htm](spells/uncommon-01-lZx8jZfKrMEtyGY0.htm)|Rejuvenating Flames|Flammes rajeunissantes|changé|
|[uncommon-01-m2i7N8tnqar3gZDZ.htm](spells/uncommon-01-m2i7N8tnqar3gZDZ.htm)|Cycle of Retribution|Cycle de représailles|changé|
|[uncommon-01-MraZBLJ4Be3ogmWL.htm](spells/uncommon-01-MraZBLJ4Be3ogmWL.htm)|Clinging Ice|Glace tenace|changé|
|[uncommon-01-NNoKWiWKqJkdD2ln.htm](spells/uncommon-01-NNoKWiWKqJkdD2ln.htm)|Veil of Dreams|Voile de rêves|changé|
|[uncommon-01-QjdvYC1QkpMaemoX.htm](spells/uncommon-01-QjdvYC1QkpMaemoX.htm)|Nudge the Odds|Renforcer les chances|changé|
|[uncommon-01-rOwOLlfVgLMg1FND.htm](spells/uncommon-01-rOwOLlfVgLMg1FND.htm)|Elemental Sentinel|Sentinelle élémentaire|changé|
|[uncommon-01-tsKnoBuBbKMXkiz5.htm](spells/uncommon-01-tsKnoBuBbKMXkiz5.htm)|Abyssal Pact|Pacte chthonien|changé|
|[uncommon-01-TYbCj4dgXDOZou9k.htm](spells/uncommon-01-TYbCj4dgXDOZou9k.htm)|Reinforce Eidolon|Renforcer l'eidolon|changé|
|[uncommon-01-u4FGIUQgruLjml7J.htm](spells/uncommon-01-u4FGIUQgruLjml7J.htm)|Magic's Vessel|Réceptacle magique|changé|
|[uncommon-01-Vpohy4XH1DaH95hT.htm](spells/uncommon-01-Vpohy4XH1DaH95hT.htm)|Daemonic Pact|Pacte daémoniaque|changé|
|[uncommon-01-W37iBXLsY2trJ1rS.htm](spells/uncommon-01-W37iBXLsY2trJ1rS.htm)|Weapon Surge|Arme améliorée|changé|
|[uncommon-01-w3uGXDVEdbLFZVO0.htm](spells/uncommon-01-w3uGXDVEdbLFZVO0.htm)|Angelic Halo|Halo angélique|changé|
|[uncommon-01-WOM9alxiTdp0HEVD.htm](spells/uncommon-01-WOM9alxiTdp0HEVD.htm)|Distortion Lens|Loupe de distorsion|changé|
|[uncommon-01-X8PSYw6WC2ePYSXd.htm](spells/uncommon-01-X8PSYw6WC2ePYSXd.htm)|Stumbling Curse|Malédiction titubante|changé|
|[uncommon-01-ZeftDoh0nFAXBAWY.htm](spells/uncommon-01-ZeftDoh0nFAXBAWY.htm)|Appearance of Wealth|Richesse apparente|changé|
|[uncommon-01-Zmh4ynfnCtwKeAYl.htm](spells/uncommon-01-Zmh4ynfnCtwKeAYl.htm)|Heal Animal|Guérison des animaux|changé|
|[uncommon-01-znMvKqcRDilIVMwA.htm](spells/uncommon-01-znMvKqcRDilIVMwA.htm)|Exchange Image|Échanger les visages|changé|
|[uncommon-02-29JyWqqZ0thCFr1C.htm](spells/uncommon-02-29JyWqqZ0thCFr1C.htm)|Mind Games|Jeux d'esprit|changé|
|[uncommon-02-5pwK2FZX6QwgtfqX.htm](spells/uncommon-02-5pwK2FZX6QwgtfqX.htm)|Inveigle|Envoûtement|changé|
|[uncommon-02-7OFKYR1VY6EXDuiR.htm](spells/uncommon-02-7OFKYR1VY6EXDuiR.htm)|Clawsong|Chant de la griffe|changé|
|[uncommon-02-8Hw3P6eurX1MYm7L.htm](spells/uncommon-02-8Hw3P6eurX1MYm7L.htm)|Dancing Shield|Bouclier dansant|changé|
|[uncommon-02-c3b6LdLlQDPngNIb.htm](spells/uncommon-02-c3b6LdLlQDPngNIb.htm)|Create Undead|Création de mort-vivant|changé|
|[uncommon-02-Dbd5W6G8U2vzWolN.htm](spells/uncommon-02-Dbd5W6G8U2vzWolN.htm)|Consecrate|Consécration|changé|
|[uncommon-02-dZV8nZUKRhGIr6g9.htm](spells/uncommon-02-dZV8nZUKRhGIr6g9.htm)|Heartbond|Lien du coeur|changé|
|[uncommon-02-eW7DqGEvU50CDHqc.htm](spells/uncommon-02-eW7DqGEvU50CDHqc.htm)|Pack Attack|Attaque en meute|changé|
|[uncommon-02-h6VoHgPC0JCTVzgP.htm](spells/uncommon-02-h6VoHgPC0JCTVzgP.htm)|Vicious Jealousy|Jalousie vicieuse|changé|
|[uncommon-02-Popa5umI3H33levx.htm](spells/uncommon-02-Popa5umI3H33levx.htm)|Rime Slick|Couche de givre|changé|
|[uncommon-02-sLzPzk7DJnfuORJ0.htm](spells/uncommon-02-sLzPzk7DJnfuORJ0.htm)|Animate Object|Animation d'objet|changé|
|[uncommon-02-v3vFzGazNSFEDdRB.htm](spells/uncommon-02-v3vFzGazNSFEDdRB.htm)|Radiant Field|Champ rayonnant|changé|
|[uncommon-02-w00PBRjENqJa7Vxu.htm](spells/uncommon-02-w00PBRjENqJa7Vxu.htm)|Rite of Repatriation|Rite de rapatriement|changé|
|[uncommon-02-Wt94cw03L77sbud7.htm](spells/uncommon-02-Wt94cw03L77sbud7.htm)|Breath of Drought|Souffle de sécheresse|changé|
|[uncommon-03-0chL1b4OFIZxpN3v.htm](spells/uncommon-03-0chL1b4OFIZxpN3v.htm)|Steal Shadow|Vol d'ombre|changé|
|[uncommon-03-5WM3WjshXgrkVCg6.htm](spells/uncommon-03-5WM3WjshXgrkVCg6.htm)|Beastmaster Trance|Transe du maître des bêtes|changé|
|[uncommon-03-82gDiXTKsCmdIF6Q.htm](spells/uncommon-03-82gDiXTKsCmdIF6Q.htm)|Invoke the Harrow|Invoquer le Tourment|changé|
|[uncommon-03-dL3A7H4LlRH26Imo.htm](spells/uncommon-03-dL3A7H4LlRH26Imo.htm)|Arcane Weaving|Tissage arcanique|changé|
|[uncommon-03-DLFupJuCfzyLCQcO.htm](spells/uncommon-03-DLFupJuCfzyLCQcO.htm)|Eyes of the Dead|Yeux de la Mort|changé|
|[uncommon-03-FrKPwgFxWIGMGgs4.htm](spells/uncommon-03-FrKPwgFxWIGMGgs4.htm)|Deceiver's Cloak|Cape du trompeur|changé|
|[uncommon-03-g4MAIQodRDVfNp1B.htm](spells/uncommon-03-g4MAIQodRDVfNp1B.htm)|Personal Blizzard|Blizzard personnel|changé|
|[uncommon-03-JqHAxsMUZ4Mr5bTr.htm](spells/uncommon-03-JqHAxsMUZ4Mr5bTr.htm)|Ghostly Shift|Transformation fantomatique|changé|
|[uncommon-03-kvZCQz4NoMAfjvif.htm](spells/uncommon-03-kvZCQz4NoMAfjvif.htm)|Gasping Marsh|Bouffée marécageuse|changé|
|[uncommon-03-L37RTc7K79OUpZ7X.htm](spells/uncommon-03-L37RTc7K79OUpZ7X.htm)|Interstellar Void|Vide intersidéral|changé|
|[uncommon-03-LTUaK3smfm5eDiFK.htm](spells/uncommon-03-LTUaK3smfm5eDiFK.htm)|Song of Marching|Chanson de marche|changé|
|[uncommon-03-Q1OWufw6dUiY8yEI.htm](spells/uncommon-03-Q1OWufw6dUiY8yEI.htm)|Shroud of Flame|Voile de flammes|changé|
|[uncommon-03-SxRVCc1Q2MtVuPMo.htm](spells/uncommon-03-SxRVCc1Q2MtVuPMo.htm)|Sign of Conviction|Signe de conviction|changé|
|[uncommon-03-TUbXnR4RAuYzRx1u.htm](spells/uncommon-03-TUbXnR4RAuYzRx1u.htm)|Pyrotechnics|Pyrotechnie|changé|
|[uncommon-03-vhMCd15ZwNJn0zen.htm](spells/uncommon-03-vhMCd15ZwNJn0zen.htm)|Malicious Shadow|Ombre malicieuse|changé|
|[uncommon-04-2WNFbvToMsEGxmeK.htm](spells/uncommon-04-2WNFbvToMsEGxmeK.htm)|Darkened Sight|Vision obscurcie|changé|
|[uncommon-04-8ifpNZkaxrbs3dBJ.htm](spells/uncommon-04-8ifpNZkaxrbs3dBJ.htm)|Perfected Body|Corps parfait|changé|
|[uncommon-04-APDrC83QljsyHenB.htm](spells/uncommon-04-APDrC83QljsyHenB.htm)|Sage's Curse|Malédiction du savant|changé|
|[uncommon-04-ba1RyDpwq6tfW8VM.htm](spells/uncommon-04-ba1RyDpwq6tfW8VM.htm)|Cloak of Light|Cape de lumière|changé|
|[uncommon-04-CHZQJg7O7991Vl4m.htm](spells/uncommon-04-CHZQJg7O7991Vl4m.htm)|Familiar Form|Forme de familier|changé|
|[uncommon-04-FCfd1zUrqPaLEtau.htm](spells/uncommon-04-FCfd1zUrqPaLEtau.htm)|Dirge of Remembrance|Chant funèbre du souvenir|changé|
|[uncommon-04-JOdOpbPDl7nqvJUm.htm](spells/uncommon-04-JOdOpbPDl7nqvJUm.htm)|Dispelling Globe|Globe de dissipation|changé|
|[uncommon-04-KPGGkyBFbKse7KpK.htm](spells/uncommon-04-KPGGkyBFbKse7KpK.htm)|Spiral of Horrors|Aura effroyable|changé|
|[uncommon-04-LrhTFHUtSS9ahogL.htm](spells/uncommon-04-LrhTFHUtSS9ahogL.htm)|Traveler's Transit|Voyageur en transit|changé|
|[uncommon-04-MTNlvZ0A9xY5sOg1.htm](spells/uncommon-04-MTNlvZ0A9xY5sOg1.htm)|Rest Eternal|Repos éternel|changé|
|[uncommon-04-nEXkk85BQu1cREa4.htm](spells/uncommon-04-nEXkk85BQu1cREa4.htm)|Garden of Death|Jardin mortel|changé|
|[uncommon-04-NOB92Wpn7jXvtyVW.htm](spells/uncommon-04-NOB92Wpn7jXvtyVW.htm)|Competitive Edge|Avantage du compétiteur|changé|
|[uncommon-04-Nun72GTmb31YqSKh.htm](spells/uncommon-04-Nun72GTmb31YqSKh.htm)|Invisibility Cloak|Cape d'invisibilité|changé|
|[uncommon-04-OsrtOeG0TvDNnEFH.htm](spells/uncommon-04-OsrtOeG0TvDNnEFH.htm)|Safeguard Secret|Secret bien gardé|changé|
|[uncommon-04-pLEhF3f7KRxFo0vp.htm](spells/uncommon-04-pLEhF3f7KRxFo0vp.htm)|Girzanje's March|Marche de Guirzanjé|changé|
|[uncommon-04-Up4zLmZDgciUDpIv.htm](spells/uncommon-04-Up4zLmZDgciUDpIv.htm)|Wood Walk|Démarche de bois|changé|
|[uncommon-04-vfHr1N8Rf2bBpdgn.htm](spells/uncommon-04-vfHr1N8Rf2bBpdgn.htm)|Elemental Tempest|Tempête élémentaire|changé|
|[uncommon-04-VGK9s6LCMMS027zP.htm](spells/uncommon-04-VGK9s6LCMMS027zP.htm)|Confront Selves|Se confronter à soi-même|changé|
|[uncommon-04-yfCykJ6cs0uUL79b.htm](spells/uncommon-04-yfCykJ6cs0uUL79b.htm)|Radiant Heart of Devotion|Coeur de dévotion radieux|changé|
|[uncommon-04-zu40ATlcCjtfWBnj.htm](spells/uncommon-04-zu40ATlcCjtfWBnj.htm)|Misty Memory|Mémoire brumeuse|changé|
|[uncommon-04-zvvHOQV78WKUB33l.htm](spells/uncommon-04-zvvHOQV78WKUB33l.htm)|Life Siphon|Siphon de vie|changé|
|[uncommon-05-9BGEf9Sv5rgNBCk0.htm](spells/uncommon-05-9BGEf9Sv5rgNBCk0.htm)|Dance of Darkness|Danse de ténèbres|changé|
|[uncommon-05-9BnhadUO8FMLmeZ3.htm](spells/uncommon-05-9BnhadUO8FMLmeZ3.htm)|Mind Probe|Sonde mentale|changé|
|[uncommon-05-Dby1eQ7y5dXBWlyc.htm](spells/uncommon-05-Dby1eQ7y5dXBWlyc.htm)|Contagious Idea|Idée contagieuse|changé|
|[uncommon-05-DdXKfIjDtORUtUvY.htm](spells/uncommon-05-DdXKfIjDtORUtUvY.htm)|Fey Glamour|Enchantement féerique|changé|
|[uncommon-05-h47yv6j6x1pUtzlr.htm](spells/uncommon-05-h47yv6j6x1pUtzlr.htm)|Arcane Countermeasure|Contremesure arcanique|changé|
|[uncommon-05-h7h0wMxu4WOpveQ3.htm](spells/uncommon-05-h7h0wMxu4WOpveQ3.htm)|Ritual Obstruction|Obstruction du rituel|changé|
|[uncommon-05-HMTloW1hvRFJ5Z2D.htm](spells/uncommon-05-HMTloW1hvRFJ5Z2D.htm)|Consuming Darkness|Obscurité dévorante|changé|
|[uncommon-05-nQS4vPm5zprqkzFZ.htm](spells/uncommon-05-nQS4vPm5zprqkzFZ.htm)|Curse of Death|Malédiction de mort|changé|
|[uncommon-05-pfFxj4NI5KwO119I.htm](spells/uncommon-05-pfFxj4NI5KwO119I.htm)|Blind Eye|Oeil aveugle|changé|
|[uncommon-05-qr0HOiiuqj5LKlDt.htm](spells/uncommon-05-qr0HOiiuqj5LKlDt.htm)|Pillars of Sand|Piliers de sable|changé|
|[uncommon-05-s4HTepjhQWV1yfBs.htm](spells/uncommon-05-s4HTepjhQWV1yfBs.htm)|Foresee the Path|Prévoir le chemin|changé|
|[uncommon-05-scTRIrTfXquVYHGw.htm](spells/uncommon-05-scTRIrTfXquVYHGw.htm)|Drop Dead|Raide mort|changé|
|[uncommon-05-SE3MddYAUyPKABuF.htm](spells/uncommon-05-SE3MddYAUyPKABuF.htm)|Infectious Comedy|Humour contagieux|changé|
|[uncommon-05-ViqzVEprQVzCXZ9f.htm](spells/uncommon-05-ViqzVEprQVzCXZ9f.htm)|Dancing Blade|Lame dansante|changé|
|[uncommon-05-VrhCpCIQB8PFax77.htm](spells/uncommon-05-VrhCpCIQB8PFax77.htm)|Spellmaster's Ward|Protection du Maître des sorts|changé|
|[uncommon-05-wykbu2KW9tMBRySr.htm](spells/uncommon-05-wykbu2KW9tMBRySr.htm)|Hologram Cage|Cage hologramme|changé|
|[uncommon-05-Z1MoMTcgFQiCI90t.htm](spells/uncommon-05-Z1MoMTcgFQiCI90t.htm)|Tesseract Tunnel|Tunnel tesseract|changé|
|[uncommon-06-2B0C22OuX9YrIJ5y.htm](spells/uncommon-06-2B0C22OuX9YrIJ5y.htm)|Ash Form|Forme de cendre|changé|
|[uncommon-06-4j0FQ1mkidBAXuQV.htm](spells/uncommon-06-4j0FQ1mkidBAXuQV.htm)|Heroic Feat|Don héroïque|changé|
|[uncommon-06-69L70wKfGDY66Mk9.htm](spells/uncommon-06-69L70wKfGDY66Mk9.htm)|Teleport|Téléportation|changé|
|[uncommon-06-7DN13ILADW2N9Z1t.htm](spells/uncommon-06-7DN13ILADW2N9Z1t.htm)|Commune|Communion|changé|
|[uncommon-06-9kOI14Jep97TzGO7.htm](spells/uncommon-06-9kOI14Jep97TzGO7.htm)|Life-Giving Form|Forme génératrice de vie|changé|
|[uncommon-06-B0Ng2VlhEJMuUXUH.htm](spells/uncommon-06-B0Ng2VlhEJMuUXUH.htm)|Bound in Death|Liés dans la mort|changé|
|[uncommon-06-B4dDkYsHFo1H0CIF.htm](spells/uncommon-06-B4dDkYsHFo1H0CIF.htm)|Awaken Animal|Éveil d'animal|changé|
|[uncommon-06-bynT1UKaDqr8dLNM.htm](spells/uncommon-06-bynT1UKaDqr8dLNM.htm)|Flaming Fusillade|Fusillade enflammée|changé|
|[uncommon-06-EOWh6VVcSjB3WPjX.htm](spells/uncommon-06-EOWh6VVcSjB3WPjX.htm)|Shadow Illusion|Illusion de l'ombre|changé|
|[uncommon-06-Er9XNUlL0wB0PM36.htm](spells/uncommon-06-Er9XNUlL0wB0PM36.htm)|Ward Domain|Protection de domaine|changé|
|[uncommon-06-g1dDUKsIrdlpdqy9.htm](spells/uncommon-06-g1dDUKsIrdlpdqy9.htm)|Ravenous Darkness|Ténèbres affamées|changé|
|[uncommon-06-gBJtUhewnihlqxm7.htm](spells/uncommon-06-gBJtUhewnihlqxm7.htm)|Rose's Thorns|Épines de rose|changé|
|[uncommon-06-mtxMpGWpwwWSbySj.htm](spells/uncommon-06-mtxMpGWpwwWSbySj.htm)|For Love, For Lightning|Pour l'amour, pour l'éclair|changé|
|[uncommon-06-P1iqzEIidRagUs7W.htm](spells/uncommon-06-P1iqzEIidRagUs7W.htm)|Zero Gravity|Gravité zéro|changé|
|[uncommon-06-qGWORxQ0aSsH2taf.htm](spells/uncommon-06-qGWORxQ0aSsH2taf.htm)|Suffocate|Suffocation|changé|
|[uncommon-06-r784cIz17eWujtQj.htm](spells/uncommon-06-r784cIz17eWujtQj.htm)|Scrying|Scrutation|changé|
|[uncommon-06-rTvNWcKNpOnGklGF.htm](spells/uncommon-06-rTvNWcKNpOnGklGF.htm)|Gray Shadow|Ombre grise|changé|
|[uncommon-06-VUMtDHr8CRwwr3Mj.htm](spells/uncommon-06-VUMtDHr8CRwwr3Mj.htm)|Aura of the Unremarkable|Aura quelconque|changé|
|[uncommon-06-xEjGEBvTfDJECSki.htm](spells/uncommon-06-xEjGEBvTfDJECSki.htm)|Ancestral Form|Forme ancestrale|changé|
|[uncommon-06-yUM5OYeMY8971b2S.htm](spells/uncommon-06-yUM5OYeMY8971b2S.htm)|Terminate Bloodline|Achever une lignée|changé|
|[uncommon-06-zhqnMOVPzVvWSUbC.htm](spells/uncommon-06-zhqnMOVPzVvWSUbC.htm)|Tempest Form|Forme tempêtueuse|changé|
|[uncommon-07-D2nPKbIS67m9199U.htm](spells/uncommon-07-D2nPKbIS67m9199U.htm)|Ethereal Jaunt|Forme éthérée|changé|
|[uncommon-07-EgkypvUZIZkx1UlQ.htm](spells/uncommon-07-EgkypvUZIZkx1UlQ.htm)|Blightburn Blast|Explosion de fléau brûlant|changé|
|[uncommon-07-oahqARSgOGDRybBQ.htm](spells/uncommon-07-oahqARSgOGDRybBQ.htm)|Control Sand|Contrôle du sable|changé|
|[uncommon-07-sFwoKj0TsacsmoWj.htm](spells/uncommon-07-sFwoKj0TsacsmoWj.htm)|Darklight|Lumière noire|changé|
|[uncommon-07-tYP8unoR0a5Dq9EA.htm](spells/uncommon-07-tYP8unoR0a5Dq9EA.htm)|Litany of Depravity|Litanie de dépravation|changé|
|[uncommon-07-Vw2CNwlRRKABsuZi.htm](spells/uncommon-07-Vw2CNwlRRKABsuZi.htm)|Entrancing Eyes|Yeux enchanteurs|changé|
|[uncommon-08-2CNqkt2s2IYkVnv6.htm](spells/uncommon-08-2CNqkt2s2IYkVnv6.htm)|Imprisonment|Emprisonnement|changé|
|[uncommon-08-4ddJSjC9Zz5DX0oG.htm](spells/uncommon-08-4ddJSjC9Zz5DX0oG.htm)|Freedom|Liberté|changé|
|[uncommon-08-oGV6YdpZLIG4G4gH.htm](spells/uncommon-08-oGV6YdpZLIG4G4gH.htm)|Impaling Briars|Ronces empaleuses|changé|
|[uncommon-09-XYhU3Wi94n1RKxTa.htm](spells/uncommon-09-XYhU3Wi94n1RKxTa.htm)|Storm Lord|Seigneur des tempêtes|changé|
|[uncommon-10-IGXGs9PlqUCvODcH.htm](spells/uncommon-10-IGXGs9PlqUCvODcH.htm)|Song of the Fallen|Chanson des morts au combat|changé|
|[uncommon-10-U13bC0tNgrlHoeTK.htm](spells/uncommon-10-U13bC0tNgrlHoeTK.htm)|Gate|Portail|changé|
|[unique-04-8Lk9K3tRnV68MQPW.htm](spells/unique-04-8Lk9K3tRnV68MQPW.htm)|Transmigrate|Transmigration|changé|
|[unique-05-aVqsE5OedOwdHQvF.htm](spells/unique-05-aVqsE5OedOwdHQvF.htm)|Summerland Spell|Sort estival|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[common-01-0cF9HvHzzWSbCFBP.htm](spells/common-01-0cF9HvHzzWSbCFBP.htm)|Empty Inside|Vide intérieur|libre|
|[common-01-0Dcd4iEXqCrkm4Jn.htm](spells/common-01-0Dcd4iEXqCrkm4Jn.htm)|Liberating Command|Ordre libérateur|libre|
|[common-01-0fKHBh5goe2eiFYL.htm](spells/common-01-0fKHBh5goe2eiFYL.htm)|Negate Aroma|Supprimer l'arôme|officielle|
|[common-01-0uRpypf1Hi7ahvTl.htm](spells/common-01-0uRpypf1Hi7ahvTl.htm)|Shattering Gem|Gemme explosive|libre|
|[common-01-0zU8CPejjQFnhZFI.htm](spells/common-01-0zU8CPejjQFnhZFI.htm)|Figment|Fantasme|libre|
|[common-01-1meVElIu1CEVYWkv.htm](spells/common-01-1meVElIu1CEVYWkv.htm)|Noxious Vapors|Vapeurs nocives|libre|
|[common-01-1Yoipc5jNcMehtEW.htm](spells/common-01-1Yoipc5jNcMehtEW.htm)|Slashing Gust|Bourrasque tranchante|libre|
|[common-01-2iQKhCQBijhj5Rf3.htm](spells/common-01-2iQKhCQBijhj5Rf3.htm)|Infuse Vitality|Infusion de vitalité|libre|
|[common-01-2oH5IufzdESuYxat.htm](spells/common-01-2oH5IufzdESuYxat.htm)|Illusory Object|Objet illusoire|libre|
|[common-01-3SJEBgXHolrSAFqD.htm](spells/common-01-3SJEBgXHolrSAFqD.htm)|Helpful Steps|Marches serviables|libre|
|[common-01-3wmX7htzOXiHLdAn.htm](spells/common-01-3wmX7htzOXiHLdAn.htm)|Delay Consequence|Conséquence retardée|libre|
|[common-01-4c1c6eNzU1PFGkAy.htm](spells/common-01-4c1c6eNzU1PFGkAy.htm)|Endure|Endurer|libre|
|[common-01-4gBIw4IDrSfFHik4.htm](spells/common-01-4gBIw4IDrSfFHik4.htm)|Daze|Hébétement|libre|
|[common-01-4k99nOJudmeNnBTF.htm](spells/common-01-4k99nOJudmeNnBTF.htm)|Shielded Arm|Bras protégé|libre|
|[common-01-4koZzrnMXhhosn0D.htm](spells/common-01-4koZzrnMXhhosn0D.htm)|Fear|Terreur|officielle|
|[common-01-4WAib3GichxLjp5p.htm](spells/common-01-4WAib3GichxLjp5p.htm)|Alarm|Alarme|libre|
|[common-01-4YnON9JHYqtLzccu.htm](spells/common-01-4YnON9JHYqtLzccu.htm)|Summon Animal|Convocation d'animal|libre|
|[common-01-4ZGte0i9YbLh4dRi.htm](spells/common-01-4ZGte0i9YbLh4dRi.htm)|Item Facade|Façade|officielle|
|[common-01-5gophZ4AOKW4VW27.htm](spells/common-01-5gophZ4AOKW4VW27.htm)|Phase Bolt|Carreau de phase|libre|
|[common-01-5LYi9Efs6cko4GGL.htm](spells/common-01-5LYi9Efs6cko4GGL.htm)|Object Reading|Lecture d'objet|libre|
|[common-01-5wjl0ZwEvvUh7sor.htm](spells/common-01-5wjl0ZwEvvUh7sor.htm)|Swarmsense|Sens de la nuée|libre|
|[common-01-60sgbuMWN0268dB7.htm](spells/common-01-60sgbuMWN0268dB7.htm)|Telekinetic Projectile|Projectile télékinésique|libre|
|[common-01-6DfLZBl8wKIV03Iq.htm](spells/common-01-6DfLZBl8wKIV03Iq.htm)|Ignition|Allumage|libre|
|[common-01-7CUgqHunmHfW2lC5.htm](spells/common-01-7CUgqHunmHfW2lC5.htm)|Parch|Dessécher|libre|
|[common-01-7tp97g0UCJ9wOrd5.htm](spells/common-01-7tp97g0UCJ9wOrd5.htm)|Withering Grasp|Poigne de flétrissement|libre|
|[common-01-7ZinJNzxq0XF0oMx.htm](spells/common-01-7ZinJNzxq0XF0oMx.htm)|Bane|Imprécation|libre|
|[common-01-8E97SA9KAWCNdXfO.htm](spells/common-01-8E97SA9KAWCNdXfO.htm)|Schadenfreude|Joie malsaine|libre|
|[common-01-8lZhUreL1bRk1v4Z.htm](spells/common-01-8lZhUreL1bRk1v4Z.htm)|Briny Bolt|Carreau de saumure|libre|
|[common-01-8TQiFzGf4feoHeH0.htm](spells/common-01-8TQiFzGf4feoHeH0.htm)|Chilling Spray|Aspersion de froid|libre|
|[common-01-8Uzc9WKqRr755S5d.htm](spells/common-01-8Uzc9WKqRr755S5d.htm)|Horizon Thunder Sphere|Sphère de tonnerre lointain|libre|
|[common-01-8xRzLhwGL7Dgy3EZ.htm](spells/common-01-8xRzLhwGL7Dgy3EZ.htm)|Sanctuary|Sanctuaire|officielle|
|[common-01-9I8mp7RkjeXbkYfx.htm](spells/common-01-9I8mp7RkjeXbkYfx.htm)|Timber|Tronc|libre|
|[common-01-9u6X9ykhzG11NK1n.htm](spells/common-01-9u6X9ykhzG11NK1n.htm)|Magic Stone|Pierre magique|libre|
|[common-01-aAbfKn8maGjJjk2W.htm](spells/common-01-aAbfKn8maGjJjk2W.htm)|Mystic Armor|Armure mystique|libre|
|[common-01-Aap7nGpC7neTWm78.htm](spells/common-01-Aap7nGpC7neTWm78.htm)|Animal Allies|Alliés animaux|libre|
|[common-01-aEM2cttJ2eYcLssW.htm](spells/common-01-aEM2cttJ2eYcLssW.htm)|Fleet Step|Pas rapide|libre|
|[common-01-aIHY2DArKFweIrpf.htm](spells/common-01-aIHY2DArKFweIrpf.htm)|Command|Injonction|officielle|
|[common-01-atlgGNI1E1Ox3O3a.htm](spells/common-01-atlgGNI1E1Ox3O3a.htm)|Ghost Sound|Son imaginaire|libre|
|[common-01-AUctDF2fqPZN2w4W.htm](spells/common-01-AUctDF2fqPZN2w4W.htm)|Sigil|Symbole|libre|
|[common-01-Azoh0BSoCASrA1lr.htm](spells/common-01-Azoh0BSoCASrA1lr.htm)|Lock|Verrouillage|libre|
|[common-01-b5BQbwmuBhgPXTyi.htm](spells/common-01-b5BQbwmuBhgPXTyi.htm)|Haunting Hymn|Hymne obsédant|libre|
|[common-01-b6AQvzs8EotmlK56.htm](spells/common-01-b6AQvzs8EotmlK56.htm)|Flourishing Flora|Flore florissante|libre|
|[common-01-buhP7vfetUxQJlIJ.htm](spells/common-01-buhP7vfetUxQJlIJ.htm)|Restyle|Relooking|libre|
|[common-01-BvNbDwFYaidKJG9j.htm](spells/common-01-BvNbDwFYaidKJG9j.htm)|Time Sense|Perception du temps|libre|
|[common-01-BX5UIAUarWG43Fg2.htm](spells/common-01-BX5UIAUarWG43Fg2.htm)|Root Reading|Lecture de racine|libre|
|[common-01-C2DwamKrW6QHoDQg.htm](spells/common-01-C2DwamKrW6QHoDQg.htm)|Deep Breath|Inspiration profonde|libre|
|[common-01-cgfl9Qr46bKaA59e.htm](spells/common-01-cgfl9Qr46bKaA59e.htm)|Glass Shield|Bouclier en verre|libre|
|[common-01-cJq5NarY0eOZN74A.htm](spells/common-01-cJq5NarY0eOZN74A.htm)|Share Burden|Partage du fardeau|libre|
|[common-01-cokeXkDHUAo4zHsw.htm](spells/common-01-cokeXkDHUAo4zHsw.htm)|Oathkeeper's Insignia|Insigne du gardien du serment|libre|
|[common-01-CxpFy4HJHf4ACbxF.htm](spells/common-01-CxpFy4HJHf4ACbxF.htm)|Putrefy Food and Drink|Corruption de la nourriture et de la boisson|libre|
|[common-01-D442XMADp01qJ7Cs.htm](spells/common-01-D442XMADp01qJ7Cs.htm)|Mindlink|Lien mental|officielle|
|[common-01-D7ZEhTNIDWDLC2J4.htm](spells/common-01-D7ZEhTNIDWDLC2J4.htm)|Puff of Poison|Bouffée de poison|libre|
|[common-01-dDiOnjcsBFbAvP6t.htm](spells/common-01-dDiOnjcsBFbAvP6t.htm)|Gale Blast|Coup de vent|libre|
|[common-01-DeNz6eAUlE0IE9U3.htm](spells/common-01-DeNz6eAUlE0IE9U3.htm)|Winter Bolt|Trait d'hiver|libre|
|[common-01-DgcSiOCR1uDXGaEA.htm](spells/common-01-DgcSiOCR1uDXGaEA.htm)|Synchronize|Synchroniser|libre|
|[common-01-DYdvMZ8G2LiSLVWw.htm](spells/common-01-DYdvMZ8G2LiSLVWw.htm)|Spider Sting|Morsure d'araignée|officielle|
|[common-01-EE7Q5BHIrfWNCPtT.htm](spells/common-01-EE7Q5BHIrfWNCPtT.htm)|Runic Body|Corps runique|libre|
|[common-01-EqdSKqr8Qj5TUhMR.htm](spells/common-01-EqdSKqr8Qj5TUhMR.htm)|Approximate|Approximer|libre|
|[common-01-eSL5hVT9gXrnRLtd.htm](spells/common-01-eSL5hVT9gXrnRLtd.htm)|Spout|Jaillissement|libre|
|[common-01-ET2BYfeNaHqfZIWs.htm](spells/common-01-ET2BYfeNaHqfZIWs.htm)|Instant Pottery|Poterie instantanée|libre|
|[common-01-eyKkLp8V55ydA64J.htm](spells/common-01-eyKkLp8V55ydA64J.htm)|Weaken Earth|Terre fragilisée|libre|
|[common-01-F1fQC9SarAkjNxpP.htm](spells/common-01-F1fQC9SarAkjNxpP.htm)|Gravitational Pull|Force gravitationnelle|libre|
|[common-01-F23T5tHPo3WsFiHW.htm](spells/common-01-F23T5tHPo3WsFiHW.htm)|Quick Sort|Tri rapide|libre|
|[common-01-f9m9DayyGy3meqUX.htm](spells/common-01-f9m9DayyGy3meqUX.htm)|Dehydrate|Déshydrater|libre|
|[common-01-fkUCvynklBGb4LaI.htm](spells/common-01-fkUCvynklBGb4LaI.htm)|Conductive Weapon|Arme conductrice|libre|
|[common-01-FWuLUKIbkJZjBiuA.htm](spells/common-01-FWuLUKIbkJZjBiuA.htm)|Eat Fire|Manger le feu|libre|
|[common-01-g1eY1vN44mgluE33.htm](spells/common-01-g1eY1vN44mgluE33.htm)|Charged Javelin|Javelot chargé|libre|
|[common-01-g8QqHpv2CWDwmIm1.htm](spells/common-01-g8QqHpv2CWDwmIm1.htm)|Gust of Wind|Bourrasque de vent|libre|
|[common-01-Gb7SeieEvd0pL2Eh.htm](spells/common-01-Gb7SeieEvd0pL2Eh.htm)|Sure Strike|Coup assuré|libre|
|[common-01-gfPjmG6Fe6D3MFjl.htm](spells/common-01-gfPjmG6Fe6D3MFjl.htm)|Pest Form|Forme de nuisible|officielle|
|[common-01-gISYsBFby1TiXfBt.htm](spells/common-01-gISYsBFby1TiXfBt.htm)|Acid Splash|Aspersion d'acide|libre|
|[common-01-gKKqvLohtrSJj3BM.htm](spells/common-01-gKKqvLohtrSJj3BM.htm)|Force Barrage|Salve de force|libre|
|[common-01-gMODOGamz88rgHuf.htm](spells/common-01-gMODOGamz88rgHuf.htm)|Protection|Protection|libre|
|[common-01-gpzpAAAJ1Lza2JVl.htm](spells/common-01-gpzpAAAJ1Lza2JVl.htm)|Detect Magic|Détection de la magie|libre|
|[common-01-GUnw9YXaW3YyaCAU.htm](spells/common-01-GUnw9YXaW3YyaCAU.htm)|Adapt Self|Auto-adaptation|officielle|
|[common-01-gYjPm7YwGtEa1oxh.htm](spells/common-01-gYjPm7YwGtEa1oxh.htm)|Ray of Frost|Rayon de givre|libre|
|[common-01-HokKyQl5g655xx9U.htm](spells/common-01-HokKyQl5g655xx9U.htm)|Interposing Earth|Terre interposée|libre|
|[common-01-i35dpZFI7jZcRoBo.htm](spells/common-01-i35dpZFI7jZcRoBo.htm)|Illusory Disguise|Déguisement illusoire|officielle|
|[common-01-idcBYnj4DUHL0IJt.htm](spells/common-01-idcBYnj4DUHL0IJt.htm)|Weave Wood|Tisser le bois|libre|
|[common-01-ifXNOhtmU4fKL68v.htm](spells/common-01-ifXNOhtmU4fKL68v.htm)|Redact|Corriger|libre|
|[common-01-IHjiWCouSacC5b3g.htm](spells/common-01-IHjiWCouSacC5b3g.htm)|Wooden Fists|Poings de bois|libre|
|[common-01-ILoXUDP84ru1hYLg.htm](spells/common-01-ILoXUDP84ru1hYLg.htm)|Buoyant Bubbles|Bulles de flottaison|libre|
|[common-01-iohRnd4gUCmJlh54.htm](spells/common-01-iohRnd4gUCmJlh54.htm)|Lose the Path|Mauvais chemin|libre|
|[common-01-IxhGEKl63R4QBvkj.htm](spells/common-01-IxhGEKl63R4QBvkj.htm)|Frostbite|Morsure du froid|libre|
|[common-01-iYRDFxeVpJ5KIjmr.htm](spells/common-01-iYRDFxeVpJ5KIjmr.htm)|Needle Darts|Fléchettes aiguilles|libre|
|[common-01-izcxFQFwf3woCnFs.htm](spells/common-01-izcxFQFwf3woCnFs.htm)|Guidance|Assistance surnaturelle|libre|
|[common-01-J7Y7tl0bbdz7TcCc.htm](spells/common-01-J7Y7tl0bbdz7TcCc.htm)|Enfeeble|Affaiblissement|libre|
|[common-01-jfVCuOpzC6mUrf6f.htm](spells/common-01-jfVCuOpzC6mUrf6f.htm)|Hydraulic Push|Poussée hydraulique|officielle|
|[common-01-jsWthW1Qy6tg3SwD.htm](spells/common-01-jsWthW1Qy6tg3SwD.htm)|Draw Ire|Attirer la colère|libre|
|[common-01-JUJYVbtNNFdvyrBS.htm](spells/common-01-JUJYVbtNNFdvyrBS.htm)|Necromancer's Generosity|Générosité du nécromant|libre|
|[common-01-k34hDOfIIMAxNL4a.htm](spells/common-01-k34hDOfIIMAxNL4a.htm)|Grim Tendrils|Sinistres volutes|libre|
|[common-01-K9gI08enGtmih5X1.htm](spells/common-01-K9gI08enGtmih5X1.htm)|Protector Tree|Arbre protecteur|libre|
|[common-01-kBhaPuzLUSwS6vVf.htm](spells/common-01-kBhaPuzLUSwS6vVf.htm)|Electric Arc|Arc électrique|libre|
|[common-01-kcelf6IHl3L9VXXg.htm](spells/common-01-kcelf6IHl3L9VXXg.htm)|Vitality Lash|Déchaînement de vitalité|libre|
|[common-01-KcLVELhCUcKXxiKE.htm](spells/common-01-KcLVELhCUcKXxiKE.htm)|Tailwind|Vent arrière|libre|
|[common-01-KFpBT6FPfSFhxQ27.htm](spells/common-01-KFpBT6FPfSFhxQ27.htm)|Juvenile Companion|Compagnon juvénile|libre|
|[common-01-lg73SvJZno1ypPAj.htm](spells/common-01-lg73SvJZno1ypPAj.htm)|Read the Air|Lire l'ambiance|libre|
|[common-01-lmHmKGs1N3yNAvln.htm](spells/common-01-lmHmKGs1N3yNAvln.htm)|Wash Your Luck|Laver votre chance|libre|
|[common-01-LvezN4a3kYf1OHMg.htm](spells/common-01-LvezN4a3kYf1OHMg.htm)|Floating Disk|Disque flottant|libre|
|[common-01-lXsFKdaOz9t9w11U.htm](spells/common-01-lXsFKdaOz9t9w11U.htm)|Draw Moisture|Tirer l'humidité|libre|
|[common-01-m5QS8q5M6K0euKcT.htm](spells/common-01-m5QS8q5M6K0euKcT.htm)|Healing Plaster|Emplâtre|libre|
|[common-01-mAMEt4FFbdqoRnkN.htm](spells/common-01-mAMEt4FFbdqoRnkN.htm)|Void Warp|Distorsion du vide|libre|
|[common-01-MMIPT6jfwHfLPkm5.htm](spells/common-01-MMIPT6jfwHfLPkm5.htm)|Wall of Shrubs|Haie d'arbustes|libre|
|[common-01-mnhuvqTIELcpJOFX.htm](spells/common-01-mnhuvqTIELcpJOFX.htm)|Musical Accompaniment|Accompagnement musical|libre|
|[common-01-MPxbKoR54gkYkqLO.htm](spells/common-01-MPxbKoR54gkYkqLO.htm)|Gouging Claw|Griffe creusante|libre|
|[common-01-MVrxZarUTnJxAUN8.htm](spells/common-01-MVrxZarUTnJxAUN8.htm)|Tether|Relier|libre|
|[common-01-NkeLctXo9FLGnDhi.htm](spells/common-01-NkeLctXo9FLGnDhi.htm)|Divine Plagues|Plaies divines|libre|
|[common-01-nX85Brzax9f650aK.htm](spells/common-01-nX85Brzax9f650aK.htm)|Invisible Item|Objet invisible|libre|
|[common-01-nXmC2Xx9WmS5NsAo.htm](spells/common-01-nXmC2Xx9WmS5NsAo.htm)|Share Lore|Connaissance partagée|libre|
|[common-01-o4lRVTwSxnOOn5vl.htm](spells/common-01-o4lRVTwSxnOOn5vl.htm)|Sleep|Sommeil|libre|
|[common-01-O9w7r4BKgPogYDDe.htm](spells/common-01-O9w7r4BKgPogYDDe.htm)|Produce Flame|Flammes|libre|
|[common-01-OhD2Z6rIGGD5ocZA.htm](spells/common-01-OhD2Z6rIGGD5ocZA.htm)|Read Aura|Lecture de l'aura|libre|
|[common-01-pBevG6bSQOiyflev.htm](spells/common-01-pBevG6bSQOiyflev.htm)|Befuddle|Embrouiller|libre|
|[common-01-PGVhjDbzC4lf6aXF.htm](spells/common-01-PGVhjDbzC4lf6aXF.htm)|Breadcrumbs|Miettes de pain|libre|
|[common-01-PRrZ7anETWPm90YY.htm](spells/common-01-PRrZ7anETWPm90YY.htm)|Disguise Magic|Magie déguisée|libre|
|[common-01-pwzdSlJgYqN7bs2w.htm](spells/common-01-pwzdSlJgYqN7bs2w.htm)|Telekinetic Hand|Manipulation télékinésique|officielle|
|[common-01-q5vbYgLztQ04FQZg.htm](spells/common-01-q5vbYgLztQ04FQZg.htm)|Seashell of Stolen Sound|Coquillage de son volé|libre|
|[common-01-Q7QQ91vQtyi1Ux36.htm](spells/common-01-Q7QQ91vQtyi1Ux36.htm)|Jump|Saut|libre|
|[common-01-qCkxBOLMCCXEjBWo.htm](spells/common-01-qCkxBOLMCCXEjBWo.htm)|Illuminate|Illuminer|libre|
|[common-01-qTr2oCgIXl703Whb.htm](spells/common-01-qTr2oCgIXl703Whb.htm)|Thoughtful Gift|Cadeau attentionné|libre|
|[common-01-Qw3fnUlaUbnn7ipC.htm](spells/common-01-Qw3fnUlaUbnn7ipC.htm)|Prestidigitation|Prestidigitation|officielle|
|[common-01-qwZBXN6zBoB9BHXE.htm](spells/common-01-qwZBXN6zBoB9BHXE.htm)|Divine Lance|Lance divine|libre|
|[common-01-qXTB7Ec9yYh5JPPV.htm](spells/common-01-qXTB7Ec9yYh5JPPV.htm)|Cleanse Cuisine|Cuisine purifiée|libre|
|[common-01-r3NeUnsgt9mS03Sn.htm](spells/common-01-r3NeUnsgt9mS03Sn.htm)|Shocking Grasp|Décharge électrique|officielle|
|[common-01-R8bqnYiThB6MYTxD.htm](spells/common-01-R8bqnYiThB6MYTxD.htm)|Phantom Pain|Douleur fantôme|libre|
|[common-01-r8g7oSumKOHDqJsd.htm](spells/common-01-r8g7oSumKOHDqJsd.htm)|Agitate|Agitation|libre|
|[common-01-rerNA6YZsdxuJYt3.htm](spells/common-01-rerNA6YZsdxuJYt3.htm)|Déjà Vu|Déjà vu|libre|
|[common-01-rfZpqmj0AIIdkVIs.htm](spells/common-01-rfZpqmj0AIIdkVIs.htm)|Heal|Guérison|libre|
|[common-01-Rn2LkoSq1XhLsODV.htm](spells/common-01-Rn2LkoSq1XhLsODV.htm)|Pummeling Rubble|Frappe de débris|libre|
|[common-01-rnNGALRtsjspFTws.htm](spells/common-01-rnNGALRtsjspFTws.htm)|Acidic Burst|Explosion d'acide|libre|
|[common-01-rVhHaWqUsVUO4GuY.htm](spells/common-01-rVhHaWqUsVUO4GuY.htm)|Eject Soul|Expulsion de l'âme|libre|
|[common-01-RztmhJrLLQWoGVdB.htm](spells/common-01-RztmhJrLLQWoGVdB.htm)|Object Memory|Mémoire de l'objet|libre|
|[common-01-s3abwDbTV43pGFFW.htm](spells/common-01-s3abwDbTV43pGFFW.htm)|Shillelagh|Gourdin magique|officielle|
|[common-01-S6Kkk15MWGqzC00a.htm](spells/common-01-S6Kkk15MWGqzC00a.htm)|Draconic Barrage|Barrage draconique|libre|
|[common-01-s7ILzY2xh1tc9U1v.htm](spells/common-01-s7ILzY2xh1tc9U1v.htm)|Tame|Apprivoiser|libre|
|[common-01-SnjhtQYexDtNDdEg.htm](spells/common-01-SnjhtQYexDtNDdEg.htm)|Stabilize|Stabilisation|libre|
|[common-01-sPHcuLIKj9SDaDAD.htm](spells/common-01-sPHcuLIKj9SDaDAD.htm)|Kinetic Ram|Bélier kinétique|libre|
|[common-01-sX2g6WFSQPNW9jzx.htm](spells/common-01-sX2g6WFSQPNW9jzx.htm)|Warp Step|Pas déformant|libre|
|[common-01-szIyEsvihc5e1w8n.htm](spells/common-01-szIyEsvihc5e1w8n.htm)|Soothe|Apaiser|officielle|
|[common-01-TFitdEOpQC4SzKQQ.htm](spells/common-01-TFitdEOpQC4SzKQQ.htm)|Runic Weapon|Arme runique|libre|
|[common-01-thAHF1zxNplLCJPO.htm](spells/common-01-thAHF1zxNplLCJPO.htm)|Caustic Blast|Explosion caustique|libre|
|[common-01-TjWHgXqPv5jywMti.htm](spells/common-01-TjWHgXqPv5jywMti.htm)|Pocket Library|Bibliothèque de poche|libre|
|[common-01-TPI9fRCAYsDqpAe4.htm](spells/common-01-TPI9fRCAYsDqpAe4.htm)|Temporary Tool|Outil temporaire|libre|
|[common-01-TSyFZNAbqfkRrcq0.htm](spells/common-01-TSyFZNAbqfkRrcq0.htm)|Concordant Choir|Choeur concordant|libre|
|[common-01-TTwOKGqmZeKSyNMH.htm](spells/common-01-TTwOKGqmZeKSyNMH.htm)|Gentle Landing|Atterrissage en douceur|libre|
|[common-01-TVKNbcgTee19PXZR.htm](spells/common-01-TVKNbcgTee19PXZR.htm)|Shield|Bouclier|libre|
|[common-01-tXa5vOu5giBNCjdR.htm](spells/common-01-tXa5vOu5giBNCjdR.htm)|Know the Way|Connaître le chemin|libre|
|[common-01-UKsIOWmMx4hSpafl.htm](spells/common-01-UKsIOWmMx4hSpafl.htm)|Dizzying Colors|Couleurs vertigineuses|libre|
|[common-01-uMADQaASgYNsSDDM.htm](spells/common-01-uMADQaASgYNsSDDM.htm)|Tremor Signs|Vibrations indicatives|libre|
|[common-01-uToa7ksKAzmEpkKC.htm](spells/common-01-uToa7ksKAzmEpkKC.htm)|Admonishing Ray|Rayon d'admonestation|libre|
|[common-01-uZK2BYzPnxUBnDjr.htm](spells/common-01-uZK2BYzPnxUBnDjr.htm)|Tangle Vine|Liane gênante|libre|
|[common-01-VACvA5VRbddjt5s9.htm](spells/common-01-VACvA5VRbddjt5s9.htm)|Infectious Enthusiasm|Enthousiasme communicatif|libre|
|[common-01-vJZ83ehQiM906lea.htm](spells/common-01-vJZ83ehQiM906lea.htm)|Detect Metal|Détection des métaux|libre|
|[common-01-vLA0q0WOK2YPuJs6.htm](spells/common-01-vLA0q0WOK2YPuJs6.htm)|Charm|Charme|officielle|
|[common-01-vLzFcIaSXs7YTIqJ.htm](spells/common-01-vLzFcIaSXs7YTIqJ.htm)|Message|Message|libre|
|[common-01-vtCCVe869shMCJMj.htm](spells/common-01-vtCCVe869shMCJMj.htm)|Echoing Weapon|Arme à écho|libre|
|[common-01-W02bHXylIpoXbO4e.htm](spells/common-01-W02bHXylIpoXbO4e.htm)|Bullhorn|Porte-voix|libre|
|[common-01-W69zswpj0Trdy5rj.htm](spells/common-01-W69zswpj0Trdy5rj.htm)|Air Bubble|Bulle d'air|officielle|
|[common-01-W6QlRwQLPoBSw6PZ.htm](spells/common-01-W6QlRwQLPoBSw6PZ.htm)|Snowball|Boule de neige|officielle|
|[common-01-WBmvzNDfpwka3qT4.htm](spells/common-01-WBmvzNDfpwka3qT4.htm)|Light|Lumière|libre|
|[common-01-wdA52JJnsuQWeyqz.htm](spells/common-01-wdA52JJnsuQWeyqz.htm)|Harm|Mise à mal|libre|
|[common-01-Wu0xFpewMKRK3HG8.htm](spells/common-01-Wu0xFpewMKRK3HG8.htm)|Grease|Graisse|libre|
|[common-01-WUNZizIe6rnVHGcr.htm](spells/common-01-WUNZizIe6rnVHGcr.htm)|Take Root|Prendre racine|libre|
|[common-01-WzLKjSw6hsBhuklC.htm](spells/common-01-WzLKjSw6hsBhuklC.htm)|Create Water|Création d'eau|officielle|
|[common-01-X56VdA98VedAfGTU.htm](spells/common-01-X56VdA98VedAfGTU.htm)|Thicket of Knives|Multitude de couteaux|libre|
|[common-01-X9dkmh23lFwMjrYd.htm](spells/common-01-X9dkmh23lFwMjrYd.htm)|Ant Haul|Charge de fourmi|officielle|
|[common-01-xGFAGlonNySXrunq.htm](spells/common-01-xGFAGlonNySXrunq.htm)|Buffeting Winds|Vents turbulents|libre|
|[common-01-XhgMx9WC6NfXd9RP.htm](spells/common-01-XhgMx9WC6NfXd9RP.htm)|Hyperfocus|Hyperacuité|libre|
|[common-01-xqmHD8JIjak15lRk.htm](spells/common-01-xqmHD8JIjak15lRk.htm)|Phantasmal Minion|Sbire fantasmagorique|libre|
|[common-01-XSujb7EsSwKl19Uu.htm](spells/common-01-XSujb7EsSwKl19Uu.htm)|Bless|Bénédiction|libre|
|[common-01-y6rAdMK6EFlV6U0t.htm](spells/common-01-y6rAdMK6EFlV6U0t.htm)|Breathe Fire|Cracher du feu|libre|
|[common-01-yafsV0ni7rFgqJBj.htm](spells/common-01-yafsV0ni7rFgqJBj.htm)|Biting Words|Mots mordants|libre|
|[common-01-yAiSNmz39qXOIlco.htm](spells/common-01-yAiSNmz39qXOIlco.htm)|Mud Pit|Fosse de boue|libre|
|[common-01-yD819yTgbBFcCh5M.htm](spells/common-01-yD819yTgbBFcCh5M.htm)|Fold Metal|Métal plié|libre|
|[common-01-yHujiDQPdtXW797e.htm](spells/common-01-yHujiDQPdtXW797e.htm)|Spirit Link|Lien spirituel|libre|
|[common-01-Yrek2Yd4k3DPC2zV.htm](spells/common-01-Yrek2Yd4k3DPC2zV.htm)|Zenith Star|Étoile zénithale|libre|
|[common-01-yV7Ouzaoe7DHLESI.htm](spells/common-01-yV7Ouzaoe7DHLESI.htm)|Ventriloquism|Ventriloquie|officielle|
|[common-01-yvs1zN5Pai5U4CJX.htm](spells/common-01-yvs1zN5Pai5U4CJX.htm)|Summon Instrument|Convocation d'instrument|libre|
|[common-01-Z7N5IxJCwrAdIgSg.htm](spells/common-01-Z7N5IxJCwrAdIgSg.htm)|Message Rune|Rune messagère|libre|
|[common-01-zA0jNIBRgLsyTpbm.htm](spells/common-01-zA0jNIBRgLsyTpbm.htm)|Scatter Scree|Projection d'éboulis|libre|
|[common-01-zdb8cjOIDVKYMWdr.htm](spells/common-01-zdb8cjOIDVKYMWdr.htm)|Penumbral Shroud|Voile d'ombre|libre|
|[common-01-zDJS8E66UI0himqV.htm](spells/common-01-zDJS8E66UI0himqV.htm)|Thunderstrike|Frappe tonitruante|libre|
|[common-01-zhDIiQlJmrd4UDNC.htm](spells/common-01-zhDIiQlJmrd4UDNC.htm)|Rousing Splash|Aspersion réveillante|libre|
|[common-01-zJQnkKEKbJqGB3iB.htm](spells/common-01-zJQnkKEKbJqGB3iB.htm)|Goblin Pox|Variole gobeline|libre|
|[common-01-zlnXpME1T2uvn8Lr.htm](spells/common-01-zlnXpME1T2uvn8Lr.htm)|Vanishing Tracks|Traces évanescentes|libre|
|[common-01-ZrRhweQ7KQ0wKN3m.htm](spells/common-01-ZrRhweQ7KQ0wKN3m.htm)|Signal Skyrocket|Fusée de signalisation|libre|
|[common-02-02J0rDTk37KN2sjt.htm](spells/common-02-02J0rDTk37KN2sjt.htm)|Quench|Extinction du feu|libre|
|[common-02-0qaqksrGGDj74HXE.htm](spells/common-02-0qaqksrGGDj74HXE.htm)|Revealing Light|Lumière révélatrice|libre|
|[common-02-1xbFBQDRs0hT5xZ9.htm](spells/common-02-1xbFBQDRs0hT5xZ9.htm)|Shatter|Fracassement|officielle|
|[common-02-23cv1p49NFd0Onng.htm](spells/common-02-23cv1p49NFd0Onng.htm)|Ash Cloud|Nuage de cendres|libre|
|[common-02-24U1b9K4Lj94cgaj.htm](spells/common-02-24U1b9K4Lj94cgaj.htm)|Magnetic Repulsion|Répulsion magnétique|libre|
|[common-02-2qGqa33E4GPUCbMV.htm](spells/common-02-2qGqa33E4GPUCbMV.htm)|Humanoid Form|Forme humanoïde|libre|
|[common-02-3GXU3ugrwLv0P7AH.htm](spells/common-02-3GXU3ugrwLv0P7AH.htm)|Ignite Fireworks|Allumer un feu d'artifice|libre|
|[common-02-3JG1t3T4mWn6vTke.htm](spells/common-02-3JG1t3T4mWn6vTke.htm)|Blur|Flou|officielle|
|[common-02-3q9tBMWsWQKlXPPJ.htm](spells/common-02-3q9tBMWsWQKlXPPJ.htm)|Flame Wisp|Feu follet enflammé|libre|
|[common-02-3VxVbZqIRvpKkg3O.htm](spells/common-02-3VxVbZqIRvpKkg3O.htm)|Fungal Infestation|Infestation fongique|libre|
|[common-02-41TZEjhO6D1nWw2X.htm](spells/common-02-41TZEjhO6D1nWw2X.htm)|Augury|Augure|libre|
|[common-02-5esP2GVzvxWsMgaX.htm](spells/common-02-5esP2GVzvxWsMgaX.htm)|Environmental Endurance|Endurance environnementale|officielle|
|[common-02-5KobTMrZeZxuXMgl.htm](spells/common-02-5KobTMrZeZxuXMgl.htm)|Gecko Grip|Adhérence du gecko|officielle|
|[common-02-6Ot4N22t5tPD51BO.htm](spells/common-02-6Ot4N22t5tPD51BO.htm)|Knock|Déblocage|libre|
|[common-02-7dBxguCNWCoPYxIi.htm](spells/common-02-7dBxguCNWCoPYxIi.htm)|Clad In Metal|Plaqué de métal|libre|
|[common-02-7dKtMehJ6YrXwJLR.htm](spells/common-02-7dKtMehJ6YrXwJLR.htm)|Magnetic Attraction|Attraction magnétique|libre|
|[common-02-8ViwItUgwT4lOvvb.htm](spells/common-02-8ViwItUgwT4lOvvb.htm)|False Vitality|Fausse vitalité|libre|
|[common-02-9h9YCncqah6VNsKf.htm](spells/common-02-9h9YCncqah6VNsKf.htm)|Acid Grip|Pogne acide|libre|
|[common-02-9HpwDN4MYQJnW0LG.htm](spells/common-02-9HpwDN4MYQJnW0LG.htm)|Dispel Magic|Dissipation de la magie|officielle|
|[common-02-9s5tqqXNzcoKamWx.htm](spells/common-02-9s5tqqXNzcoKamWx.htm)|Web|Toile d'araignée|officielle|
|[common-02-9XHmC2JgTUIQ1CCm.htm](spells/common-02-9XHmC2JgTUIQ1CCm.htm)|Mist|Brume|libre|
|[common-02-aXoh6OQAL57lgh0a.htm](spells/common-02-aXoh6OQAL57lgh0a.htm)|Expeditious Excavation|Excavation expéditive|libre|
|[common-02-BBvV7qoXGdw09q1C.htm](spells/common-02-BBvV7qoXGdw09q1C.htm)|Speak with Animals|Communication avec les animaux|libre|
|[common-02-Bc6QqTmFV4q7G7L0.htm](spells/common-02-Bc6QqTmFV4q7G7L0.htm)|Splinter Volley|Volée d'échardes|libre|
|[common-02-BCuHKrDeJ4eq53M6.htm](spells/common-02-BCuHKrDeJ4eq53M6.htm)|Sure Footing|Remise sur pied|libre|
|[common-02-C0D2eqzTAhiKm4j9.htm](spells/common-02-C0D2eqzTAhiKm4j9.htm)|Gentle Breeze|Douce brise|libre|
|[common-02-cB17yFc9456Pyfec.htm](spells/common-02-cB17yFc9456Pyfec.htm)|Vomit Swarm|Vomir une nuée|libre|
|[common-02-CLThxp8Qf43IQ3Sb.htm](spells/common-02-CLThxp8Qf43IQ3Sb.htm)|Extract Poison|Extraction du poison|libre|
|[common-02-CQb8HtQ1BPeZmu9h.htm](spells/common-02-CQb8HtQ1BPeZmu9h.htm)|Stupefy|Abrutir|libre|
|[common-02-CQWTyDxiVSW4uRn7.htm](spells/common-02-CQWTyDxiVSW4uRn7.htm)|Empty Pack|Emballage vide|libre|
|[common-02-cwXiKPkZrIupjwlQ.htm](spells/common-02-cwXiKPkZrIupjwlQ.htm)|Summoner's Precaution|Précaution du conjurateur|libre|
|[common-02-CXICME10TkEJxz0P.htm](spells/common-02-CXICME10TkEJxz0P.htm)|Shape Wood|Façonnage du bois|officielle|
|[common-02-d18m4AAaxJQqUEh2.htm](spells/common-02-d18m4AAaxJQqUEh2.htm)|Voice on the Breeze|Voix dans la brise|libre|
|[common-02-d7Lwx6KAs47MtF0q.htm](spells/common-02-d7Lwx6KAs47MtF0q.htm)|Share Life|Partage de vie|libre|
|[common-02-dileJ0Yxqg76LMvu.htm](spells/common-02-dileJ0Yxqg76LMvu.htm)|One with Plants|Uni aux plantes|libre|
|[common-02-dLdRqT6UxTKlsPgp.htm](spells/common-02-dLdRqT6UxTKlsPgp.htm)|Death Knell|Mise à mort|libre|
|[common-02-dxOF7d5kAWusLKWF.htm](spells/common-02-dxOF7d5kAWusLKWF.htm)|Reaper's Lantern|Lanterne de la Faucheuse|libre|
|[common-02-EAYViUegegttJF1u.htm](spells/common-02-EAYViUegegttJF1u.htm)|Tremorsense|Perception des vibrations|libre|
|[common-02-EfFMLVbmkBWmzoLF.htm](spells/common-02-EfFMLVbmkBWmzoLF.htm)|Clear Mind|Clarté d'esprit|officielle|
|[common-02-eidmNlfl2DUVIclc.htm](spells/common-02-eidmNlfl2DUVIclc.htm)|Brine Dragon Bile|Bile de dragon de saumure|libre|
|[common-02-eIQ86FOXK34HiNLs.htm](spells/common-02-eIQ86FOXK34HiNLs.htm)|Embed Message|Message implanté|libre|
|[common-02-ElpqHitq4FqwLURV.htm](spells/common-02-ElpqHitq4FqwLURV.htm)|Cleanse Air|Purifier l'air|libre|
|[common-02-eSEmqOBuywoJ6tYd.htm](spells/common-02-eSEmqOBuywoJ6tYd.htm)|Inner Radiance Torrent|Torrent de radiance intérieure|libre|
|[common-02-Et8RSCLx8w7uOLvo.htm](spells/common-02-Et8RSCLx8w7uOLvo.htm)|Sound Body|Corps assaini|libre|
|[common-02-f8hRqLJaxBVhF1u0.htm](spells/common-02-f8hRqLJaxBVhF1u0.htm)|Acid Arrow|Flèche acide|libre|
|[common-02-f8SBoXiXQjlCKqly.htm](spells/common-02-f8SBoXiXQjlCKqly.htm)|Illusory Creature|Créature illusoire|libre|
|[common-02-FFBYPZEmAQoll09t.htm](spells/common-02-FFBYPZEmAQoll09t.htm)|Rubble Step|Semer des graviers|libre|
|[common-02-Fr58LDSrbndgld9n.htm](spells/common-02-Fr58LDSrbndgld9n.htm)|Resist Energy|Résistance à l'énergie|officielle|
|[common-02-fTK6ysisGhy3hRvz.htm](spells/common-02-fTK6ysisGhy3hRvz.htm)|Rapid Adaptation|Adaptation rapide|libre|
|[common-02-fWU7Qjp1JiX9g6eg.htm](spells/common-02-fWU7Qjp1JiX9g6eg.htm)|Animus Mine|Mine psychique|libre|
|[common-02-fZPCv2VHuM2yPbC8.htm](spells/common-02-fZPCv2VHuM2yPbC8.htm)|Deafness|Surdité|officielle|
|[common-02-gIdDLrbswTV3OBJy.htm](spells/common-02-gIdDLrbswTV3OBJy.htm)|Silence|Silence|libre|
|[common-02-hoR6w8BqX2F35Tdx.htm](spells/common-02-hoR6w8BqX2F35Tdx.htm)|Blistering Invective|Invectives torrides|libre|
|[common-02-HRb2doyaLtaoCfi3.htm](spells/common-02-HRb2doyaLtaoCfi3.htm)|Faerie Fire|Lueurs féeriques|libre|
|[common-02-HTou8cG05yuSkesj.htm](spells/common-02-HTou8cG05yuSkesj.htm)|Status|Rapport|officielle|
|[common-02-IhwREVWG0OzzrbWA.htm](spells/common-02-IhwREVWG0OzzrbWA.htm)|Iron Gut|Boyaux de fer|libre|
|[common-02-J6vNvrUT3b1hx2iA.htm](spells/common-02-J6vNvrUT3b1hx2iA.htm)|Entangling Flora|Flore enchevêtrante|libre|
|[common-02-j8vIoIEWElvpwkcI.htm](spells/common-02-j8vIoIEWElvpwkcI.htm)|Mirror Image|Image miroir|libre|
|[common-02-JhRuR7Jj3ViShpq7.htm](spells/common-02-JhRuR7Jj3ViShpq7.htm)|Ghoulish Cravings|Désirs macabres|libre|
|[common-02-jlOAXuIOM3YxZKmn.htm](spells/common-02-jlOAXuIOM3YxZKmn.htm)|Propulsive Breeze|Brise propulsive|libre|
|[common-02-jW2asKFchuoxniSH.htm](spells/common-02-jW2asKFchuoxniSH.htm)|Dismantle|Démanteler|libre|
|[common-02-jwK43yKsHTkJQvQ9.htm](spells/common-02-jwK43yKsHTkJQvQ9.htm)|See the Unseen|Discerner l'invisible|libre|
|[common-02-kIwA7kwp5E0AC3yM.htm](spells/common-02-kIwA7kwp5E0AC3yM.htm)|Warrior's Regret|Regret du combattant|libre|
|[common-02-kMffb6SvhCBMMI4k.htm](spells/common-02-kMffb6SvhCBMMI4k.htm)|Elemental Zone|Zone élémentaire|libre|
|[common-02-L0GoJpHxSD0wRY5k.htm](spells/common-02-L0GoJpHxSD0wRY5k.htm)|Phantasmal Treasure|Trésor fantasmatique|libre|
|[common-02-L10q1ng6U3sega9S.htm](spells/common-02-L10q1ng6U3sega9S.htm)|Guiding Star|Étoile guide|libre|
|[common-02-l194BTf9cAFlSQ96.htm](spells/common-02-l194BTf9cAFlSQ96.htm)|Thermal Remedy|Remède thermique|libre|
|[common-02-LFcGNGLMkGlsnEKQ.htm](spells/common-02-LFcGNGLMkGlsnEKQ.htm)|Spy's Mark|Marque de l'espion|libre|
|[common-02-Mkbq9xlAUxHUHyR2.htm](spells/common-02-Mkbq9xlAUxHUHyR2.htm)|Paranoia|Paranoïa|officielle|
|[common-02-mLAYvbafVKfBgEhz.htm](spells/common-02-mLAYvbafVKfBgEhz.htm)|Loose Time's Arrow|Relâcher la flèche du temps|libre|
|[common-02-mrDi3v933gsmnw25.htm](spells/common-02-mrDi3v933gsmnw25.htm)|Telekinetic Maneuver|Manoeuvre télékinésique|libre|
|[common-02-Mt6ZzkVX8Q4xigFq.htm](spells/common-02-Mt6ZzkVX8Q4xigFq.htm)|Create Food|Création de nourriture|officielle|
|[common-02-MZGkMsPBztFN0pUO.htm](spells/common-02-MZGkMsPBztFN0pUO.htm)|Water Breathing|Respiration aquatique|officielle|
|[common-02-oNUyCqbpGWHifS02.htm](spells/common-02-oNUyCqbpGWHifS02.htm)|Fungal Hyphae|Hyphe fongique|libre|
|[common-02-PjhUmyKnq6K5uDby.htm](spells/common-02-PjhUmyKnq6K5uDby.htm)|Shrink|Rétrécissement|officielle|
|[common-02-pZTqGY1MLRjgKasV.htm](spells/common-02-pZTqGY1MLRjgKasV.htm)|Darkvision|Vision dans le noir|officielle|
|[common-02-q5qmNn144ZJGxnvJ.htm](spells/common-02-q5qmNn144ZJGxnvJ.htm)|Befitting Attire|Tenue adéquate|libre|
|[common-02-qCmihBL0G0G5ExPF.htm](spells/common-02-qCmihBL0G0G5ExPF.htm)|Persistent Servant|Serviteur persistant|libre|
|[common-02-QLTgY1AqFvX82vq2.htm](spells/common-02-QLTgY1AqFvX82vq2.htm)|Fireproof|Ignifugé|libre|
|[common-02-rDDYGY2ekZ1yVv7q.htm](spells/common-02-rDDYGY2ekZ1yVv7q.htm)|Lucky Number|Nombre porte bonheur|libre|
|[common-02-rdTEF1hfAWbN58NE.htm](spells/common-02-rdTEF1hfAWbN58NE.htm)|Enhance Victuals|Amélioration des victuailles|officielle|
|[common-02-RS1nrqEjEi1ZlC53.htm](spells/common-02-RS1nrqEjEi1ZlC53.htm)|Waterproof|Imperméable|libre|
|[common-02-rthC6dGm3nNrt1xN.htm](spells/common-02-rthC6dGm3nNrt1xN.htm)|Ghostly Carrier|Porteur fantomatique|officielle|
|[common-02-Ru9v8U9IRk3LtWx8.htm](spells/common-02-Ru9v8U9IRk3LtWx8.htm)|Feral Shades|Ombres sauvages|libre|
|[common-02-sBSalosrt7C4IXas.htm](spells/common-02-sBSalosrt7C4IXas.htm)|Cauterize Wounds|Cautériser les blessures|libre|
|[common-02-Seaah9amXg70RKw2.htm](spells/common-02-Seaah9amXg70RKw2.htm)|Water Walk|Marche sur l'eau|officielle|
|[common-02-siU9xRlqWXeKT0mH.htm](spells/common-02-siU9xRlqWXeKT0mH.htm)|Feast of Ashes|Festin de cendres|libre|
|[common-02-SUKaxVZW2TlM8lu0.htm](spells/common-02-SUKaxVZW2TlM8lu0.htm)|Cleanse Affliction|Affliction purifiée|libre|
|[common-02-TaaMEYdZXQXF0Sks.htm](spells/common-02-TaaMEYdZXQXF0Sks.htm)|Blood Vendetta|Vendetta du sang|libre|
|[common-02-tlSE7Ly8vi1Dgddv.htm](spells/common-02-tlSE7Ly8vi1Dgddv.htm)|Laughing Fit|Accès de rigolade|libre|
|[common-02-udZEKwUryIsobjBG.htm](spells/common-02-udZEKwUryIsobjBG.htm)|Falsify Heat|Falsifier la chaleur|libre|
|[common-02-UirEIHILQgip87qv.htm](spells/common-02-UirEIHILQgip87qv.htm)|Charitable Urge|Désir Charitable|libre|
|[common-02-uiv8FKuxE8HOWTxW.htm](spells/common-02-uiv8FKuxE8HOWTxW.htm)|Instant Armor|Armure instantanée|libre|
|[common-02-v4KzRPol5XQOOmk0.htm](spells/common-02-v4KzRPol5XQOOmk0.htm)|Heat Metal|Métal brûlant|libre|
|[common-02-v89KwyaBd6g5rWVS.htm](spells/common-02-v89KwyaBd6g5rWVS.htm)|Exploding Earth|Terre explosiive|libre|
|[common-02-vb2dFNtbofJ7A9BW.htm](spells/common-02-vb2dFNtbofJ7A9BW.htm)|Summoner's Visage|Visage du conjurateur|libre|
|[common-02-vTQvfYu2llKQedmY.htm](spells/common-02-vTQvfYu2llKQedmY.htm)|Translate|Traduction|libre|
|[common-02-wp09USMB3GIW1qbp.htm](spells/common-02-wp09USMB3GIW1qbp.htm)|Animal Form|Forme animale|libre|
|[common-02-WPKJOhEihhcIm2uQ.htm](spells/common-02-WPKJOhEihhcIm2uQ.htm)|Marvelous Mount|Monture merveilleuse|libre|
|[common-02-wzctak6BxOW8xvFV.htm](spells/common-02-wzctak6BxOW8xvFV.htm)|Enlarge|Agrandissement|libre|
|[common-02-wzLkNU3AAqOSKFPR.htm](spells/common-02-wzLkNU3AAqOSKFPR.htm)|Noise Blast|Explosion de son|officielle|
|[common-02-x0rWq0wS06dns4G2.htm](spells/common-02-x0rWq0wS06dns4G2.htm)|Final Sacrifice|Sacrifice final|libre|
|[common-02-X3dYByf3YmkcdwG0.htm](spells/common-02-X3dYByf3YmkcdwG0.htm)|Slough Skin|Mues de peau|libre|
|[common-02-XEhzFKNTSARsofav.htm](spells/common-02-XEhzFKNTSARsofav.htm)|Advanced Scurvy|Scorbut avancé|libre|
|[common-02-XFtO4BBI22Uox2QP.htm](spells/common-02-XFtO4BBI22Uox2QP.htm)|Sudden Blight|Dégradation soudaine|libre|
|[common-02-xlRv8vCTHh1NeMpw.htm](spells/common-02-xlRv8vCTHh1NeMpw.htm)|Mimic Undead|Imiter un mort-vivant|libre|
|[common-02-XXqE1eY3w3z6xJCB.htm](spells/common-02-XXqE1eY3w3z6xJCB.htm)|Invisibility|Invisibilité|libre|
|[common-02-Y8cSjhU33oUqccxJ.htm](spells/common-02-Y8cSjhU33oUqccxJ.htm)|Brand the Impenitent|Marquer l'impénitent|libre|
|[common-02-yhz9fF69uwRhnHix.htm](spells/common-02-yhz9fF69uwRhnHix.htm)|Animal Messenger|Messager animal|officielle|
|[common-02-Yv3AFIO55FONQYMH.htm](spells/common-02-Yv3AFIO55FONQYMH.htm)|Sonata Span|Pont sonate|libre|
|[common-02-ywHwnXihTnXbm9Xq.htm](spells/common-02-ywHwnXihTnXbm9Xq.htm)|Blazing Armory|Arsenal embrasé|libre|
|[common-02-YWrfKetOqDwVFut7.htm](spells/common-02-YWrfKetOqDwVFut7.htm)|Oaken Resilience|Résilience du chêne|libre|
|[common-02-ZxHC7V7HtjUsB8zH.htm](spells/common-02-ZxHC7V7HtjUsB8zH.htm)|Blazing Bolt|Trait flamboyant|libre|
|[common-03-06pzGkKTyPE3tHR8.htm](spells/common-03-06pzGkKTyPE3tHR8.htm)|Gravity Well|Puits de gravité|libre|
|[common-03-0gdZrT9lwO17EIxc.htm](spells/common-03-0gdZrT9lwO17EIxc.htm)|Ooze Form|Forme de vase|libre|
|[common-03-0Rl3W7kiq9xVZRcr.htm](spells/common-03-0Rl3W7kiq9xVZRcr.htm)|Time Pocket|Poche de temps|libre|
|[common-03-10siFBMF4pIDhVmf.htm](spells/common-03-10siFBMF4pIDhVmf.htm)|Cup of Dust|Tasse de poussière|libre|
|[common-03-1HfusQ8NDWutGvMx.htm](spells/common-03-1HfusQ8NDWutGvMx.htm)|Animal Vision|Vision animale|officielle|
|[common-03-1xM9muPRa8hiyJtI.htm](spells/common-03-1xM9muPRa8hiyJtI.htm)|Dive and Breach|Plongée et brèche|libre|
|[common-03-3A3ueS9Q0s3U8KXE.htm](spells/common-03-3A3ueS9Q0s3U8KXE.htm)|Noxious Metals|Métaux nocifs|libre|
|[common-03-3mQkAjexj6hhAnkR.htm](spells/common-03-3mQkAjexj6hhAnkR.htm)|Behold the Weave|Contemplez la trame|libre|
|[common-03-3x6eUCm17n6ROzUa.htm](spells/common-03-3x6eUCm17n6ROzUa.htm)|Crisis of Faith|Manque de foi|officielle|
|[common-03-5OcZ3HBkrRFhSWCz.htm](spells/common-03-5OcZ3HBkrRFhSWCz.htm)|Claim Curse|Réclamer la malédiction|libre|
|[common-03-5XUn9NADr05IyiVw.htm](spells/common-03-5XUn9NADr05IyiVw.htm)|Soothing Blossoms|Bourgeons apaisants|libre|
|[common-03-70BjbNRVc4OTLAgN.htm](spells/common-03-70BjbNRVc4OTLAgN.htm)|Tempest Cloak|Cape tempête|libre|
|[common-03-87zLUuTMmZ9zc7gH.htm](spells/common-03-87zLUuTMmZ9zc7gH.htm)|Oneiric Mire|Bourbier onirique|libre|
|[common-03-8cuqRFB3mWBOgy61.htm](spells/common-03-8cuqRFB3mWBOgy61.htm)|Shadow Projectile|Projectile d'ombre|libre|
|[common-03-9AAkVUCwF6WVNNY2.htm](spells/common-03-9AAkVUCwF6WVNNY2.htm)|Lightning Bolt|Éclair|officielle|
|[common-03-9GkOWDFDEMuV3hJr.htm](spells/common-03-9GkOWDFDEMuV3hJr.htm)|Familiar's Face|Visage du familier|libre|
|[common-03-9TauMFkIsmvKJNzZ.htm](spells/common-03-9TauMFkIsmvKJNzZ.htm)|Elemental Absorption|Absorption élémentaire|libre|
|[common-03-aq76wpgsVEENDyau.htm](spells/common-03-aq76wpgsVEENDyau.htm)|Shadow Spy|Espion de l'ombre|libre|
|[common-03-aUMmmtPmBdCdVDed.htm](spells/common-03-aUMmmtPmBdCdVDed.htm)|Wooden Double|Double de bois|libre|
|[common-03-B3eLlbaPxOYHcs1o.htm](spells/common-03-B3eLlbaPxOYHcs1o.htm)|Curse Of Lost Time|Malédiction du temps perdu|libre|
|[common-03-bH9cH9aDByY91l1d.htm](spells/common-03-bH9cH9aDByY91l1d.htm)|Organsight|Vision des organes|libre|
|[common-03-CIjj9CU5ekeq1oLT.htm](spells/common-03-CIjj9CU5ekeq1oLT.htm)|Warding Aggression|Agression protectrice|libre|
|[common-03-czO0wbT1i320gcu9.htm](spells/common-03-czO0wbT1i320gcu9.htm)|Roaring Applause|Applaudissements rugissants|libre|
|[common-03-D5ZAWGqxml5xOiZb.htm](spells/common-03-D5ZAWGqxml5xOiZb.htm)|Cloud Dragon's Cloak|Cape du dragon des nuages|libre|
|[common-03-DCQHaLrYXMI37dvW.htm](spells/common-03-DCQHaLrYXMI37dvW.htm)|Paralyze|Paralysie|officielle|
|[common-03-DyiD239dNS7RIxZE.htm](spells/common-03-DyiD239dNS7RIxZE.htm)|Holy Light|Lumière sainte|libre|
|[common-03-eIPIZp2FUbFcLNdj.htm](spells/common-03-eIPIZp2FUbFcLNdj.htm)|Pillar of Water|Pilier d'eau|libre|
|[common-03-F9mA2Bg27QKniIdv.htm](spells/common-03-F9mA2Bg27QKniIdv.htm)|Bottomless Stomach|Estomac sans fond|libre|
|[common-03-fI20AVwOzJMHXRdo.htm](spells/common-03-fI20AVwOzJMHXRdo.htm)|Levitate|Lévitation|libre|
|[common-03-fPlFRu4dp09qJs3K.htm](spells/common-03-fPlFRu4dp09qJs3K.htm)|Mind of Menace|Esprit de menace|libre|
|[common-03-GKpEcy9WId6NJvtx.htm](spells/common-03-GKpEcy9WId6NJvtx.htm)|Stinking Cloud|Nuage nauséabond|officielle|
|[common-03-gPvtmKMRpg9I9D7H.htm](spells/common-03-gPvtmKMRpg9I9D7H.htm)|Earthbind|Cloué à terre|officielle|
|[common-03-GUeRTriJkMlMlVrk.htm](spells/common-03-GUeRTriJkMlMlVrk.htm)|Bind Undead|Lier un mort-vivant|libre|
|[common-03-gWmLT5SkA0qH2mNE.htm](spells/common-03-gWmLT5SkA0qH2mNE.htm)|Envenom Companion|Compagnon venimeux|libre|
|[common-03-HHGUBGle4OjoxvNR.htm](spells/common-03-HHGUBGle4OjoxvNR.htm)|Sculpt Sound|Sculpter le son|libre|
|[common-03-HIY4XUJZKmFLAJTn.htm](spells/common-03-HIY4XUJZKmFLAJTn.htm)|Life Connection|Connexion vitale|libre|
|[common-03-HXhWYJviWalN5tQ2.htm](spells/common-03-HXhWYJviWalN5tQ2.htm)|Clairaudience|Clairaudience|officielle|
|[common-03-I0j56TNRmGcTyoqJ.htm](spells/common-03-I0j56TNRmGcTyoqJ.htm)|Invisibility Sphere|Sphère d'invisibilité|officielle|
|[common-03-IihxWhRfpsBgQ5jS.htm](spells/common-03-IihxWhRfpsBgQ5jS.htm)|Enthrall|Discours captivant|officielle|
|[common-03-it4ZsAi6XgvGcodc.htm](spells/common-03-it4ZsAi6XgvGcodc.htm)|Wall of Wind|Mur de vent|officielle|
|[common-03-JHntYF0SbaWKq7wR.htm](spells/common-03-JHntYF0SbaWKq7wR.htm)|Distracting Chatter|Conversation distrayante|libre|
|[common-03-k9x6bXXpIgAXMDsx.htm](spells/common-03-k9x6bXXpIgAXMDsx.htm)|Whirling Scarves|Écharpes tourbillonnantes|libre|
|[common-03-KEagmnwgsSbGmkme.htm](spells/common-03-KEagmnwgsSbGmkme.htm)|Cave Fangs|Crocs de caverne|libre|
|[common-03-KEkPxx7Sm6Xf4W3s.htm](spells/common-03-KEkPxx7Sm6Xf4W3s.htm)|Blazing Dive|Plongeon ardent|libre|
|[common-03-KjC2ya8HD5AENeHI.htm](spells/common-03-KjC2ya8HD5AENeHI.htm)|Sand Form|Forme de sable|libre|
|[common-03-KqvqNAfGIE5a9wSv.htm](spells/common-03-KqvqNAfGIE5a9wSv.htm)|Heroism|Héroïsme|officielle|
|[common-03-KsWhliKfUs3IpW3c.htm](spells/common-03-KsWhliKfUs3IpW3c.htm)|Wall of Thorns|Mur d'épines|officielle|
|[common-03-kWh8sJH7yawidMyW.htm](spells/common-03-kWh8sJH7yawidMyW.htm)|Shrink Item|Rétrécir un objet|officielle|
|[common-03-Llx0xKvtu8S4z6TI.htm](spells/common-03-Llx0xKvtu8S4z6TI.htm)|Day's Weight|Poids du jour|libre|
|[common-03-LNOwaJAkCfsgTKgV.htm](spells/common-03-LNOwaJAkCfsgTKgV.htm)|Wall of Water|Mur aquatique|libre|
|[common-03-lONs4r14LEBZRLLw.htm](spells/common-03-lONs4r14LEBZRLLw.htm)|Shift Blame|Bouc émissaire|libre|
|[common-03-MNiT0dHol5fEcKlz.htm](spells/common-03-MNiT0dHol5fEcKlz.htm)|Threefold Aspect|Triple aspect|libre|
|[common-03-mwPfoYfVGSMAaUec.htm](spells/common-03-mwPfoYfVGSMAaUec.htm)|Cozy Cabin|Cabane douillette|libre|
|[common-03-N1Z1oLPdBxaSgrEE.htm](spells/common-03-N1Z1oLPdBxaSgrEE.htm)|Vampiric Feast|Festin vampirique|libre|
|[common-03-NeoRkU7C4BIz8fUM.htm](spells/common-03-NeoRkU7C4BIz8fUM.htm)|Bracing Tendrils|Lianes de soutien|libre|
|[common-03-nplNt08TvokZUxtR.htm](spells/common-03-nplNt08TvokZUxtR.htm)|Agonizing Despair|Désespoir agonisant|libre|
|[common-03-NyxQAazFBdBAqZGX.htm](spells/common-03-NyxQAazFBdBAqZGX.htm)|Elemental Annihilation Wave|Vague d'annihilation élémentaire|libre|
|[common-03-o6YCGx4lycsYpww4.htm](spells/common-03-o6YCGx4lycsYpww4.htm)|Haste|Rapidité|officielle|
|[common-03-OuZY7VYeylub5K7l.htm](spells/common-03-OuZY7VYeylub5K7l.htm)|Lotus Walk|Marche du lotus|libre|
|[common-03-ppA1StEigPLKEQqR.htm](spells/common-03-ppA1StEigPLKEQqR.htm)|Wanderer's Guide|Guide du pélerin|officielle|
|[common-03-Pwq6T7xpfAJXV5aj.htm](spells/common-03-Pwq6T7xpfAJXV5aj.htm)|Phantom Prison|Prison fantôme|libre|
|[common-03-qJGW6BbIcU6sfA1d.htm](spells/common-03-qJGW6BbIcU6sfA1d.htm)|Time Jump|Saut temporel|libre|
|[common-03-qvwIwJ9QBihy8R0t.htm](spells/common-03-qvwIwJ9QBihy8R0t.htm)|Speak with Plants|Communication avec les plantes|libre|
|[common-03-rfFZKfeCSweFv7P3.htm](spells/common-03-rfFZKfeCSweFv7P3.htm)|Impending Doom|Malheur annoncé|libre|
|[common-03-RvBlSIJmxiqfCpR9.htm](spells/common-03-RvBlSIJmxiqfCpR9.htm)|Feet to Fins|Pieds en nageoires|officielle|
|[common-03-sRfSBHWHdbIa0aGc.htm](spells/common-03-sRfSBHWHdbIa0aGc.htm)|Chilling Darkness|Ténèbres glaciales|officielle|
|[common-03-sxQZ6yqTn0czJxVd.htm](spells/common-03-sxQZ6yqTn0czJxVd.htm)|Fireball|Boule de feu|libre|
|[common-03-T4QKmtYPeCgYxVGe.htm](spells/common-03-T4QKmtYPeCgYxVGe.htm)|Crashing Wave|Vague déferlante|libre|
|[common-03-ThE5zPYKF4weiljj.htm](spells/common-03-ThE5zPYKF4weiljj.htm)|Show the Way|Montrer le chemin|libre|
|[common-03-tSosbMsftXcRaQgT.htm](spells/common-03-tSosbMsftXcRaQgT.htm)|Sea of Thought|Océan de pensées|libre|
|[common-03-Tx8OqkBFA2QlaldW.htm](spells/common-03-Tx8OqkBFA2QlaldW.htm)|Magnetic Acceleration|Accélération magnétique|libre|
|[common-03-vh1RpbWfqdNC4L3P.htm](spells/common-03-vh1RpbWfqdNC4L3P.htm)|One with Stone|Uni avec la pierre|officielle|
|[common-03-VosLNn2M8S7JH67D.htm](spells/common-03-VosLNn2M8S7JH67D.htm)|Blindness|Cécité|officielle|
|[common-03-VVAZPCvd4d90qVA1.htm](spells/common-03-VVAZPCvd4d90qVA1.htm)|Secret Page|Page secrète|officielle|
|[common-03-WsUwpfmhKrKwoIe3.htm](spells/common-03-WsUwpfmhKrKwoIe3.htm)|Slow|Lenteur|officielle|
|[common-03-xabHqik5SoyDc5Db.htm](spells/common-03-xabHqik5SoyDc5Db.htm)|Moth's Supper|Souper du papillon de nuit|libre|
|[common-03-XI6Lzd2B5pernkPd.htm](spells/common-03-XI6Lzd2B5pernkPd.htm)|Insect Form|Forme d'insecte|officielle|
|[common-03-XQSPJHOf3TyfqvgS.htm](spells/common-03-XQSPJHOf3TyfqvgS.htm)|Web of Eyes|Mur d'yeux|libre|
|[common-03-y3jlOfgsQH1JjkJE.htm](spells/common-03-y3jlOfgsQH1JjkJE.htm)|Sanctified Ground|Zone sanctifiée|libre|
|[common-03-yM3KTTSAIHhyuP14.htm](spells/common-03-yM3KTTSAIHhyuP14.htm)|Dream Message|Message onirique|officielle|
|[common-03-YuCBhE9tGgN8G2zU.htm](spells/common-03-YuCBhE9tGgN8G2zU.htm)|Blastback|Rétroexplosion|libre|
|[common-03-ZIk1EYbG6l4JuUkO.htm](spells/common-03-ZIk1EYbG6l4JuUkO.htm)|Coral Scourge|Fléau coralien|libre|
|[common-03-ZYoC630tNGutgbE0.htm](spells/common-03-ZYoC630tNGutgbE0.htm)|Hypercognition|Hypercognition|officielle|
|[common-04-07xYlmGX32XtHGEt.htm](spells/common-04-07xYlmGX32XtHGEt.htm)|Vampiric Maiden|Vierge vampirique|libre|
|[common-04-0fYE64odlKqISzft.htm](spells/common-04-0fYE64odlKqISzft.htm)|Rusting Grasp|Poigne oxydante|libre|
|[common-04-1NLMPmyCB2MBoCuR.htm](spells/common-04-1NLMPmyCB2MBoCuR.htm)|Tortoise and the Hare|La tortue et le lièvre|libre|
|[common-04-2BV2yYPfVJ5zirZt.htm](spells/common-04-2BV2yYPfVJ5zirZt.htm)|Mountain Resilience|Résilience du rocher|libre|
|[common-04-3qz32XdiugYhsr32.htm](spells/common-04-3qz32XdiugYhsr32.htm)|Glass Form|Forme de verre|libre|
|[common-04-3xD8DYrr8YDVYGg7.htm](spells/common-04-3xD8DYrr8YDVYGg7.htm)|Spike Stones|Pointes de pierre|libre|
|[common-04-4y2DMq9DV5HvmnxC.htm](spells/common-04-4y2DMq9DV5HvmnxC.htm)|Chroma Leach|Siphon chromatique|libre|
|[common-04-5zeSxzDxMSr0wgSF.htm](spells/common-04-5zeSxzDxMSr0wgSF.htm)|Sanguine Mist|Brume sanguine|libre|
|[common-04-6gEKZaGdH842RF3J.htm](spells/common-04-6gEKZaGdH842RF3J.htm)|Ymeri's Mark|Marque d'Yméri|libre|
|[common-04-6nTBr5XNuKOuPM5m.htm](spells/common-04-6nTBr5XNuKOuPM5m.htm)|Foul Miasma|Miasmes infâmes|libre|
|[common-04-6XscENSWyYRHqK0A.htm](spells/common-04-6XscENSWyYRHqK0A.htm)|Airlift|Envolée|libre|
|[common-04-7tR29sQt35NfIWqN.htm](spells/common-04-7tR29sQt35NfIWqN.htm)|Anathematic Reprisal|Représailles anathèmatiques|libre|
|[common-04-8M03UxGXjYyDFAoy.htm](spells/common-04-8M03UxGXjYyDFAoy.htm)|Weapon Storm|Tempête d'armes|libre|
|[common-04-A2JfEKe6BZcTG1S8.htm](spells/common-04-A2JfEKe6BZcTG1S8.htm)|Fly|Vol|officielle|
|[common-04-A7w3YQBrFNH8KYsB.htm](spells/common-04-A7w3YQBrFNH8KYsB.htm)|Mirror's Misfortune|Malchance du miroir|libre|
|[common-04-a8xXWCQhIR7o6IvP.htm](spells/common-04-a8xXWCQhIR7o6IvP.htm)|Invisibility Curtain|Couverture d'invisibilité|libre|
|[common-04-aqRYNoSvxsVfqglH.htm](spells/common-04-aqRYNoSvxsVfqglH.htm)|Unfettered Movement|Mouvement sans entrave|officielle|
|[common-04-b5sGjGlBf58f8jn0.htm](spells/common-04-b5sGjGlBf58f8jn0.htm)|Air Walk|Marche dans les airs|officielle|
|[common-04-bAxjHBi37CVwInfU.htm](spells/common-04-bAxjHBi37CVwInfU.htm)|Stifling Stillness|Immobilité étouffante|libre|
|[common-04-caehfpQz7yp9yNzz.htm](spells/common-04-caehfpQz7yp9yNzz.htm)|Dutiful Challenge|Défi de dévouement|libre|
|[common-04-cOwSsSXRsBaXUvlr.htm](spells/common-04-cOwSsSXRsBaXUvlr.htm)|Stasis|Stase|libre|
|[common-04-D31YX7zvRBvenTAz.htm](spells/common-04-D31YX7zvRBvenTAz.htm)|Petal Storm|Tempête de pétales|libre|
|[common-04-DcmWrD0V5PWQQyDm.htm](spells/common-04-DcmWrD0V5PWQQyDm.htm)|Variable Gravity|Gravité variable|libre|
|[common-04-DZ9bzXYqMjAK9TzC.htm](spells/common-04-DZ9bzXYqMjAK9TzC.htm)|Holy Cascade|Cascade bénite|libre|
|[common-04-e36Z2t6tLdW3RUzZ.htm](spells/common-04-e36Z2t6tLdW3RUzZ.htm)|Elemental Gift|Don élémentaire|libre|
|[common-04-eexkxcqnkXazsGfK.htm](spells/common-04-eexkxcqnkXazsGfK.htm)|Enervation|Affaiblissement|libre|
|[common-04-EGTTGI1pkmsVPqHp.htm](spells/common-04-EGTTGI1pkmsVPqHp.htm)|Winning Streak|Série victorieuse|libre|
|[common-04-ER1LOEgCLtmEKd05.htm](spells/common-04-ER1LOEgCLtmEKd05.htm)|Ravenous Portal|Portail affamé|libre|
|[common-04-Ey4pKybxyPAxq0ew.htm](spells/common-04-Ey4pKybxyPAxq0ew.htm)|Rigid Form|Forme rigide|libre|
|[common-04-fH08MI4KP0KH2EQ9.htm](spells/common-04-fH08MI4KP0KH2EQ9.htm)|Soothing Spring|Source apaisante|libre|
|[common-04-FHfrCABOmGVaEpPx.htm](spells/common-04-FHfrCABOmGVaEpPx.htm)|Life-Draining Roots|Racines draineuses de vie|libre|
|[common-04-FL1OxeOUDzsJYfY4.htm](spells/common-04-FL1OxeOUDzsJYfY4.htm)|Zephyr Slip|Glissement zéphir|libre|
|[common-04-FSu6ZKxr3xdS75wq.htm](spells/common-04-FSu6ZKxr3xdS75wq.htm)|Roar of the Wyrm|Rugissement du Ver|libre|
|[common-04-gfKhtVsXF3HKSdmY.htm](spells/common-04-gfKhtVsXF3HKSdmY.htm)|Seal Fate|Sceller le destin|libre|
|[common-04-GoKkejPj5yWJPIPK.htm](spells/common-04-GoKkejPj5yWJPIPK.htm)|Adaptive Ablation|Dissonnance adaptative|libre|
|[common-04-h7RtKSKGViNtD5o4.htm](spells/common-04-h7RtKSKGViNtD5o4.htm)|Coral Eruption|Éruption corallienne|libre|
|[common-04-Hb8GdAhP0zBCv3zU.htm](spells/common-04-Hb8GdAhP0zBCv3zU.htm)|Chromatic Ray|Rayon chromatique|libre|
|[common-04-HisaZTk67YAxLGBq.htm](spells/common-04-HisaZTk67YAxLGBq.htm)|Ephemeral Hazards|Dangers éphémères|libre|
|[common-04-HqTI6wRrck1YXp3F.htm](spells/common-04-HqTI6wRrck1YXp3F.htm)|Telepathy|Télépathie|officielle|
|[common-04-HvBpSeUDg0KcK2Hg.htm](spells/common-04-HvBpSeUDg0KcK2Hg.htm)|Ocular Overload|Surcharge oculaire|libre|
|[common-04-hVU9msO9yGkxKZ3J.htm](spells/common-04-hVU9msO9yGkxKZ3J.htm)|Divine Wrath|Colère divine|libre|
|[common-04-IarZrgCeaiUqOuRu.htm](spells/common-04-IarZrgCeaiUqOuRu.htm)|Wall of Fire|Mur de feu|officielle|
|[common-04-ikSb3LRGnrwXJBVX.htm](spells/common-04-ikSb3LRGnrwXJBVX.htm)|Vital Beacon|Fanal de vie|officielle|
|[common-04-iMmexY6ZosLS4I5R.htm](spells/common-04-iMmexY6ZosLS4I5R.htm)|Door to Beyond|Porte vers l'au-delà|libre|
|[common-04-Jmxru8zMdYMRuO5n.htm](spells/common-04-Jmxru8zMdYMRuO5n.htm)|Vision of Death|Vision de mort|libre|
|[common-04-JyxTmqjYYn63V5LY.htm](spells/common-04-JyxTmqjYYn63V5LY.htm)|Honeyed Words|Paroles mielleuses|libre|
|[common-04-K1wmI4qPmRhFczmy.htm](spells/common-04-K1wmI4qPmRhFczmy.htm)|Dust Storm|Tempête de poussière|libre|
|[common-04-KG7amdeXWc7MjGXe.htm](spells/common-04-KG7amdeXWc7MjGXe.htm)|Asterism|Astérisme|libre|
|[common-04-KhM8MhoUgoUjBMIz.htm](spells/common-04-KhM8MhoUgoUjBMIz.htm)|Dinosaur Form|Forme de dinosaure|officielle|
|[common-04-KSAEhNfZyXMO7Z7V.htm](spells/common-04-KSAEhNfZyXMO7Z7V.htm)|Outcast's Curse|Malédiction du paria|officielle|
|[common-04-ksLCg62cLOojw3gN.htm](spells/common-04-ksLCg62cLOojw3gN.htm)|Planar Tether|Bride planaire|libre|
|[common-04-LiGbewa9pO0yjbsY.htm](spells/common-04-LiGbewa9pO0yjbsY.htm)|Confusion|Confusion|officielle|
|[common-04-lJdAvY1sJyEmNDc6.htm](spells/common-04-lJdAvY1sJyEmNDc6.htm)|Phoenix Ward|Protection du phénix|libre|
|[common-04-McnPlLFvKtQVXNcG.htm](spells/common-04-McnPlLFvKtQVXNcG.htm)|Shape Stone|Façonnage de la pierre|officielle|
|[common-04-MLdMUOdwSKegOGlo.htm](spells/common-04-MLdMUOdwSKegOGlo.htm)|Dull Ambition|Ambition insipide|libre|
|[common-04-mUz3wqbMQ5JQ06M5.htm](spells/common-04-mUz3wqbMQ5JQ06M5.htm)|Infectious Melody|Mélodie contagieuse|libre|
|[common-04-Mv5L4201uk8hnAtD.htm](spells/common-04-Mv5L4201uk8hnAtD.htm)|Reflective Scales|Écailles réfléchisantes|libre|
|[common-04-n7OgbKme4hNwxVwQ.htm](spells/common-04-n7OgbKme4hNwxVwQ.htm)|Draw the Lightning|Attirer la foudre|libre|
|[common-04-NBSBFHxBm88qxQUy.htm](spells/common-04-NBSBFHxBm88qxQUy.htm)|Chromatic Armor|Armure chromatique|libre|
|[common-04-nrW6lGV4xDMqLS3P.htm](spells/common-04-nrW6lGV4xDMqLS3P.htm)|Remember the Lost|Remembrance des morts|libre|
|[common-04-NzXpEzcZAjuDTZjK.htm](spells/common-04-NzXpEzcZAjuDTZjK.htm)|Aerial Form|Forme aérienne|libre|
|[common-04-Pd2M1XY8EXrSfWgJ.htm](spells/common-04-Pd2M1XY8EXrSfWgJ.htm)|Swarm Form|Forme de nuée|libre|
|[common-04-piMJO6aYeDJbrhEo.htm](spells/common-04-piMJO6aYeDJbrhEo.htm)|Solid Fog|Brouillard dense|libre|
|[common-04-qJQADktwD0x8kLAy.htm](spells/common-04-qJQADktwD0x8kLAy.htm)|Resilient Sphere|Sphère d'isolement|officielle|
|[common-04-qTzvVnAMWL05VitC.htm](spells/common-04-qTzvVnAMWL05VitC.htm)|Painful Vibrations|Vibrations douloureuses|libre|
|[common-04-Qu4IThrk1wpONwjT.htm](spells/common-04-Qu4IThrk1wpONwjT.htm)|Fey Form|Forme de fée|libre|
|[common-04-qwlh6aDgi86U3Q7H.htm](spells/common-04-qwlh6aDgi86U3Q7H.htm)|Suggestion|Suggestion|officielle|
|[common-04-rwWtpZpkNYvypknx.htm](spells/common-04-rwWtpZpkNYvypknx.htm)|Grasp of the Deep|Poigne des profondeurs|libre|
|[common-04-srEKUvym9kDVbEhD.htm](spells/common-04-srEKUvym9kDVbEhD.htm)|Replicate|Réplique|libre|
|[common-04-sUvTGELaggrBSgGm.htm](spells/common-04-sUvTGELaggrBSgGm.htm)|Implement of Destruction|Outil de destruction|libre|
|[common-04-tgIhRUFtgDSELpJE.htm](spells/common-04-tgIhRUFtgDSELpJE.htm)|Spell Immunity|Immunité contre les sorts|officielle|
|[common-04-tlcrVRqW1MSKJ5IC.htm](spells/common-04-tlcrVRqW1MSKJ5IC.htm)|Phantasmal Killer|Assassin imaginaire|officielle|
|[common-04-TLmkJ8ga8s057HBp.htm](spells/common-04-TLmkJ8ga8s057HBp.htm)|Soft Landing|Atterissage en douceur|libre|
|[common-04-TUj8eugNqAvB1vVR.htm](spells/common-04-TUj8eugNqAvB1vVR.htm)|Creation|Création|libre|
|[common-04-Uqj344bezBq3ESdq.htm](spells/common-04-Uqj344bezBq3ESdq.htm)|Nightmare|Cauchemar|officielle|
|[common-04-V8wXOsoejQhe6CyG.htm](spells/common-04-V8wXOsoejQhe6CyG.htm)|Vapor Form|Forme vaporeuse|libre|
|[common-04-vhe9DduqaivMs8FV.htm](spells/common-04-vhe9DduqaivMs8FV.htm)|Ghostly Transcription|Transcription fantomatique|libre|
|[common-04-VlNcjmYyu95vOUe8.htm](spells/common-04-VlNcjmYyu95vOUe8.htm)|Translocate|Translocation|libre|
|[common-04-VuPDHoVEPLbMfCJC.htm](spells/common-04-VuPDHoVEPLbMfCJC.htm)|Bestial Curse|Malédiction bestiale|libre|
|[common-04-VUwpDY4Z91s9QCg0.htm](spells/common-04-VUwpDY4Z91s9QCg0.htm)|Bottle the Storm|Tempête en bouteille|libre|
|[common-04-VXUrO8TwRqBpNzdU.htm](spells/common-04-VXUrO8TwRqBpNzdU.htm)|Bloodspray Curse|Malédiction du sang répandu|libre|
|[common-04-wfqBbQ2RXj4VjKUZ.htm](spells/common-04-wfqBbQ2RXj4VjKUZ.htm)|Elemental Sense|Perception élémentaire|libre|
|[common-04-wjJW9hWY5CkkMvY5.htm](spells/common-04-wjJW9hWY5CkkMvY5.htm)|Diamond Dust|Poussière de diamant|libre|
|[common-04-Y3G6Y6EDgCY0s3fq.htm](spells/common-04-Y3G6Y6EDgCY0s3fq.htm)|Hydraulic Torrent|Torrent hydraulique|libre|
|[common-04-ykyKclKTCMp2SFXa.htm](spells/common-04-ykyKclKTCMp2SFXa.htm)|Countless Eyes|Yeux innombrables|libre|
|[common-04-YrzBLPLd3r9m6t1p.htm](spells/common-04-YrzBLPLd3r9m6t1p.htm)|Fire Shield|Bouclier de feu|libre|
|[common-04-yY1H5zhO5dHmD8lz.htm](spells/common-04-yY1H5zhO5dHmD8lz.htm)|Font of Serenity|Fontaine de sérénité|libre|
|[common-04-zR67Rt3UMHKC5evy.htm](spells/common-04-zR67Rt3UMHKC5evy.htm)|Flicker|Oscillation|libre|
|[common-04-zvKWclOZ7A53DObE.htm](spells/common-04-zvKWclOZ7A53DObE.htm)|Clairvoyance|Clairvoyance|officielle|
|[common-05-1b55SgYTV65JvmQd.htm](spells/common-05-1b55SgYTV65JvmQd.htm)|Blessing of Defiance|Bénédiction du défi|libre|
|[common-05-1bw6XJMOERcbC5Iq.htm](spells/common-05-1bw6XJMOERcbC5Iq.htm)|Rip the Spirit|Arracher l'esprit|libre|
|[common-05-1K6AYGisvo9gqdhs.htm](spells/common-05-1K6AYGisvo9gqdhs.htm)|Elemental Form|Forme élémentaire|libre|
|[common-05-2w4OpAGihn1JSHFD.htm](spells/common-05-2w4OpAGihn1JSHFD.htm)|Black Tentacles|Tentacules noirs|libre|
|[common-05-3526YUgCvB5ZsKuX.htm](spells/common-05-3526YUgCvB5ZsKuX.htm)|Mantle of the Unwavering Heart|Manteau du coeur inébranlable|libre|
|[common-05-3E1p58MNQMO4GAxt.htm](spells/common-05-3E1p58MNQMO4GAxt.htm)|Stagnate Time|Stagnation du temps|libre|
|[common-05-3puDanGfpEt6jK5k.htm](spells/common-05-3puDanGfpEt6jK5k.htm)|Cone of Cold|Cône de froid|officielle|
|[common-05-59NR1hA2jPSgg2sW.htm](spells/common-05-59NR1hA2jPSgg2sW.htm)|Blister|Pustule|libre|
|[common-05-5tpk4Q2QI3FVhm99.htm](spells/common-05-5tpk4Q2QI3FVhm99.htm)|Rewinding Step|Retour en arrière|libre|
|[common-05-6AqH5SGchbdhOJxA.htm](spells/common-05-6AqH5SGchbdhOJxA.htm)|Flammable Fumes|Émanations inflammables|libre|
|[common-05-8THDHP0UC7SgOYYF.htm](spells/common-05-8THDHP0UC7SgOYYF.htm)|Inevitable Disaster|Désastre inévitable|libre|
|[common-05-A16eFTRh82xIjMu8.htm](spells/common-05-A16eFTRh82xIjMu8.htm)|Divine Immolation|Immolation divine|libre|
|[common-05-bay4AfSu2iIozNNW.htm](spells/common-05-bay4AfSu2iIozNNW.htm)|Banishment|Bannissement|libre|
|[common-05-BilnTGuXrof9Dt9D.htm](spells/common-05-BilnTGuXrof9Dt9D.htm)|Synaptic Pulse|Pulsation synaptique|officielle|
|[common-05-BoA00y45uDlmou07.htm](spells/common-05-BoA00y45uDlmou07.htm)|Secret Chest|Coffre secret|libre|
|[common-05-CcXtbTxtdV9MKQfu.htm](spells/common-05-CcXtbTxtdV9MKQfu.htm)|Mantle of the Frozen Heart|Manteau du coeur gelé|libre|
|[common-05-CuRat8IXBUu9C3Yw.htm](spells/common-05-CuRat8IXBUu9C3Yw.htm)|Portrait of the Artist|Portrait de l'artiste|libre|
|[common-05-CzjQtkRuRlzRvwzg.htm](spells/common-05-CzjQtkRuRlzRvwzg.htm)|Healing Well|Puits de guérison|libre|
|[common-05-d9sBzPOXX3KT8uTu.htm](spells/common-05-d9sBzPOXX3KT8uTu.htm)|Grisly Growths|Horribles excroissances|libre|
|[common-05-E3X2RbzWHCdz7gsk.htm](spells/common-05-E3X2RbzWHCdz7gsk.htm)|Flame Strike|Colonne de feu|officielle|
|[common-05-e4c73RBCQAZdYxau.htm](spells/common-05-e4c73RBCQAZdYxau.htm)|Pressure Zone|Zone de pression|libre|
|[common-05-Ek5XI0aEdZhBgm21.htm](spells/common-05-Ek5XI0aEdZhBgm21.htm)|Scouting Eye|Oeil éclaireur|libre|
|[common-05-fg5ZlSVXzGHNJfbO.htm](spells/common-05-fg5ZlSVXzGHNJfbO.htm)|Elemental Breath|Souffle élémentaire|libre|
|[common-05-Fu8Ml47ZfXpSYe7E.htm](spells/common-05-Fu8Ml47ZfXpSYe7E.htm)|Aberrant Form|Forme d'aberration|libre|
|[common-05-GaRQlC9Yw1BGKHfN.htm](spells/common-05-GaRQlC9Yw1BGKHfN.htm)|Wave of Despair|Vague de désespoir|libre|
|[common-05-gasOL6a0WhOhUsgL.htm](spells/common-05-gasOL6a0WhOhUsgL.htm)|Etheric Shards|Échardes éthérées|libre|
|[common-05-GP3wewkQXEPrLxYj.htm](spells/common-05-GP3wewkQXEPrLxYj.htm)|Subconscious Suggestion|Suggestion subconsciente|officielle|
|[common-05-Hnc7eGi7vyZenAIm.htm](spells/common-05-Hnc7eGi7vyZenAIm.htm)|Breath of Life|Souffle de vie|libre|
|[common-05-i88Vi47PDDoP6gQv.htm](spells/common-05-i88Vi47PDDoP6gQv.htm)|Geyser|Geyser|libre|
|[common-05-IqJ9URobmJ9L9UBG.htm](spells/common-05-IqJ9URobmJ9L9UBG.htm)|Shadow Blast|Explosion d'ombre|libre|
|[common-05-j2kfNBFpLa0JjVr8.htm](spells/common-05-j2kfNBFpLa0JjVr8.htm)|Mantle of the Magma Heart|Manteau du coeur magmatique|libre|
|[common-05-kOa055FIrO9Smnya.htm](spells/common-05-kOa055FIrO9Smnya.htm)|Wall of Stone|Mur de pierre|officielle|
|[common-05-lETZeqBPoj2htGVk.htm](spells/common-05-lETZeqBPoj2htGVk.htm)|Quicken Time|Temps accéléré|libre|
|[common-05-LzfrBDxxPTiuN7uL.htm](spells/common-05-LzfrBDxxPTiuN7uL.htm)|Transmute Rock And Mud|Transmutation de la roche et de la boue|libre|
|[common-05-m4Mc5XbdML92BKOE.htm](spells/common-05-m4Mc5XbdML92BKOE.htm)|Strange Geometry|Géométrie étrange|libre|
|[common-05-MlpbeZ61Euhl0d60.htm](spells/common-05-MlpbeZ61Euhl0d60.htm)|Toxic Cloud|Nuage toxique|officielle|
|[common-05-N9KjnGyVMKgqKcCw.htm](spells/common-05-N9KjnGyVMKgqKcCw.htm)|Bandit's Doom|Malheur du bandit|libre|
|[common-05-OAt2ZEns1gIOCgrn.htm](spells/common-05-OAt2ZEns1gIOCgrn.htm)|Synesthesia|Synesthésie|libre|
|[common-05-oXeEbcUdgJGWHGEJ.htm](spells/common-05-oXeEbcUdgJGWHGEJ.htm)|Impaling Spike|Pique empaleuse|libre|
|[common-05-OYOCWWefZPEfp8Nl.htm](spells/common-05-OYOCWWefZPEfp8Nl.htm)|Blazing Fissure|Fissure brûlante|libre|
|[common-05-p6ebe2PliRkGbmiV.htm](spells/common-05-p6ebe2PliRkGbmiV.htm)|Flame Dancer|Danseur enflammé|libre|
|[common-05-R5FHRv7VqyRnxg2t.htm](spells/common-05-R5FHRv7VqyRnxg2t.htm)|Wall of Ice|Mur de glace|officielle|
|[common-05-R9xqCBblkS5KE4y7.htm](spells/common-05-R9xqCBblkS5KE4y7.htm)|Sending|Communication à distance|officielle|
|[common-05-TCk2MDwf5L5OYjFC.htm](spells/common-05-TCk2MDwf5L5OYjFC.htm)|Cloak of Colors|Cape colorée|libre|
|[common-05-TDaMnCtZ72uyYrz8.htm](spells/common-05-TDaMnCtZ72uyYrz8.htm)|Blink Charge|Charge clignotante|libre|
|[common-05-TLqMFgCewxuricJw.htm](spells/common-05-TLqMFgCewxuricJw.htm)|Repelling Pulse|Pulsation répulsive|libre|
|[common-05-U58aQWJ47VrI36yP.htm](spells/common-05-U58aQWJ47VrI36yP.htm)|Hallucination|Hallucination|libre|
|[common-05-Ucf8eynbZMfUucjE.htm](spells/common-05-Ucf8eynbZMfUucjE.htm)|Illusory Scene|Scène illusoire|libre|
|[common-05-UcS6Tw9OuOAEDfzg.htm](spells/common-05-UcS6Tw9OuOAEDfzg.htm)|Mantle of the Melting Heart|Manteau du coeur fondant|libre|
|[common-05-VDK5cQ94BszDrMiJ.htm](spells/common-05-VDK5cQ94BszDrMiJ.htm)|Entwined Roots|Racines entremêlées|libre|
|[common-05-xFp4EwVcYwSG336t.htm](spells/common-05-xFp4EwVcYwSG336t.htm)|Slither|Nid de serpent|libre|
|[common-05-xPrbxyOEwy9QaPVn.htm](spells/common-05-xPrbxyOEwy9QaPVn.htm)|Chameleon Coat|Couverture caméléon|libre|
|[common-05-xxWhyl81w3ckslAU.htm](spells/common-05-xxWhyl81w3ckslAU.htm)|Howling Blizzard|Blizzard hurlant|libre|
|[common-05-y0Vy7iNL3ET8K00C.htm](spells/common-05-y0Vy7iNL3ET8K00C.htm)|Dreaming Potential|Potentiel onirique|libre|
|[common-05-YIMampGpij4Y30yE.htm](spells/common-05-YIMampGpij4Y30yE.htm)|Speak with Stones|Discuter avec les pierres|libre|
|[common-05-YtesyvfAIwXOqISq.htm](spells/common-05-YtesyvfAIwXOqISq.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[common-05-YvXKGlHOt7mdW2jZ.htm](spells/common-05-YvXKGlHOt7mdW2jZ.htm)|Death Ward|Protection contre la mort|libre|
|[common-05-z2mfh3oPnfYqXflY.htm](spells/common-05-z2mfh3oPnfYqXflY.htm)|Mariner's Curse|Malédiction du marin|libre|
|[common-05-zCcfPS4y5SrZzU2x.htm](spells/common-05-zCcfPS4y5SrZzU2x.htm)|Plant Form|Forme de plante|libre|
|[common-05-ZClfmMoKG3E926Uq.htm](spells/common-05-ZClfmMoKG3E926Uq.htm)|Flames of Ego|Flammes d'égo|libre|
|[common-05-zCLyETFoPqCQXLVy.htm](spells/common-05-zCLyETFoPqCQXLVy.htm)|Flowing Strike|Frappe fluide|libre|
|[common-05-ZLLY6ThJXCCrO0rL.htm](spells/common-05-ZLLY6ThJXCCrO0rL.htm)|Wall of Flesh|Mur de chair|libre|
|[common-05-ZW8ovbu1etdfMre3.htm](spells/common-05-ZW8ovbu1etdfMre3.htm)|Acid Storm|Tempête d'acide|libre|
|[common-06-0XP2XOxT9VSiXFDr.htm](spells/common-06-0XP2XOxT9VSiXFDr.htm)|Phantasmal Calamity|Calamité imaginaire|officielle|
|[common-06-2hMy9ROM2dOZB8Jq.htm](spells/common-06-2hMy9ROM2dOZB8Jq.htm)|Lignify|Lignifier|libre|
|[common-06-5BbU1V6wGSGbrmRD.htm](spells/common-06-5BbU1V6wGSGbrmRD.htm)|Never Mind|Écervelé|libre|
|[common-06-5c692cCcTDXjSEzk.htm](spells/common-06-5c692cCcTDXjSEzk.htm)|Dragon Form|Forme de dragon|libre|
|[common-06-6lO6uMQxbqmYho0e.htm](spells/common-06-6lO6uMQxbqmYho0e.htm)|Necrotize|Nécroser|libre|
|[common-06-7Iela4GgVeO3LfAo.htm](spells/common-06-7Iela4GgVeO3LfAo.htm)|Wall of Force|Mur de force|libre|
|[common-06-7PnjDM52lb4LEqHR.htm](spells/common-06-7PnjDM52lb4LEqHR.htm)|Arrow Salvo|Salve de flèches|libre|
|[common-06-8Umt1AzYfFbC4fui.htm](spells/common-06-8Umt1AzYfFbC4fui.htm)|Spellwrack|Ravages magiques|libre|
|[common-06-9W2Qv0wXLg6tQg3y.htm](spells/common-06-9W2Qv0wXLg6tQg3y.htm)|Scintillating Safeguard|Précaution scintillante|libre|
|[common-06-aSDfGcUOUVGU5m1g.htm](spells/common-06-aSDfGcUOUVGU5m1g.htm)|Daemon Form|Forme de daémon|libre|
|[common-06-ayRXv0wQH00TTNZe.htm](spells/common-06-ayRXv0wQH00TTNZe.htm)|Purple Worm Sting|Dard du ver pourpre|officielle|
|[common-06-c3XygMbzrZMgV1y3.htm](spells/common-06-c3XygMbzrZMgV1y3.htm)|Collective Transposition|Transposition collective|libre|
|[common-06-CB5TlGv5ZghtMifi.htm](spells/common-06-CB5TlGv5ZghtMifi.htm)|Wall of Metal|Mur de métal|libre|
|[common-06-dKbv80rlAWVpz83C.htm](spells/common-06-dKbv80rlAWVpz83C.htm)|Demon Form|Forme de démon|libre|
|[common-06-dN8QBNuTiaBHCKUe.htm](spells/common-06-dN8QBNuTiaBHCKUe.htm)|Cursed Metamorphosis|Métamorphose maudite|libre|
|[common-06-dR53f0p15EW7D0xC.htm](spells/common-06-dR53f0p15EW7D0xC.htm)|Devil Form|Forme de diable|libre|
|[common-06-fd31tAHSSGXyOxW6.htm](spells/common-06-fd31tAHSSGXyOxW6.htm)|Vampiric Exsanguination|Saignée du vampire|officielle|
|[common-06-FgRpddPORsRFwNoX.htm](spells/common-06-FgRpddPORsRFwNoX.htm)|Chromatic Image|Image chromatique|libre|
|[common-06-Gn5deIoobHpd3SiR.htm](spells/common-06-Gn5deIoobHpd3SiR.htm)|Flame Vortex|Vortex de flammes|libre|
|[common-06-GYD0XZ4t3tQq6shc.htm](spells/common-06-GYD0XZ4t3tQq6shc.htm)|Zealous Conviction|Conviction zélée|officielle|
|[common-06-JbAcSLu62TU1OgNF.htm](spells/common-06-JbAcSLu62TU1OgNF.htm)|Tangling Creepers|Entraves végétales|libre|
|[common-06-jj5d830iUi2ZlQfs.htm](spells/common-06-jj5d830iUi2ZlQfs.htm)|Blessed Boundary|Frontière bénie|libre|
|[common-06-k0qJfSZH5xUEggwU.htm](spells/common-06-k0qJfSZH5xUEggwU.htm)|Suspended Retribution|Châtiment suspendu|libre|
|[common-06-K2bTUhucPyhXlzjw.htm](spells/common-06-K2bTUhucPyhXlzjw.htm)|Personal Ocean|Océan personnelle|libre|
|[common-06-kuoYff1csM5eAcAP.htm](spells/common-06-kuoYff1csM5eAcAP.htm)|Fire Seeds|Germes de feu|libre|
|[common-06-m34WOIGZCEg1h76G.htm](spells/common-06-m34WOIGZCEg1h76G.htm)|Blanket Of Stars|Couverture d'étoiles|libre|
|[common-06-MLYTWHfewOpI3Cz7.htm](spells/common-06-MLYTWHfewOpI3Cz7.htm)|Field of Razors|Champ de rasoirs|libre|
|[common-06-MMCQsh12TPaDdPbV.htm](spells/common-06-MMCQsh12TPaDdPbV.htm)|Righteous Might|Force du colosse|officielle|
|[common-06-NhGXgmI3AjkkwnPk.htm](spells/common-06-NhGXgmI3AjkkwnPk.htm)|Blinding Fury|Furie aveuglante|libre|
|[common-06-OdqM06M0wDUqZWiR.htm](spells/common-06-OdqM06M0wDUqZWiR.htm)|Cast into Time|Balancer dans le temps|libre|
|[common-06-OOMQ8Z6EIrGt5NJ6.htm](spells/common-06-OOMQ8Z6EIrGt5NJ6.htm)|Unexpected Transposition|Transposition inattendue|libre|
|[common-06-peCF6VArm8urfwxZ.htm](spells/common-06-peCF6VArm8urfwxZ.htm)|Blade Barrier|Barrière de lames|officielle|
|[common-06-PHVHBbdHeQRfjLmE.htm](spells/common-06-PHVHBbdHeQRfjLmE.htm)|Spirit Blast|Coup spirituel|officielle|
|[common-06-Pr1ruNTbzGn3H9w5.htm](spells/common-06-Pr1ruNTbzGn3H9w5.htm)|Stone to Flesh|Pierre en chair|officielle|
|[common-06-TDNlDWbYb58Y55Da.htm](spells/common-06-TDNlDWbYb58Y55Da.htm)|Chain Lightning|Éclairs multiples|officielle|
|[common-06-u5FAOOXuqK7fLqKW.htm](spells/common-06-u5FAOOXuqK7fLqKW.htm)|Tree of Seasons|Arbre des saisons|libre|
|[common-06-uqlxMQQeSGWEVjki.htm](spells/common-06-uqlxMQQeSGWEVjki.htm)|Truesight|Vision véritable|officielle|
|[common-06-yrZA4k2VAqEP8xx7.htm](spells/common-06-yrZA4k2VAqEP8xx7.htm)|Repulsion|Répulsion|libre|
|[common-06-YtBZq49N4Um1cwm7.htm](spells/common-06-YtBZq49N4Um1cwm7.htm)|Nature's Reprisal|Réprésailles de la nature|libre|
|[common-06-znv4ECL7ZtuiagtA.htm](spells/common-06-znv4ECL7ZtuiagtA.htm)|Petrify|Pétrifier|libre|
|[common-07-0jadeyQIItIuRgeH.htm](spells/common-07-0jadeyQIItIuRgeH.htm)|Eclipse Burst|Éclipse soudaine|libre|
|[common-07-2uEuYmlyx1R7zQeH.htm](spells/common-07-2uEuYmlyx1R7zQeH.htm)|Pollen Pods|Gousses polliniques|libre|
|[common-07-2Vkd1IxylPceUAAF.htm](spells/common-07-2Vkd1IxylPceUAAF.htm)|Regenerate|Régénération|officielle|
|[common-07-4nEY2BcV85wavKLO.htm](spells/common-07-4nEY2BcV85wavKLO.htm)|Prismatic Armor|Armure prismatique|libre|
|[common-07-8kJbiBEjMWG4VUjs.htm](spells/common-07-8kJbiBEjMWG4VUjs.htm)|Warp Mind|Déformer l'esprit|libre|
|[common-07-9aFg4EuOBVz0i3Lb.htm](spells/common-07-9aFg4EuOBVz0i3Lb.htm)|Corrosive Body|Corps corrosif|libre|
|[common-07-a3aQxCpoj1q1NQxC.htm](spells/common-07-a3aQxCpoj1q1NQxC.htm)|Sunburst|Éruption solaire|libre|
|[common-07-AlbpWWN87yGegoAF.htm](spells/common-07-AlbpWWN87yGegoAF.htm)|True Target|Cible verrouillée|officielle|
|[common-07-bVtkBJvGLP69qVGI.htm](spells/common-07-bVtkBJvGLP69qVGI.htm)|Unfettered Pack|Meute sans entrave|officielle|
|[common-07-d6o52BnjViNz7Gub.htm](spells/common-07-d6o52BnjViNz7Gub.htm)|Prismatic Spray|Rayon prismatique|officielle|
|[common-07-dKWc83KKiXoIJkhp.htm](spells/common-07-dKWc83KKiXoIJkhp.htm)|Beheading Buzz Saw|Scie à décapiter|libre|
|[common-07-E3zIZ4pjlOuWeGRz.htm](spells/common-07-E3zIZ4pjlOuWeGRz.htm)|Angel Form|Forme d'ange|libre|
|[common-07-G56DJkxlUjFv0C4Z.htm](spells/common-07-G56DJkxlUjFv0C4Z.htm)|Time Beacon|Repère temporel|libre|
|[common-07-ggtZTxeyEnDYbt6f.htm](spells/common-07-ggtZTxeyEnDYbt6f.htm)|Momentary Recovery|Récupération momentanée|libre|
|[common-07-gMbrNFKz9NP3TR4s.htm](spells/common-07-gMbrNFKz9NP3TR4s.htm)|Inexhaustible Cynicism|Cynisme inépuisable|libre|
|[common-07-hiVL8qsnTJtpouw0.htm](spells/common-07-hiVL8qsnTJtpouw0.htm)|Divine Vessel|Réceptacle divin|officielle|
|[common-07-IHUs4qn27RrdFQ5Y.htm](spells/common-07-IHUs4qn27RrdFQ5Y.htm)|Cosmic Form|Forme cosmique|libre|
|[common-07-jBGAYmR0BkkbpJvG.htm](spells/common-07-jBGAYmR0BkkbpJvG.htm)|Visions of Danger|Dangereuses visions|libre|
|[common-07-JLdbyGKhjwAAoRLs.htm](spells/common-07-JLdbyGKhjwAAoRLs.htm)|Tempest of Shades|Tempête d'ombres|libre|
|[common-07-m2xFMNyQiUKQDRaj.htm](spells/common-07-m2xFMNyQiUKQDRaj.htm)|Energy Aegis|Égide énergétique|officielle|
|[common-07-M5dp7ILSCKID9fDK.htm](spells/common-07-M5dp7ILSCKID9fDK.htm)|Shock to the System|Choc pour le système|libre|
|[common-07-O6VQC1Bs4aSYDa6R.htm](spells/common-07-O6VQC1Bs4aSYDa6R.htm)|Mask of Terror|Masque terrifiant|libre|
|[common-07-O7ZEqWjwdKyo2CUv.htm](spells/common-07-O7ZEqWjwdKyo2CUv.htm)|Volcanic Eruption|Eruption volcanique|officielle|
|[common-07-rsZ5c0AUyywe5yoK.htm](spells/common-07-rsZ5c0AUyywe5yoK.htm)|Retrocognition|Rétrocognition|libre|
|[common-07-sX2o0HH4RjJDAZ8C.htm](spells/common-07-sX2o0HH4RjJDAZ8C.htm)|Divine Decree|Décret divin|libre|
|[common-07-uc4I1diSSX6XYzb3.htm](spells/common-07-uc4I1diSSX6XYzb3.htm)|Telekinetic Bombardment|Bombardement télékinésique|libre|
|[common-07-vFkz0gVBUT2gGnm1.htm](spells/common-07-vFkz0gVBUT2gGnm1.htm)|Frigid Flurry|Rafale glacée|libre|
|[common-07-VTb0yI6P1bLkzuRr.htm](spells/common-07-VTb0yI6P1bLkzuRr.htm)|Shadow Raid|Raid de l'ombre|libre|
|[common-07-x9RIFhquazom4p02.htm](spells/common-07-x9RIFhquazom4p02.htm)|Deity's Strike|Frappe de la divinité|libre|
|[common-07-XS7Wyh5YC0NWeWyB.htm](spells/common-07-XS7Wyh5YC0NWeWyB.htm)|Fiery Body|Corps enflammé|officielle|
|[common-07-yWySiasyPZcRoe4d.htm](spells/common-07-yWySiasyPZcRoe4d.htm)|Heaving Earth|Soulèvement de la terre|libre|
|[common-07-Z9OrRXKgAPv6Hn5l.htm](spells/common-07-Z9OrRXKgAPv6Hn5l.htm)|Execute|Exécution|libre|
|[common-08-0AZOIMRvZtePuGOw.htm](spells/common-08-0AZOIMRvZtePuGOw.htm)|Rainbow Fumarole|Fumerole arc-en-ciel|libre|
|[common-08-4MOew29Z1oCX8O28.htm](spells/common-08-4MOew29Z1oCX8O28.htm)|Moment of Renewal|Instant de renouveau|officielle|
|[common-08-89Hj5QuqvcwVXcqj.htm](spells/common-08-89Hj5QuqvcwVXcqj.htm)|Ferrous Form|Forme ferreuse|libre|
|[common-08-8AMvNVOUEtxBCDvJ.htm](spells/common-08-8AMvNVOUEtxBCDvJ.htm)|Monstrosity Form|Forme monstrueuse|libre|
|[common-08-BKIet436snMNcnez.htm](spells/common-08-BKIet436snMNcnez.htm)|Polar Ray|Rayon polaire|officielle|
|[common-08-C2GYCH3TtUFqPfdX.htm](spells/common-08-C2GYCH3TtUFqPfdX.htm)|Arctic Rift|Faille arctique|libre|
|[common-08-fBoiPXmcIO50OFFR.htm](spells/common-08-fBoiPXmcIO50OFFR.htm)|Boil Blood|Sang bouilli|libre|
|[common-08-Ht35SDf9PDStJfoC.htm](spells/common-08-Ht35SDf9PDStJfoC.htm)|Spirit Song|Chant spirituel|officielle|
|[common-08-it4wx2mDIJDlZGqS.htm](spells/common-08-it4wx2mDIJDlZGqS.htm)|Divine Armageddon|Armaggedon divin|libre|
|[common-08-JAaETUBg0xlttpCH.htm](spells/common-08-JAaETUBg0xlttpCH.htm)|Summon Archmage|Convocation d'archimage|libre|
|[common-08-Jvyy6oVIQsD34MHB.htm](spells/common-08-Jvyy6oVIQsD34MHB.htm)|Uncontrollable Dance|Danse incontrôlable|libre|
|[common-08-KtTGLbLG9nqMbUYL.htm](spells/common-08-KtTGLbLG9nqMbUYL.htm)|Divine Inspiration|Inspiration divine|libre|
|[common-08-kVNo3ga0lwLKPrem.htm](spells/common-08-kVNo3ga0lwLKPrem.htm)|Summon Elemental Herald|Convocation de héraut élémentaire|libre|
|[common-08-kWDt0JcKPgX6MvdD.htm](spells/common-08-kWDt0JcKPgX6MvdD.htm)|Burning Blossoms|Pétales brûlants|libre|
|[common-08-M0jQlpQYUr0pp2Sv.htm](spells/common-08-M0jQlpQYUr0pp2Sv.htm)|Desiccate|Dessécher|libre|
|[common-08-MS60WhVifb45qORJ.htm](spells/common-08-MS60WhVifb45qORJ.htm)|Spiritual Epidemic|Épidémie spirituelle|officielle|
|[common-08-Oj1PJBMQD9vuwCv7.htm](spells/common-08-Oj1PJBMQD9vuwCv7.htm)|Quandary|Dilemme|libre|
|[common-08-qlxM7Ik3uUeUIOcv.htm](spells/common-08-qlxM7Ik3uUeUIOcv.htm)|Canticle of Everlasting Grief|Cantique du chagrin éternel|libre|
|[common-08-rwCh2qTYPA44KEoK.htm](spells/common-08-rwCh2qTYPA44KEoK.htm)|Dream Council|Conseil onirique|officielle|
|[common-08-U0hL0LLaprcnAyzC.htm](spells/common-08-U0hL0LLaprcnAyzC.htm)|Migration|Migration|libre|
|[common-08-vm9O7ne48NM72yrJ.htm](spells/common-08-vm9O7ne48NM72yrJ.htm)|Falling Sky|Chute du ciel|libre|
|[common-08-wfleiawxsfhpRRwf.htm](spells/common-08-wfleiawxsfhpRRwf.htm)|Disappearance|Disparition|officielle|
|[common-08-x7SPrsRxGb2Vy2nu.htm](spells/common-08-x7SPrsRxGb2Vy2nu.htm)|Earthquake|Tremblement de terre|libre|
|[common-08-y2cQYLr5mljDSu1G.htm](spells/common-08-y2cQYLr5mljDSu1G.htm)|Unrelenting Observation|Observation implacable|libre|
|[common-09-10VcmSYNBrvBphu1.htm](spells/common-09-10VcmSYNBrvBphu1.htm)|Massacre|Massacre|libre|
|[common-09-2EIUqc8TCTQimggQ.htm](spells/common-09-2EIUqc8TCTQimggQ.htm)|Summon Draconic Legion|Convocation de légion draconique|libre|
|[common-09-coKiMMBLESkaLNVa.htm](spells/common-09-coKiMMBLESkaLNVa.htm)|Proliferating Eyes|Yeux proliférants|libre|
|[common-09-FEsuyf203wTNE2et.htm](spells/common-09-FEsuyf203wTNE2et.htm)|Wails of the Damned|Plainte des damnés|libre|
|[common-09-fkDeKktdmbeplYRY.htm](spells/common-09-fkDeKktdmbeplYRY.htm)|Overwhelming Presence|Présence écrasante|libre|
|[common-09-HHCgEEkeeShVQf8d.htm](spells/common-09-HHCgEEkeeShVQf8d.htm)|Bilocation|Bilocalisation|libre|
|[common-09-hq57j7Nif1zuQ2Ab.htm](spells/common-09-hq57j7Nif1zuQ2Ab.htm)|Unspeakable Shadow|Ombre imprononçable|libre|
|[common-09-ItZgGznUBhgWBwFG.htm](spells/common-09-ItZgGznUBhgWBwFG.htm)|Telepathic Demand|Ordre télépathique|officielle|
|[common-09-jrBa9deU2ULFWvSl.htm](spells/common-09-jrBa9deU2ULFWvSl.htm)|Falling Stars|Chute d'étoiles|libre|
|[common-09-KPDHmmjJiw7PhTYF.htm](spells/common-09-KPDHmmjJiw7PhTYF.htm)|Resplendent Mansion|Manoir resplendissant|libre|
|[common-09-MJx7DmjsWYzDZ3a4.htm](spells/common-09-MJx7DmjsWYzDZ3a4.htm)|Phantasmagoria|Fantasmagorie|libre|
|[common-09-PngDCmU0MXZkbu0v.htm](spells/common-09-PngDCmU0MXZkbu0v.htm)|Prismatic Sphere|Sphère prismatique|officielle|
|[common-09-pswdik31kuHEvdno.htm](spells/common-09-pswdik31kuHEvdno.htm)|Metamorphosis|Métamorphose|libre|
|[common-09-qDjeG6dxT4aEEC6J.htm](spells/common-09-qDjeG6dxT4aEEC6J.htm)|Weird|Ennemi subconscient|officielle|
|[common-09-qsNeG9KZpODSACMq.htm](spells/common-09-qsNeG9KZpODSACMq.htm)|Foresight|Prémonition|officielle|
|[common-09-TbYqDhlNRiWHe146.htm](spells/common-09-TbYqDhlNRiWHe146.htm)|One with the Land|Uni à la terre|libre|
|[common-09-Tc5NLaMu71vrGTJQ.htm](spells/common-09-Tc5NLaMu71vrGTJQ.htm)|Nature's Enmity|Hostilité naturelle|officielle|
|[common-09-ZqmP9gijBmK7y8Xy.htm](spells/common-09-ZqmP9gijBmK7y8Xy.htm)|Weapon of Judgment|Arme du jugement|libre|
|[common-10-1dsahW4g1ggXtypx.htm](spells/common-10-1dsahW4g1ggXtypx.htm)|Freeze Time|Figer le temps|libre|
|[common-10-6s0UW4bujggma9TC.htm](spells/common-10-6s0UW4bujggma9TC.htm)|Fabricated Truth|Vérité fabriquée|officielle|
|[common-10-AuIiqc7jjiy1GZ75.htm](spells/common-10-AuIiqc7jjiy1GZ75.htm)|Manifestation|Manifestation|libre|
|[common-10-ckUOoqOM7Kg7VqxB.htm](spells/common-10-ckUOoqOM7Kg7VqxB.htm)|Avatar|Avatar|libre|
|[common-10-Di9gL6KIZ9eFgbs1.htm](spells/common-10-Di9gL6KIZ9eFgbs1.htm)|Fated Confrontation|Confrontation finale|libre|
|[common-10-hvKtmoHwekDZ5iOH.htm](spells/common-10-hvKtmoHwekDZ5iOH.htm)|Shadow Army|Armée de l'ombre|libre|
|[common-10-pmP8HhXvvEKP3LqU.htm](spells/common-10-pmP8HhXvvEKP3LqU.htm)|Primal Herd|Troupeau primitif|officielle|
|[common-10-UG0SmRYSdbrx2rTA.htm](spells/common-10-UG0SmRYSdbrx2rTA.htm)|Indestructibility|Invulnérabilité|libre|
|[common-10-vQMAdnIwnV9prPiG.htm](spells/common-10-vQMAdnIwnV9prPiG.htm)|Element Embodied|Incarnation élémentaire|libre|
|[common-10-wLIvH0AT1u7oa64N.htm](spells/common-10-wLIvH0AT1u7oa64N.htm)|Cataclysm|Cataclysme|libre|
|[common-10-ZXwxs5tRjEGrjAJT.htm](spells/common-10-ZXwxs5tRjEGrjAJT.htm)|Nature Incarnate|Incarnation de la nature|libre|
|[rare-01-5QD75UbyB5EiG3yz.htm](spells/rare-01-5QD75UbyB5EiG3yz.htm)|Scorching Blast|Souffle brûlant|libre|
|[rare-01-6wLY5LnehCo3tHlr.htm](spells/rare-01-6wLY5LnehCo3tHlr.htm)|Aqueous Blast|Souffle aqueux|libre|
|[rare-01-fxRaWoeOGyi6THYH.htm](spells/rare-01-fxRaWoeOGyi6THYH.htm)|Frenzied Revelry|Festivités frénétiques|libre|
|[rare-01-GzlVI3qYdCtUigLz.htm](spells/rare-01-GzlVI3qYdCtUigLz.htm)|Fey Abeyance|Interdiction des fées|libre|
|[rare-01-k6f5nvSv0XIhbiHj.htm](spells/rare-01-k6f5nvSv0XIhbiHj.htm)|Hollow Heart|Coeur creux|libre|
|[rare-01-L6kr9AUZ8iwuxIip.htm](spells/rare-01-L6kr9AUZ8iwuxIip.htm)|Know Location|Connaître l'emplacement|libre|
|[rare-01-MiTFNcqCI9f34A2V.htm](spells/rare-01-MiTFNcqCI9f34A2V.htm)|Inkshot|Jet d'encre|libre|
|[rare-01-mOUwbIN1SUp8FyPR.htm](spells/rare-01-mOUwbIN1SUp8FyPR.htm)|Cinder Gaze|Regard de cendre|libre|
|[rare-01-t4hGPdh6vAEgBFgZ.htm](spells/rare-01-t4hGPdh6vAEgBFgZ.htm)|Victory Cry|Cri de victoire|libre|
|[rare-01-WFSazGLCUuPsBv1p.htm](spells/rare-01-WFSazGLCUuPsBv1p.htm)|Frost's Touch|Contact du givre|libre|
|[rare-01-xM35hJacTM1BSXUl.htm](spells/rare-01-xM35hJacTM1BSXUl.htm)|Silver's Refrain|Refrain d'argent|libre|
|[rare-01-yp4w9SG4RuqRM8KD.htm](spells/rare-01-yp4w9SG4RuqRM8KD.htm)|Spirit Object|Objet animé|libre|
|[rare-02-bZ8BibvvYOKAbYjn.htm](spells/rare-02-bZ8BibvvYOKAbYjn.htm)|Blast of the Bellows|Souffle des soufflets|libre|
|[rare-02-H4oF5szC7aogqtvw.htm](spells/rare-02-H4oF5szC7aogqtvw.htm)|Worm's Repast|Repas du ver|libre|
|[rare-02-lrFKkzgz80B5vTBb.htm](spells/rare-02-lrFKkzgz80B5vTBb.htm)|Binding Muzzle|Muselière fixée|libre|
|[rare-02-NacrNSvfODxpZena.htm](spells/rare-02-NacrNSvfODxpZena.htm)|Blazing Blade|Lame ardente|libre|
|[rare-02-oYxxcgszpT242sEi.htm](spells/rare-02-oYxxcgszpT242sEi.htm)|Winter's Breath|Souffle de l'hiver|libre|
|[rare-02-PvkEzzCaDT07DcJb.htm](spells/rare-02-PvkEzzCaDT07DcJb.htm)|Open the Wall of Ghosts|Ouvrir le Mur des fantômes|libre|
|[rare-02-yrsgOdj5oqp7lEQY.htm](spells/rare-02-yrsgOdj5oqp7lEQY.htm)|Gnaw at the Moon|Grignoter la lune|libre|
|[rare-03-32NpJbPblay77K6v.htm](spells/rare-03-32NpJbPblay77K6v.htm)|Tattoo Whispers|Murmures tatoués|libre|
|[rare-03-57ulIxg3Of2wCbEh.htm](spells/rare-03-57ulIxg3Of2wCbEh.htm)|Electrified Crystal Ward|Glyphe de cristal électrique|officielle|
|[rare-03-9gMQPCaFM27PEIh4.htm](spells/rare-03-9gMQPCaFM27PEIh4.htm)|Return the Favor|Rendre la faveur|libre|
|[rare-03-e4vxRJ4sjUXMEIGP.htm](spells/rare-03-e4vxRJ4sjUXMEIGP.htm)|Fate's Travels|Voyages du destin|libre|
|[rare-03-Irlpzl1YBSJ6xF5e.htm](spells/rare-03-Irlpzl1YBSJ6xF5e.htm)|Deep Sight|Vision des profondeurs|libre|
|[rare-03-jrlCEw9rqBi4qU6g.htm](spells/rare-03-jrlCEw9rqBi4qU6g.htm)|Ki Cutting Sight|Vision tranchante du ki|libre|
|[rare-03-VJgSQBYwXGDbMmiW.htm](spells/rare-03-VJgSQBYwXGDbMmiW.htm)|Mindscape Shift|Changement de paysage mental|libre|
|[rare-03-YZnLggKDHkMY4cnw.htm](spells/rare-03-YZnLggKDHkMY4cnw.htm)|Transcribe Conflict|Conflit retranscrit|libre|
|[rare-04-11haL5O9KMpY5Fv7.htm](spells/rare-04-11haL5O9KMpY5Fv7.htm)|Entangle Fate|Entremêler le destin|libre|
|[rare-04-3ySPK8qwNcuESwa0.htm](spells/rare-04-3ySPK8qwNcuESwa0.htm)|Purifying Veil|Voile purificateur|libre|
|[rare-04-53Kw9WQtvrEtABEu.htm](spells/rare-04-53Kw9WQtvrEtABEu.htm)|Compel True Name|Forcer le nom véritable|libre|
|[rare-04-5p8naLYjFcG13OkU.htm](spells/rare-04-5p8naLYjFcG13OkU.htm)|Rebounding Barrier|Barrière rebondissante|libre|
|[rare-04-9j35xXKVVtHCh1Pe.htm](spells/rare-04-9j35xXKVVtHCh1Pe.htm)|Shaken Confidence|Confiance ébranlée|libre|
|[rare-04-bqx1tJeOZq1Ufhcc.htm](spells/rare-04-bqx1tJeOZq1Ufhcc.htm)|Recall Past Life|Se souvenir de vies antérieures|libre|
|[rare-04-eONVFSbBiqaDLGey.htm](spells/rare-04-eONVFSbBiqaDLGey.htm)|Wall Of Mirrors|Mur de miroirs|libre|
|[rare-04-fMnjP4hpNRV9EfVM.htm](spells/rare-04-fMnjP4hpNRV9EfVM.htm)|Inevitable Destination|Destination inévitable|libre|
|[rare-04-h5UqEdeqK8iTcU0J.htm](spells/rare-04-h5UqEdeqK8iTcU0J.htm)|Simulacrum|Simulacrum|libre|
|[rare-04-hdzyDrRdHSse88XM.htm](spells/rare-04-hdzyDrRdHSse88XM.htm)|Extract Brain|Extraction du cerveau|libre|
|[rare-04-i06YFOtfGfpUvcD7.htm](spells/rare-04-i06YFOtfGfpUvcD7.htm)|Wind Whispers|Murmures du vent|libre|
|[rare-04-IT1aaqDBAISlHDUV.htm](spells/rare-04-IT1aaqDBAISlHDUV.htm)|Achaekek's Clutch|Étreinte d'Achaékek|libre|
|[rare-04-jqb52wy7A0Eqwuzx.htm](spells/rare-04-jqb52wy7A0Eqwuzx.htm)|Vision of Beauty|Vision de la beauté|libre|
|[rare-04-kREpsv78anJpIiq2.htm](spells/rare-04-kREpsv78anJpIiq2.htm)|Detect Creator|Détection du créateur|libre|
|[rare-04-LX4pCagYLpc9hEji.htm](spells/rare-04-LX4pCagYLpc9hEji.htm)|Aromatic Lure|Leurre aromatique|libre|
|[rare-04-me5E8UvbBrHe3dZh.htm](spells/rare-04-me5E8UvbBrHe3dZh.htm)|Amity Cycle|Cycle d'amitié|libre|
|[rare-04-mer4V7vWdTs1oLbG.htm](spells/rare-04-mer4V7vWdTs1oLbG.htm)|Isolation|Isolement|libre|
|[rare-04-mSM659xN2VIAHiF3.htm](spells/rare-04-mSM659xN2VIAHiF3.htm)|Wordsmith|Façonneur de mots|libre|
|[rare-04-mTpPZIJ2sdgusPP1.htm](spells/rare-04-mTpPZIJ2sdgusPP1.htm)|Euphoric Renewal|Renouveau euphorique|libre|
|[rare-04-n2CM3NeRuq5RSY19.htm](spells/rare-04-n2CM3NeRuq5RSY19.htm)|Prophet's Luck|Chance du prophète|libre|
|[rare-04-oND3oFBjxrhyVvyG.htm](spells/rare-04-oND3oFBjxrhyVvyG.htm)|Community Repair|Réparation communautaire|libre|
|[rare-04-RbORUmnwlB8b3mNf.htm](spells/rare-04-RbORUmnwlB8b3mNf.htm)|Internal Insurrection|Insurrection interne|libre|
|[rare-04-TRpUjBNPQz3Eshaq.htm](spells/rare-04-TRpUjBNPQz3Eshaq.htm)|Weaponize Secret|Douloureux secret|libre|
|[rare-04-u0AtDZs6BhBPtjEs.htm](spells/rare-04-u0AtDZs6BhBPtjEs.htm)|Forgotten Lines|Lignes oubliées|libre|
|[rare-05-DcUGLLyaa9tHH1kN.htm](spells/rare-05-DcUGLLyaa9tHH1kN.htm)|Incarnate Ancestry|Ancêtre incarné|libre|
|[rare-05-dNpIc5k1aCI3bHIg.htm](spells/rare-05-dNpIc5k1aCI3bHIg.htm)|Mosquito Blight|Fléau du moustique|libre|
|[rare-05-I2fwPslQth0DTPQD.htm](spells/rare-05-I2fwPslQth0DTPQD.htm)|Incendiary Fog|Brouillard incendiaire|libre|
|[rare-05-PcmFpaHPCReNp1BD.htm](spells/rare-05-PcmFpaHPCReNp1BD.htm)|Over the Coals|Sur des charbons ardents|libre|
|[rare-05-qVXraYXlTjissCaG.htm](spells/rare-05-qVXraYXlTjissCaG.htm)|Mother's Blessing|Bénédiction de la Mère|libre|
|[rare-05-rnFAHvKpcsU4BJD4.htm](spells/rare-05-rnFAHvKpcsU4BJD4.htm)|Shall not Falter, Shall not Rout|Tu ne faibliras pas, tu ne prendras pas la fuite|libre|
|[rare-05-wrm1zIiLyFD1Is6h.htm](spells/rare-05-wrm1zIiLyFD1Is6h.htm)|Aspirational State|État d'aspiration|libre|
|[rare-05-zaZieDHguhahmM2z.htm](spells/rare-05-zaZieDHguhahmM2z.htm)|Waters of Prediction|Eaux de prédiction|libre|
|[rare-05-ZyREiMaul0VhDYh3.htm](spells/rare-05-ZyREiMaul0VhDYh3.htm)|Glacial Heart|Coeur glacial|libre|
|[rare-06-29AyhknPKiDBcy8s.htm](spells/rare-06-29AyhknPKiDBcy8s.htm)|Statuette|Statuette|libre|
|[rare-06-BazbvgNmK46XjrVc.htm](spells/rare-06-BazbvgNmK46XjrVc.htm)|Word of Revision|Mot de révision|libre|
|[rare-06-FKtPRWcs4zhwn9N1.htm](spells/rare-06-FKtPRWcs4zhwn9N1.htm)|Form of the Sandpoint Devil|Forme de diable de Pointesable|libre|
|[rare-06-FyOgUkq71LNC143w.htm](spells/rare-06-FyOgUkq71LNC143w.htm)|Catch Your Name|Attraper votre nom|libre|
|[rare-06-kLxJx7BECVgHh2vb.htm](spells/rare-06-kLxJx7BECVgHh2vb.htm)|Temporal Ward|Protection temporelle|libre|
|[rare-07-nzbnTqHgNKiGZkrZ.htm](spells/rare-07-nzbnTqHgNKiGZkrZ.htm)|Word of Recall|Mot de Rappel|libre|
|[rare-07-u3pdkfmy0AWICdoM.htm](spells/rare-07-u3pdkfmy0AWICdoM.htm)|Ravenous Reanimation|Réanimation vorace|libre|
|[rare-08-4ONjK2hoMBmuAAyk.htm](spells/rare-08-4ONjK2hoMBmuAAyk.htm)|Summon Irii|Convocation d'Irii|libre|
|[rare-08-fLiowSDQXo3vCQDh.htm](spells/rare-08-fLiowSDQXo3vCQDh.htm)|Rite of the Red Star|Rituel de l'étoile rouge|libre|
|[rare-08-gtWxTfMbIN5RHQw6.htm](spells/rare-08-gtWxTfMbIN5RHQw6.htm)|All is One, One is All|Un est tout, tout est un|libre|
|[rare-08-TvZiwZRianfTSbEg.htm](spells/rare-08-TvZiwZRianfTSbEg.htm)|Hypnopompic Terrors|Terreurs hypnopompiques|libre|
|[rare-08-Y9w4TYnb8cXU4UKY.htm](spells/rare-08-Y9w4TYnb8cXU4UKY.htm)|Split Shadow|Ombre fractionnée|libre|
|[rare-08-ZwwIUavMbEwcZz35.htm](spells/rare-08-ZwwIUavMbEwcZz35.htm)|Create Demiplane|Création de demi-plan|libre|
|[rare-09-AlCFTjSBaCHuRHBv.htm](spells/rare-09-AlCFTjSBaCHuRHBv.htm)|Prismatic Shield|Bouclier prismatique|libre|
|[rare-09-i6GUJCWdNu2278oA.htm](spells/rare-09-i6GUJCWdNu2278oA.htm)|Call Fluxwraith|Convoquer un spectre du flot temporel|libre|
|[rare-09-LEMfFL551YB120RN.htm](spells/rare-09-LEMfFL551YB120RN.htm)|Blunt the Final Blade|Émousser la Lame finale|libre|
|[rare-09-pZr1xrCpaSu6qrXU.htm](spells/rare-09-pZr1xrCpaSu6qrXU.htm)|Clone|Clone|libre|
|[rare-09-tgJTm276cikEL8vU.htm](spells/rare-09-tgJTm276cikEL8vU.htm)|Summon Ancient Fleshforged|Convocation de forgé de chair ancien|libre|
|[rare-09-z39jFoNJrobyn3MQ.htm](spells/rare-09-z39jFoNJrobyn3MQ.htm)|Undertaker|Croque-mort|libre|
|[rare-10-6dDtGIUerazSHIOu.htm](spells/rare-10-6dDtGIUerazSHIOu.htm)|Wish|Souhait|libre|
|[rare-10-dQGn0eAyvYMDdj0h.htm](spells/rare-10-dQGn0eAyvYMDdj0h.htm)|Destroy Mindscape|Détruire un paysage mental|libre|
|[rare-10-FA55Fxf8MBXhje95.htm](spells/rare-10-FA55Fxf8MBXhje95.htm)|Dinosaur Fort|Fort dinosaure|libre|
|[rare-10-WRP8TDf36hqHyGv1.htm](spells/rare-10-WRP8TDf36hqHyGv1.htm)|Summon Kaiju|Convocation de Kaiju|libre|
|[rare-10-X0t0gr7S7CeCt2Q5.htm](spells/rare-10-X0t0gr7S7CeCt2Q5.htm)|Anima Invocation (Modified)|Invocation d'anima modifiée|libre|
|[uncommon-01-0H1ozccQGGFLUwFI.htm](spells/uncommon-01-0H1ozccQGGFLUwFI.htm)|Cry of Destruction|Cri de destruction|officielle|
|[uncommon-01-0JUOgbbFCapp3HlW.htm](spells/uncommon-01-0JUOgbbFCapp3HlW.htm)|Elemental Toss|Projectile élémentaire|officielle|
|[uncommon-01-0xR9vrt6uDFl0Umo.htm](spells/uncommon-01-0xR9vrt6uDFl0Umo.htm)|Untamed Shift|Transformation indomptée|libre|
|[uncommon-01-1lmzILdCFENln8Cy.htm](spells/uncommon-01-1lmzILdCFENln8Cy.htm)|Physical Boost|Amélioration physique|officielle|
|[uncommon-01-1RxPW9I70Nd7wokz.htm](spells/uncommon-01-1RxPW9I70Nd7wokz.htm)|Whispering Quiet|Calme murmurant|libre|
|[uncommon-01-2gQYrCPwBmwau26O.htm](spells/uncommon-01-2gQYrCPwBmwau26O.htm)|Life Link|Lien vital|libre|
|[uncommon-01-3mINzPzup2m9qzFU.htm](spells/uncommon-01-3mINzPzup2m9qzFU.htm)|Sepulchral Mask|Masque sépucral|libre|
|[uncommon-01-4bFxUA59xeq6Snhw.htm](spells/uncommon-01-4bFxUA59xeq6Snhw.htm)|Hallowed Ground|Sol sacré|libre|
|[uncommon-01-53NqGTJOf4LcjVyD.htm](spells/uncommon-01-53NqGTJOf4LcjVyD.htm)|Shielding Strike|Frappe bouclier|libre|
|[uncommon-01-5p9Y4ACrtRM4gTpN.htm](spells/uncommon-01-5p9Y4ACrtRM4gTpN.htm)|Dimensional Assault|Assaut dimensionnel|libre|
|[uncommon-01-5Pc55FGGqVpIAJ62.htm](spells/uncommon-01-5Pc55FGGqVpIAJ62.htm)|Loremaster's Etude|Étude du maître savant|libre|
|[uncommon-01-6GjJtLJnwC18Y0aZ.htm](spells/uncommon-01-6GjJtLJnwC18Y0aZ.htm)|Lift Nature's Caul|Vraie nature|libre|
|[uncommon-01-6UafOE1ZUbHamsZJ.htm](spells/uncommon-01-6UafOE1ZUbHamsZJ.htm)|Dim the Light|Diminuer la lumière|libre|
|[uncommon-01-8bdt1TvNKzsCu9Ct.htm](spells/uncommon-01-8bdt1TvNKzsCu9Ct.htm)|Join Pasts|Joindre les passés|libre|
|[uncommon-01-8RWfKConLYFZpQ9X.htm](spells/uncommon-01-8RWfKConLYFZpQ9X.htm)|Untamed Form|Forme indomptée|libre|
|[uncommon-01-9Ga1AOQdHKYXUY4O.htm](spells/uncommon-01-9Ga1AOQdHKYXUY4O.htm)|Purifying Icicle|Stalactite purificateur|libre|
|[uncommon-01-9mPEoOPN0AMuixIv.htm](spells/uncommon-01-9mPEoOPN0AMuixIv.htm)|Force Fang|Croc de force|libre|
|[uncommon-01-aHu20NHj7YIqxr80.htm](spells/uncommon-01-aHu20NHj7YIqxr80.htm)|Arms of Nature|Armes de la nature|libre|
|[uncommon-01-AiWtiVmyasyL42J8.htm](spells/uncommon-01-AiWtiVmyasyL42J8.htm)|Crushing Ground|Sol écrasant|libre|
|[uncommon-01-aq1yonHeYpbaj3XI.htm](spells/uncommon-01-aq1yonHeYpbaj3XI.htm)|Patron's Puppet|Poupée du patron|libre|
|[uncommon-01-AspA30tzKCHFWRf0.htm](spells/uncommon-01-AspA30tzKCHFWRf0.htm)|Incendiary Aura|Aura incendiaire|libre|
|[uncommon-01-BA143r8fCqmSjdRf.htm](spells/uncommon-01-BA143r8fCqmSjdRf.htm)|Nettleskin|Peau d'épine|libre|
|[uncommon-01-BH3sUerzMb2bWnv1.htm](spells/uncommon-01-BH3sUerzMb2bWnv1.htm)|Call of the Grave|Appel de la tombe|officielle|
|[uncommon-01-BQMEaeJiEFC1nepg.htm](spells/uncommon-01-BQMEaeJiEFC1nepg.htm)|Forced Mercy|Pitié forcée|libre|
|[uncommon-01-BRtKFk0PKfWIlCAB.htm](spells/uncommon-01-BRtKFk0PKfWIlCAB.htm)|Sweet Dream|Doux rêve|officielle|
|[uncommon-01-bSDTWUIvgXkBaEv8.htm](spells/uncommon-01-bSDTWUIvgXkBaEv8.htm)|Hand of the Apprentice|Main de l'apprenti|libre|
|[uncommon-01-cDFAQN7Z3es07WSA.htm](spells/uncommon-01-cDFAQN7Z3es07WSA.htm)|Perfected Mind|Esprit perfectionné|officielle|
|[uncommon-01-CgekONDD5jIxL8oM.htm](spells/uncommon-01-CgekONDD5jIxL8oM.htm)|Life's Flowing River|Rivière du cours de la vie|libre|
|[uncommon-01-cOjlzWerBwbPWVkX.htm](spells/uncommon-01-cOjlzWerBwbPWVkX.htm)|Agile Feet|Pieds agiles|libre|
|[uncommon-01-CR8OKDbeFJoZbOCu.htm](spells/uncommon-01-CR8OKDbeFJoZbOCu.htm)|Extend Boost|Étendre le boost|libre|
|[uncommon-01-d2pi7laQkzlr3wrS.htm](spells/uncommon-01-d2pi7laQkzlr3wrS.htm)|Ancestral Touch|Toucher ancestral|libre|
|[uncommon-01-D6T17BdazhNy3KPm.htm](spells/uncommon-01-D6T17BdazhNy3KPm.htm)|Soul Siphon|Siphon d'âme|libre|
|[uncommon-01-dDt8VFuLuhznT19v.htm](spells/uncommon-01-dDt8VFuLuhznT19v.htm)|Snare Hopping|Piège artisanal bondissant|libre|
|[uncommon-01-dhpXpzt7TCm8TbHM.htm](spells/uncommon-01-dhpXpzt7TCm8TbHM.htm)|Thermal Stasis|Stase thermique|libre|
|[uncommon-01-Dj44lViYKvOJ8a53.htm](spells/uncommon-01-Dj44lViYKvOJ8a53.htm)|Ignite Ambition|Ambition enflammée|libre|
|[uncommon-01-dtOUkMC57izf93z5.htm](spells/uncommon-01-dtOUkMC57izf93z5.htm)|Ancestral Memories|Souvenirs ancestraux|officielle|
|[uncommon-01-DU5daB09xwfE1y38.htm](spells/uncommon-01-DU5daB09xwfE1y38.htm)|Waking Nightmare|Cauchemar éveillé|libre|
|[uncommon-01-dXIRotMLsABDQQSB.htm](spells/uncommon-01-dXIRotMLsABDQQSB.htm)|Scholarly Recollection|Souvenirs érudits|libre|
|[uncommon-01-EDABphKEPUBiMmQC.htm](spells/uncommon-01-EDABphKEPUBiMmQC.htm)|Verdant Sprout|Éclosion verdoyante|libre|
|[uncommon-01-Einy9RNTGVq1kY3j.htm](spells/uncommon-01-Einy9RNTGVq1kY3j.htm)|Inside Ropes|Tripes internes|libre|
|[uncommon-01-ekGHLJSHGgWMUwkY.htm](spells/uncommon-01-ekGHLJSHGgWMUwkY.htm)|Touch of Corruption (Healing)|Toucher de corruption (Soins)|libre|
|[uncommon-01-EzB9i7R6aBRAtJCh.htm](spells/uncommon-01-EzB9i7R6aBRAtJCh.htm)|Tempest Touch|Toucher tempêtueux|libre|
|[uncommon-01-f0Z5mqGA6Yu79B8x.htm](spells/uncommon-01-f0Z5mqGA6Yu79B8x.htm)|Uplifting Overture|Prélude inspirant|libre|
|[uncommon-01-fAlzXtQAASaJx0mY.htm](spells/uncommon-01-fAlzXtQAASaJx0mY.htm)|Life Boost|Gain de vie|libre|
|[uncommon-01-FedTjedva2rYk33r.htm](spells/uncommon-01-FedTjedva2rYk33r.htm)|Undeath's Blessing|Bénédiction de la mort-vivance|libre|
|[uncommon-01-ffz6wlSMzhaDpjg6.htm](spells/uncommon-01-ffz6wlSMzhaDpjg6.htm)|Earthworks|Ouvrages de terre|libre|
|[uncommon-01-fHawnhOHwzr4Mwr7.htm](spells/uncommon-01-fHawnhOHwzr4Mwr7.htm)|Elemental Counter|Contre élémentaire|libre|
|[uncommon-01-fprqWKUc0jnMIyGU.htm](spells/uncommon-01-fprqWKUc0jnMIyGU.htm)|Airburst|Explosion d'air|libre|
|[uncommon-01-Fr2CGvWcgSyLcUi7.htm](spells/uncommon-01-Fr2CGvWcgSyLcUi7.htm)|Bit of Luck|Un peu de chance|officielle|
|[uncommon-01-FTR8m4qrhYzTRyrD.htm](spells/uncommon-01-FTR8m4qrhYzTRyrD.htm)|Forge|Forge|libre|
|[uncommon-01-fXdADBwxmBsU9xPk.htm](spells/uncommon-01-fXdADBwxmBsU9xPk.htm)|Warped Terrain|Terrain déformé|libre|
|[uncommon-01-gwOYh5zMVZB0HNcT.htm](spells/uncommon-01-gwOYh5zMVZB0HNcT.htm)|Unimpeded Stride|Déplacement sans entrave|libre|
|[uncommon-01-GYI4xloAgkm6tTrT.htm](spells/uncommon-01-GYI4xloAgkm6tTrT.htm)|Touch of Undeath|Contact de la non-mort|officielle|
|[uncommon-01-HG4afO9EOGEU9bZN.htm](spells/uncommon-01-HG4afO9EOGEU9bZN.htm)|Death's Call|Appel de la mort|officielle|
|[uncommon-01-HOj2YsTpkoMpYJH9.htm](spells/uncommon-01-HOj2YsTpkoMpYJH9.htm)|Practice Makes Perfect|En forgeant on devient forgeron|libre|
|[uncommon-01-HStu2Yhw3iQER9tY.htm](spells/uncommon-01-HStu2Yhw3iQER9tY.htm)|Boost Eidolon|Booster l'eidolon|libre|
|[uncommon-01-Hu38hoAUSYeFpkVa.htm](spells/uncommon-01-Hu38hoAUSYeFpkVa.htm)|Force Bolt|Trait de force|officielle|
|[uncommon-01-hW9pgce6vTme61g1.htm](spells/uncommon-01-hW9pgce6vTme61g1.htm)|Unfetter Eidolon|Eidolon affranchi|libre|
|[uncommon-01-i8PBZsnoCrK7IWph.htm](spells/uncommon-01-i8PBZsnoCrK7IWph.htm)|Tentacular Limbs|Membres tentaculaires|officielle|
|[uncommon-01-IAjvwqgiDr3qGYxY.htm](spells/uncommon-01-IAjvwqgiDr3qGYxY.htm)|Courageous Anthem|Hymne de courage|libre|
|[uncommon-01-iAnpxrLaBU4V6Sej.htm](spells/uncommon-01-iAnpxrLaBU4V6Sej.htm)|Tidal Surge|Raz-de-marée|officielle|
|[uncommon-01-IkS3lDGUpIOMug7v.htm](spells/uncommon-01-IkS3lDGUpIOMug7v.htm)|Faerie Dust|Poussière féerique|officielle|
|[uncommon-01-irTdhxTixU9u9YUm.htm](spells/uncommon-01-irTdhxTixU9u9YUm.htm)|Lingering Composition|Composition prolongée|libre|
|[uncommon-01-IWUe32Y5k2QFd7YQ.htm](spells/uncommon-01-IWUe32Y5k2QFd7YQ.htm)|Gravity Weapon|Arme pesante|libre|
|[uncommon-01-jFmWSIpJGGebim6y.htm](spells/uncommon-01-jFmWSIpJGGebim6y.htm)|Touch of Corruption|Toucher de corruption|libre|
|[uncommon-01-Juk3cD5385Ftybct.htm](spells/uncommon-01-Juk3cD5385Ftybct.htm)|Verminous Lure|Appât à vermine|libre|
|[uncommon-01-k2QrUk7jWMAWozMh.htm](spells/uncommon-01-k2QrUk7jWMAWozMh.htm)|Brain Drain|Drain du cerveau|libre|
|[uncommon-01-k43PIYwuQqjeJ3S3.htm](spells/uncommon-01-k43PIYwuQqjeJ3S3.htm)|Forced Quiet|Silence forcé|officielle|
|[uncommon-01-K8vvrOgW4bGakXxm.htm](spells/uncommon-01-K8vvrOgW4bGakXxm.htm)|Dragon Claws|Griffes de dragon|officielle|
|[uncommon-01-KAapTzGKJMbMQCL1.htm](spells/uncommon-01-KAapTzGKJMbMQCL1.htm)|Spinning Staff|Bâton tournoyant|libre|
|[uncommon-01-KIV2LqzS5KtqOItV.htm](spells/uncommon-01-KIV2LqzS5KtqOItV.htm)|Heal Companion|Guérison de compagnon|libre|
|[uncommon-01-kJKSLfCgqxmN2FY8.htm](spells/uncommon-01-kJKSLfCgqxmN2FY8.htm)|Personal Rain Cloud|Nuage de pluie individuel|libre|
|[uncommon-01-KMFRKzNCq7hVNH7H.htm](spells/uncommon-01-KMFRKzNCq7hVNH7H.htm)|Charming Push|Poussée charmeuse|libre|
|[uncommon-01-kvm68hVtmADiIvN4.htm](spells/uncommon-01-kvm68hVtmADiIvN4.htm)|Jealous Hex|Maléfice de jalousie|officielle|
|[uncommon-01-lbrWMnS2pecKaSVB.htm](spells/uncommon-01-lbrWMnS2pecKaSVB.htm)|Swampcall|Appel du marais|libre|
|[uncommon-01-lpoWfblSMLcJfxsZ.htm](spells/uncommon-01-lpoWfblSMLcJfxsZ.htm)|Forbidden Thought|Pensées interdites|libre|
|[uncommon-01-lsR3RLEdBG4rcSzd.htm](spells/uncommon-01-lsR3RLEdBG4rcSzd.htm)|Efficient Apport|Apport efficace|libre|
|[uncommon-01-lXxP1ziyf4ozkpmv.htm](spells/uncommon-01-lXxP1ziyf4ozkpmv.htm)|Split the Tongue|Langue bifide|libre|
|[uncommon-01-lY9fOk1qBDDhBT8s.htm](spells/uncommon-01-lY9fOk1qBDDhBT8s.htm)|Protective Wards|Champs de protection|libre|
|[uncommon-01-m1vKX4xxWQJXfupu.htm](spells/uncommon-01-m1vKX4xxWQJXfupu.htm)|Glowing Trail|Trace luisante|libre|
|[uncommon-01-mFHQ2u4LWiejqKQG.htm](spells/uncommon-01-mFHQ2u4LWiejqKQG.htm)|Overstuff|Gaver|officielle|
|[uncommon-01-mlNYROcFrUF8nFgk.htm](spells/uncommon-01-mlNYROcFrUF8nFgk.htm)|Spray of Stars|Aspersion d'étoiles|libre|
|[uncommon-01-MmQiEc7aM9PDLO2J.htm](spells/uncommon-01-MmQiEc7aM9PDLO2J.htm)|Touch of Obedience|Obéissance|officielle|
|[uncommon-01-myC2EIrsjmB8xosi.htm](spells/uncommon-01-myC2EIrsjmB8xosi.htm)|Pushing Gust|Bourrasque agressive|officielle|
|[uncommon-01-nnSipUPNd3sm5vYL.htm](spells/uncommon-01-nnSipUPNd3sm5vYL.htm)|Vibrant Thorns|Épines florissantes|libre|
|[uncommon-01-NtzNCW32UlPdY2xS.htm](spells/uncommon-01-NtzNCW32UlPdY2xS.htm)|Ashen Wind|Vent cendreux|libre|
|[uncommon-01-nVfP43Xbs6I1PO8v.htm](spells/uncommon-01-nVfP43Xbs6I1PO8v.htm)|Shooting Star|Étoile tirée|libre|
|[uncommon-01-ofAQyLtLEGnOIs3N.htm](spells/uncommon-01-ofAQyLtLEGnOIs3N.htm)|Harrowing|Tirage du Tourment|libre|
|[uncommon-01-oFwmdb6LlRrh9AUT.htm](spells/uncommon-01-oFwmdb6LlRrh9AUT.htm)|Diviner's Sight|Vision du devin|officielle|
|[uncommon-01-OJ91rm1FkJSlf3nk.htm](spells/uncommon-01-OJ91rm1FkJSlf3nk.htm)|Ancient Dust|Poussière antique|libre|
|[uncommon-01-oJKZi8OQgmVXHOc0.htm](spells/uncommon-01-oJKZi8OQgmVXHOc0.htm)|Fire Ray|Rayon de feu|libre|
|[uncommon-01-oPVyu2a0K3aTVIR8.htm](spells/uncommon-01-oPVyu2a0K3aTVIR8.htm)|Elysian Whimsy|Caprice élyséen|libre|
|[uncommon-01-OyiKIbWllLZC6sGz.htm](spells/uncommon-01-OyiKIbWllLZC6sGz.htm)|Genie's Veil|Voile du génie|libre|
|[uncommon-01-P9bqJsF3WkxGAJKJ.htm](spells/uncommon-01-P9bqJsF3WkxGAJKJ.htm)|Sudden Shift|Déplacement soudain|officielle|
|[uncommon-01-PaHxcqXihXkkXPsB.htm](spells/uncommon-01-PaHxcqXihXkkXPsB.htm)|Unraveling Blast|Explosion de dénouement|libre|
|[uncommon-01-pHrVvoTKygXeczVG.htm](spells/uncommon-01-pHrVvoTKygXeczVG.htm)|Nymph's Token|Amulette de la nymphe|libre|
|[uncommon-01-pRKaEXnjGJXbPHPC.htm](spells/uncommon-01-pRKaEXnjGJXbPHPC.htm)|Hurtling Stone|Jet de pierre|officielle|
|[uncommon-01-ps0nmhclT6aIXgd8.htm](spells/uncommon-01-ps0nmhclT6aIXgd8.htm)|Ki Rush|Ruée Ki|officielle|
|[uncommon-01-Q25JQAgnJSGgFDKZ.htm](spells/uncommon-01-Q25JQAgnJSGgFDKZ.htm)|Veil of Confidence|Voile de confiance|libre|
|[uncommon-01-QnTtGCAvdWRU4spv.htm](spells/uncommon-01-QnTtGCAvdWRU4spv.htm)|Detect Alignment|Détection de l'alignement|libre|
|[uncommon-01-QozxgBbcmktLKdBs.htm](spells/uncommon-01-QozxgBbcmktLKdBs.htm)|Updraft|Courant ascendant|libre|
|[uncommon-01-QqxwHeYEVylkYjsO.htm](spells/uncommon-01-QqxwHeYEVylkYjsO.htm)|Detect Poison|Détection du poison|officielle|
|[uncommon-01-R569jdqpNry8m0TJ.htm](spells/uncommon-01-R569jdqpNry8m0TJ.htm)|Guided Introspection|Introspection guidée|libre|
|[uncommon-01-r82rqcm0MmGaBFkM.htm](spells/uncommon-01-r82rqcm0MmGaBFkM.htm)|Thunderous Strike|Frappe tonitruante|libre|
|[uncommon-01-rhJyqB9g3ziImQgM.htm](spells/uncommon-01-rhJyqB9g3ziImQgM.htm)|Healer's Blessing|Bénédiction du guérisseur|officielle|
|[uncommon-01-rMOI8JFJ0nT2mrCF.htm](spells/uncommon-01-rMOI8JFJ0nT2mrCF.htm)|Phase Familiar|Déphasage de familier|libre|
|[uncommon-01-ro0omoBKiJiMuDRa.htm](spells/uncommon-01-ro0omoBKiJiMuDRa.htm)|Overselling Flourish|Cabotinage|libre|
|[uncommon-01-rQYob0QMJ0I1U2sU.htm](spells/uncommon-01-rQYob0QMJ0I1U2sU.htm)|Protector's Sacrifice|Sacrifice du protecteur|officielle|
|[uncommon-01-s8gTmnNMg4H4bHEF.htm](spells/uncommon-01-s8gTmnNMg4H4bHEF.htm)|Gritty Wheeze|Haleine abrasive|libre|
|[uncommon-01-SdXFiQ4Py8761sNO.htm](spells/uncommon-01-SdXFiQ4Py8761sNO.htm)|Glutton's Jaw|Mâchoires gloutonnes|libre|
|[uncommon-01-SE0fbgBj7atuukdv.htm](spells/uncommon-01-SE0fbgBj7atuukdv.htm)|Cloak of Shadow|Manteau d'ombre|officielle|
|[uncommon-01-SuBtUJiU6DbSJYIw.htm](spells/uncommon-01-SuBtUJiU6DbSJYIw.htm)|Moonbeam|Rayon de lune|libre|
|[uncommon-01-sUr5KCpeE6AXfvPp.htm](spells/uncommon-01-sUr5KCpeE6AXfvPp.htm)|Imaginary Weapon|Arme imaginaire|libre|
|[uncommon-01-TjrZGl8z2INgf3vi.htm](spells/uncommon-01-TjrZGl8z2INgf3vi.htm)|Friendfetch|Récupère ami|libre|
|[uncommon-01-tWzxuJdbXqvskdIo.htm](spells/uncommon-01-tWzxuJdbXqvskdIo.htm)|Fortify Summoning|Convocation fortifiée|libre|
|[uncommon-01-u2uSeH6YSbK1ajTy.htm](spells/uncommon-01-u2uSeH6YSbK1ajTy.htm)|Magic Hide|Peau magique|libre|
|[uncommon-01-UbHK19RYbxRXWgWX.htm](spells/uncommon-01-UbHK19RYbxRXWgWX.htm)|Temporal Distortion|Distorsion temporelle|libre|
|[uncommon-01-UGJzJRJDoonfWqqI.htm](spells/uncommon-01-UGJzJRJDoonfWqqI.htm)|Athletic Rush|Athlétisme poussé|officielle|
|[uncommon-01-UmXhuKrYZR3W16mQ.htm](spells/uncommon-01-UmXhuKrYZR3W16mQ.htm)|Discern Secrets|Discerner les secrets|libre|
|[uncommon-01-ut9IhJ9jSZSHDUop.htm](spells/uncommon-01-ut9IhJ9jSZSHDUop.htm)|Charming Touch|Contact charmeur|officielle|
|[uncommon-01-Vctwx1ewa8HUOA94.htm](spells/uncommon-01-Vctwx1ewa8HUOA94.htm)|Diabolic Edict|Édit diabolique|officielle|
|[uncommon-01-vQuwLqtFFYt0K15N.htm](spells/uncommon-01-vQuwLqtFFYt0K15N.htm)|Cornucopia|Corne d'abondance|libre|
|[uncommon-01-Vvxgn7saUPW2bJhb.htm](spells/uncommon-01-Vvxgn7saUPW2bJhb.htm)|Read Fate|Lire le destin|libre|
|[uncommon-01-vZSWzkw0uF4iFWSM.htm](spells/uncommon-01-vZSWzkw0uF4iFWSM.htm)|Celestial Accord|Accord céleste|libre|
|[uncommon-01-WILXkjU5Yq3yw10r.htm](spells/uncommon-01-WILXkjU5Yq3yw10r.htm)|Counter Performance|Contre-représentation|libre|
|[uncommon-01-WV2aMNN9DO5ZBjSv.htm](spells/uncommon-01-WV2aMNN9DO5ZBjSv.htm)|Flense|Dépecer|libre|
|[uncommon-01-X3RUan7TVqo6UpUG.htm](spells/uncommon-01-X3RUan7TVqo6UpUG.htm)|Fated Healing|Guérison du destin|libre|
|[uncommon-01-XcMObj2p9nIBp53b.htm](spells/uncommon-01-XcMObj2p9nIBp53b.htm)|Scramble Body|Corps brouillé|libre|
|[uncommon-01-xn0V2HDrmDWNzPEt.htm](spells/uncommon-01-xn0V2HDrmDWNzPEt.htm)|Savor the Sting|Savourer la douleur|officielle|
|[uncommon-01-xTpp8dHZsNMDm75B.htm](spells/uncommon-01-xTpp8dHZsNMDm75B.htm)|Creative Splash|Avalanche créative|officielle|
|[uncommon-01-Xxdwkt0EEDgP1LGc.htm](spells/uncommon-01-Xxdwkt0EEDgP1LGc.htm)|Song of Strength|Chanson de force|libre|
|[uncommon-01-YgbYvkLvnWJ4WfEA.htm](spells/uncommon-01-YgbYvkLvnWJ4WfEA.htm)|Flashy Disappearance|Disparition spectaculaire|libre|
|[uncommon-01-YGRpHU5yxw73mls8.htm](spells/uncommon-01-YGRpHU5yxw73mls8.htm)|Soothing Words|Paroles apaisantes|officielle|
|[uncommon-01-yH13KXUK2x093NUv.htm](spells/uncommon-01-yH13KXUK2x093NUv.htm)|Face in the Crowd|Fondu dans la foule|officielle|
|[uncommon-01-YKexU7BkwNq4ESCc.htm](spells/uncommon-01-YKexU7BkwNq4ESCc.htm)|Serrate|Dentelé|libre|
|[uncommon-01-YVK3JUkPVzHIeGXQ.htm](spells/uncommon-01-YVK3JUkPVzHIeGXQ.htm)|Cackle|Gloussement|libre|
|[uncommon-01-yyz029C9eqfY38PT.htm](spells/uncommon-01-yyz029C9eqfY38PT.htm)|Telekinetic Rend|Déchirure télékinésique|libre|
|[uncommon-01-zHSp4PzoOE72DV4o.htm](spells/uncommon-01-zHSp4PzoOE72DV4o.htm)|Torturous Trauma|Trauma de la torture|libre|
|[uncommon-01-ZL8NTvB22NeEWhVG.htm](spells/uncommon-01-ZL8NTvB22NeEWhVG.htm)|Ki Strike|Frappe Ki|officielle|
|[uncommon-01-zNN9212H2FGfM7VS.htm](spells/uncommon-01-zNN9212H2FGfM7VS.htm)|Lay on Hands|Imposition des mains|officielle|
|[uncommon-01-zTN6zuruDUKOea6h.htm](spells/uncommon-01-zTN6zuruDUKOea6h.htm)|Rising Surf|Vague porteuse|libre|
|[uncommon-01-zul5cBTfr7NXHBZf.htm](spells/uncommon-01-zul5cBTfr7NXHBZf.htm)|Dazzling Flash|Éclair éblouissant|libre|
|[uncommon-02-1hLxZznnA2kvXlIt.htm](spells/uncommon-02-1hLxZznnA2kvXlIt.htm)|Cutting Insult|Insulte coupante|libre|
|[uncommon-02-4Gl3WSUqYjVVIsOg.htm](spells/uncommon-02-4Gl3WSUqYjVVIsOg.htm)|Shadow Zombie|Zombie de l'ombre|libre|
|[uncommon-02-4Sg6ZngswhphxiBD.htm](spells/uncommon-02-4Sg6ZngswhphxiBD.htm)|Swallow Light|Avaler la lumière|libre|
|[uncommon-02-66xBcxqzYcpbItBU.htm](spells/uncommon-02-66xBcxqzYcpbItBU.htm)|Purify Soul Path|Purifier le chemin de l'âme|libre|
|[uncommon-02-8rj45fKzCFcB0fxs.htm](spells/uncommon-02-8rj45fKzCFcB0fxs.htm)|Enlarge Companion|Agrandissement de compagnon|libre|
|[uncommon-02-8VzbumNyMEdSzZSz.htm](spells/uncommon-02-8VzbumNyMEdSzZSz.htm)|Impeccable Flow|Flux impeccable|libre|
|[uncommon-02-AsKLseOo8hwv5Jha.htm](spells/uncommon-02-AsKLseOo8hwv5Jha.htm)|Invoke the Crimson Oath|Invoquer le Serment écarlate|libre|
|[uncommon-02-avT46uIH3xYJPSv4.htm](spells/uncommon-02-avT46uIH3xYJPSv4.htm)|Teeth to Terror|Des dents à la terreur|libre|
|[uncommon-02-b6UnLNikoq2Std1f.htm](spells/uncommon-02-b6UnLNikoq2Std1f.htm)|Magic Warrior Aspect|Aspect du guerrier magique|officielle|
|[uncommon-02-bH0kPuf7UKxRvi2P.htm](spells/uncommon-02-bH0kPuf7UKxRvi2P.htm)|Rallying Anthem|Hymne de ralliement|libre|
|[uncommon-02-c8R2fpk88fBwJ1ie.htm](spells/uncommon-02-c8R2fpk88fBwJ1ie.htm)|Triple Time|À trois temps|libre|
|[uncommon-02-cf7Jkm39uEjUFtHt.htm](spells/uncommon-02-cf7Jkm39uEjUFtHt.htm)|Sea Surge|Onde océanique|libre|
|[uncommon-02-CI7F5qdwp6YngyFt.htm](spells/uncommon-02-CI7F5qdwp6YngyFt.htm)|Confetti Cloud|Nuage de confettis|libre|
|[uncommon-02-crf7EL1JtBYwvAEg.htm](spells/uncommon-02-crf7EL1JtBYwvAEg.htm)|Blood Duplicate|Duplicata de sang|libre|
|[uncommon-02-FzAtX8yXBjTaisJK.htm](spells/uncommon-02-FzAtX8yXBjTaisJK.htm)|Undetectable Alignment|Alignement indétectable|officielle|
|[uncommon-02-GfrKNJ9pNeATiKCc.htm](spells/uncommon-02-GfrKNJ9pNeATiKCc.htm)|Hunter's Luck|Chance du chasseur|libre|
|[uncommon-02-GQopUYTuhmtb7WMG.htm](spells/uncommon-02-GQopUYTuhmtb7WMG.htm)|Perfect Strike|Coup parfait|libre|
|[uncommon-02-kLyyJayja75K9rXX.htm](spells/uncommon-02-kLyyJayja75K9rXX.htm)|Illusory Shroud|Voile illusoire|libre|
|[uncommon-02-lOZhcvtej10TqlQm.htm](spells/uncommon-02-lOZhcvtej10TqlQm.htm)|Lifelink Surge|Afflux du lien vital|libre|
|[uncommon-02-LrRyNA2bo5UwBxud.htm](spells/uncommon-02-LrRyNA2bo5UwBxud.htm)|Horrifying Blood Loss|Perte de sang horrifique|libre|
|[uncommon-02-MkHshwYy2UOeEHRR.htm](spells/uncommon-02-MkHshwYy2UOeEHRR.htm)|Lucky Month|Mois chanceux|libre|
|[uncommon-02-NhNKzq1DvFxkvTEc.htm](spells/uncommon-02-NhNKzq1DvFxkvTEc.htm)|Vision of Weakness|Vision de faiblesse|libre|
|[uncommon-02-obVA6duK5fGbfFUY.htm](spells/uncommon-02-obVA6duK5fGbfFUY.htm)|Empathic Link|Lien empathique|libre|
|[uncommon-02-oOFilBgsXTVIbJpN.htm](spells/uncommon-02-oOFilBgsXTVIbJpN.htm)|Phantom Ship|Bateau fantôme|libre|
|[uncommon-02-oryfsRK27jAUnziw.htm](spells/uncommon-02-oryfsRK27jAUnziw.htm)|Imp Sting|Dard de diablotin|libre|
|[uncommon-02-ou56ShiFH7GWF8hX.htm](spells/uncommon-02-ou56ShiFH7GWF8hX.htm)|Light of Revelation|Lumière de révélation|libre|
|[uncommon-02-pMTltbI3S3UIuFaR.htm](spells/uncommon-02-pMTltbI3S3UIuFaR.htm)|Sun Blade|Lame solaire|libre|
|[uncommon-02-qFO9HgplrShoaAPY.htm](spells/uncommon-02-qFO9HgplrShoaAPY.htm)|Lock Item|Verrouiller l'objet|libre|
|[uncommon-02-qJZZdYBdNaWRJFER.htm](spells/uncommon-02-qJZZdYBdNaWRJFER.htm)|Wholeness of Body|Plénitude physique|officielle|
|[uncommon-02-QZ7OHptO1xnwaruq.htm](spells/uncommon-02-QZ7OHptO1xnwaruq.htm)|Penumbral Disguise|Déguisement pénombral|libre|
|[uncommon-02-RGBZrVRIEDb2G48h.htm](spells/uncommon-02-RGBZrVRIEDb2G48h.htm)|Soothing Mist|Brume apaisante|libre|
|[uncommon-02-S708JF3E0kuhuRzG.htm](spells/uncommon-02-S708JF3E0kuhuRzG.htm)|Bone Spray|Aspersion d'os|libre|
|[uncommon-02-SspI4ijjR7N7r4Cc.htm](spells/uncommon-02-SspI4ijjR7N7r4Cc.htm)|Bralani Referendum|Référendum bralani|libre|
|[uncommon-02-tp4K7mYDL5MRHvJc.htm](spells/uncommon-02-tp4K7mYDL5MRHvJc.htm)|Magic Warrior Transformation|Transformation du guerrier magique|officielle|
|[uncommon-02-Tw9e2rPaNdxcM1Rp.htm](spells/uncommon-02-Tw9e2rPaNdxcM1Rp.htm)|Erase Trail|Effacer la piste|libre|
|[uncommon-02-uopaLE01meX11Mbw.htm](spells/uncommon-02-uopaLE01meX11Mbw.htm)|Umbral Mindtheft|Vol d'intellect ombral|libre|
|[uncommon-02-vctIUOOgSmxAF0KG.htm](spells/uncommon-02-vctIUOOgSmxAF0KG.htm)|Fear the Sun|Peur du soleil|libre|
|[uncommon-02-vGEgI8e7AW6FQ3tP.htm](spells/uncommon-02-vGEgI8e7AW6FQ3tP.htm)|Animal Feature|Trait animal|libre|
|[uncommon-02-VJW3IzDpAFio7uls.htm](spells/uncommon-02-VJW3IzDpAFio7uls.htm)|Impart Empathy|Conférer l'empathie|libre|
|[uncommon-02-X3fWP6YCSzcdtg93.htm](spells/uncommon-02-X3fWP6YCSzcdtg93.htm)|Allfood|Mangetout|libre|
|[uncommon-02-Xrz6wPXDBUr27izR.htm](spells/uncommon-02-Xrz6wPXDBUr27izR.htm)|Grave Impressions|Impressions de la tombe|libre|
|[uncommon-02-xyRjD52YZl3DBsiy.htm](spells/uncommon-02-xyRjD52YZl3DBsiy.htm)|Instant Parade|Parade instantanée|libre|
|[uncommon-02-zdNUbHqqZzjA07oM.htm](spells/uncommon-02-zdNUbHqqZzjA07oM.htm)|Boneshaker|Secoueur d'os|libre|
|[uncommon-02-zK0e9d9DSnxC4eAD.htm](spells/uncommon-02-zK0e9d9DSnxC4eAD.htm)|Sudden Bolt|Décharge soudaine|libre|
|[uncommon-03-0aoRcxRyIPHfbifk.htm](spells/uncommon-03-0aoRcxRyIPHfbifk.htm)|Nothing Up My Sleeve|Rien dans ma manche|libre|
|[uncommon-03-0jWBnIDFpJjJShdQ.htm](spells/uncommon-03-0jWBnIDFpJjJShdQ.htm)|Dragon Breath (Blue or Bronze)|Souffle de dragon (Bleu ou Bronze)|libre|
|[uncommon-03-1bXLp8cyIKFjZtVx.htm](spells/uncommon-03-1bXLp8cyIKFjZtVx.htm)|Temporal Twin|Jumelage temporel|libre|
|[uncommon-03-1OiPVy1wuoXi6LR5.htm](spells/uncommon-03-1OiPVy1wuoXi6LR5.htm)|Firework Blast|Explosion pyrotechnique|libre|
|[uncommon-03-1xLVcA8Y1onw7toT.htm](spells/uncommon-03-1xLVcA8Y1onw7toT.htm)|Dirge of Doom|Chant funeste|officielle|
|[uncommon-03-2jWVNdVlbJq84dfT.htm](spells/uncommon-03-2jWVNdVlbJq84dfT.htm)|Battlefield Persistence|Persévérance sur le champ de bataille|libre|
|[uncommon-03-2ZPqcM9wNoVnpwkK.htm](spells/uncommon-03-2ZPqcM9wNoVnpwkK.htm)|Magical Fetters|Entraves magiques|libre|
|[uncommon-03-3ipOKanMLnJrPwbr.htm](spells/uncommon-03-3ipOKanMLnJrPwbr.htm)|Guardian's Aegis|Égide du gardien|libre|
|[uncommon-03-3P3M9SysSesALyoT.htm](spells/uncommon-03-3P3M9SysSesALyoT.htm)|Dragon Breath (Sky)|Souffle de dragon (Céleste)|libre|
|[uncommon-03-6mmEz1UoTHGB7Sy9.htm](spells/uncommon-03-6mmEz1UoTHGB7Sy9.htm)|Dragon Breath (Black, Brine or Copper)|Souffle de dragon (Cuivre, Noir ou Saumure)|libre|
|[uncommon-03-73gohsek77nLGPWC.htm](spells/uncommon-03-73gohsek77nLGPWC.htm)|Heatvision|Perception de la chaleur|libre|
|[uncommon-03-8MAHUK6jphbME4BR.htm](spells/uncommon-03-8MAHUK6jphbME4BR.htm)|Mindscape Door|Porte du paysage mental|libre|
|[uncommon-03-8OWA91bgm5r6QPaH.htm](spells/uncommon-03-8OWA91bgm5r6QPaH.htm)|Dividing Trench|Tranchée de séparation|libre|
|[uncommon-03-98gJvb8Xtn8OLIY7.htm](spells/uncommon-03-98gJvb8Xtn8OLIY7.htm)|Rally Point|Point de ralliement|libre|
|[uncommon-03-9vDmTCHyAWdWWPIs.htm](spells/uncommon-03-9vDmTCHyAWdWWPIs.htm)|Contact Friends|Contacter les amis|libre|
|[uncommon-03-a0AMgATvQGwDR5dR.htm](spells/uncommon-03-a0AMgATvQGwDR5dR.htm)|Vector Screen|Écran vectoriel|libre|
|[uncommon-03-aewxsale5xWEPKLk.htm](spells/uncommon-03-aewxsale5xWEPKLk.htm)|Ring of Truth|Cercle de vérité|libre|
|[uncommon-03-aZg3amDcrXz3cLCz.htm](spells/uncommon-03-aZg3amDcrXz3cLCz.htm)|Horrific Visage|Visage terrifiant|officielle|
|[uncommon-03-B8aCUMCHCIMUCEVK.htm](spells/uncommon-03-B8aCUMCHCIMUCEVK.htm)|Elemental Motion|Mobilité élémentaire|officielle|
|[uncommon-03-bK7EZFCxZgDcko6D.htm](spells/uncommon-03-bK7EZFCxZgDcko6D.htm)|Unseen Heralds|Hérauts invisibles|libre|
|[uncommon-03-bmDgIbLa5NfkP97J.htm](spells/uncommon-03-bmDgIbLa5NfkP97J.htm)|Dragon Breath (Silver or White)|Souffle de dragon (Argent ou Blanc)|libre|
|[uncommon-03-BqJAOPimCq5uCcEJ.htm](spells/uncommon-03-BqJAOPimCq5uCcEJ.htm)|Shatter Mind|Fracturer l'esprit|libre|
|[uncommon-03-Cd6Crl4wpQaMSYrF.htm](spells/uncommon-03-Cd6Crl4wpQaMSYrF.htm)|Subjugate Undead|Subjuguer un mort-vivant|libre|
|[uncommon-03-cqdmSmQnM0q6wbWG.htm](spells/uncommon-03-cqdmSmQnM0q6wbWG.htm)|Drain Life|Drain de vie|officielle|
|[uncommon-03-DeF63UTmr7rchF60.htm](spells/uncommon-03-DeF63UTmr7rchF60.htm)|Wall of Shadow|Mur d'ombre|libre|
|[uncommon-03-dgMauNKWeRIu8pMN.htm](spells/uncommon-03-dgMauNKWeRIu8pMN.htm)|Glass Sand|Sable vitrifié|libre|
|[uncommon-03-DgNOpb8H9MTAu9KL.htm](spells/uncommon-03-DgNOpb8H9MTAu9KL.htm)|Consecrate Flesh|Chair consacrée|libre|
|[uncommon-03-E80SrXuBdZViPGiH.htm](spells/uncommon-03-E80SrXuBdZViPGiH.htm)|Pulverizing Cascade|Cascade pulvérisante|libre|
|[uncommon-03-ECApRjIIxD0JogOa.htm](spells/uncommon-03-ECApRjIIxD0JogOa.htm)|Stone Lance|Lance de pierre|libre|
|[uncommon-03-eIPIZp2FUbFcLNdj.htm](spells/uncommon-03-eIPIZp2FUbFcLNdj.htm)|Pillar of Water|Pilier d'eau|libre|
|[uncommon-03-EoKBlgf6Smt8opaU.htm](spells/uncommon-03-EoKBlgf6Smt8opaU.htm)|Veil of Privacy|Voile de confidentialité|libre|
|[uncommon-03-ErCsWsqXe8ccRjgO.htm](spells/uncommon-03-ErCsWsqXe8ccRjgO.htm)|Far Sight|Vision lointaine|libre|
|[uncommon-03-fnDBgdEeuzHRupDu.htm](spells/uncommon-03-fnDBgdEeuzHRupDu.htm)|Dragon Breath (Umbral)|Souffle de dragon (ombral)|libre|
|[uncommon-03-FPnkOYyWIaOzkmqn.htm](spells/uncommon-03-FPnkOYyWIaOzkmqn.htm)|Untwisting Iron Buffer|Protection du fer qui ne plie pas|officielle|
|[uncommon-03-gIVaSCrLhhBzGHQY.htm](spells/uncommon-03-gIVaSCrLhhBzGHQY.htm)|Reincarnate|Réincarnation|libre|
|[uncommon-03-GurWFDj4IjKv73kL.htm](spells/uncommon-03-GurWFDj4IjKv73kL.htm)|Excise Lexicon|Exciser le lexique|libre|
|[uncommon-03-GxxnhRIaoGKtu1iO.htm](spells/uncommon-03-GxxnhRIaoGKtu1iO.htm)|Extend Spell|Extension de durée|libre|
|[uncommon-03-h5cMTygLpjY3IEF0.htm](spells/uncommon-03-h5cMTygLpjY3IEF0.htm)|Sudden Recollection|Souvenir soudain|libre|
|[uncommon-03-HWrNMQENi9WSGbnF.htm](spells/uncommon-03-HWrNMQENi9WSGbnF.htm)|Wall of Radiance|Mur rayonnant|libre|
|[uncommon-03-iG1USiSRXMjCDGAr.htm](spells/uncommon-03-iG1USiSRXMjCDGAr.htm)|Percussive Impact|Impact percutant|libre|
|[uncommon-03-ilGsyGLGjjIPHbyP.htm](spells/uncommon-03-ilGsyGLGjjIPHbyP.htm)|Embrace the Pit|Étreinte de la fosse|officielle|
|[uncommon-03-iQD8OhhkwhvD8Blw.htm](spells/uncommon-03-iQD8OhhkwhvD8Blw.htm)|Swamp of Sloth|Marais de paresse|libre|
|[uncommon-03-J5KrjQKCg2PrF1vz.htm](spells/uncommon-03-J5KrjQKCg2PrF1vz.htm)|Ancestral Defense|Défense ancestrale|libre|
|[uncommon-03-JcobNl4iE9HmMYtE.htm](spells/uncommon-03-JcobNl4iE9HmMYtE.htm)|Dragon Breath|Souffle de dragon|officielle|
|[uncommon-03-jOWQ5wPd4xvSkjI5.htm](spells/uncommon-03-jOWQ5wPd4xvSkjI5.htm)|Mystic Carriage|Carrosse mystique|libre|
|[uncommon-03-jupppBeFqhe6LMIb.htm](spells/uncommon-03-jupppBeFqhe6LMIb.htm)|Dragon Breath (Green)|Souffle de dragon (Vert)|libre|
|[uncommon-03-k9M0kUhhHHGYquiA.htm](spells/uncommon-03-k9M0kUhhHHGYquiA.htm)|Dragon Breath (Cloud)|Souffle de dragon (Nuage)|libre|
|[uncommon-03-KAWQ5x2j4tUJ91ry.htm](spells/uncommon-03-KAWQ5x2j4tUJ91ry.htm)|Sparkleskin|Peau à paillettes|libre|
|[uncommon-03-kfsv7zSHb3pr7s9v.htm](spells/uncommon-03-kfsv7zSHb3pr7s9v.htm)|Dragon Breath (Gold, Magma or Red)|Souffle de dragon (Magma, Or ou Rouge)|officielle|
|[uncommon-03-KHnhPHL4x1AQHfbC.htm](spells/uncommon-03-KHnhPHL4x1AQHfbC.htm)|Mind Reading|Lecture des pensées|libre|
|[uncommon-03-KktHf7zIAWOr499h.htm](spells/uncommon-03-KktHf7zIAWOr499h.htm)|Ranger's Bramble|Roncier du rôdeur|libre|
|[uncommon-03-kRsmUlSWhi6PJvZ7.htm](spells/uncommon-03-kRsmUlSWhi6PJvZ7.htm)|Angelic Wings|Ailes d'ange|officielle|
|[uncommon-03-kuZnUNrhXHRYQ2eM.htm](spells/uncommon-03-kuZnUNrhXHRYQ2eM.htm)|Eidolon's Wrath|Courroux de l'eidolon|libre|
|[uncommon-03-l4bHFR9UW2XiY3kH.htm](spells/uncommon-03-l4bHFR9UW2XiY3kH.htm)|Omnidirectional Scan|Balayage omnidirectionnel|libre|
|[uncommon-03-LbPLNWlLCxKCo5gF.htm](spells/uncommon-03-LbPLNWlLCxKCo5gF.htm)|Access Lore|Accès à la connaissance|libre|
|[uncommon-03-LbqunTurwXB3u9Vp.htm](spells/uncommon-03-LbqunTurwXB3u9Vp.htm)|Time Skip|Passage du temps|libre|
|[uncommon-03-LFSwMtQVP05EzlZe.htm](spells/uncommon-03-LFSwMtQVP05EzlZe.htm)|Thunderburst|Explosion tonitruante|libre|
|[uncommon-03-LQzlKbYjZSMFQawP.htm](spells/uncommon-03-LQzlKbYjZSMFQawP.htm)|Locate|Localisation|officielle|
|[uncommon-03-LrFUj76CHDBV0vHW.htm](spells/uncommon-03-LrFUj76CHDBV0vHW.htm)|Sun's Fury|Furie du soleil|libre|
|[uncommon-03-M9TiCE1vlG1j2faM.htm](spells/uncommon-03-M9TiCE1vlG1j2faM.htm)|Martyr's Intervention|Intervention du martyr|libre|
|[uncommon-03-mBojKJatf9PTYC38.htm](spells/uncommon-03-mBojKJatf9PTYC38.htm)|Fey Disappearance|Disparition féerique|officielle|
|[uncommon-03-mpGCMTldMVa0pWYs.htm](spells/uncommon-03-mpGCMTldMVa0pWYs.htm)|Circle of Protection|Cercle de protection|libre|
|[uncommon-03-mriIWoSDtJTJIBjX.htm](spells/uncommon-03-mriIWoSDtJTJIBjX.htm)|Caster's Imposition|Imposition de l'incantateur|libre|
|[uncommon-03-mtlyAyf30JnIvxVn.htm](spells/uncommon-03-mtlyAyf30JnIvxVn.htm)|Moonlight Ray|Rayon de lumière de lune|libre|
|[uncommon-03-N0h3UodJFKdNQw1Y.htm](spells/uncommon-03-N0h3UodJFKdNQw1Y.htm)|Dragon Breath (Brass)|Souffle de dragon (Airain)|libre|
|[uncommon-03-nUSi2B7RhIKjaiXQ.htm](spells/uncommon-03-nUSi2B7RhIKjaiXQ.htm)|Astral Rain|Pluie astrale|libre|
|[uncommon-03-NXm8z4W4YZazE2gn.htm](spells/uncommon-03-NXm8z4W4YZazE2gn.htm)|Dragon Breath (Sea)|Souffle de dragon (Mer)|libre|
|[uncommon-03-o0l57UfBm9ScEUMW.htm](spells/uncommon-03-o0l57UfBm9ScEUMW.htm)|Rune Trap|Rune piégée|libre|
|[uncommon-03-oLylenH2jlP5UbRT.htm](spells/uncommon-03-oLylenH2jlP5UbRT.htm)|Painted Scout|Éclaireur peint|libre|
|[uncommon-03-oo7YcRC2gcez81PV.htm](spells/uncommon-03-oo7YcRC2gcez81PV.htm)|Ki Blast|Explosion Ki|officielle|
|[uncommon-03-OTd17oXwJH9qb1cS.htm](spells/uncommon-03-OTd17oXwJH9qb1cS.htm)|Incendiary Ashes|Cendres incendiaires|libre|
|[uncommon-03-ovx7O2FHvkjXhMcA.htm](spells/uncommon-03-ovx7O2FHvkjXhMcA.htm)|Perseis's Precautions|Précautions de Perséis|libre|
|[uncommon-03-pQ3NIzZXeIIcU81C.htm](spells/uncommon-03-pQ3NIzZXeIIcU81C.htm)|Spirit Veil|Voile spirituel|libre|
|[uncommon-03-pSNLufPPsReKQtJR.htm](spells/uncommon-03-pSNLufPPsReKQtJR.htm)|Armor of Bones|Armure d'os|libre|
|[uncommon-03-PVXqMko4yGgw90uo.htm](spells/uncommon-03-PVXqMko4yGgw90uo.htm)|Heart's Desire|Désir du coeur|libre|
|[uncommon-03-Q56HLIHVKY6bC5W3.htm](spells/uncommon-03-Q56HLIHVKY6bC5W3.htm)|Blinding Beauty|Beauté aveuglante|libre|
|[uncommon-03-Q690d3mw3TUrKX7E.htm](spells/uncommon-03-Q690d3mw3TUrKX7E.htm)|Geas|Serment rituel|officielle|
|[uncommon-03-qhJfRnkCRrMI4G1O.htm](spells/uncommon-03-qhJfRnkCRrMI4G1O.htm)|Aberrant Whispers|Murmures aberrants|officielle|
|[uncommon-03-QpjHqxwTGdILLvjD.htm](spells/uncommon-03-QpjHqxwTGdILLvjD.htm)|Ephemeral Tracking|Pistage éphémère|libre|
|[uncommon-03-rtisvvvhkpZPdgXc.htm](spells/uncommon-03-rtisvvvhkpZPdgXc.htm)|Cascade Countermeasure|Contremesure en cascade|libre|
|[uncommon-03-t5HAnKSHfSfHAJwH.htm](spells/uncommon-03-t5HAnKSHfSfHAJwH.htm)|Dragon Breath (Crystal or Forest)|Souffle de dragon (Cristal ou Forêt)|libre|
|[uncommon-03-tC9HJ3sfQJFTacrE.htm](spells/uncommon-03-tC9HJ3sfQJFTacrE.htm)|Dragon Breath (Sovereign)|Souffle de dragon (Souverain)|libre|
|[uncommon-03-TT9owkeMBXJxcERB.htm](spells/uncommon-03-TT9owkeMBXJxcERB.htm)|Unseasonable Squall|Rafale hors saison|libre|
|[uncommon-03-tvBhGfGcg2fsMOMe.htm](spells/uncommon-03-tvBhGfGcg2fsMOMe.htm)|Dragon Breath (Underworld)|Souffle de dragon (Souterrain)|libre|
|[uncommon-03-tvO6Kmc2pQve9DC5.htm](spells/uncommon-03-tvO6Kmc2pQve9DC5.htm)|Unfolding Wind Rush|Ruée du vent qui se déploie|officielle|
|[uncommon-03-u3G7KX1qpFJlSeWm.htm](spells/uncommon-03-u3G7KX1qpFJlSeWm.htm)|Owb Pact|Pacte avec un owb|libre|
|[uncommon-03-uuoPmbjEtqzWZs0v.htm](spells/uncommon-03-uuoPmbjEtqzWZs0v.htm)|Unseen Custodians|Domestiques invisibles|libre|
|[uncommon-03-v4QHuVOhFD1JMAqu.htm](spells/uncommon-03-v4QHuVOhFD1JMAqu.htm)|Powerful Inhalation|Inhalation puissante|libre|
|[uncommon-03-VBNevVovTCpn04vL.htm](spells/uncommon-03-VBNevVovTCpn04vL.htm)|Unblinking Flame Revelation|Révélation de la flamme qui ne vacille pas|officielle|
|[uncommon-03-vvbS69EHZuUTq0dr.htm](spells/uncommon-03-vvbS69EHZuUTq0dr.htm)|Life Pact|Pacte vital|libre|
|[uncommon-03-Wi2HcreCfujKiCvW.htm](spells/uncommon-03-Wi2HcreCfujKiCvW.htm)|Whirling Flames|Flammes tourbillonnantes|libre|
|[uncommon-03-X1b9ollVMSLXDN9o.htm](spells/uncommon-03-X1b9ollVMSLXDN9o.htm)|Litany against Wrath|Litanie contre la colère|libre|
|[uncommon-03-X4T5RlQBrdpmA35n.htm](spells/uncommon-03-X4T5RlQBrdpmA35n.htm)|Entropic Wheel|Roue entropique|libre|
|[uncommon-03-YQWq1DZuLRk32M3h.htm](spells/uncommon-03-YQWq1DZuLRk32M3h.htm)|Inscrutable Mask|Masque insondable|libre|
|[uncommon-03-ytboJsyZEbE1MLeV.htm](spells/uncommon-03-ytboJsyZEbE1MLeV.htm)|Unbreaking Wave Advance|Avancée de la vague inexorable|libre|
|[uncommon-03-Z3kJty995FkrsZRb.htm](spells/uncommon-03-Z3kJty995FkrsZRb.htm)|Combustion|Combustion|libre|
|[uncommon-03-ziHDISWkFSwz3pmn.htm](spells/uncommon-03-ziHDISWkFSwz3pmn.htm)|Delay Affliction|Retarder l'affliction|libre|
|[uncommon-03-ZJOQBqkNErZu1QAa.htm](spells/uncommon-03-ZJOQBqkNErZu1QAa.htm)|Wall of Virtue|Mur de vertu|libre|
|[uncommon-04-0qA4MfMkFklOz2Lk.htm](spells/uncommon-04-0qA4MfMkFklOz2Lk.htm)|Fearful Feast|Festin d'épouvante|libre|
|[uncommon-04-29p7NMY2OTpaINzt.htm](spells/uncommon-04-29p7NMY2OTpaINzt.htm)|Necrotic Radiation|Radiation nécrotique|libre|
|[uncommon-04-2jB9eyX7YekwoCvA.htm](spells/uncommon-04-2jB9eyX7YekwoCvA.htm)|Word of Freedom|Mot de liberté|libre|
|[uncommon-04-2SYq0ZTsOtJEigFx.htm](spells/uncommon-04-2SYq0ZTsOtJEigFx.htm)|Mystic Beacon|Phare mystique|libre|
|[uncommon-04-4cEDhvchskRvxSw6.htm](spells/uncommon-04-4cEDhvchskRvxSw6.htm)|Procyal Philosophy|Philosophie procyale|libre|
|[uncommon-04-4DaHIgtMBTyxebY3.htm](spells/uncommon-04-4DaHIgtMBTyxebY3.htm)|Commanding Lash|Fouet du maître|officielle|
|[uncommon-04-4LSf04FFvDgMyDk6.htm](spells/uncommon-04-4LSf04FFvDgMyDk6.htm)|Rune of Observation|Rune d'observation|libre|
|[uncommon-04-4nHYSMHito1GUXlm.htm](spells/uncommon-04-4nHYSMHito1GUXlm.htm)|Rebuke Death|Réprimander la mort|libre|
|[uncommon-04-5LGDygjRxURUOKGR.htm](spells/uncommon-04-5LGDygjRxURUOKGR.htm)|Radiant Beam|Rayon radieux|libre|
|[uncommon-04-5LrQIlimsPXK2xAG.htm](spells/uncommon-04-5LrQIlimsPXK2xAG.htm)|Steal Voice|Voler la voix|libre|
|[uncommon-04-5z1700cfG8Ybm0e6.htm](spells/uncommon-04-5z1700cfG8Ybm0e6.htm)|False Nature|Fausse nature|libre|
|[uncommon-04-6RNymgUvS87lmQOj.htm](spells/uncommon-04-6RNymgUvS87lmQOj.htm)|Community Restoration|Récupération communautaire|libre|
|[uncommon-04-7d4DUTDIlzDa8OvX.htm](spells/uncommon-04-7d4DUTDIlzDa8OvX.htm)|Destructive Aura|Aura destructrice|libre|
|[uncommon-04-7Fd4lxozd11MQ55N.htm](spells/uncommon-04-7Fd4lxozd11MQ55N.htm)|Atone|Pénitence|libre|
|[uncommon-04-7OwZHalOdRCRnFmZ.htm](spells/uncommon-04-7OwZHalOdRCRnFmZ.htm)|Lucky Break|Répit chanceux|libre|
|[uncommon-04-7yWXx3qC4eFNHhxD.htm](spells/uncommon-04-7yWXx3qC4eFNHhxD.htm)|Blight|Flétrissement végétal|officielle|
|[uncommon-04-8XuNn0h0rHE24m3B.htm](spells/uncommon-04-8XuNn0h0rHE24m3B.htm)|Vital Luminance|Luminosité vitale|officielle|
|[uncommon-04-a5uwCgOe7ayHPtHe.htm](spells/uncommon-04-a5uwCgOe7ayHPtHe.htm)|Enhance Senses|Amélioration des sens|libre|
|[uncommon-04-AnWCohzPgK4L9GVl.htm](spells/uncommon-04-AnWCohzPgK4L9GVl.htm)|Detect Scrying|Détection de la scrutation|libre|
|[uncommon-04-APTMURAW1N0Wpk4w.htm](spells/uncommon-04-APTMURAW1N0Wpk4w.htm)|Precious Metals|Métaux précieux|libre|
|[uncommon-04-Az3PmWnlWSb5ELX9.htm](spells/uncommon-04-Az3PmWnlWSb5ELX9.htm)|Abundant Step|Pas chassé|officielle|
|[uncommon-04-bKDsmKVosexwJ80i.htm](spells/uncommon-04-bKDsmKVosexwJ80i.htm)|Mantis Form|Forme de la mante|officielle|
|[uncommon-04-cBUuG1yJHGeKffpg.htm](spells/uncommon-04-cBUuG1yJHGeKffpg.htm)|Localized Quake|Séisme localisé|libre|
|[uncommon-04-CPNlhDQP3aDmLzB3.htm](spells/uncommon-04-CPNlhDQP3aDmLzB3.htm)|Ordained Purpose|Résolution sacrée|libre|
|[uncommon-04-dARE6VfJ3Uoq5M53.htm](spells/uncommon-04-dARE6VfJ3Uoq5M53.htm)|Daydreamer's Curse|Malédiction du rêveur éveillé|libre|
|[uncommon-04-DH9Y3RQGWO0GzXGU.htm](spells/uncommon-04-DH9Y3RQGWO0GzXGU.htm)|Protector's Sphere|Sphère du protecteur|libre|
|[uncommon-04-e9tVGZhYW37CtbHA.htm](spells/uncommon-04-e9tVGZhYW37CtbHA.htm)|Movanic Glimmer|Éclair movanique|libre|
|[uncommon-04-eb4FXf62NYArTqek.htm](spells/uncommon-04-eb4FXf62NYArTqek.htm)|Artistic Flourish|Fioritures artistiques|officielle|
|[uncommon-04-eG1fBodYwolaXK98.htm](spells/uncommon-04-eG1fBodYwolaXK98.htm)|Enduring Might|Puissance protectrice|officielle|
|[uncommon-04-ehd9PtifLMwjk4ep.htm](spells/uncommon-04-ehd9PtifLMwjk4ep.htm)|Concealment's Curtain|Rideau de dissimulation|libre|
|[uncommon-04-f9duI5d8xNYqxI8d.htm](spells/uncommon-04-f9duI5d8xNYqxI8d.htm)|Magic Mailbox|Boite aux lettres magiques|libre|
|[uncommon-04-FhOaQDTSnsY7tiam.htm](spells/uncommon-04-FhOaQDTSnsY7tiam.htm)|Rewrite Memory|Réécriture de la mémoire|libre|
|[uncommon-04-FM3SmEW8N1FCRjqt.htm](spells/uncommon-04-FM3SmEW8N1FCRjqt.htm)|Talking Corpse|Cadavre loquace|officielle|
|[uncommon-04-GjmMFEJcDd6MDG2P.htm](spells/uncommon-04-GjmMFEJcDd6MDG2P.htm)|Spiritual Attunement|Harmonisation spirituelle|libre|
|[uncommon-04-GqvKSxzN7A7kuFk4.htm](spells/uncommon-04-GqvKSxzN7A7kuFk4.htm)|Tempt Fate|Tenter le destin|libre|
|[uncommon-04-gSFg9zKwgcNZLMEs.htm](spells/uncommon-04-gSFg9zKwgcNZLMEs.htm)|Stormwind Flight|Vol de l'ouragan|officielle|
|[uncommon-04-GzdgM0m7wXKuFSho.htm](spells/uncommon-04-GzdgM0m7wXKuFSho.htm)|Rope Trick|Corde enchantée|officielle|
|[uncommon-04-HBJPsonQnWcC3qdX.htm](spells/uncommon-04-HBJPsonQnWcC3qdX.htm)|Mirage|Mirage|libre|
|[uncommon-04-HqZ1VWIcXXZXWm3K.htm](spells/uncommon-04-HqZ1VWIcXXZXWm3K.htm)|Web of Influence|Toile d'influence|libre|
|[uncommon-04-i7u6gAdNcyIyyo3h.htm](spells/uncommon-04-i7u6gAdNcyIyyo3h.htm)|Favorable Review|Critique favorable|libre|
|[uncommon-04-I8CPe9Pp7GABqOyB.htm](spells/uncommon-04-I8CPe9Pp7GABqOyB.htm)|Zeal for Battle|Zèle au combat|officielle|
|[uncommon-04-ivKnEtI1z4UqEKIA.htm](spells/uncommon-04-ivKnEtI1z4UqEKIA.htm)|Pulse of Civilization|Pouls de la civilisation|libre|
|[uncommon-04-J5MNC4xq3CHH31qT.htm](spells/uncommon-04-J5MNC4xq3CHH31qT.htm)|Eradicate Undeath|Éradication de la mort-vivance|libre|
|[uncommon-04-J8pL8yTshga8QOk8.htm](spells/uncommon-04-J8pL8yTshga8QOk8.htm)|Delusional Pride|Fierté illusoire|officielle|
|[uncommon-04-jJphHQlENHFlSElH.htm](spells/uncommon-04-jJphHQlENHFlSElH.htm)|Captivating Adoration|Adoration captivante|officielle|
|[uncommon-04-Jli9WBjQZ2MmKJ8y.htm](spells/uncommon-04-Jli9WBjQZ2MmKJ8y.htm)|Spiritual Anamnesis|Anamnèse spirituelle|libre|
|[uncommon-04-K4LXpaBWrGy6jIER.htm](spells/uncommon-04-K4LXpaBWrGy6jIER.htm)|Downpour|Déluge|libre|
|[uncommon-04-kF0rs9mCPvJGfAZE.htm](spells/uncommon-04-kF0rs9mCPvJGfAZE.htm)|Fortissimo Composition|Composition fortissimo|officielle|
|[uncommon-04-l6zjNysNedpJcmDT.htm](spells/uncommon-04-l6zjNysNedpJcmDT.htm)|Know the Enemy|Connaître son ennemi|officielle|
|[uncommon-04-L8pzCOi7Jzx5ALs9.htm](spells/uncommon-04-L8pzCOi7Jzx5ALs9.htm)|Disperse into Air|Disparition dans les airs|officielle|
|[uncommon-04-LoBjvguamA12iyW0.htm](spells/uncommon-04-LoBjvguamA12iyW0.htm)|Energy Absorption|Absorption d'énergie|officielle|
|[uncommon-04-LVwmAH5NGvTuuQSU.htm](spells/uncommon-04-LVwmAH5NGvTuuQSU.htm)|Swarming Wasp Stings|Nuée de dards de guêpes|libre|
|[uncommon-04-NxOYiKCqcuAHVRCj.htm](spells/uncommon-04-NxOYiKCqcuAHVRCj.htm)|Transcribe Moment|Instant transcrit|libre|
|[uncommon-04-oiUhJbJ3YCKF62Fu.htm](spells/uncommon-04-oiUhJbJ3YCKF62Fu.htm)|Darkened Eyes|Assombrir le regard|officielle|
|[uncommon-04-OM5NeD7a1CYNqy8S.htm](spells/uncommon-04-OM5NeD7a1CYNqy8S.htm)|Liminal Doorway|Entrée liminaire|libre|
|[uncommon-04-onjZCEHs3JJJRTD0.htm](spells/uncommon-04-onjZCEHs3JJJRTD0.htm)|Peaceful Bubble|Bulle paisible|libre|
|[uncommon-04-oOaEfsdVpgTXRhrY.htm](spells/uncommon-04-oOaEfsdVpgTXRhrY.htm)|Runic Impression|Impression runique|libre|
|[uncommon-04-oXCwHBeDja4e0Mx0.htm](spells/uncommon-04-oXCwHBeDja4e0Mx0.htm)|Touch of the Moon|Contact lunaire|officielle|
|[uncommon-04-pkcOby5prOausy1k.htm](spells/uncommon-04-pkcOby5prOausy1k.htm)|Read Omens|Lire les présages|officielle|
|[uncommon-04-pZBovpzdBNLQQQmE.htm](spells/uncommon-04-pZBovpzdBNLQQQmE.htm)|Repel Metal|Répulsion du métal|libre|
|[uncommon-04-PztLrElcZfLwRnEq.htm](spells/uncommon-04-PztLrElcZfLwRnEq.htm)|Dreamer's Call|Appel du rêveur|libre|
|[uncommon-04-qzsQmpiQodHBBWYI.htm](spells/uncommon-04-qzsQmpiQodHBBWYI.htm)|Malignant Sustenance|Alimentation maléfique|libre|
|[uncommon-04-S1Msrwi990FE7uMO.htm](spells/uncommon-04-S1Msrwi990FE7uMO.htm)|Call The Blood|Appel du sang|libre|
|[uncommon-04-s3LISC5Z55urpBgU.htm](spells/uncommon-04-s3LISC5Z55urpBgU.htm)|Path of Least Resistance|Chemin de la moindre résistance|libre|
|[uncommon-04-S7ylpCJyq0CYkux9.htm](spells/uncommon-04-S7ylpCJyq0CYkux9.htm)|Clownish Curse|Malédiction clownesque|libre|
|[uncommon-04-SDkIFrrO1PsE02Kd.htm](spells/uncommon-04-SDkIFrrO1PsE02Kd.htm)|Shifting Form|Forme changeante|officielle|
|[uncommon-04-sGenGMmE1ntkXCtN.htm](spells/uncommon-04-sGenGMmE1ntkXCtN.htm)|Interdisciplinary Incantation|Incantation interdisciplinaire|libre|
|[uncommon-04-SkarN4VlNxSJSJNw.htm](spells/uncommon-04-SkarN4VlNxSJSJNw.htm)|Wild Winds Stance|Posture des vents violents|officielle|
|[uncommon-04-skvgOWNTitLehL0b.htm](spells/uncommon-04-skvgOWNTitLehL0b.htm)|Shared Nightmare|Cauchemar partagé|libre|
|[uncommon-04-SSsUC7rZo0CwayPn.htm](spells/uncommon-04-SSsUC7rZo0CwayPn.htm)|Retributive Pain|Souffrance vengeresse|libre|
|[uncommon-04-tYLBjOTvBVn9JtRb.htm](spells/uncommon-04-tYLBjOTvBVn9JtRb.htm)|Unity|Unité|officielle|
|[uncommon-04-uJXs4M6IeixfPBLc.htm](spells/uncommon-04-uJXs4M6IeixfPBLc.htm)|Clinging Shadows Stance|Posture des ombres tenaces|libre|
|[uncommon-04-uNsliWpl8Q1JdFcM.htm](spells/uncommon-04-uNsliWpl8Q1JdFcM.htm)|Discern Lies|Détection du mensonge|officielle|
|[uncommon-04-vGMbpV7GWIFPNUaZ.htm](spells/uncommon-04-vGMbpV7GWIFPNUaZ.htm)|Bursting Bloom|Fleur explosive|libre|
|[uncommon-04-VmqdVWCb8zAUCW8S.htm](spells/uncommon-04-VmqdVWCb8zAUCW8S.htm)|Debilitating Dichotomy|Dichotomie débilitante|libre|
|[uncommon-04-vSSKyUdrHu86E5Gk.htm](spells/uncommon-04-vSSKyUdrHu86E5Gk.htm)|Nature's Bounty|Générosité naturelle|libre|
|[uncommon-04-WVc30DGbM7TRLLul.htm](spells/uncommon-04-WVc30DGbM7TRLLul.htm)|Ghostly Tragedy|Tragédie fantomatique|libre|
|[uncommon-04-y7Tusv3CieZktkkV.htm](spells/uncommon-04-y7Tusv3CieZktkkV.htm)|Flame Barrier|Barrière de flammes|officielle|
|[uncommon-04-ZeHeNQ5BNq6m5F1j.htm](spells/uncommon-04-ZeHeNQ5BNq6m5F1j.htm)|Take its Course|Suivre son cours|officielle|
|[uncommon-04-ZhJ8d9Uk4lwIx86b.htm](spells/uncommon-04-ZhJ8d9Uk4lwIx86b.htm)|Plant Growth|Croissance végétale|libre|
|[uncommon-04-ZhLYJlOZzUB1OKoe.htm](spells/uncommon-04-ZhLYJlOZzUB1OKoe.htm)|Trickster's Twin|Jumeau du trompeur|officielle|
|[uncommon-04-ZjbVgIIqMstmdkqP.htm](spells/uncommon-04-ZjbVgIIqMstmdkqP.htm)|Glimpse the Truth|Entrevoir la vérité|officielle|
|[uncommon-04-zjG6NncHyAKqSF7m.htm](spells/uncommon-04-zjG6NncHyAKqSF7m.htm)|Dimensional Steps|Pas interdimensionnel|officielle|
|[uncommon-04-zjYQyuBNT5VjV4mz.htm](spells/uncommon-04-zjYQyuBNT5VjV4mz.htm)|Elephant Form|Forme d'éléphant|libre|
|[uncommon-05-115Xp9E38CJENhNS.htm](spells/uncommon-05-115Xp9E38CJENhNS.htm)|Magic Passage|Passage magique|libre|
|[uncommon-05-1QMjWGq1DPOQr4VL.htm](spells/uncommon-05-1QMjWGq1DPOQr4VL.htm)|Dread Ambience|Ambiance terrifiante|libre|
|[uncommon-05-2mVW1KT3AjW2pvDO.htm](spells/uncommon-05-2mVW1KT3AjW2pvDO.htm)|Litany against Sloth|Litanie contre la paresse|officielle|
|[uncommon-05-2YIr0S2Gt14PMMQp.htm](spells/uncommon-05-2YIr0S2Gt14PMMQp.htm)|Grasping Grave|Étreinte de la tombe|officielle|
|[uncommon-05-3DB3F2hIx2dMbX8n.htm](spells/uncommon-05-3DB3F2hIx2dMbX8n.htm)|The World's a Stage|Le monde est une scène|libre|
|[uncommon-05-3r897dYO8oYvuyn5.htm](spells/uncommon-05-3r897dYO8oYvuyn5.htm)|Summon Healing Servitor|Convocation de protecteur soigneur|libre|
|[uncommon-05-4Cntq9odgW6xMpAs.htm](spells/uncommon-05-4Cntq9odgW6xMpAs.htm)|Astral Projection|Projection astrale|libre|
|[uncommon-05-4cv1cuBHjYmVfBGI.htm](spells/uncommon-05-4cv1cuBHjYmVfBGI.htm)|Wisdom of the Winds|Sagesse des vents|libre|
|[uncommon-05-5NReCnDFFuThEAHB.htm](spells/uncommon-05-5NReCnDFFuThEAHB.htm)|Elemental Servitor|Serviteur élémentaire|libre|
|[uncommon-05-5ttAVJbWg2GVKmrN.htm](spells/uncommon-05-5ttAVJbWg2GVKmrN.htm)|Shadow Jump|Saut de l'ombre|libre|
|[uncommon-05-7uL4XhHpxZpgNJPh.htm](spells/uncommon-05-7uL4XhHpxZpgNJPh.htm)|Blinding Foam|Mousse aveuglante|libre|
|[uncommon-05-8rAcQOvm0nFrqAUt.htm](spells/uncommon-05-8rAcQOvm0nFrqAUt.htm)|Lashunta's Life Bubble|Bulle de vie du lashunta|libre|
|[uncommon-05-8TBgEzjZxPaOJOm1.htm](spells/uncommon-05-8TBgEzjZxPaOJOm1.htm)|Wronged Monk's Wrath|Courroux du moine lésé|libre|
|[uncommon-05-92JNBuY5pfFmywOd.htm](spells/uncommon-05-92JNBuY5pfFmywOd.htm)|Desperate Repair|Réparation désespérée|libre|
|[uncommon-05-9bOqFkewRz7Z3HZ5.htm](spells/uncommon-05-9bOqFkewRz7Z3HZ5.htm)|Scouring Pulse|Impulsion décapante|libre|
|[uncommon-05-9LHr9SuDLTicdbXs.htm](spells/uncommon-05-9LHr9SuDLTicdbXs.htm)|Hellfire Plume|Volute de feu infernal|libre|
|[uncommon-05-9o5aG5025ZczjkPb.htm](spells/uncommon-05-9o5aG5025ZczjkPb.htm)|Untwisting Iron Roots|Racines du fer qui ne plie pas|libre|
|[uncommon-05-9WlTR9JlEcjRmGiD.htm](spells/uncommon-05-9WlTR9JlEcjRmGiD.htm)|Celestial Brand|Marque céleste|libre|
|[uncommon-05-ALuvl9GYawnsZZCx.htm](spells/uncommon-05-ALuvl9GYawnsZZCx.htm)|Unfolding Wind Buffet|Ballotement du vent qui se déploie|libre|
|[uncommon-05-B3tbO85GBpzQ3u8l.htm](spells/uncommon-05-B3tbO85GBpzQ3u8l.htm)|Wish-Twisted Form|Forme déformée par un souhait|libre|
|[uncommon-05-BAu5AgqoO556clyK.htm](spells/uncommon-05-BAu5AgqoO556clyK.htm)|Rewrite Possibility|Réécrire les éventualités|libre|
|[uncommon-05-bZMwSGc9t5K7uxZV.htm](spells/uncommon-05-bZMwSGc9t5K7uxZV.htm)|Redistribute Potential|Potentiel redistribué|libre|
|[uncommon-05-crF4g9jRN1y84MSD.htm](spells/uncommon-05-crF4g9jRN1y84MSD.htm)|Abyssal Wrath|Colère chthonienne|officielle|
|[uncommon-05-ddKBoCjmSyPSHcws.htm](spells/uncommon-05-ddKBoCjmSyPSHcws.htm)|Unusual Anatomy|Étrange anatomie|officielle|
|[uncommon-05-ddqmGnShyoRBKFkZ.htm](spells/uncommon-05-ddqmGnShyoRBKFkZ.htm)|Engrave Memory|Mémoire gravée|libre|
|[uncommon-05-DgCS456mXKw97vNy.htm](spells/uncommon-05-DgCS456mXKw97vNy.htm)|Ode to Ouroboros|Ode à Ouroboros|libre|
|[uncommon-05-ES6FkwXXqYr4ujQH.htm](spells/uncommon-05-ES6FkwXXqYr4ujQH.htm)|Blood Feast|Festin sanglant|libre|
|[uncommon-05-F1qxaqsEItmBura2.htm](spells/uncommon-05-F1qxaqsEItmBura2.htm)|Nature's Pathway|Chemin de la nature|officielle|
|[uncommon-05-floDCUOSzT0F6r77.htm](spells/uncommon-05-floDCUOSzT0F6r77.htm)|Unblinking Flame Aura|Aura de la flamme qui ne vacille pas|libre|
|[uncommon-05-gfVXAW95YWRz0pJC.htm](spells/uncommon-05-gfVXAW95YWRz0pJC.htm)|Telepathic Bond|Lien télépathique|officielle|
|[uncommon-05-gsYEuWv04XTDxe91.htm](spells/uncommon-05-gsYEuWv04XTDxe91.htm)|Call Spirit|Appel d'un esprit|officielle|
|[uncommon-05-Hp2wmnRS6TUGZlZ2.htm](spells/uncommon-05-Hp2wmnRS6TUGZlZ2.htm)|Rallying Banner|Bannière de ralliement|libre|
|[uncommon-05-HWJODX2zPg5cg34F.htm](spells/uncommon-05-HWJODX2zPg5cg34F.htm)|Dragon Wings|Ailes de dragon|officielle|
|[uncommon-05-Ifc2b6bNVdjKV7Si.htm](spells/uncommon-05-Ifc2b6bNVdjKV7Si.htm)|Stormburst|Explosion tempêtueuse|libre|
|[uncommon-05-IoHxAkK0uGqrgtWl.htm](spells/uncommon-05-IoHxAkK0uGqrgtWl.htm)|Wyvern Sting|Dard de vouivre|libre|
|[uncommon-05-kqhPt9344UkcGVYO.htm](spells/uncommon-05-kqhPt9344UkcGVYO.htm)|Resurrect|Résurrection|libre|
|[uncommon-05-kRxlkPPe6Gr7Du59.htm](spells/uncommon-05-kRxlkPPe6Gr7Du59.htm)|Wind Jump|Saut du vent|officielle|
|[uncommon-05-mm3hZ6jgaJaKK16n.htm](spells/uncommon-05-mm3hZ6jgaJaKK16n.htm)|Litany of Self-Interest|Litanie d'égoïsme|libre|
|[uncommon-05-NIop4eI2i7cKFGad.htm](spells/uncommon-05-NIop4eI2i7cKFGad.htm)|Mirecloak|Cape-miroir|libre|
|[uncommon-05-nnYxDWU3Vzg9WenB.htm](spells/uncommon-05-nnYxDWU3Vzg9WenB.htm)|Fire's Pathway|Passage par le feu|libre|
|[uncommon-05-pCvJ4yoZJxDtgUMI.htm](spells/uncommon-05-pCvJ4yoZJxDtgUMI.htm)|Restorative Moment|Moment restoratif|libre|
|[uncommon-05-PEfSofHm73IT3Khc.htm](spells/uncommon-05-PEfSofHm73IT3Khc.htm)|House of Imaginary Walls|Demeure aux murs imaginaires|officielle|
|[uncommon-05-PgDFDvX64eswapSS.htm](spells/uncommon-05-PgDFDvX64eswapSS.htm)|Ectoplasmic Expulsion|Excision ectoplasmique|libre|
|[uncommon-05-QIjc7Zyej0P3b9v5.htm](spells/uncommon-05-QIjc7Zyej0P3b9v5.htm)|Oblivious Expulsion|Expulsion amnésique|libre|
|[uncommon-05-Qlp8G3knwLGhAxQ0.htm](spells/uncommon-05-Qlp8G3knwLGhAxQ0.htm)|Elemental Blast|Déflagration élémentaire|officielle|
|[uncommon-05-qxJE2iSJwPFLZrcK.htm](spells/uncommon-05-qxJE2iSJwPFLZrcK.htm)|Belittling Boast|Vantardise dévalorisante|libre|
|[uncommon-05-RCbLd7dfquHnuvrZ.htm](spells/uncommon-05-RCbLd7dfquHnuvrZ.htm)|False Vision|Vision faussée|libre|
|[uncommon-05-rtA3HRGoy7PQTOhq.htm](spells/uncommon-05-rtA3HRGoy7PQTOhq.htm)|Terrain Transposition|Transposition de terrain|libre|
|[uncommon-05-ru3YdXajUREbKQDV.htm](spells/uncommon-05-ru3YdXajUREbKQDV.htm)|Return Beacon|Phare de retour|libre|
|[uncommon-05-rxvS7EMJ7qmexAyA.htm](spells/uncommon-05-rxvS7EMJ7qmexAyA.htm)|Umbral Journey|Voyage ombral|officielle|
|[uncommon-05-SnBc7Gl5VormWzS1.htm](spells/uncommon-05-SnBc7Gl5VormWzS1.htm)|Bountiful Oasis|Oasis abondant|libre|
|[uncommon-05-SwUiVavHKMWG7t5K.htm](spells/uncommon-05-SwUiVavHKMWG7t5K.htm)|Truespeech|Langage universel|officielle|
|[uncommon-05-SzKkzq3Rr6vKIxbp.htm](spells/uncommon-05-SzKkzq3Rr6vKIxbp.htm)|Shepherd of Souls|Berger des âmes|libre|
|[uncommon-05-tf4PMMMzR5xxJDun.htm](spells/uncommon-05-tf4PMMMzR5xxJDun.htm)|Cleansing Flames|Flammes purificatrices|libre|
|[uncommon-05-tj86Rnq3QuQnDtG3.htm](spells/uncommon-05-tj86Rnq3QuQnDtG3.htm)|Hunter's Vision|Vision du chasseur|libre|
|[uncommon-05-VDWIZuLOJqwBthHc.htm](spells/uncommon-05-VDWIZuLOJqwBthHc.htm)|Ravening Maw|Mâchoires dévorantes|libre|
|[uncommon-05-vgy00hnqxN9VoeoF.htm](spells/uncommon-05-vgy00hnqxN9VoeoF.htm)|Planar Servitor|Serviteur planaire|libre|
|[uncommon-05-wYSLTNvmxbe78l2c.htm](spells/uncommon-05-wYSLTNvmxbe78l2c.htm)|Soulshelter Vessel|Réceptacle spirituel|libre|
|[uncommon-05-x2LALaHXO7644GQA.htm](spells/uncommon-05-x2LALaHXO7644GQA.htm)|You're Mine|Tu es à moi|officielle|
|[uncommon-05-XlQBVvlDWGrGlApl.htm](spells/uncommon-05-XlQBVvlDWGrGlApl.htm)|Establish Ward|Établir une protection|libre|
|[uncommon-05-Y1YysjZ40ft0aLFN.htm](spells/uncommon-05-Y1YysjZ40ft0aLFN.htm)|Spiritual Torrent|Torrent spirituel|libre|
|[uncommon-05-y5pzaNfb17CM1slC.htm](spells/uncommon-05-y5pzaNfb17CM1slC.htm)|Blackfinger's Blades|Lames de Doigtnoir|libre|
|[uncommon-05-yRf59eFtZ50cGlem.htm](spells/uncommon-05-yRf59eFtZ50cGlem.htm)|Heroes' Feast|Festin des héros|libre|
|[uncommon-05-Z60JRD78wT3afOEJ.htm](spells/uncommon-05-Z60JRD78wT3afOEJ.htm)|Portrait of Spite|Portrait de dépit|libre|
|[uncommon-05-ZAX0OOcKtYMQlquR.htm](spells/uncommon-05-ZAX0OOcKtYMQlquR.htm)|Symphony of the Unfettered Heart|Symphonie du coeur affranchi|libre|
|[uncommon-05-zoY0fQYTF1NzezTg.htm](spells/uncommon-05-zoY0fQYTF1NzezTg.htm)|Steal the Sky|Voler le ciel|libre|
|[uncommon-05-ZQk2cGBCkATO25w0.htm](spells/uncommon-05-ZQk2cGBCkATO25w0.htm)|Unbreaking Wave Vapor|Vapeur de la vague inexorable|libre|
|[uncommon-06-2ykmAVKrsAWcazcC.htm](spells/uncommon-06-2ykmAVKrsAWcazcC.htm)|Binding Circle|Cercle de négociation|libre|
|[uncommon-06-3KiM09e8DN9AdTPA.htm](spells/uncommon-06-3KiM09e8DN9AdTPA.htm)|Hag's Fruit|Fruit de la guenaude|libre|
|[uncommon-06-9lz67JCNCLDfIEy3.htm](spells/uncommon-06-9lz67JCNCLDfIEy3.htm)|Awaken Object|Éveil d'objet|libre|
|[uncommon-06-EX6gTy8s4wNyhOnl.htm](spells/uncommon-06-EX6gTy8s4wNyhOnl.htm)|Heinous Future|Avenir funeste|libre|
|[uncommon-06-GzN9bG6cKZ96YC6l.htm](spells/uncommon-06-GzN9bG6cKZ96YC6l.htm)|Claim Undead|Réclamer un mort-vivant|libre|
|[uncommon-06-IkGYwHRLhkuoGReG.htm](spells/uncommon-06-IkGYwHRLhkuoGReG.htm)|Raise Dead|Rappel à la vie|libre|
|[uncommon-06-In2A7GCyxxaqZdPI.htm](spells/uncommon-06-In2A7GCyxxaqZdPI.htm)|Moonlight Bridge|Pont de Lumière lunaire|libre|
|[uncommon-06-kwlKUxEuT8T15YW6.htm](spells/uncommon-06-kwlKUxEuT8T15YW6.htm)|Primal Summons|Convocations primordiales|libre|
|[uncommon-06-M6HsK7W6JMkJXeog.htm](spells/uncommon-06-M6HsK7W6JMkJXeog.htm)|Ideal Mimicry|Imitation idéale|libre|
|[uncommon-06-MT8usUfwudDVUm5H.htm](spells/uncommon-06-MT8usUfwudDVUm5H.htm)|Manifold Lives|Vies multiples|libre|
|[uncommon-06-NCUXsaj2yFxOVhrK.htm](spells/uncommon-06-NCUXsaj2yFxOVhrK.htm)|Create Skinstitch|Création de maille-peaux|libre|
|[uncommon-06-OsOhx3TGIZ7AhD0P.htm](spells/uncommon-06-OsOhx3TGIZ7AhD0P.htm)|Dominate|Domination|officielle|
|[uncommon-06-pZc8ZwtsyWnxUUW0.htm](spells/uncommon-06-pZc8ZwtsyWnxUUW0.htm)|Primal Call|Appel primordial|libre|
|[uncommon-06-ReUoodsqUYSTwSgE.htm](spells/uncommon-06-ReUoodsqUYSTwSgE.htm)|Fateful Condemnation|Condamnation funeste|libre|
|[uncommon-06-s0SerOrkkUd7SAH9.htm](spells/uncommon-06-s0SerOrkkUd7SAH9.htm)|Bacchanalia|Bacchanales|libre|
|[uncommon-06-sYd3hqv2ZQqa0PSc.htm](spells/uncommon-06-sYd3hqv2ZQqa0PSc.htm)|Sky Signs|Signes célestes|libre|
|[uncommon-06-USM530HlzZ1RMd99.htm](spells/uncommon-06-USM530HlzZ1RMd99.htm)|Champion's Sacrifice|Sacrifice du champion|officielle|
|[uncommon-06-vF52ktg0wUIAlf57.htm](spells/uncommon-06-vF52ktg0wUIAlf57.htm)|Mantle of Heaven's Slopes|Manteau des pentes du Paradis|libre|
|[uncommon-06-XULNb8ItUsfupxqH.htm](spells/uncommon-06-XULNb8ItUsfupxqH.htm)|Dread Secret|Secret effroyable|libre|
|[uncommon-06-Z82DvtSXafPmh4KV.htm](spells/uncommon-06-Z82DvtSXafPmh4KV.htm)|Shambling Horror|Horreur ambulante|libre|
|[uncommon-06-Zvg0FWzClGbzucFd.htm](spells/uncommon-06-Zvg0FWzClGbzucFd.htm)|Speaking Sky|Ciel parlant|libre|
|[uncommon-07-0JigNJDRwevZOyjI.htm](spells/uncommon-07-0JigNJDRwevZOyjI.htm)|Soothing Ballad|Ballade apaisante|officielle|
|[uncommon-07-1VOjUKd1pacI67RZ.htm](spells/uncommon-07-1VOjUKd1pacI67RZ.htm)|Curse of the Spirit Orchestra|Malédiction de l'orchestre d'eprits|libre|
|[uncommon-07-37ESlJzUvVbOudOT.htm](spells/uncommon-07-37ESlJzUvVbOudOT.htm)|Reverse Gravity|Inversion de la gravité|libre|
|[uncommon-07-3MzuRDc7ccylpW2e.htm](spells/uncommon-07-3MzuRDc7ccylpW2e.htm)|Hasted Assault|Assaut hâtif|libre|
|[uncommon-07-5bTt2CvYHPvaR7QQ.htm](spells/uncommon-07-5bTt2CvYHPvaR7QQ.htm)|Interplanar Teleport|Téléportation interplanaire|libre|
|[uncommon-07-5ZW1w9f4gWlSIuWA.htm](spells/uncommon-07-5ZW1w9f4gWlSIuWA.htm)|Teleportation Circle|Cercle de téléportation|libre|
|[uncommon-07-6TKGaQm4PfEMkeRd.htm](spells/uncommon-07-6TKGaQm4PfEMkeRd.htm)|Supreme Connection|Connection suprème|libre|
|[uncommon-07-8eOMAIvT7S1CJSit.htm](spells/uncommon-07-8eOMAIvT7S1CJSit.htm)|Untwisting Iron Augmentation|Amélioration du fer qui ne plie pas|libre|
|[uncommon-07-HmKajQS0DP23bipp.htm](spells/uncommon-07-HmKajQS0DP23bipp.htm)|Planar Displacement|Déplacement planaire|libre|
|[uncommon-07-hp6Q64dl7xbdn4gQ.htm](spells/uncommon-07-hp6Q64dl7xbdn4gQ.htm)|Collective Memories|Mémoires collectives|libre|
|[uncommon-07-IQchIYUwbsVTa9Mc.htm](spells/uncommon-07-IQchIYUwbsVTa9Mc.htm)|Allegro|Allégro|officielle|
|[uncommon-07-KADeCawMG6WAzYHa.htm](spells/uncommon-07-KADeCawMG6WAzYHa.htm)|Unblinking Flame Emblem|Emblème de la flamme qui ne vacille pas|libre|
|[uncommon-07-n3b3pDmA6L5YRTyq.htm](spells/uncommon-07-n3b3pDmA6L5YRTyq.htm)|Litany of Righteousness|Litanie de vertu|libre|
|[uncommon-07-n8eEXXAtguoErW0y.htm](spells/uncommon-07-n8eEXXAtguoErW0y.htm)|Shadow's Web|Toile d'ombre|libre|
|[uncommon-07-OOELvfkTedBMlWtq.htm](spells/uncommon-07-OOELvfkTedBMlWtq.htm)|Spell Turning|Renvoi de sorts|libre|
|[uncommon-07-qOeBQyC1z7OScHvP.htm](spells/uncommon-07-qOeBQyC1z7OScHvP.htm)|Maze of Locked Doors|Dédale de portes fermées|libre|
|[uncommon-07-QVMjPfXlpnmeuWKS.htm](spells/uncommon-07-QVMjPfXlpnmeuWKS.htm)|Leng Sting|Piqûre de Leng|libre|
|[uncommon-07-Shiuhdb2nO6Qgk3k.htm](spells/uncommon-07-Shiuhdb2nO6Qgk3k.htm)|Moonburst|Éruption lunaire|libre|
|[uncommon-07-vPWMEyVTreMOoFnm.htm](spells/uncommon-07-vPWMEyVTreMOoFnm.htm)|Planar Palace|Palais planaire|libre|
|[uncommon-07-wU6hNzK8Yfqdmc8m.htm](spells/uncommon-07-wU6hNzK8Yfqdmc8m.htm)|Possession|Possession|libre|
|[uncommon-07-Ww4cPZ3QHTSpaM1m.htm](spells/uncommon-07-Ww4cPZ3QHTSpaM1m.htm)|Unfolding Wind Blitz|Attaques éclair du vent qui se déploie|libre|
|[uncommon-07-XZE4BawIlTf88Yl9.htm](spells/uncommon-07-XZE4BawIlTf88Yl9.htm)|Planar Seal|Sceau planaire|officielle|
|[uncommon-07-Yk3t4ekEiFIoEz9c.htm](spells/uncommon-07-Yk3t4ekEiFIoEz9c.htm)|Power Word Blind|Mot de pouvoir aveuglant|officielle|
|[uncommon-07-YtJXpiu4ijkB6nP2.htm](spells/uncommon-07-YtJXpiu4ijkB6nP2.htm)|Unbreaking Wave Barrier|Barrière de la vague inexorable|libre|
|[uncommon-08-7PJSqUeKxTqOVrPk.htm](spells/uncommon-08-7PJSqUeKxTqOVrPk.htm)|Power Word Stun|Mot de pouvoir étourdissant|officielle|
|[uncommon-08-8fEfjvC01gNclDKJ.htm](spells/uncommon-08-8fEfjvC01gNclDKJ.htm)|Deluge|Déluge intense|libre|
|[uncommon-08-C2w3YfBKjIRS07DP.htm](spells/uncommon-08-C2w3YfBKjIRS07DP.htm)|Hidden Mind|Esprit dissimulé|libre|
|[uncommon-08-CeSh8QcVnqP5OlLj.htm](spells/uncommon-08-CeSh8QcVnqP5OlLj.htm)|Pinpoint|Déterminer la position|libre|
|[uncommon-08-Ovvflf5aFbmBxqq8.htm](spells/uncommon-08-Ovvflf5aFbmBxqq8.htm)|Quivering Palm|Paume vibratoire|officielle|
|[uncommon-08-PgLvO8UNHSj5f61m.htm](spells/uncommon-08-PgLvO8UNHSj5f61m.htm)|Devour Life|Dévorer la vie|libre|
|[uncommon-08-pt3gEnzA159uHcJC.htm](spells/uncommon-08-pt3gEnzA159uHcJC.htm)|Prying Survey|Surveillance scrutatrice|libre|
|[uncommon-08-wTYxxYJWN348oV15.htm](spells/uncommon-08-wTYxxYJWN348oV15.htm)|Medusa's Wrath|Courroux de la méduse|libre|
|[uncommon-08-XkDCzMIyc0YOjw05.htm](spells/uncommon-08-XkDCzMIyc0YOjw05.htm)|Control Weather|Contrôle du climat|officielle|
|[uncommon-08-YiIOsc8T6E2iDxgh.htm](spells/uncommon-08-YiIOsc8T6E2iDxgh.htm)|Undermine Reality|Saper la réalité|libre|
|[uncommon-09-9DtuMEyHhtHFRp5a.htm](spells/uncommon-09-9DtuMEyHhtHFRp5a.htm)|Untwisting Iron Pillar|Pilier du fer qui ne plie pas|libre|
|[uncommon-09-aFd00WT266VYPqOG.htm](spells/uncommon-09-aFd00WT266VYPqOG.htm)|Unblinking Flame Ignition|Allumage de la flamme qui ne vacille pas|libre|
|[uncommon-09-axYqY70kYu2lN20R.htm](spells/uncommon-09-axYqY70kYu2lN20R.htm)|Unbreaking Wave Containment|Endiguement de la vague inexorable|libre|
|[uncommon-09-BI4iwu3nApyIG0zY.htm](spells/uncommon-09-BI4iwu3nApyIG0zY.htm)|Astral Labyrinth|Labyrinthe astral|libre|
|[uncommon-09-GYmXvS9NJ7QwfWGg.htm](spells/uncommon-09-GYmXvS9NJ7QwfWGg.htm)|Seize Soul|Appropriation de l'âme|libre|
|[uncommon-09-ihbRf964JDXztcy3.htm](spells/uncommon-09-ihbRf964JDXztcy3.htm)|Disjunction|Disjonction|officielle|
|[uncommon-09-KkimKrhDxEJVm6TH.htm](spells/uncommon-09-KkimKrhDxEJVm6TH.htm)|Vital Singularity|Singularité vitale|libre|
|[uncommon-09-kUSShxXzY1sPtJA0.htm](spells/uncommon-09-kUSShxXzY1sPtJA0.htm)|Detonate Magic|Magie détonante|libre|
|[uncommon-09-m3lcOFm400lQCUps.htm](spells/uncommon-09-m3lcOFm400lQCUps.htm)|Power Word Kill|Mot de pouvoir mortel|officielle|
|[uncommon-09-mau1Olq58ECF0ZPi.htm](spells/uncommon-09-mau1Olq58ECF0ZPi.htm)|Empty Body|Désertion de l'âme|officielle|
|[uncommon-09-xFY9RtDE4DQKlWNR.htm](spells/uncommon-09-xFY9RtDE4DQKlWNR.htm)|Crusade|Croisade|officielle|
|[uncommon-09-XjNROFtWnwovhCsq.htm](spells/uncommon-09-XjNROFtWnwovhCsq.htm)|Unfolding Wind Crash|Crash du vent qui se déploie|libre|
|[uncommon-09-xxhS66k68u5iIOHC.htm](spells/uncommon-09-xxhS66k68u5iIOHC.htm)|Upheaval|Soulèvement|libre|
|[uncommon-09-YDMOqndvYFu3OjA6.htm](spells/uncommon-09-YDMOqndvYFu3OjA6.htm)|Ki Form|Forme ki|libre|
|[uncommon-09-ZMY58Yk5hnyfeE3q.htm](spells/uncommon-09-ZMY58Yk5hnyfeE3q.htm)|Linnorm Sting|Piqûre de linnorm|libre|
|[uncommon-10-dMKP4fkWx8V2cqAy.htm](spells/uncommon-10-dMKP4fkWx8V2cqAy.htm)|Remake|Recréer|libre|
|[uncommon-10-lyJDBD9OFW11vLyT.htm](spells/uncommon-10-lyJDBD9OFW11vLyT.htm)|Fatal Aria|Aria fatale|officielle|
|[uncommon-10-qqQYrXaRJXr7uc4i.htm](spells/uncommon-10-qqQYrXaRJXr7uc4i.htm)|Apex Companion|Compagnon alpha|libre|
|[uncommon-10-uGXWkR2h8q9MRzEM.htm](spells/uncommon-10-uGXWkR2h8q9MRzEM.htm)|Hero's Defiance|Défi du héros|officielle|
|[uncommon-10-Um0aaJotqMKGmAlR.htm](spells/uncommon-10-Um0aaJotqMKGmAlR.htm)|Pied Piping|Flûte de Hamelin|libre|
|[unique-10-nuE9qsY2HfPFEgAo.htm](spells/unique-10-nuE9qsY2HfPFEgAo.htm)|Raga of Remembrance|Râga du souvenir|libre|
