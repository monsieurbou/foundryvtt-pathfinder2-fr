# État de la traduction (age-of-ashes-bestiary)

 * **officielle**: 86
 * **libre**: 41
 * **changé**: 2


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[N8vOjTD2SqS2b9Sy.htm](age-of-ashes-bestiary/N8vOjTD2SqS2b9Sy.htm)|Lesser Manifestation Of Dahak|Manifestation mineure de Dahak|changé|
|[xPV49ZBCwDdCq5eI.htm](age-of-ashes-bestiary/xPV49ZBCwDdCq5eI.htm)|Vazgorlu|Vazgorlu|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[080v247YtmFRxT3l.htm](age-of-ashes-bestiary/080v247YtmFRxT3l.htm)|Bshez "Sand Claws" Shak|Bshez "Griffes de sable" Shak|officielle|
|[1lkay2gwgEquq0NF.htm](age-of-ashes-bestiary/1lkay2gwgEquq0NF.htm)|Scarlet Triad Sneak|Fureteur de la Triade Écarlate|libre|
|[1tDEWL9mAIXvTYik.htm](age-of-ashes-bestiary/1tDEWL9mAIXvTYik.htm)|Warbal Bumblebrasher|Warbal des Impétueux-Bafouilleurs|officielle|
|[2FbG5nLE91b6BNiB.htm](age-of-ashes-bestiary/2FbG5nLE91b6BNiB.htm)|Duergar Slave Lord|Seigneur esclavagiste Duergar|officielle|
|[30FX1WFr0RkGeueX.htm](age-of-ashes-bestiary/30FX1WFr0RkGeueX.htm)|Inizra Arumelo|Inizra Arumélo|officielle|
|[4vEQ8zRXC51Qo4Mv.htm](age-of-ashes-bestiary/4vEQ8zRXC51Qo4Mv.htm)|Bloody Blade Mercenary|Mercenaire des Lames Sanglantes|officielle|
|[53lk7ek73j65A09B.htm](age-of-ashes-bestiary/53lk7ek73j65A09B.htm)|Candlaron's Echo|Écho de Candlaron|libre|
|[595MAHwB4JWtzLAA.htm](age-of-ashes-bestiary/595MAHwB4JWtzLAA.htm)|Soulbound Ruin|Ruine des âmes|libre|
|[5dSVk2y88SLsPPON.htm](age-of-ashes-bestiary/5dSVk2y88SLsPPON.htm)|Hermean Mutant|Mutants d'Herméa|officielle|
|[5DwiLZJf6KxpIjDq.htm](age-of-ashes-bestiary/5DwiLZJf6KxpIjDq.htm)|Grippli Archer|Archer Grippli|officielle|
|[5jFR05QRixpOChRy.htm](age-of-ashes-bestiary/5jFR05QRixpOChRy.htm)|Deculi|Deculi|officielle|
|[5r2S6FeE6D1Oh66T.htm](age-of-ashes-bestiary/5r2S6FeE6D1Oh66T.htm)|Tarrasque, The Armageddon Engine|La Tarasque, le Porteur d'Armageddon|officielle|
|[60aeSJvu09ZM3SPx.htm](age-of-ashes-bestiary/60aeSJvu09ZM3SPx.htm)|Racharak|Racharak|officielle|
|[6AN7eagk2WrWc4im.htm](age-of-ashes-bestiary/6AN7eagk2WrWc4im.htm)|Mengkare|Mengkare|libre|
|[7WNFNE3SVMRGbBDf.htm](age-of-ashes-bestiary/7WNFNE3SVMRGbBDf.htm)|Scarlet Triad Sniper|Tireuse embusquée de la Triade Écarlate|officielle|
|[91BSCAJA1Oto2ctf.htm](age-of-ashes-bestiary/91BSCAJA1Oto2ctf.htm)|Blood Boar|Sanglier sanglant|officielle|
|[9oNwQuwKGUuG9G9g.htm](age-of-ashes-bestiary/9oNwQuwKGUuG9G9g.htm)|Laslunn|Laslunn|officielle|
|[aaDiR0EIWRQx8wdy.htm](age-of-ashes-bestiary/aaDiR0EIWRQx8wdy.htm)|Calmont|Calmont|officielle|
|[Afq4Osh3W1k9Bcsh.htm](age-of-ashes-bestiary/Afq4Osh3W1k9Bcsh.htm)|Promise Guard|Garde de Promesse|officielle|
|[Aii18rhNPMLW4Pxh.htm](age-of-ashes-bestiary/Aii18rhNPMLW4Pxh.htm)|Skeletal Hellknight|Chevalier infernal squelettique|libre|
|[aiNE06bxKD6jfoLd.htm](age-of-ashes-bestiary/aiNE06bxKD6jfoLd.htm)|Uri Zandivar|Uri Zandivar|libre|
|[ajt6K2LyAxhJ8GuP.htm](age-of-ashes-bestiary/ajt6K2LyAxhJ8GuP.htm)|Black Powder Bomb|Bombe de poudre noire|officielle|
|[AtiN3EsRpHn5qbuv.htm](age-of-ashes-bestiary/AtiN3EsRpHn5qbuv.htm)|Immortal Ichor|Ichor immortel|libre|
|[axCgEgk7n1yXpgdp.htm](age-of-ashes-bestiary/axCgEgk7n1yXpgdp.htm)|Caustic Vapor|Vapeur corrosive|officielle|
|[BudeoAoJ4dOxjj0K.htm](age-of-ashes-bestiary/BudeoAoJ4dOxjj0K.htm)|Dahak's Shell|L'enveloppe de Dahak|officielle|
|[C4PD4p4I9byZ8yp6.htm](age-of-ashes-bestiary/C4PD4p4I9byZ8yp6.htm)|King Harral|Roi Harral|libre|
|[CJVRPFTKuJroiD5C.htm](age-of-ashes-bestiary/CJVRPFTKuJroiD5C.htm)|Xevalorg|Xevalorg|libre|
|[CmbcgWZgOSDmo2Pm.htm](age-of-ashes-bestiary/CmbcgWZgOSDmo2Pm.htm)|Precentor|Chef de choeur|libre|
|[dG5DBgrxlaimsWOS.htm](age-of-ashes-bestiary/dG5DBgrxlaimsWOS.htm)|Falrok|Falrok|libre|
|[dlW3UaXVpnzjd6xe.htm](age-of-ashes-bestiary/dlW3UaXVpnzjd6xe.htm)|Heuberk Thropp|Heuberk Thropp|libre|
|[dTikLHqGfiSYemuZ.htm](age-of-ashes-bestiary/dTikLHqGfiSYemuZ.htm)|Veshumirix|Veshumirix|officielle|
|[DxlauoGqSVDZWBOM.htm](age-of-ashes-bestiary/DxlauoGqSVDZWBOM.htm)|Acidic Needle Launcher|Lanceur d'aiguilles acides|officielle|
|[egTpOr4Wc0L5e0iY.htm](age-of-ashes-bestiary/egTpOr4Wc0L5e0iY.htm)|Scarlet Triad Enforcer|Sbire de la Triade Écarlate|officielle|
|[Ejxngh2tHFseZQHW.htm](age-of-ashes-bestiary/Ejxngh2tHFseZQHW.htm)|Vaklish|Vaklish|libre|
|[El7eSPV9aRi98Ufy.htm](age-of-ashes-bestiary/El7eSPV9aRi98Ufy.htm)|Mercenary Sailor|Marin mercenaire|officielle|
|[EUYisYdY3fcGL8zp.htm](age-of-ashes-bestiary/EUYisYdY3fcGL8zp.htm)|Lesser Dragonstorm|Tempête draconique mineure|officielle|
|[Ev930dfPpwCR8Zju.htm](age-of-ashes-bestiary/Ev930dfPpwCR8Zju.htm)|Ilgreth|Ilgreth|libre|
|[F9BHPV16N4eEL0TP.htm](age-of-ashes-bestiary/F9BHPV16N4eEL0TP.htm)|Dragonstorm Fire Giant|Géant du feu de la Tempête Draconique|officielle|
|[Fe1lYhCUY4UO4Plw.htm](age-of-ashes-bestiary/Fe1lYhCUY4UO4Plw.htm)|Talamira|Talamira|libre|
|[gF8Qy1k8gPBEUBAd.htm](age-of-ashes-bestiary/gF8Qy1k8gPBEUBAd.htm)|Scarlet Triad Mage|Magicien de la Triade Écarlate|libre|
|[H0pP1GqpMfX1WEiQ.htm](age-of-ashes-bestiary/H0pP1GqpMfX1WEiQ.htm)|Grauladon|Grauladon|officielle|
|[h7apR3QSxVZmy2nt.htm](age-of-ashes-bestiary/h7apR3QSxVZmy2nt.htm)|Dahak's Skull|Le crâne de Dahak|officielle|
|[H7NO4Q7ctHnfGKeJ.htm](age-of-ashes-bestiary/H7NO4Q7ctHnfGKeJ.htm)|Weathered Wail|La Plainte qui érode|libre|
|[HbgvZUYFJd2VfwmQ.htm](age-of-ashes-bestiary/HbgvZUYFJd2VfwmQ.htm)|Tree of Dreadful Dreams|Arbre des rêves effroyables|officielle|
|[HI2xA7LCpNPkpV03.htm](age-of-ashes-bestiary/HI2xA7LCpNPkpV03.htm)|Wailing Crystals|Cristaux gémissants|libre|
|[hPPdfkiRZ1LUpN2h.htm](age-of-ashes-bestiary/hPPdfkiRZ1LUpN2h.htm)|Damurdiel's Vengeance|La vengeance de Damurdielle|officielle|
|[IdpkEJycZgrkvdSn.htm](age-of-ashes-bestiary/IdpkEJycZgrkvdSn.htm)|Phantom Bells|Cloches spectrales|officielle|
|[Ig1joUmSHSNL6QVU.htm](age-of-ashes-bestiary/Ig1joUmSHSNL6QVU.htm)|Ingnovim's Assistant|Assistant d'Ingnovim|officielle|
|[j4BCcTvIMtoUPRzw.htm](age-of-ashes-bestiary/j4BCcTvIMtoUPRzw.htm)|Aluum Enforcer|Exécuteur Aluum|libre|
|[ja9KowUPZjhwhJ1x.htm](age-of-ashes-bestiary/ja9KowUPZjhwhJ1x.htm)|Echoes of Betrayal|Échos de trahison|officielle|
|[JD6kdfZveObBe1mR.htm](age-of-ashes-bestiary/JD6kdfZveObBe1mR.htm)|Xotanispawn|Rejeton de Xotani|officielle|
|[jO2fGZayk4R1AzYK.htm](age-of-ashes-bestiary/jO2fGZayk4R1AzYK.htm)|Bida|Bida|officielle|
|[jZc4PrsX3HCJnkXx.htm](age-of-ashes-bestiary/jZc4PrsX3HCJnkXx.htm)|Emaliza Zandivar|Emaliza Zandivar|libre|
|[kciXaXw31gHA3gZl.htm](age-of-ashes-bestiary/kciXaXw31gHA3gZl.htm)|Jahsi|Jahsi|libre|
|[KEbmwWwmpAmIoBcm.htm](age-of-ashes-bestiary/KEbmwWwmpAmIoBcm.htm)|Mental Scream Trap|Piège hurlement mental|officielle|
|[kLX36WXp6rjTt71z.htm](age-of-ashes-bestiary/kLX36WXp6rjTt71z.htm)|Rinnarv Bontimar|Rinnarv Bontimar|libre|
|[KROWh0CteDFFGsih.htm](age-of-ashes-bestiary/KROWh0CteDFFGsih.htm)|Hellcrown|Souverain infernal|officielle|
|[KzKQYgiUZ3hJo473.htm](age-of-ashes-bestiary/KzKQYgiUZ3hJo473.htm)|Kalavakus|Kalavakus|officielle|
|[L4K4V09tQVXj7ZiI.htm](age-of-ashes-bestiary/L4K4V09tQVXj7ZiI.htm)|Dmiri Yoltosha|Dmiri Yoltosha|officielle|
|[L7IJO5z82nEN9IjM.htm](age-of-ashes-bestiary/L7IJO5z82nEN9IjM.htm)|Lazurite-Infused Stone Golem|Golem de pierre imprégné de lazurite|libre|
|[l7WhZMoYlQC8lELj.htm](age-of-ashes-bestiary/l7WhZMoYlQC8lELj.htm)|Dragonstorm|Tempête draconique|officielle|
|[lcYcBIFmfKFt8Hcs.htm](age-of-ashes-bestiary/lcYcBIFmfKFt8Hcs.htm)|Ingnovim Tluss|Ingnovim Tluss|officielle|
|[lDgabn0WtDKbLtfc.htm](age-of-ashes-bestiary/lDgabn0WtDKbLtfc.htm)|Luminous Ward|Protection lumineuse|officielle|
|[lRqptquQcM6ZcQ4O.htm](age-of-ashes-bestiary/lRqptquQcM6ZcQ4O.htm)|Ishti|Ishti|officielle|
|[Lx5JiVOGWnzzjCrW.htm](age-of-ashes-bestiary/Lx5JiVOGWnzzjCrW.htm)|Scarlet Triad Bruiser|Cogneur de la Triade Écarlate|officielle|
|[MFTMMXiwvW1aqOzs.htm](age-of-ashes-bestiary/MFTMMXiwvW1aqOzs.htm)|Kralgurn|Kralgurn|officielle|
|[MOyYEls8wNGHoe8F.htm](age-of-ashes-bestiary/MOyYEls8wNGHoe8F.htm)|Doorwarden|Gardien des portes|officielle|
|[MWQmOXxGcbaMsXDD.htm](age-of-ashes-bestiary/MWQmOXxGcbaMsXDD.htm)|Hezle|Hezle|libre|
|[mY494hn9sKVD9q8C.htm](age-of-ashes-bestiary/mY494hn9sKVD9q8C.htm)|Jaggaki|Jaggaki|libre|
|[mYJ6NNthl702Pz2s.htm](age-of-ashes-bestiary/mYJ6NNthl702Pz2s.htm)|Scarlet Triad Agent|Agent de la Triade Écarlate|officielle|
|[n6FQeNsDgKaDIF7b.htm](age-of-ashes-bestiary/n6FQeNsDgKaDIF7b.htm)|Spiritbound Aluum|Aluum lié spirituellement|libre|
|[NK49bh04355Lgz5r.htm](age-of-ashes-bestiary/NK49bh04355Lgz5r.htm)|Nketiah|Nketiah|libre|
|[nQ2eBOpK71I8D2JC.htm](age-of-ashes-bestiary/nQ2eBOpK71I8D2JC.htm)|Scarlet Triad Poisoner|Empoisonneur de la Triade Écarlate|officielle|
|[NrMgYvnIHFxeFEhX.htm](age-of-ashes-bestiary/NrMgYvnIHFxeFEhX.htm)|Dalos|Dalos|officielle|
|[NwLrwNqwedh95iry.htm](age-of-ashes-bestiary/NwLrwNqwedh95iry.htm)|Scarlet Triad Thug|Voyou de la Triade Écarlate|officielle|
|[NwNu2yvMnbvPvpY8.htm](age-of-ashes-bestiary/NwNu2yvMnbvPvpY8.htm)|Ekujae Guardian|Gardien Ekujae|libre|
|[oHUIqVPBpHAbN3qO.htm](age-of-ashes-bestiary/oHUIqVPBpHAbN3qO.htm)|Gloomglow Mushrooms|Champignons éclat-de-ténèbres|officielle|
|[OJH8y8LqmgbkN8ce.htm](age-of-ashes-bestiary/OJH8y8LqmgbkN8ce.htm)|Ghastly Bear|Ours blême|officielle|
|[oPXRJxxWrY2kz2qf.htm](age-of-ashes-bestiary/oPXRJxxWrY2kz2qf.htm)|Belmazog|Belmazog|libre|
|[Oqj8XIWBb29NZ8QX.htm](age-of-ashes-bestiary/Oqj8XIWBb29NZ8QX.htm)|Tixitog|Tixitog|officielle|
|[Pmi71NUPAIE3YMws.htm](age-of-ashes-bestiary/Pmi71NUPAIE3YMws.htm)|Vision of Dahak|Vision de Dahak|officielle|
|[qemmmfu7exswGMGZ.htm](age-of-ashes-bestiary/qemmmfu7exswGMGZ.htm)|Kelda Halrig|Kelda Halrig|officielle|
|[QKkvnlqrhgLHuP1t.htm](age-of-ashes-bestiary/QKkvnlqrhgLHuP1t.htm)|Mialari Docur|Mialari Docur|libre|
|[QoS5lEJqji2h490F.htm](age-of-ashes-bestiary/QoS5lEJqji2h490F.htm)|Lifeleech Crystal Patches|Parcelles de cristaux buveurs de vie|officielle|
|[qouYGPM8mE4KUCTe.htm](age-of-ashes-bestiary/qouYGPM8mE4KUCTe.htm)|Rusty Mae|Mae la Rouille|libre|
|[r02b4f1XNq7OihhD.htm](age-of-ashes-bestiary/r02b4f1XNq7OihhD.htm)|Renali|Renali|libre|
|[rd8Scu1YBSyKFwnk.htm](age-of-ashes-bestiary/rd8Scu1YBSyKFwnk.htm)|Emperor Bird|Oiseau impérial|officielle|
|[roRmUbaC9kJC7met.htm](age-of-ashes-bestiary/roRmUbaC9kJC7met.htm)|Endless Elven Aging|Éternel vieillissement des elfes|officielle|
|[scPhFbBqlmD2fQHa.htm](age-of-ashes-bestiary/scPhFbBqlmD2fQHa.htm)|Dragonscarred Dead|Mort dracobalafré|officielle|
|[sqXPqmuUYOgxspV3.htm](age-of-ashes-bestiary/sqXPqmuUYOgxspV3.htm)|Barzillai's Hounds|Molosses de Barzillai|officielle|
|[sQZDwS08l6Agsryq.htm](age-of-ashes-bestiary/sQZDwS08l6Agsryq.htm)|Barushak Il-Varashma|Barushak Il-Varashma|libre|
|[SUQJPKt8N7rxx7Q1.htm](age-of-ashes-bestiary/SUQJPKt8N7rxx7Q1.htm)|Spiked Doorframe|Chambranle à pics|officielle|
|[SVh7cPwyGgmZORVz.htm](age-of-ashes-bestiary/SVh7cPwyGgmZORVz.htm)|Scarlet Triad Boss|Boss de la Triade Écarlate|officielle|
|[SVLUUPOXDINbKyFL.htm](age-of-ashes-bestiary/SVLUUPOXDINbKyFL.htm)|Malarunk|Malarunk|officielle|
|[sY47PB9b7nCJjgRq.htm](age-of-ashes-bestiary/sY47PB9b7nCJjgRq.htm)|Manifestation Of Dahak|Manifestation de Dahak|libre|
|[T3UVfAfcvAMe9rih.htm](age-of-ashes-bestiary/T3UVfAfcvAMe9rih.htm)|Charau-ka Dragon Priest|Prêtre dragon Charau-ka|libre|
|[teabm1YiRAKdUaEQ.htm](age-of-ashes-bestiary/teabm1YiRAKdUaEQ.htm)|Seismic Spears Trap|Piège de lances sismiques|officielle|
|[tGyPrTGSpndXKU88.htm](age-of-ashes-bestiary/tGyPrTGSpndXKU88.htm)|Nolly Peltry|Nolly Peltry|officielle|
|[tr3qlFJVHqloE9zI.htm](age-of-ashes-bestiary/tr3qlFJVHqloE9zI.htm)|Forge-Spurned|Rejet de la forge|officielle|
|[U2QGjhcg5QFFkHwv.htm](age-of-ashes-bestiary/U2QGjhcg5QFFkHwv.htm)|Saggorak Poltergeist|Poltergeists de Saggorak|officielle|
|[u9QIJIpkSgW5VRIG.htm](age-of-ashes-bestiary/u9QIJIpkSgW5VRIG.htm)|Town Hall Fire|Incendie de l'hôtel de ville|officielle|
|[UD7EwTQG2Sbl4d8R.htm](age-of-ashes-bestiary/UD7EwTQG2Sbl4d8R.htm)|Voz Lirayne|Voz Lirayne|libre|
|[UQDxkvqXKg03ESTQ.htm](age-of-ashes-bestiary/UQDxkvqXKg03ESTQ.htm)|Gerhard Pendergrast|Gerhard Pendergrast|libre|
|[VQiBBu6D2srK6Fmj.htm](age-of-ashes-bestiary/VQiBBu6D2srK6Fmj.htm)|Wrath of the Destroyer|Le Courroux du Destructeur|officielle|
|[w1Pqv0YmG4MevpQc.htm](age-of-ashes-bestiary/w1Pqv0YmG4MevpQc.htm)|Corrupt Guard|Garde corrompu|officielle|
|[We1Nq7CrqDQHEZxY.htm](age-of-ashes-bestiary/We1Nq7CrqDQHEZxY.htm)|Quarry Sluiceway|Les écluses de la carrière|officielle|
|[WouSA1UsIzvBTOix.htm](age-of-ashes-bestiary/WouSA1UsIzvBTOix.htm)|Carnivorous Crystal|Cristaux carnivores|officielle|
|[X2edjYEqAHsdQc6L.htm](age-of-ashes-bestiary/X2edjYEqAHsdQc6L.htm)|Grippli Greenspeaker|Orateur végétal Grippli|libre|
|[X4oeJnv1KcpHFO6i.htm](age-of-ashes-bestiary/X4oeJnv1KcpHFO6i.htm)|Xotani, The Firebleeder|Xotani, le Saignefeu|officielle|
|[X6TTBlHIfJZ43OqR.htm](age-of-ashes-bestiary/X6TTBlHIfJZ43OqR.htm)|Dragonshard Guardian|Gardien d'éclat Draconique|officielle|
|[X7zSx8LFh2ZDnOYy.htm](age-of-ashes-bestiary/X7zSx8LFh2ZDnOYy.htm)|Teyam Ishtori|Téyam Ishtori|libre|
|[XHJC4G6bfPiVXGKE.htm](age-of-ashes-bestiary/XHJC4G6bfPiVXGKE.htm)|Ilssrah Embermead|Ilssrah Ambromel|libre|
|[XNso2IMnhnfHcMCn.htm](age-of-ashes-bestiary/XNso2IMnhnfHcMCn.htm)|Zuferian|Zuferian|officielle|
|[xrxjjjRMKzwsYbGm.htm](age-of-ashes-bestiary/xrxjjjRMKzwsYbGm.htm)|Accursed Forge-Spurned|Rejet de la forge maudit|officielle|
|[xXtGS9uBa9z43X7y.htm](age-of-ashes-bestiary/xXtGS9uBa9z43X7y.htm)|Graveshell|Carapace funeste|officielle|
|[YdLf7y2ZHudpjWx9.htm](age-of-ashes-bestiary/YdLf7y2ZHudpjWx9.htm)|Living Sap|Sève vivante|officielle|
|[YIXAcvSyI1C94r9l.htm](age-of-ashes-bestiary/YIXAcvSyI1C94r9l.htm)|Zephyr Guard|Garde zéphyr|officielle|
|[yLhO5vUrPQF42Lh8.htm](age-of-ashes-bestiary/yLhO5vUrPQF42Lh8.htm)|Charau-ka|Charau-ka|officielle|
|[yrU0xi4eKBmcGudo.htm](age-of-ashes-bestiary/yrU0xi4eKBmcGudo.htm)|Animated Dragonstorm|Tempête draconique animée|officielle|
|[YV3wA2Kgjp74l7YJ.htm](age-of-ashes-bestiary/YV3wA2Kgjp74l7YJ.htm)|Alak Stagram|Alak Stagram|officielle|
|[z3zWW4jLADAei1uo.htm](age-of-ashes-bestiary/z3zWW4jLADAei1uo.htm)|Ash Web|Toile cendrée|officielle|
|[z4rlpEsE2KgzpOCc.htm](age-of-ashes-bestiary/z4rlpEsE2KgzpOCc.htm)|Remnant of Barzillai|Vestige de Barzillai|officielle|
|[zdJHl3xMY7n2Lwlf.htm](age-of-ashes-bestiary/zdJHl3xMY7n2Lwlf.htm)|Trapped Lathe|Tour piégé|officielle|
|[zNIjGSxkG8xyDLgR.htm](age-of-ashes-bestiary/zNIjGSxkG8xyDLgR.htm)|Dragon Pillar|Colonne draconique|officielle|
|[ZqL6MP4XxDys9dmW.htm](age-of-ashes-bestiary/ZqL6MP4XxDys9dmW.htm)|Aiudara Wraith|Âme-en-peine des aiudaras|officielle|
|[ZRGRAqkDEZbRn8x3.htm](age-of-ashes-bestiary/ZRGRAqkDEZbRn8x3.htm)|Crucidaemon|Crucidaémon|libre|
|[ZtSb3mHZ5sD2uqHd.htm](age-of-ashes-bestiary/ZtSb3mHZ5sD2uqHd.htm)|Mud Spider|Araignée de boue|officielle|
