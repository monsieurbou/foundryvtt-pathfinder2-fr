# État de la traduction (blood-lords-bestiary)

 * **libre**: 191
 * **changé**: 4


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[mqHULEf9atfBObra.htm](blood-lords-bestiary/mqHULEf9atfBObra.htm)|Dead Faine|Fainemort|changé|
|[mZUwni8Hhv27vj4s.htm](blood-lords-bestiary/mZUwni8Hhv27vj4s.htm)|Glyph of Warding (B8)|Glyphe de garde (B8)|changé|
|[t92NDPmfXnOke88G.htm](blood-lords-bestiary/t92NDPmfXnOke88G.htm)|Pesgahi the Poisoner|Pesgahi l'empoisonneur|changé|
|[UT5B2LCirpTgaEEx.htm](blood-lords-bestiary/UT5B2LCirpTgaEEx.htm)|Shadowforged Guardian|Gardien forgeombre|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[07DkxPmw2du1xu16.htm](blood-lords-bestiary/07DkxPmw2du1xu16.htm)|Mental Assault|Agression mentale|libre|
|[0a2d830uaGnBN2OS.htm](blood-lords-bestiary/0a2d830uaGnBN2OS.htm)|Ghoul Razorclaw|Goule Grifferasoir|libre|
|[0iZHpYIbxquRDd8E.htm](blood-lords-bestiary/0iZHpYIbxquRDd8E.htm)|Pyrogeist|Pyrogeist|libre|
|[0NtGFCLjz0lPPp4o.htm](blood-lords-bestiary/0NtGFCLjz0lPPp4o.htm)|Night's Breath Trap|Piège du souffle de la nuit|libre|
|[0PJ75DaIe8x5ynp1.htm](blood-lords-bestiary/0PJ75DaIe8x5ynp1.htm)|Ghoul Antipaladin|Goule anti-paladin|libre|
|[0vCny4bIzIKehi5u.htm](blood-lords-bestiary/0vCny4bIzIKehi5u.htm)|Arboreal Snag (Axan Wood)|Arboréen mort-vivant (Forêt d'Axan)|libre|
|[1EOyUGMELVUIxFeB.htm](blood-lords-bestiary/1EOyUGMELVUIxFeB.htm)|Rotbomber|Bombardier purulent|libre|
|[1Hc1BLc9LIXLuBdg.htm](blood-lords-bestiary/1Hc1BLc9LIXLuBdg.htm)|Rival Corpsekiller|Tueur de cadavre rival|libre|
|[1igzK6SDdQkv8NXb.htm](blood-lords-bestiary/1igzK6SDdQkv8NXb.htm)|Matron Uldrula|Matronne Uldrula|libre|
|[1KDrm9ErOHyMsDUP.htm](blood-lords-bestiary/1KDrm9ErOHyMsDUP.htm)|Straugh|Straugh|libre|
|[1McAtOIMRIVOa0lf.htm](blood-lords-bestiary/1McAtOIMRIVOa0lf.htm)|Rival Necromancer|Nécromancien rival|libre|
|[1YdRAByWMiyUIXwt.htm](blood-lords-bestiary/1YdRAByWMiyUIXwt.htm)|Princess Kerinza|Princesse Kérinza|libre|
|[2cSy2hsHLS0mYLXs.htm](blood-lords-bestiary/2cSy2hsHLS0mYLXs.htm)|Crystallized Agony|Agonie cristallisée|libre|
|[2DhycjxPVPIAftM3.htm](blood-lords-bestiary/2DhycjxPVPIAftM3.htm)|Terrorguard|Garde-terreur|libre|
|[2p9BaMPkI0HiTWyC.htm](blood-lords-bestiary/2p9BaMPkI0HiTWyC.htm)|Sulvik|Sulvik|libre|
|[2uBSQl6CHkbAdzWo.htm](blood-lords-bestiary/2uBSQl6CHkbAdzWo.htm)|Hungry Cottage|Chaumière affamée|libre|
|[2ZpYVlsDigX5ZQR6.htm](blood-lords-bestiary/2ZpYVlsDigX5ZQR6.htm)|Mummy Valet|Momie valet|libre|
|[3t57lJmLz6qQPqoD.htm](blood-lords-bestiary/3t57lJmLz6qQPqoD.htm)|Hyrune Loxenna|Hyrune Loxenna|libre|
|[3Yi3PBesesksFZi6.htm](blood-lords-bestiary/3Yi3PBesesksFZi6.htm)|Intellect Assemblage|Amalgame d'intelligence|libre|
|[49cAFgzk6Zok82Y2.htm](blood-lords-bestiary/49cAFgzk6Zok82Y2.htm)|Drowned Mummy|Momie noyée|libre|
|[4yAbOWwebJGWRmt2.htm](blood-lords-bestiary/4yAbOWwebJGWRmt2.htm)|Ghostly Mob|Foule fantomatique|libre|
|[4Z704FJ69DWl71vP.htm](blood-lords-bestiary/4Z704FJ69DWl71vP.htm)|Creeping Crone|Sorcière insidieuse|libre|
|[5fTgvzhP4maX4tcs.htm](blood-lords-bestiary/5fTgvzhP4maX4tcs.htm)|Precarious Bone Pile|Tas d'os instable|libre|
|[5HgUBmFaEWVZhx2g.htm](blood-lords-bestiary/5HgUBmFaEWVZhx2g.htm)|Choking Tethers|Pilier étrangleur|libre|
|[5pfU6ZAyAWq3VEdq.htm](blood-lords-bestiary/5pfU6ZAyAWq3VEdq.htm)|Kepgeda the Hag-Nailed|Kepgéda la guenaude clouée|libre|
|[5qm8l3dAnnFA1EQQ.htm](blood-lords-bestiary/5qm8l3dAnnFA1EQQ.htm)|Pain Milker|Distillateur de douleur|libre|
|[614jXwpPEVRulHYD.htm](blood-lords-bestiary/614jXwpPEVRulHYD.htm)|Demilich Skull|Crâne de demi-liche|libre|
|[624fuRv6nLi31THS.htm](blood-lords-bestiary/624fuRv6nLi31THS.htm)|Prachalla|Prachalla|libre|
|[6DRyViLYSsw2sYBy.htm](blood-lords-bestiary/6DRyViLYSsw2sYBy.htm)|Seldeg's Steed|Monture de Seldeg|libre|
|[6I4ZtQJPT0ToITvq.htm](blood-lords-bestiary/6I4ZtQJPT0ToITvq.htm)|Floating Femur|Fémur flottant|libre|
|[7Ah4NAOuhPqMQ5e7.htm](blood-lords-bestiary/7Ah4NAOuhPqMQ5e7.htm)|Ghiasi's Double|Double de Ghiasi|libre|
|[7BqiEDwmEPDwGMAn.htm](blood-lords-bestiary/7BqiEDwmEPDwGMAn.htm)|Harmony In Agony|Harmony en Agonie|libre|
|[7In0QgKUCCNcrTEA.htm](blood-lords-bestiary/7In0QgKUCCNcrTEA.htm)|Shoki|Shoki|libre|
|[7tmoC1LEp3taxaRp.htm](blood-lords-bestiary/7tmoC1LEp3taxaRp.htm)|Eseneth|Éseneth|libre|
|[8I2hoMYo2hXVYZZ0.htm](blood-lords-bestiary/8I2hoMYo2hXVYZZ0.htm)|Nydazuul|Nydazuul|libre|
|[8k2a2J7gSqV877sZ.htm](blood-lords-bestiary/8k2a2J7gSqV877sZ.htm)|Cleansing Fire|Feu purificateur|libre|
|[9cYqx6JeqTC6UDIJ.htm](blood-lords-bestiary/9cYqx6JeqTC6UDIJ.htm)|Button Mash|Écraser les boutons|libre|
|[9yd6jVPcLusk4BX1.htm](blood-lords-bestiary/9yd6jVPcLusk4BX1.htm)|Ancient Skaveling|Skaveling ancien|libre|
|[A3joovUPMBOVDpVz.htm](blood-lords-bestiary/A3joovUPMBOVDpVz.htm)|Facetbound Nullifier|Invalideur à facettes|libre|
|[A8jGRgn97x59cJn8.htm](blood-lords-bestiary/A8jGRgn97x59cJn8.htm)|Nwanyian Defender|Défenseur Nwanyien|libre|
|[a8PBT5CIg5yM96qh.htm](blood-lords-bestiary/a8PBT5CIg5yM96qh.htm)|Avian Rage|Rage aviaire|libre|
|[AvF72rXp6bcQHPdo.htm](blood-lords-bestiary/AvF72rXp6bcQHPdo.htm)|Facetbound Cascader|Équilibriste facetté|libre|
|[AyUIaZ5TiRkyl3NL.htm](blood-lords-bestiary/AyUIaZ5TiRkyl3NL.htm)|Crush the Uninvited|Écraser les importuns|libre|
|[aZMbCNaUnmqIVoyA.htm](blood-lords-bestiary/aZMbCNaUnmqIVoyA.htm)|Granite Vulture|Vautour de granit|libre|
|[b8IAeHndW8YVEzDM.htm](blood-lords-bestiary/b8IAeHndW8YVEzDM.htm)|Aeolaeka|Aéolaeka|libre|
|[bblPYqxgXeRmR4Bi.htm](blood-lords-bestiary/bblPYqxgXeRmR4Bi.htm)|Ghiono|Ghiono|libre|
|[BHuDRFyr8FbNHjVk.htm](blood-lords-bestiary/BHuDRFyr8FbNHjVk.htm)|Tylegmut's Last Meal|Le dernier repas de Tylegmut|libre|
|[BiJxQ9mMOGxS6XnA.htm](blood-lords-bestiary/BiJxQ9mMOGxS6XnA.htm)|Necrohulk Flailer|Nécromutant saccageur|libre|
|[bqyNCz1RU5M3S2S3.htm](blood-lords-bestiary/bqyNCz1RU5M3S2S3.htm)|The Flight|La fuite|libre|
|[btuLRbWKuZK7ozti.htm](blood-lords-bestiary/btuLRbWKuZK7ozti.htm)|Arghun the Annihilator|Arghun l'exterminateur|libre|
|[bZPLaHBF66XuRQlv.htm](blood-lords-bestiary/bZPLaHBF66XuRQlv.htm)|Lasheeli|Lasheeli|libre|
|[c8OrFlroChWV6ad4.htm](blood-lords-bestiary/c8OrFlroChWV6ad4.htm)|Mechanical Laborer|Laboureur mécanique|libre|
|[CMQu4nHDLH7tt6yu.htm](blood-lords-bestiary/CMQu4nHDLH7tt6yu.htm)|Ghoul Gnawer|Goule-rongeur|libre|
|[Cq2Dy62tDzOApDrE.htm](blood-lords-bestiary/Cq2Dy62tDzOApDrE.htm)|Graelar the Whisper|Graélar le Murmure|libre|
|[cUUyIZ3Y8VAbfI9P.htm](blood-lords-bestiary/cUUyIZ3Y8VAbfI9P.htm)|Kerinza|Kérinza|libre|
|[CVBM8oY76Yw4IxN5.htm](blood-lords-bestiary/CVBM8oY76Yw4IxN5.htm)|Scrabbling Ribcage|Cage thoracique crapahutante|libre|
|[CWg8NX6XHv2sEVlg.htm](blood-lords-bestiary/CWg8NX6XHv2sEVlg.htm)|Weeping Jack|Jack pleureur|libre|
|[dEUFmId6fuT97MFg.htm](blood-lords-bestiary/dEUFmId6fuT97MFg.htm)|Solar Glass Golem|Golem de verre solaire|libre|
|[DIDEV6UK5S5jQunK.htm](blood-lords-bestiary/DIDEV6UK5S5jQunK.htm)|Animated Fireplace|Cheminée animée|libre|
|[DInkix6cy2NgjpJd.htm](blood-lords-bestiary/DInkix6cy2NgjpJd.htm)|Shabti Slayer|Tueur shabti|libre|
|[dmUBlUsO6AoxjgxL.htm](blood-lords-bestiary/dmUBlUsO6AoxjgxL.htm)|Firebrand Bastion|Agitateur bastion|libre|
|[dUl7C2wT4mT3mAGH.htm](blood-lords-bestiary/dUl7C2wT4mT3mAGH.htm)|Ruby|Ruby|libre|
|[DunLW2kBqOpE0pkL.htm](blood-lords-bestiary/DunLW2kBqOpE0pkL.htm)|Decrosia|Décrosia|libre|
|[duqhXs5SXmvSC1uI.htm](blood-lords-bestiary/duqhXs5SXmvSC1uI.htm)|Hollow Husk|Vestige de dépouille|libre|
|[e5iMWebwtUQrOtPB.htm](blood-lords-bestiary/e5iMWebwtUQrOtPB.htm)|Yulthruk|Yulthruk|libre|
|[ej9sP9qKkeUiIMQT.htm](blood-lords-bestiary/ej9sP9qKkeUiIMQT.htm)|Ostovite Nest|Nid d'Ostovites|libre|
|[epfj3ZAwLHMlYlF3.htm](blood-lords-bestiary/epfj3ZAwLHMlYlF3.htm)|Virulak Necromancer|Nécromancien virulak|libre|
|[EqO67DHLlB88vSJZ.htm](blood-lords-bestiary/EqO67DHLlB88vSJZ.htm)|Theater Phantasm|Fantôme de théâtre|libre|
|[eReDW3oTDV8ehPlL.htm](blood-lords-bestiary/eReDW3oTDV8ehPlL.htm)|Slamming Gate|Porte à ressorts|libre|
|[ereMELTgIov45VFO.htm](blood-lords-bestiary/ereMELTgIov45VFO.htm)|Caustic Shower|Douche caustique|libre|
|[Eu7KtTDs6ia5ijUK.htm](blood-lords-bestiary/Eu7KtTDs6ia5ijUK.htm)|Shadow Worm|Ver ombral|libre|
|[EUr4hJvZf5lRQSKu.htm](blood-lords-bestiary/EUr4hJvZf5lRQSKu.htm)|Tenebric Giant|Géant ténébreux|libre|
|[fekYhQe1qrvwarBL.htm](blood-lords-bestiary/fekYhQe1qrvwarBL.htm)|Benefactor's End|Fin du bienfaiteur|libre|
|[FiY8hCD5XoZjp9yX.htm](blood-lords-bestiary/FiY8hCD5XoZjp9yX.htm)|Aquatic Ooze|Vase aquatique|libre|
|[fOklsDO2EB5CHuLF.htm](blood-lords-bestiary/fOklsDO2EB5CHuLF.htm)|Bulette|Bulette|libre|
|[FVBJjBbvITVpHiec.htm](blood-lords-bestiary/FVBJjBbvITVpHiec.htm)|Prince Doriel|Prince Doriel|libre|
|[FzEwUkw6SCgQ1YJg.htm](blood-lords-bestiary/FzEwUkw6SCgQ1YJg.htm)|Wheel Archon|Archon-roue|libre|
|[geyq1PRVsbVAFuCx.htm](blood-lords-bestiary/geyq1PRVsbVAFuCx.htm)|Keystone Repulsion|Répulsion des clés de base|libre|
|[glq0AJMjNAB0R2Ke.htm](blood-lords-bestiary/glq0AJMjNAB0R2Ke.htm)|Terra-cotta Soldier|Soldat de terre cuite|libre|
|[gnnyh4HUnmfWEMKl.htm](blood-lords-bestiary/gnnyh4HUnmfWEMKl.htm)|Shadow Lash|Lanière d'ombre|libre|
|[GVIv3WxwbwztNmvD.htm](blood-lords-bestiary/GVIv3WxwbwztNmvD.htm)|Teaching Assistant|Aide-enseignant|libre|
|[hHUbjTLoYfd1XvV1.htm](blood-lords-bestiary/hHUbjTLoYfd1XvV1.htm)|Urgathoa's Ire|Courroux d'Urgathoa|libre|
|[HKH4GURuq59l0ag4.htm](blood-lords-bestiary/HKH4GURuq59l0ag4.htm)|Zombie Rival Necromancer|Nécromancien zombie rival|libre|
|[HNcBJFUhdLfOZxPS.htm](blood-lords-bestiary/HNcBJFUhdLfOZxPS.htm)|Ghiasi|Ghiasi|libre|
|[hQwP2F2CZfH1ZA0N.htm](blood-lords-bestiary/hQwP2F2CZfH1ZA0N.htm)|Sahni Bride-Of-The-Sea|Sahni Épouse-de-l'Océan|libre|
|[HQwuxqW1cTS0PrDM.htm](blood-lords-bestiary/HQwuxqW1cTS0PrDM.htm)|Spiraling Comets|Comètes flottantes|libre|
|[i722khDhe3zTB881.htm](blood-lords-bestiary/i722khDhe3zTB881.htm)|Wrathful Dinosaur|Dinosaure vengeur|libre|
|[I83vD5fNYIC1s3Xg.htm](blood-lords-bestiary/I83vD5fNYIC1s3Xg.htm)|Time Rift|Rupture du temps|libre|
|[I9Nn9RsYdovSFiD6.htm](blood-lords-bestiary/I9Nn9RsYdovSFiD6.htm)|Skull Fairy|Fée-crâne|libre|
|[IDfaWPdUjUmjiDvj.htm](blood-lords-bestiary/IDfaWPdUjUmjiDvj.htm)|Phantom Soldiers|Soldats fantômes|libre|
|[IeAy6hvDcnU1pCZr.htm](blood-lords-bestiary/IeAy6hvDcnU1pCZr.htm)|Guloval|Guloval|libre|
|[iVzVgK6igRZ8hkCu.htm](blood-lords-bestiary/iVzVgK6igRZ8hkCu.htm)|Animated Tea Cart|Chariot à thé animé|libre|
|[IXPZR1DTdT7Tu7UG.htm](blood-lords-bestiary/IXPZR1DTdT7Tu7UG.htm)|Bloodshroud|Linceul sanguin|libre|
|[JFB7wDTyG8nbihJD.htm](blood-lords-bestiary/JFB7wDTyG8nbihJD.htm)|Necrohulk Smasher|Nécromutant frappeur|libre|
|[JiT7B2Y7yYG8G0ZL.htm](blood-lords-bestiary/JiT7B2Y7yYG8G0ZL.htm)|Nwanyian Archer|Archer Nwanyien|libre|
|[k8NnItW7Hp79bg26.htm](blood-lords-bestiary/k8NnItW7Hp79bg26.htm)|Cobblebone Swarm|Nuée d'ossebriques|libre|
|[Kg7E5rophKEmIqDz.htm](blood-lords-bestiary/Kg7E5rophKEmIqDz.htm)|Sickle Blade|Lame de serpe|libre|
|[KJm77VlSLPNpukcI.htm](blood-lords-bestiary/KJm77VlSLPNpukcI.htm)|Vice-Chancellor Vikroti Stroh|Vice-chancelier Vikroti Stroh|libre|
|[Kq2dxvP4wqJJVzBr.htm](blood-lords-bestiary/Kq2dxvP4wqJJVzBr.htm)|Meat Guardian|Gardien charognard|libre|
|[KQkouk6tku8akpmU.htm](blood-lords-bestiary/KQkouk6tku8akpmU.htm)|Necromunculus|Nécromunculus|libre|
|[ku6gSDVfrxBKwAC3.htm](blood-lords-bestiary/ku6gSDVfrxBKwAC3.htm)|Nightmare Portal|Portail du cauchemar|libre|
|[L30BKaq55auUhcPY.htm](blood-lords-bestiary/L30BKaq55auUhcPY.htm)|Shimmernewt|Triton miroitant|libre|
|[l5jzqFVpGEg6Mj3e.htm](blood-lords-bestiary/l5jzqFVpGEg6Mj3e.htm)|Skeletal Knight|Squelette chevalier|libre|
|[LeJeFOI8P0L7pxxS.htm](blood-lords-bestiary/LeJeFOI8P0L7pxxS.htm)|Zombie Hound|Chien zombie|libre|
|[lfQ4w6hWk4v2ewYR.htm](blood-lords-bestiary/lfQ4w6hWk4v2ewYR.htm)|The Putrid Rise|Remontée putride|libre|
|[Lk0Ekva2RstJ0PNm.htm](blood-lords-bestiary/Lk0Ekva2RstJ0PNm.htm)|Soul Stasis|Immobilisme de l'âme|libre|
|[lmOabS84lQC3gJx4.htm](blood-lords-bestiary/lmOabS84lQC3gJx4.htm)|Collapsing Bridge|Effondrement du pont en fer|libre|
|[lq8H7BBc5C2y9BN0.htm](blood-lords-bestiary/lq8H7BBc5C2y9BN0.htm)|Psychic Wave|Vague psychique|libre|
|[LRHYWNJg7231HUOc.htm](blood-lords-bestiary/LRHYWNJg7231HUOc.htm)|Dirge Piper|Symphoniste funeste|libre|
|[lycxuueclDmiIAOF.htm](blood-lords-bestiary/lycxuueclDmiIAOF.htm)|Unstable Fiendflame Cage|Cage flamme-fiélon instable|libre|
|[mfBLoIaSYNbmmtht.htm](blood-lords-bestiary/mfBLoIaSYNbmmtht.htm)|Drusilla|Drusilla|libre|
|[MKYtgU2G42jk2Dwo.htm](blood-lords-bestiary/MKYtgU2G42jk2Dwo.htm)|Restored Doll|Poupée restaurée|libre|
|[N1VAU3XomX53b6HL.htm](blood-lords-bestiary/N1VAU3XomX53b6HL.htm)|Rust Hag|Guenaude de rouille|libre|
|[N387myolfXJxWYvF.htm](blood-lords-bestiary/N387myolfXJxWYvF.htm)|Bone Shard Tough|Voyou Éclat d'os|libre|
|[ngWoxzjuoZAiQHSj.htm](blood-lords-bestiary/ngWoxzjuoZAiQHSj.htm)|Necromancer Troop|Troupe de nécromancien|libre|
|[NiDaVEpAOgbuwnau.htm](blood-lords-bestiary/NiDaVEpAOgbuwnau.htm)|Kemnebi's Puppet|Marionnette de Kemnébi|libre|
|[nmv3qAignhfl1in2.htm](blood-lords-bestiary/nmv3qAignhfl1in2.htm)|Mosghuta, Boss Cow|Mosghuta, la vache Boss|libre|
|[noCFIy7gHIaaQEdJ.htm](blood-lords-bestiary/noCFIy7gHIaaQEdJ.htm)|Yurgak|Yurgak|libre|
|[NogvW222v6TGMz5w.htm](blood-lords-bestiary/NogvW222v6TGMz5w.htm)|Glyph of Warding (D5)|Glyphe de garde (D5)|libre|
|[NoOG4QIbIEBWKk1I.htm](blood-lords-bestiary/NoOG4QIbIEBWKk1I.htm)|Soul Slime|Vase d'âme|libre|
|[nv41Nzzaavh1tv3w.htm](blood-lords-bestiary/nv41Nzzaavh1tv3w.htm)|Ashes of Despair|Cendres de désespoir|libre|
|[nX7wdbO4lvdGlFPM.htm](blood-lords-bestiary/nX7wdbO4lvdGlFPM.htm)|Phalanx of Phalanges|Nuée de phalanges|libre|
|[nzY0BPNKt4NF1Nhq.htm](blood-lords-bestiary/nzY0BPNKt4NF1Nhq.htm)|Bellator Mortus Soldier|Soldat du Bellator Mortus|libre|
|[O0jtf0qQnuSTrNUp.htm](blood-lords-bestiary/O0jtf0qQnuSTrNUp.htm)|Grave Hag|Guenaude des tombes|libre|
|[O4revJ2qwWVmSTwN.htm](blood-lords-bestiary/O4revJ2qwWVmSTwN.htm)|Cleansing Hall|Corridor purificateur|libre|
|[OgaEYZJaxGXPnhcz.htm](blood-lords-bestiary/OgaEYZJaxGXPnhcz.htm)|Afziaka Stalker|Harceleur Afziaka|libre|
|[OhJ4yePPE1zQ8JNL.htm](blood-lords-bestiary/OhJ4yePPE1zQ8JNL.htm)|Sahreg the Dirge Screamer|Crieur funèbre Sahreg|libre|
|[oQzwF5Wwn2w8POgS.htm](blood-lords-bestiary/oQzwF5Wwn2w8POgS.htm)|Vanth Warrior|Combattant Vanth|libre|
|[OuiTvUpfZVNlIIQV.htm](blood-lords-bestiary/OuiTvUpfZVNlIIQV.htm)|Azmakian Effigy|Effigie azmakienne|libre|
|[oVqkX1LugEW4qoXN.htm](blood-lords-bestiary/oVqkX1LugEW4qoXN.htm)|Kelganth|Kelganth|libre|
|[p6wud30R7C5OAFGz.htm](blood-lords-bestiary/p6wud30R7C5OAFGz.htm)|Ghast Outlaw|Blême hors-la-loi|libre|
|[PCGYst9QySIxsjDK.htm](blood-lords-bestiary/PCGYst9QySIxsjDK.htm)|Chattering Jaws|Mâchoires claquantes|libre|
|[PFAxHAnorkDmvi26.htm](blood-lords-bestiary/PFAxHAnorkDmvi26.htm)|Clockwork Rifler|Carabinier mécanique|libre|
|[pg4YAN1whVZsnLKf.htm](blood-lords-bestiary/pg4YAN1whVZsnLKf.htm)|Grace "The Rhino" Owano|Grâce "La rhino" Owano|libre|
|[pHiRAXi3YxFhBeLi.htm](blood-lords-bestiary/pHiRAXi3YxFhBeLi.htm)|Yshula|Yshula|libre|
|[picyq0Q5kjJeAfH8.htm](blood-lords-bestiary/picyq0Q5kjJeAfH8.htm)|Perilous Flash Flood|Inondation périlleuse|libre|
|[PJ9O7J5MWhxvYnPW.htm](blood-lords-bestiary/PJ9O7J5MWhxvYnPW.htm)|Virulak Villager|Villageois Virulak|libre|
|[pJ9Pfo3DCsV97GYO.htm](blood-lords-bestiary/pJ9Pfo3DCsV97GYO.htm)|Crooked Coffin Brewer|Brasseur du Cercueil tordu|libre|
|[ppFL3vBBYT4EXQom.htm](blood-lords-bestiary/ppFL3vBBYT4EXQom.htm)|Shadow Leydroth|Leydroth des ombres|libre|
|[pQJbY3PTeHNYbkho.htm](blood-lords-bestiary/pQJbY3PTeHNYbkho.htm)|Zuntishan Guard|Garde Zuntishan|libre|
|[pqlqDWVOtPxJ2oRn.htm](blood-lords-bestiary/pqlqDWVOtPxJ2oRn.htm)|Urbulinex|Urbulinex|libre|
|[PTAKDBMDrm1JJTYC.htm](blood-lords-bestiary/PTAKDBMDrm1JJTYC.htm)|Mithral Golem|Golem d'aubargent|libre|
|[ptVkyQSFmsX5PACJ.htm](blood-lords-bestiary/ptVkyQSFmsX5PACJ.htm)|Unfinished Portalrender|Sourceportail inachevé|libre|
|[puzF8NkxRVv3Lrz8.htm](blood-lords-bestiary/puzF8NkxRVv3Lrz8.htm)|Skewering Hall|Couloir empaleur|libre|
|[Q5HbADjzw2s2biXq.htm](blood-lords-bestiary/Q5HbADjzw2s2biXq.htm)|The Fight|Le combat|libre|
|[Q7Mfkogemi5D9LJh.htm](blood-lords-bestiary/Q7Mfkogemi5D9LJh.htm)|Hungering Growth|Tumeur affamée|libre|
|[QKFdks4RCxWxMw2K.htm](blood-lords-bestiary/QKFdks4RCxWxMw2K.htm)|Shadow Heart|Coeur ombreux|libre|
|[QuuCHbVDpBfyu5JB.htm](blood-lords-bestiary/QuuCHbVDpBfyu5JB.htm)|Anguished Sarenite|Sarénite tourmenté|libre|
|[R7NpmPUqPkMtsUIl.htm](blood-lords-bestiary/R7NpmPUqPkMtsUIl.htm)|Hall of Mirrors|Galerie des miroirs|libre|
|[rAKazZKUhWdwPQBx.htm](blood-lords-bestiary/rAKazZKUhWdwPQBx.htm)|Basiri, Wellspring Keeper|Basiri, gardienne de la source|libre|
|[rAKxH1Jbpx6DSlMi.htm](blood-lords-bestiary/rAKxH1Jbpx6DSlMi.htm)|Soul-Drinking Spirit Collectors|Esprit capteur draineur d'âme|libre|
|[RC1QKxrLyEZRM2EW.htm](blood-lords-bestiary/RC1QKxrLyEZRM2EW.htm)|Revisit Past Pains|Moments pénibles du passé|libre|
|[RIKDtmwVQQeJUZKg.htm](blood-lords-bestiary/RIKDtmwVQQeJUZKg.htm)|Vampire Taviah|Taviah la vampire|libre|
|[RIUiN0518gxNIsKD.htm](blood-lords-bestiary/RIUiN0518gxNIsKD.htm)|Shrieking Souls|Âme terrifiée|libre|
|[rJOlv5hfotegHwx2.htm](blood-lords-bestiary/rJOlv5hfotegHwx2.htm)|Kepgeda's Keening Cauldron|Le chaudron gémissant de Kepgéda|libre|
|[RpFpcfMqD6wqIHfE.htm](blood-lords-bestiary/RpFpcfMqD6wqIHfE.htm)|Vampire Rival Necromancer|Nécromancien vampire rival|libre|
|[s16EyzsD3GLLY43o.htm](blood-lords-bestiary/s16EyzsD3GLLY43o.htm)|Summoning Rune (Cockatrice)|Rune de convocation (Cockatrice)|libre|
|[S1rfj73uV1ZUBCJ3.htm](blood-lords-bestiary/S1rfj73uV1ZUBCJ3.htm)|Skeleton Rival Corpsekiller|Tueur de cadavre squelette rival|libre|
|[sJRDLWLc7F6ZCc31.htm](blood-lords-bestiary/sJRDLWLc7F6ZCc31.htm)|Sorvinaesen|Sorvinaësen|libre|
|[SSFzb5JBZDG6sr9B.htm](blood-lords-bestiary/SSFzb5JBZDG6sr9B.htm)|Opkherab|Opkhérab|libre|
|[SUTCBq10R5kEdvon.htm](blood-lords-bestiary/SUTCBq10R5kEdvon.htm)|Zombie Chuul|Chuul zombie|libre|
|[syFR3SChA5KCx8OA.htm](blood-lords-bestiary/syFR3SChA5KCx8OA.htm)|Vampire Guardian|Gardien vampire|libre|
|[sySkOlAi825RpRrj.htm](blood-lords-bestiary/sySkOlAi825RpRrj.htm)|Sallowdrudge|Rivenoyé|libre|
|[tu9Y0LJ6ghOpkJTX.htm](blood-lords-bestiary/tu9Y0LJ6ghOpkJTX.htm)|Seldeg Bheldis|Seldeg Bheldis|libre|
|[TV3BSvxoG8p1fe0k.htm](blood-lords-bestiary/TV3BSvxoG8p1fe0k.htm)|Urglid|Urglid|libre|
|[UsvCc0wceUBOwbdl.htm](blood-lords-bestiary/UsvCc0wceUBOwbdl.htm)|Bursting Bone Vault|Chambre forte d'os|libre|
|[UtMyUvcmy22QTuxK.htm](blood-lords-bestiary/UtMyUvcmy22QTuxK.htm)|Shadow Hooks|Crochet d'ombres|libre|
|[vf8FZOnFx71tnhZ2.htm](blood-lords-bestiary/vf8FZOnFx71tnhZ2.htm)|Castigate the Faithless|Punir les infidèles|libre|
|[vUv8SR7Pa5fONiuN.htm](blood-lords-bestiary/vUv8SR7Pa5fONiuN.htm)|Charghar|Charghar|libre|
|[VWGpfLFiu9HKv9hJ.htm](blood-lords-bestiary/VWGpfLFiu9HKv9hJ.htm)|Void Garden|Jardin du néant|libre|
|[vYMWNYvaKYxKm2Jh.htm](blood-lords-bestiary/vYMWNYvaKYxKm2Jh.htm)|Kapoacinth|Kapoacinth|libre|
|[Wh9Rr5uiYgMc55s9.htm](blood-lords-bestiary/Wh9Rr5uiYgMc55s9.htm)|Tombstone Troll|Troll de pierre tombale|libre|
|[woGNg9IcqOtEy9hb.htm](blood-lords-bestiary/woGNg9IcqOtEy9hb.htm)|Zombie Horse|Cheval zombie|libre|
|[wqnc9i6hDS9FP8IO.htm](blood-lords-bestiary/wqnc9i6hDS9FP8IO.htm)|Agorron Guard|Garde Agorron|libre|
|[X6bu6VmaXgtQMBTY.htm](blood-lords-bestiary/X6bu6VmaXgtQMBTY.htm)|Skeleton Rival Necromancer|Nécromancien squelette rival|libre|
|[X9Sz3ttieDhYSlvB.htm](blood-lords-bestiary/X9Sz3ttieDhYSlvB.htm)|Kemnebi|Kemnébi|libre|
|[xtDVxT49QGCo78yp.htm](blood-lords-bestiary/xtDVxT49QGCo78yp.htm)|Rumin Purgo|Rumin Purgo|libre|
|[XWb7V3dkiDlBD8Bn.htm](blood-lords-bestiary/XWb7V3dkiDlBD8Bn.htm)|Blaanlool|Blaanlool|libre|
|[yfyh7TkvcQuyWyie.htm](blood-lords-bestiary/yfyh7TkvcQuyWyie.htm)|Shadowbound Monk Statue|Statue de moine ombreliée|libre|
|[YHDUmGATLSHUgGiM.htm](blood-lords-bestiary/YHDUmGATLSHUgGiM.htm)|Compression Tunnel|Tunnel compresseur|libre|
|[yQHyvoLOi6HxUTfe.htm](blood-lords-bestiary/yQHyvoLOi6HxUTfe.htm)|Umbraex|Umbraex|libre|
|[yydyiiBoUGrzPu2v.htm](blood-lords-bestiary/yydyiiBoUGrzPu2v.htm)|Pokmit Bloody-Pike|Pokmit Pique-sanglant|libre|
|[YZf8zp3ylEoAFyBX.htm](blood-lords-bestiary/YZf8zp3ylEoAFyBX.htm)|Arboreal Snag|Arboréen mort-vivant|libre|
|[ZDrt7yX9seyY3lCC.htm](blood-lords-bestiary/ZDrt7yX9seyY3lCC.htm)|Shabti Votary|Dévôt shabti|libre|
|[zEPIkUw9ZlXi5Jzb.htm](blood-lords-bestiary/zEPIkUw9ZlXi5Jzb.htm)|Afziaka Brute|Brute Afziaka|libre|
|[zgqX9s4hwJ9wMowm.htm](blood-lords-bestiary/zgqX9s4hwJ9wMowm.htm)|Electrified Gate|Barrière électrifiée|libre|
|[zrMBLTlxPCIwtwGo.htm](blood-lords-bestiary/zrMBLTlxPCIwtwGo.htm)|Iron Taviah|Iron Taviah|libre|
|[zVWCYNybRlSdYJrn.htm](blood-lords-bestiary/zVWCYNybRlSdYJrn.htm)|Memory Hole|Trou de mémoire|libre|
|[zw1uj88nmUrCf7iS.htm](blood-lords-bestiary/zw1uj88nmUrCf7iS.htm)|Ectoplasmic Amalgam|Amalgame ectoplasmique|libre|
|[zYY1lFk4tole9Lx5.htm](blood-lords-bestiary/zYY1lFk4tole9Lx5.htm)|Ghoul Crocodile|Goule crocodile|libre|
|[ZzRnrcSstcvJx5Dg.htm](blood-lords-bestiary/ZzRnrcSstcvJx5Dg.htm)|Keystone Trap|Piège de la clé de voûte|libre|
