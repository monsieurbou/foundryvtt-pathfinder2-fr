# État de la traduction (pathfinder-dark-archive)

 * **libre**: 27


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0AdtolhMWrPlzVZY.htm](pathfinder-dark-archive/0AdtolhMWrPlzVZY.htm)|Call of the Void|Appel du néant|libre|
|[34gooaLEuQh0Cu9n.htm](pathfinder-dark-archive/34gooaLEuQh0Cu9n.htm)|Cocoon of Lucid Potential|Cocon du Potentiel lucide|libre|
|[3RL07afKtflSq5Ff.htm](pathfinder-dark-archive/3RL07afKtflSq5Ff.htm)|Entrapping Chair|Chaise piégeuse|libre|
|[6EELzUwaHp1oMfCO.htm](pathfinder-dark-archive/6EELzUwaHp1oMfCO.htm)|False Step Floor|Faux-plancher|libre|
|[71Dv5WjrNOzy1Jtr.htm](pathfinder-dark-archive/71Dv5WjrNOzy1Jtr.htm)|Reflected Desires|Reflet des désirs|libre|
|[8Lo4sys48l7YaK5J.htm](pathfinder-dark-archive/8Lo4sys48l7YaK5J.htm)|The Morrowkin|Le Morrowkin|libre|
|[92rwV7OmBQyY4ZV4.htm](pathfinder-dark-archive/92rwV7OmBQyY4ZV4.htm)|Sigil of Deepest Fears|Symbole des angoisses|libre|
|[9rQvZZQbn1IUfr77.htm](pathfinder-dark-archive/9rQvZZQbn1IUfr77.htm)|Mirror Door|Porte Miroir|libre|
|[cZwTVO2KF85Ak4o6.htm](pathfinder-dark-archive/cZwTVO2KF85Ak4o6.htm)|Confounding Portal|Portail déconcertant|libre|
|[dbbWRZqE509Ac4bH.htm](pathfinder-dark-archive/dbbWRZqE509Ac4bH.htm)|Shrinking Hall|Couloir rétrécissant|libre|
|[eX1vJnbUagKPdSbU.htm](pathfinder-dark-archive/eX1vJnbUagKPdSbU.htm)|Spirit Window|Fenêtres hantées|libre|
|[ICdVjs6JEs0YAefl.htm](pathfinder-dark-archive/ICdVjs6JEs0YAefl.htm)|Reflection|Reflet|libre|
|[IyE7jYfltfPSYzNy.htm](pathfinder-dark-archive/IyE7jYfltfPSYzNy.htm)|False Floor|Sol illusoire|libre|
|[Ldw3d0TYwtMywbM0.htm](pathfinder-dark-archive/Ldw3d0TYwtMywbM0.htm)|Clone Mirrors|Miroir de clônage|libre|
|[mu0khG3glMMREcnD.htm](pathfinder-dark-archive/mu0khG3glMMREcnD.htm)|Distortion Mirror|Miroir déformant|libre|
|[NGV18Lovi4HsjJOc.htm](pathfinder-dark-archive/NGV18Lovi4HsjJOc.htm)|Shuffling Hall|Couloir de mélanges|libre|
|[ObeSl1UJMrCxsVJ6.htm](pathfinder-dark-archive/ObeSl1UJMrCxsVJ6.htm)|Bounding Hounds|Molosses fantômes|libre|
|[P2fNCqJsR0gazMzM.htm](pathfinder-dark-archive/P2fNCqJsR0gazMzM.htm)|Exhaling Portal|Porte venteuse|libre|
|[q9q6ag5sSEEefgUs.htm](pathfinder-dark-archive/q9q6ag5sSEEefgUs.htm)|Verdure's Moonflower|Fleur de lune de verdure|libre|
|[q9R4hKHeErDOaFJu.htm](pathfinder-dark-archive/q9R4hKHeErDOaFJu.htm)|Quartz-Spawned Shadow|Ombre rejeton de quartz|libre|
|[RHjdD47Lr9JrXWes.htm](pathfinder-dark-archive/RHjdD47Lr9JrXWes.htm)|Disorienting Illusions|Illusions distordues|libre|
|[spP95evfvogIuclj.htm](pathfinder-dark-archive/spP95evfvogIuclj.htm)|The Weaver in Dreams|Le Tisseur dans les Rêves|libre|
|[tGhJpdNRItMZTpG7.htm](pathfinder-dark-archive/tGhJpdNRItMZTpG7.htm)|Constricting Hall|Couloir étrangleur|libre|
|[WiFzLguamOR69IcD.htm](pathfinder-dark-archive/WiFzLguamOR69IcD.htm)|Mother Mitera|Mère Mitéra|libre|
|[wjWedZ12f3t8ixE1.htm](pathfinder-dark-archive/wjWedZ12f3t8ixE1.htm)|Shrouded Assailant|Agresseur voilé|libre|
|[wmKvVsfc6Hg1Bj8I.htm](pathfinder-dark-archive/wmKvVsfc6Hg1Bj8I.htm)|K. H. W.'s Echo|Écho de KHW|libre|
|[Z9iID4f6DoEuVrLo.htm](pathfinder-dark-archive/Z9iID4f6DoEuVrLo.htm)|Thalassophobic Pool|Bassin thalassophobique|libre|
