# État de la traduction (kingmaker-features)

 * **changé**: 53
 * **officielle**: 77
 * **aucune**: 2


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[8bBRW8SxM5bgwjXJ.htm](kingmaker-features/8bBRW8SxM5bgwjXJ.htm)|Engines of War|
|[aupG5cvPAfJszMfO.htm](kingmaker-features/aupG5cvPAfJszMfO.htm)|Reduced Accuracy|
|[EmfxjNH1uxtDDqZu.htm](kingmaker-features/EmfxjNH1uxtDDqZu.htm)|Effect: Guard (Critical Success)|
|[ib8BcumGskNVJdZS.htm](kingmaker-features/ib8BcumGskNVJdZS.htm)|Overrun|
|[rF8Nsas5KrrBEeqr.htm](kingmaker-features/rF8Nsas5KrrBEeqr.htm)|Effect: Guard (Success)|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0yeq1xgdscswGtRZ.htm](kingmaker-features/0yeq1xgdscswGtRZ.htm)|Ruin Resistance|Résistance à la ruine|changé|
|[1U9SbpBDXgsLGlzq.htm](kingmaker-features/1U9SbpBDXgsLGlzq.htm)|Defensive Stance|Posture défensive|changé|
|[3jYTaOb8FQJueNzz.htm](kingmaker-features/3jYTaOb8FQJueNzz.htm)|Fame and Fortune|Gloire et fortune|changé|
|[4h7c6C5doDA5rxyO.htm](kingmaker-features/4h7c6C5doDA5rxyO.htm)|Ambush|Embuscade|changé|
|[7LMDwpVijK7x5Jst.htm](kingmaker-features/7LMDwpVijK7x5Jst.htm)|Shaken|Ébranlée|changé|
|[899sW8mbJSn16zga.htm](kingmaker-features/899sW8mbJSn16zga.htm)|Hold the Line|Maintien des positions|changé|
|[8wjiF3ctXUjP9oyX.htm](kingmaker-features/8wjiF3ctXUjP9oyX.htm)|Counterattack|Contre-attaquer|changé|
|[8XXylMGJuqe1ozMk.htm](kingmaker-features/8XXylMGJuqe1ozMk.htm)|Rally|Rallier les troupes|changé|
|[9dkyZ7r1z7loOxI7.htm](kingmaker-features/9dkyZ7r1z7loOxI7.htm)|Insider Trading|Délit d’initié|changé|
|[AF9U2WOkD9dDlIXE.htm](kingmaker-features/AF9U2WOkD9dDlIXE.htm)|Field Triage|Triage des blessés|changé|
|[agjqRWTTIWHwiKsd.htm](kingmaker-features/agjqRWTTIWHwiKsd.htm)|Battlefield Medicine|Médecine de guerre|changé|
|[AK3JMI3Bgz5QQw03.htm](kingmaker-features/AK3JMI3Bgz5QQw03.htm)|Civic Planning|Planification des travaux|changé|
|[AOFU8pOTMjVdiyNd.htm](kingmaker-features/AOFU8pOTMjVdiyNd.htm)|False Retreat|Feindre la retraite|changé|
|[aZImSBBmTHff2POP.htm](kingmaker-features/aZImSBBmTHff2POP.htm)|Routed|En déroute|changé|
|[CtNmx1wUgHDikQqI.htm](kingmaker-features/CtNmx1wUgHDikQqI.htm)|Fine Living|Vie raffinée|changé|
|[d5CJgabylXtTD2qj.htm](kingmaker-features/d5CJgabylXtTD2qj.htm)|Pinned|Immobilisée|changé|
|[dEdTJgaaYiDitp5O.htm](kingmaker-features/dEdTJgaaYiDitp5O.htm)|Outflank|Déborder|changé|
|[DURRMyANFnFccM24.htm](kingmaker-features/DURRMyANFnFccM24.htm)|Efficient|Efficace|changé|
|[DxZRVOsPVnQvI0fe.htm](kingmaker-features/DxZRVOsPVnQvI0fe.htm)|Focused Devotion|Dévotion focalisée|changé|
|[E7B8TQD1Z9HCwqkr.htm](kingmaker-features/E7B8TQD1Z9HCwqkr.htm)|Experienced Leadership|Dirigeants expérimentés|changé|
|[Ei8EMUwVxUNyMQJJ.htm](kingmaker-features/Ei8EMUwVxUNyMQJJ.htm)|Lost|Perdue|changé|
|[eu0ttUFovbgmyLJ5.htm](kingmaker-features/eu0ttUFovbgmyLJ5.htm)|Defeated|Vaincue|changé|
|[G2eBcOnUHb3yT7JL.htm](kingmaker-features/G2eBcOnUHb3yT7JL.htm)|Dirty Fighting|Combattre de façon déloyale|changé|
|[G6MAyRjG91I8iNLR.htm](kingmaker-features/G6MAyRjG91I8iNLR.htm)|Opening Salvo|Première salve|changé|
|[gb1LQ2vw8hUelVDU.htm](kingmaker-features/gb1LQ2vw8hUelVDU.htm)|Cooperative Leadership|Dirigeants coopératifs|changé|
|[ggVahjiAlVICpiPA.htm](kingmaker-features/ggVahjiAlVICpiPA.htm)|Taunt|Intimider|changé|
|[GIbm9qo8VuFgPywJ.htm](kingmaker-features/GIbm9qo8VuFgPywJ.htm)|Covering Fire|Tir de couverture|changé|
|[H9myGV9t4SdcadA9.htm](kingmaker-features/H9myGV9t4SdcadA9.htm)|Inspiring Entertainment|Divertissements inspirants|changé|
|[Hi4LKGOKe6yMDOH5.htm](kingmaker-features/Hi4LKGOKe6yMDOH5.htm)|Feint|Feinter|changé|
|[HLu1o1RzCZxWjOJJ.htm](kingmaker-features/HLu1o1RzCZxWjOJJ.htm)|Overwhelming Bombardment|Bombardement impitoyable|changé|
|[HUGtP04pX8h0vgUc.htm](kingmaker-features/HUGtP04pX8h0vgUc.htm)|Flaming Shot|Tir enflammé|changé|
|[IhjlbJinff1wUSjL.htm](kingmaker-features/IhjlbJinff1wUSjL.htm)|Retreat|Se replier|changé|
|[JYY8vQxPe9AIGTvv.htm](kingmaker-features/JYY8vQxPe9AIGTvv.htm)|Fortified Fiefs|Fiefs fortifiés|changé|
|[lcNZMlMNPUrnMgQ2.htm](kingmaker-features/lcNZMlMNPUrnMgQ2.htm)|Sharpshooter|Tireurs d'élite|changé|
|[M4MFMxGyZZyGOAKh.htm](kingmaker-features/M4MFMxGyZZyGOAKh.htm)|Settlement Construction|Construction d'une communauté|changé|
|[mHWF5XwUi8RK2lET.htm](kingmaker-features/mHWF5XwUi8RK2lET.htm)|Flexible Tactics|Tactiques flexibles|changé|
|[NsehZsc6lmPje8YS.htm](kingmaker-features/NsehZsc6lmPje8YS.htm)|Liquidate Resources|Liquider des ressources|changé|
|[OMSi4mgHqoBqUFuj.htm](kingmaker-features/OMSi4mgHqoBqUFuj.htm)|Quality of Life|Bon niveau de vie|changé|
|[oVhkdym3D6TiTbrN.htm](kingmaker-features/oVhkdym3D6TiTbrN.htm)|Mired|Enlisée|changé|
|[phtwOol1wETryF7b.htm](kingmaker-features/phtwOol1wETryF7b.htm)|Guard|Se défendre|changé|
|[Pu5bptxLrKFyEzFh.htm](kingmaker-features/Pu5bptxLrKFyEzFh.htm)|Disengage|Se désengager|changé|
|[qCxyuNhzaaYlYBum.htm](kingmaker-features/qCxyuNhzaaYlYBum.htm)|Battle|Combattre|changé|
|[QRwIcmCEHpDQm9DO.htm](kingmaker-features/QRwIcmCEHpDQm9DO.htm)|Merciless|Implacabilité|changé|
|[sMBQ8i8xN1X0rSs9.htm](kingmaker-features/sMBQ8i8xN1X0rSs9.htm)|Life Of Luxury|Vie de luxe|changé|
|[U19mDhbYbOBQmbO9.htm](kingmaker-features/U19mDhbYbOBQmbO9.htm)|Endure Anarchy|Résister à l'anarchie|changé|
|[VJ8S2h9D8ksQ5O1w.htm](kingmaker-features/VJ8S2h9D8ksQ5O1w.htm)|Defensive Tactics|Tactiques défensives|changé|
|[wHoXoyci1lddRR2R.htm](kingmaker-features/wHoXoyci1lddRR2R.htm)|Advance|Avancer|changé|
|[Y8Gpb1Ma8w3Gb8qX.htm](kingmaker-features/Y8Gpb1Ma8w3Gb8qX.htm)|Reckless Flankers|Prise en tenaille téméraire|changé|
|[Z6jMZgAxI1zRO7Sl.htm](kingmaker-features/Z6jMZgAxI1zRO7Sl.htm)|All-Out Assault|Mener un assaut général|changé|
|[ZSUPsqeMBDTaX2H4.htm](kingmaker-features/ZSUPsqeMBDTaX2H4.htm)|Explosive Shot|Tir explosif|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1wzNJEvPCuMwJdHe.htm](kingmaker-features/1wzNJEvPCuMwJdHe.htm)|Recover Army|Soigner une armée|officielle|
|[2IqM1XHELKwARAx1.htm](kingmaker-features/2IqM1XHELKwARAx1.htm)|Engaged|Engagée|officielle|
|[2WrcEhR8WzgHOoZE.htm](kingmaker-features/2WrcEhR8WzgHOoZE.htm)|Clandestine Business|Mener des affaires clandestines|officielle|
|[35z42dfsd7zHZIoP.htm](kingmaker-features/35z42dfsd7zHZIoP.htm)|Quick Recovery|Récupération rapide|officielle|
|[3e1qshiIssEUbNw3.htm](kingmaker-features/3e1qshiIssEUbNw3.htm)|Cavalry Experts|Expertise en cavalerie|officielle|
|[3IHNdiGw6klNARiM.htm](kingmaker-features/3IHNdiGw6klNARiM.htm)|Celebrate Holiday|Célébrer un jour de fête|officielle|
|[3qJcDEZNwJV6hpkg.htm](kingmaker-features/3qJcDEZNwJV6hpkg.htm)|Establish Trade Agreement|Conclure un accord commercial|officielle|
|[3tI2d60BTA3hN2MW.htm](kingmaker-features/3tI2d60BTA3hN2MW.htm)|Skill Training|Compétence qualifiante|officielle|
|[4QbqwZxEbSt3qcHw.htm](kingmaker-features/4QbqwZxEbSt3qcHw.htm)|Offensive Gambit|Manoeuvre offensive|officielle|
|[77aIhr8LOfTt1QHp.htm](kingmaker-features/77aIhr8LOfTt1QHp.htm)|Collect Taxes|Collecter des impôts|officielle|
|[8B6PmgtMH2TthBSX.htm](kingmaker-features/8B6PmgtMH2TthBSX.htm)|Establish Work Site|Établir un site d'exploitation|officielle|
|[8upne4E6Q7Da5T5w.htm](kingmaker-features/8upne4E6Q7Da5T5w.htm)|Distant|Distante|officielle|
|[aWIjuZ4sJI8SnK2D.htm](kingmaker-features/aWIjuZ4sJI8SnK2D.htm)|Destroyed|Anéantie|officielle|
|[B70XC3Ci1SqemPlT.htm](kingmaker-features/B70XC3Ci1SqemPlT.htm)|Craft Luxuries|Fabriquer des produits de luxe|officielle|
|[bBXJdNxd0Pa1qScw.htm](kingmaker-features/bBXJdNxd0Pa1qScw.htm)|Hire Adventurers|Engager des aventuriers|officielle|
|[bc9JdbyJ8HWrpLLx.htm](kingmaker-features/bc9JdbyJ8HWrpLLx.htm)|Train Army|Former une armée|officielle|
|[BChcBEZpcqMnLISC.htm](kingmaker-features/BChcBEZpcqMnLISC.htm)|Pull Together|Coopération|officielle|
|[BK0mMlFP2jIS4xGl.htm](kingmaker-features/BK0mMlFP2jIS4xGl.htm)|Kingdom Assurance|Assurance du royaume|officielle|
|[c8lNjzrY2WM60Zsv.htm](kingmaker-features/c8lNjzrY2WM60Zsv.htm)|Fortified|En position de défense|officielle|
|[cvOXpo1Sy8nfXfPO.htm](kingmaker-features/cvOXpo1Sy8nfXfPO.htm)|Claim Hex|Revendiquer un hexagone|officielle|
|[dmzpRlOrorEFUETl.htm](kingmaker-features/dmzpRlOrorEFUETl.htm)|Weary|Harassée|officielle|
|[DrUT5sE0EuU5dtPa.htm](kingmaker-features/DrUT5sE0EuU5dtPa.htm)|Increased Ammunition|Augmentation des munitions|officielle|
|[EgUxcWpmUaVrHP3L.htm](kingmaker-features/EgUxcWpmUaVrHP3L.htm)|Build Roads|Construire des routes|officielle|
|[FBjZ1aAE6a4jt8r2.htm](kingmaker-features/FBjZ1aAE6a4jt8r2.htm)|Keen Eyed|Vue perçante|officielle|
|[FGQfcX6BelYrJatp.htm](kingmaker-features/FGQfcX6BelYrJatp.htm)|Send Diplomatic Envoy|Dépêcher un représentant diplomatique|officielle|
|[Fko5kdpi9Oxas6Ty.htm](kingmaker-features/Fko5kdpi9Oxas6Ty.htm)|Recruit Army|Recruter une armée|officielle|
|[FQJqfnB9FDaLttWa.htm](kingmaker-features/FQJqfnB9FDaLttWa.htm)|Envy of the World|Joyau mondial|officielle|
|[G6UuSaPpXYa80qDw.htm](kingmaker-features/G6UuSaPpXYa80qDw.htm)|Rest and Relax|Se reposer et se détendre|officielle|
|[GhNppJC2nzceW8FC.htm](kingmaker-features/GhNppJC2nzceW8FC.htm)|Bloodied but Unbroken|Ensanglantés mais invaincus|officielle|
|[i7nsYS9lgFlgBXnl.htm](kingmaker-features/i7nsYS9lgFlgBXnl.htm)|Create a Masterpiece|Créer un chef d'œuvre|officielle|
|[ibcMcEGRbPRtk9Pu.htm](kingmaker-features/ibcMcEGRbPRtk9Pu.htm)|Outflanked|Débordée|officielle|
|[ie9Wuohdx3JgzRbP.htm](kingmaker-features/ie9Wuohdx3JgzRbP.htm)|Fortify Hex|Fortifier un hexagone|officielle|
|[IiD3kKszFfHZ9cZ1.htm](kingmaker-features/IiD3kKszFfHZ9cZ1.htm)|Toughened Soldiers|Soldats endurcis|officielle|
|[IWanYXCWh3cvC6ti.htm](kingmaker-features/IWanYXCWh3cvC6ti.htm)|Disband Army|Démobiliser une armée|officielle|
|[iYLFBep0kddHQkW9.htm](kingmaker-features/iYLFBep0kddHQkW9.htm)|Garrison Army|Placer une armée en garnison|officielle|
|[J1yiZK4gr5c9zFOl.htm](kingmaker-features/J1yiZK4gr5c9zFOl.htm)|Irrigation|Irriguer|officielle|
|[JHdIw0ADSRVSo0YP.htm](kingmaker-features/JHdIw0ADSRVSo0YP.htm)|Abandon Hex|Abandonner un hexagone|officielle|
|[JHLZPhd9APIqJHsA.htm](kingmaker-features/JHLZPhd9APIqJHsA.htm)|Clear Hex|Nettoyer un hexagone|officielle|
|[kiiKY1n1EVnuzEDK.htm](kingmaker-features/kiiKY1n1EVnuzEDK.htm)|Expansion Expert|Expert en expansion|officielle|
|[KSqdhh3JYt5wTbEd.htm](kingmaker-features/KSqdhh3JYt5wTbEd.htm)|Civil Service|Fonction publique|officielle|
|[l0Wlgj6LaDNRRUa6.htm](kingmaker-features/l0Wlgj6LaDNRRUa6.htm)|Gather Livestock|Réunir du bétail|officielle|
|[LhefN6fEfIRuRozx.htm](kingmaker-features/LhefN6fEfIRuRozx.htm)|Manage Trade Agreements|Gérer les accord commerciaux|officielle|
|[li7WowfuIbGmCxKo.htm](kingmaker-features/li7WowfuIbGmCxKo.htm)|Repair Reputation (Trained)|Redorer sa réputation (qualifié)|officielle|
|[nc4XV5NFuHDz93O0.htm](kingmaker-features/nc4XV5NFuHDz93O0.htm)|Pledge of Fealty (Trained)|Serment d'allégence (Qualifié)|officielle|
|[nDDEbrWj2JouxlRw.htm](kingmaker-features/nDDEbrWj2JouxlRw.htm)|Practical Magic|Magie pratique|officielle|
|[NF4ftwc8fPG2xXRE.htm](kingmaker-features/NF4ftwc8fPG2xXRE.htm)|Supernatural Solution|Trouver une solution surnaturelle|officielle|
|[NPUQJAt6lzBGRPZf.htm](kingmaker-features/NPUQJAt6lzBGRPZf.htm)|Infiltration|Infiltration|officielle|
|[NpVIqD3LgI0Kb9RA.htm](kingmaker-features/NpVIqD3LgI0Kb9RA.htm)|Build Structure|Bâtir une structure|officielle|
|[nWAGfa4DoEODaKsq.htm](kingmaker-features/nWAGfa4DoEODaKsq.htm)|Improve Lifestyle|Améliorer la qualité de vie|officielle|
|[o4LgcCVBBpf0wpfD.htm](kingmaker-features/o4LgcCVBBpf0wpfD.htm)|Low-Light Vision|Vision nocturne|officielle|
|[O58VXVbilHphAKDp.htm](kingmaker-features/O58VXVbilHphAKDp.htm)|Free and Fair|Transparence et équité|officielle|
|[o81alpjEki9cESun.htm](kingmaker-features/o81alpjEki9cESun.htm)|Harvest Crops|Récolter|officielle|
|[ofQBVTc54viN8v6C.htm](kingmaker-features/ofQBVTc54viN8v6C.htm)|Provide Care|Dispenser des soins|officielle|
|[QbzN7Ip8LWRLqAes.htm](kingmaker-features/QbzN7Ip8LWRLqAes.htm)|Focused Attention|Donner de son temps|officielle|
|[QKBoVicXa0GfKrYG.htm](kingmaker-features/QKBoVicXa0GfKrYG.htm)|Request Foreign Aid|Demander une aide extérieure|officielle|
|[QtSFmMG3Yos0JFSD.htm](kingmaker-features/QtSFmMG3Yos0JFSD.htm)|Tap Treasury|Puiser dans la trésorerie|officielle|
|[Rej53lopIy2DKQm6.htm](kingmaker-features/Rej53lopIy2DKQm6.htm)|Favored Land|Terre bien-aimée|officielle|
|[RPUUcyuMbvud45TS.htm](kingmaker-features/RPUUcyuMbvud45TS.htm)|Establish Settlement|Fonder une communauté|officielle|
|[rw2BHBpLyh5Xvqw5.htm](kingmaker-features/rw2BHBpLyh5Xvqw5.htm)|Demolish|Démolir|officielle|
|[rXNZTmesfoBln2vs.htm](kingmaker-features/rXNZTmesfoBln2vs.htm)|Go Fishing|Pêcher|officielle|
|[s6WMS915YCbbfty4.htm](kingmaker-features/s6WMS915YCbbfty4.htm)|Quell Unrest|Calmer l'agitation|officielle|
|[Sc4nKtdFqZqeh86f.htm](kingmaker-features/Sc4nKtdFqZqeh86f.htm)|Creative Solution|Trouver une solution créative|officielle|
|[sdwSrVfu9yG1uwio.htm](kingmaker-features/sdwSrVfu9yG1uwio.htm)|Live off the Land|Vivre de la terre|officielle|
|[swAA2zetF14HIhUc.htm](kingmaker-features/swAA2zetF14HIhUc.htm)|Trade Commodities|Vendre des marchandises|officielle|
|[tWS8aMCyeGKcnFh1.htm](kingmaker-features/tWS8aMCyeGKcnFh1.htm)|Concealed|Masquée|officielle|
|[u8LpoCGIo22kw00n.htm](kingmaker-features/u8LpoCGIo22kw00n.htm)|Prognostication|Lire les augures|officielle|
|[UvIWZAPm8kfq3hDq.htm](kingmaker-features/UvIWZAPm8kfq3hDq.htm)|Keep up the Pressure|Maintien de la pression|officielle|
|[VadpbmRYNXSX7g8z.htm](kingmaker-features/VadpbmRYNXSX7g8z.htm)|Purchase Commodities|Acheter des marchandises|officielle|
|[vHKPoZycxZAA1vpz.htm](kingmaker-features/vHKPoZycxZAA1vpz.htm)|New Leadership|Renouveler le gouvernement|officielle|
|[WFng3pxgEAdpdy1p.htm](kingmaker-features/WFng3pxgEAdpdy1p.htm)|Muddle Through|Débrouillardise|officielle|
|[WGpkcIChjIk1i0q0.htm](kingmaker-features/WGpkcIChjIk1i0q0.htm)|Crush Dissent|Écraser toute dissidence|officielle|
|[WuSEEEP2rEEz1d9t.htm](kingmaker-features/WuSEEEP2rEEz1d9t.htm)|Outfit Army|Équiper une armée|officielle|
|[x2a7kuyipzczqsFc.htm](kingmaker-features/x2a7kuyipzczqsFc.htm)|Capital Investment|Investissement en capital|officielle|
|[XaK0Q9fda8IiIvrz.htm](kingmaker-features/XaK0Q9fda8IiIvrz.htm)|Relocate Capital|Changer de capitale|officielle|
|[y1JGtzGmMtVh5USK.htm](kingmaker-features/y1JGtzGmMtVh5USK.htm)|Darkvision|Vision dans le noir|officielle|
|[yrmHICfnXFGP5fJf.htm](kingmaker-features/yrmHICfnXFGP5fJf.htm)|Establish Farmland|Établir des terres agricoles|officielle|
|[ysKkyZp02RMujObE.htm](kingmaker-features/ysKkyZp02RMujObE.htm)|Deploy Army|Déployer une armée|officielle|
