# État de la traduction (action-macros)

 * **libre**: 71


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1AIo5UcVbCmvpRL3.htm](action-macros/1AIo5UcVbCmvpRL3.htm)|Exploration: Avoid Notice|Exploration : Échapper aux regards|libre|
|[1JpYPlIkjyseE9JU.htm](action-macros/1JpYPlIkjyseE9JU.htm)|Create a Diversion - Trick: Deception|Faire diversion - Astuce: Duperie|libre|
|[1Sj2Pz3VI2SFWqZw.htm](action-macros/1Sj2Pz3VI2SFWqZw.htm)|Make an Impression: Diplomacy|Faire bonne impression : Diplomatie|libre|
|[2qhYHkcSsTJoSwrJ.htm](action-macros/2qhYHkcSsTJoSwrJ.htm)|Tumble Through: Acrobatics|Déplacement acrobatique : Acrobaties|libre|
|[4hfQEMiEOBbqelAh.htm](action-macros/4hfQEMiEOBbqelAh.htm)|Raise a Shield|Lever un bouclier|libre|
|[50Q0DYL33Kalu1BH.htm](action-macros/50Q0DYL33Kalu1BH.htm)|Escape: Acrobatics|S'échapper : Acrobaties|libre|
|[55mxH0w8UkY1o3Xv.htm](action-macros/55mxH0w8UkY1o3Xv.htm)|Balance: Acrobatics|Garder l'équilibre : Acrobaties|libre|
|[7zqvzpR9gvHPUDqC.htm](action-macros/7zqvzpR9gvHPUDqC.htm)|Whirling Throw: Athletics|Projection en rotation : Athlétisme|libre|
|[8YrH37NzKRuiKFbF.htm](action-macros/8YrH37NzKRuiKFbF.htm)|Pick a Lock: Thievery|Crocheter une serrure : Vol|libre|
|[9RNumMausgG7adgL.htm](action-macros/9RNumMausgG7adgL.htm)|Coerce: Intimidation|Contraindre : Intimidation|libre|
|[9Ul5Op5OceT9P5SS.htm](action-macros/9Ul5Op5OceT9P5SS.htm)|Maneuver in Flight: Acrobatics|Manoeuvre en vol : Acrobaties|libre|
|[aDsYSdRqiC6qQIOQ.htm](action-macros/aDsYSdRqiC6qQIOQ.htm)|Create a Diversion - Distracting Words: Deception|Faire diversion - Paroles de diversion : Duperie|libre|
|[Al5LYMMdeDcpC9Br.htm](action-macros/Al5LYMMdeDcpC9Br.htm)|Track: Survival|Pister : Survie|libre|
|[aOrMEnTVPpb1vbzj.htm](action-macros/aOrMEnTVPpb1vbzj.htm)|Escape: Unarmed Attack|S'échapper : Attaque à mains nues|libre|
|[BQTA7bL264189Xla.htm](action-macros/BQTA7bL264189Xla.htm)|Repair: Crafting|Réparer : Artisanat|libre|
|[Bw3rSN774LaHLELJ.htm](action-macros/Bw3rSN774LaHLELJ.htm)|Perform - Play keyboard instrument: Performance|Se produire - jouer d'un instrument à clavier : Représentation|libre|
|[dWcrojMk0d2WRPBq.htm](action-macros/dWcrojMk0d2WRPBq.htm)|Perform - Singing: Performance|Se produire - chanter : Représentation|libre|
|[EDLftLWLBefTFDtu.htm](action-macros/EDLftLWLBefTFDtu.htm)|Bon Mot: Diplomacy|Bon mot : Diplomatie|libre|
|[EpiQHXzHv0GS1tj0.htm](action-macros/EpiQHXzHv0GS1tj0.htm)|Escape|S'échapper|libre|
|[FlM3HvpnsZpCKawG.htm](action-macros/FlM3HvpnsZpCKawG.htm)|Hide: Stealth|Se cacher : Discrétion|libre|
|[Gj68YCVlDjc75iCP.htm](action-macros/Gj68YCVlDjc75iCP.htm)|Palm an Object: Thievery|Escamoter un objet : Vol|libre|
|[gRj7xUfcpUZQLrOC.htm](action-macros/gRj7xUfcpUZQLrOC.htm)|Trip: Athletics|Croc-en-jambe : Athlétisme|libre|
|[gzTJWA5vJkZI9Gj5.htm](action-macros/gzTJWA5vJkZI9Gj5.htm)|Reposition: Athletics|Repositionner : Athlétisme|libre|
|[HQrDnBbbSCQEXrSb.htm](action-macros/HQrDnBbbSCQEXrSb.htm)|Perform - Play wind instrument: Performance|Se produire - jouer d'un instrument à vent : Représentation|libre|
|[HSTkVuv0SjTNK3Xx.htm](action-macros/HSTkVuv0SjTNK3Xx.htm)|Sneak: Stealth|Être furtif : Discrétion|libre|
|[I6MMTpk9faBOHb2T.htm](action-macros/I6MMTpk9faBOHb2T.htm)|Encouraging Words|Paroles encourageantes|libre|
|[i6WwSpxDuV2jCwB4.htm](action-macros/i6WwSpxDuV2jCwB4.htm)|Perform - Comedy: Performance|Se produire - jouer la comédie : Représentation|libre|
|[i95kcGLIQKOTsnv6.htm](action-macros/i95kcGLIQKOTsnv6.htm)|Grapple: Athletics|Saisir : Athlétisme|libre|
|[jOM1w2GymtSuEurf.htm](action-macros/jOM1w2GymtSuEurf.htm)|Tamper: Crafting|Trafiquer : Artisanat|libre|
|[k5nW4jGyXD0Oq9LR.htm](action-macros/k5nW4jGyXD0Oq9LR.htm)|Impersonate: Deception|Se déguiser : Duperie|libre|
|[l5pbgrj8SSNtRGs8.htm](action-macros/l5pbgrj8SSNtRGs8.htm)|Administer First Aid - Stabilize: Medicine|Prodiguer les premiers soins - Stabiliser : Médecine|libre|
|[lkEcQQss16SIrVxM.htm](action-macros/lkEcQQss16SIrVxM.htm)|Escape: Athletics|S'échapper : Athlétisme|libre|
|[LN67MgbGE8IHb2X0.htm](action-macros/LN67MgbGE8IHb2X0.htm)|Sense Direction: Survival|S'orienter : Survie|libre|
|[LXCy1iJddD95Z91s.htm](action-macros/LXCy1iJddD95Z91s.htm)|Climb: Athletics|Escalader : Athlétisme|libre|
|[m4iM5r3TfvQs5Y2n.htm](action-macros/m4iM5r3TfvQs5Y2n.htm)|Treat Disease: Medicine|Soigner la maladie : Médecine|libre|
|[mkKko3CEBCyJVQw1.htm](action-macros/mkKko3CEBCyJVQw1.htm)|Subsist: Society|Subsister : Société|libre|
|[mNphXpAkmGsMadUv.htm](action-macros/mNphXpAkmGsMadUv.htm)|Create Forgery: Society|Contrefaire : Société|libre|
|[nEwqNNWX6scLt4sc.htm](action-macros/nEwqNNWX6scLt4sc.htm)|Demoralize: Intimidation|Démoraliser : Intimidation|libre|
|[nvpRlDKUD4SjZaJ0.htm](action-macros/nvpRlDKUD4SjZaJ0.htm)|Perform - Play string instrument: Performance|Se produire - jouer d'un instrument à cordes : Représentation|libre|
|[Om0rNX4K3pQcXDJn.htm](action-macros/Om0rNX4K3pQcXDJn.htm)|Perform - Oratory: Performance|Se produire - Art oratoire : Représentation|libre|
|[ooiO59Ch2QaebOmc.htm](action-macros/ooiO59Ch2QaebOmc.htm)|Disarm: Athletics|Désarmer : Athlétisme|libre|
|[oxowCzHbxSGOWRke.htm](action-macros/oxowCzHbxSGOWRke.htm)|Steel Your Resolve|Puiser dans votre résolution|libre|
|[PgA74rWIAupEr7LU.htm](action-macros/PgA74rWIAupEr7LU.htm)|Perform - Dance: Performance|Se produire - danser : Représentation|libre|
|[PmHt7Gb5fCrlWWTr.htm](action-macros/PmHt7Gb5fCrlWWTr.htm)|Sense Motive: Perception|Deviner les intentions : Perception|libre|
|[QPsV0qi2zXm7syt6.htm](action-macros/QPsV0qi2zXm7syt6.htm)|Long Jump: Athletics|Saut en longueur : Athlétisme|libre|
|[R03LRl2RBbsm6EcF.htm](action-macros/R03LRl2RBbsm6EcF.htm)|Treat Poison: Medicine|Soigner un empoisonnement : Médecine|libre|
|[rCgGPEyXbzLFcio6.htm](action-macros/rCgGPEyXbzLFcio6.htm)|Gather Information: Diplomacy|Recueillir des informations : Diplomatie|libre|
|[RjfPFjqPrNve6eeh.htm](action-macros/RjfPFjqPrNve6eeh.htm)|Feint: Deception|Feinter : Duperie|libre|
|[RZyfkw1DiqVy3JUC.htm](action-macros/RZyfkw1DiqVy3JUC.htm)|Decipher Writing: Occultism|Déchiffre un texte : Occultisme|libre|
|[sDUERv4E88G5BRPr.htm](action-macros/sDUERv4E88G5BRPr.htm)|Decipher Writing: Religion|Déchiffre un texte : Religion|libre|
|[T2QNEoRojMWEec4a.htm](action-macros/T2QNEoRojMWEec4a.htm)|Disable Device: Thievery|Désamorcer un dispositif : Vol|libre|
|[tbveXG4gaIoKnsWX.htm](action-macros/tbveXG4gaIoKnsWX.htm)|Request: Diplomacy|Solliciter : Diplomatie|libre|
|[tikhJ2b6AMh7wQU7.htm](action-macros/tikhJ2b6AMh7wQU7.htm)|Seek: Perception|Chercher : Perception|libre|
|[TIlUkCzviYxdVk4E.htm](action-macros/TIlUkCzviYxdVk4E.htm)|Swim: Athletics|Nager : Athlétisme|libre|
|[Tu7LIRelQsiOuo1l.htm](action-macros/Tu7LIRelQsiOuo1l.htm)|Craft: Crafting|Fabriquer : Artisanat|libre|
|[U6WjxFPn4fUqIrfl.htm](action-macros/U6WjxFPn4fUqIrfl.htm)|Decipher Writing: Arcana|Déchiffre un texte : Arcanes|libre|
|[UKHPveLpG7hUs4D0.htm](action-macros/UKHPveLpG7hUs4D0.htm)|Squeeze: Acrobatics|Se serrer : Acrobaties|libre|
|[UoRM6ZMhhhjz1vu8.htm](action-macros/UoRM6ZMhhhjz1vu8.htm)|Perform - Play percussion instrument: Performance|Se produire - jouer d'un instrument à percussions : Représentation|libre|
|[v3dlDjFlOmT5T2gC.htm](action-macros/v3dlDjFlOmT5T2gC.htm)|High Jump: Athletics|Saut en hauteur : Athlétisme|libre|
|[vmoO2VasZyTgs3PH.htm](action-macros/vmoO2VasZyTgs3PH.htm)|Perform - Acting: Performance|Se produire - Art dramatique : Représentation|libre|
|[VTg4t8kYTvXcHROq.htm](action-macros/VTg4t8kYTvXcHROq.htm)|Lie: Deception|Mentir : Duperie|libre|
|[xcrdOOiN0l6O1sIn.htm](action-macros/xcrdOOiN0l6O1sIn.htm)|Command an Animal: Nature|Diriger un animal : Nature|libre|
|[yMTKMnaYSGtDz4wk.htm](action-macros/yMTKMnaYSGtDz4wk.htm)|Force Open: Athletics|Ouvrir de force : Athlétisme|libre|
|[yNry1xMZqdWHncbV.htm](action-macros/yNry1xMZqdWHncbV.htm)|Shove: Athletics|Pousser : Athlétisme|libre|
|[YWAvvDXpdW1fYPFo.htm](action-macros/YWAvvDXpdW1fYPFo.htm)|Decipher Writing: Society|Déchiffrer un texte : Société|libre|
|[ZEWD4zcEDQwYhVT8.htm](action-macros/ZEWD4zcEDQwYhVT8.htm)|Administer First Aid - Stop Bleeding: Medicine|Prodiguer les premiers soins - Arrêter le saignement : Médecine|libre|
|[zjovbAeuLvyuWFKd.htm](action-macros/zjovbAeuLvyuWFKd.htm)|Steal: Thievery|Voler : Vol|libre|
|[zkqh01BoXDVgydzo.htm](action-macros/zkqh01BoXDVgydzo.htm)|Subsist: Survival|Subsister : Survie|libre|
|[zn0HadZeoKDALxRu.htm](action-macros/zn0HadZeoKDALxRu.htm)|Conceal an Object: Stealth|Dissimuler un objet : Vol|libre|
|[ZS5z1TqckWPIMGDN.htm](action-macros/ZS5z1TqckWPIMGDN.htm)|Athletics: Arcane Slam|Athlétisme : Plaquage arcanique|libre|
|[zUJ0UhuoFt5a7tiN.htm](action-macros/zUJ0UhuoFt5a7tiN.htm)|Create a Diversion - Gesture: Deception|Faire diversion - Geste : Duperie|libre|
