# État de la traduction (monsters-of-myth-bestiary)

 * **aucune**: 42


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0ypo8Vt3B6p9DqAt.htm](monsters-of-myth-bestiary/0ypo8Vt3B6p9DqAt.htm)|Collapsing Structure|
|[10srqxC7QZy0dq2g.htm](monsters-of-myth-bestiary/10srqxC7QZy0dq2g.htm)|Spectral Devil|
|[5PwT7t6eTC0qcK7E.htm](monsters-of-myth-bestiary/5PwT7t6eTC0qcK7E.htm)|Grisantian Lion|
|[7a57dwK5XrV78ajM.htm](monsters-of-myth-bestiary/7a57dwK5XrV78ajM.htm)|Temteki|
|[a1vTm01GBVF4TI8f.htm](monsters-of-myth-bestiary/a1vTm01GBVF4TI8f.htm)|Taljjae (The Beast)|
|[bhLbKN7MJofTabfK.htm](monsters-of-myth-bestiary/bhLbKN7MJofTabfK.htm)|Taljjae|
|[Bi6bi9Ko5x9OJfDT.htm](monsters-of-myth-bestiary/Bi6bi9Ko5x9OJfDT.htm)|Desert's Howl|
|[C87bRxKDTuJXGkPG.htm](monsters-of-myth-bestiary/C87bRxKDTuJXGkPG.htm)|Tehialai-Thief-of-Ships|
|[CgNC8uWPrJbGDklJ.htm](monsters-of-myth-bestiary/CgNC8uWPrJbGDklJ.htm)|Spawn Of Kothogaz|
|[EmWSRL2aYBcHCgrW.htm](monsters-of-myth-bestiary/EmWSRL2aYBcHCgrW.htm)|Crystal Pin|
|[EXyfhhX9GLOnK1uZ.htm](monsters-of-myth-bestiary/EXyfhhX9GLOnK1uZ.htm)|Spring-Heeled Jack|
|[FhgZOBWNMa5t7pYV.htm](monsters-of-myth-bestiary/FhgZOBWNMa5t7pYV.htm)|Mosquito Witch (The Legion Leech)|
|[G1jYZOjA3E3BqrIM.htm](monsters-of-myth-bestiary/G1jYZOjA3E3BqrIM.htm)|Melfesh Monster|
|[hfi0qJePy2cf4Fqe.htm](monsters-of-myth-bestiary/hfi0qJePy2cf4Fqe.htm)|Taljjae (The Wanderer)|
|[hLyLnBn7lEFstUG2.htm](monsters-of-myth-bestiary/hLyLnBn7lEFstUG2.htm)|Somnalu Oculus|
|[IO7sPevkM0zvnWbi.htm](monsters-of-myth-bestiary/IO7sPevkM0zvnWbi.htm)|Taljjae (The Hero)|
|[j3NhCEMLVm4HSsgr.htm](monsters-of-myth-bestiary/j3NhCEMLVm4HSsgr.htm)|Taljjae (The Nobleman)|
|[jInkcz2UYdaCzsBU.htm](monsters-of-myth-bestiary/jInkcz2UYdaCzsBU.htm)|Ulgrem-Axaan|
|[JX7k6wFmZLqNjK0k.htm](monsters-of-myth-bestiary/JX7k6wFmZLqNjK0k.htm)|Ulgrem-Lurann|
|[K7hgO9SEBai2jwYj.htm](monsters-of-myth-bestiary/K7hgO9SEBai2jwYj.htm)|Krampus Celebrant|
|[kdkmTLlqzOYVxb50.htm](monsters-of-myth-bestiary/kdkmTLlqzOYVxb50.htm)|Krampus (The Horned Miser)|
|[KdOwmclPVpcTSPjF.htm](monsters-of-myth-bestiary/KdOwmclPVpcTSPjF.htm)|Mosquito Witch|
|[KeKlBbO3hfT5mzqO.htm](monsters-of-myth-bestiary/KeKlBbO3hfT5mzqO.htm)|Storm Discharge|
|[KLxdUFCvZ7HkAP1E.htm](monsters-of-myth-bestiary/KLxdUFCvZ7HkAP1E.htm)|Somnalu|
|[LK3SgIKJqqo4Q4NQ.htm](monsters-of-myth-bestiary/LK3SgIKJqqo4Q4NQ.htm)|Cuetzmonquali|
|[mE9MQ0hnRmlR9m94.htm](monsters-of-myth-bestiary/mE9MQ0hnRmlR9m94.htm)|Mosquito Witch (The Swarm Seer)|
|[N3OJ0wxik4wEgTg7.htm](monsters-of-myth-bestiary/N3OJ0wxik4wEgTg7.htm)|Imperfect Automaton|
|[ns6fmJ8469hzBOtM.htm](monsters-of-myth-bestiary/ns6fmJ8469hzBOtM.htm)|Mosquito Witch (The Hemoprophet)|
|[O4gEaG5vzkLFl7J1.htm](monsters-of-myth-bestiary/O4gEaG5vzkLFl7J1.htm)|Grogrisant|
|[OF0fZDjNapydddM1.htm](monsters-of-myth-bestiary/OF0fZDjNapydddM1.htm)|Kuworsys|
|[OLgKekTU0LOYwoxd.htm](monsters-of-myth-bestiary/OLgKekTU0LOYwoxd.htm)|Young Linnorm|
|[qcdNzMiO3XxPHLPJ.htm](monsters-of-myth-bestiary/qcdNzMiO3XxPHLPJ.htm)|Howling Spawn|
|[RqdL0YzNF1dG163i.htm](monsters-of-myth-bestiary/RqdL0YzNF1dG163i.htm)|Kothogaz, Dance Of Disharmony|
|[sXZExyaijbRnKI2S.htm](monsters-of-myth-bestiary/sXZExyaijbRnKI2S.htm)|Taljjae (The Grandmother)|
|[VCC4nw6EXvqwgmCp.htm](monsters-of-myth-bestiary/VCC4nw6EXvqwgmCp.htm)|Ainamuuren|
|[vI7wAKNDX05DShWy.htm](monsters-of-myth-bestiary/vI7wAKNDX05DShWy.htm)|Quaking Slither|
|[W0oF6Dl7G4UeTBSC.htm](monsters-of-myth-bestiary/W0oF6Dl7G4UeTBSC.htm)|Taljjae (The General)|
|[WdnuPiAoRFaymR8R.htm](monsters-of-myth-bestiary/WdnuPiAoRFaymR8R.htm)|Fafnheir|
|[XCW1WzJqwGUS1cGi.htm](monsters-of-myth-bestiary/XCW1WzJqwGUS1cGi.htm)|Ulistul|
|[XLDh6o6vBRCDEKsl.htm](monsters-of-myth-bestiary/XLDh6o6vBRCDEKsl.htm)|Taljjae (The Hermit)|
|[XwBFT7VO9M7M6AgQ.htm](monsters-of-myth-bestiary/XwBFT7VO9M7M6AgQ.htm)|Planar Tear|
|[zE2ykzHZKNS3Rnc3.htm](monsters-of-myth-bestiary/zE2ykzHZKNS3Rnc3.htm)|Kallas Devil|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
