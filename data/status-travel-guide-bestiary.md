# État de la traduction (travel-guide-bestiary)

 * **libre**: 5


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[FMGyuNpkxMRR1Jrw.htm](travel-guide-bestiary/FMGyuNpkxMRR1Jrw.htm)|Sarkorian Wolf|Loup Sarkorien|libre|
|[mpT2AbFjoyDBDN5l.htm](travel-guide-bestiary/mpT2AbFjoyDBDN5l.htm)|Baccali Alpaca|Alpaga Baccali|libre|
|[MQBH5svYUyhd3RsK.htm](travel-guide-bestiary/MQBH5svYUyhd3RsK.htm)|Whalesteed|Dauphin de selle|libre|
|[Oefb1ZCYp2DolpZr.htm](travel-guide-bestiary/Oefb1ZCYp2DolpZr.htm)|Irriseni Owlbear|Hibours irriseni|libre|
|[yRhCsQsoz1Uqvkmk.htm](travel-guide-bestiary/yRhCsQsoz1Uqvkmk.htm)|Vulture Rat|Rat vautour|libre|
