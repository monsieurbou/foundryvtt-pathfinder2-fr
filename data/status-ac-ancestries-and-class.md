# État de la traduction (ac-ancestries-and-class)

 * **libre**: 60


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0BVoHy7R86KJoani.htm](ac-ancestries-and-class/0BVoHy7R86KJoani.htm)|Monitor Lizard|Varan|libre|
|[0MAsdLGCLSM2W3h0.htm](ac-ancestries-and-class/0MAsdLGCLSM2W3h0.htm)|Oozeform Chair|Fauteuil de forme de vase|libre|
|[2DHIdNZsWW0QMktP.htm](ac-ancestries-and-class/2DHIdNZsWW0QMktP.htm)|Legchair|Chaise sur pattes|libre|
|[2nVLooSZdV9tJw9E.htm](ac-ancestries-and-class/2nVLooSZdV9tJw9E.htm)|Ghost|Fantôme|libre|
|[3ubDMtiqlPI1Xcu8.htm](ac-ancestries-and-class/3ubDMtiqlPI1Xcu8.htm)|Goat|Chèvre|libre|
|[5mO4H9Etz6OZDXDp.htm](ac-ancestries-and-class/5mO4H9Etz6OZDXDp.htm)|Bird|Oiseau de proie|libre|
|[6dNZ1K8X9OgK8aCL.htm](ac-ancestries-and-class/6dNZ1K8X9OgK8aCL.htm)|Rhinoceros|Rhinocéros|libre|
|[6r0IkniXbYu0bPdi.htm](ac-ancestries-and-class/6r0IkniXbYu0bPdi.htm)|Pangolin|Pangolin|libre|
|[8an4Nsuh2Md5wWHI.htm](ac-ancestries-and-class/8an4Nsuh2Md5wWHI.htm)|Undead Hand|Main morte-vivante|libre|
|[8gSeDqJLLQRlQWis.htm](ac-ancestries-and-class/8gSeDqJLLQRlQWis.htm)|Horse|Cheval|libre|
|[8Y7gGICWeeiLARjj.htm](ac-ancestries-and-class/8Y7gGICWeeiLARjj.htm)|Rootball Chair|Fauteuil motte de racines|libre|
|[8z71PBlZBSXkKkLm.htm](ac-ancestries-and-class/8z71PBlZBSXkKkLm.htm)|Cave Gecko|Gecko des cavernes|libre|
|[9T6H4GbqxMm44Acv.htm](ac-ancestries-and-class/9T6H4GbqxMm44Acv.htm)|Crocodile|Crocodile|libre|
|[a9luOko3LqrhIe2k.htm](ac-ancestries-and-class/a9luOko3LqrhIe2k.htm)|Vulture|Vautour|libre|
|[BPo2AA9M7OBqJIYY.htm](ac-ancestries-and-class/BPo2AA9M7OBqJIYY.htm)|Metal Elemental|Élémentaire de métal|libre|
|[dUAqUKIdieoFVHRp.htm](ac-ancestries-and-class/dUAqUKIdieoFVHRp.htm)|Fiery Leopard|Léopard brûlant|libre|
|[DUnwMSEutz35cCvB.htm](ac-ancestries-and-class/DUnwMSEutz35cCvB.htm)|Skeletal Mount|Monture squelettique|libre|
|[eBgMfYf0PVbsGOYp.htm](ac-ancestries-and-class/eBgMfYf0PVbsGOYp.htm)|Bear|Ours|libre|
|[efkFqs6LXzWkawfL.htm](ac-ancestries-and-class/efkFqs6LXzWkawfL.htm)|Hyena|Hyène|libre|
|[eVcgERbL1hyeqCaW.htm](ac-ancestries-and-class/eVcgERbL1hyeqCaW.htm)|Skeletal Servant|Serviteur squelettique|libre|
|[hynwj4kRnb7yLNGu.htm](ac-ancestries-and-class/hynwj4kRnb7yLNGu.htm)|Vampiric Animal|Animal vampirique|libre|
|[i2uYnClqjYI8YVun.htm](ac-ancestries-and-class/i2uYnClqjYI8YVun.htm)|Elephant|Éléphant|libre|
|[ikbae8cDXga00PbG.htm](ac-ancestries-and-class/ikbae8cDXga00PbG.htm)|Zombie Carrion Bird|Oiseau charognard zombie|libre|
|[iv5aYjofYAabH5c0.htm](ac-ancestries-and-class/iv5aYjofYAabH5c0.htm)|Draft Lizard|Lézard de monte|libre|
|[jEK6PPGWmaTFAEKr.htm](ac-ancestries-and-class/jEK6PPGWmaTFAEKr.htm)|Camel|Chameau|libre|
|[jTRsRVhl5NswEueA.htm](ac-ancestries-and-class/jTRsRVhl5NswEueA.htm)|Boar|Sanglier|libre|
|[jUYTm3wUR0GX1zfO.htm](ac-ancestries-and-class/jUYTm3wUR0GX1zfO.htm)|Ulgrem-Lurann|Ulgrem-Lurann|libre|
|[kaE0e2oLPj4S3SdA.htm](ac-ancestries-and-class/kaE0e2oLPj4S3SdA.htm)|Blood Wolf|Loup de sang|libre|
|[kbPrh3KBomvQcLOn.htm](ac-ancestries-and-class/kbPrh3KBomvQcLOn.htm)|Shadow Hound|Molosse d'ombre|libre|
|[kKbXAP5Vk6iYC98b.htm](ac-ancestries-and-class/kKbXAP5Vk6iYC98b.htm)|Dromaeosaur|Droméosaure|libre|
|[KMHnOhuO57vnUHZH.htm](ac-ancestries-and-class/KMHnOhuO57vnUHZH.htm)|Water Wraith|Âme-en-peine aquatique|libre|
|[LnkdFqJYKkpFxSxt.htm](ac-ancestries-and-class/LnkdFqJYKkpFxSxt.htm)|Earth Elemental|Élémentaire de terre|libre|
|[lOMF4UyhjZcL7HM9.htm](ac-ancestries-and-class/lOMF4UyhjZcL7HM9.htm)|Badger|Blaireau|libre|
|[lOzvLCCy9QRnYw6w.htm](ac-ancestries-and-class/lOzvLCCy9QRnYw6w.htm)|Cat|Félin|libre|
|[mdbWMaCPpShOKrmR.htm](ac-ancestries-and-class/mdbWMaCPpShOKrmR.htm)|Water Elemental|Élémentaire d'eau|libre|
|[Mld98WoM7R1wOQtX.htm](ac-ancestries-and-class/Mld98WoM7R1wOQtX.htm)|Zombie Mount|Monture zombie|libre|
|[MpqwqLf7AaLOfyZh.htm](ac-ancestries-and-class/MpqwqLf7AaLOfyZh.htm)|Snake|Serpent|libre|
|[Nab2afki863WAtBG.htm](ac-ancestries-and-class/Nab2afki863WAtBG.htm)|Tyrannosaurus|Tyrannosaure|libre|
|[nOaN1YmK9fnfRNPw.htm](ac-ancestries-and-class/nOaN1YmK9fnfRNPw.htm)|Fire Elemental|Élémentaire de feu|libre|
|[O50K7MUf0Xw8wHd6.htm](ac-ancestries-and-class/O50K7MUf0Xw8wHd6.htm)|Cave Pterosaur|Ptérosaure des cavernes|libre|
|[oB8ylf56HRtXE9yq.htm](ac-ancestries-and-class/oB8ylf56HRtXE9yq.htm)|Shark|Squale|libre|
|[pAiHiUC9NfbndLNG.htm](ac-ancestries-and-class/pAiHiUC9NfbndLNG.htm)|Riding Drake|Drake de monte|libre|
|[QJghnxvSitRl5ubE.htm](ac-ancestries-and-class/QJghnxvSitRl5ubE.htm)|Gibtas|Gibtas|libre|
|[qLvwY3aKGHVrbgfk.htm](ac-ancestries-and-class/qLvwY3aKGHVrbgfk.htm)|Ape|Singe|libre|
|[Qwm9g6hS7f45sAIj.htm](ac-ancestries-and-class/Qwm9g6hS7f45sAIj.htm)|Bat|Chauve-souris|libre|
|[S0VK8tj0rOTR2MIe.htm](ac-ancestries-and-class/S0VK8tj0rOTR2MIe.htm)|Wood Elemental|Élémentaire de bois|libre|
|[SMYGh6WPjKIMAmXc.htm](ac-ancestries-and-class/SMYGh6WPjKIMAmXc.htm)|Moth|Papillon de nuit|libre|
|[tT2lEqAPTM2u1hNV.htm](ac-ancestries-and-class/tT2lEqAPTM2u1hNV.htm)|Scorpion|Scorpion|libre|
|[UwzROlMRrkPDrWjk.htm](ac-ancestries-and-class/UwzROlMRrkPDrWjk.htm)|Triceratops|Tricératops|libre|
|[vbBkKVl9tgnVasFU.htm](ac-ancestries-and-class/vbBkKVl9tgnVasFU.htm)|Wolf|Loup|libre|
|[vvlTOzxghrlewzB0.htm](ac-ancestries-and-class/vvlTOzxghrlewzB0.htm)|Skeletal Bird of Prey|Oiseau de proie squelettique|libre|
|[wcS3dKMoIxc77rtp.htm](ac-ancestries-and-class/wcS3dKMoIxc77rtp.htm)|Terror Bird|Oiseau de terreur|libre|
|[WthugGo14WNxa3L5.htm](ac-ancestries-and-class/WthugGo14WNxa3L5.htm)|Skeletal Constrictor|Serpent constricteur squelettique|libre|
|[WwQVAQTnry5vfGbC.htm](ac-ancestries-and-class/WwQVAQTnry5vfGbC.htm)|Ankylosaurus|Ankylosaure|libre|
|[wZlIF2VpF2OO3hQR.htm](ac-ancestries-and-class/wZlIF2VpF2OO3hQR.htm)|Capybara|Capybara|libre|
|[X9YDnJdJfEPDk7l2.htm](ac-ancestries-and-class/X9YDnJdJfEPDk7l2.htm)|Arboreal Sapling|Jeune arboréen|libre|
|[xiSoZz2JHX095nHo.htm](ac-ancestries-and-class/xiSoZz2JHX095nHo.htm)|Zombie|Zombie|libre|
|[xX4zQzrt9JPv4ox1.htm](ac-ancestries-and-class/xX4zQzrt9JPv4ox1.htm)|Augdunar|Augdunar|libre|
|[Yixn2Jz0f3eT1Wkt.htm](ac-ancestries-and-class/Yixn2Jz0f3eT1Wkt.htm)|Beetle|Scarabée|libre|
|[yukG72FSivsrqVoG.htm](ac-ancestries-and-class/yukG72FSivsrqVoG.htm)|Air Elemental|Éléméntaire d'air|libre|
