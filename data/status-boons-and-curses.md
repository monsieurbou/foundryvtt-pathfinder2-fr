# État de la traduction (boons-and-curses)

 * **libre**: 219
 * **changé**: 21


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[10F2EKASFp9bjsjF.htm](boons-and-curses/10F2EKASFp9bjsjF.htm)|Shizuru - Moderate Boon|Shizuru - Grâce modérée|changé|
|[2i4AQ2tHph54SzRD.htm](boons-and-curses/2i4AQ2tHph54SzRD.htm)|Casandalee - Major Boon|Casandalee - Grâce majeure|changé|
|[9EMaYf6odFEyjdSr.htm](boons-and-curses/9EMaYf6odFEyjdSr.htm)|Urgathoa - Moderate Curse|Urgathoa - Malédiction modérée|changé|
|[CmHQGVyNZ7aOmMcd.htm](boons-and-curses/CmHQGVyNZ7aOmMcd.htm)|Nethys - Moderate Curse|Néthys - Malédiction modérée|changé|
|[ctcZm0WGiBKVWVYd.htm](boons-and-curses/ctcZm0WGiBKVWVYd.htm)|Alseta - Minor Boon|Alséta - Grâce mineure|changé|
|[dHcGihuimtCf1NOs.htm](boons-and-curses/dHcGihuimtCf1NOs.htm)|Iomedae - Major Boon|Iomédae - Grâce majeure|changé|
|[J0aztZRSjNjJzwGy.htm](boons-and-curses/J0aztZRSjNjJzwGy.htm)|Arazni - Major Curse|Arazni - Malédiction majeure|changé|
|[jVeuBZsa4iFg0wC6.htm](boons-and-curses/jVeuBZsa4iFg0wC6.htm)|Nethys - Minor Boon|Néthys - Grâce mineure|changé|
|[kCCrjskDIbG0e74O.htm](boons-and-curses/kCCrjskDIbG0e74O.htm)|Irori - Major Boon|Irori - Grâce majeure|changé|
|[KuNcNP42LxehKnaV.htm](boons-and-curses/KuNcNP42LxehKnaV.htm)|Shizuru - Minor Boon|Shizuru - Grâce mineure|changé|
|[LOJVm2Tt4K2XdVLs.htm](boons-and-curses/LOJVm2Tt4K2XdVLs.htm)|Milani - Major Boon|Milani - Grâce majeure|changé|
|[MLmgyIZRJX1hfaPR.htm](boons-and-curses/MLmgyIZRJX1hfaPR.htm)|Nivi Rhombodazzle - Major Boon|Nivi Rhombéblouissante - Grâce majeure|changé|
|[N29LCwYfRWHtEqrS.htm](boons-and-curses/N29LCwYfRWHtEqrS.htm)|Urgathoa - Moderate Boon|Urgathoa - Grâce modérée|changé|
|[nFrCY6tT2B8uxaO3.htm](boons-and-curses/nFrCY6tT2B8uxaO3.htm)|Erastil - Major Boon|Érastil - Grâce majeure|changé|
|[OaUt41v2OrQkHpM4.htm](boons-and-curses/OaUt41v2OrQkHpM4.htm)|Abadar - Major Curse|Abadar - Malédiction majeure|changé|
|[oqM1fFyHNigCNBsf.htm](boons-and-curses/oqM1fFyHNigCNBsf.htm)|Milani - Moderate Curse|Milani - Malédiction modérée|changé|
|[QuNXNPHvh4DBmyZH.htm](boons-and-curses/QuNXNPHvh4DBmyZH.htm)|Kurgess - Major Curse|Kurgess - Malédiction majeure|changé|
|[sUi427H7MJnbiMxh.htm](boons-and-curses/sUi427H7MJnbiMxh.htm)|Hei Feng - Major Curse|Hei Feng - Malédiction majeure|changé|
|[uM9dzDmXfnpYCCrc.htm](boons-and-curses/uM9dzDmXfnpYCCrc.htm)|Casandalee - Moderate Boon|Casandalee - Grâce modérée|changé|
|[WWyPOtmJc6lDLfgL.htm](boons-and-curses/WWyPOtmJc6lDLfgL.htm)|Groetus - Major Boon|Groétus - Grâce majeure|changé|
|[xrSgGobm5QtTcABQ.htm](boons-and-curses/xrSgGobm5QtTcABQ.htm)|Pharasma - Moderate Boon|Pharasma - Grâce modérée|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[02FtcdSA8tjFNmvD.htm](boons-and-curses/02FtcdSA8tjFNmvD.htm)|Hei Feng - Major Boon|Hei Feng - Grâce majeure|libre|
|[0bM6m3Gx8Th3NxXp.htm](boons-and-curses/0bM6m3Gx8Th3NxXp.htm)|Nocticula - Minor Boon|Nocticula - Grâce mineure|libre|
|[0hE3yCBhmReWtZGA.htm](boons-and-curses/0hE3yCBhmReWtZGA.htm)|Ghlaunder - Minor Boon|Ghlaunder - Grâce mineure|libre|
|[0JDLq5rMOjUskRTG.htm](boons-and-curses/0JDLq5rMOjUskRTG.htm)|Besmara - Moderate Curse|Besmara - Malédiction modérée|libre|
|[0kOGxG08y4bGgLto.htm](boons-and-curses/0kOGxG08y4bGgLto.htm)|Groetus - Major Curse|Groétus - Malédiction majeure|libre|
|[0PMqydlsIjj8GNnl.htm](boons-and-curses/0PMqydlsIjj8GNnl.htm)|Norgorber - Moderate Curse|Norgorber - Malédiction modérée|libre|
|[0vmwZrIhm5rWHkYh.htm](boons-and-curses/0vmwZrIhm5rWHkYh.htm)|Pharasma - Minor Boon|Pharasma - Grâce mineure|libre|
|[1abspMhs13VKLklY.htm](boons-and-curses/1abspMhs13VKLklY.htm)|Arazni - Moderate Curse|Arazni - Malédiction modérée|libre|
|[1hdWNyedYvuZgtPr.htm](boons-and-curses/1hdWNyedYvuZgtPr.htm)|Pharasma - Moderate Curse|Pharasma - Malédiction modérée|libre|
|[1J5gmMhFP0Ul9o64.htm](boons-and-curses/1J5gmMhFP0Ul9o64.htm)|Besmara - Major Curse|Besmara - Malédiction majeure|libre|
|[1nALC8yWtjBDEaOC.htm](boons-and-curses/1nALC8yWtjBDEaOC.htm)|Rovagug - Minor Curse|Rovagug - Malédiction mineure|libre|
|[1QtjogbV1MsJp7x5.htm](boons-and-curses/1QtjogbV1MsJp7x5.htm)|Achaekek - Minor Curse|Achaékek - Malédiction mineure|libre|
|[1WPIDGaeRd76EXm1.htm](boons-and-curses/1WPIDGaeRd76EXm1.htm)|Chaldira - Major Boon|Chaldira - Grâce majeure|libre|
|[2EKgNifq3ozzKYfI.htm](boons-and-curses/2EKgNifq3ozzKYfI.htm)|Norgorber - Minor Boon|Norgorber - Grâce mineure|libre|
|[30Xdyvplx7MfX1nA.htm](boons-and-curses/30Xdyvplx7MfX1nA.htm)|Torag - Moderate Boon|Torag - Grâce modérée|libre|
|[3hZjK8dKmjWUyotV.htm](boons-and-curses/3hZjK8dKmjWUyotV.htm)|Nivi Rhombodazzle - Minor Curse|Nivi Rhombéblouissante - Malédiction mineure|libre|
|[3u3vav6qfAW6hPKE.htm](boons-and-curses/3u3vav6qfAW6hPKE.htm)|Erastil - Moderate Curse|Érastil - Malédiction modérée|libre|
|[496UIvkjyrae8xzb.htm](boons-and-curses/496UIvkjyrae8xzb.htm)|Casandalee - Moderate Curse|Casandalee - Malédiction modérée|libre|
|[4FkVB4uQK4eHZJ6Z.htm](boons-and-curses/4FkVB4uQK4eHZJ6Z.htm)|Rovagug - Minor Boon|Rovagug - Grâce mineure|libre|
|[4gVIjPeKV6ss1atA.htm](boons-and-curses/4gVIjPeKV6ss1atA.htm)|Brigh - Major Curse|Brigh - Malédiction majeure|libre|
|[4m2dQ2fvVWxtokVe.htm](boons-and-curses/4m2dQ2fvVWxtokVe.htm)|Desna - Moderate Boon|Desna - Grâce modérée|libre|
|[4sZ9tpmPj4LIgPvU.htm](boons-and-curses/4sZ9tpmPj4LIgPvU.htm)|Nethys - Major Curse|Néthys - Malédiction majeure|libre|
|[5x0wEbgxYmXonBuN.htm](boons-and-curses/5x0wEbgxYmXonBuN.htm)|Lamashtu - Minor Curse|Lamashtu - Malédiction mineure|libre|
|[6CLe2ZOkLSi27S2Z.htm](boons-and-curses/6CLe2ZOkLSi27S2Z.htm)|Sivanah - Moderate Curse|Sivanah - Malédiction modérée|libre|
|[6Cpm04jBSzSwe2oC.htm](boons-and-curses/6Cpm04jBSzSwe2oC.htm)|Nocticula - Moderate Curse|Nocticula - Malédiction modérée|libre|
|[6f8zTNIs5XXzkhkR.htm](boons-and-curses/6f8zTNIs5XXzkhkR.htm)|Lamashtu - Minor Boon|Lamashtu - Grâce mineure|libre|
|[6HcZezAVWArSonu0.htm](boons-and-curses/6HcZezAVWArSonu0.htm)|Chaldira - Moderate Boon|Chaldira - Grâce modérée|libre|
|[6Iz9b01O5t31ZioP.htm](boons-and-curses/6Iz9b01O5t31ZioP.htm)|Tsukiyo - Minor Curse|Tsukiyo - Malédiction mineure|libre|
|[7HOxIB1abpVRUjY9.htm](boons-and-curses/7HOxIB1abpVRUjY9.htm)|Grandmother Spider - Minor Boon|Grand-mère Araignée - Grâce mineure|libre|
|[7Ky13a2fl5KUuaMd.htm](boons-and-curses/7Ky13a2fl5KUuaMd.htm)|Milani - Minor Boon|Milani - Grâce mineure|libre|
|[845vvISfgkk6sei0.htm](boons-and-curses/845vvISfgkk6sei0.htm)|Torag - Minor Curse|Torag - Malédiction mineure|libre|
|[8Be5VeyjWRZUdxde.htm](boons-and-curses/8Be5VeyjWRZUdxde.htm)|Nivi Rhombodazzle - Minor Boon|Nivi Rhombéblouissante - Grâce mineure|libre|
|[8G0USrxM7d4B4EVI.htm](boons-and-curses/8G0USrxM7d4B4EVI.htm)|Gorum - Major Boon|Gorum - Grâce majeure|libre|
|[8GElMYCPjhK5uHAj.htm](boons-and-curses/8GElMYCPjhK5uHAj.htm)|Nivi Rhombodazzle - Moderate Curse|Nivi Rhombéblouissante - Malédiction modérée|libre|
|[8GyJK2tNVMIuV8Wa.htm](boons-and-curses/8GyJK2tNVMIuV8Wa.htm)|Chaldira - Moderate Curse|Chaldira - Malédiction modérée|libre|
|[8jHjxcB33N4BULE1.htm](boons-and-curses/8jHjxcB33N4BULE1.htm)|Casandalee - Minor Curse|Casandalee - Malédiction mineure|libre|
|[8x308UNvYYDuIHXH.htm](boons-and-curses/8x308UNvYYDuIHXH.htm)|Nocticula - Moderate Boon|Nocticula - Grâce modérée|libre|
|[8ZNtiMxoJIP1DJ9Q.htm](boons-and-curses/8ZNtiMxoJIP1DJ9Q.htm)|Nethys - Major Boon|Néthys - Grâce majeure|libre|
|[9jFdhTtL8zElWdRC.htm](boons-and-curses/9jFdhTtL8zElWdRC.htm)|Groetus - Moderate Boon|Groétus - Grâce modérée|libre|
|[9jxwttnEfxrOaCsY.htm](boons-and-curses/9jxwttnEfxrOaCsY.htm)|Gruhastha - Major Boon|Gruhastha - Grâce majeure|libre|
|[9nCMvbz3e71AgsoW.htm](boons-and-curses/9nCMvbz3e71AgsoW.htm)|Besmara - Minor Boon|Besmara - Grâce mineure|libre|
|[9neKaRaRi9ekttts.htm](boons-and-curses/9neKaRaRi9ekttts.htm)|Chaldira - Major Curse|Chaldira - Malédiction majeure|libre|
|[9yABJNudAI1IvifR.htm](boons-and-curses/9yABJNudAI1IvifR.htm)|Kurgess - Minor Boon|Kurgess - Grâce mineure|libre|
|[a1OZtMQSzjYOm0P3.htm](boons-and-curses/a1OZtMQSzjYOm0P3.htm)|Rovagug - Moderate Boon|Rovagug - Grâce modérée|libre|
|[A8HRlQB7reEHp50k.htm](boons-and-curses/A8HRlQB7reEHp50k.htm)|Nivi Rhombodazzle - Moderate Boon|Nivi Rhombéblouissante - Grâce modérée|libre|
|[AdNAeWhnq5Is3AZb.htm](boons-and-curses/AdNAeWhnq5Is3AZb.htm)|Torag - Major Boon|Torag - Grâce majeure|libre|
|[AfuH02H6ib4KYS6C.htm](boons-and-curses/AfuH02H6ib4KYS6C.htm)|Torag - Moderate Curse|Torag - Malédiction modérée|libre|
|[AJj2o3uWKH3ARyFr.htm](boons-and-curses/AJj2o3uWKH3ARyFr.htm)|Casandalee - Major Curse|Casandalee - Malédiction majeure|libre|
|[AOBhF0grlwKJSuxi.htm](boons-and-curses/AOBhF0grlwKJSuxi.htm)|Hei Feng - Minor Boon|Hei Feng - Grâce mineure|libre|
|[AodAErjWnvGZaL4M.htm](boons-and-curses/AodAErjWnvGZaL4M.htm)|Sivanah - Moderate Boon|Sivanah - Grâce modérée|libre|
|[AohWtOzgk2Qr9ADl.htm](boons-and-curses/AohWtOzgk2Qr9ADl.htm)|Gozreh - Moderate Curse|Gozreh - Malédiction modérée|libre|
|[aOVXhHVkLw9dLCdR.htm](boons-and-curses/aOVXhHVkLw9dLCdR.htm)|Zon-Kuthon - Moderate Curse|Zon-Kuthon - Malédiction modérée|libre|
|[auoQ66gY4r7Tk3lc.htm](boons-and-curses/auoQ66gY4r7Tk3lc.htm)|Alseta - Major Boon|Alséta - Grâce majeure|libre|
|[aV1I9VmMQBaFU9M9.htm](boons-and-curses/aV1I9VmMQBaFU9M9.htm)|Asmodeus - Moderate Boon|Asmodéus - Grâce modérée|libre|
|[AvjRCVzLmxapFOLV.htm](boons-and-curses/AvjRCVzLmxapFOLV.htm)|Gozreh - Moderate Boon|Gozreh - Grâce modérée|libre|
|[AySifuRSqWzDkCDt.htm](boons-and-curses/AySifuRSqWzDkCDt.htm)|Sivanah - Major Curse|Sivanah - Malédiction majeure|libre|
|[B69aBTRn2BwSo23z.htm](boons-and-curses/B69aBTRn2BwSo23z.htm)|Nocticula - Minor Curse|Nocticula - Malédiction mineure|libre|
|[BdV4uubwrn0KgD8H.htm](boons-and-curses/BdV4uubwrn0KgD8H.htm)|Alseta - Moderate Curse|Alséta - Malédiction modérée|libre|
|[BKZRjVVt7hkvEpk3.htm](boons-and-curses/BKZRjVVt7hkvEpk3.htm)|Shizuru - Moderate Curse|Shizuru - Malédiction modérée|libre|
|[bvBzvLDKM5sAJm9s.htm](boons-and-curses/bvBzvLDKM5sAJm9s.htm)|Achaekek - Minor Boon|Achaékek - Grâce mineure|libre|
|[c2wwILmpgyXIjlfe.htm](boons-and-curses/c2wwILmpgyXIjlfe.htm)|Torag - Major Curse|Torag - Malédiction majeure|libre|
|[C6Ercv1ugcTsbVA9.htm](boons-and-curses/C6Ercv1ugcTsbVA9.htm)|Erastil - Major Curse|Érastil - Malédiction majeure|libre|
|[Ca8ZQssvIgH6WEwZ.htm](boons-and-curses/Ca8ZQssvIgH6WEwZ.htm)|Casandalee - Minor Boon|Casandalee - Grâce mineure|libre|
|[CboMubDaWRkG94Ff.htm](boons-and-curses/CboMubDaWRkG94Ff.htm)|Hei Feng - Minor Curse|Hei Feng - Malédiction mineure|libre|
|[cFz09CWKzff2cC73.htm](boons-and-curses/cFz09CWKzff2cC73.htm)|Rovagug - Major Boon|Rovagug - Grâce majeure|libre|
|[CjRUOscsOwCmai8D.htm](boons-and-curses/CjRUOscsOwCmai8D.htm)|Desna - Moderate Curse|Desna - Malédiction modérée|libre|
|[CL8lLQJWd4N89QIm.htm](boons-and-curses/CL8lLQJWd4N89QIm.htm)|Erastil - Moderate Boon|Érastil - Grâce modérée|libre|
|[COLF56taxqleYkcZ.htm](boons-and-curses/COLF56taxqleYkcZ.htm)|Groetus - Minor Boon|Groétus - Grâce mineure|libre|
|[d198p8UBPK3VcKi9.htm](boons-and-curses/d198p8UBPK3VcKi9.htm)|Rovagug - Moderate Curse|Rovagug - Malédiction modérée|libre|
|[d2mkwn858zIqhLS7.htm](boons-and-curses/d2mkwn858zIqhLS7.htm)|Ghlaunder - Major Boon|Ghlaunder - Grâce	majeure|libre|
|[d84VbnhyQ77abLKF.htm](boons-and-curses/d84VbnhyQ77abLKF.htm)|Tsukiyo - Minor Boon|Tsukiyo - Grâce mineure|libre|
|[dFWAMWqnioz6RZxc.htm](boons-and-curses/dFWAMWqnioz6RZxc.htm)|Grandmother Spider - Major Boon|Grand-mère Araignée - Grâce majeure|libre|
|[DfXlr5qqbcjJN3gh.htm](boons-and-curses/DfXlr5qqbcjJN3gh.htm)|Abadar - Moderate Boon|Abadar - Grâce modérée|libre|
|[DjVTszddTHSo9fkZ.htm](boons-and-curses/DjVTszddTHSo9fkZ.htm)|Nivi Rhombodazzle - Major Curse|Nivi Rhombéblouissante - Malédiction majeure|libre|
|[DJxDsRXhqXvABYzK.htm](boons-and-curses/DJxDsRXhqXvABYzK.htm)|Sivanah - Minor Boon|Sivanah - Grâce mineure|libre|
|[dL6r0VfFIWlAazDW.htm](boons-and-curses/dL6r0VfFIWlAazDW.htm)|Sarenrae - Major Boon|Sarenraë - Grâce majeure|libre|
|[dLudHCFcj4p7KG3j.htm](boons-and-curses/dLudHCFcj4p7KG3j.htm)|Asmodeus - Minor Curse|Asmodéus - Malédiction mineure|libre|
|[DnlAmlrrKv3jwaOq.htm](boons-and-curses/DnlAmlrrKv3jwaOq.htm)|Pharasma - Major Curse|Pharasma - Malédiction majeure|libre|
|[dRk4kWmtau9PLQAk.htm](boons-and-curses/dRk4kWmtau9PLQAk.htm)|Chaldira - Minor Curse|Chaldira - Malédiction mineure|libre|
|[DSfHCm8PDL5hLzZ0.htm](boons-and-curses/DSfHCm8PDL5hLzZ0.htm)|Irori - Minor Curse|Irori - Malédiction mineure|libre|
|[dTJP2Hg4ZJu4Ck2y.htm](boons-and-curses/dTJP2Hg4ZJu4Ck2y.htm)|Iomedae - Minor Boon|Iomédae - Grâce mineure|libre|
|[DuGA57MDiwEKp7Y1.htm](boons-and-curses/DuGA57MDiwEKp7Y1.htm)|Sarenrae - Major Curse|Sarenrae - Malédiction majeure|libre|
|[DYEN06j9VVTOXqFK.htm](boons-and-curses/DYEN06j9VVTOXqFK.htm)|Besmara - Minor Curse|Besmare - Malédiction mineure|libre|
|[dZU3HdI2oO8LFjGq.htm](boons-and-curses/dZU3HdI2oO8LFjGq.htm)|Urgathoa - Major Boon|Urgathoa - Grâce majeure|libre|
|[e7Y4jKHQ3ptbSuxc.htm](boons-and-curses/e7Y4jKHQ3ptbSuxc.htm)|Torag - Minor Boon|Torag - Grâce mineure|libre|
|[e9bdt8UH7ZvpRrLz.htm](boons-and-curses/e9bdt8UH7ZvpRrLz.htm)|Sarenrae - Minor Boon|Sarenrae - Grâce mineure|libre|
|[eFwCopyvVam6GiCT.htm](boons-and-curses/eFwCopyvVam6GiCT.htm)|Achaekek - Major Boon|Achaékek - Grâce majeure|libre|
|[eibN2Uf0dsHCU5rE.htm](boons-and-curses/eibN2Uf0dsHCU5rE.htm)|Zon-Kuthon - Major Boon|Zon-Kuthon - Grâce majeure|libre|
|[eVRjDKt0N0qSmpCn.htm](boons-and-curses/eVRjDKt0N0qSmpCn.htm)|Urgathoa - Major Curse|Urgathoa - Malédiction majeure|libre|
|[FeCcooyIb1JDQhd7.htm](boons-and-curses/FeCcooyIb1JDQhd7.htm)|Calistria - Major Boon|Calistria - Grâce Majeure|libre|
|[ffflmrfFtpFrnVqC.htm](boons-and-curses/ffflmrfFtpFrnVqC.htm)|Kurgess - Moderate Curse|Kurgess - Malédiction modérée|libre|
|[fLBNXULLPalAKlYe.htm](boons-and-curses/fLBNXULLPalAKlYe.htm)|Cayden Cailean - Minor Curse|Cayden Cailean - Malédiction mineure|libre|
|[FRmpIgwDHeewpFkL.htm](boons-and-curses/FRmpIgwDHeewpFkL.htm)|Tsukiyo - Major Curse|Tsukiyo - Malédiction majeure|libre|
|[fsKVskUhWwMgvUaT.htm](boons-and-curses/fsKVskUhWwMgvUaT.htm)|Cayden Cailean - Moderate Boon|Cayden Cailéan - Grâce modérée|libre|
|[fV0t8xagcbqwvTpQ.htm](boons-and-curses/fV0t8xagcbqwvTpQ.htm)|Asmodeus - Minor Boon|Asmodéus - Grâce mineure|libre|
|[fV0Xa1Bd3BoWACgT.htm](boons-and-curses/fV0Xa1Bd3BoWACgT.htm)|Irori - Minor Boon|Irori - Grâce mineure|libre|
|[fV940VM5RCsNwUvA.htm](boons-and-curses/fV940VM5RCsNwUvA.htm)|Calistria - Major Curse|Calistria - Malédiction majeure|libre|
|[fXEeTSVINdWaKHhw.htm](boons-and-curses/fXEeTSVINdWaKHhw.htm)|Abadar - Minor Boon|Abadar - Grâce mineure|libre|
|[G4Y4qWEbFJXLdI2G.htm](boons-and-curses/G4Y4qWEbFJXLdI2G.htm)|Alseta - Minor Curse|Alséta - Malédiction mineure|libre|
|[gDBa72Y2jokr8Zzg.htm](boons-and-curses/gDBa72Y2jokr8Zzg.htm)|Lamashtu - Moderate Curse|Lamashtu - Malédiction modérée|libre|
|[GJdHcY56q2c3kSiA.htm](boons-and-curses/GJdHcY56q2c3kSiA.htm)|Achaekek - Major Curse|Achaékek - Malédiction majeure|libre|
|[GKyH7IQHrmmCnuop.htm](boons-and-curses/GKyH7IQHrmmCnuop.htm)|Shizuru - Major Boon|Shizuru - Grâce majeure|libre|
|[Gv05Y1IXI4RWK6YO.htm](boons-and-curses/Gv05Y1IXI4RWK6YO.htm)|Milani - Major Curse|Milani - Malédiction majeure|libre|
|[gy4ThzGHW3plFZ2a.htm](boons-and-curses/gy4ThzGHW3plFZ2a.htm)|Brigh - Minor Boon|Brigh - Grâce mineure|libre|
|[HdPVL76QiDhsLCA9.htm](boons-and-curses/HdPVL76QiDhsLCA9.htm)|Nethys - Moderate Boon|Néthys - Grâce modérée|libre|
|[hoXFKbMHeUabjW3s.htm](boons-and-curses/hoXFKbMHeUabjW3s.htm)|Sivanah - Minor Curse|Sivanah - Malédiction mineure|libre|
|[hrTl9kfSNrOQeNze.htm](boons-and-curses/hrTl9kfSNrOQeNze.htm)|Erastil - Minor Boon|Érastil - Grâce mineure|libre|
|[HrV31rAkNjV4KfCU.htm](boons-and-curses/HrV31rAkNjV4KfCU.htm)|Gorum - Minor Boon|Gorum - Grâce mineure|libre|
|[hUSknn3LYmT7eKzT.htm](boons-and-curses/hUSknn3LYmT7eKzT.htm)|Norgorber - Moderate Boon|Norgorber - Grâce modérée|libre|
|[Hvz04dGqmV8x25bw.htm](boons-and-curses/Hvz04dGqmV8x25bw.htm)|Tsukiyo - Moderate Boon|Tsukiyo - Grâce modérée|libre|
|[HXRLOkyO8F374X8R.htm](boons-and-curses/HXRLOkyO8F374X8R.htm)|Arazni - Moderate Boon|Arazni - Grâce modérée|libre|
|[ILtU0TAUZcOcwkkG.htm](boons-and-curses/ILtU0TAUZcOcwkkG.htm)|Shelyn - Major Curse|Shélyn - Malédiction majeure|libre|
|[innbfBPD1VsBIVDn.htm](boons-and-curses/innbfBPD1VsBIVDn.htm)|Ghlaunder - Moderate Boon|Ghlaunder - Grâce modérée|libre|
|[IOaepssdwTanFooc.htm](boons-and-curses/IOaepssdwTanFooc.htm)|Iomedae - Minor Curse|Iomédae - Malédiction mineure|libre|
|[iT6MnLgzuVetCzm7.htm](boons-and-curses/iT6MnLgzuVetCzm7.htm)|Brigh - Moderate Curse|Brigh - Malédiction modérée|libre|
|[jDVTrMaswJRJRTuf.htm](boons-and-curses/jDVTrMaswJRJRTuf.htm)|Besmara - Moderate Boon|Besmara - Grâce modérée|libre|
|[jfWeNqYj3rn9EysM.htm](boons-and-curses/jfWeNqYj3rn9EysM.htm)|Urgathoa - Minor Boon|Urgathoa - Grâce mineure|libre|
|[jhixjUJZlCetNRjH.htm](boons-and-curses/jhixjUJZlCetNRjH.htm)|Shelyn - Minor Curse|Shélyn - Malédiction mineure|libre|
|[jUitGTczLgoUK0rv.htm](boons-and-curses/jUitGTczLgoUK0rv.htm)|Ghlaunder - Major Curse|Ghlaunder - Malédiction majeure|libre|
|[kEAw5PMLdX2gHlwR.htm](boons-and-curses/kEAw5PMLdX2gHlwR.htm)|Gozreh - Major Curse|Gozreh - Malédiction majeure|libre|
|[KjPTFfxMAqCngQFB.htm](boons-and-curses/KjPTFfxMAqCngQFB.htm)|Nethys - Minor Curse|Néthys - Malédiction mineure|libre|
|[Ks1HaYEuSRybaq8U.htm](boons-and-curses/Ks1HaYEuSRybaq8U.htm)|Calistria - Moderate Boon|Calistria - Grâce modérée|libre|
|[kstqMWge32n0Lfdz.htm](boons-and-curses/kstqMWge32n0Lfdz.htm)|Gorum - Moderate Boon|Gorum - Grâce modérée|libre|
|[l7n8n4fAoRLIn95W.htm](boons-and-curses/l7n8n4fAoRLIn95W.htm)|Shelyn - Major Boon|Shélyn - Grâce majeure|libre|
|[La6t4fVId26PkmYk.htm](boons-and-curses/La6t4fVId26PkmYk.htm)|Cayden Cailean - Minor Boon|Cayden Cailéan - Grâce mineure|libre|
|[LfiMah3iJnnjPi29.htm](boons-and-curses/LfiMah3iJnnjPi29.htm)|Shizuru - Minor Curse|Shizuru - Malédiction mineure|libre|
|[lI7bDExxA2zQq1sP.htm](boons-and-curses/lI7bDExxA2zQq1sP.htm)|Zon-Kuthon - Minor Curse|Zon-Kuthon - Malédiction mineure|libre|
|[LMK1RXvxxD0JP0L4.htm](boons-and-curses/LMK1RXvxxD0JP0L4.htm)|Arazni - Minor Boon|Arazni - Grâce mineure|libre|
|[lOIu9jYDAR2rDe4p.htm](boons-and-curses/lOIu9jYDAR2rDe4p.htm)|Norgorber - Major Boon|Norgorber - Grâce majeure|libre|
|[LOlv11WEGUQWYKna.htm](boons-and-curses/LOlv11WEGUQWYKna.htm)|Desna - Minor Boon|Desna - Grâce mineure|libre|
|[lR9KS3pQ4gGKNspP.htm](boons-and-curses/lR9KS3pQ4gGKNspP.htm)|Gorum - Moderate Curse|Gorum - Malédiction modérée|libre|
|[lwkqTPPWeJ6olxa3.htm](boons-and-curses/lwkqTPPWeJ6olxa3.htm)|Groetus - Moderate Curse|Groétus - Malédiction modérée|libre|
|[LwXdmLWOBUIV1By0.htm](boons-and-curses/LwXdmLWOBUIV1By0.htm)|Kazutal - Moderate Boon|Kazutal - Grâce modérée|libre|
|[m33aOQij1BDcArN9.htm](boons-and-curses/m33aOQij1BDcArN9.htm)|Abadar - Major Boon|Abadar - Grâce majeure|libre|
|[M78A7uQaa4Ig8pGU.htm](boons-and-curses/M78A7uQaa4Ig8pGU.htm)|Gorum - Major Curse|Gorum - Malédiction majeure|libre|
|[MaaMu5jrfCP8w8SW.htm](boons-and-curses/MaaMu5jrfCP8w8SW.htm)|Kazutal - Moderate Curse|Kazutal - Malédiction modérée|libre|
|[McPpckNcC9hoc39a.htm](boons-and-curses/McPpckNcC9hoc39a.htm)|Hei Feng - Moderate Boon|Hei Feng - Grâce modérée|libre|
|[mcVlGufQ8rQRe61Y.htm](boons-and-curses/mcVlGufQ8rQRe61Y.htm)|Desna - Major Boon|Desna - Grâce majeure|libre|
|[MZWAF1mNNUJOzeWW.htm](boons-and-curses/MZWAF1mNNUJOzeWW.htm)|Hei Feng - Moderate Curse|Hei Feng - Malédiction modérée|libre|
|[n5p8QfZdptRZp0ii.htm](boons-and-curses/n5p8QfZdptRZp0ii.htm)|Abadar - Moderate Curse|Abadar - Malédiction modérée|libre|
|[NeAA1BHGGkRAmBDe.htm](boons-and-curses/NeAA1BHGGkRAmBDe.htm)|Abadar - Minor Curse|Abadar - Malédiction mineure|libre|
|[NgwW7Wt2gO9SEwPX.htm](boons-and-curses/NgwW7Wt2gO9SEwPX.htm)|Gorum - Minor Curse|Gorum - Malédiction mineure|libre|
|[nqOoUH4r8NEi1i2g.htm](boons-and-curses/nqOoUH4r8NEi1i2g.htm)|Calistria - Moderate Curse|Calistria - Malédiction modérée|libre|
|[NV1w0rwc7Tkhhkac.htm](boons-and-curses/NV1w0rwc7Tkhhkac.htm)|Grandmother Spider - Major Curse|Grand-mère Araignée - Malédiction majeure|libre|
|[nz77SkH0xWbcw5SY.htm](boons-and-curses/nz77SkH0xWbcw5SY.htm)|Gruhastha - Minor Curse|Gruhastha - Malédiction mineure|libre|
|[NZFTnX1xzI40q8Qr.htm](boons-and-curses/NZFTnX1xzI40q8Qr.htm)|Chaldira - Minor Boon|Chaldira - Grâce mineure|libre|
|[OV7cKDx5b50sz875.htm](boons-and-curses/OV7cKDx5b50sz875.htm)|Milani - Moderate Boon|Milani - Grâce modérée|libre|
|[P9ujY9f0o779TeEn.htm](boons-and-curses/P9ujY9f0o779TeEn.htm)|Tsukiyo - Moderate Curse|Tsukiyo - Malédiction modérée|libre|
|[ParIWsb6B4fdhrHF.htm](boons-and-curses/ParIWsb6B4fdhrHF.htm)|Pharasma - Minor Curse|Pharasma - Malédiction mineure|libre|
|[pEtqyTx6Oa0OqNKl.htm](boons-and-curses/pEtqyTx6Oa0OqNKl.htm)|Brigh - Moderate Boon|Brigh - Grâce modérée|libre|
|[pf2pAvNcz1Qcbowg.htm](boons-and-curses/pf2pAvNcz1Qcbowg.htm)|Alseta - Moderate Boon|Alséta - Grâce modérée|libre|
|[PoHos7qriDzQN7Gw.htm](boons-and-curses/PoHos7qriDzQN7Gw.htm)|Rovagug - Major Curse|Rovagug - Malédiction majeure|libre|
|[pt4dHeoDwfj8adUE.htm](boons-and-curses/pt4dHeoDwfj8adUE.htm)|Ghlaunder - Minor Curse|Ghlaunder - Malédiction mineure|libre|
|[pUtQOAKeXslaD5g3.htm](boons-and-curses/pUtQOAKeXslaD5g3.htm)|Arazni - Minor Curse|Arazni - Malédiction mineure|libre|
|[pwmFrNkLscXrDSPN.htm](boons-and-curses/pwmFrNkLscXrDSPN.htm)|Erastil - Minor Curse|Érastil - Malédiction mineure|libre|
|[py9v5HfCIzCoyC0C.htm](boons-and-curses/py9v5HfCIzCoyC0C.htm)|Lamashtu - Major Curse|Lamashtu - Malédiction majeure|libre|
|[qbo52AnXV8KDXLnL.htm](boons-and-curses/qbo52AnXV8KDXLnL.htm)|Grandmother Spider - Minor Curse|Grand-mère Araignée - Malédiction mineure|libre|
|[QGUFWuz9uF49EIhy.htm](boons-and-curses/QGUFWuz9uF49EIhy.htm)|Cayden Cailean - Major Curse|Cayden Cailéan - Malédiction majeure|libre|
|[qhVfbZCxZJS8NmB3.htm](boons-and-curses/qhVfbZCxZJS8NmB3.htm)|Irori - Moderate Curse|Irori - Malédiction modérée|libre|
|[qTmjEC528YnvPgXE.htm](boons-and-curses/qTmjEC528YnvPgXE.htm)|Iomedae - Moderate Boon|Iomédae - Grâce modérée|libre|
|[QVEkEdxCnoGxMo9l.htm](boons-and-curses/QVEkEdxCnoGxMo9l.htm)|Zon-Kuthon - Moderate Boon|Zon Kuthon - Grâce modérée|libre|
|[r4VjA1Wje3mtK2M4.htm](boons-and-curses/r4VjA1Wje3mtK2M4.htm)|Nocticula - Major Boon|Nocticula - Grâce majeure|libre|
|[r9hOxyTwN0DsrTPU.htm](boons-and-curses/r9hOxyTwN0DsrTPU.htm)|Arazni - Major Boon|Arazni - Grâce majeure|libre|
|[rA7ZM6WGnJNrWTmo.htm](boons-and-curses/rA7ZM6WGnJNrWTmo.htm)|Iomedae - Moderate Curse|Iomédae - Malédiction modérée|libre|
|[RLdclcC8zoNAVony.htm](boons-and-curses/RLdclcC8zoNAVony.htm)|Groetus - Minor Curse|Groétus - Malédiction mineure|libre|
|[rN9uF0XUMm4LPFOk.htm](boons-and-curses/rN9uF0XUMm4LPFOk.htm)|Grandmother Spider - Moderate Boon|Grand-mère Araignée - Grâce modérée|libre|
|[S1qGyGG25YImRhmS.htm](boons-and-curses/S1qGyGG25YImRhmS.htm)|Sarenrae - Moderate Boon|Sarenraë - Grâce modérée|libre|
|[SaiJX9KBN3lC0RUy.htm](boons-and-curses/SaiJX9KBN3lC0RUy.htm)|Zon-Kuthon - Major Curse|Zon-Kuthon - Malédiction majeure|libre|
|[sL6WZ4EIHcIuEwVW.htm](boons-and-curses/sL6WZ4EIHcIuEwVW.htm)|Gruhastha - Moderate Curse|Gruhastha - Malédiction modérée|libre|
|[sy5UyOvxitqX244D.htm](boons-and-curses/sy5UyOvxitqX244D.htm)|Asmodeus - Major Boon|Asmodéus - Grâce majeure|libre|
|[T2csDhA9WsNiwpd5.htm](boons-and-curses/T2csDhA9WsNiwpd5.htm)|Kazutal - Major Boon|Kazutal - Grâce majeure|libre|
|[tFnPBYcAZ0X3GbI5.htm](boons-and-curses/tFnPBYcAZ0X3GbI5.htm)|Lamashtu - Moderate Boon|Lamashtu - Grâce modérée|libre|
|[tilqIbJZLqeknTYo.htm](boons-and-curses/tilqIbJZLqeknTYo.htm)|Shelyn - Minor Boon|Shélyn - Grâce mineure|libre|
|[to4pGXcJqF1VpqBt.htm](boons-and-curses/to4pGXcJqF1VpqBt.htm)|Kurgess - Major Boon|Kurgess - Grâce majeure|libre|
|[U7ZJfuPLQPyoaj4M.htm](boons-and-curses/U7ZJfuPLQPyoaj4M.htm)|Pharasma - Major Boon|Pharasma - Grâce majeure|libre|
|[ubXii3t9Jf9YHcCk.htm](boons-and-curses/ubXii3t9Jf9YHcCk.htm)|Kazutal - Minor Boon|Kazutal - Grâce mineure|libre|
|[Ul5jjHfvvfB14a3z.htm](boons-and-curses/Ul5jjHfvvfB14a3z.htm)|Kurgess - Minor Curse|Kurgess - Malédiction mineure|libre|
|[unfh0BOx2PEhxWGB.htm](boons-and-curses/unfh0BOx2PEhxWGB.htm)|Besmara - Major Boon|Besmara - Grâce majeure|libre|
|[Up7nUIT42zwhgZf4.htm](boons-and-curses/Up7nUIT42zwhgZf4.htm)|Desna - Minor Curse|Desna - Malédiction mineure|libre|
|[uVZrqxtOOutr4Ss9.htm](boons-and-curses/uVZrqxtOOutr4Ss9.htm)|Gozreh - Minor Boon|Gozreh - Grâce mineure|libre|
|[V7FYIgphCCkYsEXF.htm](boons-and-curses/V7FYIgphCCkYsEXF.htm)|Sarenrae - Moderate Curse|Sarenraë - Malédiction modérée|libre|
|[vfQ7TsScMBTNiznn.htm](boons-and-curses/vfQ7TsScMBTNiznn.htm)|Gozreh - Major Boon|Gozreh - Grâce majeure|libre|
|[vJd6Ikya7B6XH4lb.htm](boons-and-curses/vJd6Ikya7B6XH4lb.htm)|Kazutal - Minor Curse|Kazutal - Malédiction mineure|libre|
|[vU5leGDNAZSfPgTz.htm](boons-and-curses/vU5leGDNAZSfPgTz.htm)|Shelyn - Moderate Curse|Shélyn - Malédiction modérée|libre|
|[VZodqtMF3qrMl8a7.htm](boons-and-curses/VZodqtMF3qrMl8a7.htm)|Sarenrae - Minor Curse|Sarenraë - Grâce mineure|libre|
|[wEqPbkQMuqlL2TPn.htm](boons-and-curses/wEqPbkQMuqlL2TPn.htm)|Irori - Major Curse|Irori - Malédiction majeure|libre|
|[wIjqVRP0Hm2bIfyc.htm](boons-and-curses/wIjqVRP0Hm2bIfyc.htm)|Calistria - Minor Curse|Calistria - Malédiction mineure|libre|
|[wlBiDpWvjKiw9k2z.htm](boons-and-curses/wlBiDpWvjKiw9k2z.htm)|Zon-Kuthon - Minor Boon|Zon-Kuthon - Grâce mineure|libre|
|[wpOtcMUdymzpbtC4.htm](boons-and-curses/wpOtcMUdymzpbtC4.htm)|Gruhastha - Minor Boon|Gruhastha - Grâce mineure|libre|
|[wREsKC8CTifEyj6v.htm](boons-and-curses/wREsKC8CTifEyj6v.htm)|Norgorber - Major Curse|Norgorber - Malédiction majeure|libre|
|[WVlWzn918RNExxpe.htm](boons-and-curses/WVlWzn918RNExxpe.htm)|Sivanah - Major Boon|Sivanah - Grâce majeure|libre|
|[wXAVESzPURB1iBWW.htm](boons-and-curses/wXAVESzPURB1iBWW.htm)|Achaekek - Moderate Curse|Achaékek - Malédiction modérée|libre|
|[wZWklm7jLry2rTOk.htm](boons-and-curses/wZWklm7jLry2rTOk.htm)|Shizuru - Major Curse|Shizuru - Malédiction majeure|libre|
|[x1QrKuvZ2TbTvLUY.htm](boons-and-curses/x1QrKuvZ2TbTvLUY.htm)|Cayden Cailean - Moderate Curse|Cayden Cailéan - Malédiction modérée|libre|
|[XFd7rnb9cchNw0WN.htm](boons-and-curses/XFd7rnb9cchNw0WN.htm)|Norgorber - Minor Curse|Norgorber - Malédiction mineure|libre|
|[XFiI9qC4MNX4WUdh.htm](boons-and-curses/XFiI9qC4MNX4WUdh.htm)|Lamashtu - Major Boon|Lamashtu - Grâce majeure|libre|
|[XfMZ83VA7dtF7fL5.htm](boons-and-curses/XfMZ83VA7dtF7fL5.htm)|Urgathoa - Minor Curse|Urgathoa - Malédiction mineure|libre|
|[Xlu60yqtTXutwG2G.htm](boons-and-curses/Xlu60yqtTXutwG2G.htm)|Tsukiyo - Major Boon|Tsukiyo - Grâce majeure|libre|
|[XQg4eq4yIyEO7z3M.htm](boons-and-curses/XQg4eq4yIyEO7z3M.htm)|Brigh - Major Boon|Brigh - Grâce majeure|libre|
|[xrczhivVmi7MrEfz.htm](boons-and-curses/xrczhivVmi7MrEfz.htm)|Irori - Moderate Boon|Irori - Grâce modérée|libre|
|[XujNRFYY4w1NgArr.htm](boons-and-curses/XujNRFYY4w1NgArr.htm)|Kurgess - Moderate Boon|Kurgess - Grâce modérée|libre|
|[XV9hL75EXA9tIusv.htm](boons-and-curses/XV9hL75EXA9tIusv.htm)|Kazutal - Major Curse|Kazutal - Malédiction majeure|libre|
|[xwhPzOXxX1fON3j4.htm](boons-and-curses/xwhPzOXxX1fON3j4.htm)|Achaekek - Moderate Boon|Achaékek - Grâce modérée|libre|
|[XYM7v72doPR3LwgA.htm](boons-and-curses/XYM7v72doPR3LwgA.htm)|Gozreh - Minor Curse|Gozreh - Malédiction mineure|libre|
|[y736aBzm4RKwB7RQ.htm](boons-and-curses/y736aBzm4RKwB7RQ.htm)|Ghlaunder - Moderate Curse|Ghlaunder - Malédiction modérée|libre|
|[y9DNpX7krUiOq0YI.htm](boons-and-curses/y9DNpX7krUiOq0YI.htm)|Asmodeus - Moderate Curse|Asmodéus - Malédiction modérée|libre|
|[yBoHo1dcZpKygfJE.htm](boons-and-curses/yBoHo1dcZpKygfJE.htm)|Brigh - Minor Curse|Brigh - Malédiction mineure|libre|
|[YeWT4MjQ8vDmSThs.htm](boons-and-curses/YeWT4MjQ8vDmSThs.htm)|Nocticula - Major Curse|Nocticula - Malédiction majeure|libre|
|[YHeCTSaw1sOnXhre.htm](boons-and-curses/YHeCTSaw1sOnXhre.htm)|Gruhastha - Major Curse|Gruhastha - Malédiction majeure|libre|
|[yoXU4CqZtzgKXdlB.htm](boons-and-curses/yoXU4CqZtzgKXdlB.htm)|Asmodeus - Major Curse|Asmodéus - Malédiction majeure|libre|
|[yWASKzumwmv4TgSe.htm](boons-and-curses/yWASKzumwmv4TgSe.htm)|Calistria - Minor Boon|Calistria - Grâce mineure|libre|
|[YXjcQMRNqJbosjan.htm](boons-and-curses/YXjcQMRNqJbosjan.htm)|Grandmother Spider - Moderate Curse|Grand-mère Araignée - Malédiction modérée|libre|
|[YYc9SJ6t3gOMWYDi.htm](boons-and-curses/YYc9SJ6t3gOMWYDi.htm)|Gruhastha - Moderate Boon|Gruhastha - Grâce modérée|libre|
|[z4GfG3GatAOELKyA.htm](boons-and-curses/z4GfG3GatAOELKyA.htm)|Desna - Major Curse|Desna - Malédiction majeure|libre|
|[zMuLnoGROhRhqbDp.htm](boons-and-curses/zMuLnoGROhRhqbDp.htm)|Milani - Minor Curse|Milani - Malédiction mineure|libre|
|[zoJjzir97Ie7tbp0.htm](boons-and-curses/zoJjzir97Ie7tbp0.htm)|Shelyn - Moderate Boon|Shélyn - Grâce modérée|libre|
|[ZPlvqkjEv9a7J76T.htm](boons-and-curses/ZPlvqkjEv9a7J76T.htm)|Cayden Cailean - Major Boon|Cayden Cailéan - Grâce majeure|libre|
|[zu2yJaXxOus4tNqd.htm](boons-and-curses/zu2yJaXxOus4tNqd.htm)|Alseta - Major Curse|Alséta- Malédiction majeure|libre|
|[zWshZRICuzP7DfFV.htm](boons-and-curses/zWshZRICuzP7DfFV.htm)|Iomedae - Major Curse|Iomédae - Malédiction majeure|libre|
