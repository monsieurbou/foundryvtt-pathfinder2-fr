# État de la traduction (bestiary-family-ability-glossary)

 * **libre**: 211
 * **officielle**: 50
 * **changé**: 41


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0MBZJdTv863X2jwz.htm](bestiary-family-ability-glossary/0MBZJdTv863X2jwz.htm)|(Graveknight) Portentous Glare|(Chevalier sépulcre) Regard de mauvais augure|changé|
|[1gsgecz7rhvHeiCX.htm](bestiary-family-ability-glossary/1gsgecz7rhvHeiCX.htm)|(Beheaded) Fiendish|(Décapité) Fiélon|changé|
|[6Alm4mLj3ORxCXC2.htm](bestiary-family-ability-glossary/6Alm4mLj3ORxCXC2.htm)|(Ravener) Cowering Fear|(Dévoreur draconique) Recroquevillé de terreur|changé|
|[7llQJrvVuCh7KjZO.htm](bestiary-family-ability-glossary/7llQJrvVuCh7KjZO.htm)|(Cryptid, Primeval) Stench|(Cryptide, Primitif) Puanteur|changé|
|[871jo3ZGnybF6dC8.htm](bestiary-family-ability-glossary/871jo3ZGnybF6dC8.htm)|(Lich) Blasphemous Utterances|(Liche) Paroles blasphématoires|changé|
|[awFTdoQfYva84HkU.htm](bestiary-family-ability-glossary/awFTdoQfYva84HkU.htm)|(Ulgrem-Axaan) Befouling Odor|(Ulgrem-Axaan) Odeur de souillure|changé|
|[AydIHjDNbOZizj5U.htm](bestiary-family-ability-glossary/AydIHjDNbOZizj5U.htm)|(Ghost) Cold Spot|(Fantôme) Point froid|changé|
|[BBqjQN5Gbe4PWP56.htm](bestiary-family-ability-glossary/BBqjQN5Gbe4PWP56.htm)|(Vampire, Jiang-Shi, Basic) One More Breath|(Vampire, Jiang-shi, Basique) Une respiration supplémentaire|changé|
|[BgbSHRdkGH7raOgA.htm](bestiary-family-ability-glossary/BgbSHRdkGH7raOgA.htm)|(Graveknight) Phantom Mount|(Chevalier sépulcre) Monture fantôme|changé|
|[bTJnxBQjr7G8yr30.htm](bestiary-family-ability-glossary/bTJnxBQjr7G8yr30.htm)|(Graveknight) Sacrilegious Aura|(Chevalier sépulcre) Aura sacrilège|changé|
|[CxiEpXt7Gw3tSIOh.htm](bestiary-family-ability-glossary/CxiEpXt7Gw3tSIOh.htm)|(Zombie) Rotting Aura|(Zombie) Aura de pourrissement|changé|
|[Es8g7kZrLAuNdiD1.htm](bestiary-family-ability-glossary/Es8g7kZrLAuNdiD1.htm)|(Siabrae) Miasma|(Siabré) Miasme|changé|
|[eSp48kG7v1GNuGgh.htm](bestiary-family-ability-glossary/eSp48kG7v1GNuGgh.htm)|(Visitant) Vengeful Presence|(Revenant) Présence vengeresse|changé|
|[Ez8sVqv1EBcJuorK.htm](bestiary-family-ability-glossary/Ez8sVqv1EBcJuorK.htm)|(Zombie) Putrid Stench|(Zombie) Odeur putride|changé|
|[fTk5nmm7HHtXOdSo.htm](bestiary-family-ability-glossary/fTk5nmm7HHtXOdSo.htm)|(Lich) Dark Deliverance|(Liche) Sombre salut|changé|
|[fVyoHEO3fSR737M1.htm](bestiary-family-ability-glossary/fVyoHEO3fSR737M1.htm)|(Ghost) Draining Touch|(Fantôme) Toucher affaiblissant|changé|
|[HKFoOZZV4WdjkeeJ.htm](bestiary-family-ability-glossary/HKFoOZZV4WdjkeeJ.htm)|(Protean) Warpwave|(Protéen) Vagues de distorsion|changé|
|[i7m74TphAiFYvzPL.htm](bestiary-family-ability-glossary/i7m74TphAiFYvzPL.htm)|(Ghost) Ghost Storm|(Fantôme) Tempête Fantôme|changé|
|[ii98NEWkpvIJXgFZ.htm](bestiary-family-ability-glossary/ii98NEWkpvIJXgFZ.htm)|(Zombie, Shock) Lightning Rod|(Zombie, de choc) Sceptre de foudre|changé|
|[iiIhLkjuPYJ93Upw.htm](bestiary-family-ability-glossary/iiIhLkjuPYJ93Upw.htm)|(Melfesh Monster) Smolderstench|(Monstre melfesh) Puanteur brûlante|changé|
|[jD0M3yV6gjkXafsJ.htm](bestiary-family-ability-glossary/jD0M3yV6gjkXafsJ.htm)|(Divine Warden) Divine Destruction|(Gardien divin) Destruction divine|changé|
|[kG4fDd16fYEFvmgy.htm](bestiary-family-ability-glossary/kG4fDd16fYEFvmgy.htm)|(Nymph Queen) Nymph's Beauty|(nymphe souveraine) Beauté de la reine|changé|
|[KJrQdJQ0ZEmKYH4Y.htm](bestiary-family-ability-glossary/KJrQdJQ0ZEmKYH4Y.htm)|(Kallas Devil) Suffer the Children|(Diablesse kallas) Subir les enfants|changé|
|[kLQ0eq18SvKEvggc.htm](bestiary-family-ability-glossary/kLQ0eq18SvKEvggc.htm)|(Blackfrost Dead) Blackfrost|(Mort de froidnoir) Froidnoir|changé|
|[kQZ6pzdSn6FaxWF2.htm](bestiary-family-ability-glossary/kQZ6pzdSn6FaxWF2.htm)|(Visitant) Visitant Spells|(Revenant) Sorts de revenant|changé|
|[lD9Onw05RxdcmM2e.htm](bestiary-family-ability-glossary/lD9Onw05RxdcmM2e.htm)|(Mana Wastes Mutant) Revolting Appearance|(Mutant de la Désolation de Mana) Apparence révoltante|changé|
|[OeLdzrhIrDOvsm3E.htm](bestiary-family-ability-glossary/OeLdzrhIrDOvsm3E.htm)|(Lich) Unholy Touch|(Liche) Contact impie|changé|
|[oXRnrQQ04oi8OkDG.htm](bestiary-family-ability-glossary/oXRnrQQ04oi8OkDG.htm)|(Vampire, Vrykolakas) Feral Possession|(Vampire, Vrykolakas) Possession sauvage|changé|
|[qjgrMhwz78T31kLU.htm](bestiary-family-ability-glossary/qjgrMhwz78T31kLU.htm)|(Vampire, Strigoi) Strigoi Weaknesses|(Vampire, Strigoi) Faiblesse du strigoi|changé|
|[QMWvYf8Qowj2Fzmx.htm](bestiary-family-ability-glossary/QMWvYf8Qowj2Fzmx.htm)|(Graveknight) Dark Deliverance|(Chevalier sépulcre) Sombre salut|changé|
|[s6JRMjgA7hSGUAYX.htm](bestiary-family-ability-glossary/s6JRMjgA7hSGUAYX.htm)|(Vampire, Vrykolakas Master) Pestilential Aura|(Vampire, Maître vrykolakas) Aura pestilentielle|changé|
|[SEzkqVJxr2eJDsuJ.htm](bestiary-family-ability-glossary/SEzkqVJxr2eJDsuJ.htm)|(Ghast) Stench|(Blême) Puanteur|changé|
|[tDfv2oEPal19NtSM.htm](bestiary-family-ability-glossary/tDfv2oEPal19NtSM.htm)|(Ghost) Fetch|(Fantôme) Double fantomatique|changé|
|[UHuA1hue5xrRM6KK.htm](bestiary-family-ability-glossary/UHuA1hue5xrRM6KK.htm)|(Nymph Queen) Tied to the Land|(nymphe souveraine) Liée au territoire|changé|
|[VLCECgjkL5bNOxpx.htm](bestiary-family-ability-glossary/VLCECgjkL5bNOxpx.htm)|(Ravener) Consume Soul|(Dévoreur draconique) Dévorer les âmes|changé|
|[vOvzh8XCMmvtW0Ws.htm](bestiary-family-ability-glossary/vOvzh8XCMmvtW0Ws.htm)|(Zombie, Shock) Electromechanical Phasing|(Zombie, de choc) Phase électromécanique|changé|
|[wEL7ZDMaSWSSYQG9.htm](bestiary-family-ability-glossary/wEL7ZDMaSWSSYQG9.htm)|(Vampire, Vetalarana, Manipulator) Drain Thoughts|(Vampire, Vetalarana, Manipulateur) Drainer les pensées|changé|
|[wh2T2L5SMsa32RyE.htm](bestiary-family-ability-glossary/wh2T2L5SMsa32RyE.htm)|(Lich) Cold Beyond Cold|(Liche) Au-delà du froid|changé|
|[X0KoXRDEJYYtwGgK.htm](bestiary-family-ability-glossary/X0KoXRDEJYYtwGgK.htm)|(Beheaded) Whispering|(Décapité) Murmurant|changé|
|[yDE3ZEoxRUqQmAsX.htm](bestiary-family-ability-glossary/yDE3ZEoxRUqQmAsX.htm)|(Vampire, Nosferatu Overlord) Air of Sickness|(Vampire, Seigneur nosferatu) Air nauséabond|changé|
|[yWNcEsEJIoeXKBnk.htm](bestiary-family-ability-glossary/yWNcEsEJIoeXKBnk.htm)|(Lich) Void Shroud|(Liche) Linceul de néant|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[03knx0BWuYBNciXI.htm](bestiary-family-ability-glossary/03knx0BWuYBNciXI.htm)|(Graveknight) Empty Save for Dust|(Chevalier sépulcre) Vide hormis la poussière|libre|
|[08egiRxOvMX97XTc.htm](bestiary-family-ability-glossary/08egiRxOvMX97XTc.htm)|(Werecreature) Change Shape|(Créature garou) Changement de forme|officielle|
|[0B9eEetIbeSPhulD.htm](bestiary-family-ability-glossary/0B9eEetIbeSPhulD.htm)|(Divine Warden) Divine Domain Spells|(Gardien divin) Sorts divins de domaine|libre|
|[0K3vhX14UEPftqu8.htm](bestiary-family-ability-glossary/0K3vhX14UEPftqu8.htm)|(Ghost) Telekinetic Assault|(Fantôme) Assaut télékinésique|officielle|
|[1AnBJbwfdlvzA7SK.htm](bestiary-family-ability-glossary/1AnBJbwfdlvzA7SK.htm)|(Vampire, Jiang-Shi, Minister) Distant Steps|(Vampire, Jiang-Shi, Ministre) Pas distants|libre|
|[1p2UcxwgYDQzVqov.htm](bestiary-family-ability-glossary/1p2UcxwgYDQzVqov.htm)|(Beheaded) Furious Headbutt|(Décapité) Coup de tête rageur|libre|
|[268Q7HdtylwpznvG.htm](bestiary-family-ability-glossary/268Q7HdtylwpznvG.htm)|(Vampire, Vetalarana, Basic) Mental Rebirth|(Vampire, Vetalarana, Basique) Renaissance mentale|libre|
|[2BjqIO36k0Y6tgZf.htm](bestiary-family-ability-glossary/2BjqIO36k0Y6tgZf.htm)|(Vampire, Nosferatu Thrall) Fast Healing|(Vampire, Esclave nosferatu) Guérison accélérée|libre|
|[2GhEuIlgLj02gV9r.htm](bestiary-family-ability-glossary/2GhEuIlgLj02gV9r.htm)|(Skeleton) Rotten|(Squelette) Pourri|libre|
|[2k0X7JdI9MMt4pp0.htm](bestiary-family-ability-glossary/2k0X7JdI9MMt4pp0.htm)|(Zombie) Unholy Speed|(Zombie) Vitesse impie|libre|
|[3oajfAIPSf77dSBL.htm](bestiary-family-ability-glossary/3oajfAIPSf77dSBL.htm)|(Secret Society) Get out of Jail|(Société secrète) Sortir de prison|libre|
|[3vcDqURRKk0mtdav.htm](bestiary-family-ability-glossary/3vcDqURRKk0mtdav.htm)|(Mana Wastes Mutant) Grasping Tentacle|(Mutant de la Désolation de Mana) Tentacule agrippant|libre|
|[3VN2BLNE5zakeDCM.htm](bestiary-family-ability-glossary/3VN2BLNE5zakeDCM.htm)|(Lich) Metamagic Alteration|(Liche) Altération mutamagique|libre|
|[49aAeGbH5p2IJ5Fz.htm](bestiary-family-ability-glossary/49aAeGbH5p2IJ5Fz.htm)|(Kuworsys) Rapid Bombardment|(Kuworsys) Bombardement rapide|libre|
|[4Aj4FWKObfMYoSvh.htm](bestiary-family-ability-glossary/4Aj4FWKObfMYoSvh.htm)|(Ghost) Beatific Appearance|(Fantôme) Apparence bienheureuse|libre|
|[4AN6IgzsbhjCPYIZ.htm](bestiary-family-ability-glossary/4AN6IgzsbhjCPYIZ.htm)|(Zombie, Shock) Breath Weapon|(Zombie, de choc) Arme de souffle|libre|
|[4B8O9QBJuDhJkVcz.htm](bestiary-family-ability-glossary/4B8O9QBJuDhJkVcz.htm)|(Vampire, Nosferatu) Dominate|(Vampire, Nosferatu) Domination|libre|
|[4p06ewIsPnWZwuwc.htm](bestiary-family-ability-glossary/4p06ewIsPnWZwuwc.htm)|(Ghost) Lynchpin|(Fantôme) Lié dans la mort|libre|
|[4pBHIGtTK9yQmZ7h.htm](bestiary-family-ability-glossary/4pBHIGtTK9yQmZ7h.htm)|(Beheaded) Lifesense 60 feet|(Décapité) Perception de la vie 18 m|libre|
|[4UEZY3G3CiCP78Kx.htm](bestiary-family-ability-glossary/4UEZY3G3CiCP78Kx.htm)|(Visitant) Wrestle|(Revenant) Lutte|officielle|
|[5arYoSY5kcRo8TeM.htm](bestiary-family-ability-glossary/5arYoSY5kcRo8TeM.htm)|(Vampire, Vrykolakas) Vrykolakas Vulnerabilities|(Vampire, Vrykolakas) Vulnérabilités des vrykolakas|officielle|
|[5dbzWZbiTyPKgwKS.htm](bestiary-family-ability-glossary/5dbzWZbiTyPKgwKS.htm)|(Graveknight) Graveknight's Curse|(Chevalier sépulcre) Malédiction du chevalier sépulcre|officielle|
|[5DuBTf37u88IrphJ.htm](bestiary-family-ability-glossary/5DuBTf37u88IrphJ.htm)|(Vampire, Basic) Vampire Weaknesses|(Vampire, Basique) Faiblesses du vampire|officielle|
|[5lv9r2ubDCov4dFn.htm](bestiary-family-ability-glossary/5lv9r2ubDCov4dFn.htm)|(Ghost) Pyre's Memory|(Fantôme) Souvenir du bûcher funéraire|libre|
|[6GApXnJDAMAchNl7.htm](bestiary-family-ability-glossary/6GApXnJDAMAchNl7.htm)|(Harrowkin) Defensive Suit|(Créature du Tourment) Costume défensif|libre|
|[6isn9nqnvfRrC1wW.htm](bestiary-family-ability-glossary/6isn9nqnvfRrC1wW.htm)|(Graveknight) Weapon Master|(Chevalier sépulcre) Maître d'armes|officielle|
|[7jWANvnDmNcQvFve.htm](bestiary-family-ability-glossary/7jWANvnDmNcQvFve.htm)|(Graveknight) Ruinous Weapons|(Chevalier sépulcre) Armes du désastre|officielle|
|[7TYLgIMDGgUfbgGY.htm](bestiary-family-ability-glossary/7TYLgIMDGgUfbgGY.htm)|(Lich) Mask Death|(Liche) Cacher la mort|libre|
|[7V3M78uYKXxJTW37.htm](bestiary-family-ability-glossary/7V3M78uYKXxJTW37.htm)|(Castrovelian) Gaseous Adaptation|(Castrovélien) Adaptation gazeuse|libre|
|[7W6v0oULg9TCz9ym.htm](bestiary-family-ability-glossary/7W6v0oULg9TCz9ym.htm)|(Zombie) Spitting Zombie|(Zombie) Zombie cracheur|libre|
|[8IQqWkLqzvWA1JRJ.htm](bestiary-family-ability-glossary/8IQqWkLqzvWA1JRJ.htm)|(Vampire, Strigoi Progenitor) Create Spawn|(Vampire, Strigoi géniteur) Création de rejetons|libre|
|[8MACWp05F4SacE7E.htm](bestiary-family-ability-glossary/8MACWp05F4SacE7E.htm)|(Vampire, Nosferatu Thrall) Weakness|(Vampire, Esclave nosferatu) Faiblesse|libre|
|[8UQWXpYfn9oE1ZHu.htm](bestiary-family-ability-glossary/8UQWXpYfn9oE1ZHu.htm)|(Vampire, Nosferatu) Command Thrall|(Vampire, Nosferatu) Diriger un esclave|libre|
|[97Eo5oA1WeMJA7nR.htm](bestiary-family-ability-glossary/97Eo5oA1WeMJA7nR.htm)|(Skeleton) Grave Eruption|(Squelette) Éruption de la tombe|libre|
|[98twzz56mMGN5Ftw.htm](bestiary-family-ability-glossary/98twzz56mMGN5Ftw.htm)|(Vampire, Vrykolakas) Drink Blood|(Vampire, Vrykolakas) Boire le sang|libre|
|[9oy2zWxy7Klk9FxR.htm](bestiary-family-ability-glossary/9oy2zWxy7Klk9FxR.htm)|(Skeleton) Blaze|(Squelette) Embrasement|libre|
|[9w9hhvqGhrxdAzMH.htm](bestiary-family-ability-glossary/9w9hhvqGhrxdAzMH.htm)|(Secret Society) Connected|(Société secrète) Relations|libre|
|[9wX6EFimVFYFZjD7.htm](bestiary-family-ability-glossary/9wX6EFimVFYFZjD7.htm)|(Ulgrem-Axaan) Ichor Coating|(Ulgrem-Axaan) Couvert d'ichor|libre|
|[a7A4xY3NeWI2Ya4Z.htm](bestiary-family-ability-glossary/a7A4xY3NeWI2Ya4Z.htm)|(Lich) Rejuvenation|(Liche) Reconstruction|officielle|
|[a7SNHoP22SBoOAmA.htm](bestiary-family-ability-glossary/a7SNHoP22SBoOAmA.htm)|(Cryptid, Rumored) Hybrid Form|(Cryptide, Rumeur) Forme hybride|libre|
|[Acal6RaZleMsVwCw.htm](bestiary-family-ability-glossary/Acal6RaZleMsVwCw.htm)|(Sporeborn) Contagious Spores|(Sporoïde) Spores contagieuses|libre|
|[aiwlPSmNY9b6Psvd.htm](bestiary-family-ability-glossary/aiwlPSmNY9b6Psvd.htm)|(Ghost) Frightful Moan|(Fantôme) Lamentation d'épouvante|officielle|
|[AjTkksppymqzCivT.htm](bestiary-family-ability-glossary/AjTkksppymqzCivT.htm)|(Vampire, True) Dominate|(Vampire, Véritable) Domination|officielle|
|[AkkmaixGqzw75h7W.htm](bestiary-family-ability-glossary/AkkmaixGqzw75h7W.htm)|(Graveknight) Eager for Battle|(Chevalier sépulcre) Impatient de se battre|libre|
|[apVBHJT95t9Fhxpb.htm](bestiary-family-ability-glossary/apVBHJT95t9Fhxpb.htm)|(Zombie) Infested Zombie|(Zombie) Zombie infesté|libre|
|[AvoSAlqB8mZfjwr9.htm](bestiary-family-ability-glossary/AvoSAlqB8mZfjwr9.htm)|(Skeleton) Aquatic Bones|(Squelette) Os aquatiques|libre|
|[AzvCvgx9SUAB2blo.htm](bestiary-family-ability-glossary/AzvCvgx9SUAB2blo.htm)|(Vampire, Strigoi) Domain of Dusk|(Vampire, Strigoi) Domaine du crépuscule|libre|
|[b3p8x6sgTa0BOvAb.htm](bestiary-family-ability-glossary/b3p8x6sgTa0BOvAb.htm)|(Lich) Siphon Life|(Liche) Siphonner la vie|officielle|
|[BcSlVpaN72LoQ5BV.htm](bestiary-family-ability-glossary/BcSlVpaN72LoQ5BV.htm)|(Ghost) Site Bound|(Fantôme) Lié à un site|libre|
|[bpoyTnJ7zn3kG6Y9.htm](bestiary-family-ability-glossary/bpoyTnJ7zn3kG6Y9.htm)|(Harrowkin) Shuffle the Deck|(Créature du Tourment) Battre les cartes|libre|
|[Bqiumr3LEo05d8x1.htm](bestiary-family-ability-glossary/Bqiumr3LEo05d8x1.htm)|(Mana Wastes Mutant) Hungry Maw|(Mutant de la Désolation de Mana) Gueule affamée|libre|
|[Bqnh5wiXVymfDgTw.htm](bestiary-family-ability-glossary/Bqnh5wiXVymfDgTw.htm)|(Cryptid, Rumored) Burning Eyes|(Cryptide, Rumeur) Yeux brûlants|libre|
|[br5Oup4USIUXQani.htm](bestiary-family-ability-glossary/br5Oup4USIUXQani.htm)|(Vampire, Vrykolakas) Swift Tracker|(Vampire, Vrykolakas) Pistage accéléré|officielle|
|[BSLqIYqxcCVBb2Vp.htm](bestiary-family-ability-glossary/BSLqIYqxcCVBb2Vp.htm)|(Zombie) Unkillable|(Zombie) Intuable|officielle|
|[buyzzomLTrIdVvTU.htm](bestiary-family-ability-glossary/buyzzomLTrIdVvTU.htm)|(Skeleton) Frozen|(Squelette) Glacé|libre|
|[bVZ6KizWVTLJUBXi.htm](bestiary-family-ability-glossary/bVZ6KizWVTLJUBXi.htm)|(Ghast) Paralysis|(Blême) Paralysie|officielle|
|[c04ICnrzygyFG3PK.htm](bestiary-family-ability-glossary/c04ICnrzygyFG3PK.htm)|(Vampire, Basic) Drink Blood|(Vampire, Basique) Boire le sang|libre|
|[C4Gzvv335YYUm1zl.htm](bestiary-family-ability-glossary/C4Gzvv335YYUm1zl.htm)|(Sporeborn) Fungal Armor|(Sporoïde) Armure fongique|libre|
|[CdjqkgAexKk8khbB.htm](bestiary-family-ability-glossary/CdjqkgAexKk8khbB.htm)|(Zombie) Ankle Biter|(Zombie) Mordeur de cheville|libre|
|[cfqRc4clMniqQNsl.htm](bestiary-family-ability-glossary/cfqRc4clMniqQNsl.htm)|(Cryptid, Mutant) Explosive End|(Cryptide, mutant) Fin explosive|libre|
|[cYkEpJzpMu3mCrFc.htm](bestiary-family-ability-glossary/cYkEpJzpMu3mCrFc.htm)|(Ghost) Phantasmagoria|(Fantôme) Fantasmagorie|libre|
|[d6eWuFd6pw6ffvPC.htm](bestiary-family-ability-glossary/d6eWuFd6pw6ffvPC.htm)|(Melfesh Monster) Swell|(Monstre melfesh) Gonfler|libre|
|[dHoR22T6amQxcS36.htm](bestiary-family-ability-glossary/dHoR22T6amQxcS36.htm)|(Sporeborn) Consume Biomatter|(Sporoïde) Consommer de la matière organique|libre|
|[DIYZNvVP28U2UnDb.htm](bestiary-family-ability-glossary/DIYZNvVP28U2UnDb.htm)|(Siabrae) Earth Glide|(Siabré) Glissement de terrain|libre|
|[DlHTe9jLssEELY6a.htm](bestiary-family-ability-glossary/DlHTe9jLssEELY6a.htm)|(Divine Warden) Divine Innate Spells|(Gardien divin) Sorts divins innés|libre|
|[DWku0nZzkqmYjrQ5.htm](bestiary-family-ability-glossary/DWku0nZzkqmYjrQ5.htm)|(Vampire, Nosferatu) Change Shape|(Vampire, Nosferatu) Changement de forme|libre|
|[EosAqVmnYlxMEGdT.htm](bestiary-family-ability-glossary/EosAqVmnYlxMEGdT.htm)|(Sporeborn) Sporeborn Infestation|(Sporoïde) Infestation du sporoïde|libre|
|[EpDyZ0mG9beLkBna.htm](bestiary-family-ability-glossary/EpDyZ0mG9beLkBna.htm)|(Vampire, Vetalarana, Basic) Drain Thoughts|(Vampire, Vetalarana, Basique) Drainer les pensées|libre|
|[eQLGYrJhu787pEwc.htm](bestiary-family-ability-glossary/eQLGYrJhu787pEwc.htm)|(Vampire, Strigoi) Shadow Form|(Vampire, Strigoi) Forme d'ombre|libre|
|[EsbdyB9DwvPvhCCC.htm](bestiary-family-ability-glossary/EsbdyB9DwvPvhCCC.htm)|(Vampire, Nosferatu Overlord) Paralytic Fear|(Vampire, Seigneur nosferatu) Peur paralysante|libre|
|[EwhjPJGLSW9v1Fbb.htm](bestiary-family-ability-glossary/EwhjPJGLSW9v1Fbb.htm)|(Vampire, Nosferatu) Divine Innate Spells|(Vampire, Nosferatu) Sorts divins innés|libre|
|[eXleBXdAemiEoHA8.htm](bestiary-family-ability-glossary/eXleBXdAemiEoHA8.htm)|(Nymph Queen) Change Shape|(nymphe souveraine) Changement de forme|libre|
|[EyFVoDiReJsBx1rf.htm](bestiary-family-ability-glossary/EyFVoDiReJsBx1rf.htm)|(Lich) Animate Cage|(Liche) Phylactère animé|libre|
|[FA0ri2fAcMa1HgZe.htm](bestiary-family-ability-glossary/FA0ri2fAcMa1HgZe.htm)|(Werecreature) Moon Frenzy|(Créature garou) Frénésie lunaire|officielle|
|[FdUuCIkQxVoTGM78.htm](bestiary-family-ability-glossary/FdUuCIkQxVoTGM78.htm)|(Mana Wastes Mutant) Mirror Thing|(Mutant de la Désolation de Mana) Chose miroir|libre|
|[fh7ar6QrFT8YWgQ9.htm](bestiary-family-ability-glossary/fh7ar6QrFT8YWgQ9.htm)|(Vampire, Strigoi Progenitor) Drink Essence|(Vampire, Géniteur strigoi) Boire l'essence|libre|
|[fmUBaLklyVmNt3VD.htm](bestiary-family-ability-glossary/fmUBaLklyVmNt3VD.htm)|(Graveknight) Clutching Armor|(Chevalier sépulcre) Armure agrippante|libre|
|[fOpw4k05kayaL13a.htm](bestiary-family-ability-glossary/fOpw4k05kayaL13a.htm)|(Mana Wastes Mutant) Magic Hunger|(Mutant de la Désolation de Mana) Faim magique|libre|
|[fqZhojt2M5LfSKSH.htm](bestiary-family-ability-glossary/fqZhojt2M5LfSKSH.htm)|(Lich) Drain Soul Cage|(Liche) Canaliser le phylactère|officielle|
|[FsqVhavMWAoFro1L.htm](bestiary-family-ability-glossary/FsqVhavMWAoFro1L.htm)|(Ravener) Soul Ward|(Dévoreur draconique) Armure d'âmes|officielle|
|[FW4KAUHb7r8WkxUc.htm](bestiary-family-ability-glossary/FW4KAUHb7r8WkxUc.htm)|(Ghoul) Paralysis|(Goule) Paralysie|officielle|
|[FXHjmH1oce7Z3tZb.htm](bestiary-family-ability-glossary/FXHjmH1oce7Z3tZb.htm)|(Vampire, Basic) Coffin Restoration|(Vampire, Basique) Cercueil restorateur|officielle|
|[fYDrunTldWmFvfjl.htm](bestiary-family-ability-glossary/fYDrunTldWmFvfjl.htm)|(Ravener) Soulsense|(Dévoreur draconique) Perception de l'âme|libre|
|[G4yIjHMzD0KVpaCm.htm](bestiary-family-ability-glossary/G4yIjHMzD0KVpaCm.htm)|(Melfesh Monster) Burrowing Grasp|(Monstre melfesh) Poigne creusante|libre|
|[ga0Oj7mmjSwWQgmR.htm](bestiary-family-ability-glossary/ga0Oj7mmjSwWQgmR.htm)|(Ghost) Corrupting Gaze|(Fantôme) Regard corrupteur|libre|
|[gbKpCw21tEmehq6e.htm](bestiary-family-ability-glossary/gbKpCw21tEmehq6e.htm)|(Siabrae) Rejuvenation|(Siabré) Reconstruction|libre|
|[GD1ZD4Rl2hTPhvjL.htm](bestiary-family-ability-glossary/GD1ZD4Rl2hTPhvjL.htm)|(Zombie, Shock) Arcing Strikes|(Zombie, de choc) Frappes d'arc|libre|
|[GgmLWLBLGaFvunTS.htm](bestiary-family-ability-glossary/GgmLWLBLGaFvunTS.htm)|(Vampire, Vetalarana, Basic) Thoughtsense 100 feet|(Vampire, Vetalarana, Basique) Perception des pensées 30 m|libre|
|[GpKQH8hnSYWiyuQU.htm](bestiary-family-ability-glossary/GpKQH8hnSYWiyuQU.htm)|(Vampire, Nosferatu) Drink Blood|(Vampire, Nosferatu) Boire le sang|libre|
|[gSXxJ2FYEGrX1Psy.htm](bestiary-family-ability-glossary/gSXxJ2FYEGrX1Psy.htm)|(Secret Society) Not Today!|(Société secrète) Pas aujourd'hui !|libre|
|[h2C99bXIwPGRdZQ0.htm](bestiary-family-ability-glossary/h2C99bXIwPGRdZQ0.htm)|(Castrovelian) Extra Large|(Castrovélien) Extra Grand|libre|
|[hA6HsM4i4yPfEsDH.htm](bestiary-family-ability-glossary/hA6HsM4i4yPfEsDH.htm)|(Ghast) Ghast Fever|(Blême) Fièvre des blêmes|libre|
|[haMVGGDY3mLuKy9s.htm](bestiary-family-ability-glossary/haMVGGDY3mLuKy9s.htm)|(Vampire, Vrykolakas Master) Bubonic Plague|(Vampire, Maître vrykolakas) Peste bubonique|officielle|
|[HdSaMUb4uEsbtQTn.htm](bestiary-family-ability-glossary/HdSaMUb4uEsbtQTn.htm)|(Skeleton) Skeleton of Roses|(Squelette) Squelette de roses|libre|
|[hEeHuM3OVzJADsSa.htm](bestiary-family-ability-glossary/hEeHuM3OVzJADsSa.htm)|(Blackfrost Dead) Mindburning Gaze|(Mort de froidnoir) Regard brûle esprit|libre|
|[HEmOAVbJ3T9pon6T.htm](bestiary-family-ability-glossary/HEmOAVbJ3T9pon6T.htm)|(Beheaded) Entangling|(Décapité) Enchevêtrant|libre|
|[HENTrR5W8AZvsIyv.htm](bestiary-family-ability-glossary/HENTrR5W8AZvsIyv.htm)|(Sporeborn) Spore Explosion|(Sporoïde) Explosion de spore|libre|
|[hEWAzlIoJg3NzA7Q.htm](bestiary-family-ability-glossary/hEWAzlIoJg3NzA7Q.htm)|(Spring-Heeled Jack) Change Shape|(Jack à ressort) Changement de forme|libre|
|[hfT7YDwx0UX4expm.htm](bestiary-family-ability-glossary/hfT7YDwx0UX4expm.htm)|(Lich) Steal Soul|(Liche) Voler l'âme|libre|
|[HLPLYJ9bOazb2ZPX.htm](bestiary-family-ability-glossary/HLPLYJ9bOazb2ZPX.htm)|(Ravener) Discorporate|(Dévoreur draconique) Dématérialisation|libre|
|[hOOXkWcTw9EFKJev.htm](bestiary-family-ability-glossary/hOOXkWcTw9EFKJev.htm)|(Vampire, Nosferatu) Plagued Coffin Restoration|(Vampire, Nosferatu) Récupération dans un cercueil pestiféré|libre|
|[hwrZQSADT36TjDdv.htm](bestiary-family-ability-glossary/hwrZQSADT36TjDdv.htm)|(Vampire, Jiang-Shi, Basic) Warped Fulu|(Vampire, Jiang-shi, basique) Fulu tordu|libre|
|[Hy7bO4TSNoQNMzA6.htm](bestiary-family-ability-glossary/Hy7bO4TSNoQNMzA6.htm)|(Mana Wastes Mutant) Energy Blast|(Mutant de la Désolation de Mana) Décharge d'énergie|libre|
|[i4WBOAb7CmY53doM.htm](bestiary-family-ability-glossary/i4WBOAb7CmY53doM.htm)|(Melfesh Monster) False Synapses|(Monstre melfesh) Faux synapses|libre|
|[iAXHLkxuuCUOwqkN.htm](bestiary-family-ability-glossary/iAXHLkxuuCUOwqkN.htm)|(Werecreature) Animal Empathy|(Créature garou) Empathie animale|officielle|
|[ICnpftxZEilrYjn0.htm](bestiary-family-ability-glossary/ICnpftxZEilrYjn0.htm)|(Werecreature) Curse of the Werecreature|(Créature garou) Malédiction du garou|officielle|
|[IFQd4GiHlV33p7oK.htm](bestiary-family-ability-glossary/IFQd4GiHlV33p7oK.htm)|(Nymph Queen) Inspiration|(nymphe souveraine) Inspiration|libre|
|[IL8wtCi23ch62zXG.htm](bestiary-family-ability-glossary/IL8wtCi23ch62zXG.htm)|(Vampire, Nosferatu Thrall) Rally|(Vampire, Esclave nosferatu) Rallier|libre|
|[IOk0MRs3f9FrarKL.htm](bestiary-family-ability-glossary/IOk0MRs3f9FrarKL.htm)|(Protean) Protean Anatomy|(Protéen) Anatomie protéenne|libre|
|[ipGBYIXk4u47Mp1D.htm](bestiary-family-ability-glossary/ipGBYIXk4u47Mp1D.htm)|(Zombie) Feast|(Zombie) Festoyer|officielle|
|[iwLj14liESK5OBN8.htm](bestiary-family-ability-glossary/iwLj14liESK5OBN8.htm)|(Ghost) Malevolent Possession|(Fantôme) Possession malveillante|officielle|
|[IxJbFJ8dG5RbZWBD.htm](bestiary-family-ability-glossary/IxJbFJ8dG5RbZWBD.htm)|(Graveknight) Devastating Blast|(Chevalier sépulcre) Déflagration dévastatrice|libre|
|[ixPqVlqLaYTB1b23.htm](bestiary-family-ability-glossary/ixPqVlqLaYTB1b23.htm)|(Ghoul) Consume Flesh|(Goule) Dévorer la chair|officielle|
|[jciRdEHgGNGFXLsg.htm](bestiary-family-ability-glossary/jciRdEHgGNGFXLsg.htm)|(Siabrae) Blight Mastery|(Siabré) Maîtrise du flétrissement|libre|
|[jjWisLWwcdxsqv8o.htm](bestiary-family-ability-glossary/jjWisLWwcdxsqv8o.htm)|(Kallas Devil) Blameless|(Diablesse Kallas) Irréprochable|libre|
|[JSBmboE6bYVxDT9d.htm](bestiary-family-ability-glossary/JSBmboE6bYVxDT9d.htm)|(Ghost) Inhabit Object|(Fantôme) Occuper un objet|officielle|
|[jsZZJDd4ZuYMlEVV.htm](bestiary-family-ability-glossary/jsZZJDd4ZuYMlEVV.htm)|(Coven) Share Senses|(Cercle) Partage des sens|libre|
|[jTuK430yY8VgIByB.htm](bestiary-family-ability-glossary/jTuK430yY8VgIByB.htm)|(Ghost) Haunted House|(Fantôme) Maison hantée|libre|
|[K0scCV18j5FzM2ei.htm](bestiary-family-ability-glossary/K0scCV18j5FzM2ei.htm)|(Cryptid, Rumored) Shifting Form|(Cryptide, Rumeur) Forme changeante|libre|
|[K5bNE90vmeFzktnM.htm](bestiary-family-ability-glossary/K5bNE90vmeFzktnM.htm)|(Skeleton) Collapse|(Squelette) Écroulement|officielle|
|[KcUHCVhHnkMD8j3k.htm](bestiary-family-ability-glossary/KcUHCVhHnkMD8j3k.htm)|(Greater Barghest) Mutation - Toxic Breath|(Barghest supérieur) Mutation - Souffle toxique|officielle|
|[kew9yPbf83smsCyL.htm](bestiary-family-ability-glossary/kew9yPbf83smsCyL.htm)|(Mana Wastes Mutant) Energy Resistance|(Mutant de la Désolation de Mana) Résistance à l'énergie|libre|
|[KLMdplDgOfXSLh6g.htm](bestiary-family-ability-glossary/KLMdplDgOfXSLh6g.htm)|(Cryptid, Rumored) Howl|(Cryptide, Rumeur) Hurlement|libre|
|[KOnVQPlRY1CqcJXy.htm](bestiary-family-ability-glossary/KOnVQPlRY1CqcJXy.htm)|(Beheaded) Bleeding|(Décapité) Saignant|libre|
|[kUApLn0cOsXQNSrL.htm](bestiary-family-ability-glossary/kUApLn0cOsXQNSrL.htm)|(Clockwork Creature) Malfunction - Damaged Propulsion|(Créature mécanique) Dysfonctionnement - Propulsion endommagée|libre|
|[KwmYKsaXHKfZgB2c.htm](bestiary-family-ability-glossary/KwmYKsaXHKfZgB2c.htm)|(Spring-Heeled Jack) Vanishing Leap|(Jack à ressort) Saut évanouissant|libre|
|[l2ov5uPpfOAoyXAL.htm](bestiary-family-ability-glossary/l2ov5uPpfOAoyXAL.htm)|(Cryptid, Rumored) Obscura Vulnerability|(Cryptide, Rumeur) Vulnérabilité obscure|libre|
|[l5FyTQQ0OfICCS1c.htm](bestiary-family-ability-glossary/l5FyTQQ0OfICCS1c.htm)|(Cryptid, Primeval) Shockwave|(Cryptide, Primitif) Onde de choc|libre|
|[L6EypYQTdK4XPldM.htm](bestiary-family-ability-glossary/L6EypYQTdK4XPldM.htm)|(Secret Society) Prepared Trap|(Société secrète) Piège préparé|libre|
|[L7kL2ps5k80XLtwV.htm](bestiary-family-ability-glossary/L7kL2ps5k80XLtwV.htm)|(Vampire, Jiang-Shi, Minister) Tumult of the Blood|(Vampire, Jiang-shi, Ministre) Tumulte du sang|libre|
|[L80gn6WOWi9roJW3.htm](bestiary-family-ability-glossary/L80gn6WOWi9roJW3.htm)|(Divine Warden) Instrument of Faith|(Gardien divin) Instrument de foi|libre|
|[lAnJ25DSs44Ya8jg.htm](bestiary-family-ability-glossary/lAnJ25DSs44Ya8jg.htm)|(Ghost) Fade|(Fantôme) Disparaître|libre|
|[LKxsPOf0hAS32Sp8.htm](bestiary-family-ability-glossary/LKxsPOf0hAS32Sp8.htm)|(Vampire, Nosferatu Thrall) Mindbound|(Vampire, Esclave nosferatu) Lié par l'esprit|libre|
|[LOd8QwaKP3hnyGkc.htm](bestiary-family-ability-glossary/LOd8QwaKP3hnyGkc.htm)|(Kallas Devil) Cold Currents|(diablesse Kallas) Courants froids|libre|
|[lv1T0kAmG2JS7PWs.htm](bestiary-family-ability-glossary/lv1T0kAmG2JS7PWs.htm)|(Mana Wastes Mutant) Sprouted Limb|(Mutant de la Désolation de Mana) Membre ajouté|libre|
|[m3flWBRtelwHv9oP.htm](bestiary-family-ability-glossary/m3flWBRtelwHv9oP.htm)|(Sporeborn) Potent Poison|(Sporoïde) Poison puissant|libre|
|[m6teF5ADh7vuM8Zr.htm](bestiary-family-ability-glossary/m6teF5ADh7vuM8Zr.htm)|(Ghast) Consume Flesh|(Blême) Dévorer la chair|libre|
|[MAC97gjFcdiqLyhp.htm](bestiary-family-ability-glossary/MAC97gjFcdiqLyhp.htm)|(Skeleton) Screaming Skull|(Squelette) Crâne hurlant|libre|
|[mEbrInCpag7YThH2.htm](bestiary-family-ability-glossary/mEbrInCpag7YThH2.htm)|(Dragon) Change Shape|(Dragon) Changement de forme|libre|
|[MhymIeTQoxbacG1o.htm](bestiary-family-ability-glossary/MhymIeTQoxbacG1o.htm)|(Zombie) Plague-Ridden|(Zombie) Pestiféré|officielle|
|[mmotrGIfshEHi8Rr.htm](bestiary-family-ability-glossary/mmotrGIfshEHi8Rr.htm)|(Mana Wastes Mutant) Chameleon Skin|(Mutant de la Désolation de Mana) Peau caméléon|libre|
|[mqai5e7YAuK2tbB9.htm](bestiary-family-ability-glossary/mqai5e7YAuK2tbB9.htm)|(Mana Wastes Mutant) Vengeful Bite|(Mutant de la Désolation de Mana) Morsure vengeresse|libre|
|[mwEig0MYM7EIibSU.htm](bestiary-family-ability-glossary/mwEig0MYM7EIibSU.htm)|(Greater Barghest) Mutation - Vestigial Arm Strike|(Barghest supérieur) Mutation - Frappe de membre atrophié|officielle|
|[mZpX54PgTxPta5y4.htm](bestiary-family-ability-glossary/mZpX54PgTxPta5y4.htm)|(Graveknight) Graveknight's Shield|(Chevalier sépulcre) Bouclier du chevalier sépulcre|libre|
|[N7UV5CZXtcoxDxCF.htm](bestiary-family-ability-glossary/N7UV5CZXtcoxDxCF.htm)|(Ghoul) Ghoul Fever|(Goule) Fièvre des goules|libre|
|[N9OUII3LfRr6hNP8.htm](bestiary-family-ability-glossary/N9OUII3LfRr6hNP8.htm)|(Cryptid, Rumored) Vanishing Escape|(Cryptide, Rumeur) Fuite fugace|libre|
|[na1WJDEaoqpcQuOR.htm](bestiary-family-ability-glossary/na1WJDEaoqpcQuOR.htm)|(Kallas Devil) Infectious Water|(Diablesse Kallas) Eau infectieuse|libre|
|[ndN9zEIheRZGOEUW.htm](bestiary-family-ability-glossary/ndN9zEIheRZGOEUW.htm)|(Secret Society) Tag Team|(Société secrète) Travail en équipe|libre|
|[NekhS5WlNuVVLBXe.htm](bestiary-family-ability-glossary/NekhS5WlNuVVLBXe.htm)|(Sporeborn) Rancid Spore Pods|(Sporoïde) Capsules de spores rancies|libre|
|[nF7RKPONY5H9kEIo.htm](bestiary-family-ability-glossary/nF7RKPONY5H9kEIo.htm)|(Ghost) Memento Mori|(Fantôme) Memento Mori|libre|
|[NgiwaeUqMPfkYvQq.htm](bestiary-family-ability-glossary/NgiwaeUqMPfkYvQq.htm)|(Vampire, Nosferatu) Plague of Ancients|(Vampire, Nosferatu) Peste des anciens|libre|
|[nJBaRtGMGVcQNg6Z.htm](bestiary-family-ability-glossary/nJBaRtGMGVcQNg6Z.htm)|(Harrowkin) Read the Cards|(Créature du Tourment) Lire les cartes|libre|
|[Nnl5wg6smOzieTop.htm](bestiary-family-ability-glossary/Nnl5wg6smOzieTop.htm)|(Vampire, True) Turn to Mist|(Vampire, Véritable) Se changer en brume|officielle|
|[noOXyIXmwYN2rRd1.htm](bestiary-family-ability-glossary/noOXyIXmwYN2rRd1.htm)|(Vampire, True) Children of the Night|(Vampire, Véritable) Enfants de la nuit|officielle|
|[NSO2l1jXK32oTnVP.htm](bestiary-family-ability-glossary/NSO2l1jXK32oTnVP.htm)|(Kallas Devil) Freezing Touch|(Diablesse Kallas) Contact frigorifiant|libre|
|[nxF03w5vKrw1jmxQ.htm](bestiary-family-ability-glossary/nxF03w5vKrw1jmxQ.htm)|(Lich) Familiar Soul|(Liche) Familier phylactère|libre|
|[O6AXB9aZB0K14sS5.htm](bestiary-family-ability-glossary/O6AXB9aZB0K14sS5.htm)|(Vampire, Jiang-Shi, Basic) Rigor Mortis|(Vampire, Jiang-shi, Basique) Rigor mortis|libre|
|[oD1cA9uXYQ8evT8f.htm](bestiary-family-ability-glossary/oD1cA9uXYQ8evT8f.htm)|(Melfesh Monster) Vent Flames|(Monstre melfesh) Évents de flammes|libre|
|[OD3VPUalSKDTYFmF.htm](bestiary-family-ability-glossary/OD3VPUalSKDTYFmF.htm)|(Greater Barghest) Mutation - Wings|(Barghest supérieur) Mutation - Ailes|officielle|
|[OMSsLUcnRj6ycEUa.htm](bestiary-family-ability-glossary/OMSsLUcnRj6ycEUa.htm)|(Graveknight) Channel Magic|(Chevalier sépulcre) Canaliser la magie|libre|
|[OPGu0WsUHpHSmu80.htm](bestiary-family-ability-glossary/OPGu0WsUHpHSmu80.htm)|(Divine Warden) Faith Bound|(Gardien divin) Lié à la foi|libre|
|[OQt7nHInDBAmHaG6.htm](bestiary-family-ability-glossary/OQt7nHInDBAmHaG6.htm)|(Vampire, Nosferatu Thrall) Mortal Shield|(Vampire, Esclave nosferatu) Bouclier mortel|libre|
|[OSnNiMdqgARyEVuv.htm](bestiary-family-ability-glossary/OSnNiMdqgARyEVuv.htm)|(Vampire, Jiang-Shi, Basic) Breathsense 60 feet|(Vampire, Jiang-shi, Basique) Perception de la respiration 18 m|libre|
|[P5YTG6I8ci4lwhZ1.htm](bestiary-family-ability-glossary/P5YTG6I8ci4lwhZ1.htm)|(Skeleton) Bloody|(Squellette) Sanglant|officielle|
|[pEJkjqjjBXj4YjYZ.htm](bestiary-family-ability-glossary/pEJkjqjjBXj4YjYZ.htm)|(Kuworsys) Careless Block|(Kuworsys) Blocage insouciant|libre|
|[PjYwPIUUrirQFiee.htm](bestiary-family-ability-glossary/PjYwPIUUrirQFiee.htm)|(Kallas Devil) Underwater Views|(Diablesse kallas) Vues sous marines|libre|
|[px2XMvw2s5RLV0X1.htm](bestiary-family-ability-glossary/px2XMvw2s5RLV0X1.htm)|(Lich) Aura of Rot|(Liche) Aura de pourriture|libre|
|[pxiSbjfWaKCG1xLD.htm](bestiary-family-ability-glossary/pxiSbjfWaKCG1xLD.htm)|(Graveknight) Betrayed Revivification|(Chevalier sépulcre) Résurrection vengeresse|officielle|
|[pyBxJKGeheGA8et4.htm](bestiary-family-ability-glossary/pyBxJKGeheGA8et4.htm)|(Clockwork Creature) Malfunction - Backfire|(Créature mécanique) Dysfonctionnement - Retour de flamme|libre|
|[Qf94y985g0o6lEoN.htm](bestiary-family-ability-glossary/Qf94y985g0o6lEoN.htm)|(Zombie) Tearing Grapple|(Zombie) Prise déchiquetée|libre|
|[qfDwBrCXeIYp0W8T.htm](bestiary-family-ability-glossary/qfDwBrCXeIYp0W8T.htm)|(Cryptid, Rumored) Creature Obscura|(Cryptide, Rumeur) Créature obscure|libre|
|[qlNOqsfJH2gj7DCs.htm](bestiary-family-ability-glossary/qlNOqsfJH2gj7DCs.htm)|(Kallas Devil) Waterfall Torrent|(Diablesse Kallas) Cascade torrentielle|libre|
|[QlrbnkeZu6M4kvOy.htm](bestiary-family-ability-glossary/QlrbnkeZu6M4kvOy.htm)|(Worm That Walks) Discorporate|(Ver-qui-marche) Dématérialisation|libre|
|[QMlQ5AvcunCvjlfM.htm](bestiary-family-ability-glossary/QMlQ5AvcunCvjlfM.htm)|(Vampire, Jiang-Shi, Basic) Drain Qi|(Vampire, Jiang-shi, Basique) Drainer le Qi|libre|
|[qs21GajPmFYel3pc.htm](bestiary-family-ability-glossary/qs21GajPmFYel3pc.htm)|(Melfesh Monster) Corpse Bomb|(Monstre Melfesh) Bombe cadavérique|libre|
|[qt2exWwQTzoObKfW.htm](bestiary-family-ability-glossary/qt2exWwQTzoObKfW.htm)|(Golem) Inexorable March|(Golem) Marche inexorable|officielle|
|[QuHeuVNDnmOCS9M1.htm](bestiary-family-ability-glossary/QuHeuVNDnmOCS9M1.htm)|(Vampire, Vrykolakas Master) Create Spawn|(Vampire, Maître vrykolakas) Création de rejetons|libre|
|[qYVKpytVx46oBVox.htm](bestiary-family-ability-glossary/qYVKpytVx46oBVox.htm)|(Cryptid, Experimental) Energy Wave|(Cryptide, expérimental) Vague d'énergie|libre|
|[qZnZriRRtjK7FtP8.htm](bestiary-family-ability-glossary/qZnZriRRtjK7FtP8.htm)|(Zombie) Persistent Limbs|(Zombie) Membres persistants|libre|
|[R0tsWv6QHd2jbQON.htm](bestiary-family-ability-glossary/R0tsWv6QHd2jbQON.htm)|(Cryptid, Primeval) Grasp for Life|(Cryptide, Primitif) Lutte pour la vie|libre|
|[R1vYhCJ2KvT8uAy1.htm](bestiary-family-ability-glossary/R1vYhCJ2KvT8uAy1.htm)|(Vampire, Jiang-Shi, Basic) Jiang-Shi Vulnerabilities|(Vampire, Jiang-shi, Basique) Vulnérabilités du Jiang-shi|libre|
|[r34QDwKiWZoVymJa.htm](bestiary-family-ability-glossary/r34QDwKiWZoVymJa.htm)|(Golem) Golem Antimagic|(Golem) Antimagie des golems|officielle|
|[R5VgwNbwc3QbqVXC.htm](bestiary-family-ability-glossary/R5VgwNbwc3QbqVXC.htm)|(Vampire, Strigoi Progenitor) Shadow Escape|(Vampire, Géniteur strigoi) Fuite d'ombre|libre|
|[rBc9fJlMXhzvn05L.htm](bestiary-family-ability-glossary/rBc9fJlMXhzvn05L.htm)|(Vampire, Strigoi) Drink Essence|(Vampire, Strigoi) Boire l'essence|libre|
|[rdQzd5ClgGPP4Qmt.htm](bestiary-family-ability-glossary/rdQzd5ClgGPP4Qmt.htm)|(Skeleton) Nimble|(Squelette) Agile|libre|
|[RgYpzTk3XapgVeXZ.htm](bestiary-family-ability-glossary/RgYpzTk3XapgVeXZ.htm)|(Skeleton) Bone Missile|(Squelette) Côte missile|libre|
|[rlNiZiUb9pRKFKQo.htm](bestiary-family-ability-glossary/rlNiZiUb9pRKFKQo.htm)|(Beheaded) Giant|(Décapité) Géant|libre|
|[RQ9NaIerG95wkhjl.htm](bestiary-family-ability-glossary/RQ9NaIerG95wkhjl.htm)|(Ulgrem-Axaan) Fallen Victim|(Ulgrem-Axaan) Victime déchue|libre|
|[RvzFTym3r22VhMrm.htm](bestiary-family-ability-glossary/RvzFTym3r22VhMrm.htm)|(Kuworsys) Smash and Grab|(Kuworsys) Frapper et saisir|libre|
|[rwEFqSLw4yIscGrO.htm](bestiary-family-ability-glossary/rwEFqSLw4yIscGrO.htm)|(Vampire, Vetalarana, Basic) Vetalarana Vulnerabilities|(Vampire, Vetalarana, Basique) Vulnérabilités des vétalaranas|libre|
|[sajLbVE6VpCsR0Kl.htm](bestiary-family-ability-glossary/sajLbVE6VpCsR0Kl.htm)|(Skeleton) Bone Powder|(Squelette) Poudre d'os|libre|
|[sAyR4uJbsWukZFZf.htm](bestiary-family-ability-glossary/sAyR4uJbsWukZFZf.htm)|(Vampire, True) Mist Escape|(Vampire, Véritable) Fuite brumeuse|libre|
|[sECjzfaYaW68vbLV.htm](bestiary-family-ability-glossary/sECjzfaYaW68vbLV.htm)|(Vampire, Vrykolakas Master) Change Shape|(Vampire, Maître vrykolakas) Changement de forme|libre|
|[SEmSk1INZDmeoB5R.htm](bestiary-family-ability-glossary/SEmSk1INZDmeoB5R.htm)|(Nymph Queen) Focus Beauty|(nymphe souveraine) Focaliser la beauté|libre|
|[SEU4K3QRAUHEMRl2.htm](bestiary-family-ability-glossary/SEU4K3QRAUHEMRl2.htm)|(Cryptid, Mutant) Unusual Bane|(Cryptide, Mutant) Crainte inhabituelle|libre|
|[SFDNK6AauPO6io5B.htm](bestiary-family-ability-glossary/SFDNK6AauPO6io5B.htm)|(Mana Wastes Mutant) Eldritch Attraction|(Mutant de la Désolation de Mana) Attraction mystique|libre|
|[Sj1IGDjUmlwFpC35.htm](bestiary-family-ability-glossary/Sj1IGDjUmlwFpC35.htm)|(Vampire, Nosferatu) Nosferatu Vulnerabilities|(Vampire, Nosferatu) Vulnérabilités du nosferatu|libre|
|[sWDlb5vJRK092MMt.htm](bestiary-family-ability-glossary/sWDlb5vJRK092MMt.htm)|(Vampire, Vetalarana, Manipulator) Control Comatose|(Vampire, Vetalarana, Manipulateur) Diriger le comateux|libre|
|[tdqw4QXzA2x7fDLT.htm](bestiary-family-ability-glossary/tdqw4QXzA2x7fDLT.htm)|(Lich) Pillage Mind|(Liche) Pillage de l'esprit|libre|
|[TIJ7r9lXvNYSIMLI.htm](bestiary-family-ability-glossary/TIJ7r9lXvNYSIMLI.htm)|(Siabrae) Stone Antlers|(Siabré) Bois de pierre|libre|
|[tKixfds07IRH2nnh.htm](bestiary-family-ability-glossary/tKixfds07IRH2nnh.htm)|(Vampire, Vrykolakas Master) Children of the Night|(Vampire, Maître vrykolakas) Enfants de la nuit|libre|
|[tQY2xmcIhzCM7oCC.htm](bestiary-family-ability-glossary/tQY2xmcIhzCM7oCC.htm)|(Melfesh Monster) Torrential Advance|(Monstre melfesh) Avancée torrentielle|libre|
|[trYchslD8fLokkT9.htm](bestiary-family-ability-glossary/trYchslD8fLokkT9.htm)|(Mana Wastes Mutant) Hulking Form|(Mutant de la Désolation de Mana) Forme impressionnante|libre|
|[uDjn2b2ZrZycQQyv.htm](bestiary-family-ability-glossary/uDjn2b2ZrZycQQyv.htm)|(Cryptid, Experimental) Clobber|(Cryptide, Expérimental) Cogneur|libre|
|[UlKGxPaKtRGWuq3V.htm](bestiary-family-ability-glossary/UlKGxPaKtRGWuq3V.htm)|(Divine Warden) Faithful Weapon|(Gardien divin) Arme fidèle|libre|
|[uMedzgKYui5X3Qtn.htm](bestiary-family-ability-glossary/uMedzgKYui5X3Qtn.htm)|(Vampire, Nosferatu Overlord) Divine Innate Spells|(Vampire, Seigneur nosferatu) Sorts innés divins|libre|
|[unR8VVR4yyRnsmnB.htm](bestiary-family-ability-glossary/unR8VVR4yyRnsmnB.htm)|(Ghost) Rejuvenation|(Fantôme) Reconstruction|officielle|
|[UOqq8lkBPcPtKoCN.htm](bestiary-family-ability-glossary/UOqq8lkBPcPtKoCN.htm)|(Melfesh Monster) Flame Lash|(Monstre melfesh) Lâché de flammes|libre|
|[uQ7cI0oerU7lDLhT.htm](bestiary-family-ability-glossary/uQ7cI0oerU7lDLhT.htm)|(Cryptid, Experimental) Power Surge|(Cryptide, Expérimental) Déferlante d'énergie|libre|
|[UsWJ13sDyOgOGWvm.htm](bestiary-family-ability-glossary/UsWJ13sDyOgOGWvm.htm)|(Cryptid, Experimental) Augmented|(Cryptide, Expérimental) Amélioré|libre|
|[UU1Fp3PRuTFONjC9.htm](bestiary-family-ability-glossary/UU1Fp3PRuTFONjC9.htm)|(Ghoul) Swift Leap|(Goule) Bond rapide|officielle|
|[uV4jf2pkMRGLdhJX.htm](bestiary-family-ability-glossary/uV4jf2pkMRGLdhJX.htm)|(Ghost) Dreamwalker|(Fantôme) Marcheur de rêves|libre|
|[v14HanKkdPGQd7Km.htm](bestiary-family-ability-glossary/v14HanKkdPGQd7Km.htm)|(Blackfrost Dead) Shattering Death|(Mort de froidnoir) Mort fracassante|libre|
|[v6he7HLxYGzaMnvL.htm](bestiary-family-ability-glossary/v6he7HLxYGzaMnvL.htm)|(Zombie) Disgusting Pustules|(Zombie) Pustules écoeurantes|officielle|
|[vOocS1EiRCXPtbgB.htm](bestiary-family-ability-glossary/vOocS1EiRCXPtbgB.htm)|(Visitant) Roar|(Revenant) Rugissement|officielle|
|[vX0CRZWok5ghasb3.htm](bestiary-family-ability-glossary/vX0CRZWok5ghasb3.htm)|(Secret Society) Prepared Diversion|(Société secrète) Diversion préparée|libre|
|[VXKiqiW7gXfFCz1U.htm](bestiary-family-ability-glossary/VXKiqiW7gXfFCz1U.htm)|(Melfesh Monster) Mycelial Tomb|(Monstre melfesh) Tombe mycélienne|libre|
|[VzKVJlX2ocv1ezzp.htm](bestiary-family-ability-glossary/VzKVJlX2ocv1ezzp.htm)|(Visitant) Noxious Breath|(Revenant) Souffle nocif|officielle|
|[W5fD1ebH6Ri8HNzh.htm](bestiary-family-ability-glossary/W5fD1ebH6Ri8HNzh.htm)|(Graveknight) Create Grave Squire|(Chevalier sépulcre) Créer un écuyer sépulcre|libre|
|[W77QhVNE46WDyMXi.htm](bestiary-family-ability-glossary/W77QhVNE46WDyMXi.htm)|(Skeleton) Explosive Death|(Squelette) Mort explosive|officielle|
|[wdO3EvDCGoDYW44S.htm](bestiary-family-ability-glossary/wdO3EvDCGoDYW44S.htm)|(Melfesh Monster) Death Throes|(Monstre melfesh) Affres de la mort|libre|
|[wJDfLmOJ2eJTmSwQ.htm](bestiary-family-ability-glossary/wJDfLmOJ2eJTmSwQ.htm)|(Mana Wastes Mutant) Increased Speed|(Mutant de la Désolation de Mana) Vitesse améliorée|libre|
|[wlhvSroB6r5cSd8Y.htm](bestiary-family-ability-glossary/wlhvSroB6r5cSd8Y.htm)|(Greater Barghest) Mutation - Poison Fangs|(Barghest supérieur) Mutation - Crocs empoisonnés|officielle|
|[WLmFKSzR6Xz9RqAu.htm](bestiary-family-ability-glossary/WLmFKSzR6Xz9RqAu.htm)|(Cryptid, Mutant) Marrowlance|(Cryptide, Mutant) Lance-à-moëlle|libre|
|[WQjFPBQPDu6vyJny.htm](bestiary-family-ability-glossary/WQjFPBQPDu6vyJny.htm)|(Ghost) Corporeal Manifestation|(Fantôme) Manifestation corporelle|libre|
|[wqptDG0IW6ExnISC.htm](bestiary-family-ability-glossary/wqptDG0IW6ExnISC.htm)|(Vampire, True) Create Spawn|(Vampire, Véritable) Création de rejetons|libre|
|[WtRxZEPH963RkUCj.htm](bestiary-family-ability-glossary/WtRxZEPH963RkUCj.htm)|(Kuworsys) Rain Blows|(Kuworsys) Pluie de coups|libre|
|[WUXY9F8SLVQSC2b8.htm](bestiary-family-ability-glossary/WUXY9F8SLVQSC2b8.htm)|(Siabrae) Stony Shards|(Siabré) éclats de pierre|libre|
|[wzezkgOufQev7BLK.htm](bestiary-family-ability-glossary/wzezkgOufQev7BLK.htm)|(Ghost) Revenant|(Fantôme) Revenant|libre|
|[X2mdpb1Dhl890YbA.htm](bestiary-family-ability-glossary/X2mdpb1Dhl890YbA.htm)|(Worm That Walks) Swarm Shape|(Ver-qui-marche) Forme de nuée|officielle|
|[XbHMVjHtbPaPr9P5.htm](bestiary-family-ability-glossary/XbHMVjHtbPaPr9P5.htm)|(Ravener) Vicious Criticals|(Dévoreur draconique) Critiques vicieux|libre|
|[XHut4MN0JBgm7WaN.htm](bestiary-family-ability-glossary/XHut4MN0JBgm7WaN.htm)|(Vampire, True) Drink Blood|(Vampire, Véritable) Boire le sang|libre|
|[XpGCN9KJN0CCIlzU.htm](bestiary-family-ability-glossary/XpGCN9KJN0CCIlzU.htm)|(Cryptid, Rumored) Stalk|(Cryptide, rumeur) Traque|libre|
|[xS8ybzuqPSi3Jb8k.htm](bestiary-family-ability-glossary/xS8ybzuqPSi3Jb8k.htm)|(Clockwork Creature) Wind-Up|(Créature mécanique) Remonter|libre|
|[xuXloICuGtfA42AN.htm](bestiary-family-ability-glossary/xuXloICuGtfA42AN.htm)|(Clockwork Creature) Malfunction - Loose Screws|(Créature mécanique) Dysfonctionnement - Vis lâches|libre|
|[XvPm866TKSfclErJ.htm](bestiary-family-ability-glossary/XvPm866TKSfclErJ.htm)|(Lich) Paralyzing Touch|(Liche) Toucher paralysant|officielle|
|[xwbiTvx3qyVEOqh6.htm](bestiary-family-ability-glossary/xwbiTvx3qyVEOqh6.htm)|(Vampire, Jiang-Shi, Minister) Dark Enlightenment|(Vampire, Jiang-shi, Ministre) Illumination sombre|libre|
|[XWtIvqCWmg8Tfr1N.htm](bestiary-family-ability-glossary/XWtIvqCWmg8Tfr1N.htm)|(Secret Society) Skill Savvy|(Société secrète) Habiletés sociales|libre|
|[xxI7QpVWLiGjdu4B.htm](bestiary-family-ability-glossary/xxI7QpVWLiGjdu4B.htm)|(Cryptid, Experimental) Operational Flaw|(Cryptide, Expérimental) Défaut opérationnel|libre|
|[Y46uZK6X7J7iMsgq.htm](bestiary-family-ability-glossary/Y46uZK6X7J7iMsgq.htm)|(Ulgrem-Axaan) Crocodile Tears|(Ulgreem-Aaxan) Larmes de crocodile|libre|
|[Y7cRjlRkdo3siz5y.htm](bestiary-family-ability-glossary/Y7cRjlRkdo3siz5y.htm)|(Coven) Locate Coven|(Cercle) Localisation du cercle|libre|
|[yaDe9hO9fm2N4SqH.htm](bestiary-family-ability-glossary/yaDe9hO9fm2N4SqH.htm)|(Spring-Heeled Jack) Resonant Terror|(Jack à ressort) Terreur résonnante|libre|
|[ydrDZ7lqioLPWRlB.htm](bestiary-family-ability-glossary/ydrDZ7lqioLPWRlB.htm)|(Blackfrost Dead) Blackfrost Breath|(Mort de froidnoir) Souffle de froidnoir|libre|
|[YELSFD2oTNMFkPJ2.htm](bestiary-family-ability-glossary/YELSFD2oTNMFkPJ2.htm)|(Secret Society) Shibboleth|(Société secrète) Shibboleth|libre|
|[yfcqbMRmCkfPWV9O.htm](bestiary-family-ability-glossary/yfcqbMRmCkfPWV9O.htm)|(Ulgrem-Axaan) Crushing Weight|(Ulgrem-Axaan) Poids écrasant|libre|
|[yfNDFjz7VBvvLwee.htm](bestiary-family-ability-glossary/yfNDFjz7VBvvLwee.htm)|(Vampire, Vetalarana, Manipulator) Paralyzing Claws|(Vampire, Vetalarana, Manipulateur) Griffes paralysantes|libre|
|[yLdiZ2AnjZ8KuT7v.htm](bestiary-family-ability-glossary/yLdiZ2AnjZ8KuT7v.htm)|(Skeleton) Bone Storm|(Squelette) Tempête d'os|libre|
|[YQmf1Otd4xoxsSGU.htm](bestiary-family-ability-glossary/YQmf1Otd4xoxsSGU.htm)|(Mana Wastes Mutant) Caustic Pustules|(Mutant de la Désolation du Mana) Pustules caustiques|libre|
|[yR9VLXIjJ2iOX77z.htm](bestiary-family-ability-glossary/yR9VLXIjJ2iOX77z.htm)|(Blackfrost Dead) Ice Climb|(Mort de froidnoir) Escalade de glace|libre|
|[Yv0mQAeVuQ2Id9vk.htm](bestiary-family-ability-glossary/Yv0mQAeVuQ2Id9vk.htm)|(Mana Wastes Mutant) Too Many Eyes|(Mutant de la Désolation de Mana) Trop d'yeux|libre|
|[YVw626nVHlWwm4ej.htm](bestiary-family-ability-glossary/YVw626nVHlWwm4ej.htm)|(Skeleton) Crumbling Bones|(Squelette) Os coulants|libre|
|[z9yqLBExOMk1y9cg.htm](bestiary-family-ability-glossary/z9yqLBExOMk1y9cg.htm)|(Cryptid, Primeval) Broken Arsenal|(Cryptide, Primitif) Arsenal brisé|libre|
|[zehifmU1fTeGs2ev.htm](bestiary-family-ability-glossary/zehifmU1fTeGs2ev.htm)|(Vampire, Strigoi) Levitation|(Vampire, Stigoi) Lévitation|libre|
|[ZIFaA4jDQjM0vq8q.htm](bestiary-family-ability-glossary/ZIFaA4jDQjM0vq8q.htm)|(Graveknight) Rejuvenation|(Chevalier sépulcre) Reconstruction|libre|
|[zkjsfsweJmsB66CS.htm](bestiary-family-ability-glossary/zkjsfsweJmsB66CS.htm)|(Coven) Contribute Spell|(Cercle) Contribuer au sort|libre|
|[ZMTnwyXjyfy7Ryi6.htm](bestiary-family-ability-glossary/ZMTnwyXjyfy7Ryi6.htm)|(Vampire, True) Change Shape|(Vampire, Véritable) Changement de forme|officielle|
|[ZRHUnhaHK2w1el8f.htm](bestiary-family-ability-glossary/ZRHUnhaHK2w1el8f.htm)|(Skeleton) Lacquered|(Squelette) Laqué|libre|
|[Zt1p0dh8ZUXIMxEt.htm](bestiary-family-ability-glossary/Zt1p0dh8ZUXIMxEt.htm)|(Harrowkin) Harrowkin Suit|(Créature du Tourment) Couleur de la créature du Tourment|libre|
|[Zu6feO9NUZJlsKuc.htm](bestiary-family-ability-glossary/Zu6feO9NUZJlsKuc.htm)|(Worm That Walks) Squirming Embrace|(Ver-qui-marche) Étreinte grouillante|officielle|
|[ZUxt6s54TMgydXoW.htm](bestiary-family-ability-glossary/ZUxt6s54TMgydXoW.htm)|(Cryptid, Mutant) Shifting Iridescence|(Cryptide, Mutant) Iridescence changeante|libre|
|[zwgUxJBNFqWOuaBX.htm](bestiary-family-ability-glossary/zwgUxJBNFqWOuaBX.htm)|(Vampire, Vrykolakas Master) Dominate Animal|(Vampire, Maître vrykolakas) Domination animale|officielle|
