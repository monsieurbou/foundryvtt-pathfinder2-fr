# État de la traduction (vehicles)

 * **libre**: 53
 * **changé**: 3


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[bMgQftCEiSqa1pTk.htm](vehicles/bMgQftCEiSqa1pTk.htm)|Vonthos's Golden Bridge|Passerelle d'or de Vonthos|changé|
|[KC1QdoG0mLi9Lzol.htm](vehicles/KC1QdoG0mLi9Lzol.htm)|Bunta|Bunta|changé|
|[RGEmINZa7YXvbruu.htm](vehicles/RGEmINZa7YXvbruu.htm)|Clunkerjunker|Casse-bagnole|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0H2BbJCAKauXYKE6.htm](vehicles/0H2BbJCAKauXYKE6.htm)|Sand Barge|Barge des sables|libre|
|[1ldIHIoe1aKHilX2.htm](vehicles/1ldIHIoe1aKHilX2.htm)|Sandsailer|Voilier des sables|libre|
|[2dLy1sfqVbNS2inJ.htm](vehicles/2dLy1sfqVbNS2inJ.htm)|Firefly|Luciole électrique|libre|
|[66dAXEMUjL7BrX8I.htm](vehicles/66dAXEMUjL7BrX8I.htm)|Carriage|Cariole|libre|
|[6CFijrzNSXtS8ExJ.htm](vehicles/6CFijrzNSXtS8ExJ.htm)|Sky Chariot (Light)|Chariot céleste léger|libre|
|[7ye7abxWAFdVbATr.htm](vehicles/7ye7abxWAFdVbATr.htm)|Chariot (Heavy)|Chariot lourd|libre|
|[8rXCF2Fr4iA0VmFx.htm](vehicles/8rXCF2Fr4iA0VmFx.htm)|Sand Racer|Bolide de sable|libre|
|[bUwBenWXjdUIKXkm.htm](vehicles/bUwBenWXjdUIKXkm.htm)|Clockwork Borer|Foreur mécanique|libre|
|[cmLOaqR3qebmLA1P.htm](vehicles/cmLOaqR3qebmLA1P.htm)|Clockwork Bumblebee|Bourdon mécanique|libre|
|[Cr4T0TcaxMr3bUaj.htm](vehicles/Cr4T0TcaxMr3bUaj.htm)|Speedster|Bolide|libre|
|[cvP9ytBY9SJbaNbo.htm](vehicles/cvP9ytBY9SJbaNbo.htm)|Apparatus of the Octopus|Appareil pieuvre|libre|
|[Cx53sUC5FZ3GZT0O.htm](vehicles/Cx53sUC5FZ3GZT0O.htm)|Raft|Radeau|libre|
|[d4sKpXnX6k2fHduY.htm](vehicles/d4sKpXnX6k2fHduY.htm)|Battery Tower|Tour Batterie|libre|
|[FbSRhHvCmXBSVkrn.htm](vehicles/FbSRhHvCmXBSVkrn.htm)|Steam Cart|Chariot à vapeur|libre|
|[fiSvQBWyyJuXnVvn.htm](vehicles/fiSvQBWyyJuXnVvn.htm)|Automated Cycle|Vélo automatisé|libre|
|[fUJ1RB9l9DppWdCH.htm](vehicles/fUJ1RB9l9DppWdCH.htm)|Titanic Stomper|Piétineur titanique|libre|
|[GMoxs9RQ88uMxfW4.htm](vehicles/GMoxs9RQ88uMxfW4.htm)|Bathysphere|Batysphère|libre|
|[HA93YJ0qjmRk0Nh8.htm](vehicles/HA93YJ0qjmRk0Nh8.htm)|Galley|Galère|libre|
|[IhniEmlnkb3pFRZG.htm](vehicles/IhniEmlnkb3pFRZG.htm)|Clockwork Hopper|Bondisseur mécanique|libre|
|[iItKvPymPNMUaGY7.htm](vehicles/iItKvPymPNMUaGY7.htm)|Hot Air Balloon|Montgolfière|libre|
|[JQAknpXhJo5bwgOM.htm](vehicles/JQAknpXhJo5bwgOM.htm)|Strider|Glisseur|libre|
|[jrLiGrOwgOhr83PL.htm](vehicles/jrLiGrOwgOhr83PL.htm)|Clockwork Castle|Château mécanique|libre|
|[K0ThJViHQzzxRmqE.htm](vehicles/K0ThJViHQzzxRmqE.htm)|Second Kiss|Second baiser|libre|
|[l0RfYgabnbNoHnkS.htm](vehicles/l0RfYgabnbNoHnkS.htm)|Rowboat|Bâteau à rames|libre|
|[MCU4ZiNNAX7GQNJb.htm](vehicles/MCU4ZiNNAX7GQNJb.htm)|Firework Pogo|Pogo feu d'artifice|libre|
|[mq8bsMnrY81gUEYb.htm](vehicles/mq8bsMnrY81gUEYb.htm)|Cart|Charrette|libre|
|[MrGgl8HDfQYITahL.htm](vehicles/MrGgl8HDfQYITahL.htm)|Clockwork Wagon|Carrosse mécanique|libre|
|[nnFXTqngaq5k1V7C.htm](vehicles/nnFXTqngaq5k1V7C.htm)|Longship|Drakkar|libre|
|[oHEqb4rOoZUzBmkH.htm](vehicles/oHEqb4rOoZUzBmkH.htm)|Helepolis|Hélépolis|libre|
|[oq6khbojns3YRi2g.htm](vehicles/oq6khbojns3YRi2g.htm)|Cliff Crawler|Chenille de falaise|libre|
|[oyKhe8J0h8G9IHE3.htm](vehicles/oyKhe8J0h8G9IHE3.htm)|Glider|Planeur|libre|
|[Qlvei5jjRXdsXUPo.htm](vehicles/Qlvei5jjRXdsXUPo.htm)|Cutter|Cotre|libre|
|[QMPVAWtaClZtD2Ip.htm](vehicles/QMPVAWtaClZtD2Ip.htm)|Shark Diver|Requin plongeur|libre|
|[R2ga1eyeSQA9w6KF.htm](vehicles/R2ga1eyeSQA9w6KF.htm)|Sailing Ship|Bâteau à voile|libre|
|[slkeDT68Zjmzz5l5.htm](vehicles/slkeDT68Zjmzz5l5.htm)|Steam Trolley|Carriole à vapeur|libre|
|[t57KE8cj9MdG6Y5H.htm](vehicles/t57KE8cj9MdG6Y5H.htm)|Bone Ship|Vaisseau squelettique|libre|
|[tVBApIWuwKNwOics.htm](vehicles/tVBApIWuwKNwOics.htm)|Steam Giant|Géant à vapeur|libre|
|[UNOwkQq5lc4cYdZP.htm](vehicles/UNOwkQq5lc4cYdZP.htm)|Wagon|Carrosse|libre|
|[V6ZuxECTGSyHAm7h.htm](vehicles/V6ZuxECTGSyHAm7h.htm)|Hillcross Glider|Planeur des collines|libre|
|[vbJL4dPQcXfYigwv.htm](vehicles/vbJL4dPQcXfYigwv.htm)|Sky Chariot (Medium)|Chariot celeste moyen|libre|
|[VNlJ7zPnOPThFEmR.htm](vehicles/VNlJ7zPnOPThFEmR.htm)|Airship|Navire volant|libre|
|[VRhdfL78HS5sT4gF.htm](vehicles/VRhdfL78HS5sT4gF.htm)|Snail Coach|Coche escargot|libre|
|[vU3UnKWCRFhnvNLV.htm](vehicles/vU3UnKWCRFhnvNLV.htm)|Ambling Surveyor|Enquêteur ambulant|libre|
|[w6auGNQQ2VyMT001.htm](vehicles/w6auGNQQ2VyMT001.htm)|Sand Diver|Plongeur des sables|libre|
|[WBMeO6BPqEzc6Yv8.htm](vehicles/WBMeO6BPqEzc6Yv8.htm)|Chariot (Light)|Chariot léger|libre|
|[WjOkOaWOGkLmuSq5.htm](vehicles/WjOkOaWOGkLmuSq5.htm)|Armored Carriage|Carriole blindée|libre|
|[WsfgcVXqqmk1EVRY.htm](vehicles/WsfgcVXqqmk1EVRY.htm)|Velocipede|Vélocipède|libre|
|[WWvqHSAYOw4ysn3e.htm](vehicles/WWvqHSAYOw4ysn3e.htm)|Sleigh|Traineau|libre|
|[WXwFZ3DrLe2PchNf.htm](vehicles/WXwFZ3DrLe2PchNf.htm)|Cauldron of Flying|Chaudron de vol|libre|
|[Y2XrhWFdiGcVAUsz.htm](vehicles/Y2XrhWFdiGcVAUsz.htm)|Sky Chariot (Heavy)|Chariot céleste lourd|libre|
|[ySR1Ds607BSX1tCw.htm](vehicles/ySR1Ds607BSX1tCw.htm)|Adaptable Paddleboat|Bâteau à rames ajustable|libre|
|[yukamyipO8x9VuZa.htm](vehicles/yukamyipO8x9VuZa.htm)|Siege Tower|Tour de siège|libre|
|[yXbxHtz3xVIMbxyh.htm](vehicles/yXbxHtz3xVIMbxyh.htm)|Mobile Inn|Auberge mobile|libre|
