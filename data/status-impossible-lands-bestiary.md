# État de la traduction (impossible-lands-bestiary)

 * **changé**: 8
 * **libre**: 11


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1MxubcO4SB8PwmKT.htm](impossible-lands-bestiary/1MxubcO4SB8PwmKT.htm)|Benthic Reaver|Castor benthique|changé|
|[1SIx3wcRvplbfwk3.htm](impossible-lands-bestiary/1SIx3wcRvplbfwk3.htm)|Stone Sister|Soeur de pierre|changé|
|[4DQotKcUZebLfeeL.htm](impossible-lands-bestiary/4DQotKcUZebLfeeL.htm)|Cursed King|Roi maudit|changé|
|[98wPo6efoFhxdVwl.htm](impossible-lands-bestiary/98wPo6efoFhxdVwl.htm)|Spell Pitchers|Sarracénies à sort|changé|
|[AIQMB0ysHNeodKG4.htm](impossible-lands-bestiary/AIQMB0ysHNeodKG4.htm)|Spellscar Fext|Fexte de la Cicatrice magique|changé|
|[gZ2qX5vbWg7otMVT.htm](impossible-lands-bestiary/gZ2qX5vbWg7otMVT.htm)|Quantium Golem (Acid)|Golem de Quantium (acide)|changé|
|[UgGyHJ39EHztCV5m.htm](impossible-lands-bestiary/UgGyHJ39EHztCV5m.htm)|Kasesh (Stone)|Kasesh (pierre)|changé|
|[wjNbuzeqoVSR0Wwm.htm](impossible-lands-bestiary/wjNbuzeqoVSR0Wwm.htm)|Quantium Golem (Electricity)|Golem de Quantium (Électricité)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[fGZeQOarR6Im7Lnk.htm](impossible-lands-bestiary/fGZeQOarR6Im7Lnk.htm)|Kashrishi Evaluator|Évaluateur Kashrishi|libre|
|[FrLxEXUPE3mMLrYN.htm](impossible-lands-bestiary/FrLxEXUPE3mMLrYN.htm)|Mana Whorl|Spire de mana|libre|
|[JmGKKXdAxUkJtdTm.htm](impossible-lands-bestiary/JmGKKXdAxUkJtdTm.htm)|Ugvashi|Ugvashi|libre|
|[K2AOcLMDOVNbgPXp.htm](impossible-lands-bestiary/K2AOcLMDOVNbgPXp.htm)|Fleshforged Conformer|Conformateur forgé de chair|libre|
|[KTabPRN489yjTvek.htm](impossible-lands-bestiary/KTabPRN489yjTvek.htm)|Fleshforged Dreadnought|Cuirassé des forges de chair|libre|
|[lhr2fjewILo4nyUZ.htm](impossible-lands-bestiary/lhr2fjewILo4nyUZ.htm)|Mutant Gnoll Hulk|Mastodonte gnoll mutant|libre|
|[LVY9JAhTnBC2SeqZ.htm](impossible-lands-bestiary/LVY9JAhTnBC2SeqZ.htm)|Skinskitter|Dermine|libre|
|[qbfMDAa3RXvwyG7k.htm](impossible-lands-bestiary/qbfMDAa3RXvwyG7k.htm)|Gunpowder Ooze|Vase poudrière|libre|
|[qmw82W0WhPkn9UTW.htm](impossible-lands-bestiary/qmw82W0WhPkn9UTW.htm)|Clockwork Cannoneer|Canonnier mécanique|libre|
|[wj7dVmPstRWzcLzC.htm](impossible-lands-bestiary/wj7dVmPstRWzcLzC.htm)|Cracked Earth|Terre craquelée|libre|
|[xl2UxOFVSDGqaVS5.htm](impossible-lands-bestiary/xl2UxOFVSDGqaVS5.htm)|Ratajin Mastermind|Cerveau ratajin|libre|
