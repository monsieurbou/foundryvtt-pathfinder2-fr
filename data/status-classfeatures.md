# État de la traduction (classfeatures)

 * **officielle**: 121
 * **libre**: 439
 * **changé**: 6


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[bard-01-fEOj0eOBe34qYdAa.htm](classfeatures/bard-01-fEOj0eOBe34qYdAa.htm)|Occult Spellcasting|Incantation de barde|changé|
|[inventor-07-78HIjRbGoONMpF31.htm](classfeatures/inventor-07-78HIjRbGoONMpF31.htm)|Breakthrough Innovation|Innovation de rupture|changé|
|[None-00-jklanV9AbZDnZHaD.htm](classfeatures/None-00-jklanV9AbZDnZHaD.htm)|Catharsis Emotion (Misery)|Émotion cathartique (Misère)|changé|
|[oracle-01-7AVspOB6ITNzGFZi.htm](classfeatures/oracle-01-7AVspOB6ITNzGFZi.htm)|Divine Spellcasting (Oracle)|Incantation divine (Oracle)|changé|
|[thaumaturge-01-1vgFGSnn0DIBmK7j.htm](classfeatures/thaumaturge-01-1vgFGSnn0DIBmK7j.htm)|Chalice|Calice|changé|
|[thaumaturge-01-AltwHU7hCqTwpn48.htm](classfeatures/thaumaturge-01-AltwHU7hCqTwpn48.htm)|Lantern|Lanterne|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[alchemist-01-7JbiaZ8bxODM5mzS.htm](classfeatures/alchemist-01-7JbiaZ8bxODM5mzS.htm)|Bomber|Artificier|officielle|
|[alchemist-01-cU2ofQLj7pg6wTSi.htm](classfeatures/alchemist-01-cU2ofQLj7pg6wTSi.htm)|Research Field|Domaine de recherche|officielle|
|[alchemist-01-eNZnx4LISDNftbx2.htm](classfeatures/alchemist-01-eNZnx4LISDNftbx2.htm)|Chirurgeon|Chirurgien|officielle|
|[alchemist-01-P9quO9XZi3OWFe1k.htm](classfeatures/alchemist-01-P9quO9XZi3OWFe1k.htm)|Toxicologist|Toxicologiste|libre|
|[alchemist-01-Pe0zmIqyTBc2Td0I.htm](classfeatures/alchemist-01-Pe0zmIqyTBc2Td0I.htm)|Advanced Alchemy|Alchimie avancée|officielle|
|[alchemist-01-sPtl05wwTpqFI0lL.htm](classfeatures/alchemist-01-sPtl05wwTpqFI0lL.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[alchemist-01-tvdb1jkjl2bRZjSp.htm](classfeatures/alchemist-01-tvdb1jkjl2bRZjSp.htm)|Mutagenist|Mutagèniste|officielle|
|[alchemist-01-w3aS3tsvH2Ub6XMn.htm](classfeatures/alchemist-01-w3aS3tsvH2Ub6XMn.htm)|Alchemy|Alchimie|libre|
|[alchemist-01-wySB9VHOW1v3TX1L.htm](classfeatures/alchemist-01-wySB9VHOW1v3TX1L.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[alchemist-01-XPPG7nN9pxt0sjMg.htm](classfeatures/alchemist-01-XPPG7nN9pxt0sjMg.htm)|Formula Book|Recueil de formules|libre|
|[alchemist-05-6zo2PJGYoig7nFpR.htm](classfeatures/alchemist-05-6zo2PJGYoig7nFpR.htm)|Field Discovery (Toxicologist)|Découverte de domaine de recherche (toxicologiste)|libre|
|[alchemist-05-7JK2a1D3VeWDcObo.htm](classfeatures/alchemist-05-7JK2a1D3VeWDcObo.htm)|Powerful Alchemy|Alchimie puissante|libre|
|[alchemist-05-8QAFgy9U8PxEa7Dw.htm](classfeatures/alchemist-05-8QAFgy9U8PxEa7Dw.htm)|Field Discovery (Bomber)|Découverte de domaine de recherche (artificier)|officielle|
|[alchemist-05-IxxPEahbqXwIXum7.htm](classfeatures/alchemist-05-IxxPEahbqXwIXum7.htm)|Field Discovery|Découverte de domaine de recherche|officielle|
|[alchemist-05-qC0Iz6SlG2i9gv6g.htm](classfeatures/alchemist-05-qC0Iz6SlG2i9gv6g.htm)|Field Discovery (Chirurgeon)|Découverte de domaine de recherche (chirurgien)|officielle|
|[alchemist-05-V4Jt7eDnJBLv5bDj.htm](classfeatures/alchemist-05-V4Jt7eDnJBLv5bDj.htm)|Field Discovery (Mutagenist)|Découverte de domaine de recherche (mutagèniste)|officielle|
|[alchemist-07-4ocPy4O0OCLY0XCM.htm](classfeatures/alchemist-07-4ocPy4O0OCLY0XCM.htm)|Alchemical Weapon Expertise|Expertise avec les armes alchimiques|officielle|
|[alchemist-07-DFQDtT1Van4fFEHi.htm](classfeatures/alchemist-07-DFQDtT1Van4fFEHi.htm)|Perpetual Infusions (Bomber)|Infusions perpétuelles (artificier)|officielle|
|[alchemist-07-Dug1oaVYejLmYEFt.htm](classfeatures/alchemist-07-Dug1oaVYejLmYEFt.htm)|Perpetual Infusions (Mutagenist)|Infusions perpétuelles (Mutagèniste)|officielle|
|[alchemist-07-fzvIe6FwwCuIdnjX.htm](classfeatures/alchemist-07-fzvIe6FwwCuIdnjX.htm)|Perpetual Infusions (Chirurgeon)|Infusions perpétuelles (chirurgien)|officielle|
|[alchemist-07-LlZ5R50z9j8jysZL.htm](classfeatures/alchemist-07-LlZ5R50z9j8jysZL.htm)|Perpetual Infusions (Toxicologist)|Infusions perpétuelles (Toxicologiste)|libre|
|[alchemist-07-ZqwHAoIZrI1dGoqK.htm](classfeatures/alchemist-07-ZqwHAoIZrI1dGoqK.htm)|Perpetual Infusions|Infusions perpétuelles|libre|
|[alchemist-09-3e1PlMXmlSwKoc6d.htm](classfeatures/alchemist-09-3e1PlMXmlSwKoc6d.htm)|Alchemical Expertise|Expertise alchimique|officielle|
|[alchemist-09-76cwNLJEm4Yetnee.htm](classfeatures/alchemist-09-76cwNLJEm4Yetnee.htm)|Double Brew|Double préparation|officielle|
|[alchemist-11-8rEVg03QJ71ic3PP.htm](classfeatures/alchemist-11-8rEVg03QJ71ic3PP.htm)|Perpetual Potency (Bomber)|Efficacité perpétuelle (Artificier)|officielle|
|[alchemist-11-JOdbVu14phvdjhaY.htm](classfeatures/alchemist-11-JOdbVu14phvdjhaY.htm)|Perpetual Potency (Toxicologist)|Efficacité perpétuelle (Toxicologiste)|libre|
|[alchemist-11-MGn2wezOr3VAdO3U.htm](classfeatures/alchemist-11-MGn2wezOr3VAdO3U.htm)|Perpetual Potency|Efficacité perpétuelle|officielle|
|[alchemist-11-mZFqRLYOQEqKA8ri.htm](classfeatures/alchemist-11-mZFqRLYOQEqKA8ri.htm)|Perpetual Potency (Mutagenist)|Efficacité perpétuelle (Mutagèniste)|officielle|
|[alchemist-11-VS5vkqUQu4n7E28Y.htm](classfeatures/alchemist-11-VS5vkqUQu4n7E28Y.htm)|Perpetual Potency (Chirurgeon)|Efficacité perpétuelle (Chirurgien)|officielle|
|[alchemist-13-1BKdOJ0HNL6Eg3xw.htm](classfeatures/alchemist-13-1BKdOJ0HNL6Eg3xw.htm)|Greater Field Discovery (Mutagenist)|Découverte de domaine de recherche supérieure (mutagèniste)|officielle|
|[alchemist-13-bv3Qel8v9tpoFbw4.htm](classfeatures/alchemist-13-bv3Qel8v9tpoFbw4.htm)|Alchemist Armor Expertise (Level 13)|Expertise avec les armures de l'alchimiste (Niveau 13)|libre|
|[alchemist-13-JJcaVijwRt9dsnac.htm](classfeatures/alchemist-13-JJcaVijwRt9dsnac.htm)|Greater Field Discovery (Chirurgeon)|Découverte de domaine de recherche supérieure (chirurgien)|officielle|
|[alchemist-13-MEwvBnT2VsO5lQ6I.htm](classfeatures/alchemist-13-MEwvBnT2VsO5lQ6I.htm)|Greater Field Discovery|Découverte de domaine de recherche supérieure|libre|
|[alchemist-13-RGs4uR3CAvgbtBAA.htm](classfeatures/alchemist-13-RGs4uR3CAvgbtBAA.htm)|Greater Field Discovery (Bomber)|Découverte de domaine de recherche supérieure (artificier)|officielle|
|[alchemist-13-tnqyQrhrZeDtDvcO.htm](classfeatures/alchemist-13-tnqyQrhrZeDtDvcO.htm)|Greater Field Discovery (Toxicologist)|Découverte de domaine de recherche supérieure (toxicologiste)|libre|
|[alchemist-15-Eood6pNPaJxuSgD1.htm](classfeatures/alchemist-15-Eood6pNPaJxuSgD1.htm)|Alchemical Alacrity|Alacrité alchimique|officielle|
|[alchemist-17-11nGqrSJOoGRlDjO.htm](classfeatures/alchemist-17-11nGqrSJOoGRlDjO.htm)|Perpetual Perfection|Perfection perpétuelle|libre|
|[alchemist-17-3R19zS7gERhEX87F.htm](classfeatures/alchemist-17-3R19zS7gERhEX87F.htm)|Perpetual Perfection (Toxicologist)|Perfection perpétuelle (toxicologiste)|libre|
|[alchemist-17-CGetAmSbv06fW7GT.htm](classfeatures/alchemist-17-CGetAmSbv06fW7GT.htm)|Perpetual Perfection (Mutagenist)|Perfection perpétuelle (Mutagèniste)|officielle|
|[alchemist-17-eG7FBDjCdEFzW9V9.htm](classfeatures/alchemist-17-eG7FBDjCdEFzW9V9.htm)|Alchemical Mastery|Maîtrise alchimique|officielle|
|[alchemist-17-xO90iBD8XNGyaCkz.htm](classfeatures/alchemist-17-xO90iBD8XNGyaCkz.htm)|Perpetual Perfection (Bomber)|Perfection perpétuelle (Artificier)|officielle|
|[alchemist-17-YByJ9O7oe8wxfbqs.htm](classfeatures/alchemist-17-YByJ9O7oe8wxfbqs.htm)|Perpetual Perfection (Chirurgeon)|Perfection perpétuelle (Chirurgien)|officielle|
|[alchemist-19-FiVYuIPTBzPzNP4E.htm](classfeatures/alchemist-19-FiVYuIPTBzPzNP4E.htm)|Alchemist Armor Mastery (Level 19)|Maîtrise d'armure de l'alchimiste (Niveau 19)|libre|
|[barbarian-01-0FtzFbUrN56KA67z.htm](classfeatures/barbarian-01-0FtzFbUrN56KA67z.htm)|Animal Instinct|Instinct animal|libre|
|[barbarian-01-0jSS6pgNXsC8k4o7.htm](classfeatures/barbarian-01-0jSS6pgNXsC8k4o7.htm)|Elemental Instinct|Instinct élémentaire|libre|
|[barbarian-01-dU7xRpg4kFd01hwZ.htm](classfeatures/barbarian-01-dU7xRpg4kFd01hwZ.htm)|Instinct|Instinct|officielle|
|[barbarian-01-JuKD6k7nDwfO0Ckv.htm](classfeatures/barbarian-01-JuKD6k7nDwfO0Ckv.htm)|Giant Instinct|Instinct de géant|officielle|
|[barbarian-01-k7M9jedvt31AJ5ZR.htm](classfeatures/barbarian-01-k7M9jedvt31AJ5ZR.htm)|Fury Instinct|Instinct de la furie|officielle|
|[barbarian-01-SCYSjUbMmw8JD9P9.htm](classfeatures/barbarian-01-SCYSjUbMmw8JD9P9.htm)|Superstition Instinct|Instinct superstitieux|libre|
|[barbarian-01-TQqv9Q5mB4PW6LH9.htm](classfeatures/barbarian-01-TQqv9Q5mB4PW6LH9.htm)|Spirit Instinct|Instinct spirituel|libre|
|[barbarian-01-VDot7CDcXElxmkkz.htm](classfeatures/barbarian-01-VDot7CDcXElxmkkz.htm)|Dragon Instinct|Instinct du dragon|officielle|
|[barbarian-01-WZUCvxqbigXos1L9.htm](classfeatures/barbarian-01-WZUCvxqbigXos1L9.htm)|Rage|Rage|libre|
|[barbarian-05-EEUTd0jAyfwTLzjk.htm](classfeatures/barbarian-05-EEUTd0jAyfwTLzjk.htm)|Brutality|Brutalité|officielle|
|[barbarian-09-ie6xDX9GMEcA2Iuq.htm](classfeatures/barbarian-09-ie6xDX9GMEcA2Iuq.htm)|Raging Resistance|Résistance enragée|officielle|
|[barbarian-11-88Q33X2a0iYPkbzd.htm](classfeatures/barbarian-11-88Q33X2a0iYPkbzd.htm)|Mighty Rage|Rage formidable|officielle|
|[barbarian-13-ejP4jVQkS48uKRFz.htm](classfeatures/barbarian-13-ejP4jVQkS48uKRFz.htm)|Weapon Fury|Fureur armée|officielle|
|[barbarian-13-TuL0UfqH14MtqYVh.htm](classfeatures/barbarian-13-TuL0UfqH14MtqYVh.htm)|Greater Juggernaut|Juggernaut supérieur|officielle|
|[barbarian-15-7JjhxMFo8DMwpGx0.htm](classfeatures/barbarian-15-7JjhxMFo8DMwpGx0.htm)|Greater Weapon Specialization (Barbarian)|Spécialisation martiale supérieure (Barbare)|officielle|
|[barbarian-15-BZnqKnqKVImjSIFE.htm](classfeatures/barbarian-15-BZnqKnqKVImjSIFE.htm)|Indomitable Will|Volonté indomptable|officielle|
|[barbarian-17-7MhzrbOyue5GQsck.htm](classfeatures/barbarian-17-7MhzrbOyue5GQsck.htm)|Heightened Senses|Sens aiguisés|officielle|
|[barbarian-17-qMtyQGUllPdgpzUo.htm](classfeatures/barbarian-17-qMtyQGUllPdgpzUo.htm)|Quick Rage|Rage rapide|officielle|
|[barbarian-19-QTCIahokREpnAYDi.htm](classfeatures/barbarian-19-QTCIahokREpnAYDi.htm)|Armor of Fury|Armure de furie|officielle|
|[barbarian-19-VLiT503OLOM3vaDx.htm](classfeatures/barbarian-19-VLiT503OLOM3vaDx.htm)|Devastator|Dévastateur|officielle|
|[bard-01-4ripp6EfdVpS0d60.htm](classfeatures/bard-01-4ripp6EfdVpS0d60.htm)|Enigma|Énigmatique|libre|
|[bard-01-6FsusoMYxxjyIkVh.htm](classfeatures/bard-01-6FsusoMYxxjyIkVh.htm)|Spell Repertoire (Bard)|Répertoire de sorts (Barde)|officielle|
|[bard-01-AIOBWGOS4nkfH3kW.htm](classfeatures/bard-01-AIOBWGOS4nkfH3kW.htm)|Muses|Muses|libre|
|[bard-01-N03BtRvjX9TeHRa4.htm](classfeatures/bard-01-N03BtRvjX9TeHRa4.htm)|Warrior|Combattant|libre|
|[bard-01-s0VbbQJNlSgPocui.htm](classfeatures/bard-01-s0VbbQJNlSgPocui.htm)|Composition Spells|Sorts de composition|libre|
|[bard-01-y0jGimYdMGDJWrEq.htm](classfeatures/bard-01-y0jGimYdMGDJWrEq.htm)|Polymath|Touche-à-tout|libre|
|[bard-01-YMBsi4bndRAk5CX4.htm](classfeatures/bard-01-YMBsi4bndRAk5CX4.htm)|Maestro|Virtuose|libre|
|[bard-11-4lp8oG9A3zuqhPBS.htm](classfeatures/bard-11-4lp8oG9A3zuqhPBS.htm)|Bard Weapon Expertise|Expertise avec les armes du barde|officielle|
|[bard-19-NjsOpWbbzUY2Hpk3.htm](classfeatures/bard-19-NjsOpWbbzUY2Hpk3.htm)|Magnum Opus|Magnum opus|libre|
|[champion-00-EtltLdiy9kNfHU0c.htm](classfeatures/champion-00-EtltLdiy9kNfHU0c.htm)|Blade Ally|Allié lame|libre|
|[champion-00-QQP0mu0cyWIwNUh9.htm](classfeatures/champion-00-QQP0mu0cyWIwNUh9.htm)|Shield Ally|Allié bouclier|libre|
|[champion-00-Z6E1O8X7CFcyczB1.htm](classfeatures/champion-00-Z6E1O8X7CFcyczB1.htm)|Steed Ally|Allié monture|libre|
|[champion-01-8YIA0jh64Ecz0TG6.htm](classfeatures/champion-01-8YIA0jh64Ecz0TG6.htm)|Desecrator|Profanateur|libre|
|[champion-01-ehL7mnkqxN5wIkgu.htm](classfeatures/champion-01-ehL7mnkqxN5wIkgu.htm)|Deity and Cause|Divinité et cause|officielle|
|[champion-01-EQ6DVIQHAUXUhY6Y.htm](classfeatures/champion-01-EQ6DVIQHAUXUhY6Y.htm)|Antipaladin|Antipaladin|libre|
|[champion-01-FCoMFUsth4xB4veC.htm](classfeatures/champion-01-FCoMFUsth4xB4veC.htm)|Liberator|Libérateur|libre|
|[champion-01-FeBsYn2mHfMVDZvw.htm](classfeatures/champion-01-FeBsYn2mHfMVDZvw.htm)|Deific Weapon|Arme déifique|officielle|
|[champion-01-fykh5pE99O3I2sOI.htm](classfeatures/champion-01-fykh5pE99O3I2sOI.htm)|Champion's Code|Code du champion|officielle|
|[champion-01-HiIvez0TqESbleB5.htm](classfeatures/champion-01-HiIvez0TqESbleB5.htm)|Tyrant|Tyran|libre|
|[champion-01-JiY2ZB4FkK8RJm4T.htm](classfeatures/champion-01-JiY2ZB4FkK8RJm4T.htm)|The Tenets of Evil|Les principes du Mal|libre|
|[champion-01-nxZYP3KGfTSkaW6J.htm](classfeatures/champion-01-nxZYP3KGfTSkaW6J.htm)|The Tenets of Good|Les principes du Bien|libre|
|[champion-01-peEXunfbSD8WcMFk.htm](classfeatures/champion-01-peEXunfbSD8WcMFk.htm)|Paladin|Paladin|officielle|
|[champion-01-Q1VfQZp49hkhY0HY.htm](classfeatures/champion-01-Q1VfQZp49hkhY0HY.htm)|Devotion Spells|Sorts de dévotion|officielle|
|[champion-01-sXVX4ARUuo8Egrz5.htm](classfeatures/champion-01-sXVX4ARUuo8Egrz5.htm)|Champion's Reaction|Réaction de champion|officielle|
|[champion-01-UyuwFp0jQqYL2AdF.htm](classfeatures/champion-01-UyuwFp0jQqYL2AdF.htm)|Redeemer|Rédempteur|libre|
|[champion-03-ERwuazupczhUSZ73.htm](classfeatures/champion-03-ERwuazupczhUSZ73.htm)|Divine Ally|Allié divin|officielle|
|[champion-09-3XK573A7GH1rrLgO.htm](classfeatures/champion-09-3XK573A7GH1rrLgO.htm)|Divine Smite|Châtiment divin|officielle|
|[champion-09-VgmfNKtQLgBaNi5r.htm](classfeatures/champion-09-VgmfNKtQLgBaNi5r.htm)|Champion Expertise|Expertise du champion|officielle|
|[champion-11-uptzvOLrZ3fctrl2.htm](classfeatures/champion-11-uptzvOLrZ3fctrl2.htm)|Exalt|Éxaltation|officielle|
|[champion-11-xygfZopqXBJ6dKBA.htm](classfeatures/champion-11-xygfZopqXBJ6dKBA.htm)|Divine Will|Résolution divine|officielle|
|[champion-17-voiSCh7ZXA2ogwiC.htm](classfeatures/champion-17-voiSCh7ZXA2ogwiC.htm)|Legendary Armor|Armure légendaire|officielle|
|[champion-17-z5G0o04uV65zyxDB.htm](classfeatures/champion-17-z5G0o04uV65zyxDB.htm)|Champion Mastery|Maîtrise du Champion|officielle|
|[champion-19-LzB6X9vOaq3wq1FZ.htm](classfeatures/champion-19-LzB6X9vOaq3wq1FZ.htm)|Hero's Defiance|Défi du héros|officielle|
|[cleric-01-0Aocw3igLwna9cjp.htm](classfeatures/cleric-01-0Aocw3igLwna9cjp.htm)|Warpriest|Prêtre combattant|libre|
|[cleric-01-aiwxBj5MjnafCMyn.htm](classfeatures/cleric-01-aiwxBj5MjnafCMyn.htm)|First Doctrine (Cloistered Cleric)|Première doctrine (Prêtre cloîtré)|libre|
|[cleric-01-AvNbdGSOTWNRgcxs.htm](classfeatures/cleric-01-AvNbdGSOTWNRgcxs.htm)|Cleric Spellcasting|Incantation de prêtre|libre|
|[cleric-01-DutW12WMFPHBoLTH.htm](classfeatures/cleric-01-DutW12WMFPHBoLTH.htm)|Deity|Divinité|libre|
|[cleric-01-gblTFUOgolqFS9v4.htm](classfeatures/cleric-01-gblTFUOgolqFS9v4.htm)|Divine Font|Source divine|libre|
|[cleric-01-Qejo7FUWQtPTpgWH.htm](classfeatures/cleric-01-Qejo7FUWQtPTpgWH.htm)|First Doctrine|Doctrine : Premier dogme|officielle|
|[cleric-01-tyrBwBTzo5t9Zho7.htm](classfeatures/cleric-01-tyrBwBTzo5t9Zho7.htm)|Doctrine|Doctrine|libre|
|[cleric-01-UV1HlClbWCNcaKBZ.htm](classfeatures/cleric-01-UV1HlClbWCNcaKBZ.htm)|Anathema (Cleric)|Anathème (Prêtre)|officielle|
|[cleric-01-xxkszluN9icAiTO4.htm](classfeatures/cleric-01-xxkszluN9icAiTO4.htm)|First Doctrine (Warpriest)|Première doctrine (Prêtre combattant)|libre|
|[cleric-01-ZZzLMOUAtBVgV1DF.htm](classfeatures/cleric-01-ZZzLMOUAtBVgV1DF.htm)|Cloistered Cleric|Prêtre cloîtré|libre|
|[cleric-03-D34mPo29r1J3DPaX.htm](classfeatures/cleric-03-D34mPo29r1J3DPaX.htm)|Second Doctrine (Warpriest)|Deuxième doctrine (Prêtre combattant)|libre|
|[cleric-03-OnfrrwCfDFCFw0tc.htm](classfeatures/cleric-03-OnfrrwCfDFCFw0tc.htm)|Second Doctrine|Doctrine : deuxième dogme|officielle|
|[cleric-03-sa7BWfnyCswAvBVa.htm](classfeatures/cleric-03-sa7BWfnyCswAvBVa.htm)|Second Doctrine (Cloistered Cleric)|Deuxième doctrine (Prêtre cloîtré)|libre|
|[cleric-07-gxNxfN9OBlQ1icus.htm](classfeatures/cleric-07-gxNxfN9OBlQ1icus.htm)|Third Doctrine|Doctrine : Troisième dogme|officielle|
|[cleric-07-s8WEmc4GGZSHSC7q.htm](classfeatures/cleric-07-s8WEmc4GGZSHSC7q.htm)|Third Doctrine (Cloistered Cleric)|Troisième doctrine (Prêtre cloîtré)|libre|
|[cleric-07-Zp81uTBItG1xlH4O.htm](classfeatures/cleric-07-Zp81uTBItG1xlH4O.htm)|Third Doctrine (Warpriest)|Troisième doctrine (Prêtre combattant)|libre|
|[cleric-09-0TjNGzs0FuD7JBf4.htm](classfeatures/cleric-09-0TjNGzs0FuD7JBf4.htm)|Resolute Faith|Foi résolue|libre|
|[cleric-11-o8nHreMyiLi64rZz.htm](classfeatures/cleric-11-o8nHreMyiLi64rZz.htm)|Fourth Doctrine|Doctrine : quatrième dogme|libre|
|[cleric-11-px3gVYp7zlEQIpcl.htm](classfeatures/cleric-11-px3gVYp7zlEQIpcl.htm)|Fourth Doctrine (Warpriest)|Quatrième doctrine (Prêtre combattant)|officielle|
|[cleric-11-vxOf4LXZcqUG3P7a.htm](classfeatures/cleric-11-vxOf4LXZcqUG3P7a.htm)|Fourth Doctrine (Cloistered Cleric)|Quatrième doctrine (Prêtre cloîtré)|libre|
|[cleric-13-0mJTp4LdEHBLInoe.htm](classfeatures/cleric-13-0mJTp4LdEHBLInoe.htm)|Divine Defense|Défense divine|officielle|
|[cleric-15-kmimy4VOaoEOgOiQ.htm](classfeatures/cleric-15-kmimy4VOaoEOgOiQ.htm)|Fifth Doctrine (Warpriest)|Cinquième doctrine (Prêtre combattant)|libre|
|[cleric-15-n9W8MjjRgPpUTvWf.htm](classfeatures/cleric-15-n9W8MjjRgPpUTvWf.htm)|Fifth Doctrine (Cloistered Cleric)|Cinquième doctrine (Prêtre cloîtré)|libre|
|[cleric-15-Zb7DuGbFoLEp0H1K.htm](classfeatures/cleric-15-Zb7DuGbFoLEp0H1K.htm)|Fifth Doctrine|Doctrine : Cinquième dogme|officielle|
|[cleric-19-3uf31A91h3ywmlqm.htm](classfeatures/cleric-19-3uf31A91h3ywmlqm.htm)|Miraculous Spell|Sort miraculeux|libre|
|[cleric-19-DgGefatQ4v6xT6f9.htm](classfeatures/cleric-19-DgGefatQ4v6xT6f9.htm)|Final Doctrine (Cloistered Cleric)|Dernière doctrine (Prêtre cloîtré)|officielle|
|[cleric-19-N1ugDqZlslxbp3Uy.htm](classfeatures/cleric-19-N1ugDqZlslxbp3Uy.htm)|Final Doctrine (Warpriest)|Dernière doctrine (Prêtre combattant)|libre|
|[cleric-19-urBGOPrUwBmkixAo.htm](classfeatures/cleric-19-urBGOPrUwBmkixAo.htm)|Final Doctrine|Dernier dogme|officielle|
|[druid-01-8STJEFVJISujgpMR.htm](classfeatures/druid-01-8STJEFVJISujgpMR.htm)|Druidic Order|Ordre druidique|libre|
|[druid-01-acqqlYmti8D9QJi0.htm](classfeatures/druid-01-acqqlYmti8D9QJi0.htm)|Storm Order|Ordre de la tempête|libre|
|[druid-01-b8pnRxGuNzG0buuh.htm](classfeatures/druid-01-b8pnRxGuNzG0buuh.htm)|Druid Spellcasting|Incantation de druide|libre|
|[druid-01-d5BFFHXFJYKs5LXr.htm](classfeatures/druid-01-d5BFFHXFJYKs5LXr.htm)|Voice of Nature|Voix de la nature|libre|
|[druid-01-fKTewWlYgFuhl4KA.htm](classfeatures/druid-01-fKTewWlYgFuhl4KA.htm)|Stone Order|Ordre de la pierre|libre|
|[druid-01-FuUXyv2yBs7zRgqT.htm](classfeatures/druid-01-FuUXyv2yBs7zRgqT.htm)|Wave Order|Ordre de la vague|libre|
|[druid-01-NdeFvIXdHwKYLiUj.htm](classfeatures/druid-01-NdeFvIXdHwKYLiUj.htm)|Flame Order|Ordre de la flamme|libre|
|[druid-01-nfBn8QB6HVdzpTFV.htm](classfeatures/druid-01-nfBn8QB6HVdzpTFV.htm)|Anathema (Druid)|Anathème (druide)|libre|
|[druid-01-POBvoXifa9HaejAg.htm](classfeatures/druid-01-POBvoXifa9HaejAg.htm)|Animal Order|Ordre animal|libre|
|[druid-01-RiAGlnnp4S21BAG3.htm](classfeatures/druid-01-RiAGlnnp4S21BAG3.htm)|Wildsong|Chant sauvage|libre|
|[druid-01-u4nlOzPj2WHkIj9l.htm](classfeatures/druid-01-u4nlOzPj2WHkIj9l.htm)|Leaf Order|Ordre de la feuille|libre|
|[druid-01-v0EjtiwdeMj8ykI0.htm](classfeatures/druid-01-v0EjtiwdeMj8ykI0.htm)|Untamed Order|Ordre indompté|libre|
|[druid-11-Ra32tlqBxHzT6fzN.htm](classfeatures/druid-11-Ra32tlqBxHzT6fzN.htm)|Druid Weapon Expertise|Expertise avec les armes de druide|officielle|
|[druid-11-yqaRnXFjRnFWsWvY.htm](classfeatures/druid-11-yqaRnXFjRnFWsWvY.htm)|Wild Willpower|Détermination sauvage|libre|
|[druid-19-nzgb43mQmLgaqDoQ.htm](classfeatures/druid-19-nzgb43mQmLgaqDoQ.htm)|Primal Hierophant|Hiérophante primordial|libre|
|[fighter-01-eZNCckLzbH3GyncH.htm](classfeatures/fighter-01-eZNCckLzbH3GyncH.htm)|Shield Block|Blocage au bouclier|libre|
|[fighter-01-hmShTfPOcTaKgbf4.htm](classfeatures/fighter-01-hmShTfPOcTaKgbf4.htm)|Reactive Strike|Frappe réactive|libre|
|[fighter-03-GJKJafDGuX4BeAeN.htm](classfeatures/fighter-03-GJKJafDGuX4BeAeN.htm)|Bravery|Bravoure|libre|
|[fighter-05-gApJtAdNb9ST4Ms9.htm](classfeatures/fighter-05-gApJtAdNb9ST4Ms9.htm)|Fighter Weapon Mastery|Maîtrise martiale du guerrier|officielle|
|[fighter-07-TIvzBALymvb56L79.htm](classfeatures/fighter-07-TIvzBALymvb56L79.htm)|Battlefield Surveyor|Arpenteur du champ de bataille|libre|
|[fighter-09-8g6HzARbhfcgilP8.htm](classfeatures/fighter-09-8g6HzARbhfcgilP8.htm)|Combat Flexibility|Flexibilité martiale|libre|
|[fighter-09-gdSmSTaGDxf4g2d8.htm](classfeatures/fighter-09-gdSmSTaGDxf4g2d8.htm)|Battle Hardened|Endurci au combat|libre|
|[fighter-11-bAaI7h937Nr3g93U.htm](classfeatures/fighter-11-bAaI7h937Nr3g93U.htm)|Fighter Expertise|Expertise du guerrier|libre|
|[fighter-13-F5VenhIQMDkeGvmV.htm](classfeatures/fighter-13-F5VenhIQMDkeGvmV.htm)|Weapon Legend|Légende des armes|libre|
|[fighter-15-2ImD2TB0RVic1VUc.htm](classfeatures/fighter-15-2ImD2TB0RVic1VUc.htm)|Tempered Reflexes|Réflexes exercés|libre|
|[fighter-15-W2rwudMNcAxs8VoX.htm](classfeatures/fighter-15-W2rwudMNcAxs8VoX.htm)|Improved Flexibility|Flexibilité améliorée|officielle|
|[fighter-19-0H2LxtiZTJ275pSD.htm](classfeatures/fighter-19-0H2LxtiZTJ275pSD.htm)|Versatile Legend|Légende polyvalente|officielle|
|[gunslinger-01-a3pSIKkDVTvvNSRO.htm](classfeatures/gunslinger-01-a3pSIKkDVTvvNSRO.htm)|Way of the Drifter|Voie du vagabond|libre|
|[gunslinger-01-LDqVxLKrwEqSegiu.htm](classfeatures/gunslinger-01-LDqVxLKrwEqSegiu.htm)|Gunslinger's Way|Voies du franc-tireur|libre|
|[gunslinger-01-OmgtSDV1FubDUqWR.htm](classfeatures/gunslinger-01-OmgtSDV1FubDUqWR.htm)|Way of the Spellshot|Voie du sortiléro|libre|
|[gunslinger-01-qRLRrHf0kzaJ7xt0.htm](classfeatures/gunslinger-01-qRLRrHf0kzaJ7xt0.htm)|Way of the Pistolero|Voie du pistoléro|libre|
|[gunslinger-01-QWXvksGJhOjXbBqi.htm](classfeatures/gunslinger-01-QWXvksGJhOjXbBqi.htm)|Way of the Sniper|Voie du tireur d'élite|libre|
|[gunslinger-01-vB0yVFxJVZwalt2g.htm](classfeatures/gunslinger-01-vB0yVFxJVZwalt2g.htm)|Way of the Vanguard|Voie de l'avant-garde|libre|
|[gunslinger-01-vXbk7Nm1TOTTUNvF.htm](classfeatures/gunslinger-01-vXbk7Nm1TOTTUNvF.htm)|Singular Expertise|Expertise singulière|libre|
|[gunslinger-01-YryaWAGcHeaRnXzS.htm](classfeatures/gunslinger-01-YryaWAGcHeaRnXzS.htm)|Way of the Triggerbrand|Voie du pistolame|libre|
|[gunslinger-03-Wpdeh6EkcAKA60oH.htm](classfeatures/gunslinger-03-Wpdeh6EkcAKA60oH.htm)|Stubborn|Obstiné|libre|
|[gunslinger-05-9nRT8aq05Fy2D3i3.htm](classfeatures/gunslinger-05-9nRT8aq05Fy2D3i3.htm)|Gunslinger Weapon Mastery|Maîtrise des armes du franc-tireur|libre|
|[gunslinger-09-3JLW5vPshsJf3nCY.htm](classfeatures/gunslinger-09-3JLW5vPshsJf3nCY.htm)|Advanced Deed|Exploit avancé|libre|
|[gunslinger-09-aKr6OE8vI2BsJzf1.htm](classfeatures/gunslinger-09-aKr6OE8vI2BsJzf1.htm)|Gunslinger Expertise|Expertise du franc-tireur|libre|
|[gunslinger-13-ULOAZWZEokbJC6Rq.htm](classfeatures/gunslinger-13-ULOAZWZEokbJC6Rq.htm)|Gunslinging Legend|Légende de la gâchette|libre|
|[gunslinger-15-yc9RuXXxmZ9YidH6.htm](classfeatures/gunslinger-15-yc9RuXXxmZ9YidH6.htm)|Greater Deed|Exploit supérieur|libre|
|[gunslinger-17-RkofVX55ciXZyfAA.htm](classfeatures/gunslinger-17-RkofVX55ciXZyfAA.htm)|Shootist's Edge|Avantage du tireur|libre|
|[inventor-01-5Xj38QeMKcFdrzqH.htm](classfeatures/inventor-01-5Xj38QeMKcFdrzqH.htm)|Phlogistonic Regulator|Régulateur phlogistonique|libre|
|[inventor-01-bok3P78CMchFibxC.htm](classfeatures/inventor-01-bok3P78CMchFibxC.htm)|Weapon Innovation|Innovation d'arme|libre|
|[inventor-01-dsy2w4LfjMIWgy5D.htm](classfeatures/inventor-01-dsy2w4LfjMIWgy5D.htm)|Harmonic Oscillator|Oscillateur harmonique|libre|
|[inventor-01-fOulT6iKVqIK4jJX.htm](classfeatures/inventor-01-fOulT6iKVqIK4jJX.htm)|Razor Prongs|Pointes de rasoir|libre|
|[inventor-01-fpwtpm8pdwO1I6MO.htm](classfeatures/inventor-01-fpwtpm8pdwO1I6MO.htm)|Armor Innovation|Innovation d'armure|libre|
|[inventor-01-JH6um0St37UrjLNG.htm](classfeatures/inventor-01-JH6um0St37UrjLNG.htm)|Peerless Inventor|Inventeur hors pair|libre|
|[inventor-01-jHE4fPwU0sSIAjMo.htm](classfeatures/inventor-01-jHE4fPwU0sSIAjMo.htm)|Otherworldly Protection|Protection surnaturelle|libre|
|[inventor-01-jIAgXe2FetAKBwt7.htm](classfeatures/inventor-01-jIAgXe2FetAKBwt7.htm)|Innovation|Innovation|libre|
|[inventor-01-Nbg4ZllDI9uCowZL.htm](classfeatures/inventor-01-Nbg4ZllDI9uCowZL.htm)|Hefty Composition|Composition volumineuse|libre|
|[inventor-01-O3r84Uv6HytaSIbX.htm](classfeatures/inventor-01-O3r84Uv6HytaSIbX.htm)|Blunt Shot|Tir émoussé|libre|
|[inventor-01-o70O2FysDd7BS9e0.htm](classfeatures/inventor-01-o70O2FysDd7BS9e0.htm)|Construct Innovation|Innovation créature artificielle|libre|
|[inventor-01-O9wpXEKtKYJOMIlK.htm](classfeatures/inventor-01-O9wpXEKtKYJOMIlK.htm)|Dynamic Weighting|Poids dynamique|libre|
|[inventor-01-OkxoJWrOXhM25mhi.htm](classfeatures/inventor-01-OkxoJWrOXhM25mhi.htm)|Muscular Exoskeleton|Exosquelette musculaire|libre|
|[inventor-01-oP5zM5Yu41xcx3iu.htm](classfeatures/inventor-01-oP5zM5Yu41xcx3iu.htm)|Overdrive|Surrégime|libre|
|[inventor-01-pEm1RTNuzzQVKkR0.htm](classfeatures/inventor-01-pEm1RTNuzzQVKkR0.htm)|Explode|Exploser|libre|
|[inventor-01-pSXlZggdCCbkQqNr.htm](classfeatures/inventor-01-pSXlZggdCCbkQqNr.htm)|Pacification Tools|Outils de pacification|libre|
|[inventor-01-qIOKqT93h6CX6V4k.htm](classfeatures/inventor-01-qIOKqT93h6CX6V4k.htm)|Complex Simplicity|Simplicité complexe|libre|
|[inventor-01-qwhfPgE2tTW0hvPe.htm](classfeatures/inventor-01-qwhfPgE2tTW0hvPe.htm)|Modular Head|Tête configurable|libre|
|[inventor-01-R8cfRNPdaCkd2bud.htm](classfeatures/inventor-01-R8cfRNPdaCkd2bud.htm)|Hampering Spikes|Piques gênantes|libre|
|[inventor-01-sTWY6PLqr1X7icgZ.htm](classfeatures/inventor-01-sTWY6PLqr1X7icgZ.htm)|Speed Boosters|Accélérateurs de vitesse|libre|
|[inventor-01-X3TtVdNhrydeQ3SX.htm](classfeatures/inventor-01-X3TtVdNhrydeQ3SX.htm)|Subtle Dampeners|Amortisseurs subtils|libre|
|[inventor-01-xndlv9T3JgYYUtf8.htm](classfeatures/inventor-01-xndlv9T3JgYYUtf8.htm)|Metallic Reactance|Réactance métallique|libre|
|[inventor-01-Z1au5zxYcjZvdQpd.htm](classfeatures/inventor-01-Z1au5zxYcjZvdQpd.htm)|Entangling Form|Forme enchevêtrante|libre|
|[inventor-01-ZCfPjOn6JJ8Zrgvg.htm](classfeatures/inventor-01-ZCfPjOn6JJ8Zrgvg.htm)|Segmented Frame|Cadre segmenté|libre|
|[inventor-03-J46wcNqKXvtokBD1.htm](classfeatures/inventor-03-J46wcNqKXvtokBD1.htm)|Reconfigure|Reconfigurer|libre|
|[inventor-03-YMKxN56w617BYwu4.htm](classfeatures/inventor-03-YMKxN56w617BYwu4.htm)|Expert Overdrive|Surrégime expert|libre|
|[inventor-05-0NyPgi6UACMTmAGE.htm](classfeatures/inventor-05-0NyPgi6UACMTmAGE.htm)|Inventor Weapon Expertise|Expertise avec les armes d'inventeur|libre|
|[inventor-07-7Zw83ysONrNhJMr8.htm](classfeatures/inventor-07-7Zw83ysONrNhJMr8.htm)|Aerodynamic Construction|Construction aerodynamique|libre|
|[inventor-07-h1hNr4jABip2ERDd.htm](classfeatures/inventor-07-h1hNr4jABip2ERDd.htm)|Inconspicuous Appearance|Apparence discrète|libre|
|[inventor-07-hwqaS9m2RwGe0kJk.htm](classfeatures/inventor-07-hwqaS9m2RwGe0kJk.htm)|Layered Mesh|Mailles superposées|libre|
|[inventor-07-hxjdHBJHaB47mBUT.htm](classfeatures/inventor-07-hxjdHBJHaB47mBUT.htm)|Camouflage Pigmentation|Pigmentation de camouflage|libre|
|[inventor-07-P1GbGEePC8zDi8K4.htm](classfeatures/inventor-07-P1GbGEePC8zDi8K4.htm)|Advanced Rangefinder|Télémétrie avancée|libre|
|[inventor-07-ptBLVvRqn1fA3A4l.htm](classfeatures/inventor-07-ptBLVvRqn1fA3A4l.htm)|Rope Shot|Tir de corde|libre|
|[inventor-07-SRS8102DGTzJ4NmC.htm](classfeatures/inventor-07-SRS8102DGTzJ4NmC.htm)|Tensile Absorption|Absorption des chocs|libre|
|[inventor-07-svGLZdD4PF7miAKA.htm](classfeatures/inventor-07-svGLZdD4PF7miAKA.htm)|Dense Plating|Plaquage dense|libre|
|[inventor-07-SXv9bJFbntDOMRIL.htm](classfeatures/inventor-07-SXv9bJFbntDOMRIL.htm)|Master Overdrive|Surrégime maître|libre|
|[inventor-07-T3742fu7oIM3W6Gz.htm](classfeatures/inventor-07-T3742fu7oIM3W6Gz.htm)|Enhanced Resistance|Résistance améliorée|libre|
|[inventor-07-tPL4lrGLCB5XbgeV.htm](classfeatures/inventor-07-tPL4lrGLCB5XbgeV.htm)|Integrated Gauntlet|Gantelet intégré|libre|
|[inventor-07-V5qmKLmI3RL68RGb.htm](classfeatures/inventor-07-V5qmKLmI3RL68RGb.htm)|Hyper Boosters|Hyper accélérateurs|libre|
|[inventor-07-WxkZMlETXo165XnC.htm](classfeatures/inventor-07-WxkZMlETXo165XnC.htm)|Tangle Line|Ligne de rappel|libre|
|[inventor-07-y62b9R6RUBbkZECA.htm](classfeatures/inventor-07-y62b9R6RUBbkZECA.htm)|Manifold Alloy|Alliage feuilleté|libre|
|[inventor-07-yEfHCwsbsbP6K13Q.htm](classfeatures/inventor-07-yEfHCwsbsbP6K13Q.htm)|Heavy Construction|Construction lourde|libre|
|[inventor-07-ySkTxnoNuDluk6Cb.htm](classfeatures/inventor-07-ySkTxnoNuDluk6Cb.htm)|Antimagic Plating|Plaquage antimagie|libre|
|[inventor-09-F8oXHnu9iNTcpXbJ.htm](classfeatures/inventor-09-F8oXHnu9iNTcpXbJ.htm)|Offensive Boost|Renfort offensif|libre|
|[inventor-09-mQVC1iDyNi2tfsF8.htm](classfeatures/inventor-09-mQVC1iDyNi2tfsF8.htm)|Inventive Expertise|Expertise inventive|libre|
|[inventor-13-j0klWHkH3AxUAgok.htm](classfeatures/inventor-13-j0klWHkH3AxUAgok.htm)|Complete Reconfiguration|Reconfiguration complète|libre|
|[inventor-13-mJpPaoVlNmTK47x1.htm](classfeatures/inventor-13-mJpPaoVlNmTK47x1.htm)|Inventor Weapon Mastery|Maîtrise des armes d'inventeur|libre|
|[inventor-15-o1omL2LdHvjEwh3P.htm](classfeatures/inventor-15-o1omL2LdHvjEwh3P.htm)|Legendary Overdrive|Surrégime légendaire|libre|
|[inventor-15-tXbadIT3LzwuSR19.htm](classfeatures/inventor-15-tXbadIT3LzwuSR19.htm)|Revolutionary Innovation|Innovation révolutionnaire|libre|
|[inventor-15-vUt71oX9QNLAUmZn.htm](classfeatures/inventor-15-vUt71oX9QNLAUmZn.htm)|Energy Barrier|Barrière d'énergie|libre|
|[inventor-17-Uu8VnpAo3XZZEKPd.htm](classfeatures/inventor-17-Uu8VnpAo3XZZEKPd.htm)|Inventive Mastery|Maîtrise inventive|libre|
|[inventor-19-rOaLbipkComjc6qh.htm](classfeatures/inventor-19-rOaLbipkComjc6qh.htm)|Infinite Invention|Invention infinie|libre|
|[investigator-01-2Fe4YZCvAr9Yf6w7.htm](classfeatures/investigator-01-2Fe4YZCvAr9Yf6w7.htm)|Strategic Strike|Frappe stratégique|libre|
|[investigator-01-6FasgIXUJ1X8ekRn.htm](classfeatures/investigator-01-6FasgIXUJ1X8ekRn.htm)|On the Case|Sur l'affaire|libre|
|[investigator-01-g3mNzNphtVxyR9Xr.htm](classfeatures/investigator-01-g3mNzNphtVxyR9Xr.htm)|Empiricism Methodology|Méthodologie : empirisme|libre|
|[investigator-01-lgo65ldX7WkXC8Ir.htm](classfeatures/investigator-01-lgo65ldX7WkXC8Ir.htm)|Devise a Stratagem|Concevoir un stratagème|libre|
|[investigator-01-ln2Y1a4SxlU9sizX.htm](classfeatures/investigator-01-ln2Y1a4SxlU9sizX.htm)|Alchemical Sciences Methodology|Méthodologie : Sciences alchimiques|libre|
|[investigator-01-O3IX7rTxXWWvDVM3.htm](classfeatures/investigator-01-O3IX7rTxXWWvDVM3.htm)|Forensic Medicine Methodology|Méthodologie : Médecine forensique|libre|
|[investigator-01-uhHg9BXBiHpL5ndS.htm](classfeatures/investigator-01-uhHg9BXBiHpL5ndS.htm)|Methodology|Méthodologie|libre|
|[investigator-01-UIHUNNYZyQ3p4Vmo.htm](classfeatures/investigator-01-UIHUNNYZyQ3p4Vmo.htm)|Interrogation Methodology|Méthodologie : Interrogatoire|libre|
|[investigator-03-dmK1wya8GBi9MmCB.htm](classfeatures/investigator-03-dmK1wya8GBi9MmCB.htm)|Skillful Lessons|Leçons accomplies|libre|
|[investigator-03-DZWQspPi4IkfXV2E.htm](classfeatures/investigator-03-DZWQspPi4IkfXV2E.htm)|Keen Recollection|Souvenirs affûtés|libre|
|[investigator-09-PFvB79O2VFdiAeSj.htm](classfeatures/investigator-09-PFvB79O2VFdiAeSj.htm)|Investigator Expertise|Expertise de l'enquêteur|libre|
|[investigator-11-malYpr0CYL4fDGhr.htm](classfeatures/investigator-11-malYpr0CYL4fDGhr.htm)|Deductive Improvisation|Improvisation déductive|libre|
|[investigator-19-flEx8eY0NinF9XZU.htm](classfeatures/investigator-19-flEx8eY0NinF9XZU.htm)|Master Detective|Maître détective|libre|
|[kineticist-01-f0BF2210l9k66cso.htm](classfeatures/kineticist-01-f0BF2210l9k66cso.htm)|Kinetic Aura|Aura kinétique|libre|
|[kineticist-01-IDBB44egwiQ4qZNh.htm](classfeatures/kineticist-01-IDBB44egwiQ4qZNh.htm)|Kinetic Gate|Portail kinétique|libre|
|[kineticist-01-xhCYkt3acfFd0hr2.htm](classfeatures/kineticist-01-xhCYkt3acfFd0hr2.htm)|Impulses|Impulsions|libre|
|[kineticist-03-jyCEC3eC4B6YaGoy.htm](classfeatures/kineticist-03-jyCEC3eC4B6YaGoy.htm)|Extract Element|Extraction d'élément|libre|
|[kineticist-05-jx70hPakuTgB3lM5.htm](classfeatures/kineticist-05-jx70hPakuTgB3lM5.htm)|Gate Junction|Jonction de portail|libre|
|[kineticist-05-vEiENk0IzuzRQesp.htm](classfeatures/kineticist-05-vEiENk0IzuzRQesp.htm)|Gate's Threshold|Premier seuil du portail|libre|
|[kineticist-07-dGiMAdZEQoXQWJbn.htm](classfeatures/kineticist-07-dGiMAdZEQoXQWJbn.htm)|Kinetic Expertise|Expertise kinétique|libre|
|[kineticist-07-eTHq1Cwf1pOvsx2R.htm](classfeatures/kineticist-07-eTHq1Cwf1pOvsx2R.htm)|Kinetic Durability|Durabilité kinétique|libre|
|[kineticist-09-N5Gnf73TvuO6Reoj.htm](classfeatures/kineticist-09-N5Gnf73TvuO6Reoj.htm)|Second Gate's Threshold|Deuxième seuil du portail|libre|
|[kineticist-11-9BUk7zy1aE092XCS.htm](classfeatures/kineticist-11-9BUk7zy1aE092XCS.htm)|Kinetic Quickness|Rapidité kinétique|libre|
|[kineticist-11-SIkSIln5G1J73PrM.htm](classfeatures/kineticist-11-SIkSIln5G1J73PrM.htm)|Reflow Elements|Reflux des éléments|libre|
|[kineticist-13-GfQ5MngIn74OYhQL.htm](classfeatures/kineticist-13-GfQ5MngIn74OYhQL.htm)|Third Gate's Threshold|Troisième seuil du portail|libre|
|[kineticist-15-a6fIKxCViZwwHZLJ.htm](classfeatures/kineticist-15-a6fIKxCViZwwHZLJ.htm)|Kinetic Mastery|Maîtrise kinétique|libre|
|[kineticist-15-i3qjbhL7uukg9I80.htm](classfeatures/kineticist-15-i3qjbhL7uukg9I80.htm)|Greater Kinetic Durability|Durabilité kinétique supérieure|libre|
|[kineticist-17-DvuPpIaM4BkHLX4y.htm](classfeatures/kineticist-17-DvuPpIaM4BkHLX4y.htm)|Double Reflow|Reflux double|libre|
|[kineticist-17-o8QHrhc9MsWIoKUv.htm](classfeatures/kineticist-17-o8QHrhc9MsWIoKUv.htm)|Fourth Gate's Threshold|Quatrième seuil du portail|libre|
|[kineticist-19-8ZAc21SvRO2rfPez.htm](classfeatures/kineticist-19-8ZAc21SvRO2rfPez.htm)|Kinetic Legend|Légende kinétique|libre|
|[kineticist-19-DPxpBIxPidJfn0Xv.htm](classfeatures/kineticist-19-DPxpBIxPidJfn0Xv.htm)|Final Gate|Portail final|libre|
|[magus-01-09iL38CZZEa0q0Mt.htm](classfeatures/magus-01-09iL38CZZEa0q0Mt.htm)|Arcane Cascade|Cascade arcanique|libre|
|[magus-01-3gVDqDPSz4fB5T9G.htm](classfeatures/magus-01-3gVDqDPSz4fB5T9G.htm)|Laughing Shadow|Ombre ricanante|libre|
|[magus-01-6YJ8KFl7THkVy6Gm.htm](classfeatures/magus-01-6YJ8KFl7THkVy6Gm.htm)|Twisting Tree|Arbre tordu|libre|
|[magus-01-FkbFgmoVz5lHhSMo.htm](classfeatures/magus-01-FkbFgmoVz5lHhSMo.htm)|Conflux Spells|Sorts de confluence|libre|
|[magus-01-FTeIs1Z1Qeli4BIF.htm](classfeatures/magus-01-FTeIs1Z1Qeli4BIF.htm)|Hybrid Study|Études hybridées|libre|
|[magus-01-KVj5ofUwu3VJSrVw.htm](classfeatures/magus-01-KVj5ofUwu3VJSrVw.htm)|Spellstrike|Frappe de sort|libre|
|[magus-01-maGzhKLmgubAdUlN.htm](classfeatures/magus-01-maGzhKLmgubAdUlN.htm)|Sparkling Targe|Targe étincelante|libre|
|[magus-01-Pew7duAozEeAemif.htm](classfeatures/magus-01-Pew7duAozEeAemif.htm)|Starlit Span|Portée des étoiles|libre|
|[magus-01-wXaz41gwqNtTn6tf.htm](classfeatures/magus-01-wXaz41gwqNtTn6tf.htm)|Arcane Spellcasting (Magus)|Incantation arcanique (Magus)|libre|
|[magus-01-ZslXrvYRxHBXc1Ds.htm](classfeatures/magus-01-ZslXrvYRxHBXc1Ds.htm)|Inexorable Iron|Acier inexorable|libre|
|[magus-07-6HCI2iHyBZAr7a4P.htm](classfeatures/magus-07-6HCI2iHyBZAr7a4P.htm)|Studious Spells|Sorts studieux|libre|
|[magus-19-VmPIJomEdmgGrCMS.htm](classfeatures/magus-19-VmPIJomEdmgGrCMS.htm)|Double Spellstrike|Double frappe de sort|libre|
|[monk-01-NLHHHiAcdnZ5ohc2.htm](classfeatures/monk-01-NLHHHiAcdnZ5ohc2.htm)|Flurry of Blows|Déluge de coups|officielle|
|[monk-01-SB8UJ8rZmvbcBweJ.htm](classfeatures/monk-01-SB8UJ8rZmvbcBweJ.htm)|Powerful Fist|Poings puissants|officielle|
|[monk-03-Cq6NjvcKZOMySBVj.htm](classfeatures/monk-03-Cq6NjvcKZOMySBVj.htm)|Incredible Movement|Déplacement extraordinaire|officielle|
|[monk-03-D2AE8RfMlZ3D1FuV.htm](classfeatures/monk-03-D2AE8RfMlZ3D1FuV.htm)|Mystic Strikes|Frappes mystiques|officielle|
|[monk-05-VgZIutWjFl8oZQFi.htm](classfeatures/monk-05-VgZIutWjFl8oZQFi.htm)|Expert Strikes|Frappes expertes|officielle|
|[monk-07-1K6m6AVmn3r8XZ9d.htm](classfeatures/monk-07-1K6m6AVmn3r8XZ9d.htm)|Path to Perfection|Voie vers la perfection|libre|
|[monk-07-7lanxgmoOHNdtDe2.htm](classfeatures/monk-07-7lanxgmoOHNdtDe2.htm)|Path to Perfection (Will)|Voie vers la Perfection (Volonté)|officielle|
|[monk-07-Bwr9G9IR4ynm5wzz.htm](classfeatures/monk-07-Bwr9G9IR4ynm5wzz.htm)|Path to Perfection (Reflex)|Voie vers la perfection (Réflexes)|officielle|
|[monk-07-KIqptJsjq9pS9CP7.htm](classfeatures/monk-07-KIqptJsjq9pS9CP7.htm)|Path to Perfection (Fortitude)|Voie vers la Perfection (Vigueur)|officielle|
|[monk-09-CoRfFkisEsHE1e43.htm](classfeatures/monk-09-CoRfFkisEsHE1e43.htm)|Metal Strikes|Frappes de métal|officielle|
|[monk-09-lxImO5D0qWp0gXFB.htm](classfeatures/monk-09-lxImO5D0qWp0gXFB.htm)|Monk Expertise|Expertise du moine|officielle|
|[monk-11-RVPhB0RqmoJg7xI6.htm](classfeatures/monk-11-RVPhB0RqmoJg7xI6.htm)|Second Path to Perfection (Will)|Deuxième voie vers la Perfection (Volonté)|libre|
|[monk-11-XZnPwZ0ohlDXlFea.htm](classfeatures/monk-11-XZnPwZ0ohlDXlFea.htm)|Second Path to Perfection (Fortitude)|Deuxième voie vers la Perfection (Vigueur)|libre|
|[monk-11-y6qnbUc8y0815QNE.htm](classfeatures/monk-11-y6qnbUc8y0815QNE.htm)|Second Path to Perfection|Deuxième voie vers la perfection|libre|
|[monk-11-yDL9l9Klki6gE2ZD.htm](classfeatures/monk-11-yDL9l9Klki6gE2ZD.htm)|Second Path to Perfection (Reflex)|Deuxième voie vers la Perfection (Réflexes)|libre|
|[monk-13-0iidKkzC2yy13lIf.htm](classfeatures/monk-13-0iidKkzC2yy13lIf.htm)|Master Strikes|Frappes de maître|officielle|
|[monk-13-95LI24ZSx0d4qfKX.htm](classfeatures/monk-13-95LI24ZSx0d4qfKX.htm)|Graceful Mastery|Maîtrise gracieuse|officielle|
|[monk-15-8kukH9c4h82e3qjl.htm](classfeatures/monk-15-8kukH9c4h82e3qjl.htm)|Third Path to Perfection (Reflex)|Troisième voie vers la perfection (Réflexes)|libre|
|[monk-15-dUMsM0yDTCdV31p6.htm](classfeatures/monk-15-dUMsM0yDTCdV31p6.htm)|Third Path to Perfection (Fortitude)|Troisième voie vers la perfection (Vigueur)|libre|
|[monk-15-haoTkr2U5k7kaAKN.htm](classfeatures/monk-15-haoTkr2U5k7kaAKN.htm)|Third Path to Perfection|Troisième voie vers la perfection|libre|
|[monk-15-oVNRYF0FHbH8NsJD.htm](classfeatures/monk-15-oVNRYF0FHbH8NsJD.htm)|Third Path to Perfection (Will)|Troisième voie vers la perfection (Volonté)|libre|
|[monk-17-5cthRUkRqRtduVvN.htm](classfeatures/monk-17-5cthRUkRqRtduVvN.htm)|Adamantine Strikes|Frappes d'adamantium|officielle|
|[monk-17-JWDfzYub3JfuEtth.htm](classfeatures/monk-17-JWDfzYub3JfuEtth.htm)|Graceful Legend|Légende gracieuse|officielle|
|[monk-19-KmTfg7Sg5va4yU00.htm](classfeatures/monk-19-KmTfg7Sg5va4yU00.htm)|Perfected Form|Forme parfaite|officielle|
|[None-00-6RAHnACKReBW68Sa.htm](classfeatures/None-00-6RAHnACKReBW68Sa.htm)|Encroaching Presence|Présence envahissante|libre|
|[None-00-7PutMrgKIaAeKBuU.htm](classfeatures/None-00-7PutMrgKIaAeKBuU.htm)|Catharsis Emotion (Fear)|Émotion cathartique (Peur)|libre|
|[None-00-8Jjy7VsQAJJqCeyE.htm](classfeatures/None-00-8Jjy7VsQAJJqCeyE.htm)|Wraith Deviant Classification|Classification des déviances d'âme-en-peine|libre|
|[None-00-8PjTI21Mif26XWY7.htm](classfeatures/None-00-8PjTI21Mif26XWY7.htm)|Energetic Meltdown|Effondrement énergétique|libre|
|[None-00-ccrTv2j7eplr2JU8.htm](classfeatures/None-00-ccrTv2j7eplr2JU8.htm)|Catharsis Emotion (Pride)|Émotion cathartique (Fierté)|libre|
|[None-00-eNFYMQMS2BszErCX.htm](classfeatures/None-00-eNFYMQMS2BszErCX.htm)|Catharsis Emotion (Dedication)|Émotion cathartique (Dévouement)|libre|
|[None-00-HPYo5j8GgQIVoOOr.htm](classfeatures/None-00-HPYo5j8GgQIVoOOr.htm)|Troll Deviant Classification|Classification des déviances de troll|libre|
|[None-00-huDGjojm3hnuA1E8.htm](classfeatures/None-00-huDGjojm3hnuA1E8.htm)|Strained Metabolism|Métabolisme sous tension|libre|
|[None-00-igMHwREgpM9GsvLs.htm](classfeatures/None-00-igMHwREgpM9GsvLs.htm)|Order of the Gate|Ordre du portail|officielle|
|[None-00-lDjKVZ48zbqQf3CU.htm](classfeatures/None-00-lDjKVZ48zbqQf3CU.htm)|Catharsis Emotion (Love)|Émotion cathartique (Amour)|libre|
|[None-00-lVdfcITy5bkywW5f.htm](classfeatures/None-00-lVdfcITy5bkywW5f.htm)|Order of the Rack|Ordre du chevalet|officielle|
|[None-00-MizJPiKnopfpGmvw.htm](classfeatures/None-00-MizJPiKnopfpGmvw.htm)|Catharsis Emotion (Anger)|Émotion cathartique (Colère)|libre|
|[None-00-R41sy7weOd0JhOiW.htm](classfeatures/None-00-R41sy7weOd0JhOiW.htm)|Dragon Deviant Classification|Classification des déviances de dragon|libre|
|[None-00-rgLaxC3I2L7HSPNn.htm](classfeatures/None-00-rgLaxC3I2L7HSPNn.htm)|Catharsis Emotion (Awe)|Émotion cathartique (Émerveillement)|libre|
|[None-00-S4uY5ap2yhDRqhd0.htm](classfeatures/None-00-S4uY5ap2yhDRqhd0.htm)|Catharsis Emotion (Hatred)|Émotion cathartique (Haine)|libre|
|[None-00-t01K3DB2qHnbt1q3.htm](classfeatures/None-00-t01K3DB2qHnbt1q3.htm)|Order of the Scourge|Ordre du fléau|officielle|
|[None-00-T9gUmwghw16itUAO.htm](classfeatures/None-00-T9gUmwghw16itUAO.htm)|Catharsis Emotion (Remorse)|Émotion cathartique (Remords)|libre|
|[None-00-ub9gwFXnMuKvhnPL.htm](classfeatures/None-00-ub9gwFXnMuKvhnPL.htm)|Order of the Nail|Ordre de la pointe|officielle|
|[None-00-UTRDN1TAieBMjwP1.htm](classfeatures/None-00-UTRDN1TAieBMjwP1.htm)|Order of the Godclaw|Ordre du dieu griffu|libre|
|[None-00-YrJj8UI0XpkHv0Ho.htm](classfeatures/None-00-YrJj8UI0XpkHv0Ho.htm)|Order of the Chain|Ordre des chaînes|officielle|
|[None-00-zFY2PlJTObXpcQW4.htm](classfeatures/None-00-zFY2PlJTObXpcQW4.htm)|Catharsis Emotion (Joy)|Émotion cathartique (Joie)|libre|
|[None-00-zGxO2cETUsXuvqRu.htm](classfeatures/None-00-zGxO2cETUsXuvqRu.htm)|Order of the Pyre|Ordre du bûcher|officielle|
|[None-01-1FPVkksuE2ncw9rF.htm](classfeatures/None-01-1FPVkksuE2ncw9rF.htm)|Ki Spells|Sorts de ki|officielle|
|[None-01-1tyNn9sduyexXLfL.htm](classfeatures/None-01-1tyNn9sduyexXLfL.htm)|Psi Cantrips and Amps|Tours de magie psy et amplis|libre|
|[None-01-HwUps0waR29bwlTI.htm](classfeatures/None-01-HwUps0waR29bwlTI.htm)|Unleash Psyche|Déchaîner la psyché|libre|
|[None-01-HYTaibaCGE85rhbZ.htm](classfeatures/None-01-HYTaibaCGE85rhbZ.htm)|Runelord Specialization|Spécialisation de Seigneur des runes|libre|
|[None-01-mRvyq7G0rqRP1EAr.htm](classfeatures/None-01-mRvyq7G0rqRP1EAr.htm)|Wellspring Magic|Magie de la source|libre|
|[None-01-pUkUC8HHom2DmYzz.htm](classfeatures/None-01-pUkUC8HHom2DmYzz.htm)|Elemental Magic|Magie élémentaire|libre|
|[None-01-T25ZLQWn6O4KchLo.htm](classfeatures/None-01-T25ZLQWn6O4KchLo.htm)|Focus Spells|Sorts focalisés|libre|
|[None-01-Upf1LXtWNJ6eB5sm.htm](classfeatures/None-01-Upf1LXtWNJ6eB5sm.htm)|Flexible Spell Preparation|Préparation de sort flexible|libre|
|[None-03-D8CSi8c9XiRpVc5M.htm](classfeatures/None-03-D8CSi8c9XiRpVc5M.htm)|Alertness|Vigilance|officielle|
|[None-03-F57Na5VxfBp56kke.htm](classfeatures/None-03-F57Na5VxfBp56kke.htm)|Fortitude Expertise|Expertise en Vigueur|libre|
|[None-03-JCqACxgrm5ixX0Jy.htm](classfeatures/None-03-JCqACxgrm5ixX0Jy.htm)|Perception Expertise|Expertise en perception|libre|
|[None-03-TUOeATt52P43r5W0.htm](classfeatures/None-03-TUOeATt52P43r5W0.htm)|Reflex Expertise|Expertise en Réflexes|libre|
|[None-03-wMyDcVNmA7xGK83S.htm](classfeatures/None-03-wMyDcVNmA7xGK83S.htm)|Iron Will|Volonté de fer|officielle|
|[None-05-70jqXP2eS4tRZ0Ok.htm](classfeatures/None-05-70jqXP2eS4tRZ0Ok.htm)|Magical Fortitude|Vigueur magique|libre|
|[None-05-9XLUh9iMepZesdmc.htm](classfeatures/None-05-9XLUh9iMepZesdmc.htm)|Weapon Expertise|Expertise avec les armes|libre|
|[None-07-0npO4rPscGm0dX13.htm](classfeatures/None-07-0npO4rPscGm0dX13.htm)|Perception Mastery|Maîtrise en perception|libre|
|[None-07-9EqIasqfI8YIM3Pt.htm](classfeatures/None-07-9EqIasqfI8YIM3Pt.htm)|Weapon Specialization|Spécialisation martiale|libre|
|[None-07-cD3nSupdCvONuHiE.htm](classfeatures/None-07-cD3nSupdCvONuHiE.htm)|Expert Spellcaster|Incantateur expert|libre|
|[None-07-JQAujUXjczVnYDEI.htm](classfeatures/None-07-JQAujUXjczVnYDEI.htm)|Resolve|Résolution|libre|
|[None-07-MV6XIuAgN9uSA0Da.htm](classfeatures/None-07-MV6XIuAgN9uSA0Da.htm)|Evasion|Évasion|officielle|
|[None-07-OMZs5y16jZRW9KQK.htm](classfeatures/None-07-OMZs5y16jZRW9KQK.htm)|Juggernaut|Juggernaut|libre|
|[None-07-x5jaCJxsmD5sx3KB.htm](classfeatures/None-07-x5jaCJxsmD5sx3KB.htm)|Armor Expertise|Expertise avec les armures|officielle|
|[None-09-nPwYSuMLkJWMB4CH.htm](classfeatures/None-09-nPwYSuMLkJWMB4CH.htm)|Performer's Heart|Coeur de performeur|libre|
|[None-11-esyKPSDbFQPB4lhq.htm](classfeatures/None-11-esyKPSDbFQPB4lhq.htm)|Medium Armor Expertise (Inventor)|Expertise avec les armures intermédiaires (Inventeur)|libre|
|[None-11-FCEp9jjxxgRJDJV3.htm](classfeatures/None-11-FCEp9jjxxgRJDJV3.htm)|Medium Armor Expertise|Expertise avec les armures intermédiaires|libre|
|[None-13-a58MGVX2L589sC9g.htm](classfeatures/None-13-a58MGVX2L589sC9g.htm)|Psychic Weapon Specialization|Spécialisation martiale (Psychiste)|libre|
|[None-13-CGB1TczFhQhdQxml.htm](classfeatures/None-13-CGB1TczFhQhdQxml.htm)|Armor Mastery|Maîtrise des armures|libre|
|[None-13-L5D0NwFXdLiVSnk5.htm](classfeatures/None-13-L5D0NwFXdLiVSnk5.htm)|Improved Evasion|Évasion améliorée|officielle|
|[None-13-nLwPMPLRne1HnL00.htm](classfeatures/None-13-nLwPMPLRne1HnL00.htm)|Perception Legend|Légendaire en Perception|libre|
|[None-15-l1InYvhnQSz6Ucxc.htm](classfeatures/None-15-l1InYvhnQSz6Ucxc.htm)|Master Spellcaster|Incantateur maître|libre|
|[None-17-cGMSYAErbUG5E8X2.htm](classfeatures/None-17-cGMSYAErbUG5E8X2.htm)|Medium Armor Mastery|Maîtrise des armures intermédiaires|libre|
|[None-17-mRobjNNsABQdUUZq.htm](classfeatures/None-17-mRobjNNsABQdUUZq.htm)|Greater Performer's Heart|Coeur de performeur supérieur|libre|
|[oracle-01-cFe6vFb3gSDyNeS9.htm](classfeatures/oracle-01-cFe6vFb3gSDyNeS9.htm)|Spell Repertoire (Oracle)|Répertoire de sorts (Oracle)|libre|
|[oracle-01-EslxR2sbDK9XJaAl.htm](classfeatures/oracle-01-EslxR2sbDK9XJaAl.htm)|Time|Temps|libre|
|[oracle-01-gjOGOR30Czpnx3tM.htm](classfeatures/oracle-01-gjOGOR30Czpnx3tM.htm)|Battle|Combat|libre|
|[oracle-01-GTSvbFb36InvuH0w.htm](classfeatures/oracle-01-GTSvbFb36InvuH0w.htm)|Flames|Flammes|libre|
|[oracle-01-IaxmCkdsPlA52spu.htm](classfeatures/oracle-01-IaxmCkdsPlA52spu.htm)|Bones|Ossements|libre|
|[oracle-01-ibX2EhKkyUtbOHLj.htm](classfeatures/oracle-01-ibX2EhKkyUtbOHLj.htm)|Oracular Curse|Malédiction oraculaire|libre|
|[oracle-01-NXUOtO9NytHQurlg.htm](classfeatures/oracle-01-NXUOtO9NytHQurlg.htm)|Revelation Spells|Sorts de révélation|libre|
|[oracle-01-o1gGG36wpn9mxeop.htm](classfeatures/oracle-01-o1gGG36wpn9mxeop.htm)|Life|Vie|libre|
|[oracle-01-PRJYLksQEwT39bTl.htm](classfeatures/oracle-01-PRJYLksQEwT39bTl.htm)|Mystery|Mystère|libre|
|[oracle-01-qvRlih3u7vK3FYUR.htm](classfeatures/oracle-01-qvRlih3u7vK3FYUR.htm)|Ancestors|Ancêtres|libre|
|[oracle-01-RI2EMRBBPNSoTJXu.htm](classfeatures/oracle-01-RI2EMRBBPNSoTJXu.htm)|Cosmos|Cosmos|libre|
|[oracle-01-tZBb3Kh4nJcNoUFI.htm](classfeatures/oracle-01-tZBb3Kh4nJcNoUFI.htm)|Lore|Savoir|libre|
|[oracle-01-tzDW9l4lBwXCVYtz.htm](classfeatures/oracle-01-tzDW9l4lBwXCVYtz.htm)|Ash|Cendre|libre|
|[oracle-01-W9cF7wZztLDb1WGY.htm](classfeatures/oracle-01-W9cF7wZztLDb1WGY.htm)|Tempest|Tempête|libre|
|[oracle-11-rrzItB68Er0DzKx7.htm](classfeatures/oracle-11-rrzItB68Er0DzKx7.htm)|Major Curse|Malédiction majeure|libre|
|[oracle-17-5LOARurr4qWkfS9K.htm](classfeatures/oracle-17-5LOARurr4qWkfS9K.htm)|Greater Resolve|Résolution supérieure|libre|
|[oracle-17-F4brPlp1tHGUqyuI.htm](classfeatures/oracle-17-F4brPlp1tHGUqyuI.htm)|Extreme Curse|Malédiction extrême|libre|
|[oracle-19-571c1aGnvNVwfF6b.htm](classfeatures/oracle-19-571c1aGnvNVwfF6b.htm)|Oracular Clarity|Clarté oraculaire|libre|
|[psychic-01-0fv6NVMZZ0peGL9e.htm](classfeatures/psychic-01-0fv6NVMZZ0peGL9e.htm)|Conscious Mind|Esprit conscient|libre|
|[psychic-01-0UKDEtZ7ffTEsqCK.htm](classfeatures/psychic-01-0UKDEtZ7ffTEsqCK.htm)|Gathered Lore|Connaissances rassemblées|libre|
|[psychic-01-1mMdsSIVsyyqNr2t.htm](classfeatures/psychic-01-1mMdsSIVsyyqNr2t.htm)|Spell Repertoire (Psychic)|Répertoire de sorts (Psychiste)|libre|
|[psychic-01-1rlBo3LcwIuPDCvz.htm](classfeatures/psychic-01-1rlBo3LcwIuPDCvz.htm)|The Distant Grasp|L'étreinte distante|libre|
|[psychic-01-5tSR0WzMPFn5s3Xs.htm](classfeatures/psychic-01-5tSR0WzMPFn5s3Xs.htm)|The Oscillating Wave|La chaleur oscillante|libre|
|[psychic-01-79ZetrRF6S01P4Vf.htm](classfeatures/psychic-01-79ZetrRF6S01P4Vf.htm)|Subconscious Mind|Esprit subconscient|libre|
|[psychic-01-8dAm3ULUqaK4N5a7.htm](classfeatures/psychic-01-8dAm3ULUqaK4N5a7.htm)|The Tangible Dream|Le rêve matérialisé|libre|
|[psychic-01-EMErBnhrgUHEKAsZ.htm](classfeatures/psychic-01-EMErBnhrgUHEKAsZ.htm)|Wandering Reverie|Rêverie erratique|libre|
|[psychic-01-FaSY47siV1x6CAQp.htm](classfeatures/psychic-01-FaSY47siV1x6CAQp.htm)|The Unbound Step|Le déplacement instantané|libre|
|[psychic-01-iXwqJyBsjJNrKJae.htm](classfeatures/psychic-01-iXwqJyBsjJNrKJae.htm)|Psychic Spellcasting|Incantation psychique|libre|
|[psychic-01-PNihL10QAB1sYSRn.htm](classfeatures/psychic-01-PNihL10QAB1sYSRn.htm)|Emotional Acceptance|Acceptation émotionnelle|libre|
|[psychic-01-Qrhw4SILfT8YNQgB.htm](classfeatures/psychic-01-Qrhw4SILfT8YNQgB.htm)|Precise Discipline|Discipline précise|libre|
|[psychic-01-rdWWlqvxXgfWDaSO.htm](classfeatures/psychic-01-rdWWlqvxXgfWDaSO.htm)|The Silent Whisper|Le murmure silencieux|libre|
|[psychic-01-rvpTsj9epRuNH3uB.htm](classfeatures/psychic-01-rvpTsj9epRuNH3uB.htm)|The Infinite Eye|L'oeil absolu|libre|
|[psychic-05-Ftz5jVa9X6aXybkC.htm](classfeatures/psychic-05-Ftz5jVa9X6aXybkC.htm)|Precognitive Reflexes|Réflexes précognitifs|libre|
|[psychic-05-k9MeSdp2DbGd1hFz.htm](classfeatures/psychic-05-k9MeSdp2DbGd1hFz.htm)|Clarity of Focus|Clarté de focalisation|libre|
|[psychic-09-zAe95Uk5IPIT23K1.htm](classfeatures/psychic-09-zAe95Uk5IPIT23K1.htm)|Great Fortitude (Psychic)|Vigueur supérieure (Psychiste)|libre|
|[psychic-11-Kf9lSN6pVS2Hy4KI.htm](classfeatures/psychic-11-Kf9lSN6pVS2Hy4KI.htm)|Walls of Will|Murs de volonté|libre|
|[psychic-11-kLschzVZFoe3U63C.htm](classfeatures/psychic-11-kLschzVZFoe3U63C.htm)|Psychic Weapon Expertise|Expertise martiale (Psychiste)|libre|
|[psychic-11-wOl7EeF7S6i753Ef.htm](classfeatures/psychic-11-wOl7EeF7S6i753Ef.htm)|Extrasensory Perception|Perception extra-sensorielle|libre|
|[psychic-13-MtHLCQGD6OW98WC2.htm](classfeatures/psychic-13-MtHLCQGD6OW98WC2.htm)|Personal Barrier|Barrière personnelle|libre|
|[psychic-17-Hw6Ji7Fgx0XkVkac.htm](classfeatures/psychic-17-Hw6Ji7Fgx0XkVkac.htm)|Fortress of Will|Forteresse de volonté|libre|
|[psychic-19-mZwD2brwXlyR9RAR.htm](classfeatures/psychic-19-mZwD2brwXlyR9RAR.htm)|Infinite Mind|Esprit infini|libre|
|[ranger-01-0nIOGpHQNHsKSFKT.htm](classfeatures/ranger-01-0nIOGpHQNHsKSFKT.htm)|Hunt Prey|Chasser une proie|officielle|
|[ranger-01-6v4Rj7wWfOH1882r.htm](classfeatures/ranger-01-6v4Rj7wWfOH1882r.htm)|Flurry|Déluge|libre|
|[ranger-01-mzkkj9LEWjJPBhaq.htm](classfeatures/ranger-01-mzkkj9LEWjJPBhaq.htm)|Hunter's Edge|Spécialité du chasseur|libre|
|[ranger-01-NBHyoTrI8q62uDsU.htm](classfeatures/ranger-01-NBHyoTrI8q62uDsU.htm)|Outwit|Ruse|libre|
|[ranger-01-u6cBjqz2fiRBadBt.htm](classfeatures/ranger-01-u6cBjqz2fiRBadBt.htm)|Precision|Précision|libre|
|[ranger-01-w3HysrCgDs5uFXKX.htm](classfeatures/ranger-01-w3HysrCgDs5uFXKX.htm)|Warden Spells|Sorts de gardien|libre|
|[ranger-03-NhcF2CbXA8R1UCg4.htm](classfeatures/ranger-03-NhcF2CbXA8R1UCg4.htm)|Will Expertise|Expertise en volonté|libre|
|[ranger-05-PeZi7E9lI4vz8EGY.htm](classfeatures/ranger-05-PeZi7E9lI4vz8EGY.htm)|Trackless Journey|Parcours sans traces|libre|
|[ranger-05-QhoW8ivPvYmWzyEZ.htm](classfeatures/ranger-05-QhoW8ivPvYmWzyEZ.htm)|Ranger Weapon Expertise|Expertise avec les armes du rôdeur|libre|
|[ranger-07-7dMDxvzGKbqoEAdX.htm](classfeatures/ranger-07-7dMDxvzGKbqoEAdX.htm)|Natural Reflexes|Réflexes naturels|libre|
|[ranger-09-5likl5SAxQPrQ3KF.htm](classfeatures/ranger-09-5likl5SAxQPrQ3KF.htm)|Ranger Expertise|Expertise du rôdeur|libre|
|[ranger-09-j2R64kwUgEJ1TudD.htm](classfeatures/ranger-09-j2R64kwUgEJ1TudD.htm)|Nature's Edge|Avantage naturel|libre|
|[ranger-11-gc6OMDRxTpSfsVtk.htm](classfeatures/ranger-11-gc6OMDRxTpSfsVtk.htm)|Warden's Endurance|Endurance du gardien|libre|
|[ranger-11-RlwE99yKnhq8FUuy.htm](classfeatures/ranger-11-RlwE99yKnhq8FUuy.htm)|Unimpeded Journey|Parcours sans obstacle|libre|
|[ranger-15-rpLPCkTXCZlQ51SR.htm](classfeatures/ranger-15-rpLPCkTXCZlQ51SR.htm)|Greater Natural Reflexes|Réflexes naturels supérieurs|libre|
|[ranger-17-BJYSUbFUGcTLaPDn.htm](classfeatures/ranger-17-BJYSUbFUGcTLaPDn.htm)|Masterful Hunter (Precision)|Maître chasseur (Précision)|officielle|
|[ranger-17-JhLncIB10GSQowWL.htm](classfeatures/ranger-17-JhLncIB10GSQowWL.htm)|Masterful Hunter (Flurry)|Maître chasseur (Déluge)|officielle|
|[ranger-17-RVZC4wVy5B5W2OeS.htm](classfeatures/ranger-17-RVZC4wVy5B5W2OeS.htm)|Masterful Hunter|Maître chasseur|libre|
|[ranger-17-vWZaLE2fEKMBw3D5.htm](classfeatures/ranger-17-vWZaLE2fEKMBw3D5.htm)|Masterful Hunter (Outwit)|Maître chasseur (Ruse)|libre|
|[ranger-19-bBGb1LcffXEqar0p.htm](classfeatures/ranger-19-bBGb1LcffXEqar0p.htm)|Swift Prey|Proie rapide|libre|
|[ranger-19-phwQ2MrDZ13D2HxC.htm](classfeatures/ranger-19-phwQ2MrDZ13D2HxC.htm)|Second Skin|Seconde peau|officielle|
|[rogue-01-3KPZ7svIO6kmmEKH.htm](classfeatures/rogue-01-3KPZ7svIO6kmmEKH.htm)|Ruffian|Voyou|libre|
|[rogue-01-D8qtAo2w4jsqjBrM.htm](classfeatures/rogue-01-D8qtAo2w4jsqjBrM.htm)|Eldritch Trickster|Mystificateur|libre|
|[rogue-01-j1JE61quDxdge4mg.htm](classfeatures/rogue-01-j1JE61quDxdge4mg.htm)|Sneak Attack|Attaque sournoise|officielle|
|[rogue-01-RyOkmu0W9svavuAB.htm](classfeatures/rogue-01-RyOkmu0W9svavuAB.htm)|Mastermind|Cerveau|libre|
|[rogue-01-uGuCGQvUmioFV2Bd.htm](classfeatures/rogue-01-uGuCGQvUmioFV2Bd.htm)|Rogue's Racket|Trafics de roublard|libre|
|[rogue-01-w6rMqmGzhUahdnA7.htm](classfeatures/rogue-01-w6rMqmGzhUahdnA7.htm)|Surprise Attack|Attaque surprise|libre|
|[rogue-01-wAh2riuFRzz0edPl.htm](classfeatures/rogue-01-wAh2riuFRzz0edPl.htm)|Thief|Voleur|libre|
|[rogue-01-ZvfxtUMtfIOLYHyg.htm](classfeatures/rogue-01-ZvfxtUMtfIOLYHyg.htm)|Scoundrel|Scélérat|libre|
|[rogue-03-PNpmVmD21zViDtGC.htm](classfeatures/rogue-03-PNpmVmD21zViDtGC.htm)|Deny Advantage|Refus d'avantage|libre|
|[rogue-05-v8UNEJR5IDKi8yqa.htm](classfeatures/rogue-05-v8UNEJR5IDKi8yqa.htm)|Weapon Tricks|Astuces martiales|libre|
|[rogue-07-oa3TJpPnvL98EUHh.htm](classfeatures/rogue-07-oa3TJpPnvL98EUHh.htm)|Evasive Reflexes|Réflexes d'évitement|libre|
|[rogue-09-9SruVg2lZpNaYLOB.htm](classfeatures/rogue-09-9SruVg2lZpNaYLOB.htm)|Debilitating Strike|Frappe incapacitante|libre|
|[rogue-09-fMHYzXEUMoWskKMF.htm](classfeatures/rogue-09-fMHYzXEUMoWskKMF.htm)|Rogue Resilience|Résilience du roublard|libre|
|[rogue-11-f3Dh32EU4VsHu01b.htm](classfeatures/rogue-11-f3Dh32EU4VsHu01b.htm)|Rogue Expertise|Expertise du roublard|officielle|
|[rogue-13-BTpL6XvMk4jvVYYJ.htm](classfeatures/rogue-13-BTpL6XvMk4jvVYYJ.htm)|Greater Rogue Reflexes|Réflexes de roublard supérieurs|libre|
|[rogue-13-myvcir1LEkaVxOlE.htm](classfeatures/rogue-13-myvcir1LEkaVxOlE.htm)|Master Tricks|Astuces de maître|libre|
|[rogue-15-W1FkMHYVDg3yTU5r.htm](classfeatures/rogue-15-W1FkMHYVDg3yTU5r.htm)|Double Debilitation|Double handicap|libre|
|[rogue-17-xmZ7oeTDcQVXegUP.htm](classfeatures/rogue-17-xmZ7oeTDcQVXegUP.htm)|Agile Mind|Esprit fuyant|libre|
|[rogue-19-SUUdWG0t33VKa5q4.htm](classfeatures/rogue-19-SUUdWG0t33VKa5q4.htm)|Master Strike|Frappe de maître|libre|
|[sorcerer-01-2goYo6VNbwC6aKF1.htm](classfeatures/sorcerer-01-2goYo6VNbwC6aKF1.htm)|Bloodline|Lignages|libre|
|[sorcerer-01-3qqvnC2U8W26yae7.htm](classfeatures/sorcerer-01-3qqvnC2U8W26yae7.htm)|Bloodline: Aberrant|Lignage : Aberrant|libre|
|[sorcerer-01-5Wxjghw7lHuCxjZz.htm](classfeatures/sorcerer-01-5Wxjghw7lHuCxjZz.htm)|Bloodline: Nymph|Lignage : Nymphe|libre|
|[sorcerer-01-7WBZ2kkhZ7JorWu2.htm](classfeatures/sorcerer-01-7WBZ2kkhZ7JorWu2.htm)|Bloodline: Undead|Lignage : Mort-vivant|officielle|
|[sorcerer-01-b6hyZTs1rVGHDexz.htm](classfeatures/sorcerer-01-b6hyZTs1rVGHDexz.htm)|Bloodline: Harrow|Lignage : Tourment|libre|
|[sorcerer-01-dKTb959aCQIzSIXj.htm](classfeatures/sorcerer-01-dKTb959aCQIzSIXj.htm)|Bloodline: Wyrmblessed|Lignage : Béni du Ver|libre|
|[sorcerer-01-eW3cfCH7Wpx2vik2.htm](classfeatures/sorcerer-01-eW3cfCH7Wpx2vik2.htm)|Bloodline: Fey|Lignage : Féerique|officielle|
|[sorcerer-01-gmnx7e1g08bppbqt.htm](classfeatures/sorcerer-01-gmnx7e1g08bppbqt.htm)|Sorcerer Spellcasting|Incantation de l'ensorceleur|officielle|
|[sorcerer-01-H6ziAPvCipTPG8SH.htm](classfeatures/sorcerer-01-H6ziAPvCipTPG8SH.htm)|Bloodline Spells|Sorts de lignage|officielle|
|[sorcerer-01-lURKSJZAGKVD6cH9.htm](classfeatures/sorcerer-01-lURKSJZAGKVD6cH9.htm)|Spell Repertoire (Sorcerer)|Répertoire de sorts (Ensorceleur)|officielle|
|[sorcerer-01-O0uXZRWMNliDbkxU.htm](classfeatures/sorcerer-01-O0uXZRWMNliDbkxU.htm)|Bloodline: Hag|Lignage : Guenaude|libre|
|[sorcerer-01-o39zQMIdERWtmBSB.htm](classfeatures/sorcerer-01-o39zQMIdERWtmBSB.htm)|Bloodline: Diabolic|Lignage : Diabolique|officielle|
|[sorcerer-01-PpzH9tJULk5ksX9w.htm](classfeatures/sorcerer-01-PpzH9tJULk5ksX9w.htm)|Bloodline: Psychopomp|Lignage : Psychopompe|libre|
|[sorcerer-01-RXRnJcG4XSabZ35a.htm](classfeatures/sorcerer-01-RXRnJcG4XSabZ35a.htm)|Bloodline: Elemental|Lignage : Élémentaire|officielle|
|[sorcerer-01-TWR1wbPJuCLnGdFZ.htm](classfeatures/sorcerer-01-TWR1wbPJuCLnGdFZ.htm)|Bloodline: Phoenix|Lignage : Phénix|libre|
|[sorcerer-01-tYOMBiH3HbViNWwn.htm](classfeatures/sorcerer-01-tYOMBiH3HbViNWwn.htm)|Bloodline: Genie|Lignage : Génie|libre|
|[sorcerer-01-uoQOm41BVdSo6pAS.htm](classfeatures/sorcerer-01-uoQOm41BVdSo6pAS.htm)|Bloodline: Shadow|Lignage : Ombre|libre|
|[sorcerer-01-vhW3glAaEfq2DKrw.htm](classfeatures/sorcerer-01-vhW3glAaEfq2DKrw.htm)|Bloodline: Angelic|Lignage : Angélique|officielle|
|[sorcerer-01-w5koctOVrEcpxTIq.htm](classfeatures/sorcerer-01-w5koctOVrEcpxTIq.htm)|Bloodline: Demonic|Lignage : Démoniaque|officielle|
|[sorcerer-01-ZEtJJ5UOlV5oTWWp.htm](classfeatures/sorcerer-01-ZEtJJ5UOlV5oTWWp.htm)|Bloodline: Imperial|Lignage : Impérial|officielle|
|[sorcerer-01-ZHabYxSgYK0XbjhM.htm](classfeatures/sorcerer-01-ZHabYxSgYK0XbjhM.htm)|Bloodline: Draconic|Lignage : Draconique|officielle|
|[sorcerer-03-VKRjmXxBFLrJK01c.htm](classfeatures/sorcerer-03-VKRjmXxBFLrJK01c.htm)|Signature Spells|Sorts emblématiques|libre|
|[sorcerer-19-feCnVrPPlKhl701x.htm](classfeatures/sorcerer-19-feCnVrPPlKhl701x.htm)|Bloodline Paragon|Parangon du lignage|officielle|
|[summoner-01-gWcN75VNpSZ4FqNb.htm](classfeatures/summoner-01-gWcN75VNpSZ4FqNb.htm)|Summoner Spellcasting|Incantation du conjurateur|libre|
|[summoner-01-IPcdQAwJk0aZe5mg.htm](classfeatures/summoner-01-IPcdQAwJk0aZe5mg.htm)|Evolution Feat|Don d'évolution|libre|
|[summoner-01-Ju2Tp5s5iBB76tQO.htm](classfeatures/summoner-01-Ju2Tp5s5iBB76tQO.htm)|Spell Repertoire (Summoner)|Répertoire de sorts (Conjurateur)|libre|
|[summoner-01-qOEpe596B0UjhcG0.htm](classfeatures/summoner-01-qOEpe596B0UjhcG0.htm)|Eidolon|Eidolon|libre|
|[summoner-01-wguqw300DB5XdD8W.htm](classfeatures/summoner-01-wguqw300DB5XdD8W.htm)|Link Spells|Sorts liés|libre|
|[summoner-03-P34Jx6i4GJGoqTtG.htm](classfeatures/summoner-03-P34Jx6i4GJGoqTtG.htm)|Unlimited Signature Spells|Sorts emblématiques illimités|libre|
|[summoner-03-QiMlJ33kNEoyh1M0.htm](classfeatures/summoner-03-QiMlJ33kNEoyh1M0.htm)|Shared Vigilance|Vigilance partagée|libre|
|[summoner-05-GI5IAl4dkly4At8e.htm](classfeatures/summoner-05-GI5IAl4dkly4At8e.htm)|Ability Boosts|Primes d'attributs (conjurateur)|libre|
|[summoner-05-pda6iUaU9waXId5Q.htm](classfeatures/summoner-05-pda6iUaU9waXId5Q.htm)|Eidolon Unarmed Expertise|Expertise à mains nues de l'eidolon|libre|
|[summoner-07-oCnyGRvkfjTsZXcX.htm](classfeatures/summoner-07-oCnyGRvkfjTsZXcX.htm)|Eidolon Weapon Specialization|Spécialisation des armes de l'eidolon|libre|
|[summoner-07-skQBrwRwJW2K6ACj.htm](classfeatures/summoner-07-skQBrwRwJW2K6ACj.htm)|Eidolon Symbiosis|Symbiose de l'eidolon|libre|
|[summoner-09-dZNAXTQovlWVvAyX.htm](classfeatures/summoner-09-dZNAXTQovlWVvAyX.htm)|Shared Reflexes|réflexes partagés|libre|
|[summoner-11-2CZPYoyWih6zYTcb.htm](classfeatures/summoner-11-2CZPYoyWih6zYTcb.htm)|Eidolon Defensive Expertise|Expertise défensive de l'eidolon|libre|
|[summoner-11-q1Y12Pg2gQg2FJPR.htm](classfeatures/summoner-11-q1Y12Pg2gQg2FJPR.htm)|Twin Juggernauts|Juggernauts jumelés|libre|
|[summoner-13-NIzHfVcVMhDmvA49.htm](classfeatures/summoner-13-NIzHfVcVMhDmvA49.htm)|Eidolon Unarmed Mastery|Maîtrise à mains nues de l'eidolon|libre|
|[summoner-15-B5SyM7qHrU0gTGR0.htm](classfeatures/summoner-15-B5SyM7qHrU0gTGR0.htm)|Greater Eidolon Specialization|Spécialisation de l'eidolon supérieure|libre|
|[summoner-15-eZPfHVz14j42jCnS.htm](classfeatures/summoner-15-eZPfHVz14j42jCnS.htm)|Shared Resolve|Résolution partagée|libre|
|[summoner-17-nCE9DzkugRefREqT.htm](classfeatures/summoner-17-nCE9DzkugRefREqT.htm)|Eidolon Transcendence|Transcendance de l'eidolon|libre|
|[summoner-19-0WvI8KM5m0SaZ3MH.htm](classfeatures/summoner-19-0WvI8KM5m0SaZ3MH.htm)|Eidolon Defensive Mastery|Maîtrise défensive de l'eidolon|libre|
|[summoner-19-H0iWhiyP0QqgmAKs.htm](classfeatures/summoner-19-H0iWhiyP0QqgmAKs.htm)|Instant Manifestation|Manifestation instantanée|libre|
|[swashbuckler-01-4lGhbEjlEoGP4scl.htm](classfeatures/swashbuckler-01-4lGhbEjlEoGP4scl.htm)|Wit|Esprit|libre|
|[swashbuckler-01-5HoEwzLDJGTCZtFa.htm](classfeatures/swashbuckler-01-5HoEwzLDJGTCZtFa.htm)|Battledancer|Danseur de combat|libre|
|[swashbuckler-01-B7RMnrHwQHlezlJT.htm](classfeatures/swashbuckler-01-B7RMnrHwQHlezlJT.htm)|Gymnast|Gymnaste|libre|
|[swashbuckler-01-beW1OqibVQ3fBvRw.htm](classfeatures/swashbuckler-01-beW1OqibVQ3fBvRw.htm)|Swashbuckler's Style|Style du bretteur|libre|
|[swashbuckler-01-Jgid6Ja6Y879COlN.htm](classfeatures/swashbuckler-01-Jgid6Ja6Y879COlN.htm)|Fencer|Escrimeur|libre|
|[swashbuckler-01-KBhwFjdptrKyN5EM.htm](classfeatures/swashbuckler-01-KBhwFjdptrKyN5EM.htm)|Braggart|Fanfaron|libre|
|[swashbuckler-01-LzYi0OuOoypNb6jd.htm](classfeatures/swashbuckler-01-LzYi0OuOoypNb6jd.htm)|Panache|Panache|libre|
|[swashbuckler-01-pyo0vmxUFIFX2GNl.htm](classfeatures/swashbuckler-01-pyo0vmxUFIFX2GNl.htm)|Confident Finisher|Aboutissement assuré|libre|
|[swashbuckler-01-RQH6vigvhmiYKKjg.htm](classfeatures/swashbuckler-01-RQH6vigvhmiYKKjg.htm)|Precise Strike|Frappe précise|libre|
|[swashbuckler-03-8BOFeRE7ZfJ02N0O.htm](classfeatures/swashbuckler-03-8BOFeRE7ZfJ02N0O.htm)|Vivacious Speed|Vitesse exubérante|libre|
|[swashbuckler-03-Jtn7IugykXDlIoZq.htm](classfeatures/swashbuckler-03-Jtn7IugykXDlIoZq.htm)|Opportune Riposte|Riposte opportune|libre|
|[swashbuckler-03-pthjQIK9pDxnbER6.htm](classfeatures/swashbuckler-03-pthjQIK9pDxnbER6.htm)|Stylish Tricks|Astuces de style|libre|
|[swashbuckler-05-F5BHEav90oOJ2LwN.htm](classfeatures/swashbuckler-05-F5BHEav90oOJ2LwN.htm)|Weapon Expertise (Swashbuckler)|Expertise martiale (Bretteur)|libre|
|[swashbuckler-09-KxpaxUSuBC7hr4F7.htm](classfeatures/swashbuckler-09-KxpaxUSuBC7hr4F7.htm)|Exemplary Finisher|Aboutissement exemplaire|libre|
|[swashbuckler-09-U74JoAcLHTOsZG6q.htm](classfeatures/swashbuckler-09-U74JoAcLHTOsZG6q.htm)|Swashbuckler Expertise|Expertise du bretteur|libre|
|[swashbuckler-11-13QpCrR8a8XULbJa.htm](classfeatures/swashbuckler-11-13QpCrR8a8XULbJa.htm)|Continuous Flair|Élégance continuelle|libre|
|[swashbuckler-13-i6563IU7x4L9oRgC.htm](classfeatures/swashbuckler-13-i6563IU7x4L9oRgC.htm)|Martial Weapon Mastery|Maîtrise des armes de guerre|libre|
|[swashbuckler-13-pZYkb12t5DSwtts7.htm](classfeatures/swashbuckler-13-pZYkb12t5DSwtts7.htm)|Light Armor Expertise|Expertise avec les armures légères|libre|
|[swashbuckler-15-Pk3Ht0KZyFxSeL07.htm](classfeatures/swashbuckler-15-Pk3Ht0KZyFxSeL07.htm)|Keen Flair|Élégance aigüe|libre|
|[swashbuckler-15-Z7HX6TeFsaup7Dx9.htm](classfeatures/swashbuckler-15-Z7HX6TeFsaup7Dx9.htm)|Greater Weapon Specialization|Spécialisation martiale supérieure|libre|
|[swashbuckler-19-SHpjmM4A3Sw4GgDz.htm](classfeatures/swashbuckler-19-SHpjmM4A3Sw4GgDz.htm)|Light Armor Mastery|Maîtrise des armures légères|libre|
|[swashbuckler-19-ypfT3iybew6ZSIUl.htm](classfeatures/swashbuckler-19-ypfT3iybew6ZSIUl.htm)|Eternal Confidence|Confiance éternelle|libre|
|[thaumaturge-01-cvQmPkJtybMcHinK.htm](classfeatures/thaumaturge-01-cvQmPkJtybMcHinK.htm)|Esoteric Lore|Connaissances ésotériques|libre|
|[thaumaturge-01-DhdLzrcMvB93Rjmt.htm](classfeatures/thaumaturge-01-DhdLzrcMvB93Rjmt.htm)|Regalia|Regalia|libre|
|[thaumaturge-01-DK1LCE5pd0YCY11c.htm](classfeatures/thaumaturge-01-DK1LCE5pd0YCY11c.htm)|Bell|Cloche|libre|
|[thaumaturge-01-MyN1cQgE0HsLF20e.htm](classfeatures/thaumaturge-01-MyN1cQgE0HsLF20e.htm)|Tome|Tome|libre|
|[thaumaturge-01-N6KvTbaRsphc0Ymb.htm](classfeatures/thaumaturge-01-N6KvTbaRsphc0Ymb.htm)|Mirror|Miroir|libre|
|[thaumaturge-01-PbNS8d3w3pYQYcVN.htm](classfeatures/thaumaturge-01-PbNS8d3w3pYQYcVN.htm)|Implement's Empowerment|Renforcement de l'implément|libre|
|[thaumaturge-01-pDxdE8S8QJV2PGiB.htm](classfeatures/thaumaturge-01-pDxdE8S8QJV2PGiB.htm)|Wand|Baguette|libre|
|[thaumaturge-01-PoclGJ7BCEyIuqJe.htm](classfeatures/thaumaturge-01-PoclGJ7BCEyIuqJe.htm)|Amulet|Amulette|libre|
|[thaumaturge-01-uwLNfBprqZw2osTb.htm](classfeatures/thaumaturge-01-uwLNfBprqZw2osTb.htm)|Exploit Vulnerability|Exploiter la vulnérabilité|libre|
|[thaumaturge-01-VSQJtzQE6ikKdsnP.htm](classfeatures/thaumaturge-01-VSQJtzQE6ikKdsnP.htm)|First Implement and Esoterica|Premier implément et ésotéricas|libre|
|[thaumaturge-01-YiDkrwaxiF7Gao7y.htm](classfeatures/thaumaturge-01-YiDkrwaxiF7Gao7y.htm)|Weapon|Arme|libre|
|[thaumaturge-05-ABYmUcLdxDFXEtzu.htm](classfeatures/thaumaturge-05-ABYmUcLdxDFXEtzu.htm)|Thaumaturge Weapon Expertise|Expertise avec les armes du thaumaturge|libre|
|[thaumaturge-05-Z8WpDAdAXyefLB7Q.htm](classfeatures/thaumaturge-05-Z8WpDAdAXyefLB7Q.htm)|Second Implement|Deuxième implément|libre|
|[thaumaturge-07-Obm4ItMIIr0whYeO.htm](classfeatures/thaumaturge-07-Obm4ItMIIr0whYeO.htm)|Implement Adept|Adepte de l'implément|libre|
|[thaumaturge-09-VdwNvQwq9sHflEwe.htm](classfeatures/thaumaturge-09-VdwNvQwq9sHflEwe.htm)|Intensify Vulnerability|Vulnérabilité intensifiée|libre|
|[thaumaturge-09-yvdSUIRU5uLr5eF2.htm](classfeatures/thaumaturge-09-yvdSUIRU5uLr5eF2.htm)|Thaumaturgic Expertise|Expertise thaumaturgique|libre|
|[thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm](classfeatures/thaumaturge-11-ZEUxZ4Ta1kDPHiq5.htm)|Second Adept|Second adepte|libre|
|[thaumaturge-15-zxZzjN2T53wnH4vU.htm](classfeatures/thaumaturge-15-zxZzjN2T53wnH4vU.htm)|Third Implement|Troisième implément|libre|
|[thaumaturge-17-QEtgbY8N2V4wTbsI.htm](classfeatures/thaumaturge-17-QEtgbY8N2V4wTbsI.htm)|Implement Paragon|Parangon de l'implément|libre|
|[thaumaturge-17-VywXtJCa0Y9fdGVH.htm](classfeatures/thaumaturge-17-VywXtJCa0Y9fdGVH.htm)|Thaumaturgic Mastery|Maîtrise thaumaturgique|libre|
|[thaumaturge-19-9ItMYxEkvxqBHrV1.htm](classfeatures/thaumaturge-19-9ItMYxEkvxqBHrV1.htm)|Unlimited Esoterica|ésotéricas illimitées|libre|
|[witch-01-9c57R18pfgfqlBCD.htm](classfeatures/witch-01-9c57R18pfgfqlBCD.htm)|Silence in Snow|Silence dans la neige|libre|
|[witch-01-9OwWgOP8ZWxTAqbg.htm](classfeatures/witch-01-9OwWgOP8ZWxTAqbg.htm)|The Resentment|Le ressentiment|libre|
|[witch-01-9uLh5z2uPo6LDFRY.htm](classfeatures/witch-01-9uLh5z2uPo6LDFRY.htm)|Hex Spells|Sorts de maléfice|libre|
|[witch-01-e0VhUyjz1clW3sC4.htm](classfeatures/witch-01-e0VhUyjz1clW3sC4.htm)|Wilding Steward|Intendant de la nature|libre|
|[witch-01-FdLx4VODZEYLGOK9.htm](classfeatures/witch-01-FdLx4VODZEYLGOK9.htm)|The Inscribed One|L'inscrit|libre|
|[witch-01-ghIsqhEsJTvjJiNl.htm](classfeatures/witch-01-ghIsqhEsJTvjJiNl.htm)|Spinner of Threads|Tisseur de fils|libre|
|[witch-01-KPtF29AaeX2sJW0K.htm](classfeatures/witch-01-KPtF29AaeX2sJW0K.htm)|Patron|Patron|libre|
|[witch-01-mFqMSQoNl0NMDklv.htm](classfeatures/witch-01-mFqMSQoNl0NMDklv.htm)|Faith's Flamekeeper|Gardien de la flamme de la foi|libre|
|[witch-01-nocYmxbi4rqCC2qS.htm](classfeatures/witch-01-nocYmxbi4rqCC2qS.htm)|Patron Theme|Thème de patron|libre|
|[witch-01-r2ZPRAw9c3VGZi8A.htm](classfeatures/witch-01-r2ZPRAw9c3VGZi8A.htm)|Starless Shadow|Ombre sans étoile|libre|
|[witch-01-SOan0fqyFTrkqJLV.htm](classfeatures/witch-01-SOan0fqyFTrkqJLV.htm)|Witch Lessons|Leçons de sorcier|libre|
|[witch-01-VVMMJdIWL7fAsQf3.htm](classfeatures/witch-01-VVMMJdIWL7fAsQf3.htm)|Baba Yaga Patron|Baba Yaga|libre|
|[witch-01-yksPhweBZYVCsE1A.htm](classfeatures/witch-01-yksPhweBZYVCsE1A.htm)|Familiar (Witch)|Familier (Sorcier)|libre|
|[witch-01-zT6QiTMxxj8JYoN9.htm](classfeatures/witch-01-zT6QiTMxxj8JYoN9.htm)|Witch Spellcasting|Incantation de sorcier|libre|
|[witch-01-zy0toWeGIeQstbT4.htm](classfeatures/witch-01-zy0toWeGIeQstbT4.htm)|Mosquito Witch Patron|Sorcière moustique|libre|
|[witch-02-DtmKrCvsmutVLAhH.htm](classfeatures/witch-02-DtmKrCvsmutVLAhH.htm)|Lesson of Vengeance|Leçon de vengeance|libre|
|[witch-02-evKUM58ymuuypRn9.htm](classfeatures/witch-02-evKUM58ymuuypRn9.htm)|Lesson of Dreams|Leçon des rêves|libre|
|[witch-02-FuOHRoEU8nHOXZnk.htm](classfeatures/witch-02-FuOHRoEU8nHOXZnk.htm)|Lesson of Elements|Leçon des éléments|libre|
|[witch-02-HbREpzudMXPscgCj.htm](classfeatures/witch-02-HbREpzudMXPscgCj.htm)|Lesson of Life|Leçon de vie|libre|
|[witch-02-KHKe3PmctOFUeh85.htm](classfeatures/witch-02-KHKe3PmctOFUeh85.htm)|Lesson of Protection|Leçon de protection|libre|
|[witch-02-PLMmDXJCDdMS0V5C.htm](classfeatures/witch-02-PLMmDXJCDdMS0V5C.htm)|Lesson of Calamity|Leçon de calamité|libre|
|[witch-06-2IhZbkx889pATIjq.htm](classfeatures/witch-06-2IhZbkx889pATIjq.htm)|Lesson of Favors|Leçon de faveurs|libre|
|[witch-06-rECRfbkVwHuG06vO.htm](classfeatures/witch-06-rECRfbkVwHuG06vO.htm)|Lesson of Snow|Leçon de neige|libre|
|[witch-06-XBgBh3xZhgGQP7lF.htm](classfeatures/witch-06-XBgBh3xZhgGQP7lF.htm)|Lesson of Mischief|Leçon de malice|libre|
|[witch-06-YSxymsAe2Dvq47f2.htm](classfeatures/witch-06-YSxymsAe2Dvq47f2.htm)|Lesson of Shadow|Leçon d'ombre|libre|
|[witch-10-29ynmOCdtIFDzrWx.htm](classfeatures/witch-10-29ynmOCdtIFDzrWx.htm)|Lesson of the Frozen Queen|Leçon de la reine de glace|libre|
|[witch-10-7Y2XDqr4gRisjiAG.htm](classfeatures/witch-10-7Y2XDqr4gRisjiAG.htm)|Lesson of Renewal|Leçon de renouveau|libre|
|[witch-10-RcmqV0cuOLcnKQr0.htm](classfeatures/witch-10-RcmqV0cuOLcnKQr0.htm)|Lesson of Bargains|Leçon de marchandage|libre|
|[witch-10-wRHeY7tCN6HFFF3a.htm](classfeatures/witch-10-wRHeY7tCN6HFFF3a.htm)|Lesson of Death|Leçon de mort|libre|
|[witch-17-FuVO8ksHI1B5ozVI.htm](classfeatures/witch-17-FuVO8ksHI1B5ozVI.htm)|Will of the Pupil|Volonté du pupille|libre|
|[witch-19-cDnFXfl3i5Z2l7JP.htm](classfeatures/witch-19-cDnFXfl3i5Z2l7JP.htm)|Patron's Gift|Cadeau du patron|libre|
|[wizard-01-7nbKDBGvwSx9T27G.htm](classfeatures/wizard-01-7nbKDBGvwSx9T27G.htm)|Arcane School|École arcanique|libre|
|[wizard-01-89zWKD2CN7nRu2xp.htm](classfeatures/wizard-01-89zWKD2CN7nRu2xp.htm)|Experimental Spellshaping|Expérimentation mutamagique|libre|
|[wizard-01-au0lwQ1nAcNQwcGh.htm](classfeatures/wizard-01-au0lwQ1nAcNQwcGh.htm)|Arcane Bond|Lien arcanique|officielle|
|[wizard-01-E4GZDMn4DYk6qSEV.htm](classfeatures/wizard-01-E4GZDMn4DYk6qSEV.htm)|School of Battle Magic|École de la magie de combat|libre|
|[wizard-01-Klb35AwlkNrq1gpB.htm](classfeatures/wizard-01-Klb35AwlkNrq1gpB.htm)|Staff Nexus|Bâton nexus|libre|
|[wizard-01-L5FiuXsfW6Sa31gO.htm](classfeatures/wizard-01-L5FiuXsfW6Sa31gO.htm)|School of Mentalism|École du Mentalisme|libre|
|[wizard-01-M89l9FOnjHe63wD7.htm](classfeatures/wizard-01-M89l9FOnjHe63wD7.htm)|Arcane Thesis|Thèse arcanique|libre|
|[wizard-01-OAcxS625AXSGrQIC.htm](classfeatures/wizard-01-OAcxS625AXSGrQIC.htm)|Spell Blending|Mélange de sorts|libre|
|[wizard-01-QzWXMCSGNfvvpYgF.htm](classfeatures/wizard-01-QzWXMCSGNfvvpYgF.htm)|Spell Substitution|Substitution de sort|officielle|
|[wizard-01-S6WW4Yyg4XonXGHD.htm](classfeatures/wizard-01-S6WW4Yyg4XonXGHD.htm)|Wizard Spellcasting|Incantation de magicien|libre|
|[wizard-01-SNZ46g3u7U6x0XJj.htm](classfeatures/wizard-01-SNZ46g3u7U6x0XJj.htm)|Improved Familiar Attunement|Harmonisation avec le familier améliorée|libre|
|[wizard-01-wObrT6PytPdS5aUi.htm](classfeatures/wizard-01-wObrT6PytPdS5aUi.htm)|School of Ars Grammatica|École des lettres|libre|
|[wizard-01-xYYhJtGhFSWNifcO.htm](classfeatures/wizard-01-xYYhJtGhFSWNifcO.htm)|School of Unified Magical Theory|École de la théorie magique unifiée|libre|
|[wizard-01-YZ2XPmx1WHyWtM0g.htm](classfeatures/wizard-01-YZ2XPmx1WHyWtM0g.htm)|School of Civic Wizardry|École de magie civique|libre|
|[wizard-01-ZBFICTkzUjE4BDGJ.htm](classfeatures/wizard-01-ZBFICTkzUjE4BDGJ.htm)|School of Protean Form|École de forme protéenne|libre|
|[wizard-01-ZpFCZnVzIfZLfNii.htm](classfeatures/wizard-01-ZpFCZnVzIfZLfNii.htm)|School of the Boundary|École de la frontière|libre|
|[wizard-07-cD3nSupdCvONuHiE.htm](classfeatures/wizard-07-cD3nSupdCvONuHiE.htm)|Expert Spellcaster|Incantateur expert|libre|
|[wizard-11-GBsC2cARoFiqMi9V.htm](classfeatures/wizard-11-GBsC2cARoFiqMi9V.htm)|Wizard Weapon Expertise|Expertise avec les armes du magicien|officielle|
|[wizard-13-gU7epgcPSm0TD1UK.htm](classfeatures/wizard-13-gU7epgcPSm0TD1UK.htm)|Defensive Robes|Robes défensives|libre|
|[wizard-15-l1InYvhnQSz6Ucxc.htm](classfeatures/wizard-15-l1InYvhnQSz6Ucxc.htm)|Master Spellcaster|Incantateur maître|libre|
|[wizard-17-j5TZw3xoIo6Lz0Re.htm](classfeatures/wizard-17-j5TZw3xoIo6Lz0Re.htm)|Prodigious Will|Volonté prodigieuse|libre|
|[wizard-19-Hfaa7TuLn3nE8lr3.htm](classfeatures/wizard-19-Hfaa7TuLn3nE8lr3.htm)|Legendary Spellcaster|Incantateur légendaire|libre|
|[wizard-19-ZjwJHmjPrSs6VDez.htm](classfeatures/wizard-19-ZjwJHmjPrSs6VDez.htm)|Archwizard's Spellcraft|Art magique de l'archimage|libre|
