# État de la traduction (ac-support-benefits)

 * **libre**: 56


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[2Jaz98WiQMNnqKCD.htm](ac-support-benefits/2Jaz98WiQMNnqKCD.htm)|Monitor Lizard Support Benefit|Avantage de soutien du varan|libre|
|[3jZOVJsYwBQ7GPDi.htm](ac-support-benefits/3jZOVJsYwBQ7GPDi.htm)|Vampiric Animal Support Benefit|Avantage de soutien de l'animal vampirique|libre|
|[4kkcfQdHAb4YTVNw.htm](ac-support-benefits/4kkcfQdHAb4YTVNw.htm)|Ghost Support Benefit|Avantage de soutien de fantôme|libre|
|[5HnWXmOp8qNjfhdU.htm](ac-support-benefits/5HnWXmOp8qNjfhdU.htm)|Wolf Support Benefit|Avantage de soutien du loup|libre|
|[7rhcqwMDt2xqnI1N.htm](ac-support-benefits/7rhcqwMDt2xqnI1N.htm)|Bird Support Benefit|Avantage de soutien de l'oiseau de proie|libre|
|[8b1G95eWa3DaTvw8.htm](ac-support-benefits/8b1G95eWa3DaTvw8.htm)|Cave Pterosaur Support Benefit|Avantage de soutien du ptérosaure des grottes|libre|
|[8re1d4lM0Dn4sLto.htm](ac-support-benefits/8re1d4lM0Dn4sLto.htm)|Boar Support Benefit|Avantage de soutien du sanglier|libre|
|[9Y5QKMvy0KsliE29.htm](ac-support-benefits/9Y5QKMvy0KsliE29.htm)|Moth Support Benefit|Avantage de soutien du papillon de nuit|libre|
|[Aj7JWFXIbF3qmvSX.htm](ac-support-benefits/Aj7JWFXIbF3qmvSX.htm)|Bat Support Benefit|Avantage de soutien de la chauve-souris|libre|
|[aMFtyZ3QNQkduwqR.htm](ac-support-benefits/aMFtyZ3QNQkduwqR.htm)|Air Elemental Support Benefit|Avantage de soutien de l'élémentaire d'air|libre|
|[AvDlo1mgxXd7ZA8W.htm](ac-support-benefits/AvDlo1mgxXd7ZA8W.htm)|Bear Support Benefit|Avantage de soutien de l'ours|libre|
|[bEkXle5FLNxnp3IE.htm](ac-support-benefits/bEkXle5FLNxnp3IE.htm)|Undead Bird of Prey Support Benefit|Avantage de soutien : Oiseau de proie squelettique|libre|
|[BFvS1wnFxgUqgX8m.htm](ac-support-benefits/BFvS1wnFxgUqgX8m.htm)|Water Elemental Support Benefit|Avantage de soutien de l'élémentaire d'eau|libre|
|[bHaJZSm7SakAsk00.htm](ac-support-benefits/bHaJZSm7SakAsk00.htm)|Skeletal Servant Support Benefit|Avantage de soutien de serviteur squelettique|libre|
|[bO92dKyk8V4aM0NP.htm](ac-support-benefits/bO92dKyk8V4aM0NP.htm)|Arboreal Sapling Support Benefit|Avantage de soutien du jeune arboréen|libre|
|[C41lCXjerYjMyOTI.htm](ac-support-benefits/C41lCXjerYjMyOTI.htm)|Riding Drake Support Benefit|Avantage de soutien du drake de monte|libre|
|[CKMQC3zu1XoGjeNd.htm](ac-support-benefits/CKMQC3zu1XoGjeNd.htm)|Skeletal Mount Support Benefit|Avantage de soutien de monture squelettique|libre|
|[Du0wsxf7TitL6wSb.htm](ac-support-benefits/Du0wsxf7TitL6wSb.htm)|Dromaeosaur Support Benefit|Avantage de soutien du droméosaure|libre|
|[Fl1VEpYhmScEEon8.htm](ac-support-benefits/Fl1VEpYhmScEEon8.htm)|Capybara Support Benefit|Avantage de soutien du capybara|libre|
|[g3Wol5lNhQdxqs7t.htm](ac-support-benefits/g3Wol5lNhQdxqs7t.htm)|Draft Lizard Support Benefit|Avantage de soutien du lézard de trait|libre|
|[h65iGxPkfrT8DdPv.htm](ac-support-benefits/h65iGxPkfrT8DdPv.htm)|Shadow Hound Support Benefit|Avantage de soutien du molosse d'ombre|libre|
|[hC0YEa4Z8I4XxV3r.htm](ac-support-benefits/hC0YEa4Z8I4XxV3r.htm)|Oozeform Chair Support Benefit|Avantage de soutien du fauteuil de forme de vase|libre|
|[HCQXxqMtH81jE1W5.htm](ac-support-benefits/HCQXxqMtH81jE1W5.htm)|Cat Support Benefit|Avantage de soutien du félin|libre|
|[hjvjnSzrslebg0dk.htm](ac-support-benefits/hjvjnSzrslebg0dk.htm)|Rootball Chair Support Benefit|Avantage de soutien du Fauteuil motte de racines|libre|
|[imNJpD4afn4UrLpu.htm](ac-support-benefits/imNJpD4afn4UrLpu.htm)|Undead Hand Support Benefit|Avantage de soutien de la main morte-vivante|libre|
|[IUXX1lUd9kMzV4bq.htm](ac-support-benefits/IUXX1lUd9kMzV4bq.htm)|Water Wraith Support Benefit|Avantage de soutien de l'âme-en-peine aquatique|libre|
|[kecawjDhBFz5QA4f.htm](ac-support-benefits/kecawjDhBFz5QA4f.htm)|Rhinoceros Support Benefit|Avantage de soutien du rhinocéros|libre|
|[KomM6mTwJAzvqAgi.htm](ac-support-benefits/KomM6mTwJAzvqAgi.htm)|Wood Elemental Support Benefit|Avantage de soutien d'élémentaire de bois|libre|
|[kZvomyJobGrDs1MU.htm](ac-support-benefits/kZvomyJobGrDs1MU.htm)|Legchair Support Benefit|Avantage de soutien de la chaise sur pattes|libre|
|[lzaNwsSxjVPH1nzn.htm](ac-support-benefits/lzaNwsSxjVPH1nzn.htm)|Camel Support benefit|Avantage de soutien du chameau|libre|
|[mFTzDCqNL5EdwlGm.htm](ac-support-benefits/mFTzDCqNL5EdwlGm.htm)|Beetle Support Benefit|Avantage de soutien du scarabée|libre|
|[N9fYaUuIgD6RQ7CY.htm](ac-support-benefits/N9fYaUuIgD6RQ7CY.htm)|Zombie Mount Support Benefit|Avantage de soutien : Monture zombie|libre|
|[nDUwRm0TOtc2EX0O.htm](ac-support-benefits/nDUwRm0TOtc2EX0O.htm)|Cave Gecko Support Benefit|Avantage de soutien du gecko des cavernes|libre|
|[nQwTbiyTHahsvmBi.htm](ac-support-benefits/nQwTbiyTHahsvmBi.htm)|Badger Support Benefit|Avantage de soutien du blaireau|libre|
|[NSdcXgEHVHdfJlkb.htm](ac-support-benefits/NSdcXgEHVHdfJlkb.htm)|Fire Elemental Support Benefit|Avantage de soutien de l'élémentaire de feu|libre|
|[oLwBrrW33fIjSHLr.htm](ac-support-benefits/oLwBrrW33fIjSHLr.htm)|Terror Bird Support Benefit|Avantage de soutien de l'oiseau de terreur|libre|
|[QEZUnvbdrgQJAUs0.htm](ac-support-benefits/QEZUnvbdrgQJAUs0.htm)|Vulture Support Benefit|Avantage de soutien du vautour|libre|
|[QiQvx7jtfrkGnf13.htm](ac-support-benefits/QiQvx7jtfrkGnf13.htm)|Pangolin Support Benefit|Avantage de soutien du pangolin|libre|
|[Qm0iwIYyy2gbnYXe.htm](ac-support-benefits/Qm0iwIYyy2gbnYXe.htm)|Ape Support Benefit|Avantage de soutien du singe|libre|
|[r7QN2lUVWXlcynMW.htm](ac-support-benefits/r7QN2lUVWXlcynMW.htm)|Elephant Support Benefit|Avantage de soutien de l'éléphant|libre|
|[Si13B4VFANZN8mi1.htm](ac-support-benefits/Si13B4VFANZN8mi1.htm)|Skeletal Constrictor Support Benefit|Avantage de soutien : Serpent constricteur squelettique|libre|
|[SZD1hU2ageIDw4To.htm](ac-support-benefits/SZD1hU2ageIDw4To.htm)|Metal Elemental Support Benefit|Avantage de soutien d'élémentaire de métal|libre|
|[TDMhLC16cQOYBbmA.htm](ac-support-benefits/TDMhLC16cQOYBbmA.htm)|Triceratops Support Benefit|Avantage de soutien du tricératops|libre|
|[trujzLuDqjQ7nJEO.htm](ac-support-benefits/trujzLuDqjQ7nJEO.htm)|Snake Support Benefit|Avantage de soutien du serpent|libre|
|[ugEUIh47YrNUIsnd.htm](ac-support-benefits/ugEUIh47YrNUIsnd.htm)|Goat Support Benefit|Avantage de soutien de la chèvre|libre|
|[uZLtEOimbFnEDUKh.htm](ac-support-benefits/uZLtEOimbFnEDUKh.htm)|Ulgrem-Lurann Support Benefit|Avantage de soutien de l'ulgrem-Lurann|libre|
|[vGAy81FNblv833jk.htm](ac-support-benefits/vGAy81FNblv833jk.htm)|Augdunar Support Benefit|Avantage de soutien de l'augdunar|libre|
|[wf20kx6pPGBWwUn2.htm](ac-support-benefits/wf20kx6pPGBWwUn2.htm)|Horse Support Benefit|Avantage de soutien du cheval|libre|
|[wlh1poSMLK9wk2Nw.htm](ac-support-benefits/wlh1poSMLK9wk2Nw.htm)|Hyena Support Benefit|Avantage de soutien de la hyène|libre|
|[XrxIRaD1A5tpKm1t.htm](ac-support-benefits/XrxIRaD1A5tpKm1t.htm)|Tyrannosaurus Support Benefit|Avantage de soutien du tyrannosaure|libre|
|[XteDiKFR58AkGXlP.htm](ac-support-benefits/XteDiKFR58AkGXlP.htm)|Earth Elemental Support Benefit|Avantage de soutien d'élémentaire de terre|libre|
|[xtXdDC5KrUqh7eCu.htm](ac-support-benefits/xtXdDC5KrUqh7eCu.htm)|Scorpion Support Benefit|Avantage de soutien du scorpion|libre|
|[yAYlM3aIRYnqx6j8.htm](ac-support-benefits/yAYlM3aIRYnqx6j8.htm)|Zombie Support Benefit|Avantage de soutien du zombie|libre|
|[yfjeiYMC27YGgoGW.htm](ac-support-benefits/yfjeiYMC27YGgoGW.htm)|Crocodile Support Benefit|Avantage de soutien du crocodile|libre|
|[YsLLV4J7L6fgzTXf.htm](ac-support-benefits/YsLLV4J7L6fgzTXf.htm)|Shark Support Benefit|Avantage de soutien du squale|libre|
|[zE01vpjowhoKygig.htm](ac-support-benefits/zE01vpjowhoKygig.htm)|Zombie Carrion Bird Support Benefit|Avantage de soutien : Oiseau charognard zombie|libre|
