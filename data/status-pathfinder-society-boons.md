# État de la traduction (pathfinder-society-boons)

 * **libre**: 124
 * **changé**: 6
 * **officielle**: 2


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[95R55tdobViU7ZWe.htm](pathfinder-society-boons/95R55tdobViU7ZWe.htm)|Mentor, Magical|Mentor magique|changé|
|[BEaLe5M23Q7LKTLL.htm](pathfinder-society-boons/BEaLe5M23Q7LKTLL.htm)|Wayfinder, Adamant|Guide, Inflexible|changé|
|[rFgWvC1UADsavY5J.htm](pathfinder-society-boons/rFgWvC1UADsavY5J.htm)|Resist Corruption|Résister à la corruption|changé|
|[rJfATB750MbndGeP.htm](pathfinder-society-boons/rJfATB750MbndGeP.htm)|S2-08 - Baba Yaga's Tutelage|S2-08 - Tutorat de Baba Yaga|changé|
|[SI37oVZxjwXew3E4.htm](pathfinder-society-boons/SI37oVZxjwXew3E4.htm)|Bring Them Back Alive|Ranimez les|changé|
|[YVOyEfO8tTx4jj2q.htm](pathfinder-society-boons/YVOyEfO8tTx4jj2q.htm)|S1-20 - Waters of Warlock's Barrow|S1-20 - Eaux de Warlock's Barrow|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0CqkYRFUlb0tB9li.htm](pathfinder-society-boons/0CqkYRFUlb0tB9li.htm)|S1-23 - Seasoned Diplomat|S1-23 - Diplomate chevronné|libre|
|[0FwLrQb5EeQDAUNW.htm](pathfinder-society-boons/0FwLrQb5EeQDAUNW.htm)|Naturalist|Naturaliste|libre|
|[0izXWwYqRGQq74mG.htm](pathfinder-society-boons/0izXWwYqRGQq74mG.htm)|S1-07 - Blast from the Past|S1-07 - Retour du Passé|libre|
|[0QRG6TAVwCWL2Zr8.htm](pathfinder-society-boons/0QRG6TAVwCWL2Zr8.htm)|Heroic Inspiration|Composition fortissimo|libre|
|[0zhQQK3gvu75ac1Q.htm](pathfinder-society-boons/0zhQQK3gvu75ac1Q.htm)|Eager Protege|Protégé fervent|libre|
|[1FWpmtZHOLRvEIdp.htm](pathfinder-society-boons/1FWpmtZHOLRvEIdp.htm)|Verdant Wheel Champion|Champion de la Roue verdoyante|libre|
|[1VWMLe12ZFqF9jah.htm](pathfinder-society-boons/1VWMLe12ZFqF9jah.htm)|Hireling, Expert|Mercenaire, Expert|libre|
|[1welewgoB0z86Nms.htm](pathfinder-society-boons/1welewgoB0z86Nms.htm)|S1-03 - Team Player|S1-03 - Joueur d'équipe|libre|
|[2QzXBsURLO7xECxO.htm](pathfinder-society-boons/2QzXBsURLO7xECxO.htm)|FoP - Fiery Companion|FoP - Compagnon enflammé|libre|
|[3K8yzfU2t9chVv6h.htm](pathfinder-society-boons/3K8yzfU2t9chVv6h.htm)|AoA #149: Boon B - Katapesh Companion|AoA #149: Avantage B - Compagnon katapesh|libre|
|[3lHqim7ttvoMmVXW.htm](pathfinder-society-boons/3lHqim7ttvoMmVXW.htm)|Sellback Plan|Plan de revente|libre|
|[4lLhGgG30TMu8tIx.htm](pathfinder-society-boons/4lLhGgG30TMu8tIx.htm)|FoP - Noala's Lessons|FoP - Lesson de Noala|libre|
|[4O3qxD3I4LUjl2Vq.htm](pathfinder-society-boons/4O3qxD3I4LUjl2Vq.htm)|Wayfinder, Harmonic|Guide, Harmonique|libre|
|[5tWlPSGW5AEk10GJ.htm](pathfinder-society-boons/5tWlPSGW5AEk10GJ.htm)|Consummate Dabbler|Amateur accompli|libre|
|[632pDmCtbQxBlE3d.htm](pathfinder-society-boons/632pDmCtbQxBlE3d.htm)|S1-02 - Cryptid Scholar|S1-02 - Érudit cryptide|libre|
|[6frkVrsCgTgFhMjl.htm](pathfinder-society-boons/6frkVrsCgTgFhMjl.htm)|Vigilant Seal Champion|Champion du Sceau du vigilant|libre|
|[6gVFdrqhs0mihRao.htm](pathfinder-society-boons/6gVFdrqhs0mihRao.htm)|S1-Q02 - One-Who-Waits|S1-Q02 - Celui qui attend|libre|
|[6oTXrz56Evik64Fy.htm](pathfinder-society-boons/6oTXrz56Evik64Fy.htm)|S1-04 - A Thorny Situation|S1-04 - Une situation épineuse|libre|
|[6XUhCWU0h0qbgL5Z.htm](pathfinder-society-boons/6XUhCWU0h0qbgL5Z.htm)|S2-00 - Spark of Life (levels 7-8)|S2-00 - Étincelle de vie (niveau 7-8)|libre|
|[8cMFcCa3h9XfWNkZ.htm](pathfinder-society-boons/8cMFcCa3h9XfWNkZ.htm)|AoA #149: Boon A - Advanced Training|AoA #49 : Avantage A - Formation avancée|libre|
|[8fbBYNkFQgU3IQV0.htm](pathfinder-society-boons/8fbBYNkFQgU3IQV0.htm)|Grand Archive Champion|Champion de la Grande Archive|libre|
|[9w8TdCsGEpttXYuy.htm](pathfinder-society-boons/9w8TdCsGEpttXYuy.htm)|Mentor, Worldly|Mentor expérimenté|libre|
|[aG5cua0K2SlurXxw.htm](pathfinder-society-boons/aG5cua0K2SlurXxw.htm)|Promotional Accessory|Accessoire promotionnel|libre|
|[awcOD618fuXZN3rh.htm](pathfinder-society-boons/awcOD618fuXZN3rh.htm)|Practiced Medic|Secouriste entraîné|libre|
|[B7fDgC6tcv41lcP7.htm](pathfinder-society-boons/B7fDgC6tcv41lcP7.htm)|S1-Q11 - Amateur Genealogist|S-Q11 - Généalogiste amateur|libre|
|[bcsusUErwGQYHj6d.htm](pathfinder-society-boons/bcsusUErwGQYHj6d.htm)|Multicultural Training|Formation multiculturelle|libre|
|[bMp6W5kPNyqBoOxm.htm](pathfinder-society-boons/bMp6W5kPNyqBoOxm.htm)|Charitable Adventure|Aventure charitable|libre|
|[ca869lJo8E51WcW4.htm](pathfinder-society-boons/ca869lJo8E51WcW4.htm)|Hireling, Master|Mercenaire, Maître|libre|
|[cjjRFoOE1GtjEA3F.htm](pathfinder-society-boons/cjjRFoOE1GtjEA3F.htm)|Promotional Service Award|Récompense du service promotionnel|libre|
|[CjW54WSPDcykzqj9.htm](pathfinder-society-boons/CjW54WSPDcykzqj9.htm)|AoA #148: Boon B - Expanded Summoning|AoA #148: Avantage B - Convocation étendue|libre|
|[CKyGNdBL8Qt4ysKt.htm](pathfinder-society-boons/CKyGNdBL8Qt4ysKt.htm)|S2-03 - Fluent in Cyclops|S2-03 - langue cyclope|libre|
|[CLRt3iFqWlMwCNVl.htm](pathfinder-society-boons/CLRt3iFqWlMwCNVl.htm)|Vigilant Seal Champion, Improved|Champion du Sceau du vigilant, amélioré|libre|
|[CPcMqEd4H5yIVl3i.htm](pathfinder-society-boons/CPcMqEd4H5yIVl3i.htm)|World Traveller|Voyageur mondial|libre|
|[CW73M2apBaQh6wLm.htm](pathfinder-society-boons/CW73M2apBaQh6wLm.htm)|Unparalleled Scholarship|Boursier hors-pair|libre|
|[cyrntY2zgDyzxQGO.htm](pathfinder-society-boons/cyrntY2zgDyzxQGO.htm)|Verdant Wheel Champion, Improved|Champion de la Roue verdoyante amélioré|libre|
|[DcMAGJWq2G5kGRWn.htm](pathfinder-society-boons/DcMAGJWq2G5kGRWn.htm)|S1-16 - Blood Offering|S1-16 - Don de sang|libre|
|[DD1mlAEaXdM1pBze.htm](pathfinder-society-boons/DD1mlAEaXdM1pBze.htm)|S1-Q07 - Amateur Adjuster|S-Q07 - Expertise d'amateur|libre|
|[DkqU67wxvbDu0Qrs.htm](pathfinder-society-boons/DkqU67wxvbDu0Qrs.htm)|Mentor, Rugged|Mentor robuste|libre|
|[DWcmLzzT1eYk76c7.htm](pathfinder-society-boons/DWcmLzzT1eYk76c7.htm)|AoA #146: Mwangi Summoner|AoA #146: Invocateur mwangi|libre|
|[En3RdoXDAtyXdNKq.htm](pathfinder-society-boons/En3RdoXDAtyXdNKq.htm)|Heroic Intervention|Interventaion héroïque|libre|
|[ePZOmPbGXzrEaXXs.htm](pathfinder-society-boons/ePZOmPbGXzrEaXXs.htm)|S1-11 - Devil's Keep|S1-11 - Fort du diable|libre|
|[fTndDUqUDSVXM0Qm.htm](pathfinder-society-boons/fTndDUqUDSVXM0Qm.htm)|S1-11 - Diggen the Liar|S1-11 - Diggen le Menteur|libre|
|[G0hv2CI7TKcUcSVs.htm](pathfinder-society-boons/G0hv2CI7TKcUcSVs.htm)|Vault Delver|Chasseur de coffres|libre|
|[G7fN5K1TfmNhTb2w.htm](pathfinder-society-boons/G7fN5K1TfmNhTb2w.htm)|S2-00 - Spark of Life (levels 1-2)|S2-00 - Étincelle de vie (Niveaux 1-2)|libre|
|[gdkQ04RgMJZ0QxbZ.htm](pathfinder-society-boons/gdkQ04RgMJZ0QxbZ.htm)|Crafter's Workshop|Atelier d'artisan|libre|
|[GIdtqEFKKgwPmDBq.htm](pathfinder-society-boons/GIdtqEFKKgwPmDBq.htm)|S1-05 - Experienced Mountaineer|S1-05 - Alpiniste chevronné|libre|
|[giyLRNefaC3IsFay.htm](pathfinder-society-boons/giyLRNefaC3IsFay.htm)|S1-10 - Tarnbreaker Champions|S1-10 - Champion de Tarnbreaker|libre|
|[Gmzby7whs5O0wuYC.htm](pathfinder-society-boons/Gmzby7whs5O0wuYC.htm)|Untarnished Reputation|Réputation impeccable|libre|
|[GRiNBy8yv4sWp1ua.htm](pathfinder-society-boons/GRiNBy8yv4sWp1ua.htm)|Bequethal|Héritage|libre|
|[grkmoNwwHOROKER7.htm](pathfinder-society-boons/grkmoNwwHOROKER7.htm)|AoA #150: Boon A - Legacy of Ashes|AoA #150 : Avantage A - Héritage de cendres|libre|
|[hghR0f5IetZBvyDR.htm](pathfinder-society-boons/hghR0f5IetZBvyDR.htm)|Heroic Defiance|Défiance héroïque|libre|
|[hxp0fDMrB7BT3y3T.htm](pathfinder-society-boons/hxp0fDMrB7BT3y3T.htm)|S1-15 - Finadar Leshy|S1-15 - Léchi Finadar|libre|
|[hy1gMMpRmujuw8i4.htm](pathfinder-society-boons/hy1gMMpRmujuw8i4.htm)|Leader By Example|Meneur par l'exemple|libre|
|[ig06ikA6rnNP1JUb.htm](pathfinder-society-boons/ig06ikA6rnNP1JUb.htm)|S1-01 - Society Connections|S1-01 - Connexions sociales|libre|
|[iRLU5DTWY16HIqcc.htm](pathfinder-society-boons/iRLU5DTWY16HIqcc.htm)|S1-22 - Doom Averted|S1-22 - Péril évité|libre|
|[J1hUP5SI4MJvlK36.htm](pathfinder-society-boons/J1hUP5SI4MJvlK36.htm)|Mentor, Skillful|Mentor talentueux|libre|
|[J6lNh2fUQ6jYTRak.htm](pathfinder-society-boons/J6lNh2fUQ6jYTRak.htm)|Wayfinder, Radiant|Guide, Radiance|officielle|
|[jzJ575vAsw2MaRgo.htm](pathfinder-society-boons/jzJ575vAsw2MaRgo.htm)|Envoy's Alliance Champion|Champion de l'Alliance des émissaires|libre|
|[JznxnYTs38GYpW1p.htm](pathfinder-society-boons/JznxnYTs38GYpW1p.htm)|S1-14 - Big Game Hunter|S1-14 - Chasseur de gros gibier|libre|
|[K21KQsrR3RMcJ1du.htm](pathfinder-society-boons/K21KQsrR3RMcJ1du.htm)|Swift Traveler|Voyageur rapide|libre|
|[KXO4CUklzuJ0tZnw.htm](pathfinder-society-boons/KXO4CUklzuJ0tZnw.htm)|Home Region|Région d'origine|libre|
|[KZrXvKlAVcpLgGF8.htm](pathfinder-society-boons/KZrXvKlAVcpLgGF8.htm)|Society Recruiter|Recruteur de la Société|libre|
|[l8WNWPFUdxcqEG6e.htm](pathfinder-society-boons/l8WNWPFUdxcqEG6e.htm)|Envoy's Alliance Champion, Improved|Champion de l'Alliance des émissaires amélioré|libre|
|[lJ1lNwGxV0IrsN2z.htm](pathfinder-society-boons/lJ1lNwGxV0IrsN2z.htm)|S1-08 - River Kingdoms Politician|S1-08 - Politicien des Royaumes fluviaux|libre|
|[M1flDLvjKBH9QPcx.htm](pathfinder-society-boons/M1flDLvjKBH9QPcx.htm)|Storied Talent|Talent raconté|libre|
|[M2lGGGaPIOimeA31.htm](pathfinder-society-boons/M2lGGGaPIOimeA31.htm)|Resurrection Plan|Plan de Résurrection|libre|
|[M8cJ8qOwWj7XN6h1.htm](pathfinder-society-boons/M8cJ8qOwWj7XN6h1.htm)|Radiant Oath Champion|Champion du Serment radieux|libre|
|[MLn6UbZsU4PphXLE.htm](pathfinder-society-boons/MLn6UbZsU4PphXLE.htm)|S1-Q06 - Secrets of the Jistkan Alchemists|S1-Q06 - Secrets des alchimiste du Jistkan|libre|
|[MN3MlIx7djj7WDNY.htm](pathfinder-society-boons/MN3MlIx7djj7WDNY.htm)|Meticulous Appraisal|Évaluation méticuleuse|libre|
|[mrlQrZd0yBwOj2lk.htm](pathfinder-society-boons/mrlQrZd0yBwOj2lk.htm)|Hireling, Professional|Mercenaire professionel|libre|
|[Mu5GQ9ZEz3geAYxK.htm](pathfinder-society-boons/Mu5GQ9ZEz3geAYxK.htm)|Leshy Companion|Compagnon Léchi|libre|
|[MuhZZkLR8y9eEndN.htm](pathfinder-society-boons/MuhZZkLR8y9eEndN.htm)|S1-Q10 - Sewer Dragon Recruit|S1-Q10 - Recruter des Dragons des égoûts|libre|
|[Nadcb08xcTAoPBEX.htm](pathfinder-society-boons/Nadcb08xcTAoPBEX.htm)|AoA #147: Boon A - Heroic Membership|A0A #147: Avantage A - Adhésion héroïque|libre|
|[NR19RYJLveHrFYvf.htm](pathfinder-society-boons/NR19RYJLveHrFYvf.htm)|S1-Q03 - Legacy of the Gorget|S1-Q03 - Héritage de Gorgerin|libre|
|[ocS6B3AV1n7yHFd5.htm](pathfinder-society-boons/ocS6B3AV1n7yHFd5.htm)|Exemplary Recruiter|Recruteur exemplaire|libre|
|[oOB1uKvoBMGj7ykZ.htm](pathfinder-society-boons/oOB1uKvoBMGj7ykZ.htm)|Curse Breaker|Briseur de malédiction|libre|
|[opaFl76Nbz1dITLz.htm](pathfinder-society-boons/opaFl76Nbz1dITLz.htm)|Secondary Initiation|Initiation secondaire|libre|
|[P1sdPHrbAfqrEAM3.htm](pathfinder-society-boons/P1sdPHrbAfqrEAM3.htm)|S1-01 - Engraved Wayfinder|S01-01 - Guide gravé|libre|
|[p8TKPInhya6oab1w.htm](pathfinder-society-boons/p8TKPInhya6oab1w.htm)|S1-19 - Iolite Trainee Hobgoblin|S1-19 - Recrue Iolite Hobgobeline|libre|
|[PbEqXHWiyzii4g8T.htm](pathfinder-society-boons/PbEqXHWiyzii4g8T.htm)|S1-25 - Grand Finale|S1-25 - Grand Final|libre|
|[ph7CO8ong1sZmrNc.htm](pathfinder-society-boons/ph7CO8ong1sZmrNc.htm)|Horizon Hunters Champion|Champion des Chasseurs de l'horizon|libre|
|[prHShOQxNpLXwlss.htm](pathfinder-society-boons/prHShOQxNpLXwlss.htm)|S1-Q04 - Fane's Friend|S1-Q04 - Ami de Fane|libre|
|[PXtBhRVHM28angjV.htm](pathfinder-society-boons/PXtBhRVHM28angjV.htm)|Mentor, Protective|Mentor protecteur|libre|
|[qaSlz43CHPiCoIOC.htm](pathfinder-society-boons/qaSlz43CHPiCoIOC.htm)|Radiant Oath Champion, Improved|Champion du Serment radieux amélioré|libre|
|[rddlbymeG9KbQYQM.htm](pathfinder-society-boons/rddlbymeG9KbQYQM.htm)|Translator|Traducteur|libre|
|[rdY0vyXfrIlkQgqd.htm](pathfinder-society-boons/rdY0vyXfrIlkQgqd.htm)|Beginnings and Endings|Débuts et Fins|libre|
|[RGSw5tAc4ALp97Qe.htm](pathfinder-society-boons/RGSw5tAc4ALp97Qe.htm)|S1-Q04 - Stella's Associate|S1-Q04 - Associé de Stella|libre|
|[RJYIefY9Skx8CAgw.htm](pathfinder-society-boons/RJYIefY9Skx8CAgw.htm)|S1-Q09 - Wayfinder Connections|S1-Q09 - Connexions Guide|libre|
|[RLzWznPsb2VpbxTy.htm](pathfinder-society-boons/RLzWznPsb2VpbxTy.htm)|S1-Q12 - Fruit Basket|S1-Q12 - Panier de fruit|libre|
|[sBXsfT0uYltF8TFE.htm](pathfinder-society-boons/sBXsfT0uYltF8TFE.htm)|Hireling|Mercenaire|libre|
|[Sc9gT40T28tFhSzq.htm](pathfinder-society-boons/Sc9gT40T28tFhSzq.htm)|Horizon Hunters Champion, Improved|Champion des Chasseur de l'horizon amélioré|libre|
|[SDDbh7KmDn5HUbst.htm](pathfinder-society-boons/SDDbh7KmDn5HUbst.htm)|Promotional Vestments|Vêtements promotionnels|libre|
|[SujEcE3H5n9kx1PI.htm](pathfinder-society-boons/SujEcE3H5n9kx1PI.htm)|Wayfinder, Esoteric|Guide, Ésotérique|libre|
|[SxUBWlQj6BT5g4Rw.htm](pathfinder-society-boons/SxUBWlQj6BT5g4Rw.htm)|Wayfinder|Guide des Éclaireurs|libre|
|[T7gyK9LV14wI7Xdy.htm](pathfinder-society-boons/T7gyK9LV14wI7Xdy.htm)|S1-07 - To Seal and Protect|S1-08 - Sceller et protéger|libre|
|[taUqYoHuWH9uEbsn.htm](pathfinder-society-boons/taUqYoHuWH9uEbsn.htm)|S1-17 - Fey Influence|S1-17 - Influence féerique|libre|
|[Tmqa8Q9zaYr0uj6F.htm](pathfinder-society-boons/Tmqa8Q9zaYr0uj6F.htm)|Grand Archive Champion, Improved|Champion de la Grande Archive amélioré|libre|
|[tX2vxY8RSdcBayEu.htm](pathfinder-society-boons/tX2vxY8RSdcBayEu.htm)|S1-18 - Narsen's Web|S1-18 - Toile de Narsen|libre|
|[U6QqFDstHNjEeOUS.htm](pathfinder-society-boons/U6QqFDstHNjEeOUS.htm)|Wayfinder, Rugged|Guide, Robuste|libre|
|[UNlaVxb0pDov3efw.htm](pathfinder-society-boons/UNlaVxb0pDov3efw.htm)|Heroic Resurgence|Renaissance héroïque|libre|
|[UWZfORgk1yVNV0RF.htm](pathfinder-society-boons/UWZfORgk1yVNV0RF.htm)|Exotic Edge|Pointe d'exotisme|libre|
|[VLjkX7HpDqKaidNd.htm](pathfinder-society-boons/VLjkX7HpDqKaidNd.htm)|AoA #148: Boon A - Crystal Clear|AoA #148: Avantage A - Clair comme du cristal|libre|
|[Vmg31SsMjjimghzP.htm](pathfinder-society-boons/Vmg31SsMjjimghzP.htm)|Menace Under Otari|Menace sous Otari|libre|
|[vYSA5M9fgBdYiGLT.htm](pathfinder-society-boons/vYSA5M9fgBdYiGLT.htm)|Mentor, Combat|Mentor de combat|officielle|
|[wcOnL9nFDfCkNVMZ.htm](pathfinder-society-boons/wcOnL9nFDfCkNVMZ.htm)|AoA #149: Boon C - Expanded Summoning|AoA #149 : Avantage C - Convocation étendue|libre|
|[wdjRk8JaZGP1mjqD.htm](pathfinder-society-boons/wdjRk8JaZGP1mjqD.htm)|S1-09 - Iruxi Bane|S1-09 - Fléau iruxi|libre|
|[We3sJV72xtdPeMjp.htm](pathfinder-society-boons/We3sJV72xtdPeMjp.htm)|AoA #150: Boon B - Paragon of Promise|AoA #150 : Avantage B - Parangon de Promesse|libre|
|[WGjcz2mkgGB1qCWw.htm](pathfinder-society-boons/WGjcz2mkgGB1qCWw.htm)|Preserve|Préservation|libre|
|[wkgBg5UD3bChfl4b.htm](pathfinder-society-boons/wkgBg5UD3bChfl4b.htm)|Heroic Recall|Souvenirs héroïques|libre|
|[WV4J7NMgKXNlXonW.htm](pathfinder-society-boons/WV4J7NMgKXNlXonW.htm)|AoA #147: Boon B - Expanded Summoning|AOA #147 : Avantage B - Convocation étendue|libre|
|[WVOP420jZ4jSTYI8.htm](pathfinder-society-boons/WVOP420jZ4jSTYI8.htm)|S1-06 - Traveler of the Spirit Road|S1-06 - Voyageur de la route des esprits|libre|
|[WYHPJyza1BJgtFq4.htm](pathfinder-society-boons/WYHPJyza1BJgtFq4.htm)|S1-Q01 - Sand Slide|S1-Q01 - Glissement de sable|libre|
|[Wz2F0qk4rxZeH311.htm](pathfinder-society-boons/Wz2F0qk4rxZeH311.htm)|Heroic Hustle|Empressement héroïque|libre|
|[XBf4Uz3n0AwSmpXx.htm](pathfinder-society-boons/XBf4Uz3n0AwSmpXx.htm)|Adversary Lore|Connaissance de l'adversaire|libre|
|[XkNW2dq5hwWOViYg.htm](pathfinder-society-boons/XkNW2dq5hwWOViYg.htm)|Off-Hours Study|Devoirs de vacances|libre|
|[XoamInggkxLh6Zo0.htm](pathfinder-society-boons/XoamInggkxLh6Zo0.htm)|Academic Conference|Conférence académique|libre|
|[XOVDOCsvNBX5LS8j.htm](pathfinder-society-boons/XOVDOCsvNBX5LS8j.htm)|S1-12 - Valais's Assurance|S1-12 - Assurance de Valais|libre|
|[xVMouu7E7Xy7AGER.htm](pathfinder-society-boons/xVMouu7E7Xy7AGER.htm)|S1-13 - Convention Hero|S1-13 - Convention de héros|libre|
|[XybjaZJlmBFFztwJ.htm](pathfinder-society-boons/XybjaZJlmBFFztwJ.htm)|S1-04 - Touched by the Storm|S1-04 - Touché par la tempête|libre|
|[Y69ZRmuoj0XK9AZm.htm](pathfinder-society-boons/Y69ZRmuoj0XK9AZm.htm)|S1-18 - Light in the Dark|S1-18 - Lumière dans les ténèbres|libre|
|[y8sk7Q4uAtacYdR2.htm](pathfinder-society-boons/y8sk7Q4uAtacYdR2.htm)|S1-Q02 - Student of the Unforgiving Fire|S1-Q02 - Étudiant du feu impitoyable|libre|
|[ybrynsFe4a0jJcgz.htm](pathfinder-society-boons/ybrynsFe4a0jJcgz.htm)|S2-00 - Spark of Life (levels 3-6)|S2-00 - Étincelle de vie (Niveau 3-6)|libre|
|[yNTePhAPVMguqUVA.htm](pathfinder-society-boons/yNTePhAPVMguqUVA.htm)|S1-Q08 - Numerian Archaeologist|S1-Q08 - Archéologiste numérien|libre|
|[YVpNrVBLRrHjaR4C.htm](pathfinder-society-boons/YVpNrVBLRrHjaR4C.htm)|S1-00 - Nexian Researcher|S1-00 - Chercheur nexien|libre|
|[ZtLTqT4G5q8SLJCT.htm](pathfinder-society-boons/ZtLTqT4G5q8SLJCT.htm)|S1-09 - Ally of the Iruxi|S1-09 - Allié des Iruxi|libre|
|[ZZvIzyH6SwyVkEYm.htm](pathfinder-society-boons/ZZvIzyH6SwyVkEYm.htm)|S1-21 - Maze Walker|S1-21 - Marcheur de labyrinthe|libre|
