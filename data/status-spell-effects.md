# État de la traduction (spell-effects)

 * **changé**: 129
 * **libre**: 337
 * **officielle**: 5
 * **aucune**: 3


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[hpbCDbDOoVyhOmck.htm](spell-effects/hpbCDbDOoVyhOmck.htm)|Spell Effect: Everlight|
|[Ig9p2rDXGYHpEuTx.htm](spell-effects/Ig9p2rDXGYHpEuTx.htm)|Spell Effect: Traveler's Transit|
|[znWiM5RYLvf8STmR.htm](spell-effects/znWiM5RYLvf8STmR.htm)|Spell Effect: Darkened Sight|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06zdFoxzuTpPPGyJ.htm](spell-effects/06zdFoxzuTpPPGyJ.htm)|Spell Effect: Rejuvenating Flames|Effet : Flammes rajeunissantes|changé|
|[0koqUuC1YW4F1l5z.htm](spell-effects/0koqUuC1YW4F1l5z.htm)|Spell Effect: Anima Invocation|Effet : Invocation d'anima|changé|
|[0OC945wcZ4H4akLz.htm](spell-effects/0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|Effet : Visage du conjurateur|changé|
|[0s6YaL3IjqECmjab.htm](spell-effects/0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Wyrm|Effet : Rugissement du Ver|changé|
|[0W87OkYi3qCwNGSj.htm](spell-effects/0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|Effet : Forme de daémon (Piscodaémon)|changé|
|[0yy4t4UY1HqrEo70.htm](spell-effects/0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|Effet : Forme de diable (Barbazu)|changé|
|[14m4s0FeRSqRlHwL.htm](spell-effects/14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|Effet : Contremesure arcanique|changé|
|[1kelGCsoXyGRqMd9.htm](spell-effects/1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|Effet : Édit diabolique|changé|
|[1n84AqLtsdT8I64W.htm](spell-effects/1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|Effet : Forme de daémon (Ceustodaémon)|changé|
|[1XlF12UbvJsTZxfp.htm](spell-effects/1XlF12UbvJsTZxfp.htm)|Spell Effect: Evolution Surge|Effet : Flux d'évolution|changé|
|[20egTKICPMhibqgn.htm](spell-effects/20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|Effet : Forme de démon (Vrock)|changé|
|[2wfrhRLmmgPSKbAZ.htm](spell-effects/2wfrhRLmmgPSKbAZ.htm)|Spell Effect: Animal Feature (Claws)|Effet : Trait animal (Griffes)|changé|
|[3vWfew0TIrcGRjLZ.htm](spell-effects/3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|Effet : Forme d'ange (Deva monadique)|changé|
|[41WThj17MZBXTO2X.htm](spell-effects/41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|Effet : Agrandissement (Intensifié 4e)|changé|
|[4ag0OHKfjROmR4Pm.htm](spell-effects/4ag0OHKfjROmR4Pm.htm)|Spell Effect: Anticipate Peril|Effet : Anticipation du danger|changé|
|[4FD4vJqVpuuJjk9Q.htm](spell-effects/4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|Effet : Échange d'esprit (Succès critique)|changé|
|[4xtce4xbwZ9wjwMW.htm](spell-effects/4xtce4xbwZ9wjwMW.htm)|Spell Effect: Angelic Halo|Effet : Halo angélique|changé|
|[5yCL7InrJDHpaQjz.htm](spell-effects/5yCL7InrJDHpaQjz.htm)|Spell Effect: Ant Haul|Effet : Charge de fourmi|changé|
|[6embuvXCpS3YOD5u.htm](spell-effects/6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|Effet : Toucher de résilience|changé|
|[6qvLnIkWAoGvTIWy.htm](spell-effects/6qvLnIkWAoGvTIWy.htm)|Spell Effect: Community Repair (Critical Failure)|Effet : Réparation communautaire (Échec critique)|changé|
|[70qdCBokXBvKIUIQ.htm](spell-effects/70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|Effet : Vision de faiblesse|changé|
|[7zJPd2BsFl82qFRV.htm](spell-effects/7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression (+3)|Effet : Agression protectrice (+3)|changé|
|[81TfqzTfIqkQA4Dy.htm](spell-effects/81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|Effet : Domination tonitruante|changé|
|[8adLKKzJy49USYJt.htm](spell-effects/8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|Effet : Chanson de force|changé|
|[8aNZhlkzRTRKlKag.htm](spell-effects/8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Horned)|Effet : Forme de dragon (Or)|changé|
|[8ecGfjmxnBY3WWao.htm](spell-effects/8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|Effet : Multitude de couteaux|changé|
|[8jaYbxnP4Z5wvnQs.htm](spell-effects/8jaYbxnP4Z5wvnQs.htm)|Spell Effect: Transmigrate (Encounter)|Effet: Transmigration (Rencontre)|changé|
|[8XaSpienzVXLmcfp.htm](spell-effects/8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|Effet : Composition fortissimo (Force, +3)|changé|
|[A61eVVVyUuaUl3tz.htm](spell-effects/A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|Effet : Marque céleste|changé|
|[aDOL3OAEWf3ka9oT.htm](spell-effects/aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|Effet : Protection du sang|changé|
|[AJkRUIdYLnt4QOOg.htm](spell-effects/AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|Effet : Tenter le destin|changé|
|[an4yZ6dyIDOFa1wa.htm](spell-effects/an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|Effet : Paroles apaisantes|changé|
|[AnawxScxqUiRuGTm.htm](spell-effects/AnawxScxqUiRuGTm.htm)|Spell Effect: Divine Vessel 9th level (Evil)|Effet : Réceptacle divin Niveau 9 (Mauvais)|changé|
|[b8bfWIICHOsGVzjp.htm](spell-effects/b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|Effet : Forme monstrueuse (Phénix)|changé|
|[Bc2Bwuan3716eAyY.htm](spell-effects/Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|Effet : Source de sérénité|changé|
|[Bd86oAvK3RLN076H.htm](spell-effects/Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|Effet : Forme d'ange (Deva movanique)|changé|
|[BDMEqBsumguTrMXa.htm](spell-effects/BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|Effet : Forme de diable (Érinye)|changé|
|[BKam63zT98iWMJH7.htm](spell-effects/BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|Effet : Composition fortissimo (Défense, +3)|changé|
|[C1GAlZ6Gmmw0QIJN.htm](spell-effects/C1GAlZ6Gmmw0QIJN.htm)|Spell Effect: Hymn of Healing|Effet : Hymne de guérison|changé|
|[Chol7ExtoN2T36mP.htm](spell-effects/Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|Effet : Composition fortissimo (Défense, +2)|changé|
|[ctMxYPGEpstvhW9C.htm](spell-effects/ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|Effet : Sceau d'interdiction|changé|
|[cVVZXNbV0nElVOPZ.htm](spell-effects/cVVZXNbV0nElVOPZ.htm)|Spell Effect: Light|Effet : Lumière|changé|
|[cwetyC5o4dRyFWJZ.htm](spell-effects/cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|Effet : Générosité du nécromant|changé|
|[DdqWMj7cuf4S1bgr.htm](spell-effects/DdqWMj7cuf4S1bgr.htm)|Spell Effect: Frostbite|Effet : Morsure du froid|changé|
|[deG1dtfuQph03Kkg.htm](spell-effects/deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|Effet : Gourdin magique|changé|
|[dEsaufFnfYihu5Ex.htm](spell-effects/dEsaufFnfYihu5Ex.htm)|Spell Effect: Discern Secrets (Sense Motive)|Effet : Discerner les secrets (Deviner les intentions)|changé|
|[dIftJU6Ki2QSLCOD.htm](spell-effects/dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel (9th level)|Effet : Réceptacle divin Niveau 9|changé|
|[DLwTvjjnqs2sNGuG.htm](spell-effects/DLwTvjjnqs2sNGuG.htm)|Spell Effect: Rallying Anthem|Effet : Hymne de ralliement|changé|
|[DrNpuMj14wVj4bWF.htm](spell-effects/DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Fortune)|Effet : Forme de dragon (Cuivre)|changé|
|[dU4viL9kh554TKeB.htm](spell-effects/dU4viL9kh554TKeB.htm)|Spell Effect: Community Repair|Effet : Réparation communautaire|changé|
|[dWbg2gACxMkSnZag.htm](spell-effects/dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|Effet : Champs de protection|changé|
|[dXq7z633ve4E0nlX.htm](spell-effects/dXq7z633ve4E0nlX.htm)|Spell Effect: Regenerate|Effet : Régénération|changé|
|[Eik8Fj8nGo2GLcbn.htm](spell-effects/Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|Effet : Forme monstrueuse (Serpent de mer)|changé|
|[EScdpppYsf9KhG4D.htm](spell-effects/EScdpppYsf9KhG4D.htm)|Spell Effect: Ghostly Weapon|Effet : Arme fantomatique|changé|
|[ETgzIIv3M2zvclAR.htm](spell-effects/ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Conspirator)|Effet : Forme de dragon (Bleu)|changé|
|[EUxTav62IXTz5CxW.htm](spell-effects/EUxTav62IXTz5CxW.htm)|Spell Effect: Nature Incarnate (Kaiju)|Effet : Incarnation de la nature (Kaiju)|changé|
|[evK8JR3j2iWGWaug.htm](spell-effects/evK8JR3j2iWGWaug.htm)|Spell Effect: Divine Vessel (Evil)|Effet : Réceptacle divin (Mauvais)|changé|
|[F10ofwC0k1ELIaV4.htm](spell-effects/F10ofwC0k1ELIaV4.htm)|Spell Effect: Impeccable Flow (Critical Failure Effect)|Effet : Flux impeccable (Effet d'échec critique)|changé|
|[fCIT9YgGUwIc3Z9G.htm](spell-effects/fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|Effet : Attirer la foudre|changé|
|[Fjnm1l59KH5YJ7G9.htm](spell-effects/Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|Effet : Composition fortissimo (Force, +2)|changé|
|[fKeZDm8kpDFK5HWp.htm](spell-effects/fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|Effet : Forme de diable (Sarglagon)|changé|
|[FqG4zXjSoxq9qTlf.htm](spell-effects/FqG4zXjSoxq9qTlf.htm)|Spell Effect: Transmigrate (Skill)|Effet: Transmigration (Compétence)|changé|
|[FR4ucNi2ceHZdrpB.htm](spell-effects/FR4ucNi2ceHZdrpB.htm)|Spell Effect: Warding Aggression (+1)|Effet : Agression protectrice (+1)|changé|
|[GDzn5DToE62ZOTrP.htm](spell-effects/GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel|Effet : Réceptacle divin|changé|
|[GhGoZdAZtzZTYCzj.htm](spell-effects/GhGoZdAZtzZTYCzj.htm)|Spell Effect: Animal Feature (Jaws)|Effet : Trait animal (Mâchoires)|changé|
|[hdOb5Iu6Zd3pHoGI.htm](spell-effects/hdOb5Iu6Zd3pHoGI.htm)|Spell Effect: Discern Secrets (Recall Knowledge)|Effet : Discerner les secrets (Se souvenir)|changé|
|[HEbbxKtBzsLhFead.htm](spell-effects/HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|Effet : Forme de diable (Osyluth)|changé|
|[hnfQyf05IIa7WPBB.htm](spell-effects/hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|Effet : Forme de démon (Nabasu)|changé|
|[HtaDbgTIzdiTiKLX.htm](spell-effects/HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|Effet : À trois temps|changé|
|[ib58LaffEUIypuzL.htm](spell-effects/ib58LaffEUIypuzL.htm)|Spell Effect: Evolution Surge (Huge)|Effet : Flux d'évolution (Très grand)|changé|
|[iJ7TVW5tDnZG9DG8.htm](spell-effects/iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|Effet : Avantage du compétiteur|changé|
|[IjoYgBgEYSTZKmab.htm](spell-effects/IjoYgBgEYSTZKmab.htm)|Spell Effect: Healer's Blessing|Effet : Bénédiction du guérisseur|changé|
|[JhihziXQuoteftdd.htm](spell-effects/JhihziXQuoteftdd.htm)|Spell Effect: Lay on Hands (Vs. Undead)|Effet : Imposition des mains (contre les morts-vivants)|changé|
|[jPZXZjetdauqYuEH.htm](spell-effects/jPZXZjetdauqYuEH.htm)|Aura: Angelic Halo|Aura : Halo angélique|changé|
|[JqrTrvwV7pYStMXz.htm](spell-effects/JqrTrvwV7pYStMXz.htm)|Spell Effect: Levitate|Effet : Lévitation|changé|
|[jtW3VfI5Kktuy3GH.htm](spell-effects/jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Empyreal)|Effet : Forme de dragon (Bronze)|changé|
|[KMF2t3qqzyFP0rxL.htm](spell-effects/KMF2t3qqzyFP0rxL.htm)|Spell Effect: Divine Vessel (Good)|Effet : Réceptacle divin (Bon)|changé|
|[kZ39XWJA3RBDTnqG.htm](spell-effects/kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|Effet : Composition fortissimo (Courage, +2)|changé|
|[lIl0yYdS9zojOZhe.htm](spell-effects/lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|Effet : Forme génératrice de vie|changé|
|[lTL5VwNrZ5xiitGV.htm](spell-effects/lTL5VwNrZ5xiitGV.htm)|Spell Effect: Nudge the Odds|Effet : Renforcer les chances|changé|
|[LXf1Cqi1zyo4DaLv.htm](spell-effects/LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|Effet : Rétrécissement|changé|
|[lywUJljQy2SxRZQt.htm](spell-effects/lywUJljQy2SxRZQt.htm)|Spell Effect: Nature Incarnate (Green Man)|Effet : Incarnation de la nature (Homme vert)|changé|
|[m1tQTBrolf7uZBW0.htm](spell-effects/m1tQTBrolf7uZBW0.htm)|Spell Effect: Discern Secrets (Seek)|Effet : Discerner les secrets (Chercher)|changé|
|[MjtPtndJx31q2N9R.htm](spell-effects/MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|Effet : Toucher amplificateur|changé|
|[mmJNE57hC7G3SPae.htm](spell-effects/mmJNE57hC7G3SPae.htm)|Spell Effect: Silence|Effet : Silence|changé|
|[n6NK7wqhTxWr3ij8.htm](spell-effects/n6NK7wqhTxWr3ij8.htm)|Spell Effect: Warding Aggression (+2)|Effet : Agression protectrice (+2)|changé|
|[nemThuhp3praALY6.htm](spell-effects/nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|Effet : Conviction zélée|changé|
|[nWEx5kpkE8YlBZvy.htm](spell-effects/nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Mirage)|Effet : Forme de dragon (Vert)|changé|
|[oaRt210JV4GZIHmJ.htm](spell-effects/oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|Effet : Toucher rajeunissant|changé|
|[OeCn76SB92GPOZwr.htm](spell-effects/OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Diabolic)|Effet : Forme de dragon (Airain)|changé|
|[OxJEUhim6xzsHIyi.htm](spell-effects/OxJEUhim6xzsHIyi.htm)|Spell Effect: Divine Vessel 9th level (Good)|Effet : Réceptacle divin Niveau 9 (Bon)|changé|
|[PANUWN5xXC20WBg2.htm](spell-effects/PANUWN5xXC20WBg2.htm)|Spell Effect: False Vitality|Effet : Fausse vitalité|changé|
|[Pfllo68qdQjC4Qv6.htm](spell-effects/Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|Effet : Bouclier prismatique|changé|
|[qlz0sJIvqc0FdUdr.htm](spell-effects/qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|Effet : Arme améliorée|changé|
|[qQLHPbUFASKFky1W.htm](spell-effects/qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|Effet : Hyperacuité|changé|
|[Qr5rgoZvI4KmFY0N.htm](spell-effects/Qr5rgoZvI4KmFY0N.htm)|Spell Effect: Calm|Effet : Apaisement|changé|
|[R3j6song8sYLY5vG.htm](spell-effects/R3j6song8sYLY5vG.htm)|Spell Effect: Community Repair (Critical Success)|Effet : Réparation communautaire (Succès critique)|changé|
|[RawLEPwyT5CtCZ4D.htm](spell-effects/RawLEPwyT5CtCZ4D.htm)|Spell Effect: Protection|Effet : Protection|changé|
|[rEsgDhunQ5Yx8KZx.htm](spell-effects/rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Cave Worm)|Effet : Forme monstrueuse (Ver pourpre)|changé|
|[RfCEHpMoEAZvB9IZ.htm](spell-effects/RfCEHpMoEAZvB9IZ.htm)|Aura: Bless|Aura : Bénédiction|changé|
|[rHXOZAFBdRXIlxt5.htm](spell-effects/rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Adamantine)|Effet : Forme de dragon (Noir)|changé|
|[sccNh8j1PKLHCKh1.htm](spell-effects/sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|Effet : Forme d'ange (Choral)|changé|
|[ScF0ECWnfXMHYLDL.htm](spell-effects/ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|Effet : Forme de daémon (Leukodaémon)|changé|
|[sXe7cPazOJbX41GU.htm](spell-effects/sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|Effet : Forme de démon (Hezrou)|changé|
|[tBgwWblDp1xdxN4D.htm](spell-effects/tBgwWblDp1xdxN4D.htm)|Spell Effect: Divine Vessel (Lawful)|Effet : Réceptacle divin (Loyal)|changé|
|[TjGHxli0edXI6rAg.htm](spell-effects/TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|Effet : Joie malsaine (Succès)|changé|
|[u0AznkjTZAVnCMNp.htm](spell-effects/u0AznkjTZAVnCMNp.htm)|Spell Effect: Evolution Surge (Fly)|Effet : Flux d'évolution (Vol)|changé|
|[U2eD42cGwdvQMdN0.htm](spell-effects/U2eD42cGwdvQMdN0.htm)|Spell Effect: Fiery Body (9th Level)|Effet : Corps enflammé (Niveau 9)|changé|
|[UFfItbPq9cVq3LNa.htm](spell-effects/UFfItbPq9cVq3LNa.htm)|Spell Effect: Vitrifying Blast|Effet : Déflagration vitrifiante|changé|
|[UH2sT6eW5e31Xytd.htm](spell-effects/UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|Effet : Défi de dévouement|changé|
|[uIMaMzd6pcKmMNPJ.htm](spell-effects/uIMaMzd6pcKmMNPJ.htm)|Spell Effect: Fiery Body|Effet : Corps enflammé|changé|
|[Uj9VFXoVMH0mTTdt.htm](spell-effects/Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|Effet : Vision des organes|changé|
|[UtIOWubq7akdHMOh.htm](spell-effects/UtIOWubq7akdHMOh.htm)|Spell Effect: Augment Summoning|Effet : Convocation fortifiée|changé|
|[UTLp7omqsiC36bso.htm](spell-effects/UTLp7omqsiC36bso.htm)|Spell Effect: Bane|Effet : Imprécation|changé|
|[UVrEe0nukiSmiwfF.htm](spell-effects/UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|Effet : Renforcer l'eidolon|changé|
|[V4a9pZHNUlddAwTA.htm](spell-effects/V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Omen)|Effet : Forme de dragon (Rouge)|changé|
|[VFereWC1agrwgzPL.htm](spell-effects/VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|Effet : Composition fortissimo (Courage, +3)|changé|
|[VJpRUgSDtAO2TSRR.htm](spell-effects/VJpRUgSDtAO2TSRR.htm)|Spell Effect: Glimpse Weakness|Effet : Aperçu des faiblesses|changé|
|[VVSOzHV6Rz2YNHRl.htm](spell-effects/VVSOzHV6Rz2YNHRl.htm)|Spell Effect: Contagious Idea (Pleasant Thought)|Effet : Idée contagieuse (Pensée plaisante)|changé|
|[WEpgIGFwtRb3ef1x.htm](spell-effects/WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|Effet : Forme d'ange (Balisse)|changé|
|[wKdIf5x7zztiFHpL.htm](spell-effects/wKdIf5x7zztiFHpL.htm)|Spell Effect: Divine Vessel 9th level (Lawful)|Effet : Réceptacle divin Niveau 9 (Loyal)|changé|
|[X1kkbRrh4zJuDGjl.htm](spell-effects/X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|Effet : Forme de démon (Babau)|changé|
|[X7RD0JRxhJV9u2LC.htm](spell-effects/X7RD0JRxhJV9u2LC.htm)|Spell Effect: Infuse Vitality|Effet : Infusion de vitalité|changé|
|[XTgxkQkhlap66e54.htm](spell-effects/XTgxkQkhlap66e54.htm)|Spell Effect: Iron Gut (3rd Level)|Effet : Boyaux de fer (niveau 3)|changé|
|[zbTpf11NtbmizuzR.htm](spell-effects/zbTpf11NtbmizuzR.htm)|Spell Effect: Forge (Critical Failure)|Effet : Forge - Échec critique|changé|
|[zIRnnuj4lARq43DA.htm](spell-effects/zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|Effet : Forme de daémon (Méladaémon)|changé|
|[ZlsuhS9J0S3PuvCO.htm](spell-effects/ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Flicker|Effet : Osciller|changé|
|[zPGVOLz6xhsQN35C.htm](spell-effects/zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|Effet : Compagnon venimeux|changé|
|[zpxIwEjnLUSO1B4z.htm](spell-effects/zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|Effet : Réceptacle magique|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09rtx50laDw68FGT.htm](spell-effects/09rtx50laDw68FGT.htm)|Spell Effect: Earthquake (Shaking Ground)|Effet: Tremblement de terre (Faire trembler le sol)|libre|
|[0bfqYkNaWsdTmtrc.htm](spell-effects/0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|Effet : Compagnon juvénile|libre|
|[0Cyf07wboRp4CmcQ.htm](spell-effects/0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|Effet : Forme de dinosaure (Ankylosaure)|libre|
|[0eP0JRBPwfRyu7gN.htm](spell-effects/0eP0JRBPwfRyu7gN.htm)|Spell Effect: Phoenix Ward|Effet : Protection du phénix|libre|
|[0gv9D5RlrF5cKA3I.htm](spell-effects/0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|Effet : Adaptation de soi (Vision dans le noir)|libre|
|[0lYbjDI2N3xVl24E.htm](spell-effects/0lYbjDI2N3xVl24E.htm)|Spell Effect: Glass Form|Effet : Forme de verre|libre|
|[0o984LjzIFXxeXIF.htm](spell-effects/0o984LjzIFXxeXIF.htm)|Spell Effect: Evolution Surge (Amphibious)|Effet : Flux d'évolution (Amphibie)|libre|
|[0PO5mFRhh9HxGAtv.htm](spell-effects/0PO5mFRhh9HxGAtv.htm)|Spell Effect: Mirror Image|Effet : Image miroir|libre|
|[0q2716S34XL1y9Hh.htm](spell-effects/0q2716S34XL1y9Hh.htm)|Spell Effect: Rapid Adaptation (Underground)|Effet : Adaptation rapide (Souterrain)|libre|
|[0QVufU5o3xIxiHmP.htm](spell-effects/0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|Effet : Forme aérienne (Oiseau)|libre|
|[0R42NyuEZMVALjQs.htm](spell-effects/0R42NyuEZMVALjQs.htm)|Spell Effect: Traveler's Transit (Swim)|Effet : Voyageur en transit (Nage)|libre|
|[14AFzcwkN019dzcl.htm](spell-effects/14AFzcwkN019dzcl.htm)|Spell Effect: Shifting Form (Claws)|Effet : Forme changeant (Griffes)|libre|
|[16QrxIYal7PJFL2W.htm](spell-effects/16QrxIYal7PJFL2W.htm)|Spell Effect: Euphoric Renewal|Effet : Renouveau euphorique|libre|
|[1cBl1gVcpzOqlluC.htm](spell-effects/1cBl1gVcpzOqlluC.htm)|Spell Effect: Flame Dancer|Effet : Danseur enflammé|libre|
|[1gTnFmfLUQnCo3TK.htm](spell-effects/1gTnFmfLUQnCo3TK.htm)|Spell Effect: Dread Ambience (Success)|Effet : Ambiance terrifiante (Succès)|libre|
|[1RsScTvNdGD9zGWe.htm](spell-effects/1RsScTvNdGD9zGWe.htm)|Spell Effect: Fire Shield|Effet : Bouclier de feu|libre|
|[1VuHjj32wge2gPOr.htm](spell-effects/1VuHjj32wge2gPOr.htm)|Spell Effect: Animal Feature (Wings)|Effet : Trait animal (Ailes)|libre|
|[28NvrpZmELvyrHUt.htm](spell-effects/28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|Effet : Gravité variable (Gravité élevée)|libre|
|[2KQSsrzUqAxSXOdd.htm](spell-effects/2KQSsrzUqAxSXOdd.htm)|Spell Effect: Dancing Shield|Effet : Bouclier dansant|libre|
|[2sMXAGZfdqiy10kk.htm](spell-effects/2sMXAGZfdqiy10kk.htm)|Spell Effect: Clinging Ice|Effet : Glace tenace|libre|
|[2Ss5VblfZNHg1HjN.htm](spell-effects/2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|Effet : Forme cosmique (Soleil)|libre|
|[2SWUzp4JuNK5EX0J.htm](spell-effects/2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|Effet : Adaptation de soi (Nage)|libre|
|[3bcUiWwc5KLc4X0T.htm](spell-effects/3bcUiWwc5KLc4X0T.htm)|Aura: Protector's Sphere|Aura : Sphère du protecteur|libre|
|[3hDKcbhn0j6DsRgm.htm](spell-effects/3hDKcbhn0j6DsRgm.htm)|Spell Effect: Touch of the Moon|Effet : Contact lunaire|libre|
|[3HEiYVhqypfc4IsP.htm](spell-effects/3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|Effet : Secret bien gardé|officielle|
|[3Ktyd5F9lOPo4myk.htm](spell-effects/3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|Effet : Déguisement illusoire|libre|
|[3LyOkV25p7wA181H.htm](spell-effects/3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|Effet : Immunité à Assistance surnaturelle|officielle|
|[3qHKBDF7lrHw8jFK.htm](spell-effects/3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Effet : Assistance surnaturelle|officielle|
|[3zdBGENpmaze1bpq.htm](spell-effects/3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|Effet : Forme de vase (Cube gélatineux)|libre|
|[46vCC77mBNBWtmx3.htm](spell-effects/46vCC77mBNBWtmx3.htm)|Spell Effect: Litany of Righteousness|Effet : Litanie de vertu|libre|
|[4dnt1P2SfcePzkrF.htm](spell-effects/4dnt1P2SfcePzkrF.htm)|Spell Effect: Incendiary Ashes (Success)|Effet : Cendres incendiaires (Succès)|libre|
|[4iakL7fDcZ8RT6Tu.htm](spell-effects/4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|Effet : Fondu dans la foule|libre|
|[4ktNx3cVz5GkcGJa.htm](spell-effects/4ktNx3cVz5GkcGJa.htm)|Spell Effect: Untwisting Iron Augmentation|Effet : Amélioration du fer qui ne plie pas|libre|
|[4Lo2qb5PmavSsLNk.htm](spell-effects/4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|Effet : Égide énergétique|libre|
|[4Lt69zSeYElEfDrZ.htm](spell-effects/4Lt69zSeYElEfDrZ.htm)|Spell Effect: Dread Ambience (Critical Failure)|Effet : Ambiance terrifiante (Échec critique)|libre|
|[4vlorajqpFcS5Ozi.htm](spell-effects/4vlorajqpFcS5Ozi.htm)|Spell Effect: Flashy Disappearance|Effet : Disparition spectaculaire|libre|
|[4VOZP2ArmS12nvz8.htm](spell-effects/4VOZP2ArmS12nvz8.htm)|Spell Effect: Draw Ire (Critical Failure)|Effet : Attirer la colère (Échec critique)|libre|
|[4wZaiZJtAA0iyWR5.htm](spell-effects/4wZaiZJtAA0iyWR5.htm)|Spell Effect: Sun's Fury|Effet : Furie du soleil|libre|
|[542Keo6txtq7uvqe.htm](spell-effects/542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|Effet : Forme de dinosaure (Tyrannosaure)|libre|
|[57lnrCzGUcNUBP2O.htm](spell-effects/57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|Effet : Athlétisme poussé|libre|
|[5MI2c9IgxfSeGZQo.htm](spell-effects/5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|Effet : Saut du vent|libre|
|[5p3bKvWsJgo83FS1.htm](spell-effects/5p3bKvWsJgo83FS1.htm)|Aura: Protective Ward|Aura : Champs de protection|libre|
|[5R3ewWLFkgqTvZsc.htm](spell-effects/5R3ewWLFkgqTvZsc.htm)|Spell Effect: Elemental Gift (Fire)|Effet : Don élémentaire (Feu)|libre|
|[5xCheSMgtQhQZm00.htm](spell-effects/5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|Effet : Jardin mortel (Succès critique)|libre|
|[61Hl31nyzt63vvX9.htm](spell-effects/61Hl31nyzt63vvX9.htm)|Spell Effect: Phase Familiar|Effet : Déphasage de familier|libre|
|[6ArAZeZyYSNLI0X5.htm](spell-effects/6ArAZeZyYSNLI0X5.htm)|Spell Effect: Vital Luminance|Effet : Luminosité vitale|libre|
|[6BjslHgY01cNbKp5.htm](spell-effects/6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|Effet : Armure d'os|libre|
|[6Ev2ytJZfr2t33iy.htm](spell-effects/6Ev2ytJZfr2t33iy.htm)|Spell Effect: Evolution Surge (Sight)|Effet : Flux d'évolution (Vision)|libre|
|[6GAztnHuQSwAp1k1.htm](spell-effects/6GAztnHuQSwAp1k1.htm)|Spell Effect: Adaptive Ablation|Effet : Dissonnance adaptative|libre|
|[6IvTWcispcDaw88N.htm](spell-effects/6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|Effet : Forme d'insecte (Fourmi)|libre|
|[6SAUuqvptTeXtyfH.htm](spell-effects/6SAUuqvptTeXtyfH.htm)|Spell Effect: Sage's Curse (Success)|Effet: Malédiction du savant (Succès)|libre|
|[6SG8wVmppv4oXZtx.htm](spell-effects/6SG8wVmppv4oXZtx.htm)|Spell Effect: Mantle of the Magma Heart (Fiery Grasp)|Effet : Manteau du coeur magmatique (Poigne enflammée)|libre|
|[782NyomkDHyfsUn6.htm](spell-effects/782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Effet : Forme d'insecte (Araignée)|libre|
|[7cYUiOONB2lZfSaA.htm](spell-effects/7cYUiOONB2lZfSaA.htm)|Spell Effect: Tremorsense|Effet : Perception des vibrations|libre|
|[7tfF8ifVvOKNud8t.htm](spell-effects/7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|Effet : Forme de vase (Vase grise)|libre|
|[7tYv9lY3ksSUny2h.htm](spell-effects/7tYv9lY3ksSUny2h.htm)|Spell Effect: Gray Shadow|Effet : Ombre grise|libre|
|[7vIUF5zbvHzVcJA0.htm](spell-effects/7vIUF5zbvHzVcJA0.htm)|Spell Effect: Tailwind (8 hours)|Effet : Vent arrière (8 heures)|libre|
|[7wHYSgNG6LaxxlOz.htm](spell-effects/7wHYSgNG6LaxxlOz.htm)|Spell Effect: Thermal Remedy|Effet : Remède thermique|libre|
|[7zy4W2RXQiMEr6cp.htm](spell-effects/7zy4W2RXQiMEr6cp.htm)|Spell Effect: Dragon Claws|Effet : Griffes de dragon|officielle|
|[8eWLR0WCf5258z8X.htm](spell-effects/8eWLR0WCf5258z8X.htm)|Spell Effect: Elemental Form (Earth)|Effet : Forme élémentaire (Terre)|libre|
|[8gqb5FMTaArsKdWB.htm](spell-effects/8gqb5FMTaArsKdWB.htm)|Spell Effect: Prismatic Armor|Effet : Armure prismatique|libre|
|[8GUkKvCeI0xljCOk.htm](spell-effects/8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|Effet : Vol de l'ouragan|libre|
|[8olfnTmWh0GGPDqX.htm](spell-effects/8olfnTmWh0GGPDqX.htm)|Spell Effect: Ki Strike|Effet : Frappe ki|libre|
|[8wCVSzWYcURWewbd.htm](spell-effects/8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|Effet : Malédiction bestiale (Échec)|libre|
|[8y6Ap9xIsnseYYvk.htm](spell-effects/8y6Ap9xIsnseYYvk.htm)|Spell Effect: Tempest Cloak|Effet : Cape tempête|libre|
|[98XT2QUk6wvXIqf7.htm](spell-effects/98XT2QUk6wvXIqf7.htm)|Spell Effect: Death Knell|Effet : Mise à mort|libre|
|[9yzlmYUdvdQshTDF.htm](spell-effects/9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|Effet : Porte-voix|libre|
|[9ZIP6gWSp9OTEu8i.htm](spell-effects/9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|Effet : Bibliothèque de poche|libre|
|[a3uZckqOY9zQWzZ2.htm](spell-effects/a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|Effet : Lire l'ambiance|libre|
|[A48jNUOAmCljx8Ru.htm](spell-effects/A48jNUOAmCljx8Ru.htm)|Spell Effect: Shifting Form (Darkvision)|Effet : Forme changeante (Vision dans le noir)|libre|
|[a5rWrWwuevTzs9Io.htm](spell-effects/a5rWrWwuevTzs9Io.htm)|Spell Effect: Untamed Form|Effet : Forme indomptée|libre|
|[AAypmg9Jz6Ysf02a.htm](spell-effects/AAypmg9Jz6Ysf02a.htm)|Spell Effect: Endure|Effet : Endurer|libre|
|[AF4vQ1xoOiJ1ewH1.htm](spell-effects/AF4vQ1xoOiJ1ewH1.htm)|Spell Effect: Elemental Gift|Effet : Don élémentaire|libre|
|[afJCG4vC5WF5h5IB.htm](spell-effects/afJCG4vC5WF5h5IB.htm)|Spell Effect: Clawsong (Damage Increase D8)|Effet : Chant de la griffe (Augmentation des dégâts d8)|libre|
|[alyNtkHLNnt98Ewz.htm](spell-effects/alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|Effet : Contact accélérant|libre|
|[AM49w68oKykc2fHI.htm](spell-effects/AM49w68oKykc2fHI.htm)|Spell Effect: Rapid Adaptation (Plains)|Effet : Adaptation rapide (Plaines)|libre|
|[AmsVO5Q6078mEvNt.htm](spell-effects/AmsVO5Q6078mEvNt.htm)|Spell Effect: Ill Omen|Effet: Mauvais présage|libre|
|[amTa9jSml9ioKduN.htm](spell-effects/amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|Effet : Forme d'insecte (Coléoptère)|libre|
|[AnCRD7kDcG0DDGKn.htm](spell-effects/AnCRD7kDcG0DDGKn.htm)|Spell Effect: Fungal Infestation (Failure)|Effet : Infestation fongique(Échec)|libre|
|[b5OyBdc0bolgWZZT.htm](spell-effects/b5OyBdc0bolgWZZT.htm)|Spell Effect: Tempest Form (Air)|Effet : Forme tempêtueuse (Air)|libre|
|[b8JCq6n1STl3Wkwy.htm](spell-effects/b8JCq6n1STl3Wkwy.htm)|Spell Effect: Illusory Shroud|Effet : Voile illusoire|libre|
|[bBD7HFzBPlSxYrtW.htm](spell-effects/bBD7HFzBPlSxYrtW.htm)|Spell Effect: Wish-Twisted Form (Failure)|Effet : Forme déformée par un souhait (échec)|libre|
|[beReeFroAx24hj83.htm](spell-effects/beReeFroAx24hj83.htm)|Spell Effect: Courageous Anthem|Effet : Hymne de courage|libre|
|[BfaFe1cI9IkpvmmY.htm](spell-effects/BfaFe1cI9IkpvmmY.htm)|Spell Effect: Countless Eyes|Effet : Yeux innombrables|libre|
|[BGv44XBGtD4zOJBd.htm](spell-effects/BGv44XBGtD4zOJBd.htm)|Spell Effect: Eat Fire|Effet : Manger le feu|libre|
|[blBXnWb1Y8q8YYMh.htm](spell-effects/blBXnWb1Y8q8YYMh.htm)|Spell Effect: Primal Summons (Fire)|Effet : Convocations primordiales (Feu)|libre|
|[bOjuEX3qj7XAOoDF.htm](spell-effects/bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Effet : Forme d'insecte (Scorpion)|libre|
|[BsGZdgiEElNlgZVv.htm](spell-effects/BsGZdgiEElNlgZVv.htm)|Spell Effect: Draw Ire (Failure)|Effet : Attirer la colère (Échec)|libre|
|[BT1ofB6RvRocQOWO.htm](spell-effects/BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|Effet : Forme animale (Taureau)|libre|
|[buXx8Azr4BYWPtFg.htm](spell-effects/buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|Effet : Vendetta du sang (Échec)|libre|
|[byXkHIKFwuKrZ55M.htm](spell-effects/byXkHIKFwuKrZ55M.htm)|Spell Effect: Shifting Form (Scent)|Effet : Forme changeante (Odorat)|libre|
|[bzpW0owbAV3UX7UA.htm](spell-effects/bzpW0owbAV3UX7UA.htm)|Spell Effect: Monstrosity Form (Kaiju)|Effet: Forme monstrueuse (Kaiju)|libre|
|[C3RdbEQTvawqKAhw.htm](spell-effects/C3RdbEQTvawqKAhw.htm)|Spell Effect: Tempest Form (Water)|Effet : Forme tempêtueuse (Eau)|libre|
|[c4cIfS2974nUJDPt.htm](spell-effects/c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|Effet : Forme de fée (dryade)|libre|
|[CDNKDV3UsAp95D1m.htm](spell-effects/CDNKDV3UsAp95D1m.htm)|Spell Effect: Serrate|Effet : Dentelé|libre|
|[ceEA7nBGNmoR8Sjj.htm](spell-effects/ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|Effet : Litanie d'égoïsme|libre|
|[con2Hzt47JjpuUej.htm](spell-effects/con2Hzt47JjpuUej.htm)|Spell Effect: Resist Energy|Effet : Résistance à l'énergie|libre|
|[cSoL5aMy3PCzM4Yv.htm](spell-effects/cSoL5aMy3PCzM4Yv.htm)|Spell Effect: Return the Favor|Effet : Rendre la faveur|libre|
|[cTBYHfiXDOA09G4b.htm](spell-effects/cTBYHfiXDOA09G4b.htm)|Spell Effect: Traveler's Transit (Fly)|Effet : Voyageur en transit (Vol)|libre|
|[CTdEsMIwVYqqkH50.htm](spell-effects/CTdEsMIwVYqqkH50.htm)|Spell Effect: Litany of Depravity|Effet : Litanie de dépravation|libre|
|[CWC2fPmlgixoIKy5.htm](spell-effects/CWC2fPmlgixoIKy5.htm)|Spell Effect: Clawsong (Damage Increase D6)|Effet : Chant de la griffe (Augmentation des dégâts d6)|libre|
|[czteoX2cggQzfkK9.htm](spell-effects/czteoX2cggQzfkK9.htm)|Spell Effect: Evolution Surge (Climb)|Effet : Flux d'évolution (Escalade)|libre|
|[D0Qj5tC1hGUjzQc4.htm](spell-effects/D0Qj5tC1hGUjzQc4.htm)|Spell Effect: Elemental Motion (Water)|Effet : Mobilité élémentaire (Eau)|libre|
|[DAAtzP2QYmCzSNXk.htm](spell-effects/DAAtzP2QYmCzSNXk.htm)|Spell Effect: Sage's Curse (Failure)|Effet: Malédiction du savant (Échec)|libre|
|[DBaMtFHRPEg1JeLs.htm](spell-effects/DBaMtFHRPEg1JeLs.htm)|Spell Effect: Hidden Mind|Effet : Esprit dissimulé|libre|
|[dCQCzapIk53xmDo5.htm](spell-effects/dCQCzapIk53xmDo5.htm)|Spell Effect: Animal Feature (Cat Eyes)|Effet : Trait animal (Yeux de chats)|libre|
|[DENMzySYANjUBs4O.htm](spell-effects/DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Effet : Forme d'insecte (Mille-pattes)|libre|
|[DHYWmMGmKOpRSqza.htm](spell-effects/DHYWmMGmKOpRSqza.htm)|Spell Effect: Chromatic Armor|Effet : Armure chromatique|libre|
|[DliizYpHcmBG130w.htm](spell-effects/DliizYpHcmBG130w.htm)|Spell Effect: Elemental Form (Air)|Effet : Forme élémentaire (Air)|libre|
|[DwM5qcFp4JgKhXrY.htm](spell-effects/DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|Effet : Forme de fée (Licorne)|libre|
|[ei9MIyZbIaP4AZmh.htm](spell-effects/ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|Effet : Feu follet enflammé|libre|
|[EKdqKCuyWSkpXpyJ.htm](spell-effects/EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|Effet : Forme de vase (Poudding noir)|libre|
|[ElkXovNrHB0Doi6O.htm](spell-effects/ElkXovNrHB0Doi6O.htm)|Spell Effect: Haste|Effet : Rapidité|libre|
|[eotqxEWIgaK7nMpD.htm](spell-effects/eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|Effet : Émousser la Lame finale (Succès critique)|libre|
|[eRRiss7y7TsneiEu.htm](spell-effects/eRRiss7y7TsneiEu.htm)|Spell Effect: Cloak of Light|Effet : Cape de lumière|libre|
|[F1APSdrw5uv672hf.htm](spell-effects/F1APSdrw5uv672hf.htm)|Spell Effect: Battlefield Persistence|Effet : Persévérance sur le champ de bataille|libre|
|[F4DTpDXNu5IliyhJ.htm](spell-effects/F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|Effet : Forme animale (Cerf)|libre|
|[FD9Ce5pqcZYstcMI.htm](spell-effects/FD9Ce5pqcZYstcMI.htm)|Spell Effect: Blessing of Defiance|Effet : Bénédiction du défi|libre|
|[fEhCbATDNlt6c1Ug.htm](spell-effects/fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|Effet : Extraction du poison|libre|
|[fGK6zJ7mWz9D5QYo.htm](spell-effects/fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation (Aquatic Base Swim Speed)|Effet : Adaptation rapide (Aquatique : Vitesse de Base de nage)|libre|
|[fIloZhZVH1xTnX4B.htm](spell-effects/fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|Effet : Forme de plante (Grand tertre)|libre|
|[fLOQMycP5tmXgPv9.htm](spell-effects/fLOQMycP5tmXgPv9.htm)|Spell Effect: Elemental Gift (Earth)|Effet : Don élémentaire (Terre)|libre|
|[fpGDAz2v5PG0zUSl.htm](spell-effects/fpGDAz2v5PG0zUSl.htm)|Spell Effect: Sure Strike|Effet : Coup assuré|libre|
|[FT5Tt2DKBRutDqbV.htm](spell-effects/FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience (Critical Success)|Effet : Ambiance terrifiante (Succès critique)|libre|
|[fvIlSZPwojixVvyZ.htm](spell-effects/fvIlSZPwojixVvyZ.htm)|Spell Effect: Lucky Number|Effet : Nombre porte bonheur|libre|
|[fwaAe71qfnK7SiOB.htm](spell-effects/fwaAe71qfnK7SiOB.htm)|Spell Effect: Primal Summons (Air)|Effet : Convocations primordiales (Air)|libre|
|[FxB7DS6CFPhd2hBr.htm](spell-effects/FxB7DS6CFPhd2hBr.htm)|Spell Effect: Magic's Vessel (Resistance)|Effet : Réceptacle magique (résistance)|libre|
|[g4E9l4uA62LcRBJS.htm](spell-effects/g4E9l4uA62LcRBJS.htm)|Spell Effect: Clawsong (Versatile Piercing)|Effet : Chant de la griffe (Polyvalent P)|libre|
|[GhNVAYtoF5hK3AlD.htm](spell-effects/GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of Corruption|Effet : Toucher de corruption|libre|
|[gKGErrsS1WoAyWub.htm](spell-effects/gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|Effet : Forme d'aberration (Gogiteth)|libre|
|[GlggmEqkGVj1noOD.htm](spell-effects/GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|Effet : Tempête en bouteille|libre|
|[GnWkI3T3LYRlm3X8.htm](spell-effects/GnWkI3T3LYRlm3X8.htm)|Spell Effect: Runic Weapon|Effet : Arme runique|libre|
|[gQnDKDeBTtjwOWAk.htm](spell-effects/gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|Effet : Forme animale (Ours)|libre|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Effet : Bénédiction|libre|
|[gX8O0ArQXbEVDUbW.htm](spell-effects/gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|Effet : Étreinte de la fosse|libre|
|[h0CKGrgjGNSg21BW.htm](spell-effects/h0CKGrgjGNSg21BW.htm)|Spell Effect: Boost Eidolon|Effet : Booster l'eidolon|libre|
|[h2JzNunzO8hXiNV3.htm](spell-effects/h2JzNunzO8hXiNV3.htm)|Spell Effect: Lifelink Surge|Effet : Afflux du lien vital|libre|
|[HDKJAUXMbtxnBdgR.htm](spell-effects/HDKJAUXMbtxnBdgR.htm)|Spell Effect: Tempest Form (Mist)|Effet : Forme tempêtueuse (Brume)|libre|
|[HDT5oiQXXnRdDIKR.htm](spell-effects/HDT5oiQXXnRdDIKR.htm)|Spell Effect: Sweetest Solstice|Effet : Solstice plus sucré|libre|
|[heAj9paC8ZRh7QEj.htm](spell-effects/heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|Effet : Forme de fée (Bonnet rouge)|libre|
|[HF1r1psnITHD52B9.htm](spell-effects/HF1r1psnITHD52B9.htm)|Aura: Spiral of Horrors|Aura : Aura effroyable|libre|
|[hfcFc8cV5wF2evWP.htm](spell-effects/hfcFc8cV5wF2evWP.htm)|Spell Effect: Nature Incarnate|Effet : Incarnation de la nature|libre|
|[hkLhZsH3T6jc9S1y.htm](spell-effects/hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|Effet : Voile de rêves|libre|
|[HoCUCi2jL1OLfXWR.htm](spell-effects/HoCUCi2jL1OLfXWR.htm)|Spell Effect: Unblinking Flame Aura|Effet : Aura de la flamme qui ne vacille pas|libre|
|[HoOujAdQWCN4E6sQ.htm](spell-effects/HoOujAdQWCN4E6sQ.htm)|Spell Effect: Oaken Resilience|Effet : Résilience du chêne|libre|
|[hXtK08bTnDBSzGTJ.htm](spell-effects/hXtK08bTnDBSzGTJ.htm)|Spell Effect: Iron Gut|Effet : Boyaux de fer|libre|
|[hya8NfBB1GJofTXm.htm](spell-effects/hya8NfBB1GJofTXm.htm)|Spell Effect: Unblinking Flame Ignition|Effet : Allumage de la flamme qui ne vacille pas|libre|
|[I4PsUAaYSUJ8pwKC.htm](spell-effects/I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|Effet : Rayon de givre|libre|
|[i9YITDcrq1nKjV5l.htm](spell-effects/i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody|Effet : Mélodie contagieuse|libre|
|[IcQMLYWYDMZbq3XE.htm](spell-effects/IcQMLYWYDMZbq3XE.htm)|Spell Effect: Inscrutable Mask|Effet : Masque insondable|libre|
|[IhorZCrhO4dCq6n3.htm](spell-effects/IhorZCrhO4dCq6n3.htm)|Spell Effect: Heroes' Feast|Effet : Festin des héros|libre|
|[ihv1azg80N3kj7Vo.htm](spell-effects/ihv1azg80N3kj7Vo.htm)|Spell Effect: Lift Nature's Caul (Bonus)|Effet : Vraie nature (bonus)|libre|
|[iiV80Kexj6vPmzqU.htm](spell-effects/iiV80Kexj6vPmzqU.htm)|Spell Effect: Rapid Adaptation (Arctic)|Effet : Adaptation rapide (Arctique)|libre|
|[iN6shVYuzvQ4A95i.htm](spell-effects/iN6shVYuzvQ4A95i.htm)|Spell Effect: Shifting Form|Effet : Forme changeante|libre|
|[inNfTmtWpsxeGBI9.htm](spell-effects/inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|Effet : Vision dans le noir (24 heures)|libre|
|[iOKhr2El8R6cz6YI.htm](spell-effects/iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Effet : Forme de dinosaure (Tricératops)|libre|
|[iqtjMVl6rGQhX2k8.htm](spell-effects/iqtjMVl6rGQhX2k8.htm)|Spell Effect: Elemental Motion (Air)|Effet : Mobilité élémentaire(Air)|libre|
|[itmiGioGNuVvt4QE.htm](spell-effects/itmiGioGNuVvt4QE.htm)|Spell Effect: Rapid Adaptation (Aquatic Speed Bonus)|Effet : Adaptation rapide (Aquatique: bonus Vitesse de nage)|libre|
|[IWD5RehCxZVfgrX9.htm](spell-effects/IWD5RehCxZVfgrX9.htm)|Spell Effect: Elephant Form|Effet : Forme d'éléphant|libre|
|[IXS15IQXYCZ8vsmX.htm](spell-effects/IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|Effet : Vision dans le noir|libre|
|[IYvzqymG4xOhqFir.htm](spell-effects/IYvzqymG4xOhqFir.htm)|Spell Effect: Rousing Splash|Effet : Aspersion réveillante|libre|
|[iZYjxY0qYvg5yPP3.htm](spell-effects/iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|Effet : Ailes d'ange|libre|
|[j2LhQ7kEQhq3J3zZ.htm](spell-effects/j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|Effet : Forme animale (Grenouille)|libre|
|[J60rN48XzBGHmR6m.htm](spell-effects/J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|Effet : Incarnation élémentaire (Air)|libre|
|[j6po934p4jcUVC6l.htm](spell-effects/j6po934p4jcUVC6l.htm)|Spell Effect: Shifting Form (Speed)|Effet : Forme changeante (Vitesse)|libre|
|[j9l4LDnAwg9xzYsy.htm](spell-effects/j9l4LDnAwg9xzYsy.htm)|Spell Effect: Life Connection|Effet : Connexion vitale|libre|
|[Jemq5UknGdMO7b73.htm](spell-effects/Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Effet : Bouclier|libre|
|[JHpYudY14g0H4VWU.htm](spell-effects/JHpYudY14g0H4VWU.htm)|Spell Effect: Mountain Resilience|Effet : Résilience du rocher|libre|
|[jj0P4eGVpmdwZjlA.htm](spell-effects/jj0P4eGVpmdwZjlA.htm)|Spell Effect: Instant Armor|Effet : Armure instantanée|libre|
|[jp88SCE3VCRAyE6x.htm](spell-effects/jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|Effet : Incarnation élémentaire (Terre)|libre|
|[JrNHFNxJayevlv2G.htm](spell-effects/JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|Effet : Forme de plante (Attrape-mouches)|libre|
|[jtMo6qS47hPx6EbR.htm](spell-effects/jtMo6qS47hPx6EbR.htm)|Spell Effect: Rapid Adaptation (Forest)|Effet : Adaptation rapide (Forêt)|libre|
|[jvwKRHtOiPAm4uAP.htm](spell-effects/jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|Effet : Forme aérienne (Chauve-souris)|libre|
|[jy4edd6pvJvJgOSP.htm](spell-effects/jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings (Own Speed)|Effet : Ailes de dragon (Sa Vitesse)|libre|
|[KcBqo33ekJHxZLHo.htm](spell-effects/KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|Effet : Forme de fée (Naïade)|libre|
|[KkDRRDuycXwKPa6n.htm](spell-effects/KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|Effet : Forme de dinosaure (Brontosaure)|libre|
|[kMoOWWBqDYmPcYyS.htm](spell-effects/kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|Effet : Baignade de sang|libre|
|[KtAJN4Qr2poTL6BB.htm](spell-effects/KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|Effet : Malédiction bestiale (Échec critique)|libre|
|[kxMBdANwCcF841uA.htm](spell-effects/kxMBdANwCcF841uA.htm)|Spell Effect: Elemental Form (Water)|Effet : Forme élémentaire (Eau)|libre|
|[kz3mlFwb9tV9bFwu.htm](spell-effects/kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|Effet : Forme animale (Serpent)|libre|
|[l8HkOKfiUqd3BUwT.htm](spell-effects/l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|Effet : Forme ancestrale|libre|
|[l9HRQggofFGIxEse.htm](spell-effects/l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|Effet : Héroïsme|libre|
|[lEU3DH1tGjAigpEt.htm](spell-effects/lEU3DH1tGjAigpEt.htm)|Spell Effect: Energy Absorption|Effet : Absorption d'énergie|libre|
|[lGU4GIF2GUn21zFa.htm](spell-effects/lGU4GIF2GUn21zFa.htm)|Spell Effect: Open the Wall of Ghosts (Critical Success)|Effet : Ouvrir le mur des tanômes (Succès critique)|libre|
|[LHREWCGPkWsc4GGJ.htm](spell-effects/LHREWCGPkWsc4GGJ.htm)|Spell Effect: Faerie Dust (Failure)|Effet : Poussière féerique (Échec)|libre|
|[LldX5hnNhKzGtOS0.htm](spell-effects/LldX5hnNhKzGtOS0.htm)|Spell Effect: Elemental Absorption|Effet : Absorption élémentaire|libre|
|[llrOM8rPP9nxIuEN.htm](spell-effects/llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|Effet : Forme d'insecte (Mante)|libre|
|[lmAwCy7isFvLYdGd.htm](spell-effects/lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|Effet : Incarnation élémentaire (Feu)|libre|
|[LMXxICrByo7XZ3Q3.htm](spell-effects/LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|Effet : Déluge|libre|
|[LMzFBnOEPzDGzHg4.htm](spell-effects/LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|Effet : Anatomie étrange|libre|
|[lRfiYmsoQMJZ81NQ.htm](spell-effects/lRfiYmsoQMJZ81NQ.htm)|Spell Effect: Element Embodied (Metal)|Effet : Incarnation élémentaire (Métal)|libre|
|[LT5AV9vSN3T9x3J9.htm](spell-effects/LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|Effet : Corps corrosif|libre|
|[lyLMiauxIVUM3oF1.htm](spell-effects/lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|Effet : Imposition des mains|libre|
|[m6x0IvoeX0a0bZiQ.htm](spell-effects/m6x0IvoeX0a0bZiQ.htm)|Spell Effect: Unbreaking Wave Vapor|Effet : Vapeur de la vague inexorable|libre|
|[mAofA4oy3cRdT71K.htm](spell-effects/mAofA4oy3cRdT71K.htm)|Spell Effect: Penumbral Disguise|Effet : Déguisement pénombral|libre|
|[mCb9mWAmgWPQrkTY.htm](spell-effects/mCb9mWAmgWPQrkTY.htm)|Spell Effect: Barkskin (Arboreal's Revenge)|Effet : Résilience du chêne (Vengeance de l'arboréen)|libre|
|[Me470HI6inX3Bovh.htm](spell-effects/Me470HI6inX3Bovh.htm)|Spell Effect: Guided Introspection|Effet : Introspection guidée|libre|
|[mhklZ6wjfty0bF44.htm](spell-effects/mhklZ6wjfty0bF44.htm)|Spell Effect: Speaking Sky|Effet : Ciel parlant|libre|
|[MJSoRFfEdM4j5mNG.htm](spell-effects/MJSoRFfEdM4j5mNG.htm)|Spell Effect: Sweet Dream (Glamour)|Effet : Doux rêve (Glamour)|libre|
|[Mp7252yAsSA8lCEA.htm](spell-effects/Mp7252yAsSA8lCEA.htm)|Spell Effect: One with the Land|Effet : Uni à la terre|libre|
|[MqZ6FScbfGtXB8tt.htm](spell-effects/MqZ6FScbfGtXB8tt.htm)|Spell Effect: Runic Body|Effet : Corps runique|libre|
|[mr6mlkUMeStdChxi.htm](spell-effects/mr6mlkUMeStdChxi.htm)|Spell Effect: Animal Feature (Owl Eyes)|Effet : Trait animal (Yeux de hibou)|libre|
|[mrSulUdNbwzGSwfu.htm](spell-effects/mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaw|Effet : Mâchoires gloutonnes|libre|
|[MuRBCiZn5IKeaoxi.htm](spell-effects/MuRBCiZn5IKeaoxi.htm)|Spell Effect: Fly|Effet : Vol|libre|
|[myWvjlGLvbzkSCNO.htm](spell-effects/myWvjlGLvbzkSCNO.htm)|Effect: Cinder Gaze|Regard de cendre|libre|
|[mzDgsuuo5wCgqyxR.htm](spell-effects/mzDgsuuo5wCgqyxR.htm)|Spell Effect: Mirecloak|Effet : Cape-miroir|libre|
|[N1EM3jRyT8PCG1Py.htm](spell-effects/N1EM3jRyT8PCG1Py.htm)|Spell Effect: Traveler's Transit (Climb)|Effet : Voyageur en transit (Escalade)|libre|
|[nbW4udOUTrCGL3Gf.htm](spell-effects/nbW4udOUTrCGL3Gf.htm)|Spell Effect: Shifting Form (Climb Speed)|Effet : Forme changeante (Escalade)|libre|
|[ndj0TpLxyzbyzcm4.htm](spell-effects/ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|Effet : Nécroser (Jambes)|libre|
|[nFOJ53IkO5khO4Rr.htm](spell-effects/nFOJ53IkO5khO4Rr.htm)|Spell Effect: Entwined Roots|Effet : Racines entremêlées|libre|
|[nHXKK4pRXAzrLdEP.htm](spell-effects/nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take its Course (Affliction, Help)|Effet : Suivre son cours (Afflicton, aide)|libre|
|[nIryhRgeiacQw1Em.htm](spell-effects/nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|Effet : Bourgeons apaisants|libre|
|[nkk4O5fyzrC0057i.htm](spell-effects/nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|Effet : Apaiser|libre|
|[nLige83aiMBh0ylb.htm](spell-effects/nLige83aiMBh0ylb.htm)|Spell Effect: Musical Accompaniment|Effet : Accompagnement musical|libre|
|[npFFTAxN44WWrGnM.htm](spell-effects/npFFTAxN44WWrGnM.htm)|Spell Effect: Wash Your Luck|Effet : Laver votre chance|libre|
|[NQZ88IoKeMBsfjp7.htm](spell-effects/NQZ88IoKeMBsfjp7.htm)|Spell Effect: Life Boost|Effet : Gain de vie|libre|
|[nU4SxAk6XreHUi5h.htm](spell-effects/nU4SxAk6XreHUi5h.htm)|Spell Effect: Infectious Enthusiasm|Effet : Enthousiasme communicatif|libre|
|[nUJSdm4fy6fcwsvv.htm](spell-effects/nUJSdm4fy6fcwsvv.htm)|Spell Effect: Lashunta's Life Bubble|Effet : Bulle de vie du lashunta|libre|
|[NXzo2kdgVixIZ2T1.htm](spell-effects/NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|Effet : Compagnon alpha|libre|
|[oDDS6D2KTjpbA491.htm](spell-effects/oDDS6D2KTjpbA491.htm)|Spell Effect: Glass Shield|Effet : Bouclier en verre|libre|
|[oJbcmpBSHwmx6FD4.htm](spell-effects/oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|Effet : Forme de dinosaure (Deinonychus)|libre|
|[OlkrQOPjLclyyxCw.htm](spell-effects/OlkrQOPjLclyyxCw.htm)|Spell Effect: Mantle of the Unwavering Heart|Effet : Manteau du coeur inébranlable|libre|
|[OSapCU8heNHst21y.htm](spell-effects/OSapCU8heNHst21y.htm)|Spell Effect: Knock|Effet : Déblocage|libre|
|[otK6eG3b3FV7n2xP.htm](spell-effects/otK6eG3b3FV7n2xP.htm)|Spell Effect: Swampcall (Critical Failure)|Effet : Appel du marais (Échec critique)|libre|
|[p8F3MVUkGmpsUDOn.htm](spell-effects/p8F3MVUkGmpsUDOn.htm)|Spell Effect: Untwisting Iron Pillar|Effet : Pilier du fer qui ne plie pas|libre|
|[pcK88HqL6LjBNH2h.htm](spell-effects/pcK88HqL6LjBNH2h.htm)|Spell Effect: Faerie Dust (Critical Failure)|Effet : Poussière féerique (Échec critique)|libre|
|[PDoTV4EhJp63FEaG.htm](spell-effects/PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|Effet : Attirer la colère (Succès)|libre|
|[pE2TWhG97XbhgAdH.htm](spell-effects/pE2TWhG97XbhgAdH.htm)|Spell Effect: Incendiary Aura|Effet : Aura incendiaire|libre|
|[PhBrHvBwvq8rni9C.htm](spell-effects/PhBrHvBwvq8rni9C.htm)|Spell Effect: Evolution Surge (Large)|Effet : Flux d'évolution (Grande)|libre|
|[phIoucsDa3iplMm2.htm](spell-effects/phIoucsDa3iplMm2.htm)|Spell Effect: Elemental Form (Fire)|Effet : Forme élémentaire (Feu)|libre|
|[PkofF4bxkDUgeIoE.htm](spell-effects/PkofF4bxkDUgeIoE.htm)|Spell Effect: Touch of Corruption (Healing)|Effet : Toucher de corruption (Soins)|libre|
|[PNEGSVYhMKf6kQZ6.htm](spell-effects/PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|Effet : Appel aux armes|libre|
|[pocsoEi84Mr2buOc.htm](spell-effects/pocsoEi84Mr2buOc.htm)|Spell Effect: Evolution Surge (Scent)|Effet : Flux d'évolution (Odorat)|libre|
|[PpkOZVoHkBZUmddx.htm](spell-effects/PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|Effet : Forme de vase (Gelée ocre)|libre|
|[pPMldkAbPVOSOPIF.htm](spell-effects/pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|Effet : Protéger le compagnon|libre|
|[ppVKJY6AYggn2Fma.htm](spell-effects/ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|Effet : Corne d'abondance|libre|
|[PQHP7Oph3BQX1GhF.htm](spell-effects/PQHP7Oph3BQX1GhF.htm)|Spell Effect: Tailwind|Effet : Vent arrière|libre|
|[PQuQtDixruZmmvT4.htm](spell-effects/PQuQtDixruZmmvT4.htm)|Spell Effect: Primal Summons|Effet : Convocations primordiales|libre|
|[ptOqsN5FS0nQh7RW.htm](spell-effects/ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|Effet : Forme animale (Félin)|libre|
|[pzbU0cdyRvE66XBi.htm](spell-effects/pzbU0cdyRvE66XBi.htm)|Spell Effect: Discern Secrets|Effet: Discerner les secrets|libre|
|[q4EEYltjqpRGiLsP.htm](spell-effects/q4EEYltjqpRGiLsP.htm)|Spell Effect: Elemental Motion (Fire)|Effet : Mobilité élémentaire (Feu)|libre|
|[qbOpis7pIkXJbM2B.htm](spell-effects/qbOpis7pIkXJbM2B.htm)|Spell Effect: Elemental Motion (Earth)|Effet : Mobilité élémentaire (Terre)|libre|
|[qD1OA6dx8h33nKFC.htm](spell-effects/qD1OA6dx8h33nKFC.htm)|Spell Effect: Ferrous Form|Effet : Forme ferreuse|libre|
|[QF6RDlCoTvkVHRo4.htm](spell-effects/QF6RDlCoTvkVHRo4.htm)|Effect: Shield Immunity|Effet : Immunité à Bouclier|libre|
|[qhNUfwpkD8BRw4zj.htm](spell-effects/qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|Effet : Peau magique|libre|
|[QJRaVbulmpOzWi6w.htm](spell-effects/QJRaVbulmpOzWi6w.htm)|Spell Effect: Girzanje's March|Effet : Marche de Guirzanjé|libre|
|[qkwb5DD3zmKwvbk0.htm](spell-effects/qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mystic Armor|Effet : Armure mystique|libre|
|[qn7uO5Ih01yLJot7.htm](spell-effects/qn7uO5Ih01yLJot7.htm)|Spell Effect: Gecko Grip|Effet : Adhérence du gecko|libre|
|[qO1Gj9l8gh5CMEbf.htm](spell-effects/qO1Gj9l8gh5CMEbf.htm)|Spell Effect: Sand Form|Effet : Forme de sable|libre|
|[qo7DoF11Xl9gqmFc.htm](spell-effects/qo7DoF11Xl9gqmFc.htm)|Spell Effect: Rapid Adaptation (Desert)|Effet : Adaptation rapide (Désert)|libre|
|[Qp0dlhJaCzXIx73r.htm](spell-effects/Qp0dlhJaCzXIx73r.htm)|Spell Effect: Elemental Form|Effet : Forme élémentaire|libre|
|[qPaEEhczUWCQo6ux.htm](spell-effects/qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|Effet : Forme animale (Requin)|libre|
|[qRyynMOflGajgAR3.htm](spell-effects/qRyynMOflGajgAR3.htm)|Spell Effect: Mantle of the Magma Heart (Enlarging Eruption)|Effet : Manteau du coeur magmatique (Éruption élargie)|libre|
|[qzZmVjtc9feqoQwA.htm](spell-effects/qzZmVjtc9feqoQwA.htm)|Spell Effect: Wish-Twisted Form (Success)|Effet : Forme déformée par un souhait (succès)|libre|
|[R27azQfzeFuFc48G.htm](spell-effects/R27azQfzeFuFc48G.htm)|Spell Effect: Take its Course (Affliction, Hinder)|Effet : Suivre son cours (Affliction, entraver)|libre|
|[r4XX7yzeEOPK7l2a.htm](spell-effects/r4XX7yzeEOPK7l2a.htm)|Spell Effect: Seal Fate|Effet : Sceller le destin|libre|
|[rjM25qfw5BKj9h97.htm](spell-effects/rjM25qfw5BKj9h97.htm)|Spell Effect: Entangling Flora|Effet : Flore enchevêtrante|libre|
|[RLUhIyqH84Dle4vo.htm](spell-effects/RLUhIyqH84Dle4vo.htm)|Spell Effect: Fungal Infestation (Critical Failure)|Effet : Infestation fongique(Échec critique)|libre|
|[ROuGwXEvFznzGE9k.htm](spell-effects/ROuGwXEvFznzGE9k.htm)|Spell Effect: Swampcall (Failure)|Effet : Appel du marais (Échec)|libre|
|[rQaltMIEi2bn1Z4k.htm](spell-effects/rQaltMIEi2bn1Z4k.htm)|Spell Effect: Ki Form|Effet : Forme ki|libre|
|[rTVZ0zwiKeslRw6p.htm](spell-effects/rTVZ0zwiKeslRw6p.htm)|Spell Effect: Untamed Shift|Effet : Transformation indomptée|libre|
|[rVkUzqS4rpZp9LS3.htm](spell-effects/rVkUzqS4rpZp9LS3.htm)|Spell Effect: Bind Undead|Effet: Lier un mort-vivant|libre|
|[s6CwkSsMDGfUmotn.htm](spell-effects/s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|Effet : Protection contre la mort|libre|
|[S75DOLjKaSJGMc0D.htm](spell-effects/S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|Effet : Forme de fée (Élananxe)|libre|
|[sDN9b4bjCGH2nQnG.htm](spell-effects/sDN9b4bjCGH2nQnG.htm)|Spell Effect: Rapid Adaptation (Mountain)|Effet : Adaptation rapide (Montagne)|libre|
|[sE2txm68yZSFMV3v.htm](spell-effects/sE2txm68yZSFMV3v.htm)|Spell Effect: Sweet Dream (Voyaging)|Effet : Doux rêve (Voyage)|libre|
|[sfJyQKmoxSRo6FyP.htm](spell-effects/sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|Effet : Forme d'aberration (Gug)|libre|
|[sILRkGTwoBywy0BU.htm](spell-effects/sILRkGTwoBywy0BU.htm)|Spell Effect: Vapor Form|Effet : Forme vaporeuse|libre|
|[SjfDoeymtnYKoGUD.htm](spell-effects/SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|Effet : Forme d'aberration (Otyugh)|libre|
|[slI9P4jUp3ERPCqX.htm](spell-effects/slI9P4jUp3ERPCqX.htm)|Spell Effect: Impeccable Flow|Effet : Flux impeccable|libre|
|[sN3mQ7YrPBogEJRn.htm](spell-effects/sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|Effet : Forme animale (Canidé)|libre|
|[sPCWrhUHqlbGhYSD.htm](spell-effects/sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|Effet : Agrandissement|libre|
|[SyF5kpZlZuBF4lMf.htm](spell-effects/SyF5kpZlZuBF4lMf.htm)|Spell Effect: Tomorrow's Dawn|Effet : Aube de demain|libre|
|[T3t9776ataHzrmTs.htm](spell-effects/T3t9776ataHzrmTs.htm)|Spell Effect: Inside Ropes (3rd level)|Effet : Effet : Tripes internes (rang 3)|libre|
|[T5bk6UH7yuYog1Fp.htm](spell-effects/T5bk6UH7yuYog1Fp.htm)|Spell Effect: See the Unseen|Effet : Discerner l'invisible|libre|
|[T6XnxvsgvvOrpien.htm](spell-effects/T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|Effet : Forme de Dinosaure (Stégosaure)|libre|
|[TAAWbJgfESltn2we.htm](spell-effects/TAAWbJgfESltn2we.htm)|Spell Effect: Primal Summons (Water)|Effet : Convocations primordiales (Eau)|libre|
|[tC0Qk4AjYRd3csL7.htm](spell-effects/tC0Qk4AjYRd3csL7.htm)|Spell Effect: Swarm Form|Effet : Forme de nuée|libre|
|[tfdDpf9xSWgQer5g.htm](spell-effects/tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|Effet : Forme cosmique (Lune)|libre|
|[ThFug45WHkQQXcoF.htm](spell-effects/ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|Effet : Pas rapide|libre|
|[tjC6JeZgLDPIMHjG.htm](spell-effects/tjC6JeZgLDPIMHjG.htm)|Spell Effect: Malignant Sustenance|Effet : Alimentation maléfique|libre|
|[tk3go5Cl6Qt130Dk.htm](spell-effects/tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|Effet : Forme animale (Singe)|libre|
|[tNjimcyUwn8afeH6.htm](spell-effects/tNjimcyUwn8afeH6.htm)|Spell Effect: Gravity Weapon|Effet : Arme pesante|libre|
|[TpVkVALUBrBQjULn.htm](spell-effects/TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|Effet : Enflammer les coeurs|libre|
|[TrmNSuv6zWEiceqn.htm](spell-effects/TrmNSuv6zWEiceqn.htm)|Spell Effect: Incendiary Ashes (Failure)|Effet : Cendres incendiaires (Échec)|libre|
|[tu8FyCtmL3YYR2jL.htm](spell-effects/tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|Effet : Forme de plante (Arboréen)|libre|
|[TwtUIEyenrtAbeiX.htm](spell-effects/TwtUIEyenrtAbeiX.htm)|Spell Effect: Tangle Vine|Effet : Liane gênante|libre|
|[UAiaJYUDLtLToRUU.htm](spell-effects/UAiaJYUDLtLToRUU.htm)|Spell Effect: Deep Sight|Effet: Vision des profondeurs|libre|
|[ubHqpJwUwygkc2dR.htm](spell-effects/ubHqpJwUwygkc2dR.htm)|Spell Effect: Spiral of Horrors|Effet : Aura Effroyable|libre|
|[uD13zIE22foqmFgt.htm](spell-effects/uD13zIE22foqmFgt.htm)|Spell Effect: Weaken Earth|Effet : Terre fragilisée|libre|
|[uDOxq24S7IT2EcXv.htm](spell-effects/uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|Effet : Mémoire de l'objet (Arme ou outil)|libre|
|[uHUcP59Z5fAdomda.htm](spell-effects/uHUcP59Z5fAdomda.htm)|Spell Effect: Spirit Sense|Effet : Perception des esprits|libre|
|[UjoNm3lrhlg4ctAQ.htm](spell-effects/UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|Effet : Forme aérienne (Ptérosaure)|libre|
|[Um25D1qLtZWOSBny.htm](spell-effects/Um25D1qLtZWOSBny.htm)|Spell Effect: Shifting Form (Swim Speed)|Effet : Forme changeante (Nage)|libre|
|[uUlFic5lnr2FpNiG.htm](spell-effects/uUlFic5lnr2FpNiG.htm)|Spell Effect: Lashunta's Life Bubble (Heightened)|Effet : Bulle de vie du Lashunta (Intensifié 8e)|libre|
|[UXdt1WVA66oZOoZS.htm](spell-effects/UXdt1WVA66oZOoZS.htm)|Spell Effect: Flame Barrier|Effet : Barrière de flammes|officielle|
|[v051JKN0Dj3ve5cF.htm](spell-effects/v051JKN0Dj3ve5cF.htm)|Spell Effect: Sweet Dream (Insight)|Effet : Doux rêve (Intuition)|libre|
|[v09uwq1eHEAy2bgh.htm](spell-effects/v09uwq1eHEAy2bgh.htm)|Spell Effect: Unbreaking Wave Barrier|Effet : Barrière de la vague inexorable|libre|
|[V7jAnItnVqtfCAKt.htm](spell-effects/V7jAnItnVqtfCAKt.htm)|Spell Effect: Dragon Wings (60 Feet)|Effet : Ailes de dragon (18 mètres)|libre|
|[vf2KJ4oGJY4efnKE.htm](spell-effects/vf2KJ4oGJY4efnKE.htm)|Spell Effect: Precious Metals|Effet: Métaux précieux|libre|
|[vhFnQBvguBXo6vxx.htm](spell-effects/vhFnQBvguBXo6vxx.htm)|Spell Effect: Repel Metal|Effet : Répulsion du métal|libre|
|[ViBlOrd6hno3DiPP.htm](spell-effects/ViBlOrd6hno3DiPP.htm)|Spell Effect: Stumbling Curse|Effet : Malédiction titubante|libre|
|[vUjRvriyuHDZrsgc.htm](spell-effects/vUjRvriyuHDZrsgc.htm)|Spell Effect: Ghostly Shift|Effet : Transformation fantomatique|libre|
|[W0PjCMyGOpKAuyKX.htm](spell-effects/W0PjCMyGOpKAuyKX.htm)|Spell Effect: Weapon Surge (Major Striking)|Effet : Arme améliorée (Frappe majeure)|libre|
|[w1HwO7huxJoK0gHY.htm](spell-effects/w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|Effet : Incarnation élémentaire (Eau)|libre|
|[W4lb3417rNDd9tCq.htm](spell-effects/W4lb3417rNDd9tCq.htm)|Spell Effect: Righteous Might|Effet : Force du molosse|libre|
|[w9HpSwBLByNyRXvi.htm](spell-effects/w9HpSwBLByNyRXvi.htm)|Spell Effect: Blessing of Defiance (2 Action)|Effet : Bénédiction du défi (2 actions)|libre|
|[wdA5DW7YiYR4jXPs.htm](spell-effects/wdA5DW7YiYR4jXPs.htm)|Effect: Overwhelming Perfume|Effet: Parfum surpuissant|libre|
|[WdXLgPashH5in5eB.htm](spell-effects/WdXLgPashH5in5eB.htm)|Spell Effect: Conductive Weapon|Effet : Arme conductrice|libre|
|[wqh8D9kHGItZBvtQ.htm](spell-effects/wqh8D9kHGItZBvtQ.htm)|Spell Effect: Mantle of the Frozen Heart (Icy Claws)|Effet : Manteau du coeur gelé (Griffes de glace)|libre|
|[Wsgum7pZrPtASRf6.htm](spell-effects/Wsgum7pZrPtASRf6.htm)|Spell Effect: Word of Truth|Effet : Discours de vérité|libre|
|[WWtSEJGwKY4bQpUn.htm](spell-effects/WWtSEJGwKY4bQpUn.htm)|Spell Effect: Vital Beacon|Effet : Fanal de vie|libre|
|[X9Na6IK8FSPcTuoc.htm](spell-effects/X9Na6IK8FSPcTuoc.htm)|Spell Effect: Foresight|Effet : Prémonition|libre|
|[xgZxYqjDPNtsQ3Qp.htm](spell-effects/xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|Effet : Forme aérienne (Guêpe)|libre|
|[xKJVqN1ETnNH3tFg.htm](spell-effects/xKJVqN1ETnNH3tFg.htm)|Spell Effect: Corrosive Body (Heightened 9th)|Effet : Corps corrosif (Intensifié (9e))|libre|
|[Xl48OsJ47oDVZAVQ.htm](spell-effects/Xl48OsJ47oDVZAVQ.htm)|Spell Effect: Primal Summons (Earth)|Effet : Convocations primordiales (Terre)|libre|
|[Xlwt1wpjEKWBLUjK.htm](spell-effects/Xlwt1wpjEKWBLUjK.htm)|Spell Effect: Animal Feature (Fish Tail)|Effet : Trait animal (Queue de poisson)|libre|
|[XMBoKRRyooKnGkHk.htm](spell-effects/XMBoKRRyooKnGkHk.htm)|Spell Effect: Practice Makes Perfect|Effet : En forgeant on devient forgeron|libre|
|[XMOWgJV8UXWmpgk0.htm](spell-effects/XMOWgJV8UXWmpgk0.htm)|Spell Effect: Evolution Surge (Speed)|Effet : Flux d'évolution (Vitesse)|libre|
|[xPNKt1aQc3dquKlt.htm](spell-effects/xPNKt1aQc3dquKlt.htm)|Spell Effect: Element Embodied (Wood)|Effet : Incarnation élémentaire (Bois)|libre|
|[xPVOvWNJORvm8EwP.htm](spell-effects/xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|Effet : Imiter un mort-vivant|libre|
|[xsy1yaCj0SVsn502.htm](spell-effects/xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|Effet : Forme d'aberration (Chuul)|libre|
|[XT3AyRfx4xeXfAjP.htm](spell-effects/XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|Effet : Amélioration physique|libre|
|[XXtz5BDNNBmP9ub2.htm](spell-effects/XXtz5BDNNBmP9ub2.htm)|Spell Effect: Protector's Sphere|Effet: Sphere du protecteur|libre|
|[y4y0nusC97R7ZDL5.htm](spell-effects/y4y0nusC97R7ZDL5.htm)|Spell Effect: Elemental Betrayal|Effet : Trahison élémentaire|libre|
|[Y6aNYnGVXdAMvL7Y.htm](spell-effects/Y6aNYnGVXdAMvL7Y.htm)|Spell Effect: Thermal Stasis|Effet : Stase thermique|libre|
|[y9PJdDYFemhk6Z5o.htm](spell-effects/y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|Effet : Pieds Agiles|libre|
|[YCno88Te0nfwFgVo.htm](spell-effects/YCno88Te0nfwFgVo.htm)|Spell Effect: Mantle of the Frozen Heart (Ice Glide)|Effet : Manteau du coeur gelé (Glissement glacé)|libre|
|[ydsLEGjY89Akc4oZ.htm](spell-effects/ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|Effet : Forme de nuisible|libre|
|[yEQXaB7XyaROVqyb.htm](spell-effects/yEQXaB7XyaROVqyb.htm)|Spell Effect: Sweet Dream|Effet: Doux rêve|libre|
|[yke7fAUDxUNzouQc.htm](spell-effects/yke7fAUDxUNzouQc.htm)|Spell Effect: Elemental Gift (Air)|Effet : Don élémentaire (Air)|libre|
|[yq6iu0Qxg3YEbb6s.htm](spell-effects/yq6iu0Qxg3YEbb6s.htm)|Spell Effect: Elemental Gift (Water)|Effet : Don élémentaire (Eau)|libre|
|[yYDj0G4O3q5iGexx.htm](spell-effects/yYDj0G4O3q5iGexx.htm)|Spell Effect: Life's Fresh Bloom|Effet : Floraison de la vie|libre|
|[YzZs7r8VUOp7PiAW.htm](spell-effects/YzZs7r8VUOp7PiAW.htm)|Spell Effect: Magic Stone|Effet : Pierre magique|libre|
|[z2PYQCsDDoBZUwR5.htm](spell-effects/z2PYQCsDDoBZUwR5.htm)|Spell Effect: Wooden Fists|Effet : Poings de bois|libre|
|[zdpTPf157rXl3tEJ.htm](spell-effects/zdpTPf157rXl3tEJ.htm)|Spell Effect: Clawsong (Deadly D8)|Effet : Chant de la griffe (Mortel D8)|libre|
|[ZGzhNFB3SM8owk85.htm](spell-effects/ZGzhNFB3SM8owk85.htm)|Spell Effect: Take Root|Effet : Prendre racine|libre|
|[ZHVtJKnur9PAF5TO.htm](spell-effects/ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|Effet : Puissance protectrice|libre|
|[zjFN1cJEl3AMKiVs.htm](spell-effects/zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|Effet : Amulette de la nymphe|libre|
|[znwjWUvGOFQ6VYaE.htm](spell-effects/znwjWUvGOFQ6VYaE.htm)|Spell Effect: Entropic Wheel|Effet : Roue entropique|libre|
|[zPPZz6lcp87ALUde.htm](spell-effects/zPPZz6lcp87ALUde.htm)|Spell Effect: Ash Form|Effet: Forme Cendreuse|libre|
|[zRKw95WMezr6TgiT.htm](spell-effects/zRKw95WMezr6TgiT.htm)|Spell Effect: Moon Frenzy|Effet : Frénésie lunaire|libre|
