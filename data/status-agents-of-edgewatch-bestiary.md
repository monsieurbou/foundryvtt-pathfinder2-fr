# État de la traduction (agents-of-edgewatch-bestiary)

 * **libre**: 60
 * **officielle**: 124
 * **changé**: 3


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[8eBXEszbl4gOHGdU.htm](agents-of-edgewatch-bestiary/8eBXEszbl4gOHGdU.htm)|Wynsal Starborn|Wynsal Etoilené|changé|
|[rm0iJOMwruWSE93I.htm](agents-of-edgewatch-bestiary/rm0iJOMwruWSE93I.htm)|Olansa Terimor|Olansa Terimor|changé|
|[vPMmTtvl5UPOcCoa.htm](agents-of-edgewatch-bestiary/vPMmTtvl5UPOcCoa.htm)|Myrna Rath|Myrna Rath|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[07AGJt4ZRjwH85Xp.htm](agents-of-edgewatch-bestiary/07AGJt4ZRjwH85Xp.htm)|Mother Venom|Mère venin|libre|
|[0ti3f4fdcB5D2bLB.htm](agents-of-edgewatch-bestiary/0ti3f4fdcB5D2bLB.htm)|Casino Bouncer|Videur du casino|officielle|
|[0UbehYHzOGlNK8Hc.htm](agents-of-edgewatch-bestiary/0UbehYHzOGlNK8Hc.htm)|Baatamidar|Baatamidar|officielle|
|[10fEM7T48FUZRo6l.htm](agents-of-edgewatch-bestiary/10fEM7T48FUZRo6l.htm)|Barnacle Ghoul|Goule bernacle|libre|
|[181ucNY1zpp2Lz3x.htm](agents-of-edgewatch-bestiary/181ucNY1zpp2Lz3x.htm)|Grunka|Grunka|officielle|
|[1bPc2rjR4MghbMwD.htm](agents-of-edgewatch-bestiary/1bPc2rjR4MghbMwD.htm)|Obrousian|Obrousien|libre|
|[1eMDYXXf2leLTYHV.htm](agents-of-edgewatch-bestiary/1eMDYXXf2leLTYHV.htm)|Eberark|Eberark|officielle|
|[1G4OdEHRPF8GMHK8.htm](agents-of-edgewatch-bestiary/1G4OdEHRPF8GMHK8.htm)|Amateur Chemist|Chimiste amateur|officielle|
|[1UIJ4oknQ7btkUCb.htm](agents-of-edgewatch-bestiary/1UIJ4oknQ7btkUCb.htm)|Plunger Chute|Glissière à pistons|officielle|
|[1UtsScuNjmgwMZRn.htm](agents-of-edgewatch-bestiary/1UtsScuNjmgwMZRn.htm)|Excorion Paragon|Parangon Excorion|libre|
|[1xGAktpj6N2Ugh0r.htm](agents-of-edgewatch-bestiary/1xGAktpj6N2Ugh0r.htm)|Shatterling|Briselin|officielle|
|[2uxm1SxZXaG0ynCp.htm](agents-of-edgewatch-bestiary/2uxm1SxZXaG0ynCp.htm)|Pulping Golem|Golem Pâte à papier|libre|
|[3GNq4Vy40vk9zwMQ.htm](agents-of-edgewatch-bestiary/3GNq4Vy40vk9zwMQ.htm)|Graem|Graem|libre|
|[3If8bjsXeA2413dB.htm](agents-of-edgewatch-bestiary/3If8bjsXeA2413dB.htm)|Hegessik|Hegessik|libre|
|[3JEiwAFwkEjCDEYa.htm](agents-of-edgewatch-bestiary/3JEiwAFwkEjCDEYa.htm)|Kolo Harvan|Kolo Harvan|officielle|
|[3LLHyUz0Nabuw46L.htm](agents-of-edgewatch-bestiary/3LLHyUz0Nabuw46L.htm)|Boiling Fountains|Fontaines bouillantes|officielle|
|[3XbjmNeUtPLzxDge.htm](agents-of-edgewatch-bestiary/3XbjmNeUtPLzxDge.htm)|Penqual|Penqual|libre|
|[4BXo2a305RHmspMX.htm](agents-of-edgewatch-bestiary/4BXo2a305RHmspMX.htm)|Twisted Jack|Jack le dérangé|officielle|
|[4ffoPNBKdEwBmYgL.htm](agents-of-edgewatch-bestiary/4ffoPNBKdEwBmYgL.htm)|Battle Leader Rekarek|Cheffe de guerre Rekarek|officielle|
|[4TB1eo3O22khMyDU.htm](agents-of-edgewatch-bestiary/4TB1eo3O22khMyDU.htm)|Sad Liza|Liza l'Attristée|officielle|
|[54i6ithgmhDk7Dne.htm](agents-of-edgewatch-bestiary/54i6ithgmhDk7Dne.htm)|Rigged Cubby|Compartiment piégé|officielle|
|[5pBr5aWUb7yGCqN7.htm](agents-of-edgewatch-bestiary/5pBr5aWUb7yGCqN7.htm)|Washboard Dog Tough|Voyou des Chiens du lavoir|officielle|
|[5u7luLMeRNJ4en65.htm](agents-of-edgewatch-bestiary/5u7luLMeRNJ4en65.htm)|Bone Skipper Swarm|Nuée d'ossautilleuses|officielle|
|[64TX3n5xufgjqbwK.htm](agents-of-edgewatch-bestiary/64TX3n5xufgjqbwK.htm)|Poisoned Dart Statue|Statue à fléchette empoisonnée|officielle|
|[6c5CnrSxMYEgP6Fz.htm](agents-of-edgewatch-bestiary/6c5CnrSxMYEgP6Fz.htm)|Kekker|Kekker|libre|
|[7bMhDg4DNqoPlRsF.htm](agents-of-edgewatch-bestiary/7bMhDg4DNqoPlRsF.htm)|Avarek|Avarek|officielle|
|[7FJ3SQuHOUcGjm1x.htm](agents-of-edgewatch-bestiary/7FJ3SQuHOUcGjm1x.htm)|Eyeball Tank|Réservoir d'yeux|officielle|
|[7LN8clGKJWTpxISR.htm](agents-of-edgewatch-bestiary/7LN8clGKJWTpxISR.htm)|Blackfingers Acolyte|Acolyte de Noirs Doigts|libre|
|[7Q35HATCilOb1xXX.htm](agents-of-edgewatch-bestiary/7Q35HATCilOb1xXX.htm)|Living Paints|Peintures vivantes|officielle|
|[82wJQmzPlKttl0sc.htm](agents-of-edgewatch-bestiary/82wJQmzPlKttl0sc.htm)|Clockwork Arms|Bras mécaniques|officielle|
|[8AdsRaXftoR1beTk.htm](agents-of-edgewatch-bestiary/8AdsRaXftoR1beTk.htm)|Frefferth|Frefferth|officielle|
|[8GQ7dq7s9CetOlkg.htm](agents-of-edgewatch-bestiary/8GQ7dq7s9CetOlkg.htm)|Gang Tough|Voyou de gang|officielle|
|[8HTPdiH6yEk0jlNF.htm](agents-of-edgewatch-bestiary/8HTPdiH6yEk0jlNF.htm)|Pickled Punk|Voyou mariné|officielle|
|[8jjEOs99Bmo1v0Qc.htm](agents-of-edgewatch-bestiary/8jjEOs99Bmo1v0Qc.htm)|Teraphant|Téraphant|officielle|
|[8LlqjWcqiFq7YMmQ.htm](agents-of-edgewatch-bestiary/8LlqjWcqiFq7YMmQ.htm)|Agent of the Gray Queen|Agent de la Reine Grise|libre|
|[8PRqUfkHLvu9ufGL.htm](agents-of-edgewatch-bestiary/8PRqUfkHLvu9ufGL.htm)|Skinsaw Murderer|Meurtrier de l'Écorcheur|officielle|
|[8VAGXECLAk91hoAv.htm](agents-of-edgewatch-bestiary/8VAGXECLAk91hoAv.htm)|Field Of Opposition|Champ d'opposition|officielle|
|[94yI4TrNag0jMaYy.htm](agents-of-edgewatch-bestiary/94yI4TrNag0jMaYy.htm)|Lie-Master|Maître des mensonges|libre|
|[9M1YJxTXqya55HDx.htm](agents-of-edgewatch-bestiary/9M1YJxTXqya55HDx.htm)|Copper Hand Rogue|Roublard de la Main de cuivre|officielle|
|[9vt9Dr2a8MkkD83z.htm](agents-of-edgewatch-bestiary/9vt9Dr2a8MkkD83z.htm)|Chalky|Crayeux|officielle|
|[a3pTQfLzsJThQvI9.htm](agents-of-edgewatch-bestiary/a3pTQfLzsJThQvI9.htm)|Calennia|Calennia|libre|
|[aDgVmO3afhIgXQSN.htm](agents-of-edgewatch-bestiary/aDgVmO3afhIgXQSN.htm)|Ravenile Rager|Ravenile Ravageur|officielle|
|[ai7Q9vBHHAGj7uFE.htm](agents-of-edgewatch-bestiary/ai7Q9vBHHAGj7uFE.htm)|Svartalfar Killer|Svartalfar Assassin|libre|
|[aLhAofozzdTuqfcg.htm](agents-of-edgewatch-bestiary/aLhAofozzdTuqfcg.htm)|Acidic Poison Cloud Trap|Piège du nuage de poison acide|officielle|
|[ApEn56YEz9xavgug.htm](agents-of-edgewatch-bestiary/ApEn56YEz9xavgug.htm)|Life Magnet|Aimant de vie|officielle|
|[AYNIAAxV7TbIKPI4.htm](agents-of-edgewatch-bestiary/AYNIAAxV7TbIKPI4.htm)|Skitterstitch|Araignée de peaux recousues|officielle|
|[AZ6AaYdx2NwLWSwh.htm](agents-of-edgewatch-bestiary/AZ6AaYdx2NwLWSwh.htm)|Blackfinger's Prayer|Prière de Noirs Doigts|officielle|
|[azyIfDNNW44jY8YX.htm](agents-of-edgewatch-bestiary/azyIfDNNW44jY8YX.htm)|Barrel Launcher|Lanceur de barriques|officielle|
|[B4kIfCimsz8wfc0k.htm](agents-of-edgewatch-bestiary/B4kIfCimsz8wfc0k.htm)|Lord Guirden|Seigneur Guirden|officielle|
|[b7AnzEzoMGFpM33z.htm](agents-of-edgewatch-bestiary/b7AnzEzoMGFpM33z.htm)|Ink Drowning Vats|Cuve de noyade à l'encre|officielle|
|[BCMkxoQB4xK4BniG.htm](agents-of-edgewatch-bestiary/BCMkxoQB4xK4BniG.htm)|Secret-Keeper|Gardienne des secrets|libre|
|[BLRsSDFSMbZHcGDQ.htm](agents-of-edgewatch-bestiary/BLRsSDFSMbZHcGDQ.htm)|Black Whale Guard|Garde de la Baleine Noire|officielle|
|[BoFg19e3N8WiNa3Z.htm](agents-of-edgewatch-bestiary/BoFg19e3N8WiNa3Z.htm)|The Stabbing Beast|La Dardeuse|libre|
|[BuPf7xtqfwCjNOQv.htm](agents-of-edgewatch-bestiary/BuPf7xtqfwCjNOQv.htm)|Reginald Vancaskerkin|Reginald Vancaskerkin|officielle|
|[Bv1s6xJ55HS3Gxgs.htm](agents-of-edgewatch-bestiary/Bv1s6xJ55HS3Gxgs.htm)|Grick|Grick|officielle|
|[ce2SoJ7nRNZ1AoK6.htm](agents-of-edgewatch-bestiary/ce2SoJ7nRNZ1AoK6.htm)|Hidden Chute|Glissière cachée|officielle|
|[cLlOUUpCIAQwUuOP.htm](agents-of-edgewatch-bestiary/cLlOUUpCIAQwUuOP.htm)|Il'setsya Wyrmtouched|Il'setsya Bénie-des-wyrms|libre|
|[CLntGVs7cAIL9Trk.htm](agents-of-edgewatch-bestiary/CLntGVs7cAIL9Trk.htm)|Scathka|Scathka|libre|
|[cQMM2Ld0IBM9GcDo.htm](agents-of-edgewatch-bestiary/cQMM2Ld0IBM9GcDo.htm)|Norgorberite Poisoner|Empoisonneur Norgorbérite|officielle|
|[CTKAJG7oQaIQWCiB.htm](agents-of-edgewatch-bestiary/CTKAJG7oQaIQWCiB.htm)|Exploding Statue|Statue explosive|officielle|
|[D36nL0YjCVHfjBNw.htm](agents-of-edgewatch-bestiary/D36nL0YjCVHfjBNw.htm)|Clockwork Chopper|Trancheur mécanique|officielle|
|[d3TzpCuRJF78xHZK.htm](agents-of-edgewatch-bestiary/d3TzpCuRJF78xHZK.htm)|Black Whale Guard (F3)|Garde de la Baleine noire (F3)|officielle|
|[D5UF1i0f1IRcQcNB.htm](agents-of-edgewatch-bestiary/D5UF1i0f1IRcQcNB.htm)|Ghaele Of Kharnas|Ghaéle de Kharnas|libre|
|[DKReNCapWWubM3pm.htm](agents-of-edgewatch-bestiary/DKReNCapWWubM3pm.htm)|Shikwashim Mercenary|Mercenaire de Shikwashim|officielle|
|[E279VPhAy1a4ihqI.htm](agents-of-edgewatch-bestiary/E279VPhAy1a4ihqI.htm)|Living Mural|Peinture murale vivante|officielle|
|[E7NEf3kmsY3YjRrz.htm](agents-of-edgewatch-bestiary/E7NEf3kmsY3YjRrz.htm)|Kapral|Kapral|libre|
|[e96H6PLW11NCRK1h.htm](agents-of-edgewatch-bestiary/e96H6PLW11NCRK1h.htm)|Greater Planar Rift|Faille planaire supérieure|officielle|
|[EGQgqBfV80ll3pcf.htm](agents-of-edgewatch-bestiary/EGQgqBfV80ll3pcf.htm)|Chaos Gulgamodh|Gulgamodh du chaos|officielle|
|[eQc0ADMuHl1JzL8z.htm](agents-of-edgewatch-bestiary/eQc0ADMuHl1JzL8z.htm)|Shredskin|Chairpie|officielle|
|[eqwAdGsAk5JZKxUY.htm](agents-of-edgewatch-bestiary/eqwAdGsAk5JZKxUY.htm)|Gage Carlyle|Gage Carlyle|officielle|
|[FmiOJ9HEdCBDB89z.htm](agents-of-edgewatch-bestiary/FmiOJ9HEdCBDB89z.htm)|Burning Chandelier Trap|Piège du lustre brûlant|officielle|
|[Fmsw7P5CF3uHtD5W.htm](agents-of-edgewatch-bestiary/Fmsw7P5CF3uHtD5W.htm)|Veksciralenix|Veksciralenix|libre|
|[fV5VIoXMtixmI3Wc.htm](agents-of-edgewatch-bestiary/fV5VIoXMtixmI3Wc.htm)|Clockwork Assassin|Assassin mécanique|officielle|
|[fvOjtzuRNpmpEHXA.htm](agents-of-edgewatch-bestiary/fvOjtzuRNpmpEHXA.htm)|Binumir|Binumir|libre|
|[gd0sVQtCHbhP8iHI.htm](agents-of-edgewatch-bestiary/gd0sVQtCHbhP8iHI.htm)|Grospek Lavarsus|Grospek Lavarsus|officielle|
|[gFVazlIgCZivKzKF.htm](agents-of-edgewatch-bestiary/gFVazlIgCZivKzKF.htm)|Inky Tendrils|Vrilles noires d'encre|officielle|
|[gIcReNQOZceZBBlw.htm](agents-of-edgewatch-bestiary/gIcReNQOZceZBBlw.htm)|Jonis Flakfatter|Jonis Flakfatter|libre|
|[giVkjJeVZjhbR6eA.htm](agents-of-edgewatch-bestiary/giVkjJeVZjhbR6eA.htm)|Gas Trap|Piège à gaz|officielle|
|[gkRxUi9VrbPWOPGC.htm](agents-of-edgewatch-bestiary/gkRxUi9VrbPWOPGC.htm)|Daemonic Infector|Infector daémonique|libre|
|[gsn4NsJLwZCQUwgf.htm](agents-of-edgewatch-bestiary/gsn4NsJLwZCQUwgf.htm)|Almiraj|Almiraj|officielle|
|[GWO6vweLGT2J6q62.htm](agents-of-edgewatch-bestiary/GWO6vweLGT2J6q62.htm)|Miriel Grayleaf|Miriel Grisefeuille|officielle|
|[GYhV5eYNDO1Llbv2.htm](agents-of-edgewatch-bestiary/GYhV5eYNDO1Llbv2.htm)|Venom Mage|Mage venimeux|libre|
|[H4bY8v6e3drOIoUe.htm](agents-of-edgewatch-bestiary/H4bY8v6e3drOIoUe.htm)|Grabble Forden|Acappar Forden|libre|
|[HF0ymYmKC6KydPQ1.htm](agents-of-edgewatch-bestiary/HF0ymYmKC6KydPQ1.htm)|Franca Laurentz|Franca Laurentz|officielle|
|[HifZEgdCuZearOG2.htm](agents-of-edgewatch-bestiary/HifZEgdCuZearOG2.htm)|Clockwork Amalgam|Amalgame mécanique|officielle|
|[HXiA7x1jWnB1BqUy.htm](agents-of-edgewatch-bestiary/HXiA7x1jWnB1BqUy.htm)|Summoning Rune (Barbazu Devil)|Rune de convocation (Diable barbazu)|officielle|
|[hzThZ50RRdfTYTKc.htm](agents-of-edgewatch-bestiary/hzThZ50RRdfTYTKc.htm)|Clockwork Poison Bomb|Bombe mécanique empoisonnée|officielle|
|[IKfYWE1NLVNLCXZm.htm](agents-of-edgewatch-bestiary/IKfYWE1NLVNLCXZm.htm)|Blune's Illusory Toady|Flagorneur illusoire de Blune|libre|
|[JdMqHbaTwtOHVE7Y.htm](agents-of-edgewatch-bestiary/JdMqHbaTwtOHVE7Y.htm)|Diobel Sweeper Chemist|Chimiste des Boueurs de Diobel|libre|
|[JFcEtt18SGlb5uxm.htm](agents-of-edgewatch-bestiary/JFcEtt18SGlb5uxm.htm)|Supplicant Statues|Statues des suppliants|officielle|
|[jgSS31hwrQ1n4jVF.htm](agents-of-edgewatch-bestiary/jgSS31hwrQ1n4jVF.htm)|Waxworks Onslaught Trap|Piège d'assaut du musée de cire|officielle|
|[JhYJUNlcxiurcZcl.htm](agents-of-edgewatch-bestiary/JhYJUNlcxiurcZcl.htm)|Water Elemental Vessel (I2)|Réceptacle élémentaire d'eau (I2)|libre|
|[jJ1UTNLiRCoN1O3i.htm](agents-of-edgewatch-bestiary/jJ1UTNLiRCoN1O3i.htm)|Hendrid Pratchett|Hendrid Pratchett|libre|
|[JmHyHwaPMNKXzBts.htm](agents-of-edgewatch-bestiary/JmHyHwaPMNKXzBts.htm)|Zealborn|Zélincarné|libre|
|[JnNPdvOXtkGQHyfQ.htm](agents-of-edgewatch-bestiary/JnNPdvOXtkGQHyfQ.htm)|Gloaming Will-o'-Wisp|Feu follet crépusculaire|libre|
|[JsyO7yBf6YaF4YwF.htm](agents-of-edgewatch-bestiary/JsyO7yBf6YaF4YwF.htm)|Daemonic Rumormonger|Colporteur de rumeurs daémonique|officielle|
|[JuCLvgvxYbSRXqON.htm](agents-of-edgewatch-bestiary/JuCLvgvxYbSRXqON.htm)|Copper Hand Illusionist|Illusionniste de la Main de cuivre|libre|
|[JZUYQzQtzIwOGYvd.htm](agents-of-edgewatch-bestiary/JZUYQzQtzIwOGYvd.htm)|Ralso|Ralso|officielle|
|[k1s8syi5E2FoR3Q3.htm](agents-of-edgewatch-bestiary/k1s8syi5E2FoR3Q3.htm)|Freezing Alarm|Alarme glaçante|officielle|
|[kcvU9CatCyBUJRr2.htm](agents-of-edgewatch-bestiary/kcvU9CatCyBUJRr2.htm)|Rhevanna|Rhévanna|libre|
|[kidMmEzwgoBcHdnR.htm](agents-of-edgewatch-bestiary/kidMmEzwgoBcHdnR.htm)|The Laughing Fiend's Greeting|L'accueil du fiélon hilare|officielle|
|[l3AqEkQwRJS8TY7f.htm](agents-of-edgewatch-bestiary/l3AqEkQwRJS8TY7f.htm)|Canopy Drop|Chute de la canopée|officielle|
|[LAamprMlzk7k5auj.htm](agents-of-edgewatch-bestiary/LAamprMlzk7k5auj.htm)|Hestriviniaas|Hestriviniaas|libre|
|[LACpbwnVT7m2ZqBi.htm](agents-of-edgewatch-bestiary/LACpbwnVT7m2ZqBi.htm)|The Winder|Le Remonteur|officielle|
|[LAun416if9NFg3X2.htm](agents-of-edgewatch-bestiary/LAun416if9NFg3X2.htm)|Explosive Barrels|Barils explosifs|officielle|
|[lgbJbeSeC1f8otvH.htm](agents-of-edgewatch-bestiary/lgbJbeSeC1f8otvH.htm)|Daemonic Skinner|Dépiauteuse daémonique|officielle|
|[LkaB8RH73DY4TO9V.htm](agents-of-edgewatch-bestiary/LkaB8RH73DY4TO9V.htm)|Priest of Blackfingers|Prêtre de Noirs Doigts|libre|
|[lzmGdArS3kjJOqT6.htm](agents-of-edgewatch-bestiary/lzmGdArS3kjJOqT6.htm)|Lyrma Swampwalker|Lyrma Marchemarais|officielle|
|[M8ONVV7yl4uu0zcz.htm](agents-of-edgewatch-bestiary/M8ONVV7yl4uu0zcz.htm)|Ixusoth|Ixusoth|libre|
|[MfW7O5Ba8r2GR9ZQ.htm](agents-of-edgewatch-bestiary/MfW7O5Ba8r2GR9ZQ.htm)|Sordesdaemon|Sordesdaémon|libre|
|[MONwgTbrcZFzr6vC.htm](agents-of-edgewatch-bestiary/MONwgTbrcZFzr6vC.htm)|Antaro Boldblade|Antaro Fièrelame|officielle|
|[MqfZxoxFwzqAXhTP.htm](agents-of-edgewatch-bestiary/MqfZxoxFwzqAXhTP.htm)|Prospecti Statue|Statue de prospectif|officielle|
|[MQnhyCM9LInNYtl0.htm](agents-of-edgewatch-bestiary/MQnhyCM9LInNYtl0.htm)|Carvey|Carvey|officielle|
|[N1IpUIW2Ry7uBN3G.htm](agents-of-edgewatch-bestiary/N1IpUIW2Ry7uBN3G.htm)|Tiderunner Aquamancer|Aquamancien des Maîtres des marées|officielle|
|[nfULZbf8OOUDdrZM.htm](agents-of-edgewatch-bestiary/nfULZbf8OOUDdrZM.htm)|Water Elemental Vessel|Réceptacle élémentaire d'eau|libre|
|[nTn2szBbqQNdXhOr.htm](agents-of-edgewatch-bestiary/nTn2szBbqQNdXhOr.htm)|Skebs|Skebs|officielle|
|[oFCgiGFipWeit9sl.htm](agents-of-edgewatch-bestiary/oFCgiGFipWeit9sl.htm)|Lusca|Lusque|officielle|
|[oTa25rAxytm03T3X.htm](agents-of-edgewatch-bestiary/oTa25rAxytm03T3X.htm)|Shristi Melipdra|Shristi Mélipdra|officielle|
|[oWASKud0jwlGSfJg.htm](agents-of-edgewatch-bestiary/oWASKud0jwlGSfJg.htm)|Pelmo|Pelmo|libre|
|[P9Wg0sGcNkemOvm3.htm](agents-of-edgewatch-bestiary/P9Wg0sGcNkemOvm3.htm)|Slithering Rift|Faille rampante|officielle|
|[pE5GxoB1FtXBqnF7.htm](agents-of-edgewatch-bestiary/pE5GxoB1FtXBqnF7.htm)|Gref|Gref|libre|
|[PEjcy9CxelKC3Kp6.htm](agents-of-edgewatch-bestiary/PEjcy9CxelKC3Kp6.htm)|Zrukbat|Chauve-souris zruk|officielle|
|[pfHOwcITyC4gdCVu.htm](agents-of-edgewatch-bestiary/pfHOwcITyC4gdCVu.htm)|Garrote Master Assassin|Maître assassin du garrot|officielle|
|[phkvSUK6WXxgJOoC.htm](agents-of-edgewatch-bestiary/phkvSUK6WXxgJOoC.htm)|Alchemist Aspirant|Aspirant alchimiste|libre|
|[pK1tlsgkmzkaaCe5.htm](agents-of-edgewatch-bestiary/pK1tlsgkmzkaaCe5.htm)|Iroran Skeleton|Squelette irorien|libre|
|[pTkvg8OITTl6lsJY.htm](agents-of-edgewatch-bestiary/pTkvg8OITTl6lsJY.htm)|Minchgorm|Minchgorm|officielle|
|[q61A6EsMnDDtnqxH.htm](agents-of-edgewatch-bestiary/q61A6EsMnDDtnqxH.htm)|Ulressia The Blessed|Ulressia la Sainte|officielle|
|[QBlBYsxySiBMxf22.htm](agents-of-edgewatch-bestiary/QBlBYsxySiBMxf22.htm)|Zeal-damned Ghoul|Goule damnée par le zèle|officielle|
|[QHcBauLnHzfwkDBK.htm](agents-of-edgewatch-bestiary/QHcBauLnHzfwkDBK.htm)|Summoning Rune (Cinder Rat)|Rune de convocation (Rat des braises)|officielle|
|[QQkbvOCif8Bm1wws.htm](agents-of-edgewatch-bestiary/QQkbvOCif8Bm1wws.htm)|Izfiitar|Izfiitar|libre|
|[QRcIVhQV2vlpADSf.htm](agents-of-edgewatch-bestiary/QRcIVhQV2vlpADSf.htm)|Camarach|Camarache|libre|
|[qSL6PtsHPekEEEjx.htm](agents-of-edgewatch-bestiary/qSL6PtsHPekEEEjx.htm)|Overdrive Imentesh|Imentesh véloce|officielle|
|[qUhxXE2yT0qwSJQm.htm](agents-of-edgewatch-bestiary/qUhxXE2yT0qwSJQm.htm)|Agradaemon|Agradaémon|officielle|
|[qY1NrXKmL0y18qoz.htm](agents-of-edgewatch-bestiary/qY1NrXKmL0y18qoz.htm)|Bolar Of Stonemoor|Bolar de Landepierre|officielle|
|[qy53ECS2agScE7G3.htm](agents-of-edgewatch-bestiary/qy53ECS2agScE7G3.htm)|Kharnas's Lesser Glyph|Glyphe inférieur de Kharnas|officielle|
|[r3j5KEvULFP3fZS7.htm](agents-of-edgewatch-bestiary/r3j5KEvULFP3fZS7.htm)|Grimwold|Valsombre|officielle|
|[rAUaIxp3QFgT3bzl.htm](agents-of-edgewatch-bestiary/rAUaIxp3QFgT3bzl.htm)|Needling Stairs|Marches pleines d'aiguilles|officielle|
|[rDYDTCVUa5GS3uTE.htm](agents-of-edgewatch-bestiary/rDYDTCVUa5GS3uTE.htm)|Iron Maiden Trap|Piège de la vierge de fer|officielle|
|[rGTq4qItRB5H7nEk.htm](agents-of-edgewatch-bestiary/rGTq4qItRB5H7nEk.htm)|Graveknight Of Kharnas|Chevalier sépulcre de Kharnas|libre|
|[Rinxhe1cRXKEsXuW.htm](agents-of-edgewatch-bestiary/Rinxhe1cRXKEsXuW.htm)|Tyrroicese|Tyrroicese|officielle|
|[rM6ix6XTroJod3Vr.htm](agents-of-edgewatch-bestiary/rM6ix6XTroJod3Vr.htm)|Fayati Alummur|Fayati Alummur|officielle|
|[rsKf8ixrl3yBq1gb.htm](agents-of-edgewatch-bestiary/rsKf8ixrl3yBq1gb.htm)|Starwatch Commando|Commando de la Gardétoile|officielle|
|[RtWlzHaOrfFdJyJY.htm](agents-of-edgewatch-bestiary/RtWlzHaOrfFdJyJY.htm)|Alchemical Horror|Horreur alchimique|officielle|
|[rXePwiS4iecWNMGU.htm](agents-of-edgewatch-bestiary/rXePwiS4iecWNMGU.htm)|Grinlowe|Grinlowe|libre|
|[RyXA4wOGY8lenKVw.htm](agents-of-edgewatch-bestiary/RyXA4wOGY8lenKVw.htm)|Nenchuuj|Nenchuuj|libre|
|[S9JJsUNSeeoIClON.htm](agents-of-edgewatch-bestiary/S9JJsUNSeeoIClON.htm)|Poison Eater|Avaleur de poison|officielle|
|[SgkV5RtcK72d0HwI.htm](agents-of-edgewatch-bestiary/SgkV5RtcK72d0HwI.htm)|Excorion|Excorion|libre|
|[sn9Pjkr2jlMEqc3E.htm](agents-of-edgewatch-bestiary/sn9Pjkr2jlMEqc3E.htm)|Eunice|Eunice|libre|
|[Sq0Kb92nGkqj19Xx.htm](agents-of-edgewatch-bestiary/Sq0Kb92nGkqj19Xx.htm)|Miogimo|Miogimo|libre|
|[sUrJ7jxzBiJTbwVo.htm](agents-of-edgewatch-bestiary/sUrJ7jxzBiJTbwVo.htm)|Blune Bandersworth|Blune Bandervaleur|libre|
|[syUXLdUsEDYgni5R.htm](agents-of-edgewatch-bestiary/syUXLdUsEDYgni5R.htm)|Wrent Dicaspiron|Wrent Dicaspiron|officielle|
|[T6RsavB4ZZGdlNuA.htm](agents-of-edgewatch-bestiary/T6RsavB4ZZGdlNuA.htm)|Hands Of The Forgotten|Les mains des Oubliés|officielle|
|[t9m4ikMZsDwo9TQ1.htm](agents-of-edgewatch-bestiary/t9m4ikMZsDwo9TQ1.htm)|Maurrisa Jonne|Maurrisa Jonne|officielle|
|[TIaZIUb9Mq9B4Mf2.htm](agents-of-edgewatch-bestiary/TIaZIUb9Mq9B4Mf2.htm)|Bloody Berleth|Berleth le Sanguinaire|libre|
|[tm8vQ7gdAe9zVdDg.htm](agents-of-edgewatch-bestiary/tm8vQ7gdAe9zVdDg.htm)|Vaultbreaker Ooze|Vase perce-coffre|officielle|
|[tt5eaS28C4PrHmZD.htm](agents-of-edgewatch-bestiary/tt5eaS28C4PrHmZD.htm)|The Inkmaster|Le Maître de l'encre|libre|
|[u1cuwAE3xzhYW4Mi.htm](agents-of-edgewatch-bestiary/u1cuwAE3xzhYW4Mi.htm)|False Door Trap|Piège de la fausse porte|officielle|
|[uFU6dQfcNeKq68YT.htm](agents-of-edgewatch-bestiary/uFU6dQfcNeKq68YT.htm)|Vargouille|Vargouille|officielle|
|[Uhi3wX4KveuMSARt.htm](agents-of-edgewatch-bestiary/Uhi3wX4KveuMSARt.htm)|Giant Joro Spider|Araignée joro géante|officielle|
|[ukL9sApDCIWsVL64.htm](agents-of-edgewatch-bestiary/ukL9sApDCIWsVL64.htm)|Boiling Tub Trap|Piège des bacs d'eau bouillante|officielle|
|[UkmM3bjBoBld0uzS.htm](agents-of-edgewatch-bestiary/UkmM3bjBoBld0uzS.htm)|Flying Guillotine|Guillotine volante|officielle|
|[vwxNCuBksHYU2Dwf.htm](agents-of-edgewatch-bestiary/vwxNCuBksHYU2Dwf.htm)|Hundun Chaos Mage|Mage chaotiques hundun|officielle|
|[w2J6GpuMYM24U4sb.htm](agents-of-edgewatch-bestiary/w2J6GpuMYM24U4sb.htm)|Clockwork Injector|Injecteur mécanique|officielle|
|[Wb4Md6byPhBWe56J.htm](agents-of-edgewatch-bestiary/Wb4Md6byPhBWe56J.htm)|Cobbleswarm (AoE)|Nuée de pavés (AdA)|officielle|
|[WDTdWiC9Rdl6rqh8.htm](agents-of-edgewatch-bestiary/WDTdWiC9Rdl6rqh8.htm)|Myrucarx|Myrucarx|libre|
|[Wk2T0Wr8Sebo4br5.htm](agents-of-edgewatch-bestiary/Wk2T0Wr8Sebo4br5.htm)|Mr. Snips|Monsieur Cisailles|officielle|
|[WplBGSeB9pK9AULX.htm](agents-of-edgewatch-bestiary/WplBGSeB9pK9AULX.htm)|The Rabbit Prince|Le Prince lapin|officielle|
|[wsVW8MdOTeGgGM59.htm](agents-of-edgewatch-bestiary/wsVW8MdOTeGgGM59.htm)|Child Of Venom|Enfant de venin|officielle|
|[WXwvWHRpwK17YABQ.htm](agents-of-edgewatch-bestiary/WXwvWHRpwK17YABQ.htm)|Bregdi|Bregdi|officielle|
|[XDt87cqF85zWnlC8.htm](agents-of-edgewatch-bestiary/XDt87cqF85zWnlC8.htm)|Siege Shard|Fragment de siège|officielle|
|[Xi53GFvTgBApltjp.htm](agents-of-edgewatch-bestiary/Xi53GFvTgBApltjp.htm)|Giant Bone Skipper|Ossautilleuse géante|officielle|
|[xkkj0TW6BKNT3Bg4.htm](agents-of-edgewatch-bestiary/xkkj0TW6BKNT3Bg4.htm)|Diobel Sweeper Tough|Voyou des Boueurs de Diobel|libre|
|[xN3FDmrCKWW0psBu.htm](agents-of-edgewatch-bestiary/xN3FDmrCKWW0psBu.htm)|Sleepless Sun Veteran|Vétéran des Astres vigilants|officielle|
|[XTHcALqbg5kgxtPw.htm](agents-of-edgewatch-bestiary/XTHcALqbg5kgxtPw.htm)|Arcane Feedback Trap|Piège de rétroaction arcanique|officielle|
|[xw7S8108irh2D1Uw.htm](agents-of-edgewatch-bestiary/xw7S8108irh2D1Uw.htm)|Dart Barrage|Déluge de fléchettes|officielle|
|[y0bIU9FCWHOJxUzG.htm](agents-of-edgewatch-bestiary/y0bIU9FCWHOJxUzG.htm)|Bloody Barber Goon|Homme de main des Barbiers sanglants|officielle|
|[y1tw2ohNagqQJ6RV.htm](agents-of-edgewatch-bestiary/y1tw2ohNagqQJ6RV.htm)|Skinsaw Seamer|Piqueur de l'Écorcheur|officielle|
|[yR6p0KVvZ3tPflRt.htm](agents-of-edgewatch-bestiary/yR6p0KVvZ3tPflRt.htm)|Kemeneles|Kemeneles|libre|
|[ySpOZlKUbcxWhKQ6.htm](agents-of-edgewatch-bestiary/ySpOZlKUbcxWhKQ6.htm)|Ofalth Zombie|Zombie Olfath|libre|
|[Z6R8YjgX8Jvt9Ds4.htm](agents-of-edgewatch-bestiary/Z6R8YjgX8Jvt9Ds4.htm)|Avsheros the Betrayer|Avsheros le Traître|libre|
|[zj2sCM8tQSMG9Qm6.htm](agents-of-edgewatch-bestiary/zj2sCM8tQSMG9Qm6.htm)|Najra Lizard|Lézard Najra|officielle|
|[ZL2qLXwomKfBB8Eu.htm](agents-of-edgewatch-bestiary/ZL2qLXwomKfBB8Eu.htm)|Dreadsong Dancer|Danseur chanteffroi|libre|
|[zrh3MrS68H2gPlVs.htm](agents-of-edgewatch-bestiary/zrh3MrS68H2gPlVs.htm)|Tenome|Ténome|officielle|
|[ZY3q7AV1qbwWwNl2.htm](agents-of-edgewatch-bestiary/ZY3q7AV1qbwWwNl2.htm)|Mobana|Mobana|officielle|
