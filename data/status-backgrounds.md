# État de la traduction (backgrounds)

 * **libre**: 351
 * **officielle**: 38


Dernière mise à jour: 2024-01-28 19:20 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0c9Np7Yq5JSxZ6Tb.htm](backgrounds/0c9Np7Yq5JSxZ6Tb.htm)|Alkenstar Tinker|Bricoleur d'Alkenastre|libre|
|[0FgYkkKv9u8zxWiO.htm](backgrounds/0FgYkkKv9u8zxWiO.htm)|Child of the Twin Village|Enfant du village de jumeaux|libre|
|[0ncxOTCDptu01nR8.htm](backgrounds/0ncxOTCDptu01nR8.htm)|Dauntless|Intrépide|libre|
|[0oTvU8dNFcT42CWV.htm](backgrounds/0oTvU8dNFcT42CWV.htm)|Willowshore Urchin|Gamin des rues de Willowshore|libre|
|[0otxwSLobUCf0l1I.htm](backgrounds/0otxwSLobUCf0l1I.htm)|Surge Investigator|Enquêteur de source magique|libre|
|[0wzTmGUb8yvzMrO0.htm](backgrounds/0wzTmGUb8yvzMrO0.htm)|Back-Alley Doctor|Docteur d'arrière salle|libre|
|[0z0PSizHviOehdJF.htm](backgrounds/0z0PSizHviOehdJF.htm)|Haunting Vision|Hanté par une vision|officielle|
|[0ZfBP7Tp2P3WN7Dp.htm](backgrounds/0ZfBP7Tp2P3WN7Dp.htm)|Amnesiac|Amnésique|libre|
|[18Jzk3gAsXspFOYC.htm](backgrounds/18Jzk3gAsXspFOYC.htm)|Haunted Citizen|Citoyen hanté|libre|
|[1bjReaLqCD9pvBJ3.htm](backgrounds/1bjReaLqCD9pvBJ3.htm)|Academy Dropout|Décrocheur d'académie|libre|
|[1Fpf40RBd0EgwP7R.htm](backgrounds/1Fpf40RBd0EgwP7R.htm)|Sentinel Reflectance|Reflet de Sentinelle|libre|
|[1M91pTjatEejBjEl.htm](backgrounds/1M91pTjatEejBjEl.htm)|Hired Killer|Tueur à gages|libre|
|[1VdLr4Qm8fv1m4tM.htm](backgrounds/1VdLr4Qm8fv1m4tM.htm)|Godless Graycloak|Athéiste des Capes grises|libre|
|[22X8QMu7Z1mJzNWC.htm](backgrounds/22X8QMu7Z1mJzNWC.htm)|Quick Refugee|Réfugié rapide|libre|
|[29mBzKZ7TRm2TizG.htm](backgrounds/29mBzKZ7TRm2TizG.htm)|Dendrologist|Dendrologue|libre|
|[2bzqI0D4J3LUi8nq.htm](backgrounds/2bzqI0D4J3LUi8nq.htm)|Laborer|Manoeuvre|libre|
|[2FUdcPsXwoWT1bms.htm](backgrounds/2FUdcPsXwoWT1bms.htm)|Tech Reliant|Techno dépendant|libre|
|[2jeonnY3GZTdEMsm.htm](backgrounds/2jeonnY3GZTdEMsm.htm)|Harrow-Led|Déterminé par le Tourment|libre|
|[2lk5NOcu1aUglUdK.htm](backgrounds/2lk5NOcu1aUglUdK.htm)|Fireworks Performer|Artiste de feux d'artifice|libre|
|[2Mm0BHMJZhqIqoQG.htm](backgrounds/2Mm0BHMJZhqIqoQG.htm)|Undertaker|Croque-mort|libre|
|[2nt2nsQMBXgQRlOu.htm](backgrounds/2nt2nsQMBXgQRlOu.htm)|Northridge Scholar|Érudit de Crête nord|libre|
|[2qH61dLeaqgNOdOp.htm](backgrounds/2qH61dLeaqgNOdOp.htm)|Desert Tracker|Pisteur du désert|libre|
|[2sRZ6OsvHIpwGCAg.htm](backgrounds/2sRZ6OsvHIpwGCAg.htm)|Planar Migrant|Migrant planaire|libre|
|[2TImvKwcFLDVhzUC.htm](backgrounds/2TImvKwcFLDVhzUC.htm)|Circuit Judge|Juge itinérant|libre|
|[2UXBsyJFimwYOUFQ.htm](backgrounds/2UXBsyJFimwYOUFQ.htm)|Thrill-Seeker|Amateur de sensations fortes|libre|
|[2W2jadmj4gPNfdQ9.htm](backgrounds/2W2jadmj4gPNfdQ9.htm)|Southbank Traditionalist|Traditionnaliste de Rive du sud|libre|
|[2wGmG7ntcnkvtw8l.htm](backgrounds/2wGmG7ntcnkvtw8l.htm)|Lost Loved One|Perte de l'être cher|libre|
|[2zYdTia5t7w6J2ey.htm](backgrounds/2zYdTia5t7w6J2ey.htm)|Historical Reenactor|Reconstitueur historique|libre|
|[3frMfODIYFeqTl2k.htm](backgrounds/3frMfODIYFeqTl2k.htm)|Fortune Teller|Voyant|officielle|
|[3gN09dOT2hMwGcK2.htm](backgrounds/3gN09dOT2hMwGcK2.htm)|Cook|Cuisinier|libre|
|[3kXTGUvodNMnJTxb.htm](backgrounds/3kXTGUvodNMnJTxb.htm)|Aiudara Seeker|Chercheur d'aiudara|libre|
|[3M2FRDlunjFshzbq.htm](backgrounds/3M2FRDlunjFshzbq.htm)|Fogfen Tale-Teller|Raconteur d'histoires des Marais brumeux|libre|
|[3wLnNwWnZ2dHIbV4.htm](backgrounds/3wLnNwWnZ2dHIbV4.htm)|Diobel Pearl Diver|Pêcheur de perles de Diobel|libre|
|[3WPo7m6rJQh9L7MN.htm](backgrounds/3WPo7m6rJQh9L7MN.htm)|Emissary|Émissaire|officielle|
|[3YQ1Wcjzk8ftoyo7.htm](backgrounds/3YQ1Wcjzk8ftoyo7.htm)|Astrologer|Astrologue|libre|
|[44T9XsTcTbmMhtc1.htm](backgrounds/44T9XsTcTbmMhtc1.htm)|Crystal Healer|Lithothérapeute|libre|
|[4a2sVO0o2mMTydN8.htm](backgrounds/4a2sVO0o2mMTydN8.htm)|Local Scion|Enfant du pays|officielle|
|[4aVFnYyRajog0mNl.htm](backgrounds/4aVFnYyRajog0mNl.htm)|Mantis Scion|Descendant de la Mante|libre|
|[4fBIXtSVSRYn2ZGi.htm](backgrounds/4fBIXtSVSRYn2ZGi.htm)|Goblinblood Orphan|Orphelin des guerres du Sang gobelin|officielle|
|[4II2KZpizlBBcfCy.htm](backgrounds/4II2KZpizlBBcfCy.htm)|Borderlands Pioneer|Pionnier des terres frontalières|libre|
|[4KCa67gyxUn60zdf.htm](backgrounds/4KCa67gyxUn60zdf.htm)|Saloon Entertainer|Amuseur de saloon|libre|
|[4lbfPXwZEf3eE0ip.htm](backgrounds/4lbfPXwZEf3eE0ip.htm)|Snubbed Out Stoolie|Indic repoussé|libre|
|[4naQmCXBl0007c2W.htm](backgrounds/4naQmCXBl0007c2W.htm)|Touched by Dahak|Touché par Dahak|libre|
|[4Vc8uXVHonrasFnU.htm](backgrounds/4Vc8uXVHonrasFnU.htm)|Saboteur|Saboteur|libre|
|[4XpWgGejwb2L6WpK.htm](backgrounds/4XpWgGejwb2L6WpK.htm)|Codebreaker|Cryptographe|libre|
|[4yN5miHoMvKwZIsa.htm](backgrounds/4yN5miHoMvKwZIsa.htm)|Press-Ganged (LOWG)|Forçat des Mers (LOWG)|libre|
|[5Bfzt3sZyiaKrBO0.htm](backgrounds/5Bfzt3sZyiaKrBO0.htm)|Sign Bound|Lié à un signe|libre|
|[5dezuNcZJ6wfecnG.htm](backgrounds/5dezuNcZJ6wfecnG.htm)|Gunsmith|Armurier|libre|
|[5lcDlcXKD8eji6n3.htm](backgrounds/5lcDlcXKD8eji6n3.htm)|Astrological Augur|Augure astrologique|libre|
|[5Oh9SdD4rhwlpHzg.htm](backgrounds/5Oh9SdD4rhwlpHzg.htm)|Tide Watcher|Observateur de la marée|libre|
|[5qUQOpdlNsJjpFVX.htm](backgrounds/5qUQOpdlNsJjpFVX.htm)|Ruin Delver|Explorateur de ruines|libre|
|[5RGLAPi5sLykRcmm.htm](backgrounds/5RGLAPi5sLykRcmm.htm)|Animal Whisperer|Dresseur|officielle|
|[5RhHdnzWWKfh5faz.htm](backgrounds/5RhHdnzWWKfh5faz.htm)|Guest of Sedeq Lodge|Invité de la Loge de Sedeq|libre|
|[5Z3cLEpsx9nHVwhM.htm](backgrounds/5Z3cLEpsx9nHVwhM.htm)|Hunter|Chasseur|officielle|
|[6abqATPjYoF946LD.htm](backgrounds/6abqATPjYoF946LD.htm)|Bandit|Bandit|libre|
|[6c0rsuiiAaVqGTu7.htm](backgrounds/6c0rsuiiAaVqGTu7.htm)|Rivethun Adherent|Adhérent du Rivethun|libre|
|[6irgRkKZ8tRZzLvs.htm](backgrounds/6irgRkKZ8tRZzLvs.htm)|Artisan|Artisan|officielle|
|[6K6jJkjZ2MJYqQ6h.htm](backgrounds/6K6jJkjZ2MJYqQ6h.htm)|Bellflower Agent|Agent de la Campanule|libre|
|[6UmhTxOQeqFnppxx.htm](backgrounds/6UmhTxOQeqFnppxx.htm)|Guard|Garde|officielle|
|[6vsoyCZKqxG0lVe8.htm](backgrounds/6vsoyCZKqxG0lVe8.htm)|Inlander|Habitant de l'intérieur des terres|libre|
|[76j9ds5URXv1dqnm.htm](backgrounds/76j9ds5URXv1dqnm.htm)|Plant Whisperer|Botaniste|libre|
|[76RK9WizWYdyhMy5.htm](backgrounds/76RK9WizWYdyhMy5.htm)|Mammoth Speaker|Dresseur de mammouth|libre|
|[7AfixHrjbXgDPPkp.htm](backgrounds/7AfixHrjbXgDPPkp.htm)|Translator|Traducteur|libre|
|[7fCZTzmv5I2dI4sr.htm](backgrounds/7fCZTzmv5I2dI4sr.htm)|Discarded Duplicate|Doublure rejetée|libre|
|[7IrOApgShgnmp1A5.htm](backgrounds/7IrOApgShgnmp1A5.htm)|Rigger|Gréeur|libre|
|[7K6ZSWOoihZKSdyd.htm](backgrounds/7K6ZSWOoihZKSdyd.htm)|Ruby Phoenix Enthusiast|Passionné du Phénix de rubis|libre|
|[7OAX5QMd15svZJzX.htm](backgrounds/7OAX5QMd15svZJzX.htm)|Relentless Dedication|Dévouement persévérant|libre|
|[7QkQQsHjv2iNFIsF.htm](backgrounds/7QkQQsHjv2iNFIsF.htm)|Grave Robber|Pilleur de tombe|libre|
|[7z33GaNsmSxel1xJ.htm](backgrounds/7z33GaNsmSxel1xJ.htm)|Doomcaller|Invocateur du malheur|libre|
|[84uVpQFCqn0Atfpo.htm](backgrounds/84uVpQFCqn0Atfpo.htm)|Legendary Parents|Parents légendaires|libre|
|[86TbxxwfpWjScwSQ.htm](backgrounds/86TbxxwfpWjScwSQ.htm)|Undercover Lotus Guard|Garde Lotus infiltré|libre|
|[88WyCqU5x1eJ0MK2.htm](backgrounds/88WyCqU5x1eJ0MK2.htm)|Gladiator|Gladiateur|officielle|
|[89LEOv97ZwsjnhNx.htm](backgrounds/89LEOv97ZwsjnhNx.htm)|Gambler|Parieur|officielle|
|[8CLTXqyjdzEMKnZC.htm](backgrounds/8CLTXqyjdzEMKnZC.htm)|Cannoneer|Cannonier|libre|
|[8hsMIh3lVGfZwjG5.htm](backgrounds/8hsMIh3lVGfZwjG5.htm)|Pillar|Pilier|libre|
|[8q4PhvpmIxZD7rsV.htm](backgrounds/8q4PhvpmIxZD7rsV.htm)|Rostland Partisan|Partisan du Rost|libre|
|[8UEKgUkagUDixkL2.htm](backgrounds/8UEKgUkagUDixkL2.htm)|Issian Partisan|Partisan Issien|libre|
|[8usMHYAmFdqmmkTS.htm](backgrounds/8usMHYAmFdqmmkTS.htm)|Sponsored by a Village|Soutenu par un village|libre|
|[8UXahQfkP9GZ1TNW.htm](backgrounds/8UXahQfkP9GZ1TNW.htm)|Nomad|Nomade|officielle|
|[90vPOBrSXY27k0bL.htm](backgrounds/90vPOBrSXY27k0bL.htm)|Tyrant Witness|Témoin du Tyran|libre|
|[93icIDHD4IrqI2oV.htm](backgrounds/93icIDHD4IrqI2oV.htm)|Sodden Scavenger|Récupérateur détrempé|libre|
|[9l6q90sk8UM292CY.htm](backgrounds/9l6q90sk8UM292CY.htm)|Waste Walker|Arpenteur de la Désolation|libre|
|[9LnXsMRwYcxi7nDO.htm](backgrounds/9LnXsMRwYcxi7nDO.htm)|Bekyar Restorer|Restaurateur Bekyar|libre|
|[9lVw1JGl5ser6626.htm](backgrounds/9lVw1JGl5ser6626.htm)|Criminal|Criminel|officielle|
|[9pK15dQJVypSCjzO.htm](backgrounds/9pK15dQJVypSCjzO.htm)|Blessed|Béni|libre|
|[9uqDWl8V2AgGMRXi.htm](backgrounds/9uqDWl8V2AgGMRXi.htm)|Propaganda Promoter|Promoteur de propagande|libre|
|[9uTdwJaj27F18ZDX.htm](backgrounds/9uTdwJaj27F18ZDX.htm)|Razmiran Faithful|Fidèle de Razmir|libre|
|[a45LqkSRX07ljKdW.htm](backgrounds/a45LqkSRX07ljKdW.htm)|Merabite Prodigy|Prodige Mérabite|libre|
|[a57iBUZBQtXWZc36.htm](backgrounds/a57iBUZBQtXWZc36.htm)|Dedicated Delver|Spéléologue dévoué|libre|
|[a5dCSuAwGE2hqQjj.htm](backgrounds/a5dCSuAwGE2hqQjj.htm)|Freed Slave of Absalom|Esclave libéré d'Absalom|libre|
|[a8BmnIIUR7AYog5B.htm](backgrounds/a8BmnIIUR7AYog5B.htm)|Barkeep|Tavernier|libre|
|[a9Q4iIiAZryVWN27.htm](backgrounds/a9Q4iIiAZryVWN27.htm)|Bounty Hunter|Chasseur de primes|officielle|
|[a9YjyQRNEOyespFf.htm](backgrounds/a9YjyQRNEOyespFf.htm)|Pyre Tender|Bâtisseur de bûcher|libre|
|[AfBCrHsw1xbRFejN.htm](backgrounds/AfBCrHsw1xbRFejN.htm)|Sleepless Suns Star|Étoile des soleils sans repos|libre|
|[aisuJF1A98bHfkLH.htm](backgrounds/aisuJF1A98bHfkLH.htm)|Whispering Way Scion|Descendant de la Voie du Murmure|libre|
|[AJ41zFEYwlOUghXp.htm](backgrounds/AJ41zFEYwlOUghXp.htm)|Osirionologist|Osirionologiste|libre|
|[ajcpRVb5EG00l7Y4.htm](backgrounds/ajcpRVb5EG00l7Y4.htm)|Cursed Family|Famille maudite|libre|
|[AJuE41aToGs9tbcM.htm](backgrounds/AJuE41aToGs9tbcM.htm)|Osprey Spellcaster|Incantateur de Balbuzard|libre|
|[Am8kwC9c2GQ5bJAW.htm](backgrounds/Am8kwC9c2GQ5bJAW.htm)|Undercover Contender|Concurrent sous couverture|libre|
|[ap25MWBuFGwwhYIG.htm](backgrounds/ap25MWBuFGwwhYIG.htm)|Aspiring Free-Captain|Aspirant Capitaine-libre|libre|
|[apXTV7jJx6yJpj8D.htm](backgrounds/apXTV7jJx6yJpj8D.htm)|Prisoner|Prisonnier|libre|
|[AqhHif8mzYjlGMxJ.htm](backgrounds/AqhHif8mzYjlGMxJ.htm)|Sheriff|Shérif|libre|
|[aWAfj7bhTZM2oK81.htm](backgrounds/aWAfj7bhTZM2oK81.htm)|Hookclaw Digger|Creuseur Crochetgriffu|libre|
|[b1588nhgRoYMef44.htm](backgrounds/b1588nhgRoYMef44.htm)|Wanderlust|Goût du voyage|libre|
|[b3UC18ueX8m9Ov0W.htm](backgrounds/b3UC18ueX8m9Ov0W.htm)|Hermit|Ermite|libre|
|[B3Z5gbhMDRUATTrE.htm](backgrounds/B3Z5gbhMDRUATTrE.htm)|Gloriana's Fixer|Agent de Gloriana|libre|
|[B6kNrX1y8XNbQYea.htm](backgrounds/B6kNrX1y8XNbQYea.htm)|Goldhand Arms Dealer|Marchand d'armes de la loge de la Main d'or|libre|
|[B8kEwzPUMIjhofUm.htm](backgrounds/B8kEwzPUMIjhofUm.htm)|Sarkorian Survivor|Survivant de Sarkoris|libre|
|[b9EPEY09dYOVzdue.htm](backgrounds/b9EPEY09dYOVzdue.htm)|Shadow War Survivor|Survivant de la Guerre de l'ombre|libre|
|[BBeJA7n0xpSsBCGq.htm](backgrounds/BBeJA7n0xpSsBCGq.htm)|Lesser Scion|Benjamin|libre|
|[bCJ9p3P5uJDAtaUI.htm](backgrounds/bCJ9p3P5uJDAtaUI.htm)|Miner|Mineur|officielle|
|[bdeuzUMiPjEdlPS8.htm](backgrounds/bdeuzUMiPjEdlPS8.htm)|Crown of Chaos|Couronne du Chaos|libre|
|[bDyb0k0rTfDTyhd8.htm](backgrounds/bDyb0k0rTfDTyhd8.htm)|Geb Crusader|Croisé du Geb|libre|
|[bh6O2Ad5mkYwRngM.htm](backgrounds/bh6O2Ad5mkYwRngM.htm)|Hermean Expatriate|Expatrié d'Herméa|libre|
|[biCstJF0OGvC5NKW.htm](backgrounds/biCstJF0OGvC5NKW.htm)|Concordance Researcher|Chercheur de la Concordance|libre|
|[bNsYkUGMKmtc28MX.htm](backgrounds/bNsYkUGMKmtc28MX.htm)|Issian Patriot|Patriote issien|libre|
|[BRXXlw03K6ZeB2Li.htm](backgrounds/BRXXlw03K6ZeB2Li.htm)|Eidolon Contact|Contact d'eidolon|libre|
|[bxWID85noazU72O3.htm](backgrounds/bxWID85noazU72O3.htm)|Ratted-Out Gun Runner|Marchand d'armes en cavale|libre|
|[byyceQSVT0H5GfNB.htm](backgrounds/byyceQSVT0H5GfNB.htm)|Anti-Magical|Anti-Magie|libre|
|[BZhPPw9VD9U2ur6B.htm](backgrounds/BZhPPw9VD9U2ur6B.htm)|Witchlight Follower|Suiveur de lumière de sorcier|libre|
|[CAjQrHZZbALE7Qjy.htm](backgrounds/CAjQrHZZbALE7Qjy.htm)|Acolyte|Acolyte|officielle|
|[cCi1Lsld9Umz1uHs.htm](backgrounds/cCi1Lsld9Umz1uHs.htm)|Sense of Belonging|Sentiment d'appartenance|libre|
|[ChAoT1V3Nc4sKXz4.htm](backgrounds/ChAoT1V3Nc4sKXz4.htm)|Corpse Stitcher|Recouseur de cadavre|libre|
|[CjNA8ippwoAvpxeI.htm](backgrounds/CjNA8ippwoAvpxeI.htm)|Ozem Experience|Expérience d'Ozem|libre|
|[CKU1sbFofcwZUJMx.htm](backgrounds/CKU1sbFofcwZUJMx.htm)|Ex-Con Token Guard|Garde des Pièces ancien taulard|libre|
|[cODrdTvko4Om26ik.htm](backgrounds/cODrdTvko4Om26ik.htm)|Mechanic|Mécanicien|libre|
|[D00NdG0WtUNMO546.htm](backgrounds/D00NdG0WtUNMO546.htm)|Mammoth Herder|Berger des mammouths|libre|
|[d5fKB0ZMJQkwDF5p.htm](backgrounds/d5fKB0ZMJQkwDF5p.htm)|Otherworldly Mission|Missionné de l'autre monde|libre|
|[dAvFZ5QmbAHgXcNp.htm](backgrounds/dAvFZ5QmbAHgXcNp.htm)|Outrider|Chevaucheur des plaines|libre|
|[DBxOUwM7qhGH8MrF.htm](backgrounds/DBxOUwM7qhGH8MrF.htm)|Courier|Garçon de course|libre|
|[DHrzVqB8f1ed3zTk.htm](backgrounds/DHrzVqB8f1ed3zTk.htm)|Clown|Clown|libre|
|[DpmWrtkrmmgnVtAc.htm](backgrounds/DpmWrtkrmmgnVtAc.htm)|Construction Occultist|Occultiste de la construction|libre|
|[dSYel8ABTevOc5YA.htm](backgrounds/dSYel8ABTevOc5YA.htm)|Clockfighter|Mécanicien d'arène|libre|
|[dVRDDjT4FOu6uLDR.htm](backgrounds/dVRDDjT4FOu6uLDR.htm)|Detective|Détective|officielle|
|[DVtZab19D1vD3a0n.htm](backgrounds/DVtZab19D1vD3a0n.htm)|Post Guard of All Trades|Garde du poste touche-à-tout|libre|
|[E2ij2Cg8oMC0W0NS.htm](backgrounds/E2ij2Cg8oMC0W0NS.htm)|Nirmathi Guerrilla|Escarmoucheur Nirmathi|libre|
|[ECKic2p6yIyoGYod.htm](backgrounds/ECKic2p6yIyoGYod.htm)|Songsinger in Training|Apprenti chansonnier|libre|
|[eGPlKAoYBTUqjyrr.htm](backgrounds/eGPlKAoYBTUqjyrr.htm)|Tomb Born|Né de la tombe|libre|
|[eHfsMNiVeqwnrpG3.htm](backgrounds/eHfsMNiVeqwnrpG3.htm)|Framed in Ferrous Quarter|Piégé dans le quartier ferreux|libre|
|[EJRWGsPWzAhixuvQ.htm](backgrounds/EJRWGsPWzAhixuvQ.htm)|Belkzen Slayer|Tueur du Belkzen|libre|
|[eK8jNZvTwo2iPdEC.htm](backgrounds/eK8jNZvTwo2iPdEC.htm)|Awful Scab|Gale Effroyable|libre|
|[EYPorNgoYcp37akm.htm](backgrounds/EYPorNgoYcp37akm.htm)|Magical Misfit|Marginal de la magie|libre|
|[eYY3bX7xSH7aicqT.htm](backgrounds/eYY3bX7xSH7aicqT.htm)|Atteran Rancher|Rancher Atteran|libre|
|[f7ZTF8tnZUmTXwwF.htm](backgrounds/f7ZTF8tnZUmTXwwF.htm)|Osprey Warrior|Combattant de Balbuzard|libre|
|[ffcNsTUBsxFwbNgJ.htm](backgrounds/ffcNsTUBsxFwbNgJ.htm)|Lastwall Survivor|Survivant du Dernier-Rempart|libre|
|[FKHut73XDUGTnKkP.htm](backgrounds/FKHut73XDUGTnKkP.htm)|Field Medic|Médecin militaire|officielle|
|[FlXu29r5C4CborZv.htm](backgrounds/FlXu29r5C4CborZv.htm)|Friend of Greensteeples|Ami des verts clochers|libre|
|[fML6YrXYDqQy0g7L.htm](backgrounds/fML6YrXYDqQy0g7L.htm)|Iolite Trainee Hobgoblin|Recrue Iolite Hobgobeline|libre|
|[fo2nGLI1t1b7nAHK.htm](backgrounds/fo2nGLI1t1b7nAHK.htm)|Dreams of Vengeance|Rêves de vengeance|libre|
|[FovAoohhvG7vIbX9.htm](backgrounds/FovAoohhvG7vIbX9.htm)|Gold Falls Regular|Habitué des chutes dorées|libre|
|[fuTLDmihr9Z9e5wa.htm](backgrounds/fuTLDmihr9Z9e5wa.htm)|Cultist|Cultiste|libre|
|[FVE2mg6EEyPVLz3M.htm](backgrounds/FVE2mg6EEyPVLz3M.htm)|Student of Magic|Étudiant en magie|libre|
|[fyKPDYFeIrgzADJB.htm](backgrounds/fyKPDYFeIrgzADJB.htm)|Alkenstar Outlaw|Desperado d'Alkenastre|libre|
|[FznrY4fZASquT6hY.htm](backgrounds/FznrY4fZASquT6hY.htm)|Sponsored by a Stranger|Soutenu par un étranger|libre|
|[G3GIfGLY1xSNqj17.htm](backgrounds/G3GIfGLY1xSNqj17.htm)|Almas Clerk|Employé de bureau d'Almas|libre|
|[g7kBsG5VKCaMYEZj.htm](backgrounds/g7kBsG5VKCaMYEZj.htm)|Eclectic Scholar|Érudit éclectique|libre|
|[g8xUX7DAu2ShZ90Q.htm](backgrounds/g8xUX7DAu2ShZ90Q.htm)|Empty Whispers|Murmures vides|libre|
|[GeIFY5C1BxuWQGc7.htm](backgrounds/GeIFY5C1BxuWQGc7.htm)|Bookish Providence|Providence livresque|libre|
|[gfklP8ub45R4wXKe.htm](backgrounds/gfklP8ub45R4wXKe.htm)|Aspiring River Monarch|Aspirant Monarque du Fleuve|libre|
|[GNidqGnSABx1rQUQ.htm](backgrounds/GNidqGnSABx1rQUQ.htm)|Missionary|Missionnaire|libre|
|[GPI5kNu0xfom9kKa.htm](backgrounds/GPI5kNu0xfom9kKa.htm)|Dragon Scholar|Spécialiste des dragons|officielle|
|[gsyTHkbGRaiHgs6p.htm](backgrounds/gsyTHkbGRaiHgs6p.htm)|Firebrand Follower|Aspirant Agitateur|libre|
|[GyFfqCZZVtazmwPD.htm](backgrounds/GyFfqCZZVtazmwPD.htm)|Outskirt Dweller|Habitant de la périphérie|libre|
|[H3E69w8Xg0T7rAqD.htm](backgrounds/H3E69w8Xg0T7rAqD.htm)|Shoanti Name-Bearer|Porteur de nom Shoanti|libre|
|[h98cEl4DY75IL6KJ.htm](backgrounds/h98cEl4DY75IL6KJ.htm)|Teamster|Conducteur d'attelages|libre|
|[HdnmIaLadhRfZq8X.htm](backgrounds/HdnmIaLadhRfZq8X.htm)|Insurgent|Insurgé|libre|
|[HDquvQywAZimmcFF.htm](backgrounds/HDquvQywAZimmcFF.htm)|Refugee (Fall of Plaguestone)|Réfugié (la Chute de Plaguestone)|libre|
|[HEd2Lxgvl080nRxx.htm](backgrounds/HEd2Lxgvl080nRxx.htm)|Shadow Lodge Defector|Défecteur de la Loge d'Ombre|libre|
|[HiOvPmXEXBjUy0VZ.htm](backgrounds/HiOvPmXEXBjUy0VZ.htm)|Brevic Outcast|Paria brévien|libre|
|[HKRcQO8Xj7xzBxAw.htm](backgrounds/HKRcQO8Xj7xzBxAw.htm)|Faction Opportunist|Opportuniste des Factions|libre|
|[HMIfr9N4rco1dzBO.htm](backgrounds/HMIfr9N4rco1dzBO.htm)|Reclaimed|Reconquis|libre|
|[HNdGlDHkcczqNw2U.htm](backgrounds/HNdGlDHkcczqNw2U.htm)|Musical Prodigy|Prodige de la musique|libre|
|[hPx0xiv00GQqPWUH.htm](backgrounds/hPx0xiv00GQqPWUH.htm)|Kalistrade Follower|Suivant de Kalistrade|libre|
|[HrWEyXTgIj16Z1RR.htm](backgrounds/HrWEyXTgIj16Z1RR.htm)|Reborn Soul|Âme réincarnée|libre|
|[HsAZDAPpqynArFAp.htm](backgrounds/HsAZDAPpqynArFAp.htm)|Magaambya Academic|Étudiant du Magaambya|libre|
|[HSNUDvPgO1NESW7S.htm](backgrounds/HSNUDvPgO1NESW7S.htm)|Junk Collector|Collectionneur de déchets|libre|
|[HZ3oBBdEnsH3fWrm.htm](backgrounds/HZ3oBBdEnsH3fWrm.htm)|Shory Seeker|Chercheur Rivain|libre|
|[I0vuIFypx8ADSJQC.htm](backgrounds/I0vuIFypx8ADSJQC.htm)|Black Market Smuggler|Contrebandier du Marché noir|libre|
|[i28Z9JXhEvoc7BX5.htm](backgrounds/i28Z9JXhEvoc7BX5.htm)|Refugee (APG)|Réfugié (MJA)|libre|
|[i4hN6OYv8qmi3GLW.htm](backgrounds/i4hN6OYv8qmi3GLW.htm)|Entertainer|Bateleur|officielle|
|[I5cRrqrPCHsQqFI9.htm](backgrounds/I5cRrqrPCHsQqFI9.htm)|Magical Merchant|Marchand magique|libre|
|[i5G6E5dkGWiq838C.htm](backgrounds/i5G6E5dkGWiq838C.htm)|Scholar of the Ancients|Érudit de l'Antiquité|libre|
|[i6y4DiKvqitdE0PW.htm](backgrounds/i6y4DiKvqitdE0PW.htm)|Quick|"Vif"|libre|
|[i79pgNIAtJfkkOiw.htm](backgrounds/i79pgNIAtJfkkOiw.htm)|Tinker|Bricoleur|officielle|
|[ia5aFzfmfnMjhcms.htm](backgrounds/ia5aFzfmfnMjhcms.htm)|Folklore Enthusiast|Enthousiaste du folklore|libre|
|[iaM6TjvijLCgiHeD.htm](backgrounds/iaM6TjvijLCgiHeD.htm)|Returning Descendant|Descendant de retour|officielle|
|[idFSq3DYBRomYmZt.htm](backgrounds/idFSq3DYBRomYmZt.htm)|Concordance Scout|Éclaireur de la Concordance|libre|
|[IFHYbU6Nu8BiTsRa.htm](backgrounds/IFHYbU6Nu8BiTsRa.htm)|Acrobat|Acrobate|officielle|
|[IfpYRxN8qyV4ym0o.htm](backgrounds/IfpYRxN8qyV4ym0o.htm)|Purveyor of the Bizarre|Fournisseur de Bizarreries|libre|
|[IoBhge83aYpq0pPV.htm](backgrounds/IoBhge83aYpq0pPV.htm)|Archaeologist|Archéologue|libre|
|[IObZEUz8wneEMgR3.htm](backgrounds/IObZEUz8wneEMgR3.htm)|Deckhand|Homme de pont|libre|
|[IOcjPmcemrQFFb2b.htm](backgrounds/IOcjPmcemrQFFb2b.htm)|Time Traveler|Voyageur du temps|libre|
|[irDibuV3Wi7T43sL.htm](backgrounds/irDibuV3Wi7T43sL.htm)|Child of the Puddles|Marmot des flaques|libre|
|[IuwNiQSSeRMQyDE7.htm](backgrounds/IuwNiQSSeRMQyDE7.htm)|Sword Scion|Descendant de l'épée|libre|
|[iWWg16f3re1YChiD.htm](backgrounds/iWWg16f3re1YChiD.htm)|Oenopion-Ooze Tender|Éleveur de vases d'Oenopion|libre|
|[ixluAGUDZciLEHtb.htm](backgrounds/ixluAGUDZciLEHtb.htm)|Tax Collector|Percepteur|libre|
|[IXxdCzBS0xP20ckw.htm](backgrounds/IXxdCzBS0xP20ckw.htm)|Ward|Pupille|libre|
|[j2G71vQahw1DiWpO.htm](backgrounds/j2G71vQahw1DiWpO.htm)|Cursed|Maudit|libre|
|[j9v38iHA0sVy59SR.htm](backgrounds/j9v38iHA0sVy59SR.htm)|Pathfinder Hopeful|Aspirant Éclaireur|officielle|
|[JauSkDtMV6dhDZS8.htm](backgrounds/JauSkDtMV6dhDZS8.htm)|Political Scion|Rejeton de politicien|libre|
|[JBRBb2818DZ4hjXw.htm](backgrounds/JBRBb2818DZ4hjXw.htm)|Total Power|Pouvoir total|libre|
|[JfGVkZkaoz2lnmov.htm](backgrounds/JfGVkZkaoz2lnmov.htm)|Bright Lion|Lion Brillant|libre|
|[JjAh5pxgHrW3fwaH.htm](backgrounds/JjAh5pxgHrW3fwaH.htm)|Osprey Scribe|Scribe de Balbuzard|libre|
|[jjkY6r7NNWhxDqja.htm](backgrounds/jjkY6r7NNWhxDqja.htm)|Once Bitten|Mordu une fois|libre|
|[jpgO5zofecGeyXd5.htm](backgrounds/jpgO5zofecGeyXd5.htm)|Occult Librarian|Bibliothécaire occulte|libre|
|[K35I1WCbzT5xnJ6N.htm](backgrounds/K35I1WCbzT5xnJ6N.htm)|Animal Wrangler|Dresseur animalier|libre|
|[KaIeesLNmn3s1Y1I.htm](backgrounds/KaIeesLNmn3s1Y1I.htm)|Fightbreaker|Brise-combat|libre|
|[KboE8OcjCLD7oFWQ.htm](backgrounds/KboE8OcjCLD7oFWQ.htm)|Beast Blessed|Béni par la bête|libre|
|[KC5oI1bs6Wx8h91u.htm](backgrounds/KC5oI1bs6Wx8h91u.htm)|Deputy|Adjoint|libre|
|[KEG5KFNrXKhTsj6J.htm](backgrounds/KEG5KFNrXKhTsj6J.htm)|Money Counter|Compteur de pièces|libre|
|[khGFmnQMBYmz2ONR.htm](backgrounds/khGFmnQMBYmz2ONR.htm)|Sarkorian Reclaimer|Reconquérant du Sarkoris|libre|
|[KHMEcCCG132ILkuU.htm](backgrounds/KHMEcCCG132ILkuU.htm)|Starless One|Né sans étoile|libre|
|[kkPdZsf61qySImQD.htm](backgrounds/kkPdZsf61qySImQD.htm)|Runner|Coursier|libre|
|[kLjqmylGOXeQ5o5Y.htm](backgrounds/kLjqmylGOXeQ5o5Y.htm)|Bonuwat Wavetouched|Bonuwat Touché par les vagues|libre|
|[KLWxwhCOdqrbepbu.htm](backgrounds/KLWxwhCOdqrbepbu.htm)|Child Of Notoriety|Enfant de la notoriété|libre|
|[kmhZZgR7KlBzRBX0.htm](backgrounds/kmhZZgR7KlBzRBX0.htm)|Driver|Pilote|libre|
|[KMv7ollLVaZ81XDV.htm](backgrounds/KMv7ollLVaZ81XDV.htm)|Merchant|Marchand|officielle|
|[l5Kj0owzxfPcTvIb.htm](backgrounds/l5Kj0owzxfPcTvIb.htm)|Unsponsored|Non soutenu|libre|
|[lav3yRNPc7lQ7e9k.htm](backgrounds/lav3yRNPc7lQ7e9k.htm)|Senghor Sailor|Marin de Senghor|libre|
|[LbuWzGpCIP79UwVB.htm](backgrounds/LbuWzGpCIP79UwVB.htm)|Spell Seeker|Chercheur de sort|libre|
|[lCR8gyEZbwqh3RWi.htm](backgrounds/lCR8gyEZbwqh3RWi.htm)|Harbor Guard Moonlighter|Noctambule de la Garde du port|libre|
|[LHk50lz5Kk5ZYTeo.htm](backgrounds/LHk50lz5Kk5ZYTeo.htm)|Abadar's Avenger|Vengeur d'Abadar|libre|
|[lMJYIXSh3NSJslmi.htm](backgrounds/lMJYIXSh3NSJslmi.htm)|Sky Rider|Cavalier du ciel|libre|
|[locc0cjOmOQHe3j7.htm](backgrounds/locc0cjOmOQHe3j7.htm)|Savior of Air|Sauveur de l'air|libre|
|[LoeTd2SS6jfEgo1H.htm](backgrounds/LoeTd2SS6jfEgo1H.htm)|Genie-Blessed|Béni des génies|libre|
|[lqjmBmGHYRaSiglZ.htm](backgrounds/lqjmBmGHYRaSiglZ.htm)|Molthuni Mercenary|Mercenaire du Molthune|libre|
|[LwAu4r3uocYfpKA8.htm](backgrounds/LwAu4r3uocYfpKA8.htm)|Trailblazer|Pionnier|libre|
|[LWoPiYHAyLp8pvYx.htm](backgrounds/LWoPiYHAyLp8pvYx.htm)|Broken Tusk Recruiter|Recruteur de la Défense brisée|libre|
|[lX5KDS2hU5LihZRs.htm](backgrounds/lX5KDS2hU5LihZRs.htm)|Martial Disciple|Disciple martial|libre|
|[m1vRLRHTpCrgk89G.htm](backgrounds/m1vRLRHTpCrgk89G.htm)|Thrune Loyalist|Loyaliste Thrune|libre|
|[m2sWdTIXlyXmqsuC.htm](backgrounds/m2sWdTIXlyXmqsuC.htm)|Keys to Destiny|Clés de la destinée|libre|
|[M8EfhY6Knb2iQm6S.htm](backgrounds/M8EfhY6Knb2iQm6S.htm)|Press-Ganged (G&G)|Forçat des mers (G&G)|libre|
|[maxaKunHAOKUrR4q.htm](backgrounds/maxaKunHAOKUrR4q.htm)|Writ in the Stars|Écrit dans les étoiles|libre|
|[mcL4WLO2yxBGlvuG.htm](backgrounds/mcL4WLO2yxBGlvuG.htm)|Farmsteader|Paysan|libre|
|[MgiZ25JdT6O3fLbO.htm](backgrounds/MgiZ25JdT6O3fLbO.htm)|Anti-Tech Activist|Activiste anti-techno|libre|
|[mh0hj27ZO5qDWat1.htm](backgrounds/mh0hj27ZO5qDWat1.htm)|Osprey Barnraiser|Édifieur de grange de Balbuzard|libre|
|[MiRWGXZnEdurMvVf.htm](backgrounds/MiRWGXZnEdurMvVf.htm)|Eldritch Anatomist|Anatomiste mystique|libre|
|[moVRsnpjB5THCwxE.htm](backgrounds/moVRsnpjB5THCwxE.htm)|Street Urchin|Enfant des rues|officielle|
|[mrkgVjiEdlPjLUsN.htm](backgrounds/mrkgVjiEdlPjLUsN.htm)|Vidrian Reformer|Réformateur Vidrien|libre|
|[MrZvq1ebEgEN9cIv.htm](backgrounds/MrZvq1ebEgEN9cIv.htm)|Sandswept Survivor|Survivant du Tourbillon de sable|libre|
|[MslumKt6iwJ85GKZ.htm](backgrounds/MslumKt6iwJ85GKZ.htm)|Thuvian Unifier|Unificateur thuvien|libre|
|[mWpNrTiREluJ6fLB.htm](backgrounds/mWpNrTiREluJ6fLB.htm)|Able Carter|Charretier capable|libre|
|[mxJRdRSMsyZfBf5c.htm](backgrounds/mxJRdRSMsyZfBf5c.htm)|Hermean Heritor|Héritier d'Herméa|libre|
|[Mz7G3rTPxV2Xzy18.htm](backgrounds/Mz7G3rTPxV2Xzy18.htm)|Hammered by Fate|Martelé par le destin|libre|
|[n2JN5Kiu7tOCAHPr.htm](backgrounds/n2JN5Kiu7tOCAHPr.htm)|Market Runner|Coureur du marché|libre|
|[n7tPT1SfVTcHjrj3.htm](backgrounds/n7tPT1SfVTcHjrj3.htm)|Tapestry Refugee|Réfugié de la Tapisserie|libre|
|[nAe65lvsOJAIHGGT.htm](backgrounds/nAe65lvsOJAIHGGT.htm)|False Medium|Faux médium|libre|
|[nhQKn1tVV6PKCurq.htm](backgrounds/nhQKn1tVV6PKCurq.htm)|Butcher|"Boucher"|libre|
|[ns9BOvCyjdapYhI0.htm](backgrounds/ns9BOvCyjdapYhI0.htm)|Root Worker|Cueilleur de racines|libre|
|[nXnaV9JwUG1N2dsg.htm](backgrounds/nXnaV9JwUG1N2dsg.htm)|Attention Addict|Avide de notoriété|libre|
|[NXYce9NAHls2fcIf.htm](backgrounds/NXYce9NAHls2fcIf.htm)|Pathfinder Recruiter|Recruteur des Éclaireurs|libre|
|[NywLl1XMQmzA6rP7.htm](backgrounds/NywLl1XMQmzA6rP7.htm)|Demon Slayer|Tueur de démons|libre|
|[NZY0r4Csjul6eVPp.htm](backgrounds/NZY0r4Csjul6eVPp.htm)|Finadar Leshy|Léchi de Finadar|libre|
|[o1lhSKOpKPamTITI.htm](backgrounds/o1lhSKOpKPamTITI.htm)|Bookkeeper|Comptable|libre|
|[o3ArTg5c8pLe7iGm.htm](backgrounds/o3ArTg5c8pLe7iGm.htm)|Song of the Deep|Chant des profondeurs de l'océan|libre|
|[O4xKRtHb21DCTvQ0.htm](backgrounds/O4xKRtHb21DCTvQ0.htm)|Wished Alive|Désiré vivant|libre|
|[o7RbsQbv5iLRvd8j.htm](backgrounds/o7RbsQbv5iLRvd8j.htm)|Scout|Éclaireur|officielle|
|[OD8RTS7cTeMJJFcR.htm](backgrounds/OD8RTS7cTeMJJFcR.htm)|Learned Guard Prodigy|Prodige de la garde instruite|libre|
|[Ods1qOZ8CTCru8XQ.htm](backgrounds/Ods1qOZ8CTCru8XQ.htm)|Conservator|Conservateur|libre|
|[oEm937kNrP5sXxFD.htm](backgrounds/oEm937kNrP5sXxFD.htm)|Emancipated|Émancipé|officielle|
|[OfdT8v9apVNZyYqt.htm](backgrounds/OfdT8v9apVNZyYqt.htm)|Highborn Snoop|Fouineur bien né|libre|
|[OhP7cqvNouFgHIdJ.htm](backgrounds/OhP7cqvNouFgHIdJ.htm)|Sewer Dragon|Dragon des égoûts|libre|
|[oj4B6KLAOlUbY0wr.htm](backgrounds/oj4B6KLAOlUbY0wr.htm)|Eclipseborn|Né lors d'une éclipse|libre|
|[p27PSjFtHAWikKaw.htm](backgrounds/p27PSjFtHAWikKaw.htm)|Early Explorer|Explorateur précoce|libre|
|[P65AGDPkhD2B4JtG.htm](backgrounds/P65AGDPkhD2B4JtG.htm)|Perfection Seeker|À la recherche de la Perfection|libre|
|[p6Asr6f2cI2BWorr.htm](backgrounds/p6Asr6f2cI2BWorr.htm)|Sponsored by Teacher Ot|Soutenu par Professeur Ot|libre|
|[pBX18FI1grWwkWjk.htm](backgrounds/pBX18FI1grWwkWjk.htm)|Kyonin Emissary|Émissaire du Kyonin|libre|
|[pChM2ApTVEQ8SohP.htm](backgrounds/pChM2ApTVEQ8SohP.htm)|Reclaimer Investigator|Enquêteur reconquérant|libre|
|[pGOlKz4Krnh7MyUM.htm](backgrounds/pGOlKz4Krnh7MyUM.htm)|Haunted|Hanté|libre|
|[PhqUBXLLkVXb6oUE.htm](backgrounds/PhqUBXLLkVXb6oUE.htm)|Taldan Schemer|Intrigant taldorien|libre|
|[Phvnfdmz4bB7jrI3.htm](backgrounds/Phvnfdmz4bB7jrI3.htm)|Barrister|Avocat|officielle|
|[PJxPNMEUTimop4Lq.htm](backgrounds/PJxPNMEUTimop4Lq.htm)|Osprey Scion|Rejeton de Balbuzard|libre|
|[ppBGlWl0UkBKkJgE.htm](backgrounds/ppBGlWl0UkBKkJgE.htm)|Feybound|Lié avec les fées|libre|
|[pPR0HAeXLUxP3rx4.htm](backgrounds/pPR0HAeXLUxP3rx4.htm)|Clan Associate|Associé du clan|libre|
|[q1OCPBAgYgvwy1Of.htm](backgrounds/q1OCPBAgYgvwy1Of.htm)|Megafauna Hunter|Chasseur de mégafaune|libre|
|[Q2brdDtEoI3cmpuD.htm](backgrounds/Q2brdDtEoI3cmpuD.htm)|Feral Child|Enfant sauvage|libre|
|[qB7g1OiZ8v8zgvkL.htm](backgrounds/qB7g1OiZ8v8zgvkL.htm)|Wonder Taster|Goûteur de merveilles|libre|
|[qbvzNG8hMjb8f66D.htm](backgrounds/qbvzNG8hMjb8f66D.htm)|Squire|Écuyer|libre|
|[QGji0lMUo6to2h2k.htm](backgrounds/QGji0lMUo6to2h2k.htm)|Fire Warden|Gardien du feu|libre|
|[qJNmSgzq0ae29qCC.htm](backgrounds/qJNmSgzq0ae29qCC.htm)|Printer|Imprimeur|libre|
|[QnL7hqUi9HPenrbC.htm](backgrounds/QnL7hqUi9HPenrbC.htm)|Newcomer In Need|Nouveau venu dans le besoin|libre|
|[qNpLqx6LhBo1jY4A.htm](backgrounds/qNpLqx6LhBo1jY4A.htm)|Blow-In|Nouveau venu|libre|
|[qY4IUwVWIKPSFskP.htm](backgrounds/qY4IUwVWIKPSFskP.htm)|Aerialist|Trapéziste|libre|
|[qzKT7L1qldz14q8M.htm](backgrounds/qzKT7L1qldz14q8M.htm)|Legacy of the Hammer|Héritage du marteau|libre|
|[r0kYIbN06Cv8eNG3.htm](backgrounds/r0kYIbN06Cv8eNG3.htm)|Warrior|Homme d'armes|officielle|
|[R1v4gUu8oRMoOASM.htm](backgrounds/R1v4gUu8oRMoOASM.htm)|Wildwood Local|Habitué des bois|libre|
|[r2ZRAcoDp6EdwNrh.htm](backgrounds/r2ZRAcoDp6EdwNrh.htm)|Free Spirit|Esprit libre|libre|
|[r9fzNQEz33HyKTxm.htm](backgrounds/r9fzNQEz33HyKTxm.htm)|Pilgrim|Pélerin|libre|
|[RC4l6WsxPn89a1f8.htm](backgrounds/RC4l6WsxPn89a1f8.htm)|Raised by Belief|Élevé dans la Foi|libre|
|[RPU1aNfVz3ZbymvV.htm](backgrounds/RPU1aNfVz3ZbymvV.htm)|Scion of Slayers|Fils de tueurs|libre|
|[RxhdWJUoRTBEeHYZ.htm](backgrounds/RxhdWJUoRTBEeHYZ.htm)|Night Watch|Veilleur de nuit|libre|
|[rzyRtasSTfHS3e0y.htm](backgrounds/rzyRtasSTfHS3e0y.htm)|Returned|Ressuscité|libre|
|[SJ3nNOI5A8A4hK0Q.htm](backgrounds/SJ3nNOI5A8A4hK0Q.htm)|Winter's Child|Enfant de l'hiver|libre|
|[Sj91CUyEUeWoPh2R.htm](backgrounds/Sj91CUyEUeWoPh2R.htm)|Toymaker|Fabricant de jouets|libre|
|[SOmJyAtPOokesZoe.htm](backgrounds/SOmJyAtPOokesZoe.htm)|Farmhand|Ouvrier agricole|officielle|
|[sR3S7Xn15drU6rOF.htm](backgrounds/sR3S7Xn15drU6rOF.htm)|Starwatcher|Observateur d'étoiles|libre|
|[StWWI7Wi0WRgRmxS.htm](backgrounds/StWWI7Wi0WRgRmxS.htm)|Nocturnal Navigator|Navigateur nocturne|libre|
|[su8y75pGMVTUsNHK.htm](backgrounds/su8y75pGMVTUsNHK.htm)|Thassilonian Traveler|Voyageur Thassilonien|libre|
|[t0t1ck8iKpaI4o5W.htm](backgrounds/t0t1ck8iKpaI4o5W.htm)|Charlatan|Charlatan|officielle|
|[T537wo3aem8GvnmR.htm](backgrounds/T537wo3aem8GvnmR.htm)|Hounded Thief|Voleur pourchassé|libre|
|[T8SlgHjj0hVjKW2Q.htm](backgrounds/T8SlgHjj0hVjKW2Q.htm)|Sponsored by Family|Soutenu par la famille|libre|
|[T8YNd9Jg6XBJvNBQ.htm](backgrounds/T8YNd9Jg6XBJvNBQ.htm)|Union Representative|Représentant syndical|libre|
|[tA0nggrWfBEhvsKA.htm](backgrounds/tA0nggrWfBEhvsKA.htm)|Chelish Rebel|Rebelle chélaxien|libre|
|[TC7jpN5EA4UBIYep.htm](backgrounds/TC7jpN5EA4UBIYep.htm)|Truth Seeker|En quête de vérité|officielle|
|[tcsSxwkl4wCsfO3k.htm](backgrounds/tcsSxwkl4wCsfO3k.htm)|Trade Consortium Underling|Sous-fifre de consortium marchand|libre|
|[tGtrad5lJzXPZCx6.htm](backgrounds/tGtrad5lJzXPZCx6.htm)|Deep-Sea Diver|Plongeur des profondeurs|libre|
|[TPoP1mKpqUOpRQ5Y.htm](backgrounds/TPoP1mKpqUOpRQ5Y.htm)|Reputation Seeker|En quête de renommée|officielle|
|[tQ9t7uIssRCR2y3W.htm](backgrounds/tQ9t7uIssRCR2y3W.htm)|Final Blade Survivor|Survivant de la Lame finale|libre|
|[TQBYPTRTVGLMv7cx.htm](backgrounds/TQBYPTRTVGLMv7cx.htm)|Local Brigand|Brigand local|libre|
|[tqnrnXVTbdehohPL.htm](backgrounds/tqnrnXVTbdehohPL.htm)|Street Preacher|Prêcheur des rues|libre|
|[Tujic4RHrQJmEYX4.htm](backgrounds/Tujic4RHrQJmEYX4.htm)|Mystic Tutor|Tuteur mystique|libre|
|[Ty8FRM0k262xuHfF.htm](backgrounds/Ty8FRM0k262xuHfF.htm)|Undersea Enthusiast|Enthousiaste aquatique|libre|
|[U1gbRkmZqJ7SmpeF.htm](backgrounds/U1gbRkmZqJ7SmpeF.htm)|Banished Brighite|Adepte de Brigh banni|libre|
|[U8347JbRAjhowP1q.htm](backgrounds/U8347JbRAjhowP1q.htm)|Brevic Noble|Noble brévien|libre|
|[uC6D2nmDTATxXrV6.htm](backgrounds/uC6D2nmDTATxXrV6.htm)|Royalty|De sang royal|libre|
|[UdOUj7i8XGTI72Zc.htm](backgrounds/UdOUj7i8XGTI72Zc.htm)|Servant|Serviteur|libre|
|[uF9nw15tK6b1bgre.htm](backgrounds/uF9nw15tK6b1bgre.htm)|Ruby Phoenix Fanatic|Fanatique du Phénix de rubis|libre|
|[UFHezf1LXUwcQIAQ.htm](backgrounds/UFHezf1LXUwcQIAQ.htm)|Wandering Preacher|Prêcheur errant|libre|
|[UgityMZaujmYUpil.htm](backgrounds/UgityMZaujmYUpil.htm)|Out-Of-Towner|De passage|officielle|
|[uJcFanGjVranEarv.htm](backgrounds/uJcFanGjVranEarv.htm)|Lumber Consortium Laborer|Ouvrier du Consortium du bois|libre|
|[UK40FVVVx6IKxipW.htm](backgrounds/UK40FVVVx6IKxipW.htm)|Inexplicably Expelled|Expulsé inexplicablement|libre|
|[Uk4W7mKQbLtDDHwo.htm](backgrounds/Uk4W7mKQbLtDDHwo.htm)|Curandero|Guérisseur (curandero)|libre|
|[uker8dnTA7WhzXSR.htm](backgrounds/uker8dnTA7WhzXSR.htm)|Close Ties|Liens étroits|libre|
|[UNbV4UwZEXxJy703.htm](backgrounds/UNbV4UwZEXxJy703.htm)|Sun Dancer|Danseur du soleil|libre|
|[uNhdcyhiog7YvXPT.htm](backgrounds/uNhdcyhiog7YvXPT.htm)|Varisian Wanderer|Vagabond Varisien|libre|
|[uNvD4XK1kdvGjQVo.htm](backgrounds/uNvD4XK1kdvGjQVo.htm)|Child of Westcrown|Enfant de Couronne d'ouest|libre|
|[UU4MOe7Lozt9V8tg.htm](backgrounds/UU4MOe7Lozt9V8tg.htm)|Seer of the Dead|Voyant des morts|libre|
|[UURvnfwXypRYYXBI.htm](backgrounds/UURvnfwXypRYYXBI.htm)|Storm Survivor|Survivant d'une tempête|libre|
|[uwZ0emSBFNMGA74j.htm](backgrounds/uwZ0emSBFNMGA74j.htm)|Tall Tale|Mythomane|libre|
|[UyddtAwqDGjQ1SZK.htm](backgrounds/UyddtAwqDGjQ1SZK.htm)|Scholar of the Sky Key|Érudit de la Clé du ciel|libre|
|[v0WPfxN6G8XFfFZT.htm](backgrounds/v0WPfxN6G8XFfFZT.htm)|Scholar|Érudit|libre|
|[V1RAIckpUJd2OzXi.htm](backgrounds/V1RAIckpUJd2OzXi.htm)|Mana Wastes Refugee|Réfugié de la Désolation de Mana|libre|
|[v319qChG1uAQkGEe.htm](backgrounds/v319qChG1uAQkGEe.htm)|Osprey Fisher|Pêcheur de Balbuzard|libre|
|[V31KRG7aA7xS0m8L.htm](backgrounds/V31KRG7aA7xS0m8L.htm)|Shadow Haunted|Ombre hantée|libre|
|[V3nYEAhyA54RtYky.htm](backgrounds/V3nYEAhyA54RtYky.htm)|Ulfen Raider|Raider ulfe|libre|
|[v8pS4UCYjt9OTtQL.htm](backgrounds/v8pS4UCYjt9OTtQL.htm)|Osprey Scout|Éclaireur de Balbuzard|libre|
|[V9VsomfjbPQNfaSL.htm](backgrounds/V9VsomfjbPQNfaSL.htm)|Disciple of the Gear|Disciple de l'engrenage|libre|
|[vBPu7RwNXGDQ1ThL.htm](backgrounds/vBPu7RwNXGDQ1ThL.htm)|Dreamer of the Verdant Moon|Rêveur de la lune verdoyante|libre|
|[vE6nb2OSIXqprDXk.htm](backgrounds/vE6nb2OSIXqprDXk.htm)|Sally Guard Neophyte|Néophyte de la Garde des Percées|libre|
|[vEl3OBUsSmPh8b4N.htm](backgrounds/vEl3OBUsSmPh8b4N.htm)|Friendly Darkmoon Kobold|Kobold de la lune noire amicale|libre|
|[vgin9ff2sUBMpuaI.htm](backgrounds/vgin9ff2sUBMpuaI.htm)|Former Aspis Agent|Ancien agent de l'Aspis|libre|
|[vHeP960qjhfob4Je.htm](backgrounds/vHeP960qjhfob4Je.htm)|Scavenger|Récupérateur|libre|
|[vjhB0ZTV9OZgSuSz.htm](backgrounds/vjhB0ZTV9OZgSuSz.htm)|Thassilonian Delver|Fouilleur thassilonnien|libre|
|[vmdhw37F22FCMm50.htm](backgrounds/vmdhw37F22FCMm50.htm)|Food Trader|Marchand de nourriture|libre|
|[vMyMUZpg8MYZT2AZ.htm](backgrounds/vMyMUZpg8MYZT2AZ.htm)|Wish for Riches|Souhaiter la fortune|libre|
|[vNWSzv36L1GBPPoc.htm](backgrounds/vNWSzv36L1GBPPoc.htm)|Hellknight Historian|Historien des chevaliers infernaux|officielle|
|[Vq6kOTBm114bV9y7.htm](backgrounds/Vq6kOTBm114bV9y7.htm)|Shielded Fortune|Bouclier de chance|libre|
|[vTyG1bQkEENZHroY.htm](backgrounds/vTyG1bQkEENZHroY.htm)|Junker|Camelot|libre|
|[w2GSRC5uMcSUfPGJ.htm](backgrounds/w2GSRC5uMcSUfPGJ.htm)|Chosen One|Élu|libre|
|[W6OIPZhjcV45gfoX.htm](backgrounds/W6OIPZhjcV45gfoX.htm)|Revenant|Revenant|libre|
|[wGB222d5UIBGYWsK.htm](backgrounds/wGB222d5UIBGYWsK.htm)|Mechanical Symbiosis|Symbiose mécanique|libre|
|[WIWR8jURAdSAzxIh.htm](backgrounds/WIWR8jURAdSAzxIh.htm)|Magical Experiment|Expérience magique|libre|
|[wKiFedYlHCsM5wN4.htm](backgrounds/wKiFedYlHCsM5wN4.htm)|Rostlander|Habitant du Rost|libre|
|[WrKVeUQ6KeRCm9uH.htm](backgrounds/WrKVeUQ6KeRCm9uH.htm)|Teacher|Enseignant|libre|
|[wU1qd8tZNcYn43y2.htm](backgrounds/wU1qd8tZNcYn43y2.htm)|Lost and Alone|Perdu et seul|libre|
|[wudDO9OEsRjJsqhU.htm](backgrounds/wudDO9OEsRjJsqhU.htm)|Barber|Barbier|libre|
|[WueM94C9JXk10jPd.htm](backgrounds/WueM94C9JXk10jPd.htm)|Onyx Trader|Marchand d'onyx|libre|
|[wz4fDeZtCvxC4vyO.htm](backgrounds/wz4fDeZtCvxC4vyO.htm)|Second Chance Champion|Champion de la seconde chance|libre|
|[wZikbWmCWYpjFxQF.htm](backgrounds/wZikbWmCWYpjFxQF.htm)|Wanted Witness|Témoin recherché|libre|
|[X1YhwDic2ugT9RuQ.htm](backgrounds/X1YhwDic2ugT9RuQ.htm)|Harrow-Chosen|Choisi par le Tourment|libre|
|[x2y25cE98Eq4qxbu.htm](backgrounds/x2y25cE98Eq4qxbu.htm)|Ustalavic Academic|Étudiant ustalavien|libre|
|[x3fFxtLogoy5hZfe.htm](backgrounds/x3fFxtLogoy5hZfe.htm)|Medicinal Clocksmith|Mécanicien médicinal|libre|
|[xbyQ1RAF6x4ceXLf.htm](backgrounds/xbyQ1RAF6x4ceXLf.htm)|Energy Scarred|Scarifié par l'énergie|libre|
|[XcacLiaGDgjzHS3T.htm](backgrounds/XcacLiaGDgjzHS3T.htm)|Spotter|Guetteur|libre|
|[xCCvT9tprRQVFVDq.htm](backgrounds/xCCvT9tprRQVFVDq.htm)|Grizzled Muckrucker|Fangeux désabusé|libre|
|[XHY0xrSSbX0cTJKK.htm](backgrounds/XHY0xrSSbX0cTJKK.htm)|Droskari Disciple|Disciple de Droskar|libre|
|[xKEQvxDMHWRUkL6i.htm](backgrounds/xKEQvxDMHWRUkL6i.htm)|Clockwork Researcher|Chercheur en mécanique|libre|
|[xkRZUxWna7BcNnHZ.htm](backgrounds/xkRZUxWna7BcNnHZ.htm)|Unremarkable|Sans particularité|libre|
|[xvz5F7iYBWEIjz0r.htm](backgrounds/xvz5F7iYBWEIjz0r.htm)|Bibliophile|Bibliophile|libre|
|[xyHndp512sLU4UOL.htm](backgrounds/xyHndp512sLU4UOL.htm)|Northland Forager|Glaneur du nord|libre|
|[y0WZS51fSi6dILHq.htm](backgrounds/y0WZS51fSi6dILHq.htm)|Secular Medic|Médecin séculier|libre|
|[Y35nOXZRryiyHjlk.htm](backgrounds/Y35nOXZRryiyHjlk.htm)|Nexian Mystic|Mystique nexien|libre|
|[Y50ssWBBKSRVBpSa.htm](backgrounds/Y50ssWBBKSRVBpSa.htm)|Mystic Seer|Voyant mystique|libre|
|[y9OyNsxGfmjqdcP0.htm](backgrounds/y9OyNsxGfmjqdcP0.htm)|Artist|Artiste|officielle|
|[Yai061jIDzojHzsn.htm](backgrounds/Yai061jIDzojHzsn.htm)|Necromancer's Apprentice|Apprenti de nécromant|libre|
|[yAtyaKbcHZWCJlf5.htm](backgrounds/yAtyaKbcHZWCJlf5.htm)|Witch Wary|Gare aux sorcières|libre|
|[YGJzgmHNqw9C72Kb.htm](backgrounds/YGJzgmHNqw9C72Kb.htm)|Elementally Infused|Imprégné par les éléments|libre|
|[YJpEdmSOjlA2QZeu.htm](backgrounds/YJpEdmSOjlA2QZeu.htm)|Circus Born|Circassien|libre|
|[yK40c3082U30BUX5.htm](backgrounds/yK40c3082U30BUX5.htm)|Grand Council Bureaucrat|Bureaucrate du Grand conseil|libre|
|[ykYaQwIX7sxvbhwc.htm](backgrounds/ykYaQwIX7sxvbhwc.htm)|Willing Host|Hôte consentant|libre|
|[ynObDI0VbZ4sqeMI.htm](backgrounds/ynObDI0VbZ4sqeMI.htm)|Ex-Mendevian Crusader|Ancien croisé du Mendev|libre|
|[Yu7Cl0Lk94LdPRi6.htm](backgrounds/Yu7Cl0Lk94LdPRi6.htm)|Noble|Noble|libre|
|[Yuwr2pT3z3WYTX9T.htm](backgrounds/Yuwr2pT3z3WYTX9T.htm)|Alkenstar Sojourner|Visiteur d'Alkenastre|libre|
|[YyzIzLxn2UCFubj4.htm](backgrounds/YyzIzLxn2UCFubj4.htm)|Menagerie Dung Sweeper|Balayeur de fumier de la Ménagerie|libre|
|[yZUzLCeuaIkNk4up.htm](backgrounds/yZUzLCeuaIkNk4up.htm)|Saved by Clockwork|Sauvé par la mécanique|libre|
|[z4cCsOT36MB7xldR.htm](backgrounds/z4cCsOT36MB7xldR.htm)|Barker|Aboyeur|libre|
|[ZdhPKEY9FfaOS8Wy.htm](backgrounds/ZdhPKEY9FfaOS8Wy.htm)|Herbalist|Herboriste|officielle|
|[Zmwyhsxe4i6rZN75.htm](backgrounds/Zmwyhsxe4i6rZN75.htm)|Sailor|Marin|libre|
|[zwjoAOGkT44MmDKT.htm](backgrounds/zwjoAOGkT44MmDKT.htm)|Alloysmith|Forgeron d'alliage|libre|
